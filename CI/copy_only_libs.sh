#!/bin/bash

deps_dir="$1"
target_dir="$2"
shift 2

if [ ! -d "$deps_dir" ] || [ -z "target_dir" ]; then
    echo "usage: copy_only_libs.sh <deps_dir> <target_dir>"
fi

all_deps="sol irrlicht raknet cegui catch2 lua"

SUB_PARTS="$@"
if [ -z "$SUB_PARTS" ]; then
    SUB_PARTS=$all_deps
fi

deps_dir=$(realpath "$deps_dir")
target_dir=$(realpath "$target_dir")

do_sol()
{
    cp -vr "$deps_dir/sol2/sol/." "$target_dir/include/sol"
    cp -v "$deps_dir/sol2/sol.hpp" "$target_dir/include/sol.hpp"
}

do_irrlicht()
{
    cp -vr "$deps_dir/Irrlicht_extended/include/." "$target_dir/include/irrlicht"
    cp -vr "$deps_dir/Irrlicht_extended/lib/Linux/." "$target_dir/lib/"
}

do_raknet()
{
    cp -vr "$deps_dir/RakNet/include/raknet/." "$target_dir/include/raknet"
    cp -v "$deps_dir/RakNet/lib/libRakNetLibStatic.a" "$target_dir/lib"
}

do_cegui()
{
    cp -vr "$deps_dir/cegui-0.8.7/cegui/include/CEGUI" "$target_dir/include"
    cp -vr "$deps_dir/cegui-0.8.7/build/lib/." "$target_dir/lib"
}

do_catch2()
{
    mkdir -vp "$target_dir/include/catch2"
    cp -v "$deps_dir/catch2.hpp" "$target_dir/include/catch2/catch.hpp"
}

set -e

mkdir -vp "$target_dir/include"
mkdir -vp "$target_dir/lib"

for sub_part in $SUB_PARTS; do
    case "$sub_part" in
        sol)
            do_sol ;;
        irrlicht)
            do_irrlicht ;;
        raknet)
            do_raknet ;;
        cegui)
            do_cegui ;;
        catch2)
            do_catch2 ;;
        *)
            echo "Unknown argument: '$sub_part'. Must be one of $all_deps" 1>&2
            exit 1
    esac
done
