import io
import sys

filename = sys.argv[1]

f = io.open(filename)


def wo(x, y):
    if not x.startswith(y):
        raise AssertionError()
    return x.replace(y, "")


def parseFunction(line):
    return line.split(",")


def nextline():
    line = str(f.readline()).strip()
    #print(line)
    return line


def section():
    file_report = {}
    if not nextline() == "TN:":
        raise AssertionError()
    file_report["filename"] = wo(nextline(), "SF:")

    functions = {}
    line = nextline()
    while line.startswith("FN:"):
        line, fname = wo(line, "FN:").split(",")
        functions[fname] = [line]
        line = nextline()

    while line.startswith("FNDA:"):
        count, fname = wo(line, "FNDA:").split(",")
        functions[fname] += [count]
        line = nextline()

    if not int(wo(line, "FNF:")) == len(functions):
        raise AssertionError
    if not nextline().startswith("FNH:"):
        raise AssertionError

    file_report["coverage"] = {}

    line = nextline()
    while line.startswith("DA:"):
        lnr, count = wo(line, "DA:").split(",")
        file_report["coverage"][lnr] = count
        line = nextline()

    numLines = int(wo(line, "LF:"))
    hitLines = int(wo(nextline(), "LH:"))
    file_report["total"] = 100.0 * float(hitLines) / float(numLines)
    file_report["codeLines"] = len(file_report["coverage"])
    if not nextline() == "end_of_record":
        raise AssertionError

    return file_report


end = f.seek(0, 2)
f.seek(0, 0)

file_reports = []

while f.tell() < end:
    file_reports += [section()]


total = 0
numLines = 0
for rep in file_reports:
    ls = rep["codeLines"]
    numLines += ls
    total += rep["total"] * ls
total = float(total) / float(numLines)


print('{')
print('    "total": ' + str(int(total)) + ',')
print('    "fileReports": [')
fr = list(file_reports)
for r in range(len(fr)):
    rep = fr[r]
    print('      {')
    print('      "filename": "' + rep["filename"] + '",')
    print('      "total": ' + str(int(rep["total"])) + ',')
    print('      "codeLines": ' + str(int(rep["codeLines"])) + ',')
    print('      "coverage": {')
    cv = list(rep["coverage"])
    for i in range(0, len(cv)):
        line = '        "' + str(cv[i]) + '": ' + str(rep["coverage"][cv[i]])
        if i < len(cv) - 1:
            line += ','
        else:
            line += '}'
        print(line)
    line = '       }'
    if r < len(fr) - 1: line += ','
    print(line)
print('     ]')
print('}')
