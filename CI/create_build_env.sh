#!/bin/bash

deps_dir="$1"

# it is importand to build cegui AFTER irrlicht, because cegui needs to find irrlicht.
all_deps="packages sol irrlicht raknet cegui catch2"

if [ -z "$1" ]; then
    echo "Warning: first argument '$1' isn't a valid directory!" 1>&2
    echo "usage: create_build_env.sh <deps_dir>" 1>&2
    echo "Assuming old-style usage and setting <deps_dir>='../BT_deps'" 1>&2
    deps_dir="../BT_deps"
else
    shift 1
fi


SUB_PARTS="$@"
if [ -z "$SUB_PARTS" ]; then
    SUB_PARTS=$all_deps
fi

do_packages()
{
    # get relevant packages
    SUDO=""
    [ "x$(whoami)" = "xroot" ] || SUDO="sudo"
    $SUDO apt-get update && $SUDO apt-get install -y \
        make \
        cmake \
        curl \
        git \
        wget \
        unzip \
        libsilly-dev \
        libexpat1-dev \
        libfreetype6-dev \
        liblua5.3-dev \
        liblua5.3-0 \
        xserver-xorg-dev \
        x11proto-xf86vidmode-dev \
        libxxf86vm-dev \
        mesa-common-dev \
        libgl1-mesa-dev \
        libglu1-mesa-dev \
        libxext-dev \
        libxcursor-dev \
        libgl1-mesa-glx \
        lcov
    if [ "x$?" != "x0" ]; then
        ERROR_PACKAGES=1
        ERROR=1
    fi
}

do_sol()
{
    DIRECTORY=sol2
    if [ ! -d "$DIRECTORY" ]; then
        git clone https://github.com/ThePhD/sol2.git || ERROR_SOL=1
        else
        # sol2 already cloned -> just update
        (cd $DIRECTORY && git pull) || ERROR_SOL=1
    fi

    if [ "x$ERROR_SOL" = "x1" ]; then
        echo "ERROR: there was an error installing sol2!" 1>&2
        ERROR=1
    fi
}

do_irrlicht()
{
    #Irrlicht
    DIRECTORY=Irrlicht_extended
    if [ ! -d "$DIRECTORY" ]; then
        git clone https://gitlab.com/JFT/Irrlicht_extended.git || ERROR_IRRLICHT=1
    else
        # irrlicht_extended already cloned -> just update
        (cd $DIRECTORY && git pull) || ERROR_IRRLICHT=1
    fi

    (cd $DIRECTORY; sh make-release.sh) || ERROR_IRRLICHT=1

    if [ "x$ERROR_IRRLICHT" = "x1" ]; then
        echo "ERROR: there was an error installing Irrlicht_extended!" 1>&2
        ERROR=1
    fi
}

do_raknet()
{
    #RakNet
    DIRECTORY=RakNet
    if [ ! -d "$DIRECTORY" ]; then
        git clone https://github.com/TES3MP/RakNet.git || ERROR_RAKNET=1
    else
        # RakNet already cloned -> just update
        (cd $DIRECTORY && git pull) || ERROR_RAKNET=1
    fi

    (cd $DIRECTORY && cmake . -DCMAKE_BUILD_TYPE=Release -DRAKNET_GENERATE_INCLUDE_ONLY_DIR:BOOL=ON && make) || ERROR_RAKNET=1

    if [ "x$ERROR_RAKNET" = "x1" ]; then
        echo "ERROR: there was an error installing RakNet!" 1>&2
        ERROR=1
    fi
}

do_cegui()
{
    #CEGUI
    DIRECTORY=cegui-0.8.7
    if [ ! -d "$DIRECTORY" ]; then
        FILE=cegui-0.8.7.zip

        if [ -f "$FILE" ]
        then
            echo "File $FILE does exists but Folder $DIRECTORY does not exist!! - Trying Fix"
            rm $FILE
        fi
        (wget https://bitbucket.org/cegui/cegui/downloads/cegui-0.8.7.zip && unzip cegui-0.8.7.zip) || ERROR_CEGUI=1
    fi

    if [ "x$ERROR_CEGUI" = "x0" ]; then
        if [ ! -d "$DIRECTORY/build" ]; then
            mkdir "$DIRECTORY/build"
        fi
        (
            cd "$DIRECTORY/build" || exit 1
            cmake .. -DCEGUI_BUILD_RENDERER_IRRLICHT:BOOL=ON -DIRRLICHT_H_PATH=../../Irrlicht_extended/include -DIRRLICHT_LIB=../../Irrlicht_extended/lib/Linux/libIrrlicht.a -DCEGUI_SAMPLES_ENABLED:BOOL=OFF || exit 1
            make || exit 1
            cd ..
            # copy generated headers (config + version) to general library include folder
            cp -rf build/cegui/include/CEGUI/*h cegui/include/CEGUI || exit 1
        ) || ERROR_CEGUI=1
    fi

    if [ "x$ERROR_CEGUI" = "x1" ]; then
        echo "ERROR: there was an error installing CEGUI!" 1>&2
        ERROR=1
    fi
}

do_catch2()
{
    # catch2 (unittests)
    wget https://github.com/catchorg/Catch2/releases/download/v2.2.0/catch.hpp --output-document=catch2.hpp || ERROR_CATCH2=1
    if [ "x$ERROR_CATCH2" = "x1" ]; then
        echo "ERROR: there was an error downloading catch2!" 1>&2
        ERROR=1
    fi
}

#save path
pwd=`pwd`

# Lets create our deps folder
mkdir -vp "$deps_dir"
cd "$deps_dir" || exit 2

ERROR_SOL=0
ERROR_IRRLICHT=0
ERROR_RAKNET=0
ERROR_CEGUI=0
ERROR_CATCH2=0

ERROR=0


# Get all dependencies and build them
# Please make sure you have access rights to the gitlab repos! (added your ssh-key etc.)
for sub_part in $SUB_PARTS; do
    case "$sub_part" in
        packages)
            do_packages ;;
        sol)
            do_sol ;;
        irrlicht)
            do_irrlicht ;;
        raknet)
            do_raknet ;;
        cegui)
            do_cegui ;;
        catch2)
            do_catch2 ;;
        *)
            echo "Unknown argument: '$sub_part'. Must be one of $all_deps" 1>&2
            exit 1
    esac

    if [ "x$ERROR" != "x0" ]; then
        break
    fi
done

# create the userlibs.mk
echo "LUAVERSION := 5.3" > userlibs.mk

if [ "x$ERROR" = "x1" ]; then
    echo "" 1>&2
    echo "THERE WHERE ERRORS EXECUTING THIS SCRIPT" 1>&2
    echo "errors in:" 1>&2
    [ "x$ERROR_PACKAGES" = "x0" ] || echo "packages" 1>&2
    [ "x$ERROR_SOL" = "x0" ] || echo "sol" 1>&2
    [ "x$ERROR_IRRLICHT" = "x0" ] || echo "Irrlicht_extended" 1>&2
    [ "x$ERROR_RAKNET" = "x0" ] || echo "RakNet" 1>&2
    [ "x$ERROR_CEGUI" = "x0" ] || echo "CEGUI" 1>&2
    [ "x$ERROR_CATCH2" = "x0" ] || echo "catch2" 1>&2
    echo "" 1>&2
    exit 1
fi

