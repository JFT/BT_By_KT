#!/bin/bash

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++-libc++

ln -s /usr/bin/clang++-libc++ /usr/bin/g++
ln -s -f /usr/bin/clang-5.0 /usr/bin/gcc
