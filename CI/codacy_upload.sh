set -e

python $CI_PROJECT_DIR/CI/codacy_lcov_parser.py coverage_report.lcov > codacy.json
CURL_OUTPUT=$(curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" --header "project_token: $CODACY_PROJECT_TOKEN" "https://api.codacy.com/2.0/coverage/$CI_COMMIT_SHA/cpp" --data @codacy.json)
echo "$CURL_OUTPUT"
echo "$CURL_OUTPUT" | grep success
