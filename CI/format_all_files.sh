#!/bin/bash

set -e

usage()
{
    echo "usage:"
    echo "format_all_files.sh <source_root_dir>"
}

if [ -z "$1" ]; then
    usage
    exit 1
fi

if [ ! -e "$1" ]; then
    echo "provided <source_root_dir> '$1' doesn't exist"
    usage
    exit 1
fi

SOURCE_ROOT_DIR=$1

cd "$SOURCE_ROOT_DIR"

echo "formatting all files"
clang-format-6.0 --version

git ls-files '*.cpp' '*.hpp' '*.h' '*.tpp' | while read file; do
    echo "formatting '$file'..."
    clang-format-6.0 -i "$file"
done

