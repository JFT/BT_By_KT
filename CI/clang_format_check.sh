#!/bin/bash

set -e

RED='\033[0;31m'
GREEN='\033[032m'
NC='\033[0m'

usage()
{
    echo "usage:"
    echo "clang_format_check.sh <source_root_dir>"
}

if [ -z "$1" ]; then
    usage
    exit 1
fi

if [ ! -e "$1" ]; then
    echo "provided <source_root_dir> '$1' doesn't exist"
    usage
    exit 1
fi

SOURCE_ROOT_DIR=$1

cd "$SOURCE_ROOT_DIR"

echo "clang-format check"
clang-format-6.0 --version

UNFORMATTED_FILES=0

while read file; do
    # if clang-format applied formatting the output will
    # contain 'replacement' xml nodes (which end in '</replacement>)
    # otherwise only empty an 'replacements' node.
    # If we wind '</replacement>' the file therefore isn't formatted
    if clang-format-6.0 -output-replacements-xml "$file" | grep --quiet '</replacement>'; then
        echo -e "$RED'$file' isn't formatted!$NC"
        UNFORMATTED_FILES=1
    else
        echo -e "${GREEN}'$file' is formatted.$NC"
    fi
done < <(git ls-files '*.cpp' '*.hpp' '*.h' '*.tpp')

if [ "x$UNFORMATTED_FILES" = "x1" ]; then
    echo -e "${RED}unformatted files found!$NC"
    echo "Use CI/format_all_files.sh script to format them (needs clang-format in PATH)"
    exit 1
fi
