$input v_texcoord0

#include "../bgfx_shader.sh"
#include "../shaderlib.sh"

#define ColorMapSampler texture0

SAMPLER2D(ColorMapSampler, 0);

void main()
{
    vec4 finalCol = texture2D(ColorMapSampler, v_texcoord0.xy);
    gl_FragColor = finalCol;
//    gl_FragColor = vec4(1, 1, 0, 1);
}
