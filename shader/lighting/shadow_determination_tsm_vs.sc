$input a_position, a_normal
$output v_normal, v_view, v_shadowCoord, v_position, v_texcoord2
//$output v_position, v_texcoord0, v_texcoord1, v_texcoord2

#include "../bgfx_shader.sh"
#include "../shaderlib.sh"

uniform vec4 LightPos;
uniform mat4 mLightView;
uniform mat4 mLightModelViewProj;
//uniform mat4 TrapezoidTrafo;

void main()
{
    gl_Position = mul(u_modelViewProj, vec4(a_position, 1.0));
	vec4 normal = a_normal*2.0 - 1.0;
	v_normal = normalize(mul(u_modelView, vec4(normal.xyz,0.0)).xyz);
	v_view = mul(u_modelView, vec4(a_position, 1.0)).xyz;

	vec4 worldPos = mul(u_model[0], vec4(a_position,1.0));
	v_texcoord2 = mul(mLightView, worldPos);
    vec4 lightCoord = mul(mLightModelViewProj, vec4(a_position, 1.0));
   
	vec3 posOffset = a_position + normal.xyz * 0.001;
	v_shadowCoord = mul(mLightModelViewProj,vec4(posOffset, 1.0));
	
	v_position = vec4(a_position,1.0);
}
