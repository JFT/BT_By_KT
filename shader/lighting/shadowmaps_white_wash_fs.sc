$input v_depth, v_texcoord0

#include "../bgfx_shader.sh"
#include "../shaderlib.sh"

#define ColorMapSampler texture0

SAMPLER2D(ColorMapSampler, 0);

void main()
{
    float alpha = texture2D(ColorMapSampler, v_texcoord0.xy).a;

    gl_FragColor = vec4(1.0, 1.0, 1.0, alpha);
}
