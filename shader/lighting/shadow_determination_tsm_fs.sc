$input v_normal, v_view, v_shadowCoord, v_position, v_texcoord2

#include "../bgfx_shader.sh"
#include "../shaderlib.sh"
#include "poisson.sh"

uniform vec4 LightDirection;
uniform vec4 LightColour;

uniform vec4 AmbientColour;
uniform vec4 MaterialDiffuseColour;
uniform vec4 MaterialSpecularColour;
uniform vec4 FogSettings;

uniform vec4 ShadowParams; 
uniform vec4 LightRadius;
//first entry - ESM or PCSS  
//Second Entry PCSS FilterQuality

//First entry
#define TECHNIQUE_ESM 0
#define TECHNIQUE_PCSS 1
//second entry
#define POISSON_25_25   0
#define POISSON_32_64   1
#define POISSON_100_100 2
#define POISSON_64_128  3
#define REGULAR_49_225  4


#define ShadowMapSampler texture0
#define ShadowMapPCF texture1
#define farValue LightDirection.w


#define shadowTechnique ShadowParams.x
#define pcssSetting ShadowParams.y
#define lightNear ShadowParams.z
#define lightRadiusUV LightRadius.xy
#define zBias LightRadius.z

SAMPLER2D(ShadowMapSampler, 0);
SAMPLER2DSHADOW(ShadowMapPCF, 1);

vec2 cubeMapTexCoords(vec3 v)
{
    vec2 uv;
    if (abs(v.x) > abs(v.y) && abs(v.x) > abs(v.z))
        uv = vec2(v.y / abs(v.x), v.z / abs(v.x));
    if (abs(v.y) > abs(v.x) && abs(v.y) > abs(v.z))
        uv = vec2(v.x / abs(v.y), v.z / abs(v.y));
    else
        uv = vec2(v.x / abs(v.z), v.y / abs(v.z));
    return uv * 0.5 + 0.5;
}

bool isBlack(vec3 c)
{
    return (dot(c, c) == 0.0);
}

float borderDepthTexture(sampler2D tex, vec2 uv)
{
	return ((uv.x <= 1.0) && (uv.y <= 1.0) &&
	 (uv.x >= 0.0) && (uv.y >= 0.0)) ? texture2DLod(tex, uv, 0.0).r : 1.0;
}

float borderPCFTexture(sampler2DShadow tex, vec3 uvz)
{
#if BGFX_SHADER_LANGUAGE_HLSL
	float res = shadow2D(tex, uvz);
#else
    float res = texture2D(tex, uvz);
#endif //BGFX_SHADER_LANGUAGE_HLSL
	return ((uvz.x <= 1.0) && (uvz.y <= 1.0) &&
	 (uvz.x >= 0.0) && (uvz.y >= 0.0)) ? res : 
	 ((uvz.z <= 1.0) ? 1.0 : 0.0);
}

vec2 searchRegionRadiusUV(float zWorld)
{
    return lightRadiusUV * (zWorld - lightNear) / zWorld;
}

// Project UV size to the near plane of the light
vec2 projectToLightUV(vec2 sizeUV, float zWorld)
{
    return sizeUV * lightNear / zWorld;
}

// Using similar triangles between the area light, the blocking plane and the surface point
vec2 penumbraRadiusUV(float zReceiver, float zBlocker)
{
    return lightRadiusUV * (zReceiver - zBlocker) / zBlocker;
}

float biasedZ(float z0, vec2 dz_duv, vec2 offset)
{
    return -zBias + z0 + dot(dz_duv, offset);
}

float zClipToEye(float z)
{
    return farValue * lightNear / (farValue - z * (farValue - lightNear));   
}

// Derivatives of light-space depth with respect to texture2D coordinates
vec2 depthGradient(vec2 uv, float z)
{
    vec2 dz_duv = vec2(0.0, 0.0);

    vec3 duvdist_dx = dFdx(vec3(uv,z));
    vec3 duvdist_dy = dFdy(vec3(uv,z));

    dz_duv.x = duvdist_dy.y * duvdist_dx.z;
    dz_duv.x -= duvdist_dx.y * duvdist_dy.z;

    dz_duv.y = duvdist_dx.x * duvdist_dy.z;
    dz_duv.y -= duvdist_dy.x * duvdist_dx.z;

    float det = (duvdist_dx.x * duvdist_dy.y) - (duvdist_dx.y * duvdist_dy.x);
    dz_duv /= det;

    return dz_duv;
}


// returns x: accumBlockerDepth, y: numBlockers, z: maxBlockers
vec3 findBlocker(
    vec2 uv,
    float z0,
    vec2 dz_duv,
    vec2 sRegionRadiusUV)
{
	
	float accumBlockerDepth = 0.0;
    float numBlockers = 0.0;
	float maxBlockers = 300.0;
    
    int samplingSetting = int(floor(pcssSetting+0.5f));
    if(samplingSetting == POISSON_25_25)
    {
        maxBlockers = 25.0;
        for (int i = 0; i < 25; ++i)
        {
            vec2 offset = Poisson25[i] * sRegionRadiusUV;
            float shadowMapDepth = borderDepthTexture(ShadowMapSampler, uv + offset);
            float z = biasedZ(z0, dz_duv, offset);
            if (shadowMapDepth < z)
            {
                accumBlockerDepth += shadowMapDepth;
                numBlockers++;
            }
        }
		return vec3(accumBlockerDepth,numBlockers,maxBlockers);
	}


    else if(samplingSetting == POISSON_32_64)
    {
        maxBlockers = 32.0;
        for (int i = 0; i < 32; ++i)
        {
            vec2 offset = Poisson32[i] * sRegionRadiusUV;
            float shadowMapDepth = borderDepthTexture(ShadowMapSampler, uv + offset);
            float z = biasedZ(z0, dz_duv, offset);
            if (shadowMapDepth < z)
            {
                accumBlockerDepth += shadowMapDepth;
                numBlockers++;
            }
        }
		return vec3(accumBlockerDepth,numBlockers,maxBlockers);
    }

    else if(samplingSetting == POISSON_100_100)
    {
        maxBlockers = 100.0;
        for (int i = 0; i < 100; ++i)
        {
            vec2 offset = Poisson100[i] * sRegionRadiusUV;
            float shadowMapDepth = borderDepthTexture(ShadowMapSampler, uv + offset);
            float z = biasedZ(z0, dz_duv, offset);
            if (shadowMapDepth < z)
            {
                accumBlockerDepth += shadowMapDepth;
                numBlockers++;
            }
        }

		return vec3(accumBlockerDepth,numBlockers,maxBlockers);
    }

    else if(samplingSetting == POISSON_64_128)
    {
        maxBlockers = 64.0;
        for (int i = 0; i < 64; ++i)
        {
            vec2 offset = Poisson64[i] * sRegionRadiusUV;
            float shadowMapDepth = borderDepthTexture(ShadowMapSampler, uv + offset);
            float z = biasedZ(z0, dz_duv, offset);
            if (shadowMapDepth < z)
            {
                accumBlockerDepth += shadowMapDepth;
                numBlockers++;
            }
        }
		return vec3(accumBlockerDepth,numBlockers,maxBlockers);
    }

    else if(samplingSetting ==  REGULAR_49_225)
    {
        maxBlockers = 49.0;
        vec2 stepUV = sRegionRadiusUV / 3.0;
        for (int x = -3; x <= 3; ++x)
        {
            for (int y = -3; y <= 3; ++y)
            {
                vec2 offset = vec2(x, y) * stepUV;
                float shadowMapDepth = borderDepthTexture(ShadowMapSampler, uv + offset);
                float z = biasedZ(z0, dz_duv, offset);
                if (shadowMapDepth < z)
                {
                    accumBlockerDepth += shadowMapDepth;
                    numBlockers++;
                }
            }
        }
		return vec3(accumBlockerDepth,numBlockers,maxBlockers);
    }
	
	return vec3(accumBlockerDepth,numBlockers,maxBlockers);
	
}

// Performs PCF filtering on the shadow map using multiple taps in the filter region.
float pcfFilter(vec2 uv, float z0, vec2 dz_duv, vec2 filterRadiusUV)
{
    float sum = 0.0;

    int samplingSetting = int(floor(pcssSetting+0.5f));
    
	if(samplingSetting == POISSON_25_25)
	{
		for (int i = 0; i < 25; ++i)
		{
			vec2 offset = Poisson25[i] * filterRadiusUV;
			float z = biasedZ(z0, dz_duv, offset);
			sum += borderPCFTexture(ShadowMapPCF, vec3(uv + offset, z));
		}
		return sum / 25.0;
	}
    else if(samplingSetting == POISSON_32_64)
    {
        for (int i = 0; i < 64; ++i)
        {
            vec2 offset = Poisson64[i] * filterRadiusUV;
            float z = biasedZ(z0, dz_duv, offset);
            sum += borderPCFTexture(ShadowMapPCF, vec3(uv + offset, z));
        }
        return sum / 64.0;
    }
    else if(samplingSetting == POISSON_100_100)
    {
        for (int i = 0; i < 100; ++i)
        {
            vec2 offset = Poisson100[i] * filterRadiusUV;
            float z = biasedZ(z0, dz_duv, offset);
            sum += borderPCFTexture(ShadowMapPCF, vec3(uv + offset, z));
        }
        return sum / 100.0;
    }
    else if(samplingSetting == POISSON_64_128)
    {
        for (int i = 0; i < 128; ++i)
        {
            vec2 offset = Poisson128[i] * filterRadiusUV;
            float z = biasedZ(z0, dz_duv, offset);
            sum += borderPCFTexture(ShadowMapPCF, vec3(uv + offset, z));
        }
        return sum / 128.0;
    }
    else if(samplingSetting == REGULAR_49_225)
    {
        vec2 stepUV = filterRadiusUV / 7.0;
        for (int x = -7; x <= 7; ++x)
        {
            for (int y = -7; y <= 7; ++y)
            {
                vec2 offset = vec2(x, y) * stepUV;
                float z = biasedZ(z0, dz_duv, offset);
                sum += borderPCFTexture(ShadowMapPCF, vec3(uv + offset, z));
            }                
        }
        float numSamples = 7.0 * 2.0 + 1.0;
        return sum / (numSamples * numSamples);
    }
	else
	{
	 return 1.0;
	}
}


float pcssShadow(vec2 uv, float z, vec2 dz_duv, float zEye)
{
    // ------------------------
    // STEP 1: blocker search
    // ------------------------
    
    vec2 sRegionRadiusUV = searchRegionRadiusUV(zEye);
    vec3 blockerResult = findBlocker(uv, z, dz_duv, sRegionRadiusUV);
	float accumBlockerDepth = blockerResult.x;
	float numBlockers = blockerResult.y;
	float maxBlockers = blockerResult.z;


    // Early out if not in the penumbra
    if (numBlockers == 0.0)
        return 1.0;

    // ------------------------
    // STEP 2: penumbra size
    // ------------------------
    float avgBlockerDepth = accumBlockerDepth / numBlockers;
    float avgBlockerDepthWorld = zClipToEye(avgBlockerDepth);
    vec2 penumbraRadius = penumbraRadiusUV(zEye, avgBlockerDepthWorld);
    vec2 filterRadius = projectToLightUV(penumbraRadius, zEye);

    // ------------------------
    // STEP 3: filtering
    // ------------------------
    return pcfFilter(uv, z, dz_duv, filterRadius);
}


float ESM(sampler2D _sampler, vec4 _shadowCoord, float _bias, float _depthMultiplier)
{
	vec2 texCoord = _shadowCoord.xy/_shadowCoord.w; // /2.0 + vec2(0.5,0.5);

	bool outside = any(greaterThan(texCoord, vec2_splat(1.0)))
				|| any(lessThan   (texCoord, vec2_splat(0.0)));

	if (outside)
	{
		return 1.0;
	}

	float receiver = (_shadowCoord.z -_bias)/_shadowCoord.w;
	float occluder = texture2DLod(_sampler, texCoord,0.0).r;

	float visibility = clamp(exp(_depthMultiplier * (occluder-receiver) ), 0.0, 1.0);
	return visibility;
}

vec2 lit(vec3 _ld, vec3 _n, vec3 _vd, float _exp)
{
	//diff
	float ndotl = dot(_n, _ld);

	//spec
	vec3 r = 2.0*ndotl*_n - _ld; // reflect(_ld, _n);
	float rdotv = dot(r, _vd);
	float spec = step(0.0, ndotl) * pow(max(0.0, rdotv), _exp) * (2.0 + _exp)/8.0;

	return max(vec2(ndotl, spec), 0.0);
}


void main()
{

	float bias = 0.003;
	float depthMultiplier = 600.0;
	float hardness = 0.3;

	float lightFactor = 1.0f;
	int stec = int(floor((shadowTechnique+0.5f)));
	if( stec == TECHNIQUE_ESM)
	{
		lightFactor = ESM(ShadowMapSampler,v_shadowCoord,bias,depthMultiplier*hardness);
	}
	else if(stec == TECHNIQUE_PCSS)
	{
		vec2 uv = v_shadowCoord.xy/v_shadowCoord.w;
		float z = (v_shadowCoord.z)/v_shadowCoord.w;
		vec2 dz_duv = depthGradient(uv,z);
		float zEye = -v_texcoord2.z;
		lightFactor = pcssShadow(uv, z, dz_duv, zEye);
	}

	vec3 lightD = -normalize(LightDirection.xyz);

	vec2 lc = lit(lightD, v_normal, -normalize(v_view), 0.0);

	//Fog.
	vec3 fogColor = FogSettings.xyz; 
	float fogDensity = FogSettings.w;//0.00035;
	float LOG2 = 1.442695;
	float z = length(v_view);
	float fogFactor = clamp(1.0/exp2(fogDensity*fogDensity*z*z*LOG2), 0.2, 1.0);

	vec3 ambi = AmbientColour.xyz*AmbientColour.w*LightColour.xyz;
	vec3 brdf    = (MaterialDiffuseColour.xyz*MaterialDiffuseColour.w*lc.x + 
					MaterialSpecularColour.xyz*MaterialSpecularColour.w*lc.y) * LightColour.xyz * lightFactor;

	vec3 final = toGamma(ambi+brdf);
	

    gl_FragColor.xyz = mix(fogColor,final,fogFactor);
	gl_FragColor.w = 1.0;
}
