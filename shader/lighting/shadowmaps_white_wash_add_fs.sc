$input v_depth, v_texcoord0

#include "../bgfx_shader.sh"
#include "../shaderlib.sh"

#define ColorMapSampler texture0

SAMPLER2D(ColorMapSampler, 0);

float luminance(vec3 color)
{
	return clamp(color.r * 0.3 + color.g * 0.59 + color.b * 0.11, 0.0, 1.0);
}
void main()
{
	vec4 diffuseTex = texture2D(ColorMapSampler, v_texcoord0.xy);

	gl_FragColor = vec4(1.0, 1.0, 1.0, luminance(diffuseTex.rgb));
}
