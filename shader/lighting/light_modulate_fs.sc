$input v_texcoord0

#include "../bgfx_shader.sh"
#include "../shaderlib.sh"

#define ColorMapSampler texture0
#define ScreenMapSampler texture1
#define DepthMapSampler texture2

SAMPLER2D(ColorMapSampler,  0);
SAMPLER2D(ScreenMapSampler, 1);
SAMPLER2D(DepthMapSampler, 2);

void main()
{
    vec4 finalCol = texture2D(ColorMapSampler, v_texcoord0.xy);
    vec4 lightCol = texture2D(ScreenMapSampler, v_texcoord0.xy);
    vec4 depthVal = texture2D(DepthMapSampler, v_texcoord0.xy);
    gl_FragColor = finalCol * lightCol;
    gl_FragDepth = depthVal.r;
}

