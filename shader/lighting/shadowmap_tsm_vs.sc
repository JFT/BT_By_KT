$input a_position, a_texcoord0
$output v_position


#include "../bgfx_shader.sh"
#include "../shaderlib.sh"

void main()
{
    vec4 lightCoord = mul(u_modelViewProj, vec4(a_position, 1.0) );
	gl_Position = lightCoord;
	v_position = lightCoord;

}
