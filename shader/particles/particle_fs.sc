$input v_color0, v_texcoord0, v_particleCoord

#include "common.sh"

#include "uniforms.sh"

SAMPLER2D(texture0, 0);
SAMPLER2D(texture1, 1);

//layout(location=0) out vec4 fragColor;

float luminance(vec3 color)
{
	return clamp(color.r * 0.3 + color.g * 0.59 + color.b * 0.11, 0.0, 1.0);
}

void main()
{
	float timePassed = texelFetch(texture0,ivec2(v_particleCoord),0).w;

	vec4 texel = texture2D(texture1, v_texcoord0);
	float lifeFadeOut = (lifeTime-timePassed)/(lifeTime);
	
	texel.a = lifeFadeOut* texel.a;

	vec4 color = texel;//texel;//vec4(texel.rgb,luminance(texel.rgb));
	color.rgb = color.rgb * color.a; // premultiplied alpha
	if(timePassed < 0.f)
	{
		color.a = 0.f;
	}

	gl_FragColor = color;
}
