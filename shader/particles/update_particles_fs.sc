$input v_texcoord0

#include "common.sh"
#include "uniforms.sh"

SAMPLER2DARRAY(texture0, 0);
SAMPLER2D(texture1, 1); //Position
SAMPLER2D(texture2, 2); //Velocity


highp float random(vec2 co, float seed)
{
    highp float a = 12.9898;
    highp float b = 78.233;
    highp float c = 43758.5453;
    highp float dt= dot(seed*co.xy ,vec2(a,b));
    highp float sn= mod(dt,3.14);
    return fract(sin(sn) * c);
}

void main()
{

	float randNumber = random(v_texcoord0,globalTime);
	
	vec4 oldPos = texelFetch(texture1,ivec2(v_texcoord0.x*textureWidth,v_texcoord0.y*textureHeight),0);
	vec4 oldVelocity = texelFetch(texture2,ivec2(v_texcoord0.x*textureWidth,v_texcoord0.y*textureHeight),0);

	//Local - Vectorfield:
	vec3 localPos = (oldPos.xyz - vFieldPosition);
	vec4 vecField = texture2DArray(texture0,vec3(2.f*localPos.x/(vFieldExtension.x*vFieldResolution.x),
									2.f*localPos.y/(vFieldExtension.y*vFieldResolution.y),2.f*localPos.z/(vFieldExtension.z*vFieldResolution.z)));

	vec3 acceleration = timeStep*vecField.xyz;
	vec3 speed = oldVelocity.xyz + acceleration;
	vec3 pos = oldPos.xyz;
	float timePassed = oldPos.w;

	vec4 finalColPos = oldPos;
	vec4 finalColVel = oldVelocity;
	
	if(timePassed >= 0.f)
	{
		timePassed = timePassed+timeStep;
		
		if(timePassed >lifeTime)
		{
			pos = vec3(-10000000.f,-1000000.f,-10000000.f);
			speed = initSpeed;
			timePassed = -1.f;
			finalColPos = vec4(pos,timePassed);
			finalColVel = vec4(speed,-1.f);
		}
		else
		{
			speed = speed*(1.0-timeStep*friction);
			finalColPos = vec4(pos+speed*timeStep,timePassed);
			finalColVel = vec4(speed,-1.f);
		}	
	}
	else
	{
		if(randNumber >= (1.f - spawnProbability))
		{
			//spawn with certain probability
			timePassed = 0.f;
			
			float xSpawn = xSpawnArea.x + random(v_texcoord0,globalTime+1.1f)*(xSpawnArea.y - xSpawnArea.x);
			float ySpawn = ySpawnArea.x + random(v_texcoord0,globalTime+2.1f)*(ySpawnArea.y - ySpawnArea.x);
			float zSpawn = zSpawnArea.x + random(v_texcoord0,globalTime+3.1f)*(zSpawnArea.y - zSpawnArea.x);
			vec3 spawnPos = vec3(xSpawn,ySpawn,zSpawn);

			finalColPos = vec4(spawnPos,timePassed);
			finalColVel = vec4(initSpeed,-1.f);
		}
	}

	
    gl_FragData[posBufferIdx] = finalColPos;
	gl_FragData[velocityBufferIdx] = finalColVel;
}
