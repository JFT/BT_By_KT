$input a_position, a_color0, a_texcoord0, i_data0
$output v_color0, v_texcoord0, v_particleCoord

#include "uniforms.sh"
#include "common.sh"
//texture that contains particleinformation
SAMPLER2D(texture0, 0);

void main()
{
	int instanceId = gl_InstanceID;
	int particleX = int(mod(instanceId,textureWidth));
	int particleY = int(instanceId/textureWidth);
	v_particleCoord = vec2(particleX,particleY);
	vec4 pPos = texelFetch(texture0,ivec2(particleX,particleY),0);
	float timePassed = pPos.w;
	pPos.w = 0;
	vec2 particleInfo = i_data0;
	v_color0 = vec4(0.8,0.3,0.1,1.0);
	
	vec3  eye = mul(u_view, vec4(pPos.xyz, 1.0) ).xyz;
	vec3  up = normalize(cross(eye, vec3(1.0, 0.0, 0.0) ) );
	vec3  right = normalize(cross(up, eye));
	//spritesize at the moment fixed for testing
	float spriteSize = 2.f;
	vec3  position = eye + spriteSize * right * a_position.x + spriteSize * up * a_position.y;
	
	//vec4 particlePosEye =  mul(u_modelView,vec4(pPos.xyz,1.0));
	
	//vec4 vertexPosEye = particlePosEye + vec4((a_position.xy*2.0-1.0)*spriteSize,0,0);
	
	v_texcoord0 = a_texcoord0;
	gl_Position = mul(u_proj,vec4(position,1.0));

}
