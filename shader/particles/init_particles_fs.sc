$input v_texcoord0

#include "common.sh"
#include "uniforms.sh"

//SAMPLER2D(texture0, 0);

void main()
{
	vec3 pos = initPos;
	vec3 speed = initSpeed;
    
	vec4 finalColPos = vec4(pos,-1.f);
    gl_FragData[posBufferIdx] = finalColPos;
	gl_FragData[velocityBufferIdx] = vec4(speed, -1.f);
}
