
uniform vec4 parameters[3];
uniform vec4 dimension[3];
uniform vec4 spawnArea[2];

#define threadGroupUpdateSize 128

#define globalTime          parameters[0].x
#define lifeTime            parameters[0].y
#define spawnProbability    parameters[0].z
#define damping             parameters[0].w

#define initPos				parameters[1].xyz
#define timeStep			parameters[1].w

#define initSpeed			parameters[2].xyz
#define friction			parameters[2].w

#define textureWidth		dimension[0].w
#define textureHeight		dimension[1].w
#define vFieldExtension		dimension[0].xyz
#define vFieldPosition		dimension[1].xyz
#define vFieldResolution	dimension[2].xyz

#define xSpawnArea		spawnArea[0].xy
#define ySpawnArea		spawnArea[0].zw
#define zSpawnArea		spawnArea[1].xy