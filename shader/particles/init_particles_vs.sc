$input a_position, a_texcoord0
$output v_texcoord0

#include "../bgfx_shader.sh"
#include "../shaderlib.sh"
#include "uniforms.sh"

void main()
{
    // even though the screenQuad coordinates are very simple they still need to be multiplied by the modelViewProjection matrix because
    // opengl renders flipped coordinates to framebuffers and the bfgx driver inverts that by adjusting the u_modelViewProj matrix
    gl_Position = mul(u_modelViewProj, vec4(a_position, 1.0));
    v_texcoord0 = a_texcoord0;
}
