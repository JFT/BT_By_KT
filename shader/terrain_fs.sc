$input v_color0, v_texcoord0, v_texcoord1, v_position, v_normal

#include "common.sh"

SAMPLER2DARRAY(texture0, 0);
SAMPLER2D(texture1, 1);

vec3 getTriPlanarBlend(vec3 _wNorm){
    // triplanar projection
    _wNorm = normalize(_wNorm);
    float exponent = 4.0f;
    //TODO: find out if this access using pow and abs is faster or if 4 multiplications are all that should be done
    float mXY = pow(abs(_wNorm.z), exponent);
    float mXZ = pow(abs(_wNorm.y), exponent);
    float mYZ = pow(abs(_wNorm.x), exponent);
    float total = 1.0f / (mXY + mXZ + mYZ);
    mXY *= total;
    mXZ *= total;
    mYZ *= total;

    return vec3(mXY, mXZ, mYZ);
}

void main()
{
    //TODO: get splatmap size (=2048.0f in the ivec2) as variable compiler argument
    vec4 splatColor = texelFetch(texture1, ivec2(2048.0f * v_texcoord0.xy), 0);
    float texNumber1 = (int(splatColor.r * 255) & 15) + 1;
    float texWeight1 = int(splatColor.r * 255) >> 4;
    //texWeight1 = texWeight1 * texWeight1 / 256;
    texWeight1 = texWeight1 / 15;
    float texNumber2 = (int(splatColor.g * 255) & 15) + 1;
    float texWeight2 = int(splatColor.g * 255) >> 4;
    //texWeight2 = texWeight2 * texWeight2 / 256;
    texWeight2 = texWeight2 / 15;
    float texNumber3 = (int(splatColor.b * 255) & 15) + 1;
    float texWeight3 = int(splatColor.b * 255) >> 4;
    //texWeight3 = texWeight3 * texWeight3 / 256;
    texWeight3 = texWeight3 / 15;
    float texNumber4 = (int(splatColor.a * 255) & 15) + 1;
    float texWeight4 = int(splatColor.a * 255) >> 4;
    //texWeight4 = texWeight4 * texWeight4 / 256;
    texWeight4 = texWeight4 / 15;
    float texWeight0 = 1.0f - texWeight1 - texWeight2 - texWeight3 - texWeight4;

    //TODO: insert tileSize via shaderCreator as compaile argument
    float tileSize = 128.0f;
    float tilingFactor = 0.2f; // determines scaling between tile and texture. > a tile will fit tilingFactor textures onto itself.

    vec3 blending = getTriPlanarBlend(v_normal);

    // default to texture 0 in the array
    vec4 xPart0 = texture2DArray(texture0, vec3(tilingFactor * v_position.xy / tileSize, 0)) * blending.x * texWeight0;
    vec4 yPart0 = texture2DArray(texture0, vec3(tilingFactor * v_position.xz / tileSize, 0)) * blending.y * texWeight0;
    vec4 zPart0 = texture2DArray(texture0, vec3(tilingFactor * v_position.yz / tileSize, 0)) * blending.z * texWeight0;
    //
    vec4 xPart1 = texture2DArray(texture0, vec3(tilingFactor * v_position.xy / tileSize, texNumber1)) * blending.x * texWeight1;
    vec4 yPart1 = texture2DArray(texture0, vec3(tilingFactor * v_position.xz / tileSize, texNumber1)) * blending.y * texWeight1;
    vec4 zPart1 = texture2DArray(texture0, vec3(tilingFactor * v_position.yz / tileSize, texNumber1)) * blending.z * texWeight1;
    vec4 xPart2 = texture2DArray(texture0, vec3(tilingFactor * v_position.xy / tileSize, texNumber2)) * blending.x * texWeight2;
    vec4 yPart2 = texture2DArray(texture0, vec3(tilingFactor * v_position.xz / tileSize, texNumber2)) * blending.y * texWeight2;
    vec4 zPart2 = texture2DArray(texture0, vec3(tilingFactor * v_position.yz / tileSize, texNumber2)) * blending.z * texWeight2;
    vec4 xPart3 = texture2DArray(texture0, vec3(tilingFactor * v_position.xy / tileSize, texNumber3)) * blending.x * texWeight3;
    vec4 yPart3 = texture2DArray(texture0, vec3(tilingFactor * v_position.xz / tileSize, texNumber3)) * blending.y * texWeight3;
    vec4 zPart3 = texture2DArray(texture0, vec3(tilingFactor * v_position.yz / tileSize, texNumber3)) * blending.z * texWeight3;
    vec4 xPart4 = texture2DArray(texture0, vec3(tilingFactor * v_position.xy / tileSize, texNumber4)) * blending.x * texWeight4;
    vec4 yPart4 = texture2DArray(texture0, vec3(tilingFactor * v_position.xz / tileSize, texNumber4)) * blending.y * texWeight4;
    vec4 zPart4 = texture2DArray(texture0, vec3(tilingFactor * v_position.yz / tileSize, texNumber4)) * blending.z * texWeight4;
    gl_FragColor = xPart0 + xPart2 + xPart3 + xPart4 + yPart0 + yPart1 + yPart2 + yPart3 + yPart4 + zPart0 + zPart1 + zPart2 + zPart3 + zPart4;

    // uncomment to show texture normals via color
    //gl_FragColor = vec4(v_normal, 1.0);
}
