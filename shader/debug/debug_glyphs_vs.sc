$input a_position, a_color0
$output v_color0, v_texcoord0

#include "../bgfx_shader.sh"
#include "../shaderlib.sh"

void main()
{
    float x = ((2.0 * (a_position.x - 0.5)) / u_viewRect.z) - 1.0;
    float y = 1.0 - ((2.0 * (a_position.y - 0.5)) / u_viewRect.w);
    //hack to force the vertexbuffer from bgfx to work
    gl_Position = vec4(x, y, 0.0, 1.0);
    v_texcoord0 = vec2(a_position.z, a_color0.x);
    v_color0 = vec4(a_color0.yzw, 1.0);
}