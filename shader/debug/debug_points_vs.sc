$input a_position, a_color0
$output v_color0

#include "../bgfx_shader.sh"
#include "../shaderlib.sh"

void main()
{

#if !defined(BGFX_SHADER_LANGUAGE_HLSL)
	gl_PointSize = a_color0.w;
#endif

    gl_Position = mul(u_modelViewProj, vec4(a_position, 1.0));
    v_color0 = vec4(a_color0.xyz, 1.0);
}
