$input v_texcoord0, v_color0

#include "../bgfx_shader.sh"
#include "../shaderlib.sh"

#define ColorMapSampler texture0

SAMPLER2D(ColorMapSampler, 0);

void main()
{
    vec4 texel = texture2D(ColorMapSampler, v_texcoord0.xy);
	vec4 finalColor = v_color0;
	finalColor.a = texel.r;
    gl_FragColor = finalColor;
}
