#!/bin/sh

# uncomment to enable debug mode
#set -x

WINPIPE="$1"
ADDPROFILE="$2"
if [ "x$ADDPROFILE" = "xadd-profile" ]; then
    DEPENDFILE="$3"

    [ -n "$DEPENDFILE" ] || exit 1

    # replace newline at end of file with "\" (+ newline)
    sed -i '$ s/$/ \\/' "$DEPENDFILE"

    # add .gcda dependency at end of file
    echo " ""$DEPENDFILE" | sed -e 's@\.depend@.gcda@' >> "$DEPENDFILE"
else

DIR_OF_SRCFILE="$3"
SRCPATH="$4"
shift 4

case "$DIR_OF_SRCFILE" in
    "$SRCPATH")
        if [ "x$WINPIPE" = "xnopipe" ]; then
            g++ -MM "$@" |
            # @ is used as a seperator for the sed patters
            # s -> sed replace s/REGEXP/REPLACEMENT/FLAGS with any seperator / (we use @)
            # \(.*)\ matches any string and makes it referencable in the REPLACEMENT
            # \1 in the REPLACEMENT is this matched string
            sed -e 's@^\(.*\)\.o:@\1.depend \1.o:@'
            # dirty hack to be able to get the exit code on windows
            g++ -MM "$@" > /dev/null || exit 1
        else
            # because we might execute make in parallel we need a unique named pipe
            PIPENAME=$(mktemp --dry-run --tmpdir="$DIR_OF_SRCFILE") || exit 1
            mkfifo "$PIPENAME" || exit 1
            # @ is used as a seperator for the sed patters
            # s -> sed replace s/REGEXP/REPLACEMENT/FLAGS with any seperator / (we use @)
            # \(.*)\ matches any string and makes it referencable in the REPLACEMENT
            # \1 in the REPLACEMENT is this matched string
            sed -e 's@^\(.*\)\.o:@\1.depend \1.o:@' < "$PIPENAME" &
            g++ -MM "$@" > "$PIPENAME" || exit 1
            # return the status of the g++ call
            [ -n "$PIPENAME" ] && rm -f "$PIPENAME"
            exit 0
        fi
        ;;
    *)
        # cut out top source dir
        DIR_OF_SRCFILE=$(echo "$DIR_OF_SRCFILE" | sed -e 's@'"^$SRCPATH/"'@@')
        if [ "x$WINPIPE" = "xnopipe" ]; then
            g++ -MM "$@" |
            sed -e 's@^\(.*\)\.o:@'"$DIR_OF_SRCFILE/"'\1.depend '"$DIR_OF_SRCFILE/"'\1.o:@'
            # dirty hack to be able to get the exit code on windows
            g++ -MM "$@" > /dev/null || exit 1
        else
            PIPENAME=$(mktemp --dry-run --tmpdir="$DIR_OF_SRCFILE") || exit 1
            mkfifo "$PIPENAME" || exit 1
            sed -e 's@^\(.*\)\.o:@'"$DIR_OF_SRCFILE/"'\1.depend '"$DIR_OF_SRCFILE/"'\1.o:@' < "$PIPENAME" &
            g++ -MM "$@" > "$PIPENAME" || exit 1
            [ -n "$PIPENAME" ] && rm -f "$PIPENAME"
            exit 0
        fi
        ;;
esac

fi # if [ "x$ADDPROFILE" = "xadd-profile" ]; then
