#include "../src/ecs/events/eventSerialization.h"
#include <catch2/catch.hpp>

using S = Events::Serialization;

TEST_CASE("Serialized size of uint32_t is 4 bytes", "[EventSerialization]")
{
    const uint32_t v = 12135;
    CHECK(S::SerializedEventSize(v) == 4);
}

TEST_CASE("Serialization and deserialization of int gets the serialized value", "[EventSerialization]")
{
    const int v = 12135;
    std::vector<S::SerializedEventBuffer_t> buff(S::SerializedEventSize(v));
    S::serialize(v, buff.data());
    CHECK(v == S::deserialize<decltype(v)>(buff.data()));
}

TEST_CASE("Serializing event size can be deserialized back", "[EventSerialization]")
{
    const int v = 654354;
    const auto size = S::SerializedEventSize(v);
    std::vector<S::SerializedEventBuffer_t> buff(sizeof(size));
    S::serialize(size, buff.data());
    CHECK(size == S::deserializeEventSize<decltype(v)>(buff.data()));
}

TEST_CASE("NeededLocalBufferSize() for an int is 1 byte (=type) + 4 byte (=value)", "[EventSerialization]")
{
    const int v = 984354;
    const auto size = S::NeededLocalBufferSize(v);
    CHECK(size == 1 + sizeof(v));
}

TEST_CASE("Events and type can be serialized + deserialized together by special functions", "[EventSerialization]")
{
    const int v = 3254832;
    const auto size = S::NeededLocalBufferSize(v);
    std::vector<S::SerializedEventBuffer_t> buff(size);
    S::serializeEventAndType(10, v, buff.data());
    CHECK(S::deserializeType(buff.data()) == 10);
    CHECK(S::deserialize<decltype(v)>(buff.data() + sizeof(Events::EventID_t)) == v);
}
