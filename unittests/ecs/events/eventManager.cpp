#include "../src/ecs/events/eventManager.h"
#include <catch2/catch.hpp>
#include "../src/ecs/events/events.h"

template <typename T>
struct WasCalled
{
    void call(const T& v)
    {
        called = true;
        value = v;
    }

    bool called = false;
    T value;
};

TEST_CASE("emitting events with no subscribers doesn't throw", "[EventManager]")
{
    EventManager e;
    e.emit<int>(5);
}

TEST_CASE("Transmits simple event", "[EventManager]")
{
    EventManager e;
    WasCalled<int> wc;
    const auto subscription = e.subscribe<int>([&](const int& e) { wc.call(e); });
    e.emit<int>(5);
    CHECK(wc.called);
    CHECK(wc.value == 5);
}

struct NeedsAckDummy
{
    int v;
};

namespace Events
{
    namespace Networking
    {
        template <>
        struct NetworkEventType<NeedsAckDummy>
        {
            static constexpr size_t type = 0;
            static constexpr bool needsServerACK = true;
        };

        // provide storage location for static variable
        constexpr bool NetworkEventType<NeedsAckDummy>::needsServerACK;
    } // namespace Networking
} // namespace Events


TEST_CASE("By default not receiving unacked events", "[EventManager]")
{
    EventManager e;
    WasCalled<NeedsAckDummy> wc;
    const auto subscription = e.subscribe<NeedsAckDummy>([&](auto& e) { wc.call(e); });
    e.emit<NeedsAckDummy>({10});
    CHECK(!wc.called);

    SECTION("but receiving acked events")
    {
        e.emit<NeedsAckDummy>({15}, true);
        CHECK(wc.called);
        CHECK(wc.value.v == 15);
    }
}

TEST_CASE("receiving unacked events if requested", "[EventManager]")
{
    EventManager e;
    WasCalled<NeedsAckDummy> wc;
    const auto subscription = e.subscribe<NeedsAckDummy>([&](auto& e) { wc.call(e); }, true);
    e.emit<NeedsAckDummy>({10});
    CHECK(wc.called);
    CHECK(wc.value.v == 10);
}

struct DummyType1
{
    int v;
};
struct DummyType2
{
    int v;
};

TEST_CASE("Two different event types", "[EventManager]")
{
    EventManager e;
    WasCalled<DummyType1> a;
    WasCalled<DummyType2> b;
    const auto subscription = e.subscribe<DummyType1>([&](auto& e) { a.call(e); });
    const auto subscription2 = e.subscribe<DummyType2>([&](auto& e) { b.call(e); });
    e.emit<DummyType2>({7});
    CHECK(b.called);
    CHECK(b.value.v == 7);
    CHECK(!a.called);
    e.emit<DummyType1>({5});
    CHECK(a.called);
    CHECK(a.value.v == 5);
}
