#include "../src/ecs/events/eventMailbox.h"
#include <catch2/catch.hpp>
#include "printPairs.h"
#include "../src/utils/threadPool.hpp"

#include <algorithm>

TEST_CASE("EventMailbox", "[EventMailbox]")
{
    ThreadPool p(2);
    EventMailbox box(p);

    SECTION("getNextEventType is InvalidEventType if no events received")
    {
        auto ap = box.getAccessPoint();
        CHECK(box.getNextEventType(ap) == Events::InvalidEventType);
    }

    SECTION("receiving 1 event from main thread")
    {
        int a = 5;

        box.receiveThreadedEvent(0, a);

        SECTION("received event type is 0")
        {
            auto ap = box.getAccessPoint();
            REQUIRE(box.getNextEventType(ap) == 0);

            SECTION("received event value is 5")
            {
                const auto evt = box.readNextEvent<int>(ap);
                CHECK(evt == 5);
                SECTION("no more events left")
                {
                    CHECK(box.getNextEventType(ap) == Events::InvalidEventType);
                }
            }
        }
    }

    SECTION("Reveiving events from other threads")
    {
        int a = 1;
        int b = 2;
        p.enqueue([&]() { box.receiveThreadedEvent(4, a); });
        p.enqueue([&]() { box.receiveThreadedEvent(5, b); });
        // hack to make sure the messages get through
        std::this_thread::sleep_for(std::chrono::seconds(1));
        auto ap = box.getAccessPoint();
        // the ordering of messages isn't neccecarily fixed
        std::vector<std::pair<int, int>> evts;
        for (int i = 0; i < 2; ++i)
        {
            const auto t = box.getNextEventType(ap);
            const auto v = box.readNextEvent<int>(ap);
            evts.emplace_back(std::make_pair(t, v));
        }
        std::sort(evts.begin(), evts.end(), [](const auto& p1, const auto& p2) {
            return p1.first < p2.first;
        });
        REQUIRE(evts[0] == std::make_pair(4, 1));
        REQUIRE(evts[1] == std::make_pair(5, 2));
    }

    SECTION("getNextEventType == InvalidEventType after calling clear()")
    {
        box.receiveThreadedEvent(0, 5);
        box.clear();
        auto ap = box.getAccessPoint();
        CHECK(box.getNextEventType(ap));
    }
}
