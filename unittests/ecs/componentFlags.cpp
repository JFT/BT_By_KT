#include "../src/ecs/componentFlags.h"
#include <catch2/catch.hpp>
#include <random>

std::random_device true_random;
std::mt19937_64 twister(true_random());

using namespace Components;

TEST_CASE("NoComponents has no components", "[ComponentFlags]")
{
    const auto flag = NoComponents;
    for (componentID_t c = 0; c < NumComponents; ++c)
    {
        CHECK(not hasComponent(flag, c));
    }
}

TEST_CASE("addComponent -> hasComponent", "[ComponentFlags]")
{
    auto flag = NoComponents;
    const auto cid = std::uniform_int_distribution<componentID_t>(0, NumComponents - 1)(twister);
    INFO("ComponentId = " << cid);
    flag = addComponent(flag, cid);
    CHECK(hasComponent(flag, cid));

    SECTION("removeComponent -> NoComponents")
    {
        CHECK(removeComponent(flag, cid) == NoComponents);
    }

    SECTION("adding another component")
    {
        const auto cid2 = std::uniform_int_distribution<componentID_t>(0, NumComponents - 1)(twister);
        if (cid != cid2)
        {
            INFO("ComponentId2 = " << cid);
            flag = addComponent(flag, cid2);
            CHECK(hasComponent(flag, cid));
            CHECK(hasComponent(flag, cid2));

            SECTION("Flags as Array")
            {
                auto arr = getComponentIDArrayFromFlag(flag);
                decltype(arr) expect = {{cid, cid2}};
                std::sort(arr.begin(), arr.end());
                std::sort(expect.begin(), expect.end());
                CHECK(arr == expect);
            }
        }
    }
}
