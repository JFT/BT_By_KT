#pragma once

#include <iostream>
#include "../src/ecs/componentID.h"
#include "../src/ecs/components.h"

struct TestComponent
{
    int value = 1929299;
    bool operator==(const TestComponent& other) const { return value == other.value; }
};

inline std::ostream& operator<<(std::ostream& out, const TestComponent& v)
{
    out << v.value;
    return out;
}

namespace Components
{
    template <>
    struct ComponentID<TestComponent>
    {
        enum
        {
            CID = 5
        };
        static constexpr const char* name = "TestComponent";
    };
} // namespace Components
