#include "../src/ecs/storageSystems/allEntitiesComponentValueStorage.h"
#include <catch2/catch.hpp>
#include "../src/utils/print/irrVectors.hpp"

#include "testComponent.h"

const std::string testTag = "[AllEntitiesComponentValueStorage]";

using Storage = AllEntitiesComponentValueStorage<TestComponent>;

TEST_CASE("getComponentID() returns TestComponent id (5)", testTag)
{
    Handles::HandleManager m;
    Storage s(m);
    CHECK(s.getComponentID() == 5);
}

TEST_CASE("getComponentName() returns TestComponent name", testTag)
{
    Handles::HandleManager m;
    Storage s(m);
    CHECK(s.getComponentName() == "TestComponent");
}

TEST_CASE("Adding and removing values", testTag)
{
    Handles::HandleManager m;
    Storage s(m);

    SECTION("Initially getComponent returns InvalidHandle")
    {
        CHECK(s.getComponent(123) == Handles::InvalidHandle);
    }

    SECTION("Default-initialized value if addComponent with nullptr called")
    {
        s.addComponent(19, nullptr);
        auto handle = s.getComponent(19);
        REQUIRE(handle != Handles::InvalidHandle);
        CHECK(m.getAsValue<TestComponent>(handle) == TestComponent{});
    }

    SECTION("Default-initialized value if addComponent with invalid handle to copy from called")
    {
        s.addComponent(19, Handles::InvalidHandle);
        auto handle = s.getComponent(19);
        REQUIRE(handle != Handles::InvalidHandle);
        CHECK(m.getAsValue<TestComponent>(handle) == TestComponent{});
    }

    SECTION("Adding a value for a high entityID doesn't add values for lower entityIDs")
    {
        s.addComponent(19, nullptr);
        for (size_t i = 0; i < 19; ++i)
        {
            CHECK(s.getComponent(i) == Handles::InvalidHandle);
        }
    }

    SECTION("addComponent with ptr to value stores the value behind the handle")
    {
        TestComponent p{7};
        s.addComponent(5, &p);
        auto handle = s.getComponent(5);
        REQUIRE(handle != Handles::InvalidHandle);
        CHECK(m.getAsValue<TestComponent>(handle).value == 7);

        SECTION("addComponent() with existing handle copies value")
        {
            const auto copied = s.addComponent(1, handle);
            CHECK(m.getAsValue<TestComponent>(copied).value == 7);
        }

        SECTION("Values of existing components can be copied to other existing components")
        {
            s.addComponent(2);
            s.copyComponent(s.getComponent(2), s.getComponent(5));
            REQUIRE(s.getComponent(2) != Handles::InvalidHandle);
            CHECK(m.getAsValue<TestComponent>(s.getComponent(2)).value == 7);
        }
    }

    SECTION("after removeComponent() the handle is InvalidHandle")
    {
        s.addComponent(5, nullptr);
        s.removeComponent(5);
        CHECK(s.getComponent(5) == Handles::InvalidHandle);
    }
}

TEST_CASE("createFreeValue() returns valid handle", testTag)
{
    Handles::HandleManager m;
    Storage s(m);
    const auto handle = s.createFreeValue();
    CHECK(handle != Handles::InvalidHandle);
}

TEST_CASE("createFreeValue()'s handle-reusing doesn't touch still-used handles")
{
    Handles::HandleManager m;
    Storage s(m);
    const auto handle1 = s.createFreeValue();
    m.getAsPtr<TestComponent>(handle1)->value = 12;
    const auto handle2 = s.createFreeValue();
    m.getAsPtr<TestComponent>(handle2)->value = 13;
    s.destroyFreeValue(handle1);
    const auto handle3 = s.createFreeValue();
    CHECK(m.getAsValue<TestComponent>(handle2).value == 13);
}
