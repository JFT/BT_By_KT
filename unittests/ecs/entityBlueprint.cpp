#include "../src/ecs/entityBlueprint.h"
#include <catch2/catch.hpp>
#include "../src/ecs/entityManager.h"

TEST_CASE("Adding components", "[EntityBlueprint]")
{
    EntityManager em;
    EntityBlueprint bp("bp", em);

    using pr = decltype(bp.getAllComponents())::value_type;

    SECTION("after adding we can read components back")
    {
        bp.addComponent(0, 13);
        CHECK(bp.getComponent(0) == 13);
        CHECK(bp.getAllComponents().front().first == 0);
        CHECK(bp.getAllComponents().front().second == 13);
    }

    SECTION("adding multiple components puts them into getAllComponents")
    {
        bp.addComponent(0, 13);
        bp.addComponent(10, 2);
        CHECK(bp.getAllComponents().at(0) == pr{0, 13});
        CHECK(bp.getAllComponents().at(1) == pr{10, 2});
    }

    SECTION("components are sorted by componentID")
    {
        bp.addComponent(10, 2);
        bp.addComponent(9, 3);
        CHECK(bp.getAllComponents()[0].first == 9);
    }
}
