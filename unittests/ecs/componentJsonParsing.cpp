
#include "../src/ecs/componentJsonParsing.h"
#include <catch2/catch.hpp>
#include "../src/utils/debug.h"
#include "../src/utils/json.h"

using namespace Components;
using namespace ComponentParsing;

// IJ = inline Json with conversion operator
class IJ
{
   public:
    IJ(const std::string text)
    {
        Json::Reader reader;
        reader.parse(text, value);
    }

    operator ::Json::Value() { return this->value; }
    Json::Value value;
};

template <typename T>
bool parsingEmptyValueReturnsFalse()
{
    T c;
    return not parseFromJson(Json::Value{}, c);
}

TEST_CASE("Empty Json Value Returns false", "[ComponentJsonParsing]")
{
    CHECK(parsingEmptyValueReturnsFalse<DebugColor>());
    CHECK(parsingEmptyValueReturnsFalse<Position>());
    CHECK(parsingEmptyValueReturnsFalse<Movement>());
    CHECK(parsingEmptyValueReturnsFalse<ProjectileMovement>());
    CHECK(parsingEmptyValueReturnsFalse<Rotation>());
    CHECK(parsingEmptyValueReturnsFalse<EntityName>());
    CHECK(parsingEmptyValueReturnsFalse<Price>());
    CHECK(parsingEmptyValueReturnsFalse<Damage>());
    CHECK(parsingEmptyValueReturnsFalse<Range>());
    CHECK(parsingEmptyValueReturnsFalse<Inventory>());
    CHECK(parsingEmptyValueReturnsFalse<Requirement>());
    CHECK(parsingEmptyValueReturnsFalse<SelectionID>());
    CHECK(parsingEmptyValueReturnsFalse<Cooldown>());
    CHECK(parsingEmptyValueReturnsFalse<Physical>());
    CHECK(parsingEmptyValueReturnsFalse<OwnerEntityID>());
    CHECK(parsingEmptyValueReturnsFalse<DroppedItemEntityID>());
    CHECK(parsingEmptyValueReturnsFalse<PlayerStats>());
    CHECK(parsingEmptyValueReturnsFalse<PlayerBase>());
}

TEST_CASE("Position: Not Two Values Throws", "[ComponentJsonParsing]")
{
    Position p;
    CHECK_THROWS(parseFromJson(IJ("[1]"), p));
    CHECK_NOTHROW(parseFromJson(IJ("[1,2]"), p));
    CHECK_THROWS(parseFromJson(IJ("[1,2,3]"), p));
    CHECK_THROWS(parseFromJson(IJ("[1,2,3,4,5]"), p));
}

TEST_CASE("Position: Parsing Values Works", "[ComponentJsonParsing]")
{
    Position p;
    CHECK(parseFromJson(IJ("[1.1, 3.5]"), p));
    CHECK(p.position.X == Approx(1.1));
    CHECK(p.position.Y == Approx(3.5));
    CHECK(parseFromJson(IJ("[2.1, 3.5]"), p));
    CHECK(p.position.X == Approx(2.1));
    CHECK(p.position.Y == Approx(3.5));
    CHECK(parseFromJson(IJ("[2.1, -5234]"), p));
    CHECK(p.position.X == Approx(2.1));
    CHECK(p.position.Y == Approx(-5234));
}

TEST_CASE("Movement: Must Contain Dictionary", "[ComponentJsonParsing]")
{
    Movement c;
    CHECK(not parseFromJson(IJ("[1.1, 3.5, 'notadict']"), c));
}

TEST_CASE("Movement: Invalid Dict Entry Throws", "[ComponentJsonParsing]")
{
    Movement c;
    CHECK_THROWS(not parseFromJson(IJ("{\"wrong\":dict}"), c));
}

TEST_CASE("Movement: Dict keys are case insensitive", "[ComponentJsonParsing]")
{
    Movement c;
    CHECK(parseFromJson(IJ("{\"speed\":5.2,\"radius\":3.3}"), c));
    CHECK(c.speed == Approx(5.2));
    CHECK(c.radius == Approx(3.3));
    CHECK(parseFromJson(IJ("{\"Speed\":2.2,\"Radius\":1.3}"), c));
    CHECK(c.speed == Approx(2.2));
    CHECK(c.radius == Approx(1.3));
}

TEST_CASE("ProjectileMovement: Must Contain Dictionary", "[ComponentJsonParsing]")
{
    ProjectileMovement c;
    CHECK(not parseFromJson(IJ("[1.1, 3.5, 'notadict']"), c));
}

TEST_CASE("ProjectileMovement: Invalid Dict Throws", "[ComponentJsonParsing]")
{
    ProjectileMovement c;
    CHECK_THROWS(parseFromJson(IJ("{\"invalid\":dict}"), c));
}

TEST_CASE("ProjectileMovement: Speed is Case Insensitive", "[ComponentJsonParsing]")
{
    ProjectileMovement c;
    CHECK(parseFromJson(IJ("{\"speed\":3.3}"), c));
    CHECK(c.speed == Approx(3.3));
    CHECK(parseFromJson(IJ("{\"Speed\":4.3}"), c));
    CHECK(c.speed == Approx(4.3));
}

TEST_CASE("Rotation: Not Three Values Throws", "[ComponentJsonParsing]")
{
    Rotation r;
    CHECK_THROWS(parseFromJson(IJ("[1]"), r));
    CHECK_THROWS(parseFromJson(IJ("[1,2]"), r));
    CHECK_NOTHROW(parseFromJson(IJ("[1,2,3]"), r));
    CHECK_THROWS(parseFromJson(IJ("[1,2,3,4,5]"), r));
}

TEST_CASE("Rotation: Parsing Values Works", "[ComponentJsonParsing]")
{
    Rotation r;
    CHECK(parseFromJson(IJ("[1.1, 3.5, 4.6]"), r));
    CHECK(r.rotation.X == Approx(1.1));
    CHECK(r.rotation.Y == Approx(3.5));
    CHECK(r.rotation.Z == Approx(4.6));
    CHECK(parseFromJson(IJ("[2.1, 3.5, 4.6]"), r));
    CHECK(r.rotation.X == Approx(2.1));
    CHECK(r.rotation.Y == Approx(3.5));
    CHECK(r.rotation.Z == Approx(4.6));
    CHECK(parseFromJson(IJ("[2.1, -5234, -71234]"), r));
    CHECK(r.rotation.X == Approx(2.1));
    CHECK(r.rotation.Y == Approx(-5234));
    CHECK(r.rotation.Z == Approx(-71234));
}

TEST_CASE("EntityName: Empty String -> returns False", "[ComponentJsonParsing]")
{
    EntityName c;
    CHECK(!parseFromJson(IJ(""), c));
    CHECK(1 == 1);
}

TEST_CASE("EntityName: String is Set", "[ComponentJsonParsing]")
{
    EntityName c;
    REQUIRE(parseFromJson(IJ("\"teststring123\""), c));
    CHECK(c.entityName == irr::core::stringw("teststring123"));
}

TEST_CASE("Price: rounds to nearest uint16", "[ComponentJsonParsing]")
{
    Price c;
    REQUIRE(parseFromJson(IJ("1.23"), c));
    CHECK(c.price == 1);
    REQUIRE(parseFromJson(IJ("1.53"), c));
    CHECK(c.price == 2);
    REQUIRE(parseFromJson(IJ("9.99"), c));
    CHECK(c.price == 10);
}
