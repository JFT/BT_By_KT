#include "../src/ecs/userinput/selectionBoxContext.h"
#include <catch2/catch.hpp>
#include "../src/utils/print/irrVectors.hpp"

namespace
{
    const std::string tag = "[SelectionBoxContext]";

    constexpr auto onSelectionDoneDummy = [](const aabbox2di&) {};

    bool operator==(const InputContext::EventState_t& a, const InputContext::EventState_t& b)
    {
        return a.handled == b.handled && a.stayInContext == b.stayInContext;
    }
} // namespace

// comparison operators for different namespaces must be declared in global scope
using ::operator==;

using InputStates = SelectionBoxContext::SelectionHandling::InputStates;

TEST_CASE("initializes in default state", tag)
{
    SelectionBoxContext c{onSelectionDoneDummy};
    CHECK(c.selectionHandling.inputState == InputStates::E_Default);
}

TEST_CASE("starts selection box at mouse position", tag)
{
    SelectionBoxContext c{onSelectionDoneDummy};
    irr::SEvent e;
    e.MouseInput.X = 12;
    e.MouseInput.Y = 45;
    c.handleMousePressedDown(e);

    CHECK(c.selectionHandling.inputState == InputStates::E_SelectionSquare);
    CHECK(c.selectionHandling.selectionStart == irr::core::vector2di{{12, 45}});

    SECTION("the selection box is at least one pixel big")
    {
        CHECK(c.selectionHandling.currentSelection.getArea() != 0);
    }

    SECTION("moving the mouse adds the new position to the selection")
    {
        e.MouseInput.X = 20;
        e.MouseInput.Y = 50;
        c.handleMouseMoved(e);
        CHECK(c.selectionHandling.selectionStart == irr::core::vector2di{{12, 45}});
        CHECK(c.selectionHandling.currentSelection.MaxEdge == irr::core::vector2di{{20, 50}});
    }

    SECTION("onSelect() resets to default state")
    {
        c.onSelect();
        CHECK(c.selectionHandling.inputState == InputStates::E_Default);
    }

    SECTION("onDeselect() resets to default state")
    {
        c.onDeselect();
        CHECK(c.selectionHandling.inputState == InputStates::E_Default);
    }
}

TEST_CASE("actionMap passes mouse clicks")
{
    SelectionBoxContext c{onSelectionDoneDummy};
    irr::SEvent e;
    e.EventType = irr::EEVENT_TYPE::EET_MOUSE_INPUT_EVENT;
    e.MouseInput.Event = irr::EMOUSE_INPUT_EVENT::EMIE_LMOUSE_PRESSED_DOWN;
    e.MouseInput.X = 17;
    e.MouseInput.Y = 20;

    CHECK(c.handleInputEvent(e) == InputContext::EventState_t{true, true});

    CHECK(c.selectionHandling.inputState == InputStates::E_SelectionSquare);
    CHECK(c.selectionHandling.selectionStart == irr::core::vector2di{{17, 20}});

    SECTION("moving the mouse adds the new position to the selection")
    {
        e.MouseInput.Event = irr::EMOUSE_INPUT_EVENT::EMIE_MOUSE_MOVED;
        e.MouseInput.X = 30;
        e.MouseInput.Y = 70;
        c.handleInputEvent(e);
        CHECK(c.selectionHandling.selectionStart == irr::core::vector2di{{17, 20}});
        CHECK(c.selectionHandling.currentSelection.MaxEdge == irr::core::vector2di{{30, 70}});
    }
}
