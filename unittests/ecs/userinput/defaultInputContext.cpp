#include "../src/ecs/userinput/defaultInputContext.h"

#include <catch2/catch.hpp>

namespace
{
    const std::string tag = "[DefaultInputContext]";

    [[nodiscard]] irr::SEvent testEvent() noexcept
    {
        irr::SEvent e;
        e.EventType = irr::EEVENT_TYPE::EET_KEY_INPUT_EVENT;
        e.KeyInput.Key = irr::EKEY_CODE::KEY_CONTROL;
        return e;
    }

    bool operator==(const InputContext::EventState_t& a, const InputContext::EventState_t& b)
    {
        return a.handled == b.handled && a.stayInContext == b.stayInContext;
    }

} // namespace

// comparison operators for different namespaces must be declared in global scope
using ::operator==;


TEST_CASE("handleInputEvent", tag)
{
    DefaultInputContext c;
    const auto event = testEvent();

    SECTION("returns not handled if not hitting mapped action")
    {
        CHECK(!c.handleInputEvent(event).handled);
    }

    SECTION("returns stay in context if not hitting mapped action")
    {
        CHECK(c.handleInputEvent(event).stayInContext);
    }

    SECTION("with registered action function")
    {
        bool called = false;
        InputContext::EventState_t state = {true, true};

        auto call = [&called, &state](const auto&) {
            called = true;
            return state;
        };
        c.setInputAction(DefaultInputContext::generateUniqueEventValue(event), call);

        SECTION("doesn't call if using different event")
        {
            auto differentEvent = event;
            differentEvent.KeyInput.Key = irr::EKEY_CODE::KEY_ADD;
            c.handleInputEvent(differentEvent);
            CHECK(!called);
        }

        SECTION("called if using registered event")
        {
            called = false;
            c.handleInputEvent(event);
            CHECK(called);
        }

        SECTION("returns return value of function")
        {
            state = InputContext::EventState_t{true, true};
            CHECK(c.handleInputEvent(event) == InputContext::EventState_t{true, true});

            state = InputContext::EventState_t{false, true};
            CHECK(c.handleInputEvent(event) == InputContext::EventState_t{false, true});

            state = InputContext::EventState_t{true, false};
            CHECK(c.handleInputEvent(event) == InputContext::EventState_t{true, false});

            state = InputContext::EventState_t{false, false};
            CHECK(c.handleInputEvent(event) == InputContext::EventState_t{false, false});
        }
    }
}
