#include "../src/ecs/userinput/inputContextManager.h"
#include "../src/ecs/userinput/inputContext.hpp"

#include <catch2/catch.hpp>

struct TestContext1 : public InputContext
{
    TestContext1()
        : InputContext(InputContextTypes::DEFAULT_CONTEXT)
    {
    }

    void onSelect() override { onSelectCalled = true; };
    void onDeselect() override { onDeselectCalled = true; };

    bool onSelectCalled = false;
    bool onDeselectCalled = false;
};

struct TestContext2 : public InputContext
{
    TestContext2()
        : InputContext(InputContextTypes::DEFAULT_CONTEXT)
    {
    }

    void onSelect() override { onSelectCalled = true; };
    void onDeselect() override { onDeselectCalled = true; };

    bool onSelectCalled = false;
    bool onDeselectCalled = false;
};

const std::string tag = "[InputContextManager]";

TEST_CASE("Has default context if initialized with it", tag)
{
    auto c1 = std::make_unique<TestContext1>();
    const auto* const ptr = c1.get();
    InputContextManager im(std::move(c1));

    CHECK(im.getInputContext(InputContextTypes::DEFAULT_CONTEXT) == ptr);
}

TEST_CASE("If changing context calls onDeselect() of old context()", tag)
{
    auto c1 = std::make_unique<TestContext1>();
    const auto* const ptr1 = c1.get();
    InputContextManager im(std::move(c1));

    auto c2 = std::make_unique<TestContext2>();
    const auto* const ptr2 = c2.get();

    im.setInputContext(InputContextTypes::ENTITY_SELECTED_CONTEXT, std::move(c2));
    im.switchContext(InputContextTypes::ENTITY_SELECTED_CONTEXT);

    CHECK(ptr1->onDeselectCalled);

    SECTION("and calls onSelect() of new context") { CHECK(ptr2->onSelectCalled); }
}
