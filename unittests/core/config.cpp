#include "../src/core/config.h"
#include <catch2/catch.hpp>
#include <irrlicht/irrlicht.h>
#include "../src/utils/static/error.h"


#include <fstream>

#define SAME_AS_DEFAULT(config, memberName) \
    (config).getSettings().memberName == (config).defaultSettings.memberName

TEST_CASE("Default Config is loaded if config.xml is not found", "[Config]")
{
    Config config;
    using irr::core::stringw;
    REQUIRE(config.loadConfig(" ") == ErrCodes::GENERIC_ERR);

    REQUIRE(SAME_AS_DEFAULT(config, soundEnabled));
    REQUIRE(SAME_AS_DEFAULT(config, soundVolume));
    REQUIRE((config.getSettings().playerName == config.defaultSettings.playerName));
    REQUIRE(SAME_AS_DEFAULT(config, playerColor));
    REQUIRE((config.getSettings().serverName == config.defaultSettings.serverName));
    REQUIRE(SAME_AS_DEFAULT(config, serverPort));
    REQUIRE((config.getSettings().serverPassword == config.defaultSettings.serverPassword));
    REQUIRE(SAME_AS_DEFAULT(config, numberOfSlots));
    REQUIRE((config.getSettings().connectedServerName == config.defaultSettings.connectedServerName));
    REQUIRE(SAME_AS_DEFAULT(config, connectedServerAddress));
    REQUIRE((config.getSettings().connectedServerPassword == config.defaultSettings.connectedServerPassword));
    REQUIRE(SAME_AS_DEFAULT(config, myIdentificationKey));
}

namespace
{
    struct InplaceConfig
    {
        InplaceConfig(const std::string& name)
            : m_name(name + ".xml")
        {
            file.open(m_name);
            file << L"<?xml version=\"1.0\"?>\n<!--(encoding=&quot;UTF-32&quot;)-->";
        }

        template <typename T>
        InplaceConfig& operator<<(const T& m)
        {
            file << m;
            return *this;
        }

        InplaceConfig& add(const std::wstring& text)
        {
            file << text;
            return *this;
        }

        std::string create()
        {
            file.close();
            return m_name;
        }

        const std::string m_name;
        std::wofstream file;
    };
} // namespace

TEST_CASE("config saving and loading tests", "[Config]")
{
    auto dev = irr::createDevice(irr::video::E_DRIVER_TYPE::EDT_NULL);

    SECTION("saving the player name")
    {
        Config config;

        const std::string name = "thisIsJustATestNameWhichShouldn'tExist";

        config.getModifiableSettings().playerName = name;
        config.saveConfig("nameChanged.xml", dev);

        Config loaded;

        REQUIRE(loaded.loadConfig("nameChanged.xml") == ErrCodes::NO_ERR);
        REQUIRE((loaded.getSettings().playerName == name));
    }

    SECTION("all drivers correctly serialized")
    {
        Config save;
        using DT = irr::video::E_DRIVER_TYPE;
        const auto drivers = {DT::DEPRECATED_EDT_DIRECT3D8_NO_LONGER_EXISTS,
                              DT::EDT_BGFX_D3D11,
                              DT::EDT_BGFX_D3D9,
                              DT::EDT_BGFX_METAL,
                              DT::EDT_BGFX_OPENGL,
                              DT::EDT_BURNINGSVIDEO,
                              DT::EDT_DIRECT3D9,
                              DT::EDT_NULL,
                              DT::EDT_OPENGL,
                              DT::EDT_SOFTWARE};
        REQUIRE(drivers.size() == DT::EDT_COUNT);
        for (const auto driver : drivers)
        {
            save.getModifiableSettings().irrlichtParams.DriverType = driver;
            REQUIRE(save.saveConfig("drivers.xml", dev) == ErrCodes::NO_ERR);
            Config load;
            REQUIRE(load.loadConfig("drivers.xml") == ErrCodes::NO_ERR);
            CHECK(load.getSettings().irrlichtParams.DriverType == driver);
        }
    }

    SECTION("incomplete file tests")
    {
        Config c;
        // CHECK(c.loadConfig(InplaceConfig("missing <configuration>").create()) ==
        // ErrCodes::GENERIC_ERR);
    }
}
