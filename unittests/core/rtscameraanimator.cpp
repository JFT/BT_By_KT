#include "../src/core/rtscameraanimator.h"
#include <catch2/catch.hpp>
#include <irrlicht/irrlicht.h>
#include "../src/utils/print/irrVectors.hpp"
#include "../src/utils/static/error.h"

TEST_CASE("creation test", "[RTSCameraAnimator]")
{
    auto dev = irr::createDevice(irr::video::E_DRIVER_TYPE::EDT_NULL);

    auto ctrl = dev->getCursorControl();
    REQUIRE(ctrl != nullptr);

    SECTION("grabs and drops a reference to cameraControl")
    {
        const auto before = ctrl->getReferenceCount();

        auto camAnim = new RTSCameraAnimator(ctrl, 1.0f, 10.0f, 400.0f, 2000.0f);
        CHECK(ctrl->getReferenceCount() > before);
        delete camAnim;

        CHECK(ctrl->getReferenceCount() == before);
    }

    SECTION("captures mouse and keyboard events")
    {
        const irr::core::vector2df bounds{-200.0f, 200.0f};
        auto camAnim = new RTSCameraAnimator(ctrl, 1.0f, 10.0f, 400.0f, 2000.0f, bounds, bounds);

        SECTION("handle mousewheel but nothing else")
        {
            irr::SEvent evt;
            evt.EventType = irr::EEVENT_TYPE::EET_MOUSE_INPUT_EVENT;
            evt.MouseInput.Event = irr::EMOUSE_INPUT_EVENT::EMIE_LMOUSE_LEFT_UP;
            CHECK(camAnim->OnEvent(evt) == false);
            evt.MouseInput.Event = irr::EMOUSE_INPUT_EVENT::EMIE_RMOUSE_LEFT_UP;
            CHECK(camAnim->OnEvent(evt) == false);
            evt.MouseInput.Event = irr::EMOUSE_INPUT_EVENT::EMIE_MOUSE_MOVED;
            CHECK(camAnim->OnEvent(evt) == true);
        }

        SECTION("all default keyboard bindings should be used")
        {
            for (const auto& elem : camAnim->getKeyMap())
            {
                CHECK(camAnim->OnEvent(BT::irrlichtHelpers::toEvent(elem.first)));
            }
        }

        SECTION("Camera movements and rotation")
        {
            REQUIRE(dev->getSceneManager() != nullptr);
            auto cam = dev->getSceneManager()->addCameraSceneNode();
            // set up the camera so screen X is X, screen Y is Z
            REQUIRE(cam != nullptr);
            // start camera somewhere away from (0,0) because otherwise unit-test might run into
            // clamping issues
            cam->setPosition({100.0f, 0.0f, 100.0f});
            cam->setTarget({100.0f, 0.0f, 50.0f});
            const auto startingPos = cam->getPosition();
            const auto startingTarget = cam->getTarget();
            cam->addAnimator(camAnim);

            // the very first call to animateNode does nothing because it doesn't track how much
            // time has passed, this is only set after the first call to animateNode. Thus we call
            // animateNode here and then also inside the sub-tests
            irr::u32 frame = 1;
            camAnim->animateNode(cam, frame++);
            CHECK(cam->getPosition() == startingPos);
            REQUIRE(cam->getPosition() != cam->getTarget());

            REQUIRE(dev->getVideoDriver() != nullptr);

            SECTION("Mouse movement to side should scroll")
            {
                irr::SEvent mouseMoved;
                mouseMoved.EventType = irr::EEVENT_TYPE::EET_MOUSE_INPUT_EVENT;
                mouseMoved.MouseInput.Event = irr::EMOUSE_INPUT_EVENT::EMIE_MOUSE_MOVED;

                auto evtAndAnimate = [&camAnim, &cam, mouseMoved](const auto frame) {
                    camAnim->OnEvent(mouseMoved);
                    camAnim->animateNode(cam, frame);
                };
                auto mouseTo = [&ctrl, &evtAndAnimate](const float x, const float y, const auto frame) {
                    ctrl->setPosition(x, y);
                    evtAndAnimate(frame);
                };

                SECTION("moving right with mouse")
                {
                    mouseTo(1.0f, 0.5f, frame++);
                    CHECK(cam->getPosition().X < startingPos.X);

                    SECTION("No more movement in same tick")
                    {
                        const auto stayedAt = cam->getPosition();
                        evtAndAnimate(frame - 1);
                        CHECK(cam->getPosition() == stayedAt);
                    }

                    SECTION("More right movement")
                    {
                        const auto oldPos = cam->getPosition();
                        evtAndAnimate(frame++);
                        CHECK(cam->getPosition().X < oldPos.X);
                    }

                    SECTION("same time other way returns to (roughly) old position")
                    {
                        mouseTo(0.0f, 0.5f, frame++);
                        CHECK(cam->getPosition().X == Approx(startingPos.X));
                    }
                }
                SECTION("moving left")
                {
                    mouseTo(0.0f, 0.5f, frame++);
                    CHECK(cam->getPosition().X > startingPos.X);
                }
                SECTION("moving up")
                {
                    mouseTo(0.5f, 0.0f, frame++);
                    CHECK(cam->getPosition().Z < startingPos.Z);
                }
                SECTION("moving down")
                {
                    mouseTo(0.5f, 1.0f, frame++);
                    CHECK(cam->getPosition().Z > startingPos.Z);
                }
                SECTION("can move and rotate at the same time")
                {
                    // this unittest test a specific scenario which occured because too many events
                    // where cleaned in case the mouse moved, which would delete any registered
                    // rotations. Because it is often very hard to not move the mouse (especially at
                    // high DPI) while clicking this test exists
                    ctrl->setPosition(irr::core::vector2df{0.5f, 0.5f});
                    irr::SEvent middleMouse;
                    middleMouse.EventType = irr::EEVENT_TYPE::EET_MOUSE_INPUT_EVENT;
                    middleMouse.MouseInput.Event = irr::EMOUSE_INPUT_EVENT::EMIE_MMOUSE_LEFT_UP;
                    camAnim->OnEvent(middleMouse);
                    camAnim->OnEvent(mouseMoved);
                    camAnim->animateNode(cam, frame++);
                    CHECK(cam->getPosition() != startingPos);
                    CHECK(cam->getTarget() == startingTarget);
                }
            } // SECTION("Mouse movement to side should scroll")

            SECTION("Scrolling with keyboard")
            {
                irr::SEvent keyInput;
                keyInput.EventType = irr::EEVENT_TYPE::EET_KEY_INPUT_EVENT;

                auto keyEvent = [&keyInput, &camAnim](const irr::EKEY_CODE key, const bool pressedDown) {
                    keyInput.KeyInput.Key = key;
                    keyInput.KeyInput.PressedDown = pressedDown;
                    camAnim->OnEvent(keyInput);
                };
                auto press = [&keyEvent](const irr::EKEY_CODE key) { keyEvent(key, true); };
                auto release = [&keyEvent](const irr::EKEY_CODE key) { keyEvent(key, false); };

                auto keyForAction = [&camAnim](const RTS_CAM_ACTION action) {
                    for (const auto& elem : camAnim->getKeyMap())
                    {
                        if (elem.second == action)
                        {
                            return BT::irrlichtHelpers::toEvent(elem.first).KeyInput.Key;
                        }
                    }
                    FAIL("No default keyboard binding for action " + std::to_string(action));
                };

                SECTION("moving right")
                {
                    press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_RIGHT));
                    camAnim->animateNode(cam, frame++);
                    release(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_RIGHT));
                    CHECK(cam->getPosition().X < startingPos.X);

                    SECTION("No more movement in same tick")
                    {
                        const auto stayedAt = cam->getPosition();
                        press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_RIGHT));
                        camAnim->animateNode(cam, frame - 1);
                        CHECK(cam->getPosition() == stayedAt);
                    }

                    SECTION("More right movement")
                    {
                        const auto oldPos = cam->getPosition();
                        press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_RIGHT));
                        camAnim->animateNode(cam, frame++);
                        CHECK(cam->getPosition().X < oldPos.X);
                    }

                    SECTION("same time other way returns to (roughly) old position")
                    {
                        press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_LEFT));
                        camAnim->animateNode(cam, frame++);
                        CHECK(cam->getPosition().X == Approx(startingPos.X));
                    }
                }
                SECTION("moving left")
                {
                    press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_LEFT));
                    camAnim->animateNode(cam, frame++);
                    CHECK(cam->getPosition().X > startingPos.X);
                }
                SECTION("moving up")
                {
                    press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_FORWARD));
                    camAnim->animateNode(cam, frame++);
                    CHECK(cam->getPosition().Z < startingPos.Z);
                }
                SECTION("moving down")
                {
                    press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_BACKWARD));
                    camAnim->animateNode(cam, frame++);
                    CHECK(cam->getPosition().Z > startingPos.Z);
                }

                SECTION("rotate camera")
                {
                    irr::SEvent middleMouse;
                    middleMouse.EventType = irr::EEVENT_TYPE::EET_MOUSE_INPUT_EVENT;
                    middleMouse.MouseInput.Event = irr::EMOUSE_INPUT_EVENT::EMIE_MMOUSE_LEFT_UP;

                    const auto targetBeforeRotate = cam->getTarget();
                    camAnim->OnEvent(middleMouse);
                    camAnim->animateNode(cam, frame++);

                    // camera continues looking at the same thing, rotation done by moving position
                    // around
                    CHECK(cam->getTarget() == targetBeforeRotate);
                    CHECK((cam->getPosition() - irr::core::vector3df(150.f, 0.f, 50.f)).getLengthSQ() ==
                          Approx(0.f).margin(0.01));

                    SECTION("Now forward is right")
                    {
                        const auto afterRotate = cam->getPosition();
                        press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_FORWARD));
                        camAnim->animateNode(cam, frame++);
                        CHECK(cam->getPosition().X < afterRotate.X);
                    }
                } // SECTION("rotate camera")

                SECTION("clamps target to bounds")
                {
                    SECTION("clamps right")
                    {
                        press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_RIGHT));
                        const auto distanceToBorder = std::abs(bounds.X - cam->getTarget().X);
                        camAnim->animateNode(cam,
                                             frame +=
                                             (2 * static_cast<uint32_t>(distanceToBorder / camAnim->getMoveSpeed())));
                        CHECK(cam->getTarget() ==
                              irr::core::vector3df(bounds.X, startingTarget.Y, startingTarget.Z));
                    }
                    SECTION("clamps left")
                    {
                        press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_LEFT));
                        const auto distanceToBorder = std::abs(bounds.Y - cam->getTarget().X);
                        camAnim->animateNode(cam,
                                             frame +=
                                             (2 * static_cast<uint32_t>(distanceToBorder / camAnim->getMoveSpeed())));
                        CHECK(cam->getTarget() ==
                              irr::core::vector3df(bounds.Y, startingTarget.Y, startingTarget.Z));
                    }

                    SECTION("clamps up")
                    {
                        press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_FORWARD));
                        const auto distanceToBorder = std::abs(bounds.X - cam->getTarget().Z);
                        camAnim->animateNode(cam,
                                             frame +=
                                             (2 * static_cast<uint32_t>(distanceToBorder / camAnim->getMoveSpeed())));
                        CHECK(cam->getTarget() ==
                              irr::core::vector3df(startingTarget.X, startingTarget.Y, bounds.X));
                    }
                    SECTION("clamps down")
                    {
                        press(keyForAction(RTS_CAM_ACTION::RTS_CAM_MOVE_BACKWARD));
                        const auto distanceToBorder = std::abs(bounds.Y - cam->getTarget().Z);
                        camAnim->animateNode(cam,
                                             frame +=
                                             (2 * static_cast<uint32_t>(distanceToBorder / camAnim->getMoveSpeed())));
                        CHECK(cam->getTarget() ==
                              irr::core::vector3df(startingTarget.X, startingTarget.Y, bounds.Y));
                    }
                } // SECTION("clamps on all borders")

            } // SECTION("Scrolling with keyboard")

        } // SECTION("Camera movements and rotation")
    }
}
