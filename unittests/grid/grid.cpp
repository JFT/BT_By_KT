#include "../src/grid/grid.h"
#include <catch2/catch.hpp>

const std::string test_tag = "[Grid]";

TEST_CASE("In a (5*64)x(7*64) grid (64 sideLength)", test_tag)
{
    grid::Grid g({{5 * 64.0f, 7 * 64.0f}}, 64.0f);

    SECTION("x = 0 is at index 0")
    {
        CHECK(g.getXIndex(0.0f) == 0);
        CHECK(g.getXIndex({0.0f, 100.0f}) == 0);
        CHECK(g.getXIndex({0.0f, 100.0f, 100.0f}) == 0);
    }

    SECTION("x = 200 is at index 3")
    {
        CHECK(g.getXIndex(200.0f) == 3);
        CHECK(g.getXIndex({200.0f, 100.0f}) == 3);
        CHECK(g.getXIndex({200.0f, 100.0f, 100.0f}) == 3);
    }

    SECTION("x = 9999999 is at index 4 (clamped)")
    {
        CHECK(g.getXIndex(9999999.0f) == 4);
        CHECK(g.getXIndex({9999999.0f, 100.0f}) == 4);
        CHECK(g.getXIndex({9999999.0f, 100.0f, 100.0f}) == 4);
    }

    SECTION("z = 0 is at index 0")
    {
        CHECK(g.getZIndex(0.0f) == 0);
        CHECK(g.getZIndex({100.0f, 0.0f}) == 0);
        CHECK(g.getZIndex({100.0f, 100.0f, 0.0f}) == 0);
    }

    CHECK(g.getCellID(0, 0) == 0);
    SECTION("cellIDs are incremented by x, then y")
    {
        CHECK(g.getCellID(1, 0) == 1);
        CHECK(g.getCellID(0, 1) == 5);
    }

    CHECK(g.getXZCoordinate(g.getCellID(4, 3)) == std::pair<grid::gridCellID_t, grid::gridCellID_t>{4, 3});

    SECTION("3x3 around (1,1) as expected")
    {
        auto around = g.get3x3Around(g.getCellID(1, 1));
        decltype(around) expect = {{0, 1, 2, 5, 6, 7, 10, 11, 12}};
        std::sort(around.begin(), around.end());
        std::sort(expect.begin(), expect.end());
        CHECK(around == expect);
    }

    SECTION("3x3 around (0,0) only has 4 valid values")
    {
        const auto around = g.get3x3Around(0);
        CHECK(std::count_if(around.cbegin(), around.cend(), [](const auto& v) {
                  return v != grid::InvalidCellID;
              }) == 4);
    }

    SECTION("getNeighbors is 3x3 without the cell itself")
    {
        const auto ar = g.get3x3Around(g.getCellID(1, 1));
        std::vector<grid::gridCellID_t> around(ar.cbegin(), ar.cend());
        const auto nr = g.getNeighbors(g.getCellID(1, 1));
        std::vector<grid::gridCellID_t> neighbors(nr.cbegin(), nr.cend());
        std::sort(around.begin(), around.end());
        std::sort(neighbors.begin(), neighbors.end());
        around.erase(std::find(around.begin(), around.end(), g.getCellID(1, 1)));
        CHECK(around == neighbors);
    }
}
