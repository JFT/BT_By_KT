#include <sstream>
#include <utility>

namespace Catch
{
    template <typename T1, typename T2>
    struct StringMaker<std::pair<T1, T2>>
    {
        static std::string convert(std::pair<T1, T2> const& value)
        {
            std::stringstream ss;
            ss << '<' << value.first << ',' << value.second << '>';
            return ss.str();
        }
    };
} // namespace Catch
