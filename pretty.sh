#!/bin/sh

if [ $# -lt 3 ]; then
    echo "pretty.sh must be called with at least 3 arguments" 1>&2
    echo "usage: pretty.sh osinfo format|test SRCPATH [{FILES|FOLDERS}...]" 1>&2
    echo "osinfo: content of make variable \$(OS)" 1>&2
    echo "format: unformatted files are formatted inplace" 1>&2
    echo "test: no files are changed. Exits with exit code 1 if any unformatted files exist" 1>&2
    echo "SRCPATH is the path to the source" 1>&2
    echo "FILES\|FOLDERS are files/folders of the project. pretty.sh will try to automatically add SCRPATH to the file/folders if it is missing" 1>&2
    exit 1
fi

OS=$1
FORMAT=$2
SRCPATH=$3

CLANG_FORMAT_CALL=""
if [ "x$OS" = "xWindows_NT" ]; then
    if [ -x "media/clang-format/win32/clang-format.exe" ]; then
        CLANG_FORMAT_CALL="media/clang-format/win32/clang-format.exe"
    elif clang-format-6.0 --version > /dev/null 2>&1; then
        CLANG_FORMAT_CALL=clang-format-6.0
    else
        clang-format --version | grep "6\.\0\.\0" && CLANG_FORMAT_CALL="clang-format"
    fi
else
    if [ -x "media/clang-format/linux/clang-format" ]; then
        export LD_LIBRARY_PATH="media/clang-format/linux:$LD_LIBRARY_PATH"
        CLANG_FORMAT_CALL="media/clang-format/linux/clang-format"
    elif clang-format-6.0 --version > /dev/null 2>&1; then
        CLANG_FORMAT_CALL=clang-format-6.0
    else
        clang-format --version | grep "6\.\0\.\0" && CLANG_FORMAT_CALL="clang-format"
    fi
fi

if [ "x$CLANG_FORMAT_CALL" = "x" ]; then
    echo "clang-format version 6.0 not found in media/clang-format and no suitable clang-format version installed!" 1>&2
    exit 2
fi
echo "using clang-format version:"
$CLANG_FORMAT_CALL --version

shift 3
FILES=$@

clang_format_file () {
    file_clang_format_file=$@
    if [ ! -f "$file_clang_format_file" ]; then
        echo "file '$file_clang_format_file' not found!" 1>&2
        exit 1
    fi
    $CLANG_FORMAT_CALL -output-replacements-xml "$file_clang_format_file" | grep -c "<replacement " >/dev/null
    if [ $? -ne 1 ]; then
        if [ "x$FORMAT" = "xformat" ]; then
            echo "formatting file '$file_clang_format_file'" 1>&2
            $CLANG_FORMAT_CALL -i "$file_clang_format_file"
        else
            echo "file '$file_clang_format_file' isn't formatted!" 1>&2
        fi
        return 0
    fi
    return 1
}

clang_format_folder() {
    DIDFORMAT=0
    FOLDER=$@
    echo "formatting files in folder '$FOLDER'" 1>&2
    IFS=`printf '\n+'`; IFS=${IFS%+}
    for file_clang_format_folder in `find "$FOLDER" \( -iname *.h -or -iname *.cpp \)`; do
        clang_format_file "$file_clang_format_folder" && DIDFORMAT=1
    done
    if [ $DIDFORMAT -eq 1 ]; then
        return 0
    fi
    return 1
}

if [ -z "$FILES" ]; then
    if [ "x$FORMAT" = "xtest" ]; then
        clang_format_folder "$SRCPATH" || exit 0
        echo "ERROR: unformatted files found!" 1>&2
        exit 1
    else
        clang_format_folder "$SRCPATH" || echo "Nothing to format in '$SRCPATH'"
    fi
    exit 0
else
    for file in "$FILES"; do
        if [ -f "$SRCPATH/$file" ]; then
            clang_format_file "$SRCPATH/$file" || echo "'$SRCPATH/$file' doesn't need formatting"
        elif [ -f "$file" ]; then
            clang_format_file "$file" || echo "'$file' doesn't need formatting"
        elif [ -d "$SRCPATH/$file" ]; then
            clang_format_folder "$SRCPATH/$file" || "Nothing to format in '$SRCPATH/$file'"
        elif [ -d "$file" ]; then
            clang_format_folder "$file" || "Nothing to format in '$file'"
        else
            echo "ERROR: no such file or directory '$file'" 1>&2
            exit 1
        fi
    done
fi
