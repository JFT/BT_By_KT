#!/bin/bash

analyze() {
    [ -n "$1" ] || echo "function analyze(): no filename given"
    [ -n "$1" ] || exit 1
    python3 interpretProfile.py "$1"
}


usage="usage: <processname> <output-file> [<sleep between measurements (default 1)> <# of samples (default 50)>] (program shoud be compiled with -ggdb or -g)"

[ -n "$1" ] || echo $usage
[ -n "$1" ] || exit 1
[ -n "$2" ] || echo $usage
[ -n "$2" ] || exit 1
processname=$1
outfile=$2

# append .profile to the filename if not specified
case "$outfile" in
    *.profile)
        # nothing to do
        ;;
    *)
        echo "automatically appending '.profile' to output filename '$outfile'"
        outfile="$outfile".profile
        ;;
esac

# if the data already exists just analyze it again
if [ -f "$outfile" ]; then
    echo "'$outfile' already exists -> only analyzing again"
    analyze "$outfile"
    exit 0
fi

sleeptime="$3"
[ -z "$sleeptime" ] && sleeptime=1
nsamples="$4"
[ -z "$nsamples" ] && nsamples=50

pid=$(pgrep -f $processname)


# filter out this profiling process
pid=$(echo "$pid" | grep --line-regexp -v "$$")

if [ -z "$pid" ]; then
	echo "process with name "$processname" not found"
	read -p "enter process id manually: " userpid
	pid=$userpid
fi

while [ $(echo $pid | wc --words) -gt 1 ];
do
    echo "more than 1 PID matches the name $processname."
    echo "pick one (enter the number in front of the PID)"
    i=1
    for single_pid in $pid
    do
        # display the commandline next to the pid of the process
        echo "$i): $single_pid $(ps --pid $single_pid --no-headers -o cmd)"
        let "i++"
    done
    read -p "enter the number in front of the PID: " userinput
    [ -n "$userinput" ] || echo "no input read."
    [ -n "$userinput" ] || continue
    i=1
    for single_pid in $pid
    do
        #echo "comparing $userinput against spid=$single_pid i=$i"
        if [ $userinput = $i ]; then
            pid=$single_pid
            break
        fi
        let "i++"
    done
    #echo "pid pipe words -> $(echo $pid | wc --words)"
    if [ $(echo $pid | wc --words) -gt 1 ]; then
        echo "incorrect input '"$userinput"'"
    else
        break
    fi
done

echo "profiling for pid $pid" process "'"$(ps --pid $pid --no-headers -o cmd)"'"
PIPENAME=$(mktemp --dry-run)

if mkfifo "$PIPENAME"; then
    echo "using input pipe '$PIPENAME' and to communicate with gdb"
    gdb --quiet -p $pid < "$PIPENAME" | {
        # wait for gdb to signal that it is ready
        echo "waiting 30 seconds for gdb to fully load" >&2
        while read -t 30 data; do  # read the first few input things and continue after no more output was generated for 30 seconds
            echo "$data" >> "$outfile"
        done
        # all echos going to stdout will be read by gdb
        echo "set pagination 0"
        echo "set interactive-mode 0"

        for x in $(seq 1 $nsamples)
        do
            echo "measuring step $x/$nsamples" >&2
            # starting gdb stops the process
            echo "thread apply all bt"
            echo "continue"
            # wait for the "Continuing" output of gdb
            while read data; do
                echo "$data" >> "$outfile"
                echo "$data" | grep --quiet "Continuing" "-" && break
            done
            echo "sleeping $sleeptime seconds" >&2
            sleep $sleeptime
            # interrupt the running process again (for next measurement or to signal the "quit" command)
            kill -s INT $pid
            # wait until gdb receives the interrupt
            while read data; do
                echo "$data" >> "$outfile"
                echo "$data" | grep --quiet "SIGINT" && break
            done
        done
        # 'detach' and 'quit' can only be used if $pid is INTeruppted!
        echo "detach"
        echo "quit"

    } > "$PIPENAME"

    echo "removing pipe '$PIPENAME'"

    rm -f "$PIPENAME"
else
    echo "couldn't create pipe -> starting gdb repeatedly"
    for x in $(seq 1 $nsamples)
    do
        echo "measuring step $x/$nsamples" >&2
        gdb --command gdbCommand -batch -p $pid >> "$outfile"
        sleep $sleeptime
    done
fi

# do the analysis afterwards
analyze "$outfile"
