#!/bin/sh

SEVENZIP=$1

if [ -z "$SEVENZIP" ]; then
    echo "" >&2
    echo "ERROR:" >&2
    echo "\$SEVENZIP variable not set!" >&2
    echo "Add a line 'SEVENZIP := path/to/7z' to your userlibs.mk" >&2
    echo "" >&2
    exit 1
fi

if [ -e "bin/Win32-Visualstudio_tmp" ]; then
    echo "" >&2
    echo "ERROR:" >&2
    echo "'bin/Win32-Visualstudio_tmp' directory already exists!" >&2
    echo "remove it and retry" >&2
    echo "" >&2
    exit 1
fi

if [ -e "lib_tmp" ]; then
    echo "" >&2
    echo "ERROR:" >&2
    echo "'lib_tmp' directory already exists!" >&2
    echo "remove it and retry" >&2
    echo "" >&2
    exit 1
fi

if [ -e "include_tmp" ]; then
    echo "" >&2
    echo "ERROR:" >&2
    echo "'lib_tmp' directory already exists!" >&2
    echo "remove it and retry" >&2
    echo "" >&2
    exit 1
fi

if [ -f "dep-package.7z" ]; then
	echo "dep-package.7z still exists: deleting"
	rm dep-package.7z
fi

# neccecary dll files
echo "renaming 'bin/Win32-Visualstudio' directory to temporary 'bin/Win32-Visualstudio_tmp'"
mv bin/Win32-Visualstudio bin/Win32-Visualstudio_tmp || exit 1
echo "re-creating 'bin/Win32-Visualstudio/' directory"
mkdir bin/Win32-Visualstudio || exit 2
echo "copying dlls from temporary to empty 'bin/Win32-Visualstudio' directory (resolving symlinks)"
cp -L bin/Win32-Visualstudio_tmp/*.dll bin/Win32-Visualstudio/ || exit 1
echo "creating archive..."
"$SEVENZIP" a -t7z -mx=9 -myx=9 -- dep-package.7z bin/Win32-Visualstudio/*.dll || exit 1
echo "removing 'bin/Win32-Visualstudio' directory containing copied data"
rm -r bin/Win32-Visualstudio || exit 1
echo "renaming temporary 'bin/Win32-Visualstudio_tmp/' directory to 'bin/Win32-Visualstudio' to restore symbolic links (if present)"
mv bin/Win32-Visualstudio_tmp bin/Win32-Visualstudio || exit 1

# libs
echo "renaming 'lib/' directory to temporary 'lib_tmp/'"
mv lib lib_tmp || exit 1
echo "re-creating 'lib/' directory"
mkdir lib || exit 1
echo "copying dlls from temporary to empty 'lib/' directory (resolving symlinks)"
cp -L lib_tmp/* lib/ || exit 1
echo "adding to archive..."
"$SEVENZIP" a -t7z -mx=9 -myx=9 -- dep-package.7z lib/* || exit 1
echo "removing 'lib/' directory containing copied data"
rm -r lib || exit 1
echo "renaming temporary 'lib_tmp/' directory to 'lib/' to restore symbolic links (if present)"
mv lib_tmp lib || exit 1

# includes
echo "renaming 'include/' directory to temporary 'include_tmp/'"
mv include include_tmp || exit 1
echo "re-creating 'include/' directory"
mkdir include || exit 1
echo "copying dlls from temporary to empty 'include/' directory (resolving symlinks)"
cp -L -R include_tmp/* include/ || exit 1
echo "adding to archive..."
"$SEVENZIP" a -t7z -mx=9 -myx=9 -- dep-package.7z include/* || exit 1
echo "removing 'include/' directory containing copied data"
rm -r include || exit 1
echo "renaming temporary 'include_tmp/' directory to 'include/' to restore symbolic links (if present)"
mv include_tmp include || exit 1

echo "done!"

