BT_Entity = {}

BT_Entity.Tank = {}

BT_Entity.Tank.onSelect = 
function()
-- reaction to selection here for Tank
end

BT_Entity.Tank.onDeselect =
function()

end

BT_Entity.Tank.onSetMovementTarget =
function()
-- handling of movementtarget set
end

BT_Entity.Tank.onDeath =
function()

end

BT_Entity.Tank.onRespawn =
function()

end

BT_Entity.Tank.onTakenDamage =
function(damage)

end

BT_Entity.Tank.onHeal =
function(healVal)

end

BT_Entity.Tank.onAbility =
function(abilityID)

end

