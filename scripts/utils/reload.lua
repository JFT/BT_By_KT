function evalfile(filename, env)
    local f = assert(loadfile(filename))
    return f()
end

function eval(text)
    local f = assert(load(text))
    return f()
end

function errorhandler(err)
    return debug.traceback(err)
end

function reload(filename)
    local success, result = xpcall(evalfile, errorhandler, filename)
    --print(string.format("success=%s filename=%s\n", success, filename))
    if not success then
        return "[ERROR]\n".. result .. "[/ERROR]\n"
    end
end

function reload_noerror(filename)
    local success, result = xpcall(evalfile, errorhandler, filename)
    --print(string.format("success=%s filename=%s\n", success, filename))
end