#!/bin/sh

set -e
set -u

echo "Reading 'BattleTanksStandalone_SOURCE_DIR' from CMakeCache.txt"

SOURCE_DIR=`grep BattleTanksStandalone_SOURCE_DIR CMakeCache.txt | awk --field-separator '=' -- '{print $2;}'`

if [ ! -e "$SOURCE_DIR/scripts/test/auto_start_game.lua" ]; then
    echo "Couldn't find '$SOURCE_DIR/scripts/test/auto_start_game.lua'. Run this script from a cmake build directory." 1>&2
    exit 1
fi

child_pids=""

kill_everything()
{
    # not an error if it fails, because the server might not be running
    pkill btServer${SUFFIX} || echo "Server not running, couldn't be killed."
    [ ! -z "$child_pids" ] && kill $child_pids
    # unset variable because kill_everything might get called multiple
    # times and fails when trying to stop already stopped processes
    child_pids=""
}

BUILD_DIR=$(pwd)

trap kill_everything EXIT
trap kill_everything INT

make -j $(nproc) battleTanks btServer

# find out weather we should be running debug or release versions
SUFFIX=""
if [ -x btServerd ]; then
    SUFFIX="d"
fi

# stop the server in case it is still running
kill_everything

NUM_INSTANCES=2
if [ $# -gt 0 ]; then
    NUM_INSTANCES=$1
fi


cd "$SOURCE_DIR"

# start the server
("${BUILD_DIR}/btServer${SUFFIX}") > server.log 2>&1 &
# child pid not saved because it is explicitly killed in `kill_everything`

tail --follow server.log | {
    while read line; do
        echo "srv: $line"
    done
} &
child_pids="$child_pids $!"

# start one client instance, piping its output into a custom script
echo "scriptingModule:loadScriptFile(\"scripts/test/auto_start_game.lua\")" | "${BUILD_DIR}/battleTanks${SUFFIX}" --console > client_1.log 2>&1 &
child_pids="$child_pids $!"

tail --follow client_1.log | {
    while read line; do
        echo "cl1: $line"
    done
} &
child_pids="$child_pids $!"

# While the first instance is searching for servers it blocks
# the server-search port.
# Start 2nd instance only after it is definitely connected to the server.
echo "Waiting so the 2nd instance can be started..."
while [ "x" = "x" ]; do
    grep -q "clicking ready" client_1.log && break
    sleep 1s
done


if [ $NUM_INSTANCES -eq 1 ]; then
    echo "Only one instance requested"
    # must wait for the children here, otherwise the script just
    # continues and kills all instances.
    for pid in $child_pids; do
        wait $pid
    done
else
    echo "Starting second instance!"
    echo "scriptingModule:loadScriptFile(\"scripts/test/auto_start_game.lua\")" | "${BUILD_DIR}/battleTanks${SUFFIX}" --console > client_2.log 2>&1 &
    child_pids="$child_pids $!"

    tail --follow client_2.log | {
        while read line; do
            echo "cl2: $line"
        done
    }
fi

echo "$0 done"

