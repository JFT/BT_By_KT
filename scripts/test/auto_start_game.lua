print("init auto_start_game.lua")

-- helpers to inspct lua
-- pp = require 'pl.pretty'
gm = getmetatable
-- usage:
-- pp.dump(object), maybe pp.dump(gm(object))
-- if this crashes (it does for most CEGUI-related objects)
-- print(pp.write(gm(object), " ", true))

-- function to dump names of all properties of a CEGUI object
function dumpProperties(element)
    local props = element:getPropertyIterator()
    print("props = ", pp.write(gm(props), " ", true))
    while not props:isAtEnd() do
        print(pp.write(props:key(), "", true), "=", props:value())
        props:next()
    end
end


serverBrowser = game:findState("serverBrowser")
game:changeState(serverBrowser)


if (BT.gui) then
    local guiSystem = CEGUI.System:getSingleton()
    local root = guiSystem:getDefaultGUIContext():getRootWindow()
    local runOnce = false
    local runOnce2 = false
    local wroteReady = false

    function clickButton(button)
        local button = tolua.cast(button, "CEGUI::Window")
        local clickArgs = CEGUI.WindowEventArgs(button)
        button:fireEvent("Clicked", clickArgs)
    end

    function connectToFirstServer(args)
        if (runOnce) then return end
        local list = CEGUI.toMultiColumnList(CEGUI.toWindowEventArgs(args).window)
        if (list:getRowCount() < 1) then
            do return end
        end
        local at = CEGUI.MCLGridRef(0, 0)
        local first = list:getItemAtGridReference(at)
        if (first) then
            list:setItemSelectState(first, true)
            local join = root:getChild("WindowRoot_ServerBrowser/Button_Join")
            if (join) then
                clickButton(join)
                runOnce = true
            end
        end
    end

    function updatedLobby(args)
        print("inside updatedLobby()")
        text = root:getChild("WindowRoot_Lobby/Window_Chat/Editbox_TextWindow"):getText()
        first = text:find("ready")
        if first and text:find("ready", first + 1) then
            clickButton(root:getChild("WindowRoot_Lobby/Button_Start"))
        end

        if not wroteReady then
            print("clicking ready")
            root:getChild("WindowRoot_Lobby/Window_Chat/Editbox_TextInput"):setText("ready")
            text = root:getChild("WindowRoot_Lobby/Window_Chat/Editbox_TextInput"):getText()
            clickButton(root:getChild("WindowRoot_Lobby/Window_Chat/Button_EnterChatMessage"))
            clickButton(root:getChild("WindowRoot_Lobby/Button_Ready"))
            wroteReady = true
        end
        return false
    end

    function windowCreated(args)
        print("inside windowCreated")
        if runOnce2 then return false end
        args = tolua.cast(args,"CEGUI::WindowEventArgs")
        if not args.window:isPropertyPresent("Name") then return end
        if args.window:getProperty("Name") == "WindowRoot_Lobby" then
            args.window:subscribeEvent("Updated", "updatedLobby")
            runOnce2 = true
        end
        return false
    end

    -- hook window creation to get a signal if the lobby is created
    CEGUI.WindowManager:getSingleton():subscribeEvent("WindowCreated", "windowCreated")

    -- listen for new servers to connect to
    root:getChild("WindowRoot_ServerBrowser/MultiColumnList_ServerList"):subscribeEvent("ListContentsChanged", "connectToFirstServer")

    print("gui events hooked")
end

-- print("test.lua completey loaded")

