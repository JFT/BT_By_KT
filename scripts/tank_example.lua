function MovementSystem_handleEvents(eventID, event)
    print("in 'handleEvent'")
    if not type(eventID) == "number" then
        print("ERROR: type(eventID) != number!!!")
        EventHandler.emit(TIRNETIRANE)
    end
    print("eventID = " .. eventID)

    if eventID == 0 then
        print("got TESTEVENT type " .. type(event) .. " -> printing vector")
        print(vector3dfToString(event.vec))
    elseif eventID == 1 then
        print("got TESTEVENT_OTHER -> printing float")
        print(event.targetQueue)
    elseif eventID == 2 then
        print("got bladiblastr -> printing int")
        print(event.targetQueue)
--        print("causing crash to stop the program")
--        EventHandler.emit(TIRNETIRANE)
    end
end

function vector3dfToString(v)
    return "Vector3df[" .. v.X .. ", " .. v.Y .. ", " .. v.Z .. "]"
end


function WaypointReached(entityID, movementComponent)
    --print("getting target queue")
    targetQueue = movementComponent.targetQueue
    --print("got target queue")

    --TODO: assert(not targetQueue.empty())

    -- remove the last target
    targetQueue:pop()

    if targetQueue:empty() then
        --print("empty target queue -> this waypoint was the final one")
        eventManager:emit_FinalWaypointReached(Events_FinalWaypointReached:new(entityID))
    else
        --print("reached intermedate target")
        eventManager:emit_IntermediateWaypointReached(Events_IntermediateWaypointReached:new(entityID))
    end
end


function MovementSystem_update(entityID, positionHandle, movementComponentHandle)
    deltaTime = 0.2
    --TODO: find a way to syncronize components between the game and the scripts.
    -- maybe by replacing strings inside the scripts before compiling? Would make the lua footprint smaller..
    -- or create a special function for each component in the script?
    --print("Inside the MovementSystem_update script!")
    --print("using invalid call to crash :-)")
    --currently using invalid call to stop the program
    --EventHandler.emit(TargetReached, entityID)
    --print("entityid is")
    --print(entityID)
    --print("entityManager at")
    --print(entityManager)
    --print("handleManager at")
    --print(handleManager)
    --print("resolving functions at (floatPtr, floatValue, vector3dfPtr, vector3dfValue, queueVector3dfPtr, queueVector3dfValue, movementComponentValue, movementComponentPtr)")
    --print(handleManager.getAsFloatPtr)
    --print(handleManager.getAsFloatValue)
    --print(handleManager.getAsVector3dfPtr)
    --print(handleManager.getAsVector3dfValue)
    --print(handleManager.getAsQueueVector3dfPtr)
    --print(handleManager.getAsQueueVector3dfValue)
    --print(handleManager.getAsMovementComponentPtr)
    --print(handleManager.getAsMovementComponentValue)

    -- check if all used classes are defined in lua
    if handleManager == nil then
        print("ERROR: 'handleManager' is nil -> wasn't defined in lua")
        -- use invalid call to provoke a crash
        handleManager.isValid(0)
    end
    if entityManager == nil then
        print("ERROR: 'entityManager' is nil -> wasn't defined in lua")
        handleManager.isValid(0)
    end
    if eventManager == nil then
        print("ERROR: 'eventManager' is nil -> wasn't defined in lua")
        handleManager.isValid(0)
    end
    if terrainNode == nil then
        print("ERROR: 'terrainNode' is nil -> wasn't defined in lua")
        handleManager.isValid(0)
    end
    if grid == nil then
        print("ERROR: 'grid' is nil -> wasn't defined in lua")
        handleManager.isValid(0)
    end

    --print("getting movementComponent data from handle id = " .. movementComponentHandle)

    if not handleManager:isValid(positionHandle) then
        print("ERROR: INVALID POSITION HANDLE " .. positionHandle .. "! when updating entityID " .. entityID)
        print("using invalid call to crash :-)")
        --currently cause a crash
        handleManager.isValid(positionHandle)
        do return end
    end
    if not handleManager:isValid(movementComponentHandle) then
        print("ERROR: INVALID MOVEMENTCOMPONENT HANDLE " .. movementComponentHandle.. "! when updating entityID " .. entityID)
        print("using invalid call to crash :-)")
        --currently cause a crash
        handleManager.isValid(movementComponentHandle)
        do return end
    end

    movementComponent = handleManager:getAsMovementComponentPtr(movementComponentHandle) -- Ptr to be able to modify the target, targetQueue

    --print("getting target queue")
    targetQueue = movementComponent.targetQueue

    -- does this entity need to move?
    if not targetQueue:empty() then
        --print("getting t_pos from handle id = " .. positionHandle)
        t_pos = handleManager:getAsVector3dfPtr(positionHandle) -- Ptr to be able to set the position
        --print("got t_pos" .. vector3dfToString(t_pos) .. " getting t_target from movementComponent")

        t_target = targetQueue:front()
        -- targets are randomly set without paying attention to the map height -> fix that here
        --print("got t_target " .. vector3dfToString(t_target))
        --print("calculated distanceSQ = " .. distanceSQ)
        --print("getting speed")
        speed = movementComponent.speed
        --print("got speed = " .. speed)

        oldCellID = grid:getCellID(t_pos)

        t_target.Y = terrainNode:getHeight(t_target.X, t_target.Z)

        distanceSQ = t_pos:getDistanceFromSQ(t_target)

        --print("tank_example script movement update: entityID " .. entityID .. " position: " .. vector3dfToString(t_pos) .. " target: " .. vector3dfToString(t_target) .. " -> distance^2 = " .. distanceSQ .. " speed = " .. speed)

        if (distanceSQ - speed*speed * deltaTime*deltaTime) < 0 then -- if the next step would move the object beyond the target: move it directly to the target (using all squared numbers because the distance is squared)
            t_pos:setBy(t_target) -- using t_pos = t_target overwrites the lua value without changing the c++ value.
            WaypointReached(entityID, movementComponent)
            eventManager:emit_PositionChanged(Events_PositionChanged:new(entityID))
            --print("testing target setting by getting the value again (using movementComponentHandle)")
            --print("t_target = " .. vector3dfToString(handleManager:getAsMovementComponentValue(movementComponentHandle).target))
        else -- the entity won't reach the waypoint in this step
            direction = (t_target - t_pos):normalize();
            t_target = t_pos + direction * speed * deltaTime
            t_target.Y = terrainNode:getHeight(t_target.X, t_target.Z)
            t_pos:setBy(t_target) -- using t_pos = newPos overwrites the lua value without changing the c++ value.
            --print("new t_pos is " .. vector3dfToString(t_pos) .. " throwing 'PositionChanged' event")
            eventManager:emit_PositionChanged(Events_PositionChanged:new(entityID))
            --print("testing by getting the value again (using positionHandle)")
            --print("t_pos = " .. vector3dfToString(handleManager:getAsVector3dfValue(positionHandle)))
        end

        newCellID = grid:getCellID(t_pos)
        if newCellID ~= oldCellID then
            --print("entity " .. entityID .. " changed gridCell from " .. oldCellID .. " -> " .. newCellID)
            eventManager:emit_GridCellChanged(Events_GridCellChanged:new(entityID, oldCellID, newCellID))
        end
    else
        print("empty targetQueue for entity " .. entityID)
        print("maybe optimize by not updating if queue is empty?")
    end
end


function Sydney_update(tank,deltaTime)
    --we keep the tank pos stuff global inside the interpreter and see what happens 
  speed = 1000.0
  --print("muchtext")
  t_pos = tank:getPosition()
  t_target = tank:getTarget()
  distance = t_pos:getDistanceFromSQ(t_target)  
  if distance> 5.0 then
    if(distance-speed*speed*deltaTime*deltaTime)<0 then
      tank:setPosition(t_target)
    else
      tank:setPosition(t_pos+direction*speed*deltaTime)
    end
  else
    tank:nextTarget()
  end
end
