# BattleTanks Standalone

A Standalone Version of the Warcraft 3 Map: BattleTanks   

Warcraft 3 Map: https://classic.btanks.net    
Main language used : C++(14)   
Scriptinglanguage: lua(v5+)    

## Build Status
[![Build Status Linux](http://37.120.163.58:8080/buildStatus/icon?job=bt_by_kt?style=plastic)](http://37.120.163.58:8080/job/bt_by_kt/)
[![Build Status Windows](https://ci.appveyor.com/api/projects/status/1pr6tobfc8ako3jj/branch/master?svg=true&passingText=Windows%20-%20OK&failingText=Windows%20-%20Fail)](https://ci.appveyor.com/project/JFT/bt-by-kt/branch/master)

## External dependencies:
- Multithreading: stl threading capabilities introduced by c++11
- 3D Rendering, I/O: Irrlicht (https://gitlab.com/JFT/Irrlicht_extended)
- GUI: CEGUI (http://cegui.org.uk/)
- Audio: SFML (https://www.sfml-dev.org/)
- Networking: RakNet (http://www.jenkinssoftware.com/index.html)
- Scripting: Lua (https://www.lua.org/home.html)
- Scriptbinding : Sol2 (https://github.com/ThePhD/sol2)

## Documentation
- http://JFT.gitlab.io/BTKT_Docu

## Quickstart
Extract the contents of the 'dep-package.7z' archive into the main folder.

Now BattleTanks should be ready to be build and debugged.

## Building all Dependencies

All external header files should be copied or linked to `include/xxxxx/` (replace `xxxxx` with `irrlicht`, `raknet`, `...`, all lowercase) with the .h files directly in folder `xxxxx`

## Irrlicht_Extended Building
Pull the Irrlicht_extended repository and change into the directory `./source/Irrlicht/`. 
In this folder you'll find a makefile, a codeblocks-project file for MinGW-Building and VisualStudio projectfiles.
Of those use the buildsystem you need for your configuration.

## CEGUI Building
To build CEGUI please visit: http://static.cegui.org.uk/docs/0.8.7/compiling.html
You'll need cmake to build CEGUI.
After successfully compiling CEGUI put libCEGUIBase-0.a and libCEGUIIrrlichtRenderer-0.a in the lib folder of your working directory. Also create a symbolic link to 'yourceguidirectory\cegui\include\CEGUI' in the include folder named 'CEGUI'. Then the project should build just fine.
- You need to download and build the dependencies (expat, silly, freetype, irrlicht (the last two are needed anyway for other stuff))
- be sure to use the cmakegui to enable the dependencies (expat, silly, freetype, irrlicht). Then link their libraries and include folders in their respective fields in cmakegui.

## Possible issues on different Platforms:
- For MinGW visit http://cegui.org.uk/wiki/How_to_install_CEGUI_0.8.4_With_MinGW for some source code changes. If you get an error with expat linking, you need to change "LIBRARY libexpat" in the def file, just like you did with pngwin.

### VisualStudio:
- Because we use written logical operators (e.g. `and` instead of `&&`) add `/FI "iso646.h"` to the compiler commandline
- Also add warning 4996 to the list of ignored warnings (thrown because of deprecated irrlicht functions)

#### RakNet Issues
- Add `ws2_32.lib` as additional dependency
- Include the source (meaning all `.h` and `.cpp` files) of RakNet in your Project so that some RakNet stuff will be build by your compiler. RakNet does some very intense ifdefing which apparently means some files have to be build project-specific.
- Manually define `_WINSOCK_DEPRECATED_NO_WARNINGS`
- Manually define `_CRT_SECURE_NO_WARNINGS`
- Manually define `NOMINMAX` (otherwise windows kills std::min() calls because it defines a min() macro)

##### Linking error with CMake generated files
(Error occured on VisualStudio Community 2015 and Cmake 3.4.1, other versions may be affected)

- Use changes from https://github.com/jaynus/RakNet/commit/c9ddb6a651655c28cae326107877dc0fe1f4f384:

In File `Lib/LibStatic/CMakeLists.txt:`

```diff
@@ -17,7 +17,7 @@ IF(WIN32 AND NOT UNIX)
IF(NOT ${CMAKE_GENERATOR} STREQUAL "MSYS Makefiles")

-		IF( MSVC10 OR MSVC11 OR MSVC12 )
+		IF( MSVC10 OR MSVC11 OR MSVC12 OR MSVC14)
			set_target_properties(RakNetLibStatic PROPERTIES STATIC_LIBRARY_FLAGS "/NODEFAULTLIB:\"LIBCD.lib LIBCMTD.lib MSVCRT.lib\"" )
        ELSE()
        	set_target_properties(RakNetLibStatic PROPERTIES STATIC_LIBRARY_FLAGS "/NODEFAULTLIB:&quot;LIBCD.lib LIBCMTD.lib MSVCRT.lib&quot;" )'''
```
And:
`Source/CCRakNetSlidingWindow.cpp:`
```diff
@@ -22,7 +22,7 @@ static const CCTimeType SYN=10000;
	#include "MTUSize.h"
    #include <stdio.h>
-	#include <math.h>
+	#include <cmath>
	#include <stdlib.h>
    #include "RakAssert.h"
    #include "RakAlloca.h"

@@ -218,7 +218,7 @@ void CCRakNetSlidingWindow::OnAck(CCTimeType curTime, CCTimeTypee rtt, bool hasBA
		double d = .05;
    	double difference = rtt - estimatedRTT;
    	stimatedRTT = estimatedRTT + d * difference;
-   	deviationRtt = deviationRtt + d * (abs(difference) - deviationRtt);
+   	deviationRtt = deviationRtt + d * (std::abs(difference) - deviationRtt);
    }

    _isContinuousSend=isContinuousSend;
``

### Codeblocks + MinGW
- Use [mingw-w64](http://mingw-w64.org) instead of the default MinGW because it has better c++11 support.
- mingw-w64 version confirmend working: i686-5.3.0-posix-dwarf-rt4-rev0 other versions should work, too
- the `pythonXX.zip` for the embedded python might be needed to allow debugging using gdb (search python downloads for `Windows x86 embeddable zip file`). Just place it inside the top project folder

#### RakNet issues
RakNet needs some adjusting to be build with mingw-w64.
- Replace `SET( CMAKE_CXX_FLAGS "/D WIN32 /D _RAKNET_DLL /D _CRT_NONSTDC_NO_DEPRECATE /D _CRT_SECURE_NO_DEPRECATE /GS- /GR- ")` with `SET( CMAKE_CXX_FLAGS "-D WIN32 -D _RAKNET_LIB -D _CRT_NONSTDC_NO_DEPRECATE -D _CRT_SECURE_NO_DEPRECATE -fno-rtti ")` in files ` Lib/DLL/CMakeLists.txt` and `/Lib/LibStatic/CMakeLists.txt`
- Also replace `set_target_properties(RakNetLibStatic PROPERTIES STATIC_LIBRARY_FLAGS "/NODEFAULTLIB:&quot;LIBCD.lib LIBCMTD.lib MSVCRT.lib&quot;" )` with `set_target_properties(RakNetLibStatic PROPERTIES STATIC_LIBRARY_FLAGS "" )` in `Lib/LibStatic/CMakeLists.txt`

###### RakNet induced error in `winnt.h`
- Apply the following patch in file `Source/DR_SHA1.h`
```diff
#else
#ifndef TCHAR
-#define TCHAR char
+//#define TCHAR char
#endif
```
- Then replace every `TCHAR` with `char` in `Source/DR_SHA1.h` and `Source/DR_SHA1.cpp`

###### Possible error in RakNet's `BitStream.h`
(Error occured on one specific computer with C::B 13.12 + MinGW g++ 4.8.1-4, other versions may be affected)

If you get the following error in RakNet's `BitStream.h`:
`error: there are no arguments to _copysign' that depend on a template parameter, so a declaration of [...]`
- Replace all offending `_copysign` with `copysign` and recompile (yes changing RakNet code might be bad but this fixed the error for me)

## Building tiles with blender
Blender uses a right-hand coordinate system with the Z-axis being up. Irrlicht uses a left-hand coordinate system with the Y-axis being up.
In order to be able to use tiles created with Blender in the game you need to export them as Wavefront (.obj) files with the following settings
- Forward: Z Forward
- Up: Y Up
