#!/bin/sh

while read line; do
{
    DO_PRINT=1
    echo "$line" | grep -q -E '^vertexdecl ' && DO_PRINT=0
    echo "$line" | grep -q -E '.*[[:space:]]*attr' && DO_PRINT=0
    echo "$line" | grep -q -E '\([[:digit:]]+\):[[:space:]]BGFX[[:space:]]*' && DO_PRINT=0

    if [ "x$DO_PRINT" = "x1" ]; then
        echo "$line"
    fi
}
done

echo "$(basename ""$0""): end of input"
