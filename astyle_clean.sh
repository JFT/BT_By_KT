SRCPATH="$1"

[ -n "$SRCPATH" ] || echo "SRCPATH=$SRCPATH is empty -> exiting"
[ -n "$SRCPATH" ] || exit 1

[ -n "`find -iname "*.cpp.orig" | grep -ZzE '^\./src/.*\.cpp\.orig'`" ] && rm `find -iname "*.cpp.orig" | grep -ZzE '^\./src/.*\.cpp\.orig'`

[ -n "`find -iname "*.h.orig" | grep -ZzE '^\./src/.*\.h\.orig'`" ] && rm `find -iname "*.h.orig" | grep -ZzE '^\./src/.*\.h\.orig'`

exit 0
