import io
import os
import sys

debugActive = True
debugActive = False


def debugPrint(*args):
    global debugActive
    if debugActive:
        print("Debug:", *args)
    else:
        return


RED='\033[0;31m'
GREY='\033[0;37m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

lineColors = [
GREY,
'\033[0;32m',
'\033[0;30m',
'\033[0;33m',
'\033[0;34m',
'\033[0;35m',
RED,
CYAN]

nextLineColor = 0

outputSimple = False
output_short_filenames = True
outputColor = True
outputLineNrs = True
outputLineLength = None
cmdline_threads = None
cmdline_with_args = False
cmdline_callpath = ""


def color(s, color):
    if outputColor:
        return color + str(s) + NC
    return s


##
# @brief strip string s up to (including) the substring x
# @param s
# @param x
# @return a substring of s beginning behind x. if s doesn't contain x returns s unmodified
def stripIncluding(s, x):
    if s.find(x) != -1:
        return s[s.find(x) + len(x):]
    return s


def shortenFilename(fileName):
    lineNr = ""
    if fileName.find(":") != -1:
        lineNr = ":" + fileName[fileName.find(":") + 1:]
        fileName = fileName[:fileName.find(":")]
    #print("input =", fileName)
    if fileName.endswith(".so") or fileName.endswith(".dll") or fileName.endswith(".S") or fileName.find(".so.") != -1:
        # definetly an external libary
        return os.path.basename(fileName) + lineNr
    if os.path.isabs(fileName):
        # 3 possibilities
        # 1) this is a single filename (this seems to happen e.g. with CBgfxDriver.cpp)
        # 2) this is an absolute path to a file of our source
        # 3) this is an absolute path to a file NOT part of our source (e.g. /home/user/programs/cegui/...)

        # 1) can be easily tested: the file won't exist if that is the case
        if not os.path.exists(fileName): # file without path not part of our source
            return os.path.basename(fileName) + lineNr
        # 2) and 3) are harder to differentiate: need to test if they point to this directory or not
        curdir = os.path.abspath(os.path.curdir)
        absFilePath = os.path.abspath(fileName)
        commonPath = os.path.commonpath([absFilePath, curdir])
        if curdir == commonPath:
            # this file is part of our source but it might be inside our 'include' directory
            relFilePath = stripIncluding(absFilePath, commonPath)
            if relFilePath.find("include/") != -1:
                return os.path.basename(relFilePath) + lineNr
            if relFilePath.startswith('/src/'):
                relFilePath = relFilePath[len('/src/'):]
            return relFilePath + lineNr
        else:
            return os.path.basename(fileName) + lineNr
    return os.path.basename(fileName) + lineNr


def getColorForFileName(fileName):
    if fileName.find(":") != -1:
        fileName = fileName[:fileName.find(":")]  # need to strip of line nr because otherwise os.path.exists() won't work
    if fileName.endswith(".so") or fileName.endswith(".dll") or fileName.endswith(".S") or fileName.find(".so.") != -1:
        # definetly an external libary
        return GREY
    if os.path.isabs(fileName):
        # 3 possibilities
        # 1) this is a single filename (this seems to happen e.g. with CBgfxDriver.cpp)
        # 2) this is an absolute path to a file of our source
        # 3) this is an absolute path to a file NOT part of our source (e.g. /home/user/programs/cegui/...)

        # 1) can be easily tested: the file won't exist if that is the case
        if not os.path.exists(fileName):  # file without path not part of our source
            return GREY
        # 2) and 3) are harder to differentiate: need to test if they point to this directory or not
        curdir = os.path.abspath(os.path.curdir)
        absFilePath = os.path.abspath(fileName)
        commonPath = os.path.commonpath([absFilePath, curdir])
        if curdir == commonPath:
            # this file is part of our source but it might be inside our 'include' directory
            relFilePath = stripIncluding(absFilePath, commonPath)
            if relFilePath.find("include/") != -1:
                return GREY
            return CYAN
    return GREY


def getFileName(line):
    debugPrint("\n\ngetFileName with line", repr(line))
    fileName = ""
    fileNameColor = ""

    fileNameStartIndex = -1
    if line.rfind(" at ") != -1:
        fileNameStartIndex = line.rfind(" at ") + len(" at ")
    elif line.rfind(" from ") != -1:
        fileNameStartIndex = line.rfind(" from ") + len(" from ")

    if fileNameStartIndex != -1:
        fileName = line[fileNameStartIndex:]
        fileNameColor = getColorForFileName(fileName)
        if output_short_filenames:
            fileName = shortenFilename(fileName)
        #fileName, fileNameColor = colorFilenames(line[fileNameStartIndex:])
        fileName = color("[" + fileName + "]", fileNameColor)
        line = line[:fileNameStartIndex]  # stripping the filename
        # strip now trailing ' at ' or ' from '
        if line.endswith(" at "):
            line = line[:line.rfind(" at ")]
        if line.endswith(" from "):
            line = line[:line.rfind(" from ")]
        debugPrint("line after stripping the filename (=" + repr(fileName) + ") and ' at ' or ' from ' line = ", repr(line))
    else:  # no filename given -> use empty '[]'
        fileName = "[]"

    debugPrint("getFileName() returning (", repr(line), "\n\n", repr(fileName), "COLOR)")
    return line, fileName, fileNameColor


def cutStrings(line):
    debugPrint("cutString from line", repr(line))
    # if there are no unescaped '"' only return the line
    if line.replace('\\"', "ESC").find('"') == -1:
        debugPrint('no unescaped " in', repr(line))
        return line
    # replace out escaped string markers (and don't change original just yet)
    line = line.replace('\\"', "ESC")

    startIndex = line.find('"')
    endIndex = line.find('"', startIndex + 1)
    while startIndex != -1 and endIndex != -1:
        debugPrint("removing string at", startIndex, endIndex, repr(line[startIndex:endIndex]))
        # cut out the string
        line = line[:startIndex] + line[endIndex + 1]
        startIndex = line.find('"')
        endIndex = line.find('"', startIndex + 1)
    return line


def findEndMarker(line, startIndex, beginMarker, endMarker):
    debugPrint("findEndMarker() for", beginMarker, startIndex, endMarker, "line =", repr(line))
    debugPrint("from: ", line[startIndex:])
    checkIndex = startIndex

    while line[checkIndex] != endMarker and checkIndex < len(line):
        if line[checkIndex] == beginMarker:
            # if there is another beginning continue the search after the end of that one
            checkIndex = findEndMarker(line, checkIndex + 1, beginMarker, endMarker) + 1
        else:
            checkIndex += 1
    debugPrint("findEndMarker() returning", checkIndex, "for startIndex", startIndex)
    return checkIndex


def collapsToLevel(line, startIndex, endIndex, beginMarker, endMarker, level):
    debugPrint("collapsToLevel() for", beginMarker, startIndex, endIndex, endMarker, "level =", level)
    debugPrint("inbetween: ", repr(line[startIndex:endIndex]))

    checkIndex = startIndex

    # recursion end
    if level < 0:
        return '[...]'

    retVal = ''
    while checkIndex < endIndex:
        if line[checkIndex] == beginMarker:
            subEndIndex = findEndMarker(line, checkIndex + 1, beginMarker, endMarker)
            retVal += beginMarker + collapsToLevel(line, checkIndex + 1, subEndIndex, beginMarker, endMarker, level - 1) + endMarker
            checkIndex = subEndIndex + 1
        else:
            retVal += line[checkIndex]
            checkIndex += 1
    debugPrint("collapsToLevel()", beginMarker, startIndex, endIndex, endMarker, "returning", repr(retVal))
    return retVal


def stripCallArguments(line):
    # replace arguments inside function calls
    # find the first argument
    firstEqualIndex = line.find("=")
    # find the last argument
    lastEqualIndex = line.rfind("=")
    debugPrint("equal signs at indices:", firstEqualIndex, lastEqualIndex)

    if firstEqualIndex != -1:
        # left of the first equal sign there is an argument name which starts at a '('
        argumentsStartIndex = line.rfind("(", 0, firstEqualIndex)
        # right of the last equal sign the arguments end at a ')'
        argumentsEndIndex = line.find(")", lastEqualIndex)
        if argumentsStartIndex != -1 and argumentsEndIndex != -1:
            # remove all arguments but leave the '()'
            arguments = line[argumentsStartIndex - 1: argumentsEndIndex]
            line = line[:argumentsStartIndex + 1] + line[argumentsEndIndex:]
            debugPrint("removed arguments", repr(arguments), "remaining =", repr(line))
    return line


def getFunctionName(line):
    debugPrint("getFunctionName() from line", repr(line))
    # input looks like this
    #
    # in bgfx::Context::apiSemWait (_msecs=-1, this=0x7f79e64b7040)
    # in __new_sem_wait (sem=sem@entry=0x7f79e64b70c0)
    # in int sol::detail::static_trampoline<&(int sol::usertype_metatable<Events::PositionChanged, std::integer_sequence<unsigned long, 0ul, 1ul, 2ul>, char const (&) [4], sol::constructor_list<sol::types<>, sol::types<unsigned long> >, char const (&) [9], unsigned long Events::PositionChanged::*, char const (&) [5], sol::destructor_wrapper<void> const&>::real_call<1ul, true, false>(lua_State*))>(lua_State*) ()
    # in GraphicSystem::handleReceivedEvents() ()

    functionName = ""
    # sometimes there is an ' in ' followed by the function name
    if line.startswith(" in "):
        line = line[line.find(" in ") + len(" in "):]
        debugPrint("found ' in ' and replaced new line =", repr(line))

    if outputSimple:
        line = cutStrings(line)

    # the function name + arguments ends at least before the last space
    index = line.rfind(" ")
    if index == -1:
        # no space -> return the full line just to be save
        return line, line

    # the functions often end with 'arguments) ()' -> get rid of the extra ()
    if line.endswith(") ()"):
        line = line[:len(line) - len(" ()")]
        debugPrint("stripped trailing () new line =", repr(line))

    if not cmdline_with_args:
        line = stripCallArguments(line)

    # now 'line' might end in ' ()' -> get rid of the space
    if line.endswith(" ()"):
        line = line[:len(line) - len(" ()")] + "()"
        debugPrint("removed space before trailing ' ()'")

    if outputLineLength is not None and len(line) > outputLineLength:  # function name is very long ... remove some of it
        debugPrint("function name", repr(line), "> " + str(outputLineLength) + " chars long -> trying to collaps templates")
        # 1st method: replace template stuff
        level = 0
        while len(collapsToLevel(line, 0, len(line), "<", ">", level + 1)) <= outputLineLength:
            level += 1
        collapsed = collapsToLevel(line, 0, len(line), "<", ">", level)
        if len(collapsed) > outputLineLength:
            debugPrint("collapsing templates didn't save enough space -> removing some text '[...]'")
            line = line[:40] + "[...]" + line[-43:]
        else:
            line = collapsed

    functionName = line.strip()

    debugPrint("getFunctionName() returning", repr(line), repr(functionName))
    return line, functionName


def getFunctionAndFile(line):
    debugPrint("\ninput line =", repr(line))
    # possible outputs of gdb:
    #
    #  #2  0x0000564b053881dc in ExampleState::update(Game*) ()
    #  #1  0x00007f79ec0bd59c in std::condition_variable::wait(std::unique_lock<std::mutex>&) () from /usr/lib/x86_64-linux-gnu/libstdc++.so.6
    #  #2  0x0000564b05225ead in std::thread::_State_impl<std::_Bind_simple<ThreadPool::ThreadPool(unsigned long)::{lambda()#1} ()> >::_M_run() ()
    #  #3  0x00007f79ec0c2eff in ?? () from /usr/lib/x86_64-linux-gnu/libstdc++.so.6
    #  #4  0x00007f79ed70e424 in start_thread (arg=0x7f79c5ffb700) at pthread_create.c:333
    #  #5  0x00007f79eb6349bf in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:105
    #  #0  pthread_cond_wait@@GLIBC_2.3.2 () at ../sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
    #  #0  0x00007f79ed716536 in futex_abstimed_wait_cancelable (private=0, abstime=0x0, expected=0, futex_word=0x7f79e64b70c0) at ../sysdeps/unix/sysv/linux/futex-internal.h:205
    #  #1  do_futex_wait (sem=sem@entry=0x7f79e64b70c0, abstime=0x0) at sem_waitcommon.c:111
    #  #2  0x0000564b05346d1d in int sol::detail::static_trampoline<&(int sol::usertype_metatable<Events::PositionChanged, std::integer_sequence<unsigned long, 0ul, 1ul, 2ul>, char const (&) [4], sol::constructor_list<sol::types<>, sol::types<unsigned long> >, char const (&) [9], unsigned long Events::PositionChanged::*, char const (&) [5], sol::destructor_wrapper<void> const&>::real_call<1ul, true, false>(lua_State*))>(lua_State*) ()
    #

    function = ""
    fileNameColor = ""

    # remove out the beginning '#X'
    if not line.startswith('#'):
        print("ERROR: line ", repr(line), "doesn't start with a '#'")
    firstSpace = line.find(" ")
    line = line[firstSpace:].strip()
    debugPrint("after splitting of beginning '#X' =", repr(line))

    # now should follow a 0x... or directly the function name
    if line.startswith("0x"):
        line = line[line.find(" "):]  # not removing the space on purpose to differentiate between the ' in ' inserted by gdb and a function which might be named 'in'
        debugPrint("after splitting of '0x...'", repr(line))

    # time to find the filename. It should be located after ' at ' or after ' from '
    line, fileName, fileNameColor = getFileName(line)

    # now only the function name should remain in this string
    # but gdb often puts an extra part of parentheses in e.g. ExampleState::update(Game*) ()
    if line.endswith(")()"):
        line = line[:len(line) - len("()")]
        debugPrint("found ')()' at the end and stripped '()'. line =", repr(line))
    # this might also be ') const()'
    if line.endswith(") const()"):
        line = line[:len(line) - len("() const")]
        debugPrint("found ') const()' at the end and stripped '()'. line =", repr(line))

    # whatever is left should be the function name
    line, function = getFunctionName(line)

    debugPrint("getFunctionAndFile() returning (", repr(function), repr(fileName), "COLOR)\n")
    return (function, fileName, fileNameColor)


def colorFunctionName(function, fileName):
    functionSplit = function.split("::")
    function = ""
    for j in range(len(functionSplit)):
        if j == len(functionSplit) - 1:
            if functionSplit[j] == "??" or fileName.find(GREY) != -1:
                function += functionSplit[j]
            else:
                function += color(functionSplit[j], RED)
        else:
            function += functionSplit[j] + "::"
    return function


def stripLineNumber(fileName):
    # to find the filename and line replace away the colors
    fileNameCopy = fileName.replace(GREY, "")
    fileNameCopy = fileNameCopy.replace(NC, "")
    for i in range(len(fileNameCopy) - 1, 0, -1):
        if fileNameCopy[i] == ":":
            extension = fileNameCopy[i:]
            # don't replace the closing bracket
            extension = extension[:-1]
            return fileName.replace(extension, "")
    return fileName


def getBTList(lines):
    retVal = []
    lastFile = None
    last_used_color = None
    for i in range(len(lines)):
        line = lines[i]
        if not line.startswith("#"): continue
        # sometimes there are lots of calls inside of unknown functions inside libraries -> collapse them together
        function, fileName, used_color = getFunctionAndFile(line)
        if function == "??" and lastFile == fileName:  # only do that if the function is unknown. Otherwise recursive calls will be masked
            continue
        if outputSimple:  # only catch the first call into external functions
            if used_color == GREY:
                if lastFile is not None and last_used_color == GREY:
                    continue
        # strip away the line number
        if not outputLineNrs:
            fileName = stripLineNumber(fileName)
        # color in only the function itself and not the namespace
        function = colorFunctionName(function, fileName)
        retVal.append((function, fileName))
        lastFile = fileName
        last_used_color = used_color
    return retVal


def makeSingleLineFromBackTrace(lines):
    retVal = ""
    for i in range(len(lines)):
        function, fileName = lines[i]
        retVal += function + " " + fileName
        if i < len(lines) - 1: retVal += " - "
    return retVal


def sortToThreads(lines):
    currentThread = None
    currentBTLines = []
    threadLines = {}
    for line in lines:
        # interrupting the program makes gdb print out that it received "SIGINT"
        if line.startswith("Thread ") and line.find('received signal SIGINT') == -1:
            # update the old thread if there was one
            if currentThread is not None:
                threadLines[currentThread].append(currentBTLines)
            currentThread = line
            currentBTLines = []
            continue
        if not line.startswith("#"):
            continue
        if threadLines.get(currentThread) is None:
            threadLines[currentThread] = []
        currentBTLines.append(line)
    return threadLines


def countOccurences(lines):
    counts = {}
    for line in lines:
        if counts.get(line) is None:
            counts[line] = 0
        counts[line] += 1
    return counts


def printNode(name, node, threadMaxCounts, prePrint=""):
    """recursively print nodes inside a tree
        prePrint: what to print before handling the subTrees? (used to pass down tree lines)
    """

    # assume that our parent already printed everything ending with a ' - ' which we can expand upon
    # and gave us the correct 'prePrint' to go before all our children

    children = sorted(list(filter(lambda x: x != 'counts', node.keys())), key=lambda x: node[x]['counts'])
    children.reverse()

    global nextLineColor
    thisLineColor = lineColors[nextLineColor]
    nextLineColor = (nextLineColor + 1) % len(lineColors)
    if name is not None:  # don't display name of root node
        # do we have any children?
        if len(children) > 1:
            print(color(".", thisLineColor), end='')
        else:
            print("-", end='')
        function, fileName = name
        if outputSimple:
            # print time spent with enough accuracy
            numEnd = 0
            for i in range(3, -1, -1):  # use a maximum of 3 digits after the floating point  (ending range at -1 because this makes it stop at 0)
                numEnd = i
                percentString = ("{:." + str(i) + "f}").format(100 * float(node['counts']) / threadMaxCounts)
                if not percentString.endswith("0"):
                    break
            print("", ("{:." + str(numEnd) + "f}").format(100 * float(node['counts']) / threadMaxCounts) + ' %', function, fileName)
        else:
            print("", node['counts'], function, fileName)

    # print all children
    for k in range(len(children)):
        key = children[k]
        if key == "counts":  # no subtree on a 'counts' key
            continue

        # prepare the first line for all children
        nextPrePrint = prePrint
        print(prePrint, end='')
        if k == len(children) - 1:  # last child (don't forget the 'counts' child!)
            print("`-", end='')
            nextPrePrint = prePrint + "  "
        elif k == 0:  # first child
            print(color("+", thisLineColor) + "-", end='')
            nextPrePrint = prePrint + color("|", thisLineColor) + " "
        else:
            print(color("+", thisLineColor) + "-", end='')
            nextPrePrint = prePrint + color("|", thisLineColor) + " "

        # recurse down to our children
        printNode(key, node[key], threadMaxCounts, nextPrePrint)


def print_usage():
    print("usage:")
    print("interpretProfile.py <X.data> OPTIONS")
    print("options can be one of")
    for x in ["--nocolor", "--simple", "--nolines", "--lineLength=LINELENGTH", "--thread=THREAD", "--args", "--no-short-filenames"]:
        print(x)
    print("--simple implies --nolines and sets lineLength to 200 if not otherwise specified")
    print("argument LINELENGTH must be a positive integer")
    print("argument THREAD can be a string (e.g. 'Thread 3') or an integer or a comma seperated list of strings and/or integers")
    print("if --args is passed the arguments functions where called with won't be cleared")


def bottomUpView():
    for thread, bts in sorted(threadLines.items(), key=lambda x: x[0]):
        if not should_display_thread(thread):
            continue

        print(thread)
        print("")
        flat = list(x for y in bts for x in y)
        filtered = [getFunctionAndFile(l) for l in flat]
        # sort by occurences
        ocDict = {}
        for function, fileName, fileNameColor in filtered:
            if not outputLineNrs:
                fileName = stripLineNumber(fileName)
            function = colorFunctionName(function, fileName)
            key = function + ' ' + fileName
            if ocDict.get(key) is None:
                ocDict[key] = 1
            else:
                ocDict[key] += 1

        maxCount = len(str(max(ocDict.values())))
        sortedKeys = sorted(ocDict.items(), key=lambda t: t[1])
        sortedKeys.reverse()

        for key, count in sortedKeys:
            print(str(count).rjust(maxCount) + ":", key)

        print("")


def should_display_thread(thread_string):
    if cmdline_threads is None:
        return True
    return any([thread_string.lower().find(t.lower()) != -1 for t in cmdline_threads])


if __name__ != "__main__":
    quit()


# main program

if len(sys.argv) < 2:
    print("ERROR: Wrong number of arguments!")
    print_usage()
    sys.exit(1)

cmdline_callpath = sys.argv[0]

# get filename to load
fileToLoad = None
for arg in sys.argv[1:]:  # 0-th argument is always the path to this script
    if arg == "--nocolor" or arg == "--nocolors" or arg == "-nocolor" or arg == "-nocolors":
        outputColor = False
    elif arg == "--no-short-filenames" or arg == "--no-short-filename":
        output_short_filenames = False
    elif arg == "--simple" or arg == "-simple":
        outputSimple = True
        outputLineNrs = False
        if outputLineLength is None:
            outputLineLength = 200
    elif arg == "--nolines" or arg == "--noLines" or arg == "-nolines" or arg == "-noLines":
        outputLineNrs = False
    elif arg == "--args":
        cmdline_with_args = True
    elif arg.find("--lineLength=") != -1 or arg.find("--linelength=") != -1:
        outputLineLength = int(arg[len("--lineLength="):])
    elif arg.find("--thread=") != -1:
        cmdline_threads = []
        for thread_input in arg[len("--thread="):].split(","):
            try:
                thread_int = int(thread_input)
                cmdline_threads.append("thread " + str(thread_int))
            except ValueError:
                cmdline_threads.append(thread_input)
    else:
        # already found a file to load?
        if fileToLoad is None:
            fileToLoad = arg
        else:
            print("ERROR: unknown argument", repr(arg))
            print_usage()
            sys.exit(2)

f = io.open(fileToLoad)
lines = f.readlines()
f.close()
# our analysis function only saves old data if a new 'Thread' line is found but the data file doesn't end in one -> append it forcefully
lines.append("Thread\n")
lines = [l.replace("\n", "") for l in lines]

threadLines = sortToThreads(lines)

bottomUpView()

for thread, bts in sorted(threadLines.items(), key=lambda x: x[0]):
    if not should_display_thread(thread):
        continue

    # use connected dicts to store callgraphs and times called
    callGraph = {}

    threadMaxCounts = 1

    print(thread + "\n")
    for i in range(len(bts)):
        # data is generated with the last function call first but we want the first function call first
        bts[i].reverse()
        bts[i] = getBTList(bts[i])
        # start callGraph at first element
        treeNode = callGraph
        for call in bts[i]:
            if treeNode.get(call) is None:
                treeNode[call] = {'counts': 1}
            else:
                treeNode[call]['counts'] = treeNode[call]['counts'] + 1
                if treeNode[call]['counts'] > threadMaxCounts:
                    threadMaxCounts = treeNode[call]['counts']
            # traverse the tree
            treeNode = treeNode[call]

    printNode(None, callGraph, threadMaxCounts, "")
    print("")
    print("")
