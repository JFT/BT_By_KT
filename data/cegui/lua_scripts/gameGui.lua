--[[
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2018 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- make a table called GameGui
GameGui = {}

GameGui.create = function()
    GameGui.layoutFile = "gameGui.layout"
    GameGui.root = nil


    --- Script entry point
    local guiSystem = CEGUI.System:getSingleton()
    local schemeMgr = CEGUI.SchemeManager:getSingleton()
    local winMgr = CEGUI.WindowManager:getSingleton()

    GameGui.root = winMgr:loadLayoutFromFile(GameGui.layoutFile)
    guiSystem:getDefaultGUIContext():getRootWindow():addChild(GameGui.root)

    return GameGui
end

GameGui.delete = function()
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():removeChild(GameGui.root)
    GameGui = nil
end
