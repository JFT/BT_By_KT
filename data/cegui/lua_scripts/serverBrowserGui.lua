--[[
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2018 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- make a table called ServerBrowserGui
ServerBrowserGui = {}

local createPasswordDialog = function()
    local PasswordDialog = {}

    PasswordDialog = {}
    PasswordDialog.State = "empty"

    PasswordDialog.onAskForPassword = function(args)
        ServerBrowserGui.PasswordDialog.DialogBox:getChild("StaticText_ServerPassword"):setText("Enter server Password")
        ServerBrowserGui.PasswordDialog.DialogBox:getChild("Editbox_ServerPassword"):setText("")
        ServerBrowserGui.PasswordDialog.DialogBox:moveToFront()
        ServerBrowserGui.PasswordDialog.DialogBox:setVisible(true)
        ServerBrowserGui.PasswordDialog.DialogBox:setModalState(true)
        ServerBrowserGui.PasswordDialog.DialogBox:getChild("Editbox_ServerPassword"):activate()
        ServerBrowserGui.PasswordDialog.State = "asking"

        return true
    end

    PasswordDialog.hide = function()
        ServerBrowserGui.PasswordDialog.State = "empty"
        ServerBrowserGui.PasswordDialog.DialogBox:setVisible(false)
        ServerBrowserGui.PasswordDialog.DialogBox:setModalState(false)
    end


    PasswordDialog.onButtonBackClicked = function(args)
        ServerBrowserGui.PasswordDialog.hide()
        return true
    end

    PasswordDialog.handleKeypress = function(args)
        if CEGUI.toKeyEventArgs(args).scancode == CEGUI.Key.Return and ServerBrowserGui.PasswordDialog.State == "asking" then
            -- allow using 'enter' to accept a password even if the input box isn't focused
            ServerBrowserGui.PasswordDialog.onServerPasswordEntered()
            return true
        end
        return false
    end

    PasswordDialog.onServerPasswordEntered = function()
        if ServerBrowserGui.PasswordDialog.State ~= "asking" then
            return
        end
        password = ServerBrowserGui.PasswordDialog.DialogBox:getChild("Editbox_ServerPassword"):getText()

        if password ~= "" then
            ServerBrowserGui.PasswordDialog.State = "connecting"
            ServerBrowserGui.PasswordDialog.DialogBox:getChild("StaticText_ServerPassword"):setText("Connecting ...")
            ServerBrowserGui.connectToSelectedServer(password)
            --animMgr = CEGUI.AnimationManager:getSingleton()
            -- 'loadAnimationsFromXML' must be moved to somewhere where it is only called once
            --animMgr:loadAnimationsFromXML("GameMenu.anims")
            --ServerBrowserGui.PasswordDialog.anim = animMgr:getAnimation("StartButtonPulsating")
            --ServerBrowserGui.PasswordDialog.instance = animMgr:instantiateAnimation(ServerBrowserGui.PasswordDialog.anim)
            --ServerBrowserGui.PasswordDialog.instance:setTargetWindow(passwordDialog)
            --ServerBrowserGui.PasswordDialog.instance:start()
        end
    end

    PasswordDialog.onServerPasswordRejected = function(id)
        if ServerBrowserGui.PasswordDialog.State ~= "connecting" then
            return
        end

        -- are we actually trying to join this server or is this
        -- a very delayed answer to an old join request?
        firstSelectedItem = ServerBrowserGui.serverTable:getFirstSelectedItem()
        if firstSelectedItem then
            if firstSelectedItem:getID() == id then
                ServerBrowserGui.PasswordDialog.State = "asking"
                ServerBrowserGui.PasswordDialog.DialogBox:getChild("StaticText_ServerPassword"):setText("Wrong password!")
                -- set focus to the password box
                ServerBrowserGui.PasswordDialog.DialogBox:getChild("Editbox_ServerPassword"):activate()
            --ServerBrowserGui.PasswordDialog.instance:stop()
            --delete necessary? ServerBrowserGui.PasswordDialog.instance = nil
            end
        end
    end

    return PasswordDialog
end

ServerBrowserGui.create = function()
    ServerBrowserGui.layoutFile = "serverBrowserGui.layout"
    ServerBrowserGui.root = nil
    ServerBrowserGui.serverTable = nil
    ServerBrowserGui.windowCreateServer = nil
    ServerBrowserGui.PasswordDialog = nil

    local guiSystem = CEGUI.System:getSingleton()
    local schemeMgr = CEGUI.SchemeManager:getSingleton()
    local winMgr = CEGUI.WindowManager:getSingleton()

    ServerBrowserGui.root = winMgr:loadLayoutFromFile(ServerBrowserGui.layoutFile)
    guiSystem:getDefaultGUIContext():getRootWindow():addChild(ServerBrowserGui.root)

    ServerBrowserGui.serverTable =
        CEGUI.toMultiColumnList(ServerBrowserGui.root:getChild("MultiColumnList_ServerList"))

    ServerBrowserGui.columnIds = {}
    ServerBrowserGui.columnIds.name = 0
    ServerBrowserGui.columnIds.password = 1
    ServerBrowserGui.columnIds.slots = 2
    ServerBrowserGui.columnIds.ping = 3
    ServerBrowserGui.serverTable:addColumn("Server Name", ServerBrowserGui.columnIds.name, CEGUI.UDim(0.45, 0))
    ServerBrowserGui.serverTable:addColumn("Password", ServerBrowserGui.columnIds.password, CEGUI.UDim(0.10, 0))
    ServerBrowserGui.serverTable:addColumn("Slots", ServerBrowserGui.columnIds.slots, CEGUI.UDim(0.30, 0))
    ServerBrowserGui.serverTable:addColumn("Ping", ServerBrowserGui.columnIds.ping, CEGUI.UDim(0.15, 0))

    ServerBrowserGui.windowCreateServer = ServerBrowserGui.root:getChild("Window_CreateServer")


    -- methods to join a selected server
    ServerBrowserGui.root:subscribeEvent("KeyDown", "ServerBrowserGui.handleKeypress")
    ServerBrowserGui.root:getChild("Button_Join"):subscribeEvent("Clicked", "ServerBrowserGui.handle_button_join")

    --[[
    -- Server password entry box
    --]]
    ServerBrowserGui.PasswordDialog = createPasswordDialog()

    -- password dialog box
    ServerBrowserGui.PasswordDialog.DialogBox = ServerBrowserGui.root:getChild("Window_EnterServerPassword")
    -- event handling for the password dialog box
    ServerBrowserGui.PasswordDialog.DialogBox:subscribeEvent("AskForPassword", "ServerBrowserGui.PasswordDialog.onAskForPassword")
    ServerBrowserGui.PasswordDialog.DialogBox:getChild("Button_Back"):subscribeEvent("Clicked", "ServerBrowserGui.PasswordDialog.onButtonBackClicked")
    -- ways to accept the password:
    -- press enter in the input box or the dialog itself
    ServerBrowserGui.PasswordDialog.DialogBox:subscribeEvent("KeyDown", "ServerBrowserGui.PasswordDialog.handleKeypress")
    ServerBrowserGui.PasswordDialog.DialogBox:getChild("Editbox_ServerPassword"):subscribeEvent("TextAccepted", "ServerBrowserGui.PasswordDialog.onServerPasswordEntered")
    -- or the "enter" button
    ServerBrowserGui.PasswordDialog.DialogBox:getChild("Button_EnterServerPassword"):subscribeEvent("Clicked", "ServerBrowserGui.PasswordDialog.onServerPasswordEntered")


    ServerBrowserGui.root:getChild("Button_Back"):subscribeEvent("Clicked",
                                                                "ServerBrowserGui.handle_button_back")

    ServerBrowserGui.root:getChild("Button_CreateServer"):subscribeEvent("Clicked",
                                                                        "ServerBrowserGui.handle_button_createServer")

    ServerBrowserGui.windowCreateServer:getChild("Button_Back"):subscribeEvent("Clicked",
                                                                            "ServerBrowserGui.handle_button_createServerMenu_back")
    ServerBrowserGui.windowCreateServer:getChild("Button_CreateServer"):subscribeEvent("Clicked",
                                                    "ServerBrowserGui.handle_button_createServerMenu_createServer")
    ServerBrowserGui.windowCreateServer:getChild("Checkbox_ServerPasswordProtected")
        :subscribeEvent("SelectStateChanged",
                        "ServerBrowserGui.handle_checkbox_server_used_password")


    ServerBrowserGui.windowCreateServer:getChild("Checkbox_JoinServer")
        :subscribeEvent("SelectStateChanged", "ServerBrowserGui.handle_checkbox_joinServer")





    -- TODO handle/implement join by double-click selection
    -- serverTable:subscribeEvent("CEGUI::MultiColumnList::EventMouseDoubleClick,
    -- handleMultiColumnList_Mouse")
    -- serverTable:subscribeEvent("CEGUI::MultiColumnList::EventSelectionChanged,
    -- handleMultiColumnList_SelectionChanged")



    MessageBoxGui.subscribe_button_yes("ServerBrowserGui.handle_messageBox_button_Yes")
    MessageBoxGui.subscribe_button_no("ServerBrowserGui.handle_messageBox_button_No")

    return ServerBrowserGui
end

ServerBrowserGui.delete = function()
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():removeChild(ServerBrowserGui.root)
    ServerBrowserGui = nil
end

ServerBrowserGui.askClientForReconnect = function(args)

    MessageBoxGui.ask("Attention!", ".. Server detected, Try reconnect with Server?")
end

ServerBrowserGui.clearServerTable = function(args)
    ServerBrowserGui.serverTable:resetList()
end


local getColorForPing = function(ping)
    if not ping then
        return CEGUI.Colour(1.0, 0, 0, 1.0) -- red
    end

    if ping <= 15 then
        return CEGUI.Colour(0.0, 1.0, 0.0, 1.0) -- green
    elseif ping <= 30 then
        return CEGUI.Colour(1.0, 0.5, 0.0, 1.0)
    end

    return CEGUI.Colour(1.0, 0, 0, 1.0) -- red
end

ServerBrowserGui.onServerPingChanged = function(id, ping)
    rowIndex = ServerBrowserGui.serverTable:getRowWithID(id);

    local getColumn = function(column)
        return ServerBrowserGui.serverTable:getItemAtGridReference(CEGUI.MCLGridRef(rowIndex, column))
    end

    if ping then
        getColumn(3):setText(ping)
    else
        getColumn(3):setText("-")
    end
    getColumn(3):setTextColours(getColorForPing(ping))

    ServerBrowserGui.serverTable:handleUpdatedItemData()
end

ServerBrowserGui.onServerChanged = function(id, filledSlots, maxSlots, passwordProtected)
    rowIndex = ServerBrowserGui.serverTable:getRowWithID(id);

    password = "No"
    if passwordProtected then
        password = "Yes"
    end

    local getColumn = function(column)
        return ServerBrowserGui.serverTable:getItemAtGridReference(CEGUI.MCLGridRef(rowIndex, column))
    end

    getColumn(1):setText(password)

    slotInfo = "(" .. tostring(filledSlots) .. "/" .. tostring(maxSlots) .. ")"
    getColumn(2):setText(slotInfo)

    ServerBrowserGui.serverTable:handleUpdatedItemData()
end

ServerBrowserGui.onServerAdded = function(id, name, address, filledSlots, maxSlots, passwordProtected)
    local row = ServerBrowserGui.serverTable:addRow()
    ServerBrowserGui.serverTable:setRowID(row, id)

    local blackTextItem = function(str)
        local item = CEGUI.createListboxTextItem(str);
        item:setTextColours(CEGUI.Colour(0, 0, 0, 1.0)) -- black
        item:setSelectionBrushImage("BT/GenericBrush")
        return item
    end

    -- initialize all columns empty
    for colName, colId in pairs(ServerBrowserGui.columnIds) do
        ServerBrowserGui.serverTable:setItem(blackTextItem(""), colId, row)
    end

    -- set the name only here, no support for changing servernames
    ServerBrowserGui.serverTable:setItem(blackTextItem(name .. " - " .. address), ServerBrowserGui.columnIds.name, row)

    ServerBrowserGui.onServerChanged(id, filledSlots, maxSlots, passwordProtected)
    ServerBrowserGui.onServerPingChanged(id, nil)
end

ServerBrowserGui.onServerRemoved = function(id)
    rowIndex = ServerBrowserGui.serverTable:getRowWithID(id);

    ServerBrowserGui.serverTable:removeRow(rowIndex)

    ServerBrowserGui.serverTable:handleUpdatedItemData()
end

ServerBrowserGui.handle_button_back = function(args)
	print("handle button_back")
    game:changeState(game:findState("Menu"))
    print("done")
    return true
end

ServerBrowserGui.connectToSelectedServer = function(password)
    firstSelectedItem = ServerBrowserGui.serverTable:getFirstSelectedItem()

    if firstSelectedItem then
        if password == nil then
            password = ""
        end

        ServerBrowserGui.onJoinRequest(firstSelectedItem:getID(), password)
    end
end

ServerBrowserGui.handle_button_join = function(args)
    ServerBrowserGui.connectToSelectedServer()
    return true
end

ServerBrowserGui.handleKeypress = function(args)
    if CEGUI.toKeyEventArgs(args).scancode == CEGUI.Key.Return then
        if ServerBrowserGui.PasswordDialog.DialogBox:isVisible() then
            return ServerBrowserGui.PasswordDialog.handleKeypress(args)
        else
            ServerBrowserGui.connectToSelectedServer()
        end
        return true
    end
    return false
end

ServerBrowserGui.handle_button_createServer = function(args)
    print("handle button_createServer")
	ServerBrowserGui.windowCreateServer:moveToFront()
    ServerBrowserGui.windowCreateServer:setVisible(true)
    return true
end

ServerBrowserGui.handle_button_createServerMenu_back = function(args)
    print("handle button_createServerMenu_back")
	--ServerBrowserGui.setServerSettingsFromConfig()
	ServerBrowserGui.windowCreateServer:setVisible(false)
    return true
end

ServerBrowserGui.handle_button_createServerMenu_createServer = function(args)
    print("handle button_createServerMenu_createServer")

	config:getModifiableSettings().serverName = ServerBrowserGui.windowCreateServer:getChild("Editbox_ServerName"):getText()
	local serverPort = config.defaultSettings.serverPort
	if (false)
        --StringWParser::parse(
		--	ServerBrowserGui.windowCreateServer:getChild("Editbox_ServerPort"):getText(),
		--	serverPort,
		--	game->getConfiguration().defaultSettings.serverPort) != ErrCodes::NO_ERR)
	then
		--Error::errContinue(
		--	"couldn't parse server port from",
		--	ServerBrowserGui.windowCreateServer:getChild("Editbox_ServerPort"):getText(),
		--	"using default of",
		--	config:getSettings().serverPort)
	else
		config:getModifiableSettings().serverPort = serverPort
	end

	local serverIsPasswordProtected = CEGUI.toToggleButton(
            ServerBrowserGui.windowCreateServer:getChild("Checkbox_ServerPasswordProtected")):isSelected()
	if (serverIsPasswordProtected) then
		config:getModifiableSettings().serverPassword =
			ServerBrowserGui.windowCreateServer:getChild("Editbox_ServerPassword"):getText()
	else
		config:getModifiableSettings().serverPassword = config.defaultSettings.serverPassword
	end

	-- TODO: maybe don't save the settings if they have only now changed or provide an extra 'save
	-- settings' button on server creation.
	-- Save Settings
	if (config:saveConfig(config_path, device)) then --TODO just save the server part
        print("Save config successful")
    else
        print("Cant save Settings to '" .. config_path .. "'")
    end


	--static PlatformIndependent::ProcessAbstraction serverProcess --TODO use
	--/*if (serverProcess.isRunning()) //TODO USE AGAIN IF NEEDED
	--{
	--	Error::errContinue("server already running!");
	--	//TODO: show a gui message that the server is already running
	--}
	--// TODO use "bin/server" (path "bin/makefile/serverDebug" just for testing)
	--else
    --if (!serverProcess.executeProcess("bin/makefile/serverDebug")) then --TODO use
	--	print("error executing server process!")
	--end
	-- TODO: wait for the server to exit at onLeave() of the state that uses this button.
	-- Need to think more about how to transfer the information about the process between
	-- gui and usingstate.
	-- serverProcess.waitForExit(); //code confirmed working on linux!

	ServerBrowserGui.windowCreateServer:setVisible(false)

    local checkbox_joinServer =
        CEGUI.toToggleButton(ServerBrowserGui.windowCreateServer:getChild("Checkbox_JoinServer"))
	--if (checkbox_joinServer:isSelected()) then --TODO use
		--RakNet::SystemAddress* systemAddressNewServer = new RakNet::SystemAddress(
		--	game->getNetwork()->getPeer()->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS).ToString(false),
		--	game->getConfiguration().getSettings().serverPort);
		--this->serverBrowserNetwork->tryToConnectToServer(
		--	*systemAddressNewServer, convertToString(game->getConfiguration().getSettings().serverPassword));
	--end

    return true
end

ServerBrowserGui.handle_checkbox_server_used_password = function(args)
    print("handle checkbox_server_used_password")

	local editbox_serverPassword = CEGUI.toEditbox(
		ServerBrowserGui.windowCreateServer:getChild("Editbox_ServerPassword"))
	local checkbox_serverPassword = CEGUI.toToggleButton(
			ServerBrowserGui.windowCreateServer:getChild("Checkbox_ServerPasswordProtected"))
	if (checkbox_serverPassword:isSelected()) then
		editbox_serverPassword:setVisible(true)
	else
		editbox_serverPassword:setVisible(false)
	end

    return true
end

ServerBrowserGui.handle_checkbox_joinServer = function(args)
    print("handle checkbox_joinServer")

	local button_createServer = ServerBrowserGui.windowCreateServer:getChild("Button_CreateServer")
    local checkbox_joinServer = CEGUI.toToggleButton(
			ServerBrowserGui.windowCreateServer:getChild("Checkbox_JoinServer"))
    if (checkbox_joinServer:isSelected()) then
        button_createServer:setText("Join")
    else
        button_createServer:setText("Create")
    end

    return true
end


--[[
-- Server password entry
--]]

ServerBrowserGui.handle_messageBox_button_Yes = function(args)
    print("handle MessageBox button_Yes")

    SettingMenuGui.root:getChild("Window_SettingMenuWindow"):setVisible(false)

    return true
end

ServerBrowserGui.handle_messageBox_button_No = function(args)
    print("handle MessageBox button_No")

    SettingMenuGui.root:getChild("Window_SettingMenuWindow"):setVisible(false)

    return true
end

