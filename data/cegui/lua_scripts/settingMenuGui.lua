--[[
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2018-2019 Florian Schulz, Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]
require("math")
require("utils")
log = require("scripts.core.log")

------------------------

local SettingMenuGui = {}
SettingMenuGui.tooManyVideoModes = 20

local config_path = "config/config.json"
local settingsTable = jsonStorage.loadTable(config_path)
local filterMinimumResolution = {Width = 800, Height = 600}

function checkConfig()
    if settingsTable == nil then
        settingsTable = {}
    end
    if settingsTable.audio == nil then
        settingsTable.audio = {effectsvolume = 100, globalvolume = 80, musicvolume = 100, 
                                soundenabled = false}
    end
    if settingsTable.player == nil then
        settingsTable.player = {color = {255, 0, 0, 255}, name = "player0"}
    end
    if settingsTable.server == nil then
        settingsTable.server = {name = "BattleTanks Server", numberOfSlots = 10, password = "",
                                port = 1234}
    end
    if settingsTable.video == nil then
        settingsTable.video = {antialias = 0, doublebuffer = true, driver = "bgfx_opengl", 
                                fullscreen = false, resolution = {800, 600}, stencilbuffer = false,
                                stereobuffer = false, videodepth = 24, vsync = false}
    end
end

SettingMenuGui.setAndAddItems = function()

   local resolutionOptionCombobox = CEGUI.toCombobox(SettingMenuGui.pageGraphics:getChild("Combobox_ResolutionOptions"))
   local videoModeList = device:getVideoModeList()

    --debugOutLevel(Debug::DebugLevels::firstOrderLoop, "found", modes->getVideoModeCount(), "possible video resolutions after filtering")

    -- listing all possible video modes fills the selection list with lots of useless modes (e.g.
    -- 640x360 16 bit) which should be filtered out
    -- but on some computers that might filter out all the possible video modes (e.g. when only 24
    -- bit video modes are available the filter can't be 32 bits)
    local minimumVideoDepth = videoModeList:getVideoModeDepth(0)
    local maximumVideoDepth = minimumVideoDepth
    local videoModeCount = videoModeList:getVideoModeCount()
    local videoDesktopDepth = videoModeList:getDesktopDepth()
    local currentScreenSize = driver:getScreenSize()
    local currentDesktopDepth = videoModeList:getDesktopDepth()

    for m = 0, videoModeCount - 1 do
            local videoDepth = videoModeList:getVideoModeDepth(m)
            local videoHeight = videoModeList:getVideoModeResolution(m).Height
            local videoWidth = videoModeList:getVideoModeResolution(m).Width
            if videoDepth >= videoDesktopDepth and 
               videoHeight >= filterMinimumResolution.Height and
               videoWidth >= filterMinimumResolution.Width then
                
                local resolutionString = tostring(videoWidth) .. 
                                        "x" .. tostring(videoHeight) ..
                                        " " .. tostring(videoDepth) .. " bit"
                
                local item = CEGUI.createListboxTextItem(resolutionString, m)
                resolutionOptionCombobox:addItem(item)

                if currentScreenSize.Height == videoHeight and 
                   currentScreenSize.Width == videoWidth and
                   currentDesktopDepth == videoDepth then
                    
                   resolutionOptionCombobox:setItemSelectState(item, true)
                end
            end
    end

    SettingMenuGui.setAttributesFromConfig()
end

SettingMenuGui.setAttributesFromConfig = function()
    --| --------------------- |--
    --| SET GENERAL ATTRIBUTES |--
    --| --------------------- |--

    -- Set PlayerName
    CEGUI.toEditbox(SettingMenuGui.pageGeneral:getChild("Editbox_PlayerName")):
        setText(tostring(settingsTable.player.name))

    --|------------------------|--
    --| SET GAMEPLAY ATTRIBUTES |--
    --| ---------------------- |--


    --|------------------------|--
    --| SET GRAPHICS ATTRIBUTES |--
    --|------------------------|--

    -- resolution and video depth selection already set in SettingMenuGui::setAndAddItems()
    -- Fullscreen Setting
    CEGUI.toToggleButton(SettingMenuGui.pageGraphics:getChild("Checkbox_FullScreen")):
        setSelected(config:getSettings().irrlichtParams.Fullscreen)
    --|---------------------|--
    --| SET AUDIO ATTRIBUTES |--
    --|---------------------|--

    CEGUI.toToggleButton(SettingMenuGui.pageSound:getChild("Checkbox_SoundEnabled")):
        setSelected(settingsTable.audio.soundenabled)
    CEGUI.toScrollbar(SettingMenuGui.pageSound:getChild("Scrollbar_GlobalVolume")):
        setScrollPosition(tonumber(settingsTable.audio.globalvolume))
    SettingMenuGui.pageSound:getChild("StaticText_GlobalValue"):
        setText(tostring(math.floor(settingsTable.audio.globalvolume)))
    
    CEGUI.toScrollbar(SettingMenuGui.pageSound:getChild("Scrollbar_MusicVolume")):
        setScrollPosition(tonumber(settingsTable.audio.musicvolume))
    SettingMenuGui.pageSound:getChild("StaticText_MusicValue"):
        setText(tostring(math.floor(settingsTable.audio.musicvolume)))
    
    CEGUI.toScrollbar(SettingMenuGui.pageSound:getChild("Scrollbar_EffectsVolume")):
        setScrollPosition(tonumber(settingsTable.audio.effectsvolume))
    SettingMenuGui.pageSound:getChild("StaticText_EffectsValue"):
        setText(tostring(math.floor(settingsTable.audio.effectsvolume)))
    
    SettingMenuGui.update_globalVolume()
    SettingMenuGui.update_effectsVolume()
    SettingMenuGui.update_musicVolume()
end

SettingMenuGui.saveAttributesInConfig = function()
    jsonStorage.saveTable(settingsTable, config_path)
    SettingMenuGui.unsavedSettings = false
end

SettingMenuGui.handle_fullscreenEnabled = function(args)
    settingsTable.video.fullscreen = CEGUI.toToggleButton(SettingMenuGui.pageGraphics:getChild("Checkbox_FullScreen")):isSelected()
end

SettingMenuGui.handle_resolutionSelection = function(args)
    local comboBox = SettingMenuGui.pageGraphics:getChild("Combobox_ResolutionOptions")
    comboBox = CEGUI.toCombobox(comboBox)
    local videoModeList = device:getVideoModeList()
    local res = videoModeList:getVideoModeResolution(comboBox:getSelectedItem():getID())
    settingsTable.video.resolution[1] = res.Width
    settingsTable.video.resolution[2] = res.Height   
end

SettingMenuGui.handle_soundEnabled = function(args)
    settingsTable.audio.soundenabled = CEGUI.toToggleButton(SettingMenuGui.pageSound:getChild("Checkbox_SoundEnabled")):isSelected()
    SettingMenuGui.update_globalVolume()
    SettingMenuGui.unsavedSettings = true
end

SettingMenuGui.handle_button_back = function(args)
    --log.trace("unsavedsettings", SettingMenuGui.unsavedSettings)
    if SettingMenuGui.unsavedSettings then
        MessageBoxGui.ask("Attention!",
                          "The unsaved settings will not be stored! Are you sure you want to quit without saving?")
    else
        SettingMenuGui.root:getChild("Window_SettingMenuWindow"):setModalState(false)
        SettingMenuGui.root:getChild("Window_SettingMenuWindow"):setVisible(false)
    end

    return true
end

SettingMenuGui.handle_button_save = function(args)
    SettingMenuGui.saveAttributesInConfig()    
    MessageBoxGui.inform("Attention!",
                         "Settings saved, please restart if not all settings took effect")
    
    return true
end

SettingMenuGui.update_globalVolume = function()
    
    local scrollPosition = CEGUI.toScrollbar(SettingMenuGui.pageSound:getChild("Scrollbar_GlobalVolume")):getScrollPosition()
    scrollPosition = utils.clamp(scrollPosition,0.0,100.0)
    SettingMenuGui.pageSound:getChild("StaticText_GlobalValue"):setText(tostring(math.floor(scrollPosition)))

    settingsTable.audio.globalvolume = math.floor(scrollPosition)

    if settingsTable.audio.soundenabled then
        game:getSoundEngine():setGlobalVolume(scrollPosition)
    else
        game:getSoundEngine():setGlobalVolume(0.0)
    end
end

SettingMenuGui.handle_globalScrollbar = function(args)
    --log.trace("handling..")
    SettingMenuGui.update_globalVolume()
    SettingMenuGui.unsavedSettings = true

    return true
end

SettingMenuGui.update_musicVolume = function()
    
    local scrollPosition = CEGUI.toScrollbar(SettingMenuGui.pageSound:getChild("Scrollbar_MusicVolume")):getScrollPosition()
    scrollPosition = utils.clamp(scrollPosition,0.0,100.0)
    SettingMenuGui.pageSound:getChild("StaticText_MusicValue"):setText(tostring(math.floor(scrollPosition)))

    settingsTable.audio.musicvolume = math.floor(scrollPosition)

    game:getSoundEngine():setMusicVolume(scrollPosition)

end

SettingMenuGui.handle_musicScrollbar = function(args)
    -- log.trace("handling..")
    SettingMenuGui.update_musicVolume()
    SettingMenuGui.unsavedSettings = true

    return true
end

SettingMenuGui.update_effectsVolume = function()
    
    local scrollPosition = CEGUI.toScrollbar(SettingMenuGui.pageSound:getChild("Scrollbar_EffectsVolume")):getScrollPosition()
    scrollPosition = utils.clamp(scrollPosition,0.0,100.0)
    SettingMenuGui.pageSound:getChild("StaticText_EffectsValue"):setText(tostring(math.floor(scrollPosition)))

    settingsTable.audio.effectsvolume = math.floor(scrollPosition)

    game:getSoundEngine():setEffectsVolume(scrollPosition)

end

SettingMenuGui.handle_effectsScrollbar = function (args)
    -- log.trace("handling..")
    SettingMenuGui.update_effectsVolume()
    SettingMenuGui.unsavedSettings = true

    return true
end

SettingMenuGui.handle_messageBox_button_Yes = function(args)

    SettingMenuGui.root:getChild("Window_SettingMenuWindow"):setVisible(false)

    return true
end

SettingMenuGui.handle_messageBox_button_No = function(args)

    SettingMenuGui.root:getChild("Window_SettingMenuWindow"):setModalState(true)

    return true
end

SettingMenuGui.handle_messageBox_button_Ok = function(args)

    SettingMenuGui.root:getChild("Window_SettingMenuWindow"):setVisible(false)

    return true
end

SettingMenuGui.create = function()
    --- Script entry point
    -- log.trace("start of init SettingMenuGui")
    checkConfig()
    SettingMenuGui.unsavedSettings = false

    local guiSystem = CEGUI.System:getSingleton()
    local schemeMgr = CEGUI.SchemeManager:getSingleton()
    local winMgr = CEGUI.WindowManager:getSingleton()

    SettingMenuGui.root = winMgr:loadLayoutFromFile("settingMenuGui.layout")

    local settingMenuPageGeneral = winMgr:loadLayoutFromFile("settingMenuPageGeneralGui.layout")
    SettingMenuGui.pageGeneral = settingMenuPageGeneral
    local settingMenuPageGameplay = winMgr:loadLayoutFromFile("settingMenuPageGameplayGui.layout")
    SettingMenuGui.pageGameplay = settingMenuPageGameplay
    local settingMenuPageGraphics = winMgr:loadLayoutFromFile("settingMenuPageGraphicsGui.layout")
    SettingMenuGui.pageGraphics = settingMenuPageGraphics
    local settingMenuPageSound = winMgr:loadLayoutFromFile("settingMenuPageSoundGui.layout")
    SettingMenuGui.pageSound = settingMenuPageSound

    local tabControl = CEGUI.toTabControl(SettingMenuGui.root:getChild("Window_SettingMenuWindow/TabControl"))

    tabControl:addTab(settingMenuPageGeneral)
    tabControl:addTab(settingMenuPageGameplay)
    tabControl:addTab(settingMenuPageGraphics)
    tabControl:addTab(settingMenuPageSound)

    SettingMenuGui.root:getChild("Window_SettingMenuWindow/Button_Back"):subscribeEvent("Clicked", "SettingMenuGui.handle_button_back")
    SettingMenuGui.root:getChild("Window_SettingMenuWindow/Button_Save"):subscribeEvent("Clicked", "SettingMenuGui.handle_button_save")


    SettingMenuGui.setAndAddItems()

    settingMenuPageGraphics:getChild("Checkbox_FullScreen"):subscribeEvent("SelectStateChanged", "SettingMenuGui.handle_fullscreenEnabled")
    
    settingMenuPageGraphics:getChild("Combobox_ResolutionOptions"):subscribeEvent("ListSelectionAccepted", "SettingMenuGui.handle_resolutionSelection")

    settingMenuPageSound:getChild("Checkbox_SoundEnabled"):subscribeEvent("SelectStateChanged", "SettingMenuGui.handle_soundEnabled")

    settingMenuPageSound:getChild("Scrollbar_GlobalVolume"):subscribeEvent("ScrollPositionChanged", "SettingMenuGui.handle_globalScrollbar")

    settingMenuPageSound:getChild("Scrollbar_MusicVolume"):subscribeEvent("ScrollPositionChanged", "SettingMenuGui.handle_musicScrollbar")

    settingMenuPageSound:getChild("Scrollbar_EffectsVolume"):subscribeEvent("ScrollPositionChanged", "SettingMenuGui.handle_effectsScrollbar")

    MessageBoxGui.subscribe_button_yes("SettingMenuGui.handle_messageBox_button_Yes")
    MessageBoxGui.subscribe_button_no("SettingMenuGui.handle_messageBox_button_No")
    MessageBoxGui.subscribe_button_ok("SettingMenuGui.handle_messageBox_button_Ok")
    -- log.trace("end of init SettingMenuGui")
    return SettingMenuGui
end

SettingMenuGui.delete = function()
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():removeChild(SettingMenuGui.root)
    SettingMenuGui.root = nil
end

return SettingMenuGui
