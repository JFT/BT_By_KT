--[[
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2019 Hannes Franke 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

Chat = {}

Chat.root = nil
Chat.chatNextIndexToAppend = 0
--- Script entry point
local guiSystem = CEGUI.System:getSingleton()
local schemeMgr = CEGUI.SchemeManager:getSingleton()
local winMgr = CEGUI.WindowManager:getSingleton()

Chat.root = LobbyGui.root:getChild("Window_Chat")

Chat.onConnectedToServer = function(serverName, nrOfSlots)
    Chat.root:getChild("Editbox_TextWindow"):setText(serverName .. ": Welcome\n")

    local textWindow = CEGUI.toMultiLineEditbox(Chat.root:getChild("Editbox_TextWindow"))
    local textWindowScrollbar = CEGUI.toScrollbar(textWindow:getVertScrollbar())

    textWindowScrollbar:setVerticalAlignment(CEGUI.VA_CENTRE)

    -- FIXME
    --[[ no 'getThumb()' in lua scrollbar. Find a different way to calculate thumb size to position the bar?
    textSize = textWindowScrollbar:getThumb():calculatePixelSize(false)
    textWindowScrollbar:setPosition(
        CEGUI.UVector2(textWindowScrollbar:getPosition().d_x - CEGUI.UDim(0, textSize.d_width / 2),
                        textWindowScrollbar:getPosition().d_y))
    --]]
    textWindowScrollbar:setEndLockEnabled(false)
end

Chat.onMessageReceived = function(string)
    local textWindow = CEGUI.toMultiLineEditbox(Chat.root:getChild("Editbox_TextWindow"))

    textWindow:appendText(string)

    local textWindowScrollbar = textWindow:getVertScrollbar()
    if textWindowScrollbar:isVisible() then
        textWindowScrollbar:setUnitIntervalScrollPosition(1.0)
    end
end

Chat.sendChatMessage = function()
    -- get message from box
    local message = Chat.root:getChild("Editbox_TextInput"):getText()
    -- clear input box
    Chat.root:getChild("Editbox_TextInput"):setText("")

    if message ~= "" then
        Chat.onSendChatMessage(message)
    end
end

Chat.handleKeypress = function(args)
    args = CEGUI.toKeyEventArgs(args)

    -- in case the editbox already has focus enable sending by pressing 'enter'
    if CEGUI.toEditbox(Chat.root:getChild("Editbox_TextInput")):hasInputFocus() then
        if args.scancode == CEGUI.Key.Return then
            Chat.sendChatMessage()
            return true
        end
    else
        -- when typing anywhere the chat input should receive focus (e.g. after clicking a button for a different slot)
        -- except if pressing CTRL, because this could be done for CTRL+C to copy stuff out of the chat history
        if args.scancode == CEGUI.Key.LeftControl or
           args.scancode == CEGUI.Key.RightControl or
           (args.sysKeys & CEGUI.SystemKeys.Control) ~= 0
           then
            return false
        end

        Chat.root:getChild("Editbox_TextInput"):activate()

        -- forward the keypress to the chat input, so the letter we typed immediately appears
        local newArgs = CEGUI.KeyEventArgs:new(args.window)
        newArgs.codepoint = args.codepoint
        newArgs.scancode = args.scancode
        newArgs.sysKeys = args.sysKeys
        Chat.root:getChild("Editbox_TextInput"):fireEvent("KeyDown", newArgs)
        return true
    end

    return false
end

Chat.root:getChild("Button_EnterChatMessage"):subscribeEvent("Clicked", Chat.sendChatMessage)
Chat.root:getChild("Button_EnterChatMessage"):subscribeEvent("Clicked", Chat.sendChatMessage)
Chat.root:getChild("Editbox_TextInput"):subscribeEvent("KeyDown", Chat.handleKeypress)

return Chat

