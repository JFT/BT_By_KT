--[[
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2018 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- make a table called MessageBoxGui
MessageBoxGui = {}

MessageBoxGui.create = function()
    MessageBoxGui.layoutFile = "messageBoxGui.layout"
    MessageBoxGui.root = nil

    MessageBoxGui.source = nil

    MessageBoxGui.buttonState_OK = 'OK'
    MessageBoxGui.buttonState_YESNO = 'YESNO'

    MessageBoxGui.button_subscribes_yes = nil
    MessageBoxGui.button_subscribes_no = nil
    MessageBoxGui.button_subscribes_ok = nil

    MessageBoxGui.root = CEGUI.WindowManager:getSingleton():loadLayoutFromFile(MessageBoxGui.layoutFile)
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():addChild(MessageBoxGui.root)

    MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_Yes"):subscribeEvent("Clicked", "MessageBoxGui.handle_button_default")
    MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_No"):subscribeEvent("Clicked", "MessageBoxGui.handle_button_default")
    MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_Ok"):subscribeEvent("Clicked", "MessageBoxGui.handle_button_default")

    return MessageBoxGui
end

MessageBoxGui.delete = function()
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():removeChild(MessageBoxGui.root)
    MessageBoxGui = nil
end

MessageBoxGui.subscribe_button_yes = function(function_str)
    print("subscribe_button_yes")
    if MessageBoxGui.button_subscribes_yes then
        MessageBoxGui.button_subscribes_yes:disconnect()
    end
    MessageBoxGui.button_subscribes_yes = MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_Yes"):subscribeEvent("Clicked", function_str)
end

MessageBoxGui.subscribe_button_no = function(function_str)
    print("subscribe_button_no")
    if MessageBoxGui.button_subscribes_no then
        MessageBoxGui.button_subscribes_no:disconnect()
    end
    MessageBoxGui.button_subscribes_no = MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_No"):subscribeEvent("Clicked", function_str)
end

MessageBoxGui.subscribe_button_ok = function(function_str)
    print("subscribe_button_ok")
    if MessageBoxGui.button_subscribes_ok then
        MessageBoxGui.button_subscribes_ok:disconnect()
    end
    MessageBoxGui.button_subscribes_ok = MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_Ok"):subscribeEvent("Clicked", function_str)
end

MessageBoxGui.inform = function(titlebarText, mainText, source)
    print("inform")
    MessageBoxGui.addText(titlebarText, mainText)
    MessageBoxGui.setButtonState(MessageBoxGui.buttonState_OK)
    MessageBoxGui.source = source

    MessageBoxGui.root:getChild("FrameWindow_MessageBox"):moveToFront()
    MessageBoxGui.root:getChild("FrameWindow_MessageBox"):setVisible(true)
    MessageBoxGui.root:getChild("FrameWindow_MessageBox"):setModalState(true)
end


MessageBoxGui.ask = function(titlebarText, mainText, source)
    print("ask")
    MessageBoxGui.addText(titlebarText, mainText)
    MessageBoxGui.setButtonState(MessageBoxGui.buttonState_YESNO)
    MessageBoxGui.source = source

    MessageBoxGui.root:getChild("FrameWindow_MessageBox"):moveToFront()
    MessageBoxGui.root:getChild("FrameWindow_MessageBox"):setVisible(true)
    MessageBoxGui.root:getChild("FrameWindow_MessageBox"):setModalState(true)
end

MessageBoxGui.addText = function(titlebarText, mainText)
    print("addText")
    MessageBoxGui.root:getChild("FrameWindow_MessageBox"):setText(titlebarText)
    MessageBoxGui.root:getChild("FrameWindow_MessageBox/StaticText_MainText"):setText(mainText)
end

MessageBoxGui.setButtonState = function(buttonState)
	print("setButtonState")
    if buttonState == MessageBoxGui.buttonState_OK then
        MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_Yes"):setVisible(false)
        MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_No"):setVisible(false)
        MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_Ok"):setVisible(true)

    elseif buttonState == MessageBoxGui.buttonState_YESNO then
        MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_Yes"):setVisible(true)
        MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_No"):setVisible(true)
        MessageBoxGui.root:getChild("FrameWindow_MessageBox/Button_Ok"):setVisible(false)

    else
        print("Unknown Button-state")
    end
end

MessageBoxGui.handle_button_default = function(args)
    MessageBoxGui.root:getChild("FrameWindow_MessageBox"):setModalState(false)
    MessageBoxGui.root:getChild("FrameWindow_MessageBox"):setVisible(false)
    return true;
end

