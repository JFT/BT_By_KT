--[[
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2018 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- make a table called TerminalGui

log = require("scripts.core.log")
repl = require("scripts.core.repl.console")
repl:loadplugin 'history'
repl:loadplugin 'completion'
repl:loadplugin 'autoreturn'

TerminalGui = {}

TerminalGui.create = function()
    TerminalGui.root = nil

    TerminalGui.console = nil
    TerminalGui.consoleScrollbar = nil
    TerminalGui.textInput = nil
    TerminalGui.completionText = nil

    TerminalGui.buttonLeftControlPressed_flag = false

    TerminalGui.commandTemp = ""
    TerminalGui.inputCommands = {}
    TerminalGui.correctCommands = {}
    TerminalGui.selectedText = ""
    TerminalGui.backSteps = 0
    TerminalGui.lastCompletionInput = nil

    TerminalGui.completionStartIdx = 0
    TerminalGui.completionCount = 0
    TerminalGui.completionViewCount = 0
    TerminalGui.showingAutoCompletion = false

    TerminalGui.root = CEGUI.WindowManager:getSingleton():loadLayoutFromFile("terminalGui.layout")
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():addChild(TerminalGui.root)

    TerminalGui.initialising()

    TerminalGui.textInput:subscribeEvent("KeyDown", "TerminalGui.handle_editbox_keyDown")
    TerminalGui.textInput:subscribeEvent("KeyUp", "TerminalGui.handle_editbox_keyUp")
    TerminalGui.console:subscribeEvent("Activated", "TerminalGui.handle_multiLineEditbox_consolSelection")

    return TerminalGui
end

TerminalGui.delete = function()
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():removeChild(TerminalGui.root)
    TerminalGui = nil
end

local function gather_results(success, ...)
  local n = select('#', ...)
  return success, { n = n, ... }
end


TerminalGui.initialising = function()
    log.trace("TerminalGui initializing")
    TerminalGui.console = CEGUI.toMultiLineEditbox(TerminalGui.root:getChild("MultiLineEditbox_Console"))
    TerminalGui.consoleScrollbar = TerminalGui.console:getVertScrollbar()
	TerminalGui.textInput = CEGUI.toEditbox(TerminalGui.root:getChild("Editbox_TextInput"))
    TerminalGui.completionText = TerminalGui.root:getChild("StaticText")
    TerminalGui.console:setText("\n \n \n \n\n")

    -- local textSize = TerminalGui.consoleScrollbar:getThumb():calculatePixelSize(false)
    TerminalGui.consoleScrollbar:setVerticalAlignment(CEGUI.VA_CENTRE)

    --TerminalGui.consoleScrollbar:setPosition(
    --    CEGUI.UVector2(TerminalGui.consoleScrollbar:getPosition().d_x - CEGUI.UDim(0, textSize.d_width / 2),
    --                   TerminalGui.consoleScrollbar:getPosition().d_y))
    TerminalGui.consoleScrollbar:setEndLockEnabled(false)
    TerminalGui.consoleScrollbar:setVisible(false)
end

TerminalGui.updateVisibility = function()
    if TerminalGui.root:isVisible() then
        log.trace("KEY F12 -> close Terminal")
        TerminalGui.root:setModalState(false)
        TerminalGui.root:setVisible(false)

    else
        log.trace("KEY F12 -> open Terminal")
        TerminalGui.root:moveToFront()
        TerminalGui.root:setVisible(true)
        TerminalGui.root:setModalState(true)
        TerminalGui.textInput:activate()
    end
end

TerminalGui.matchingStrings = function(str1, str2)
    local retstr = ""
    for i = 1, #str1 do
        local c1 = str1:sub(i,i)
        local c2 = ''
        if i < #str2 then
            c2 = str2:sub(i,i)
        end
        if c1 == c2 then
            retstr = retstr .. c1
        else
            return retstr
        end
    end
    return retstr
end

TerminalGui.handleCompletion = function()
    local results = ""
    local count = 0
    local viewCount = 0
    local completionString = ""
    local maxCount = 20

    local currentInput = TerminalGui.textInput:getText()

    local callBackFunc = function(res)
        
        if string.match(res:sub(1,4),"sol.") or
           string.match(res, "%ssol.") or string.match(res, "%psol.")
        then
            return
        end
        count = count + 1

        if count == 1 then
            completionString = res
        else
            completionString = TerminalGui.matchingStrings(completionString, res)
        end

        if count <= TerminalGui.completionStartIdx
           or (#results + #res + 4) > 500 then
            return
        end      

        if count - TerminalGui.completionStartIdx == 1 then
            results = results .. res
        else   
            results = results .. "    " .. res
        end
        viewCount = viewCount + 1        
    end
    
    if TerminalGui.lastCompletionInput == currentInput then
        if TerminalGui.completionCount == TerminalGui.completionViewCount then
            return
        end
        if TerminalGui.completionStartIdx >= TerminalGui.completionCount then
           TerminalGui.completionStartIdx = 0
        end
    else
        TerminalGui.completionStartIdx = 0
    end

    TerminalGui.lastCompletionInput = currentInput
    
    repl:complete(currentInput, callBackFunc)
    
    TerminalGui.completionCount = count
    TerminalGui.completionStartIdx = TerminalGui.completionStartIdx + viewCount
    TerminalGui.completionViewCount = viewCount
    
    if count == 1 then
        TerminalGui.textInput:setText(results)
        TerminalGui.textInput:setCaretIndex(string.len(TerminalGui.textInput:getText()))
        if TerminalGui.showingAutoCompletion then
            TerminalGui.clearCompletionText()
            TerminalGui.showingAutoCompletion = false
        end
    else
        if completionString ~= "" then
            TerminalGui.textInput:setText(completionString)
            TerminalGui.textInput:setCaretIndex(string.len(TerminalGui.textInput:getText()))
        end
        if count > maxCount then
            results = results .. "    (" .. (TerminalGui.completionStartIdx ) .."/".. count .. ")"
        end
        if not TerminalGui.showingAutoCompletion then
            TerminalGui.console:appendText("\n\n\n")

            if TerminalGui.consoleScrollbar:isVisible() then
                TerminalGui.consoleScrollbar:setUnitIntervalScrollPosition(1.0)
            end

            TerminalGui.showingAutoCompletion = true
        end
        TerminalGui.completionText:setText(results)
    end
end

TerminalGui.clearCompletionText = function()
    TerminalGui.completionText:setText("")
    TerminalGui.console:setText(TerminalGui.console:getText():sub(1,#(TerminalGui.console:getText()) - 3))
    if TerminalGui.consoleScrollbar:isVisible() then
        TerminalGui.consoleScrollbar:setUnitIntervalScrollPosition(1.0)
    end
end

TerminalGui.displayError = function(err)
    TerminalGui.addResult("Error: " .. err)
    log.error(err)
end

TerminalGui.displayResult = function(result)
    TerminalGui.addResult(result)
    log.trace("result: " .. tostring(result))
end


-- we will always call repl with the full input, never line-by line
function repl:detectcontinue(...)
    return false
end

function repl:displayresults(results)
    TerminalGui.displayResult(table.unpack(results, 1, results.n))
end

function repl:displayerror(err)
    TerminalGui.displayError(err)
end

local function gather_results(success, ...)
  local n = select('#', ...)
  return success, { n = n, ... }
end


TerminalGui.handleCommand = function()
    if TerminalGui.showingAutoCompletion then
        TerminalGui.clearCompletionText()
        TerminalGui.showingAutoCompletion = false
    end

    local newCommand = TerminalGui.textInput:getText() -- store command

    if newCommand == "" then
        return
    end

    log.trace("command " .. tostring(newCommand))
    TerminalGui.addCommand(newCommand)

    local commandFunction, err = load(newCommand) --load(table.concat(TerminalGui.correctCommands, "; ") .. "; " .. newCommand)

    if commandFunction == nil then
        -- try evaluating the expression itself, so an input of '40 + 20' will result in the output '60'
        -- this code is based on repl:handleline
        local commandFunction_, err_ = repl:compilechunk("return " .. newCommand)
        if commandFunction_ == nil then
            -- use the original compile error because also displaying the 'return' might confuse the user
            repl:displayerror(err)
        else
            local success, results = gather_results(xpcall(commandFunction_, function(...) return debug.traceback(...) end))
            if success then
              repl:displayresults(results)
            else
              repl:displayerror(results[1])
            end
        end
    elseif type(commandFunction) == "function" then
        TerminalGui.correctCommands[#TerminalGui.correctCommands+1] = newCommand
        repl:handleline(newCommand)
    end
    TerminalGui.textInput:setText("") -- reset the text
end

TerminalGui.inputError = function(errorString)
    TerminalGui.addResult(errorString)
end

TerminalGui.addCommand = function(command)

    TerminalGui.inputCommands[#TerminalGui.inputCommands+1] = command

    local lineIn = TerminalGui.getPrefixText(" In     ", #TerminalGui.inputCommands) .. command
    -- this->writeInLogFile(lineIn)

    TerminalGui.console:insertText(lineIn .. "\n", string.len(TerminalGui.console:getText()) - 7)

    if TerminalGui.consoleScrollbar:isVisible() then
        TerminalGui.consoleScrollbar:setUnitIntervalScrollPosition(1.0)
    end

    TerminalGui.backSteps = 0 -- reset previous backSteps
end

TerminalGui.addResult = function (result)
    if result == "" then
        return
    end

    local lineOut = TerminalGui.getPrefixText(" Out ", #TerminalGui.inputCommands) .. tostring(result)
    -- this->writeInLogFile(lineOut)

    TerminalGui.console:insertText(lineOut .. "\n\n", string.len(TerminalGui.console:getText()) - 7)

    if TerminalGui.consoleScrollbar:isVisible() then
        TerminalGui.consoleScrollbar:setUnitIntervalScrollPosition(1.0)
    end
end

TerminalGui.getPrefixText = function(mode, nr)
    local prefixText = mode .. "[" .. tostring(nr) .. "]: "
    return prefixText
end

TerminalGui.refreshCommandBox = function()
    if TerminalGui.backSteps > 0 then
		TerminalGui.textInput:setText(TerminalGui.inputCommands[1 + #TerminalGui.inputCommands - TerminalGui.backSteps])
    elseif TerminalGui.backSteps == 0 then
        TerminalGui.textInput:setText(TerminalGui.commandTemp)
    end
    TerminalGui.textInput:setCaretIndex(string.len(TerminalGui.textInput:getText()))

end

TerminalGui.handle_editbox_keyDown = function(args)
	--log.trace("handle button_editbox_keyDown" .. tostring(CEGUI.toKeyEventArgs(args).scancode))

    handleKeys = {
      [0x1D] = function() -- CEGUI.Key.Scan.LeftControl
        --log.trace("LeftControl release")
        TerminalGui.buttonLeftControlPressed_flag = false
        return true end,
    }

    if handleKeys[CEGUI.toKeyEventArgs(args).scancode] ~= nil then
        handleKeys[CEGUI.toKeyEventArgs(args).scancode]()
        return true
    end

    return false
end

TerminalGui.handle_editbox_keyUp = function(args)
	--log.trace("handle editbox_keyUp")

    handleKeys = {
    [0x1C] = function() -- CEGUI.Key.Scan.Return
      --log.trace("Return pressed")
      TerminalGui.handleCommand()
      return true end,
    [0xC8] = function() -- CEGUI.Key.Scan.ArrowUp
      --log.trace("ArrowUp pressed")
      if TerminalGui.backSteps == 0 then
          TerminalGui.commandTemp = TerminalGui.textInput:getText()
      end
      if TerminalGui.backSteps < #TerminalGui.inputCommands then
          TerminalGui.backSteps = TerminalGui.backSteps + 1
          TerminalGui.refreshCommandBox()
      end
      return true end,
    [0xD0] = function() -- CEGUI.Key.Scan.ArrowDown
      --log.trace("ArrowDown pressed")
      if TerminalGui.backSteps > 0 then
        TerminalGui.backSteps = TerminalGui.backSteps - 1
        TerminalGui.refreshCommandBox()
      end
      return true end,
    [0x1D] = function() -- CEGUI.Key.Scan.LeftControl
      --log.trace("LeftControl pressed")
      TerminalGui.buttonLeftControlPressed_flag = true
      return true end,
    [0x2E] = function() -- CEGUI.Key.Scan.C
      TerminalGui.selectedText = string.sub(TerminalGui.console:getText(),
                                            TerminalGui.console:getSelectionStartIndex()+1,
                                            TerminalGui.console:getSelectionStartIndex()+
                                            TerminalGui.console:getSelectionLength())
      return true end,
    [0x2F] = function() -- CEGUI.Key.Scan.V
      --log.trace("v pressed")
      local oldText = TerminalGui.textInput:getText()
      local beg = TerminalGui.textInput:getSelectionStartIndex()
      local len = TerminalGui.textInput:getSelectionLength()
      TerminalGui.textInput:setText(string.sub(oldText, 0, beg) .. string.sub(oldText, beg+len+1))
      TerminalGui.textInput:insertText(TerminalGui.selectedText, beg)
      TerminalGui.textInput:setCaretIndex(beg + string.len(TerminalGui.selectedText))
      return true end,
    [0x0F] = function() --CEGUI.Key.Scan.Tab
      --TerminalGui.addResult("Tab Pressed!")
      TerminalGui.handleCompletion()
      return end
    }

    if handleKeys[CEGUI.toKeyEventArgs(args).scancode] ~= nil then
        handleKeys[CEGUI.toKeyEventArgs(args).scancode]()
        return true
    end

    return false
end

TerminalGui.handle_multiLineEditbox_consolSelection = function(args)
	--log.trace("handle multiLineEditbox_consolSelection")
	TerminalGui.textInput:activate()
	return true
end

