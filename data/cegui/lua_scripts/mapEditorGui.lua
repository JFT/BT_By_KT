-- make a table called MapEditor
MapEditor = {}
MapEditor.root = nil
MapEditor.file_dialog = nil
MapEditor.clickSave = function(args)
	log.trace("You clicked on Save!!")
end

MapEditor.clickedLoadMap = function(args)
	log.trace("Calling loadmap for MapeditorState from Lua!")
	MapEditor.file_dialog:setVisible(true)
end

MapEditor.clickedQuit = function(args)
	local state = game:findState("Menu")
	game:changeState(state)
end

MapEditor.populateSettings = function(columnList)
	columnList:addColumn("Setting", 0, CEGUI.UDim(0.5,0))
	columnList:addColumn("Value", 1, CEGUI.UDim(0.5,0))
end


--- Script entry point
local guiSystem = CEGUI.System:getSingleton()
local schemeMgr = CEGUI.SchemeManager:getSingleton()
local winMgr = CEGUI.WindowManager:getSingleton()

local root = guiSystem:getDefaultGUIContext():getRootWindow():getChild("Mapeditor_root")

local leftTabTex = winMgr:loadLayoutFromFile("mapEditorTexturingLeft.layout")
local leftTabEntity = winMgr:loadLayoutFromFile("mapEditorEntityLeft.layout")

local rightTabEntity = winMgr:loadLayoutFromFile("mapEditorEntityRight.layout")
local rightSettings = winMgr:loadLayoutFromFile("mapEditorSettings.layout")

local tabControlLeft = CEGUI.toTabControl(root:getChild("Window_Left/TabControl")) --Note: we need to use toTabControl here to cast from CEGUI Window to TabControl
local tabControlRight = CEGUI.toTabControl(root:getChild("Window_Right/TabControl")) -- ceguis lua should offer conversions to all types

MapEditor.populateSettings(CEGUI.toMultiColumnList(rightSettings:getChild("MultiColumn_Settings")))

tabControlLeft:addTab(leftTabTex)
tabControlLeft:addTab(leftTabEntity)
tabControlRight:addTab(rightTabEntity)
tabControlRight:addTab(rightSettings)

MapEditor.file_dialog = root:getChild("Window_FileDialog")
root:getChild("Menubar/Menu/PopupMenu_Menu/Save"):subscribeEvent("Clicked", "MapEditor.clickSave")
root:getChild("Menubar/Menu/PopupMenu_Menu/Load"):subscribeEvent("Clicked", "MapEditor.clickedLoadMap")
root:getChild("Menubar/Menu/PopupMenu_Menu/Quit"):subscribeEvent("Clicked", "MapEditor.clickedQuit")
