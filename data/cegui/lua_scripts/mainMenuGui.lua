--[[
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2018 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

jsonStorage = require("jsonStorage")
-- make a table called MainMenuGui
MainMenuGui = {}

MainMenuGui.create = function()
    MainMenuGui.root = CEGUI.WindowManager:getSingleton():loadLayoutFromFile("mainMenuGui.layout")
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():addChild(MainMenuGui.root)

    MainMenuGui.root:getChild("Window_MainMenuWindow/Button_SettingMenu"):subscribeEvent("Clicked", "MainMenuGui.handle_button_settingMenu")
    MainMenuGui.root:getChild("Window_MainMenuWindow/Button_StartGame"):subscribeEvent("Clicked", "MainMenuGui.handle_button_startGame")
    MainMenuGui.root:getChild("Window_MainMenuWindow/Button_ExampleState"):subscribeEvent("Clicked", "MainMenuGui.handle_button_exampleState")
    MainMenuGui.root:getChild("Window_MainMenuWindow/Button_Quit"):subscribeEvent("Clicked", "MainMenuGui.handle_button_quit")

    -- load subGuiElements
    SettingMenuGui = require("data.cegui.lua_scripts.settingMenuGui")
    MainMenuGui.settingMenuGui = SettingMenuGui.create()
    MainMenuGui.root:addChild(MainMenuGui.settingMenuGui.root)

    game:getSoundEngine():playMusic("media/music/DDDDuell.flac")
end

MainMenuGui.delete = function()
    MainMenuGui.settingMenuGui.delete()
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():removeChild(MainMenuGui.root)
    MainMenuGui = nil
end

MainMenuGui.handle_button_startGame = function(args)
    print("handle button_startGame")
    game:changeState(game:findState("ServerBrowser"))
end

MainMenuGui.handle_button_settingMenu = function(args)
    print("handle button_settingMenu")
 MainMenuGui.settingMenuGui.root:getChild("Window_SettingMenuWindow"):moveToFront()
    MainMenuGui.settingMenuGui.root:getChild("Window_SettingMenuWindow"):setVisible(true)
    MainMenuGui.settingMenuGui.root:getChild("Window_SettingMenuWindow"):setModalState(true)
end

MainMenuGui.handle_button_exampleState = function(args)
    print("handle button_exampleState")
    game:changeState(game:findState("Example"))
end

MainMenuGui.handle_button_quit = function(args)
    print("handle button_quit")
    game:quit()
end

