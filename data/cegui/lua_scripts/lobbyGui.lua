--[[
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2018 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- helper function to forward CEGUI WindowEventArgs
local asNewWindowEventArgs = function(args)
    if type(args) == "CEGUI::WindowEventArgs" then
        return args
    end

    return CEGUI.WindowEventArgs:new(CEGUI.toWindowEventArgs(args).window)
end

-- make a table called LobbyGui
LobbyGui = {}

LobbyGui.create = function()
    LobbyGui.layoutFile = "lobbyGui.layout"
    LobbyGui.root = nil

    LobbyGui.root = CEGUI.WindowManager:getSingleton():loadLayoutFromFile(LobbyGui.layoutFile)
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():addChild(LobbyGui.root)

    LobbyGui.Chat = dofile (CEGUI.ScriptModule.ResourceGroupDirectory .. "lobby/chat.lua")

    LobbyGui.root:getChild("Button_Back"):subscribeEvent("Clicked", function(args) LobbyGui.root:fireEvent("DisconnectFromServer", asNewWindowEventArgs(args)) end)
    LobbyGui.root:getChild("Button_Ready"):subscribeEvent("Clicked", function(args) LobbyGui.root:fireEvent("Ready", asNewWindowEventArgs(args)) end)
    LobbyGui.root:getChild("Button_Start"):subscribeEvent("Clicked", function(args) LobbyGui.root:fireEvent("RequestStart", asNewWindowEventArgs(args)) end)
    -- forward any keypresses in the main window to the chat
    LobbyGui.root:subscribeEvent("KeyDown", LobbyGui.Chat.handleKeypress)

    return LobbyGui
end

LobbyGui.delete = function()
    CEGUI.System:getSingleton():getDefaultGUIContext():getRootWindow():removeChild(LobbyGui.root)
    LobbyGui = nil
end

LobbyGui.onConnectedToServer = function(serverName, nrOfSlots)
    LobbyGui.root:getChild("StaticText_ServerName"):setText(serverName)

    LobbyGui.Chat.onConnectedToServer(serverName, nrOfSlots)

    for slot= 1,nrOfSlots do
        local button = LobbyGui.root:getChild("Button" .. slot)
        button:subscribeEvent("Clicked", function(args) LobbyGui.root:fireEvent("RequestSlotChange", asNewWindowEventArgs(args)) end)
    end
end

LobbyGui.updateSlotButton = function(slotId, playerName, inUse, isReady)
    local button = LobbyGui.root:getChild("Button" .. slotId)

    if not inUse then
        button:setText("Empty")
    else
        local text = playerName
        if isReady then
            text = text .. " - R"
        end
        button:setText(text)
    end
end

