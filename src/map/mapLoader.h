/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef MAPLOADER_H
#define MAPLOADER_H

#include <irrlicht/irrString.h>
#include <irrlicht/path.h>
#include <irrlicht/vector3d.h>
#include <memory> // std::unique_ptr
#include <string>
#include <vector>
#include "pathGrid.h"
#include "pathMapXML.h"
#include "../ecs/entity.h" // entityID_t

class EntityManager;
class Game;
class GameMap;
class TerrainShader;
namespace Handles
{
    class HandleManager;
}
namespace irr
{
    namespace scene
    {
        class TerrainSceneNode;
        class ISceneManager;
        class IAnimatedMeshSceneNode;
    } // namespace scene
    namespace io
    {
        class IFileSystem;
    }
    namespace video
    {
        class ITexture;
        class IVideoDriver;
    } // namespace video
} // namespace irr

class MapLoader
{
   public:
    MapLoader(irr::scene::ISceneManager* const sceneManager,
              irr::io::IFileSystem* const fileSystem,
              irr::video::IVideoDriver* const videoDriver,
              Game& game);

    struct MapLoadingParameters
    {
        bool allowSplatMapModification = false;
        irr::core::vector3df position = irr::core::vector3df(0.0f, 0.0f, 0.0f);
        irr::core::vector3df rotation = irr::core::vector3df(0.0f, 0.0f, 0.0f);
        irr::core::vector3df scale = irr::core::vector3df(1.0f, 1.0f, 1.0f);
        std::vector<irr::core::stringw> textureArrayFilenames = std::vector<irr::core::stringw>();
        irr::core::stringw splatMapFilename = irr::core::stringw(L"");
        irr::core::stringw patchesFile = irr::core::stringw(L"");
        irr::core::stringw pathMapFile = irr::core::stringw(L"");
        std::vector<irr::io::path> staticMeshFilenames = std::vector<irr::io::path>();
    };

    std::tuple<MapLoader::MapLoadingParameters, GameMap*, std::vector<irr::scene::IAnimatedMeshSceneNode*>>
    loadMap(const std::string filename, const bool allowSplatMapModification = false);

    /// @brief save 'gameMap' using the supplied filename and parameters
    /// @param filename
    /// @param parameters will be modified because e.g. 'displayOnlyEntitiesToBakeIntoMap' will be
    /// converted into filenames of static entities to load.
    /// @param gameMap
    /// @param displayOnlyEntitiesToBakeIntoMap
    /// @param entityManager
    /// @param handleManager
    /// @return
    ErrCode saveMap(const std::string filename,
                    MapLoader::MapLoadingParameters& parameters,
                    GameMap& gameMap,
                    const std::vector<entityID_t>& displayOnlyEntitiesToBakeIntoMap,
                    EntityManager& entityManager,
                    Handles::HandleManager& handleManager);

   private:
    irr::scene::ISceneManager* const sceneManager;
    irr::io::IFileSystem* const fileSystem;
    irr::video::IVideoDriver* const videoDriver;
    Game& game;

    std::unique_ptr<irr::scene::TerrainSceneNode> loadTerrain(const MapLoadingParameters& loadingParameters);
    TerrainShader* addShader(const MapLoadingParameters& loadingParameters);
    PathGrid loadPathGrid(const MapLoadingParameters& loadingParameters);
    std::vector<irr::scene::IAnimatedMeshSceneNode*> loadStaticMeshes(const MapLoadingParameters& loadingParameters);

    TerrainShader* createShader(const MapLoadingParameters& loadingParameters);
    irr::video::ITexture* createTextureArray(std::vector<irr::core::stringw> textureArrayFilenames);
};

#endif /* ifndef MAPLOADER_H */
