/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017 Hannes Franke, Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "terrainShader.h"

#include <stdint.h> // because terrain_vs/fs_glsl.h use a uint8_t array and uint8_t is undefined by default

#include <irrlicht/irrArray.h> // have to stay at the beginning -> needs space separation to avoid clang-formats includes-sorting

#include <irrlicht/IGPUProgrammingServices.h>
#include <irrlicht/IMaterialRendererServices.h>
#include <irrlicht/ITexture.h>
#include <irrlicht/IVideoDriver.h>
#include <irrlicht/SMaterial.h>

#include "../graphics/binaryShaderLoader.h"

TerrainShader::TerrainShader(irr::video::IVideoDriver* const driver_,
                             irr::video::ITexture* const splatTexture_,
                             irr::video::ITexture* const textureArray_,
                             irr::video::ITexture* const bumpTextureArray_,
                             irr::video::ITexture* const blendTexture_)
    : driver(driver_)
    , splatTexture(splatTexture_)
    , textureArray(textureArray_)
    , bumpTextureArray(bumpTextureArray_)
    , blendTexture(blendTexture_)
{
    this->splatTexture->grab();
    this->textureArray->grab();
    this->bumpTextureArray->grab();
    this->blendTexture->grab();
}


TerrainShader::~TerrainShader()
{
    this->driver->removeTexture(this->splatTexture);
    this->driver->removeTexture(this->textureArray);
    this->driver->removeTexture(this->bumpTextureArray);
    this->driver->removeTexture(this->blendTexture);

    this->splatTexture->drop();
    this->textureArray->drop();
    this->bumpTextureArray->drop();
    this->blendTexture->drop();
}

ErrCode TerrainShader::loadShaders()
{
    auto driverType = this->driver->getDriverType();
    std::string typeExt = "";
    if (driverType == irr::video::E_DRIVER_TYPE::EDT_BGFX_OPENGL)
    {
        typeExt = "_glsl_bgfx.bin";
    }
    else if (driverType == irr::video::E_DRIVER_TYPE::EDT_BGFX_D3D11)
    {
        typeExt = "_hlsl_DX11_bgfx.bin";
    }
    else
    {
        Error::errTerminate("DriverType not Supported! Use BGFX with OpenGL or D3D11");
    }


    auto loadShader = [this, typeExt](std::string shaderName) {
        return BinaryShaderLoader::loadShader(std::string("shader/") + shaderName + typeExt);
    };

    auto terrain_vs = loadShader("terrain_vs");
    auto terrain_fs = loadShader("terrain_fs");


    this->shaderMaterial =
        this->driver->getGPUProgrammingServices()->addHighLevelShaderMaterial(terrain_vs.data(),
                                                                              terrain_vs.size(),
                                                                              "main",
                                                                              irr::video::EVST_VS_5_0,
                                                                              terrain_fs.data(),
                                                                              terrain_fs.size(),
                                                                              "main",
                                                                              irr::video::EPST_PS_5_0,
                                                                              this,
                                                                              irr::core::array<irr::core::stringc>(),
                                                                              irr::core::array<irr::u32>(),
                                                                              irr::video::EMT_SOLID);

    if (this->shaderMaterial == -1)
    {
        Error::errTerminate("couldn't create TerrainShader bgfx header! (Opengl)");
    }


    // the videoDriver grab()s this callBack
    this->drop();

    return ErrCodes::NO_ERR;
}

void TerrainShader::applyToMaterial(irr::video::SMaterial& material)
{
    material.setTexture(0, this->textureArray);
    material.setTexture(1, this->splatTexture);
    // material.setTexture(2, this->bumpTextureArray);
    // material.setTexture(3, this->blendTexture);
    material.MaterialType = static_cast<irr::video::E_MATERIAL_TYPE>(shaderMaterial);
}

void TerrainShader::OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32)
{
    // switch (this->driver->getDriverType())
    //{
    //    case irr::video::EDT_BGFX_METAL:
    //    case irr::video::EDT_BGFX_OPENGL:
    //    case irr::video::EDT_BGFX_D3D9:
    //    case irr::video::EDT_BGFX_D3D11:
    //        return; // bgfx sets the texture uniforms by itself
    //    default:
    //        break;
    //}
}
