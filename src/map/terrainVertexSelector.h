// TerrainVertexSelector based on the CTerrainTriangleSelector from Irrlicht.
// For the license of this file see licenses/irrlicht.

// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

// The code for the TerrainVertexSelector is based on the GeoMipMapSelector
// developed by Spintz. He made it available for Irrlicht and allowed it to be
// distributed under this licence. I only modified some parts. A lot of thanks go to him.

#ifndef TERRAINVERTEXSELECTOR_H
#define TERRAINVERTEXSELECTOR_H

#include <irrlicht/aabbox3d.h>
#include <irrlicht/irrArray.h>
#include <irrlicht/matrix4.h>
#include <irrlicht/triangle3d.h>

// forward declarations
namespace irr
{
    namespace scene
    {
        class ISceneNode;
        class TerrainSceneNode;
    } // namespace scene
    namespace video
    {
        struct S3DVertex2TCoords;
    }
} // namespace irr

namespace irr
{
    namespace scene
    {
        //! Triangle Selector for the TerrainSceneNode
        /** The code for the TerrainVertexSelector is based on the GeoMipMapSelector
        developed by Spintz. He made it available for Irrlicht and allowed it to be
        distributed under this licence. I only modified some parts. A lot of thanks go
        to him.
        */
        class TerrainVertexSelector
        {
           public:
            //! Constructs a selector based on an ITerrainSceneNode
            TerrainVertexSelector(TerrainSceneNode* node, s32 LOD);

            TerrainVertexSelector(const TerrainVertexSelector&) = delete;

            //! Destructor
            virtual ~TerrainVertexSelector();

            //! Clears and sets triangle data
            virtual void setTriangleData(TerrainSceneNode* node, s32 LOD);

            //! Gets all triangles.
            void getTriangles(core::triangle3df* triangles,
                              u32* indices,
                              s32 arraySize,
                              s32& outTriangleCount,
                              const core::matrix4* transform = nullptr) const;

            //! Gets all triangles which lie within a specific bounding box.
            void getTriangles(core::triangle3df* triangles,
                              u32* indices,
                              s32 arraySize,
                              s32& outTriangleCount,
                              const core::aabbox3d<f32>& box,
                              const core::matrix4* transform = nullptr) const;

            //! Gets all triangles which have or may have contact with a 3d line.
            virtual void getTriangles(core::triangle3df* triangles,
                                      u32* indices,
                                      s32 arraySize,
                                      s32& outTriangleCount,
                                      const core::line3d<f32>& line,
                                      const core::matrix4* transform = nullptr) const;

            //! Returns amount of all available triangles in this selector
            virtual s32 getTriangleCount() const;

            //! Return the scene node associated with a given triangle.
            virtual ISceneNode* getSceneNodeForTriangle(u32 triangleIndex) const;

            // Get the number of TriangleSelectors that are part of this one
            virtual u32 getSelectorCount() const;

            TerrainVertexSelector operator=(const TerrainVertexSelector&) = delete;

           private:
            friend class TerrainSceneNode;

            struct SGeoMipMapTrianglePatch
            {
                core::array<core::triangle3df> Triangles = core::array<core::triangle3df>();
                /// @brief convert triangle index to TerrainSceneNode vertex index
                irr::core::array<irr::u32> VertexIndices = core::array<irr::u32>();
                s32 NumTriangles = 0;
                core::aabbox3df Box = core::aabbox3df();
            };

            struct SGeoMipMapTrianglePatches
            {
                SGeoMipMapTrianglePatches()
                    : TrianglePatchArray()
                {
                }

                core::array<SGeoMipMapTrianglePatch> TrianglePatchArray;
                s32 NumPatches = 0;
                u32 TotalTriangles = 0;
            };

            TerrainSceneNode* SceneNode = nullptr;
            SGeoMipMapTrianglePatches TrianglePatches;
        };

    } // namespace scene
} // namespace irr

#endif // TERRAINVERTEXSELECTOR_H
