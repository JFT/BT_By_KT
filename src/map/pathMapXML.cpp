/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "pathMapXML.h"

#include <cmath>

#include <irrlicht/IFileSystem.h>
#include <irrlicht/IXMLWriter.h>
#include <irrlicht/irrXML.h>

#include "mapSystemVersion.h"
#include "pathGrid.h"
#include "../utils/debug.h"
#include "../utils/static/repr.h"
#include "../utils/stringwparser.h"

ErrCode PathMapXML::loadPathMap(const irr::io::path& filename, PathGrid& loadedPathGrid)
{
    irr::io::IrrXMLReader* xml = irr::io::createIrrXMLReader(filename.c_str());
    if (xml == nullptr)
    {
        Error::errContinue("couldn't open xml file", repr<irr::io::path>(filename));
        return ErrCodes::GENERIC_ERR;
    }

    irr::u32 lineNum = 0;

    while (xml && xml->read())
    {
        lineNum++;

        debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                      "Handling xml line with NodeType=",
                      xml->getNodeType(),
                      "NodeData=",
                      repr<const char*>(xml->getNodeData()));
        // check the node type
        switch (xml->getNodeType())
        {
            case irr::io::EXN_ELEMENT: // element open tag
            {
                if (irr::core::stringw(L"mapSystem").equals_ignore_case(xml->getNodeName()))
                {
                    irr::f32 version = -1.0;
                    if (StringWParser::parse(xml->getAttributeValueSafe("version"), version, -1.0f) == ErrCodes::NO_ERR)
                    {
                        if (std::fabs(version - tileMap::MapSystemVersion::version) > 1e-5)
                        {
                            Error::errContinue("file",
                                               filename,
                                               "was generated using map system version",
                                               version,
                                               "but the current version is",
                                               tileMap::MapSystemVersion::version,
                                               "re-generate the map using newest mapEditor");
                            return ErrCodes::GENERIC_ERR;
                        }
                    }
                    else
                    {
                        Error::errContinue("couldn't read version out of", filename, " on line", lineNum, "map loading might fail unexpectedly");
                    }
                }
                else if (irr::core::stringw(L"pathMap").equals_ignore_case(xml->getNodeName()))
                {
                    if (StringWParser::parse(xml->getAttributeValueSafe("Width"),
                                             loadedPathGrid.width,
                                             static_cast<PathGrid::PathGridDimension_t>(0)) != ErrCodes::NO_ERR)
                    {
                        Error::errContinue("couldn't parse attribute 'Width' of element "
                                           "'pathMap'!. The pathfinding grid won't function "
                                           "properly!");
                        return ErrCodes::GENERIC_ERR;
                    }
                    if (StringWParser::parse(xml->getAttributeValueSafe("Height"),
                                             loadedPathGrid.height,
                                             static_cast<PathGrid::PathGridDimension_t>(0)) != ErrCodes::NO_ERR)
                    {
                        Error::errContinue("couldn't parse attribute 'Height' of element "
                                           "'pathMap'!. The pathfinding grid won't function "
                                           "properly!");
                        return ErrCodes::GENERIC_ERR;
                    }
                    loadedPathGrid.resize(loadedPathGrid.width, loadedPathGrid.height);
                    for (PathGrid::PathGridDimension_t x = 0; x < loadedPathGrid.width; x++)
                    {
                        for (PathGrid::PathGridDimension_t z = 0; z < loadedPathGrid.height; z++)
                        {
                            loadedPathGrid.set(x, z, PathGrid::Walkability::Free);
                        }
                    }
                }
                else if (irr::core::stringw(L"pathPoint").equals_ignore_case(xml->getNodeName()))
                {
                    PathGrid::PathGridDimension_t x = 0;
                    PathGrid::PathGridDimension_t z = 0;
                    bool isWalkable = true;
                    if (StringWParser::parse(xml->getAttributeValueSafe("x"),
                                             x,
                                             static_cast<PathGrid::PathGridDimension_t>(0)) != ErrCodes::NO_ERR)
                    {
                        Error::errContinue("couldn't parse attribute 'x'='",
                                           repr<const char*>(xml->getAttributeValueSafe("x")),
                                           "of element 'pathPoint'!");
                    }
                    if (StringWParser::parse(xml->getAttributeValueSafe("z"),
                                             z,
                                             static_cast<PathGrid::PathGridDimension_t>(0)) != ErrCodes::NO_ERR)
                    {
                        Error::errContinue("couldn't parse attribute 'z'='",
                                           repr<const char*>(xml->getAttributeValueSafe("z")),
                                           "of element 'pathPoint'!");
                    }
                    if (StringWParser::parse(xml->getAttributeValueSafe("isWalkable"), isWalkable, true) !=
                        ErrCodes::NO_ERR)
                    {
                        Error::errContinue("couldn't parse attribute 'isWalkable'='",
                                           repr<const char*>(
                                               xml->getAttributeValueSafe("isWalkable")),
                                           "of element 'pathPoint'!");
                    }
                    if (x > loadedPathGrid.width or z > loadedPathGrid.height)
                    {
                        Error::errContinue("parsed x=",
                                           x,
                                           "> pathGrid.Width=",
                                           loadedPathGrid.width,
                                           "or z=",
                                           z,
                                           "> pathGrid.Height=",
                                           loadedPathGrid.height);
                    }
                    else
                    {
                        if (isWalkable)
                        {
                            loadedPathGrid.set(x, z, PathGrid::Walkability::Free);
                        }
                        else
                        {
                            loadedPathGrid.set(x, z, PathGrid::Walkability::StaticNonWalkable);
                        }
                    }
                }
            } // case irr::io::EXN_ELEMENT:
            break;

            case irr::io::EXN_ELEMENT_END:
            case irr::io::EXN_NONE:
            case irr::io::EXN_TEXT:
            case irr::io::EXN_COMMENT:
            case irr::io::EXN_CDATA:
            case irr::io::EXN_UNKNOWN:
            default:
                debugOutLevel(Debug::DebugLevels::maxLevel,
                              "unhandled xml entry type",
                              static_cast<int>(xml->getNodeType()));
                break;
        } // switch(xml->getNodeType)
    }     // while(xml && xml->read())

    if (xml != nullptr)
    {
        delete xml;
    }

    return ErrCodes::NO_ERR;
}


ErrCode PathMapXML::savePathMap(const PathGrid& pathGrid, const irr::io::path& filename, irr::io::IFileSystem* const fileSystem)
{
    irr::io::IXMLWriter* xwriter = fileSystem->createXMLWriter(filename);
    if (!xwriter)
    {
        return ErrCode(1);
    }
    // write out the obligatory xml header. Each xml-file needs to have exactly one of those.
    xwriter->writeXMLHeader();

    xwriter->writeElement(L"pathMap",
                          false,
                          L"Width",
                          irr::core::stringw(pathGrid.width).c_str(),
                          L"Height",
                          irr::core::stringw(pathGrid.height).c_str());
    xwriter->writeLineBreak();

    for (PathGrid::PathGridDimension_t x = 0; x < pathGrid.width; x++)
    {
        for (PathGrid::PathGridDimension_t z = 0; z < pathGrid.height; z++)
        {
            bool isWalkable = true;
            if (pathGrid.get(x, z) == PathGrid::Walkability::StaticNonWalkable)
            {
                // by checking only for static usage any dynamic changes are treated as 'free'
                isWalkable = false;
            }
            xwriter->writeElement(L"pathPoint",
                                  true,
                                  L"x",
                                  irr::core::stringw(x).c_str(),
                                  L"z",
                                  irr::core::stringw(z).c_str(),
                                  L"isWalkable",
                                  StringWParser::getParsableString(isWalkable).c_str());
            xwriter->writeLineBreak();
        }
    }
    xwriter->writeClosingTag(L"pathMap");
    xwriter->drop();
    return ErrCodes::NO_ERR;
}
