/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAPGENERATOR_H
#define MAPGENERATOR_H

#include <vector>

#include <irrlicht/vector3d.h>

#include "pathGrid.h"
#include "tile.h"
#include "tilechooser.h"

// forward declarations
namespace irr
{
    class IrrlichtDevice;
    namespace scene
    {
        class ISceneManager;
        class CDynamicMeshBuffer;
    } // namespace scene
    namespace video
    {
        class IImage;
    }
} // namespace irr


namespace tileMap
{
    /// @brief generates the map mesh and textures to be used by TileMapSceneNode
    class MapGenerator
    {
       public:
        /// @brief constructor of the map generator
        ///
        /// all supplied images will be grab()-ed if not nullptr and drop()-ped if MapGenerator is
        /// destroyed.
        /// @param cliffMap the cliffs to use. MapGenerator grabs() the cliffMap.
        /// @param rampMap if the red component of a pixel at any position is > 128 a ramp is
        /// created at that position. Must be the same size as cliffMap. Can be nullptr if no ramps
        /// needed.
        /// @param offsetMap must be the same size as ciffMap and rampMap. Can be nullptr if no
        /// offsets needed.
        /// @param tileDefinitionsFile
        /// @param sceneManager
        MapGenerator(const irr::video::IImage* const cliffMap,
                     const irr::video::IImage* const rampMap,
                     const irr::video::IImage* const offsetMap,
                     const irr::io::path& tileDefinitionsFile,
                     irr::scene::ISceneManager* sceneManager);
        MapGenerator(const MapGenerator&) = delete;
        ~MapGenerator();

        /// @brief generate the mesh xml file which can be loaded by a TerrainSceneNode.
        /// @param filename if the filename ends in .xml a text map format is used. Otherwise a the
        /// map is saved in binary format.
        /// @param patchSizeX how many tiles a patch spans in x direction
        /// @param patchSizeZ how many tiles a patch spans in z direction
        /// @param device needed to write the xml
        /// @param binaryFormat
        /// @return NO_ERR if succesfull, error otherwise
        ErrCode generateAndSavePatches(const irr::io::path& filename,
                                       const unsigned int patchSizeX,
                                       const unsigned patchSizeZ,
                                       irr::IrrlichtDevice* device);

        /// @brief generate the pathGrid for the currently active patches
        /// @param mapScale scale of the map (as used for the gamemap) to get the relation between
        /// pathGrid.unitSize and the map correct
        /// @param pathGrid pathGrids which will be filled with the pathing data for the currently
        /// generated patches. generatePathMap() uses PathGrid.unitSize
        /// @return currently only ErrCodes::NO_ERR (there for future use)
        ErrCode generatePathMap(const irr::core::vector3df mapScale, PathGrid& pathGrid);

        /// @brief combine and save the texture for all tiles into one
        /// @param filename filename of the combined texture
        /// @return ErrCodes::NO_ERR or anything else if memory allocation for the whole texture
        /// fails or saving of the image fails.
        ErrCode saveFullMapTexture(const irr::core::stringw filename);

        MapGenerator operator=(const MapGenerator&) = delete;

       private:
        const irr::video::IImage* const cliffMap = nullptr;
        const irr::video::IImage* const rampMap = nullptr;
        const irr::video::IImage* const offsetMap = nullptr;
        irr::scene::ISceneManager* sceneManager = nullptr;
        TileChooser tileChooser;
        std::vector<std::vector<Tile>> tileArr;

        /// @brief returns a sorted array of all unique values of the red component of the image
        /// @param image
        /// @return sorted array of unique values. If 'image' is nullptr returns array containing
        /// only the value 0.
        std::vector<irr::u32> getUniquePixelRedValues(const irr::video::IImage* const image) const;

        /// @brief return the level for the specific pixelValue as recorded in seenPixelValues
        /// @param pixelValue the value of the pixel which level should be found
        /// @param seenPixelValues array containing the boundaries for the levels. Can be generatey
        /// by getUniquePixelRedValues
        /// @return the level for the specific pixelValue
        irr::u32 getLevelForPixelValue(const irr::u32 pixelValue, const std::vector<irr::u32>& seenPixelValues);

        /// @brief check if there exista a ramp (= 3 ramp pixels in a line, height change on the
        /// last pixel) from pixelX, pixelY in 'direction' which goes upwards
        /// @param pixelX the x-position where the ramp starts in image-coordinates
        /// @param pixelY the y-position where the ramp starts in image-coordinates
        /// @return true if a ramp going upwards, starting from pixelX, pixelY exists, false
        /// otherwise. Also returns false if any of the tested pixels are outside of the image.
        bool existsRampUpInDirection(const irr::s32 pixelX, const irr::s32 pixelY, const directions::Directions direction);

        void generateRampData(std::vector<std::vector<float>>& pixelLevelMap,
                              std::vector<std::vector<std::vector<directions::Directions>>>& pixelRampDirection,
                              const std::vector<irr::u32> seenPixelValues);

        /// @brief use cliffMap, rampMap (if not nullptr) and offsetMap (if not nullptr) to generate
        /// the tiles and connect them all together
        /// @param offsetFactor factor between the red component (0-255) of the offsetMap and the
        /// actual worldspace offset it generates
        /// @param offsetZero what red component of the offsetMap is interpreted as zero
        void createTileMap(const irr::f32 offsetFactor = 0.02f, const irr::s32 offsetZero = 0);

        std::vector<irr::video::S3DVertex2TCoords> createVertices();

        ErrCode savePatchesData(const irr::u32 patchSizeX,
                                const irr::u32 patchSizeZ,
                                const irr::io::path& filename,
                                irr::IrrlichtDevice* const device,
                                const bool binaryFormat);

        ErrCode savePatchesDataXML(const irr::io::path& filename,
                                   irr::IrrlichtDevice* const device,
                                   const irr::u32 patchSizeX,
                                   const irr::u32 patchSizeZ,
                                   const irr::u32 patchCountX,
                                   const irr::u32 patchCountZ,
                                   const irr::u32 tileCountX,
                                   const irr::u32 tileCountZ,
                                   const irr::scene::CDynamicMeshBuffer* meshBuffer);

        ErrCode savePatchesDataBinary(const irr::io::path& filename,
                                      const irr::u32 patchSizeX,
                                      const irr::u32 patchSizeZ,
                                      const irr::u32 patchCountX,
                                      const irr::u32 patchCountZ,
                                      const irr::u32 tileCountX,
                                      const irr::u32 tileCountZ,
                                      const irr::scene::CDynamicMeshBuffer* meshBuffer);

        irr::core::vector2d<irr::s32> nextPixelInDirection(const irr::u32 pixelX,
                                                           const irr::u32 pixelY,
                                                           const directions::Directions direction);

        /// @brief get the pixel coordinates for a specific tile in a specifig direction
        /// @param tileX
        /// @param tileZ
        /// @param maximumPixelX the maximum x-coordinate of the pixel (width of the image - 1)
        /// @param direction the direction of the pixel inside the tile. Can only be one of the
        /// diagonal directions, otherwise throws an error
        /// @return the coordinates of the pixel in image-space. The coordinates migth be outside of
        /// the image.
        irr::core::vector2d<irr::s32> getPixelForTile(const irr::u32 tileX,
                                                      const irr::u32 tileZ,
                                                      const irr::u32 maximumPixelX,
                                                      const directions::Directions direction);


        /// @brief flood-fills the map to find all walkable and non-walkable positions.
        /// @param knownWalkablePosition a positon on the pathing grid which definitely is walkable
        /// and functions as the starting point of the flood-fill. Must be inside [0,
        /// GampMap::grid.width/height].
        /// @return currently only ErrorCodes::NO_ERR
        ErrCode
        floodFill(PathGrid& pathGrid,
                  const irr::core::vector2d<uint32_t> knownWalkablePosition = irr::core::vector2d<uint32_t>(0, 0));
    };

} // namespace tileMap
#endif /* ifndef MAPGENERATOR_H */
