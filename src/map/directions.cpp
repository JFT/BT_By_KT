/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "directions.h"

#include "../utils/stringwstream.h" // stringwstream needs to be included before debug.h and error.h because they use operator<< on a stringw
#include "../utils/debug.h"
#include "../utils/static/error.h"
#include "../utils/stringwparser.h"


std::string directions::directionToString(const directions::Directions direction)
{
    switch (direction)
    {
        case Directions::TopLeft:
            return std::string("TopLeft");
        case Directions::Top:
            return std::string("Top");
        case Directions::TopRight:
            return std::string("TopRight");
        case Directions::Right:
            return std::string("Right");
        case Directions::BottomRight:
            return std::string("BottomRight");
        case Directions::Bottom:
            return std::string("Bottom");
        case Directions::BottomLeft:
            return std::string("BottomLeft");
        case Directions::Left:
            return std::string("Left");
        case Directions::Internal:
            return std::string("Internal");
        case Directions::NumDirections:
            return std::string("NumDirections");
        default:
            Error::errContinue("Unhandled direction id", static_cast<size_t>(direction));
            return std::string("UnknownDirection");
    }
}

irr::core::stringw directions::getParsableString(const directions::Directions direction)
{
    switch (direction)
    {
        case Directions::TopLeft:
            return irr::core::stringw(L"TopLeft");
        case Directions::Top:
            return irr::core::stringw(L"Top");
        case Directions::TopRight:
            return irr::core::stringw(L"TopRight");
        case Directions::Right:
            return irr::core::stringw(L"Right");
        case Directions::BottomRight:
            return irr::core::stringw(L"BottomRight");
        case Directions::Bottom:
            return irr::core::stringw(L"Bottom");
        case Directions::BottomLeft:
            return irr::core::stringw(L"BottomLeft");
        case Directions::Left:
            return irr::core::stringw(L"Left");
        case Directions::Internal:
            return irr::core::stringw(L"Internal");
        case Directions::NumDirections:
            return irr::core::stringw(L"NumDirections");
        default:
            Error::errContinue("Unhandled direction id", static_cast<size_t>(direction));
            return irr::core::stringw(L"UnknownDirection");
    }
}

directions::Directions directions::invertDirection(const Directions direction)
{
    switch (direction)
    {
        case Directions::TopLeft:
            return Directions::BottomRight;
        case Directions::Top:
            return Directions::Bottom;
        case Directions::TopRight:
            return Directions::BottomLeft;
        case Directions::Right:
            return Directions::Left;
        case Directions::BottomRight:
            return Directions::TopLeft;
        case Directions::Bottom:
            return Directions::Top;
        case Directions::BottomLeft:
            return Directions::TopRight;
        case Directions::Left:
            return Directions::Right;
        case Directions::Internal:
            return Directions::Internal;
        case Directions::NumDirections:
        default:
            Error::errTerminate("called invertDirection with invalid direction",
                                directions::directionToString(direction));
    }
}


directions::Directions directions::rotateClockwise(const Directions direction)
{
    switch (direction)
    {
        case Directions::TopLeft:
            return Directions::TopRight;
        case Directions::Top:
            return Directions::Right;
        case Directions::TopRight:
            return Directions::BottomRight;
        case Directions::Right:
            return Directions::Bottom;
        case Directions::BottomRight:
            return Directions::BottomLeft;
        case Directions::Bottom:
            return Directions::Left;
        case Directions::BottomLeft:
            return Directions::TopLeft;
        case Directions::Left:
            return Directions::Top;
        case Directions::Internal:
            return Directions::Internal;
        case Directions::NumDirections:
            // in the context of ramps NumDirection actually makes sense because it means any
            // direction fits -> no warning on using it here
            return Directions::NumDirections;
        default:
            Error::errTerminate("Unhandled direction with id = ", static_cast<size_t>(direction));
    }
}

directions::Directions directions::combineDirections(const Directions dir1, const Directions dir2)
{
    // there probably is a prettier version to write involving c++11 stuff but this will do for now
    // TODO: figure out a more elegant way to write this function
    switch (dir1)
    {
        case Directions::Top:
            switch (dir2)
            {
                case Directions::Left:
                    return Directions::TopLeft;
                case Directions::Right:
                    return Directions::TopRight;
                case Directions::Top:
                case Directions::Bottom:
                case Directions::TopLeft:
                case Directions::TopRight:
                case Directions::BottomRight:
                case Directions::BottomLeft:
                case Directions::Internal:
                case Directions::NumDirections:
                default:
                    return Directions::NumDirections;
            }
        case Directions::Right:
            switch (dir2)
            {
                case Directions::Top:
                    return Directions::TopRight;
                case Directions::Bottom:
                    return Directions::BottomRight;
                case Directions::Right:
                case Directions::Left:
                case Directions::TopLeft:
                case Directions::TopRight:
                case Directions::BottomRight:
                case Directions::BottomLeft:
                case Directions::Internal:
                case Directions::NumDirections:
                default:
                    return Directions::NumDirections;
            }
        case Directions::Bottom:
            switch (dir2)
            {
                case Directions::Right:
                    return Directions::BottomRight;
                case Directions::Left:
                    return Directions::BottomLeft;
                case Directions::Top:
                case Directions::Bottom:
                case Directions::TopLeft:
                case Directions::TopRight:
                case Directions::BottomRight:
                case Directions::BottomLeft:
                case Directions::Internal:
                case Directions::NumDirections:
                default:
                    return Directions::NumDirections;
            }
        case Directions::Left:
            switch (dir2)
            {
                case Directions::Bottom:
                    return Directions::BottomLeft;
                case Directions::Top:
                    return Directions::TopLeft;
                case Directions::Right:
                case Directions::Left:
                case Directions::TopLeft:
                case Directions::TopRight:
                case Directions::BottomRight:
                case Directions::BottomLeft:
                case Directions::Internal:
                case Directions::NumDirections:
                default:
                    return Directions::NumDirections;
            }
        case Directions::TopLeft:
        case Directions::TopRight:
        case Directions::BottomRight:
        case Directions::BottomLeft:
        case Directions::Internal:
        case Directions::NumDirections:
        default:
            return Directions::NumDirections;
    }
}

/* ########################################################################
 * functions in other namespaces
 * ######################################################################## */

// custom parsing function to be used with the stringwparser
template <>
directions::Directions
StringWParser::parseInternal<directions::Directions>(const std::wstring str, size_t& readTo)
{
    debugOutLevel(Debug::secondOrderLoop + 5, "trying to parse string", repr<std::wstring>(str), "as direction");
    const std::array<std::wstring, static_cast<size_t>(directions::Directions::NumDirections) + 1> testStr = {
        std::wstring(L"TopLeft"),
        std::wstring(L"Top"),
        std::wstring(L"TopRight"),
        std::wstring(L"Right"),
        std::wstring(L"BottomRight"),
        std::wstring(L"Bottom"),
        std::wstring(L"BottomLeft"),
        std::wstring(L"Left"),
        std::wstring(L"Internal"),
        std::wstring(L"NumDirections")};
    constexpr std::array<directions::Directions, static_cast<size_t>(directions::Directions::NumDirections) + 1>
        testDirection = {directions::Directions::TopLeft,
                         directions::Directions::Top,
                         directions::Directions::TopRight,
                         directions::Directions::Right,
                         directions::Directions::BottomRight,
                         directions::Directions::Bottom,
                         directions::Directions::BottomLeft,
                         directions::Directions::Left,
                         directions::Directions::Internal,
                         directions::Directions::NumDirections};

    size_t matchingIndex = 0;
    // because directions like 'BottomRight' match both 'Bottom' and 'BottomRight' the longest match
    // needs to be used.
    size_t longestMatchLength = 0;
    for (size_t i = 0; i < testStr.size(); i++)
    {
        debugOutLevel(Debug::DebugLevels::secondOrderLoop + 6,
                      "cheching",
                      repr<std::wstring>(str),
                      "against",
                      repr<std::wstring>(testStr[i]));
        bool matches = true;
        for (size_t c = 0; c < testStr[i].size(); c++)
        {
            debugOutLevel(Debug::DebugLevels::secondOrderLoop + 8,
                          "cheching letter from string",
                          repr<std::wstring>(testStr[i]),
                          "[c=",
                          c,
                          "]=",
                          repr<wchar_t>(testStr[i][c]),
                          "against",
                          repr<std::wstring>(str),
                          "[",
                          c,
                          "]=",
                          repr<wchar_t>(str[c]));
            // reached the end of the string to parse but didn't match
            if (c >= str.size())
            {
                debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9, "reached end of string to parse -> no match");
                matches = false;
                break;
            }
            // letters don't match
            if (str[c] != testStr[i][c])
            {
                debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9, "letters didn't match");
                matches = false;
                break;
            }
        }
        if (matches and longestMatchLength < testStr[i].size())
        {
            debugOutLevel(Debug::DebugLevels::secondOrderLoop + 8,
                          "found new longest match with",
                          repr<std::wstring>(testStr[i]),
                          "and longestMatchLength=",
                          longestMatchLength);
            longestMatchLength = testStr[i].size();
            matchingIndex = i;
        }
    }
    // if any direction has matches the longestMatch will be at least 1 long
    readTo = longestMatchLength;
    if (longestMatchLength != 0)
    {
        return testDirection[matchingIndex];
    }
    throw std::invalid_argument(std::string("couldn't parse string") + std::string(str.begin(), str.end()) +
                                std::string("couldn't be parsed as a directions::Direction"));
}
