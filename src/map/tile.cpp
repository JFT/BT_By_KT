/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../utils/stringwstream.h" // stringwstream.h needs to be included before tile.h because it uses operator<< on a stringw.

#include "tile.h"

#include <cmath>

#include <irrlicht/EHardwareBufferFlags.h> // needed by IIndexBuffer.h
#include <irrlicht/IImage.h>
#include <irrlicht/IIndexBuffer.h>
#include <irrlicht/IVideoDriver.h>
#include <irrlicht/SColor.h> // needed to color UV coordinate points on textures.

#include "../graphics/ImageManipulation.h" // needed to create rotated copies of the texture image.
#include "../utils/printfunctions.h"


using namespace tileMap;

Tile::Tile()
    : knowsAbsoluteIndex()
    , neighbors()
    , indices()
    , relativeVertices()
    , relativeIndices()
    , relativeToAbsoluteIndex()
    , levels(0, 0, 0, 0)
    , typeName(irr::core::stringw(L""))
{
    for (unsigned int i = 0; i < static_cast<unsigned int>(directions::Directions::NumDirections); i++)
    {
        neighbors.push_back(nullptr);
    }
    // we are our internal neighbor
    neighbors[static_cast<size_t>(directions::Directions::Internal)] = this;
}

void Tile::copyFrom(const Tile& other, irr::video::IVideoDriver* videoDriver)
{
    this->knowsAbsoluteIndex = other.knowsAbsoluteIndex;
    this->neighbors = other.neighbors;
    this->indices = other.indices;
    this->relativeVertices = other.relativeVertices;
    this->relativeIndices = other.relativeIndices;
    this->relativeToAbsoluteIndex = other.relativeToAbsoluteIndex;
    this->levels = other.levels;
    this->typeName = other.typeName;
    if (videoDriver != nullptr and other.image != nullptr)
    {
        this->image = videoDriver->createImage(other.image->getColorFormat(), other.image->getDimension());
        other.image->copyTo(this->image);
    }
    else
    {
        if (this->image != nullptr)
        {
            this->image->drop();
        }
        this->image = nullptr;
    }
}

Tile::~Tile()
{
    if (this->image != nullptr)
    {
        this->image->drop();
    }
    // all other memory will be freed automatically
}

void Tile::setLevels(const Levels_t levels_)
{
    this->levels = levels_;
}

const Levels_t& Tile::getLevels() const
{
    return this->levels;
}

void Tile::setTileOrigin(const irr::core::vector3df& coord)
{
    this->tileOrigin = coord;
    // the origin should have this->levels.getLevel(directions::Directions::TopRight) but the mesh
    // might already have an internal level -> substract that away
    this->tileOrigin.Y =
        static_cast<float>(this->levels.getLevel(directions::Directions::TopRight) - this->originLevel);
}

void Tile::setOriginLevel(const irr::f32 level)
{
    this->originLevel = level;
}

void Tile::scaleTextureCoordinates(const irr::core::vector2df scale)
{
    for (irr::u32 i = 0; i < this->relativeVertices.size(); i++)
    {
        this->relativeVertices[i].TCoords *= scale;
        this->relativeVertices[i].TCoords2 *= scale;
        debugOutLevel(Debug::DebugLevels::onEvent - 1,
                      "scaling: vertex",
                      vector3dToStringW(this->relativeVertices[i].Pos),
                      "texture coordintaes: i=",
                      i,
                      "x=",
                      this->relativeVertices[i].TCoords.X,
                      "y=",
                      this->relativeVertices[i].TCoords.Y);
    }
}

void Tile::moveTextureCoordinates(const irr::core::vector2df offset)
{
    for (irr::u32 i = 0; i < this->relativeVertices.size(); i++)
    {
        this->relativeVertices[i].TCoords += offset;
        this->relativeVertices[i].TCoords2 += offset;
        debugOutLevel(Debug::DebugLevels::onEvent - 1,
                      "moving: new vertex texture coordintaes: i=",
                      i,
                      "x=",
                      this->relativeVertices[i].TCoords.X,
                      "y=",
                      this->relativeVertices[i].TCoords.Y);
    }
}

irr::video::IImage* Tile::getImage() const
{
    return this->image;
}

void Tile::setImage(irr::video::IImage* image_)
{
    // grab the image so it doesn't get deleted while used by the tile
    image_->grab();
    this->image = image_;
}

// there are only two possibilities:
// 1) the vertex already exists -> we just need its absolute index
// 2) the vertex doesn't exist -> we create it and store store the absolute index ourselves
// so we never depend on something that happens after appendVertices for this tile.
irr::u32 Tile::appendVertices(std::vector<irr::video::S3DVertex2TCoords>& vertices, const irr::u32 startIndex)
{
    // check if any of the neighboring sides have generated the vertices already
    // and if not then generate them
    irr::u32 appendedVertices = 0;
    for (irr::u32 v = 0; v < this->relativeVertices.size(); v++)
    {
        const irr::video::S3DVertex2TCoords vertex = this->relativeVertices[v];
        irr::u32 absoluteIndex = 0;
        const directions::Directions generatedBy = this->neighborKnowsAbsoluteIndexOf(vertex, absoluteIndex);
        if (generatedBy == directions::Directions::NumDirections)
        {
            // not generated yet
            this->appendVertex(v, vertices);
            this->relativeToAbsoluteIndex[v] = startIndex + appendedVertices;
            this->knowsAbsoluteIndex[v] = true;
            appendedVertices++;
            debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                          this,
                          "added",
                          appendedVertices,
                          "new vertices, last one at index",
                          startIndex + appendedVertices,
                          "and position",
                          vector3dToStringW(this->relativeVertices[v].Pos));
        }
        else
        {
            // we got the absolute index in the call to neighborKnowsAbsoluteIndexOf
            this->relativeToAbsoluteIndex[v] = absoluteIndex;
            this->knowsAbsoluteIndex[v] = true;
        }
    }

    // all necessary vertices exist now so we can generate the indices
    this->generateIndices();
    return appendedVertices;
}

irr::u32 Tile::insertIndices(irr::scene::IIndexBuffer& indexBuffer, const irr::u32 startIndex)
{
    for (irr::u32 i = 0; i < this->indices.size(); i++)
    {
        indexBuffer.setValue(startIndex + i, this->indices[i]);
    }
    return this->indices.size();
}

irr::u32 Tile::insertIndices(irr::core::array<irr::u32>& array, const irr::u32 startIndex)
{
    for (irr::u32 i = 0; i < this->indices.size(); i++)
    {
        array[startIndex + i] = this->indices[i];
    }
    return this->indices.size();
}

irr::u32 Tile::appendIndices(irr::scene::IIndexBuffer& indexBuffer)
{
    for (irr::u32 i = 0; i < this->indices.size(); i++)
    {
        indexBuffer.push_back(this->indices[i]);
    }
    return indices.size();
}

irr::u32 Tile::indicesSize() const
{
    return this->indices.size();
}

void Tile::addVertexDefinition(const irr::video::S3DVertex2TCoords& vertex)
{
    // find the direction the vertex belongs to
    const directions::Directions direction = this->getVectorDirection(vertex.Pos);
    if (direction == directions::Directions::NumDirections)
    {
        Error::errTerminate("couldn't find direction inside tile for vertex",
                            vector3dToStringW(vertex.Pos));
    }
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 3,
                  "adding vertex at",
                  vector3dToStringW(vertex.Pos),
                  "with texture coordinates",
                  vector2dToStringW(vertex.TCoords));
    this->relativeVertices.push_back(vertex);
    this->knowsAbsoluteIndex.push_back(false);
    this->relativeToAbsoluteIndex.push_back(0);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "added vertex",
                  vector3dToStringW(vertex.Pos),
                  "in direction",
                  directions::directionToString(direction));
}

void Tile::addIndexDefinition(const irr::u32 index)
{
    this->relativeIndices.push_back(index);
}

void Tile::rotateClockwise(irr::video::IVideoDriver* videoDriver)
{
    // rotate the vertices in space
    irr::core::matrix4 rotMatrix;
    rotMatrix.setRotationDegrees(irr::core::vector3df(0.0f, -90.0f, 0.0f));

    const irr::core::vector3df rotationPivot(0.5, 0.5, 0.5); // rotate around the center of the tile
    for (irr::u32 i = 0; i < this->relativeVertices.size(); ++i)
    {
        relativeVertices[i].Pos -= rotationPivot;
        rotMatrix.inverseRotateVect(relativeVertices[i].Pos);
        relativeVertices[i].Pos += rotationPivot;
        // normals don't need to be offset
        rotMatrix.inverseRotateVect(relativeVertices[i].Normal);
        // because the tileMap actually shares vertices between tiles the rotated tile can't just
        // use the normal texture image. That image has to be rotated, too.
        // Otherwise there would be visible texture artifacts on the borders between rotated tiles
        // and normal ones.
        // But because the texture is rotated the texture coordinates must be rotated, too!
        relativeVertices[i].TCoords =
            irr::core::vector2df(static_cast<irr::f32>(1.0 - relativeVertices[i].TCoords.Y),
                                 relativeVertices[i].TCoords.X);
        relativeVertices[i].TCoords2 =
            irr::core::vector2df(static_cast<irr::f32>(1.0 - relativeVertices[i].TCoords2.Y),
                                 relativeVertices[i].TCoords2.X);
    }

    // rotate the internal levels
    // the level at x=0, z=0 internal mesh coordinates will change -> save old internal level to
    // update relative level after rotating
    const irr::f32 oldOriginInternalLevel = this->levels.getLevel(directions::Directions::TopRight);
    this->levels.rotateClockwise();
    // e.g. originLevel was 1, internal was 7 rotation-> internal = 6 -> origin needs to be 0
    this->originLevel = this->originLevel -
        (oldOriginInternalLevel - this->levels.getLevel(directions::Directions::TopRight));

    if (videoDriver != nullptr)
    {
        this->image = ImageManipulation::rotateClockwise(this->image, videoDriver);
    }
}

void Tile::printTextureCoordinatesOntoImage()
{
    for (irr::u32 v = 0; v < this->relativeVertices.size(); v++)
    {
        irr::video::S3DVertex2TCoords vx = this->relativeVertices[v];
        irr::core::vector2d<irr::u32> pixelCoordinates(static_cast<irr::u32>(vx.TCoords.X) *
                                                           this->image->getDimension().Width,
                                                       static_cast<irr::u32>(vx.TCoords.Y) *
                                                           this->image->getDimension().Height);
        if (pixelCoordinates.X >= this->image->getDimension().Width)
        {
            pixelCoordinates.X--;
        }
        if (pixelCoordinates.Y >= this->image->getDimension().Height)
        {
            pixelCoordinates.Y--;
        }
        this->image->setPixel(pixelCoordinates.X, pixelCoordinates.Y, irr::video::SColor(255, 255, 0, 0));
    }
}

directions::Directions Tile::getVectorDirection(const irr::core::vector3df& p)
{
    if (-Tile::positionTolerance < p.X and p.X < Tile::positionTolerance)
    {
        // right side of tile
        if (-Tile::positionTolerance < p.Z and p.Z < Tile::positionTolerance)
        {
            return directions::Directions::TopRight;
        }
        else if (1.0 - Tile::positionTolerance < p.Z and p.Z < 1.0 + Tile::positionTolerance)
        {
            return directions::Directions::BottomRight;
        }
        else
        {
            return directions::Directions::Right;
        }
    }
    else if (1.0 - Tile::positionTolerance < p.X and p.X < 1.0 + Tile::positionTolerance)
    // left side
    {
        if (-Tile::positionTolerance < p.Z and p.Z < Tile::positionTolerance)
        {
            return directions::Directions::TopLeft;
        }
        else if (1.0 - Tile::positionTolerance < p.Z and p.Z < 1.0 + Tile::positionTolerance)
        {
            return directions::Directions::BottomLeft;
        }
        else
        {
            return directions::Directions::Left;
        }
    }
    else if (-Tile::positionTolerance < p.Z and p.Z < Tile::positionTolerance)
    {
        // no need to check the corners because they have been checked already by the ifs above
        return directions::Directions::Top;
    }
    else if (1.0 - Tile::positionTolerance < p.Z and p.Z < 1.0 + Tile::positionTolerance)
    {
        return directions::Directions::Bottom;
    }
    else if (-Tile::positionTolerance < p.X and p.X < 1.0 + Tile::positionTolerance and
             -Tile::positionTolerance < p.Z and p.Z < 1.0 + Tile::positionTolerance)
    {
        return directions::Directions::Internal;
    }
    return directions::Directions::NumDirections;
}

bool Tile::isWalkable(const irr::f32 x,
                      const irr::f32 z,
                      const irr::f32 dx,
                      const irr::f32 dz,
                      const irr::u32 nx,
                      const irr::u32 nz) const
{
    const irr::f32 heightAtXZ = this->levels.getHeight(x, z);

    const irr::f32 stepX = static_cast<irr::f32>((2.0 * dx) / nx);
    const irr::f32 stepZ = static_cast<irr::f32>((2.0 * dz) / nz);
    for (irr::f32 testX = x - dx; testX < x + dx; testX += stepX)
    {
        for (irr::f32 testZ = z - dz; testZ < z + dz; testZ += stepZ)
        {
            if (testX < 0 or testX > 1.0 or testZ < 0 or testZ > 1.0)
            {
                continue;
            }
            const irr::f32 testHeight = this->levels.getHeight(testX, testZ);
            debugOutLevel(Debug::DebugLevels::secondOrderLoop, "pathing for point", x, z, "height=", heightAtXZ, "testPoint", testX, testZ, "height=", testHeight);
            // no more height difference than half a level
            if (std::fabs(heightAtXZ - testHeight) > 0.5)
            {
                debugOutLevel(Debug::DebugLevels::secondOrderLoop, "height difference > 0.5 -> returning false");
                return false;
            }
        }
    }

    debugOutLevel(Debug::DebugLevels::secondOrderLoop, "all height differences < 0.5 -> returning true");
    return true;
}

/* ########################################################################
 * private functions
 * ######################################################################## */

irr::core::vector3df Tile::getAbsolutePosition(const irr::core::vector3df relativePosition) const
{
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 3,
                  "called getAbsolutePosition with relativePosition=",
                  vector3dToStringW(relativePosition),
                  "and levels=",
                  this->levels.to_string());
    // the correct level-height is already set by the tileOrigin and relativePosition. But the
    // offset isn't used yet.
    // setting the offset here means all neighboring tiles can still compare against the relative
    // position of the base levels without having to use the offset. (as the absolute position is
    // never actually used in comparing which vertices to use from neighboring tiles)
    irr::core::vector3df retVal;
    retVal.X = (relativePosition.X + this->tileOrigin.X);
    retVal.Y = (relativePosition.Y + this->tileOrigin.Y +
                this->levels.getSmoothedOffset(relativePosition.X, relativePosition.Z));
    retVal.Z = (relativePosition.Z + this->tileOrigin.Z);
    return retVal;
}

irr::core::vector3df Tile::getPositionInsideNeighbor(const irr::core::vector3df& relativePosition,
                                                     const directions::Directions neighbor) const
{
    if (neighbor == directions::Directions::NumDirections or
        this->neighbors[static_cast<size_t>(neighbor)] == nullptr)
    {
        Error::errTerminate("can't get position inside neighbor if that neighbor doesn't exist. "
                            "(can't get height difference)");
    }
    // relative level 0 in both tiles can translate to different heights in world coordinates
    // because each vertex gets added the this->tileOrigin.Y
    const irr::f32 heightDifference =
        (tileOrigin.Y - neighbors[static_cast<size_t>(neighbor)]->tileOrigin.Y);
    debugOutLevel(Debug::secondOrderLoop,
                  this,
                  "determined height difference to",
                  heightDifference,
                  "from this height=",
                  this->tileOrigin.Y,
                  "and other:",
                  neighbors[static_cast<size_t>(neighbor)]->tileOrigin.Y);
    // if this tile in higher than the neighbor heightDifference will be positive -> our Y=0 is
    // Y=heightDifference inside the neighbor
    switch (neighbor)
    {
        case directions::Directions::TopLeft:
            return relativePosition + irr::core::vector3df(-1.0, heightDifference, 1.0f);
        case directions::Directions::Top:
            return relativePosition + irr::core::vector3df(0.0f, heightDifference, 1.0f);
        case directions::Directions::TopRight:
            return relativePosition + irr::core::vector3df(1.0f, heightDifference, 1.0f);
        case directions::Directions::Right:
            return relativePosition + irr::core::vector3df(1.0f, heightDifference, 0.0f);
        case directions::Directions::BottomRight:
            return relativePosition + irr::core::vector3df(1.0f, heightDifference, -1.0f);
        case directions::Directions::Bottom:
            return relativePosition + irr::core::vector3df(0.0f, heightDifference, -1.0f);
        case directions::Directions::BottomLeft:
            return relativePosition + irr::core::vector3df(-1.0f, heightDifference, -1.0f);
        case directions::Directions::Left:
            return relativePosition + irr::core::vector3df(-1.0f, heightDifference, 0.0f);
        case directions::Directions::Internal:
            return relativePosition;
        case directions::Directions::NumDirections:
            Error::errTerminate("trying to get position inside neighbor "
                                "directions::Directions::NumDirections which doesn't make any "
                                "sense");
        default:
            Error::errTerminate("Unhandled neighbor with id = ", static_cast<size_t>(neighbor));
    }
}

// assuming this function will only be called if the heighbor hasn't generated the vertices (it's
// private for a reason)
void Tile::appendVertex(const irr::u32 arrayPosition, std::vector<irr::video::S3DVertex2TCoords>& vertices)
{
    irr::video::S3DVertex2TCoords vertex;
    // the vertex positions are only relative for the internal vertices (e.g. position (0,0,0)) ->
    // update them to match world coordinates
    vertex.Pos = this->getAbsolutePosition(this->relativeVertices[arrayPosition].Pos);
    vertex.Normal = this->relativeVertices[arrayPosition].Normal;
    vertex.TCoords = this->relativeVertices[arrayPosition].TCoords;
    vertex.TCoords2 = this->relativeVertices[arrayPosition].TCoords2;
    vertices.push_back(vertex);
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9,
                  this,
                  "appended vertex at",
                  vector3dToStringW(vertex.Pos),
                  "with texture coordinates:",
                  vector2dToStringW(vertex.TCoords));
}

// this function assumes all the necessary vertices have been generated
irr::u32 Tile::generateIndices()
{
    if (this->indicesSize() != 0)
    {
        Error::errTerminate("generateIndices called but they have already been generated");
    }
    // the full geometry is already defined by the relativeIndices. But we need these as
    // absoluteIndices.
    // so walk over the array and translate.
    for (unsigned int i = 0; i < relativeIndices.size(); i++)
    {
        const irr::u32 relativeIndex = this->relativeIndices[i];
        this->indices.push_back(this->relativeToAbsoluteIndex[relativeIndex]);
    }
    return this->relativeIndices.size();
}


bool Tile::knowsAbsoluteIndexOf(const irr::video::S3DVertex2TCoords& v, irr::u32& absoluteIndex) const
{
    for (irr::u32 i = 0; i < this->relativeVertices.size(); i++)
    {
        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                      this,
                      "comparing positions p=",
                      vector3dToStringW(v.Pos),
                      "and",
                      vector3dToStringW(this->relativeVertices[i].Pos));
        // combine vertices that are sufficiently equal
        if (this->relativeVertices[i].Pos.equals(v.Pos, Tile::positionTolerance) and
            this->relativeVertices[i].Normal.dotProduct(v.Normal) >=
                std::cos(Tile::normalTolerance * irr::core::DEGTORAD))
        {
            if (this->knowsAbsoluteIndex[i])
            {
                // no need to do anything to the normals (even though they could be up to
                // Tile::normalTolerance degree apart because the final mesh will get it's normals
                // recalculated anyways
                absoluteIndex = this->relativeToAbsoluteIndex[i];
                debugOutLevel(Debug::DebugLevels::secondOrderLoop + 1, this, "found vertex at relative index i=", i, "absolute index=", absoluteIndex);
                return true;
            }
            // we don't break on the else case because our model might have 2 vertices at the same
            // position and we generated the other one.
        }
    }
    return false;
}

directions::Directions Tile::neighborKnowsAbsoluteIndexOf(const irr::video::S3DVertex2TCoords& v,
                                                          irr::u32& absoluteIndex) const
{
    const directions::Directions vertexDirection = this->getVectorDirection(v.Pos);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  this,
                  "asking neighbors for absoluteIndex of vertex at",
                  vector3dToStringW(v.Pos),
                  "in direction",
                  directions::directionToString(vertexDirection));
    switch (vertexDirection)
    {
        case directions::Directions::TopLeft:
            // could have been generated by left, topleft or top neighbor
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Left, absoluteIndex))
            {
                return directions::Directions::Left;
            }
            else if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::TopLeft, absoluteIndex))
            {
                return directions::Directions::TopLeft;
            }
            else if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Top, absoluteIndex))
            {
                return directions::Directions::Top;
            }
            break;
        case directions::Directions::Top:
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Top, absoluteIndex))
            {
                return directions::Directions::Top;
            }
            break;
        case directions::Directions::TopRight:
            // could have been generated by top, topright or right neighbor
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Top, absoluteIndex))
            {
                return directions::Directions::Top;
            }
            else if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::TopRight, absoluteIndex))
            {
                return directions::Directions::TopRight;
            }
            else if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Right, absoluteIndex))
            {
                return directions::Directions::Right;
            }
            break;
        case directions::Directions::Right:
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Right, absoluteIndex))
            {
                return directions::Directions::Right;
            }
            break;
        case directions::Directions::BottomRight:
            // could have been generated by right, bottomright or bottom neighbor
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Right, absoluteIndex))
            {
                return directions::Directions::Right;
            }
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::BottomRight, absoluteIndex))
            {
                return directions::Directions::BottomRight;
            }
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Bottom, absoluteIndex))
            {
                return directions::Directions::Bottom;
            }
            break;
        case directions::Directions::Bottom:
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Bottom, absoluteIndex))
            {
                return directions::Directions::Bottom;
            }
            break;
        case directions::Directions::BottomLeft:
            // could have been generated by bottom, bottomleft or left neighbor
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Bottom, absoluteIndex))
            {
                return directions::Directions::Bottom;
            }
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::BottomLeft, absoluteIndex))
            {
                return directions::Directions::BottomLeft;
            }
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Left, absoluteIndex))
            {
                return directions::Directions::Left;
            }
            break;
        case directions::Directions::Left:
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Left, absoluteIndex))
            {
                return directions::Directions::Left;
            }
            break;
        case directions::Directions::Internal:
            if (specificNeighborKnowsAbsoluteIndexOf(v, directions::Directions::Internal, absoluteIndex))
            {
                return directions::Directions::Internal;
            }
            break;
        case directions::Directions::NumDirections:
            Error::errContinue("the position", vector3dToStringW(v.Pos), " isn't shared with any neighboring tile");
            break;
        default:
            Error::errTerminate("Unhandled tile direction with id", static_cast<size_t>(vertexDirection));
            break;
    }
    return directions::Directions::NumDirections;
}

bool Tile::specificNeighborKnowsAbsoluteIndexOf(const irr::video::S3DVertex2TCoords& v,
                                                const directions::Directions neighbor,
                                                irr::u32& absoluteIndex) const
{
    debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                  this,
                  "asking neighbor in direction",
                  directions::directionToString(neighbor));
    if (neighbors[static_cast<size_t>(neighbor)] == nullptr)
    {
        return false;
    }
    // actually asking about an internal vertex -> the neighbor who knows the index is this tile
    // itself
    if (neighbor == directions::Directions::Internal)
    {
        return this->knowsAbsoluteIndexOf(v, absoluteIndex);
    }
    irr::video::S3DVertex2TCoords vInsideNeighbor = v;
    vInsideNeighbor.Pos = this->getPositionInsideNeighbor(v.Pos, neighbor);
    return neighbors[static_cast<size_t>(neighbor)]->knowsAbsoluteIndexOf(vInsideNeighbor, absoluteIndex);
}
