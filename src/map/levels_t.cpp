/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath> // std::fabs

#include <irrlicht/irrMath.h> // defines FLT_MAX: the maximum value a float can have

#include "../utils/stringwstream.h" // stringwstream.h needs to be included before debug.h and error.h (which are included by levels_t.h) because they use use operator<< on a stringw

#include "levels_t.h"

#include "../utils/stringwparser.h"

using namespace tileMap;

Levels_t::Levels_t() {}

Levels_t::Levels_t(const irr::f32 levelTopLeft_,
                   const irr::f32 levelTopRight_,
                   const irr::f32 levelBottomLeft_,
                   const irr::f32 levelBottomRight_)
    : levelTopLeft(levelTopLeft_)
    , levelTopRight(levelTopRight_)
    , levelBottomLeft(levelBottomLeft_)
    , levelBottomRight(levelBottomRight_)
{
}

void Levels_t::rotateClockwise()
{
    const irr::f32 tmpLevel = this->levelTopLeft;
    this->levelTopLeft = this->levelBottomLeft;
    this->levelBottomLeft = this->levelBottomRight;
    this->levelBottomRight = this->levelTopRight;
    this->levelTopRight = tmpLevel;

    const irr::f32 tmpOffset = this->offsetTopLeft;
    this->offsetTopLeft = this->offsetBottomLeft;
    this->offsetBottomLeft = this->offsetBottomRight;
    this->offsetBottomRight = this->offsetTopRight;
    this->offsetTopRight = tmpOffset;

    // first rotate all rampDirections for the corners to their new positions
    const directions::Directions tmpRampDirection = this->rampDirectionTopLeft;
    this->rampDirectionTopLeft = this->rampDirectionBottomLeft;
    this->rampDirectionBottomLeft = this->rampDirectionBottomRight;
    this->rampDirectionBottomRight = this->rampDirectionTopRight;
    this->rampDirectionTopRight = tmpRampDirection;
    // now the ramp directions point the wrong way. They need to be rotated too to accout for their
    // new position.
    // (easy example: a ramp pointing to the top must point to the right after being rotatet
    // clockwise)
    this->rampDirectionTopLeft = directions::rotateClockwise(this->rampDirectionTopLeft);
    this->rampDirectionBottomLeft = directions::rotateClockwise(this->rampDirectionBottomLeft);
    this->rampDirectionBottomRight = directions::rotateClockwise(this->rampDirectionBottomRight);
    this->rampDirectionTopRight = directions::rotateClockwise(this->rampDirectionTopRight);
}

std::string Levels_t::to_string() const
{
    std::stringstream retval;
    retval << "[Levels_t ";
    retval << "levelTopLeft=" << levelTopLeft;
    if (this->rampDirectionTopLeft != directions::Directions::Internal)
    {
        retval << " ramp=" << directions::directionToString(this->rampDirectionTopLeft);
    }
    retval << " levelTopRight=" << levelTopRight;
    if (this->rampDirectionTopRight != directions::Directions::Internal)
    {
        retval << " ramp=" << directions::directionToString(this->rampDirectionTopRight);
    }
    retval << " levelBottomLeft=" << levelBottomLeft;
    if (this->rampDirectionBottomLeft != directions::Directions::Internal)
    {
        retval << " ramp=" << directions::directionToString(this->rampDirectionBottomLeft);
    }
    retval << " levelBottomRight=" << levelBottomRight;
    if (this->rampDirectionBottomRight != directions::Directions::Internal)
    {
        retval << " ramp=" << directions::directionToString(this->rampDirectionBottomRight);
    }
    retval << "]";
    return retval.str();
}

irr::core::stringw Levels_t::getParsableString() const
{
    // the level is saved as "TopLeft TopRight BottomLeft BottomRight RampTopLeft RampTopRight
    // RampBottomLeft RampBottomRight"
    irr::core::stringw retVal(L"");
    // because if the last-to-print ramp direction exists we need to print all others it is best to
    // walk the directions backwards from the way they are saved
    bool attach = false;
    if (this->rampDirectionBottomRight != directions::Directions::Internal)
    {
        retVal = irr::core::stringw(L" ") + directions::getParsableString(this->rampDirectionBottomRight);
        attach = true;
    }
    if (attach or this->rampDirectionBottomLeft != directions::Directions::Internal)
    {
        retVal = irr::core::stringw(L" ") + directions::getParsableString(this->rampDirectionBottomLeft) + retVal;
        attach = true;
    }
    if (attach or this->rampDirectionTopRight != directions::Directions::Internal)
    {
        retVal = irr::core::stringw(L" ") + directions::getParsableString(this->rampDirectionTopRight) + retVal;
        attach = true;
    }
    if (attach or this->rampDirectionTopLeft != directions::Directions::Internal)
    {
        retVal = irr::core::stringw(L" ") + directions::getParsableString(this->rampDirectionTopLeft) + retVal;
    }
    retVal = irr::core::stringw(this->levelTopLeft) + irr::core::stringw(L" ") +
        irr::core::stringw(this->levelTopRight) + irr::core::stringw(L" ") +
        irr::core::stringw(this->levelBottomLeft) + irr::core::stringw(L" ") +
        irr::core::stringw(this->levelBottomRight) + irr::core::stringw(L" ") +
        irr::core::stringw(this->offsetTopLeft) + irr::core::stringw(L" ") +
        irr::core::stringw(this->offsetTopRight) + irr::core::stringw(L" ") +
        irr::core::stringw(this->offsetBottomLeft) + irr::core::stringw(L" ") +
        irr::core::stringw(this->offsetBottomRight) + retVal;
    return retVal;
}

Levels_t::BinaryData Levels_t::getBinaryData() const
{
    BinaryData binaryData;
    binaryData.levelTopLeft = this->levelTopLeft;
    binaryData.levelTopRight = this->levelTopRight;
    binaryData.levelBottomLeft = this->levelBottomLeft;
    binaryData.levelBottomRight = this->levelBottomRight;
    binaryData.rampDirectionTopLeft = static_cast<int16_t>(this->rampDirectionTopLeft);
    binaryData.rampDirectionTopRight = static_cast<int16_t>(this->rampDirectionTopRight);
    binaryData.rampDirectionBottomLeft = static_cast<int16_t>(this->rampDirectionBottomLeft);
    binaryData.rampDirectionBottomRight = static_cast<int16_t>(this->rampDirectionBottomRight);
    binaryData.offsetTopLeft = this->offsetTopLeft;
    binaryData.offsetTopRight = this->offsetTopRight;
    binaryData.offsetBottomLeft = this->offsetBottomLeft;
    binaryData.offsetBottomRight = this->offsetBottomRight;
    return binaryData;
}

void Levels_t::setFromBinary(const BinaryData binaryData)
{
    this->levelTopLeft = binaryData.levelTopLeft;
    this->levelTopRight = binaryData.levelTopRight;
    this->levelBottomLeft = binaryData.levelBottomLeft;
    this->levelBottomRight = binaryData.levelBottomRight;
    this->rampDirectionTopLeft = static_cast<directions::Directions>(binaryData.rampDirectionTopLeft);
    this->rampDirectionTopRight = static_cast<directions::Directions>(binaryData.rampDirectionTopRight);
    this->rampDirectionBottomLeft = static_cast<directions::Directions>(binaryData.rampDirectionBottomLeft);
    this->rampDirectionBottomRight = static_cast<directions::Directions>(binaryData.rampDirectionBottomRight);
    this->offsetTopLeft = binaryData.offsetTopLeft;
    this->offsetTopRight = binaryData.offsetTopRight;
    this->offsetBottomLeft = binaryData.offsetBottomLeft;
    this->offsetBottomRight = binaryData.offsetBottomRight;
}

void Levels_t::normalize()
{
    const irr::f32 offset = min4(levelTopLeft, levelTopRight, levelBottomLeft, levelBottomRight);
    levelTopLeft -= offset;
    levelTopRight -= offset;
    levelBottomLeft -= offset;
    levelBottomRight -= offset;
}

irr::core::vector3df Levels_t::getNormalVector(const irr::f32 x, const irr::f32 z) const
{
    if (x < 0.0 or x > 1.0 or z < 0.0 or z > 1.0)
    {
        return irr::core::vector3df(-FLT_MAX, -FLT_MAX, -FLT_MAX);
    }
    irr::f32 heightTopRight = this->levelTopRight + this->offsetTopRight;
    irr::f32 heightTopLeft = this->levelTopLeft + this->offsetTopLeft;
    irr::f32 heightBottomRight = this->levelBottomRight + this->offsetBottomRight;
    irr::f32 heightBottomLeft = this->levelBottomLeft + this->offsetBottomLeft;

    // Edges point in a counterclockwise direction ie BottomEdge is from bottom left to bottom right
    // Top edge is from top right to top left, etc
    irr::core::vector3df bottomEdge(-1, heightBottomRight - heightBottomLeft, 0);
    irr::core::vector3df rightEdge(0, heightTopRight - heightBottomRight, -1);
    irr::core::vector3df topEdge(1, heightTopLeft - heightTopRight, 0);
    irr::core::vector3df leftEdge(0, heightBottomLeft - heightTopLeft, 1);
    // The second vector is always in the wrong direction, so multiply by -1 to flip it
    // TODO Confirm that 0,0 is in the top right, and if so, determine if the x and z
    //      coordinates used in the edge vectors are correct
    //      irrlicht is left handed, so this may be backwards...
    if (x < 0.5)
    {
        if (z < 0.5)
        {
            return -1 * rightEdge.crossProduct(topEdge);
        }
        else
        {
            return -1 * bottomEdge.crossProduct(rightEdge);
        }
    }
    else
    {
        if (z < 0.5)
        {
            return -1 * topEdge.crossProduct(leftEdge);
        }
        else
        {
            return -1 * leftEdge.crossProduct(bottomEdge);
        }
    }
}

irr::f32 Levels_t::getHeight(const irr::f32 x, const irr::f32 z) const
{
    if (x < 0.0 or x > 1.0 or z < 0.0 or z > 1.0)
    {
        return -FLT_MAX;
    }
    if (x < 0.5)
    {
        if (z < 0.5)
        {
            return this->levelTopRight + this->offsetTopRight;
        }
        else
        {
            return this->levelBottomRight + this->offsetBottomRight;
        }
    }
    else
    {
        if (z < 0.5)
        {
            return this->levelTopLeft + this->offsetTopLeft;
        }
        else
        {
            return this->levelBottomLeft + this->offsetBottomLeft;
        }
    }
}

irr::f32 Levels_t::getSmoothedHeight(const irr::f32 x, const irr::f32 z) const
{
    // TODO: find out if this check is really needed or if this produces (rare) wrong results for
    // movement on the terrain
    if (x < 0.0 or x > 1.0 or z < 0.0 or z > 1.0)
    {
        return -FLT_MAX;
    }
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 8, this->to_string());
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 8, "getSmoothedHeight called with x=", x, "z=", z);
    const irr::f32 heightFactorTopLeft = this->getHeightProportion(
        static_cast<irr::f32>(std::sqrt(std::pow(x - 1.0, 2.0) + std::pow(z, 2.0))));
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9,
                  "factor",
                  heightFactorTopLeft,
                  "for level TopLeft=",
                  this->levelTopLeft,
                  "and offsetTopLeft=",
                  this->offsetTopLeft);
    const irr::f32 heightFactorTopRight =
        this->getHeightProportion(static_cast<irr::f32>(std::sqrt(std::pow(x, 2.0) + std::pow(z, 2.0))));
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9,
                  "factor",
                  heightFactorTopRight,
                  "for level TopRight=",
                  this->levelTopRight,
                  "and offsetTopRight=",
                  this->offsetTopRight);
    const irr::f32 heightFactorBottomLeft = this->getHeightProportion(
        static_cast<irr::f32>(std::sqrt(std::pow(x - 1.0, 2.0) + std::pow(z - 1.0, 2.0))));
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9,
                  "factor",
                  heightFactorBottomLeft,
                  "for level BottomLeft=",
                  this->levelBottomLeft,
                  "and offsetBottomLeft=",
                  this->offsetBottomLeft);
    const irr::f32 heightFactorBottomRight = this->getHeightProportion(
        static_cast<irr::f32>(std::sqrt(std::pow(x, 2.0) + std::pow(z - 1.0, 2.0))));
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9,
                  "factor",
                  heightFactorBottomRight,
                  "for level BottomRight=",
                  this->levelBottomRight,
                  "and offsetBottomRight=",
                  this->offsetBottomRight);
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 8,
                  "returning",
                  (this->levelTopLeft + this->offsetTopLeft) * heightFactorTopLeft +
                      (this->levelTopRight + this->offsetTopRight) * heightFactorTopRight +
                      (this->levelBottomLeft + this->offsetBottomLeft) * heightFactorBottomLeft +
                      (this->levelBottomRight + this->offsetBottomRight) * heightFactorBottomRight);
    return static_cast<irr::f32>((this->levelTopLeft + this->offsetTopLeft) * heightFactorTopLeft +
                                 (this->levelTopRight + this->offsetTopRight) * heightFactorTopRight +
                                 (this->levelBottomLeft + this->offsetBottomLeft) * heightFactorBottomLeft +
                                 (this->levelBottomRight + this->offsetBottomRight) * heightFactorBottomRight);
}

irr::f32 Levels_t::getSmoothedOffset(const irr::f32 x, const irr::f32 z) const
{
    // TODO: find out if this check is really needed or if this produces (rare) wrong results for
    // movement on the terrain
    if (x < 0.0 or x > 1.0 or z < 0.0 or z > 1.0)
    {
        return -FLT_MAX;
    }
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 8, this->to_string());
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 8, "getSmoothedOffset called with x=", x, "z=", z);
    const irr::f32 heightFactorTopLeft = this->getHeightProportion(
        static_cast<irr::f32>(std::sqrt(std::pow(x - 1.0, 2.0) + std::pow(z, 2.0))));
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9, "factor", heightFactorTopLeft, "for offsetTopLeft=", this->offsetTopLeft);
    const irr::f32 heightFactorTopRight =
        this->getHeightProportion(static_cast<irr::f32>(std::sqrt(std::pow(x, 2.0) + std::pow(z, 2.0))));
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9, "factor", heightFactorTopRight, "for offsetTopRight=", this->offsetTopRight);
    const irr::f32 heightFactorBottomLeft = this->getHeightProportion(
        static_cast<irr::f32>(std::sqrt(std::pow(x - 1.0, 2.0) + std::pow(z - 1.0, 2.0))));
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9, "factor", heightFactorBottomLeft, "for offsetBottomLeft=", this->offsetBottomLeft);
    const irr::f32 heightFactorBottomRight = this->getHeightProportion(
        static_cast<irr::f32>(std::sqrt(std::pow(x, 2.0) + std::pow(z - 1.0, 2.0))));
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 9, "factor", heightFactorBottomRight, "for offsetBottomRight=", this->offsetBottomRight);
    const irr::f32 retVal = static_cast<irr::f32>(
        this->offsetTopLeft * heightFactorTopLeft + this->offsetTopRight * heightFactorTopRight +
        this->offsetBottomLeft * heightFactorBottomLeft + this->offsetBottomRight * heightFactorBottomRight);
    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 8, "returning", retVal);
    return retVal;
}

/* ########################################################################
 * private functions
 * ######################################################################## */


/* ########################################################################
 * functions in other namespaces
 * ######################################################################## */

// parsing utility for the levels
template <>
tileMap::Levels_t StringWParser::parseInternal<tileMap::Levels_t>(std::wstring str, size_t& readTo)
{
    // the level is saved as "TopLeft TopRight BottomLeft BottomRight RampTopLeft RampTopRight
    // RampBottomLeft RampBottomRight"
    tileMap::Levels_t retVal;

    size_t readToLocal;

    // levels parsing
    retVal.setLevel(directions::Directions::TopLeft, parseInternal<float>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out levels=",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);

    retVal.setLevel(directions::Directions::TopRight, parseInternal<float>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out levels=",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);

    retVal.setLevel(directions::Directions::BottomLeft, parseInternal<float>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out levels=",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);

    retVal.setLevel(directions::Directions::BottomRight, parseInternal<float>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out levels=",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);

    // offsets parsing
    retVal.setOffset(directions::Directions::TopLeft, parseInternal<float>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out offsets=",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);

    retVal.setOffset(directions::Directions::TopRight, parseInternal<float>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out offsets=",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);

    retVal.setOffset(directions::Directions::BottomLeft, parseInternal<float>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out offsets=",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);

    retVal.setOffset(directions::Directions::BottomRight, parseInternal<float>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out offsets=",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);

    // ramps parsing
    // is there still something left to parse?
    if (str.size() == 0)
    {
        return retVal;
    }
    // the ramp directions will start with a seperator, which will have been replaced by the call to
    // StringWParser::parse with ' '
    if (str[0] == L' ')
    {
        str = str.substr(1);
        readTo++;
    }

    retVal.setRampDirection(directions::Directions::TopLeft,
                            parseInternal<directions::Directions>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out rampDirection",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);

    // ramps might end at any time into the string (to save space when saving) -> always check if
    // there is still more to parse
    if (str.size() == 0)
    {
        return retVal;
    }
    if (str[0] == L' ')
    {
        str = str.substr(1);
        readTo++;
    }

    retVal.setRampDirection(directions::Directions::TopRight,
                            parseInternal<directions::Directions>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out rampDirection",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);
    if (str.size() == 0)
    {
        return retVal;
    }
    if (str[0] == L' ')
    {
        str = str.substr(1);
        readTo++;
    }

    retVal.setRampDirection(directions::Directions::BottomLeft,
                            parseInternal<directions::Directions>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out rampDirection",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);
    if (str.size() == 0)
    {
        return retVal;
    }
    if (str[0] == L' ')
    {
        str = str.substr(1);
        readTo++;
    }

    retVal.setRampDirection(directions::Directions::BottomRight,
                            parseInternal<directions::Directions>(str, readToLocal));
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out rampDirection",
                  retVal.to_string(),
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);

    return retVal;
}
