/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIRECTIONS_H
#define DIRECTIONS_H

#include <array>
#include <string>

#include <irrlicht/irrString.h>

class directions
{
   public:
    enum class Directions
    {
        TopLeft = 0,
        Top = 1,
        TopRight = 2,
        Right = 3,
        BottomRight = 4,
        Bottom = 5,
        BottomLeft = 6,
        Left = 7,
        Internal = 8,
        NumDirections = 9
    };

    static std::string directionToString(const Directions direction);
    static irr::core::stringw getParsableString(const Directions direction);
    /// @brief invertss the supplied direction
    /// @param direction
    /// @return the inverse of the direction. Internal stays Internal and NumDirections generates an
    /// error!
    static Directions invertDirection(const Directions direction);
    /// @brief rotate a direction clockwise by 90 degree
    /// @param direction
    /// @return the direction in 90 degree clockwise rotation from 'direction'
    static Directions rotateClockwise(const Directions direction);
    static Directions combineDirections(const Directions dir1, const Directions dir2);
};


#endif /* ifndef DIRECTIONS_H */
