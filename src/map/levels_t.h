/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LEVELS_H
#define LEVELS_H

#include <sstream> // used in Levels_t.to_string()

#include <irrlicht/irrTypes.h>
#include <irrlicht/vector3d.h>

#include "directions.h"
#include "../utils/debug.h"
#include "../utils/static/error.h"

namespace tileMap
{
    class Levels_t
    {
       public:
        /// @brief binary data used for saving/loading. Must be "Plain Old Data" (meaning no virtual
        /// functions, no base class, arrays, pointers or references)
        /// also make sure to only use data which sizeof() is platform independent
        // (see https://stackoverflow.com/questions/422830/structure-of-a-c-object-in-memory-vs-a-struct)
        struct BinaryData
        {
            irr::f32 levelTopLeft = 0;
            irr::f32 levelTopRight = 0;
            irr::f32 levelBottomLeft = 0;
            irr::f32 levelBottomRight = 0;
            int16_t rampDirectionTopLeft = static_cast<int16_t>(directions::Directions::Internal);
            int16_t rampDirectionTopRight = static_cast<int16_t>(directions::Directions::Internal);
            int16_t rampDirectionBottomLeft = static_cast<int16_t>(directions::Directions::Internal);
            int16_t rampDirectionBottomRight = static_cast<int16_t>(directions::Directions::Internal);
            irr::f32 offsetTopLeft = 0;
            irr::f32 offsetTopRight = 0;
            irr::f32 offsetBottomLeft = 0;
            irr::f32 offsetBottomRight = 0;
        };

        Levels_t();
        Levels_t(const irr::f32 levelTopLeft_,
                 const irr::f32 levelTopRight_,
                 const irr::f32 levelBottomLeft_,
                 const irr::f32 levelBottomRight_);

        inline irr::f32 getLevel(const directions::Directions direction) const
        {
            switch (direction)
            {
                case directions::Directions::TopLeft:
                    return this->levelTopLeft;
                case directions::Directions::TopRight:
                    return this->levelTopRight;
                case directions::Directions::BottomRight:
                    return this->levelBottomRight;
                case directions::Directions::BottomLeft:
                    return this->levelBottomLeft;
                case directions::Directions::Top:
                case directions::Directions::Right:
                case directions::Directions::Bottom:
                case directions::Directions::Left:
                case directions::Directions::Internal:
                case directions::Directions::NumDirections:
                default:
                    Error::errTerminate("called getLevel with invalid direction",
                                        directions::directionToString(direction));
            }
        }

        inline void setLevel(const directions::Directions direction, const irr::f32 level)
        {
            switch (direction)
            {
                case directions::Directions::TopLeft:
                    this->levelTopLeft = level;
                    break;
                case directions::Directions::TopRight:
                    this->levelTopRight = level;
                    break;
                case directions::Directions::BottomRight:
                    this->levelBottomRight = level;
                    break;
                case directions::Directions::BottomLeft:
                    this->levelBottomLeft = level;
                    break;
                case directions::Directions::Top:
                case directions::Directions::Right:
                case directions::Directions::Bottom:
                case directions::Directions::Left:
                case directions::Directions::Internal:
                case directions::Directions::NumDirections:
                default:
                    Error::errTerminate("called setLevel with invalid direction",
                                        directions::directionToString(direction));
            }
        }

        inline directions::Directions getRampDirection(const directions::Directions direction) const
        {
            switch (direction)
            {
                case directions::Directions::TopLeft:
                    return this->rampDirectionTopLeft;
                case directions::Directions::TopRight:
                    return this->rampDirectionTopRight;
                case directions::Directions::BottomRight:
                    return this->rampDirectionBottomRight;
                case directions::Directions::BottomLeft:
                    return this->rampDirectionBottomLeft;
                case directions::Directions::Top:
                case directions::Directions::Right:
                case directions::Directions::Bottom:
                case directions::Directions::Left:
                case directions::Directions::Internal:
                case directions::Directions::NumDirections:
                default:
                    Error::errTerminate("called getRampDirection with invalid direction",
                                        directions::directionToString(direction));
            }
        }

        inline void setRampDirection(const directions::Directions direction, const directions::Directions rampDirection)
        {
            switch (direction)
            {
                case directions::Directions::TopLeft:
                    this->rampDirectionTopLeft = rampDirection;
                    break;
                case directions::Directions::TopRight:
                    this->rampDirectionTopRight = rampDirection;
                    break;
                case directions::Directions::BottomRight:
                    this->rampDirectionBottomRight = rampDirection;
                    break;
                case directions::Directions::BottomLeft:
                    this->rampDirectionBottomLeft = rampDirection;
                    break;
                case directions::Directions::Top:
                case directions::Directions::Right:
                case directions::Directions::Bottom:
                case directions::Directions::Left:
                case directions::Directions::Internal:
                case directions::Directions::NumDirections:
                default:
                    Error::errTerminate("called setRampDirection with invalid direction",
                                        directions::directionToString(direction));
            }
        }

        inline irr::f32 getOffset(const directions::Directions direction) const
        {
            switch (direction)
            {
                case directions::Directions::TopLeft:
                    return this->offsetTopLeft;
                case directions::Directions::TopRight:
                    return this->offsetTopRight;
                case directions::Directions::BottomRight:
                    return this->offsetBottomRight;
                case directions::Directions::BottomLeft:
                    return this->offsetBottomLeft;
                case directions::Directions::Top:
                case directions::Directions::Right:
                case directions::Directions::Bottom:
                case directions::Directions::Left:
                case directions::Directions::Internal:
                case directions::Directions::NumDirections:
                default:
                    Error::errTerminate("called getOffset with invalid direction",
                                        directions::directionToString(direction));
            }
        }

        inline void setOffset(const directions::Directions direction, const irr::f32 offset)
        {
            switch (direction)
            {
                case directions::Directions::TopLeft:
                    this->offsetTopLeft = offset;
                    break;
                case directions::Directions::TopRight:
                    this->offsetTopRight = offset;
                    break;
                case directions::Directions::BottomRight:
                    this->offsetBottomRight = offset;
                    break;
                case directions::Directions::BottomLeft:
                    this->offsetBottomLeft = offset;
                    break;
                case directions::Directions::Top:
                case directions::Directions::Right:
                case directions::Directions::Bottom:
                case directions::Directions::Left:
                case directions::Directions::Internal:
                case directions::Directions::NumDirections:
                default:
                    Error::errTerminate("called setOffset with invalid direction",
                                        directions::directionToString(direction));
            }
        }

        /// @brief rotates the levels and ramp information 90\degree clockwise
        ///
        /// the ramp directions are also rotated accordingly. E.g. a ramp up in direction 'Top'
        /// would point up in direction 'Right' after rotating.
        void rotateClockwise();

        /// @brief returns a human-readable representation of the levels.
        std::string to_string() const;

        /// @brief get a string that will yield the same Levels_t if parsed via the stringwparser
        irr::core::stringw getParsableString() const;

        /// @brief get data that can be saved into a binary file
        BinaryData getBinaryData() const;

        /// @brief set from binary data read from a binary file
        void setFromBinary(const BinaryData binaryData);

        /// @brief scales the levels so that the minimum level is 0. Used to compare levels.
        void normalize();

        /// @brief Returns an estimation of the normal vector at a point
        irr::core::vector3df getNormalVector(const irr::f32 x, const irr::f32 z) const;

        /// @brief return the height closest to the position (x,z).
        ///
        /// Doesn't interpolate in any way but instead uses hard cutoffs around the level borders.
        /// If the center between two levels are hit the result to the left and bottom is returned
        /// (meaning (0.5,0.5) gets the level at bottomRight)
        /// @param x
        /// @param z the coordinates to get the height at. Level-relative (meaning x,z \in [0, 1])
        /// @return the height closest to (x,z). Returns -irr::FLT_MAX if (x,z) outside of [0, 1].
        irr::f32 getHeight(const irr::f32 x, const irr::f32 z) const;

        /// @brief get the smoothed height closest to (x,z)
        ///
        /// Use if you need a smoothed height value which doesn't contain the discrete jumps that
        /// getHeight() contains but is computationally more expensive.
        /// @param x
        /// @param z the coordinates to get the smoothed height at. Level-relative (meaning x,z \in
        /// [0,1])
        /// @return smooth height at (x,z) using levels and offsets for smoothing (see
        /// getSmoothedOffsets for smooth height using only offsets). Returns -irr::FLT_MAX if (x,z)
        /// outside of [0,1].
        irr::f32 getSmoothedHeight(const irr::f32 x, const irr::f32 z) const;

        /// @brief get the smoothed offsets closest to (x, z)
        ///
        /// this function exists because a tile when determining the absolute position of a vertex
        /// already has the level inside the vertex height (because the model was chosen based on
        /// the levels) but not the offsets and it needs only the smoothed offsets.
        /// @param x
        /// @param z the coordinates to get the smoothed height at. Level-relative (meaning x,z \in
        /// [0,1])
        /// @return smooth height at (x,z) smoothing only using the offsets (see getSmoothedHeight
        /// for smooth height with levels included). Returns -irr::FLT_MAX if (x,z) outside of
        /// [0,1].
        irr::f32 getSmoothedOffset(const irr::f32 x, const irr::f32 z) const;

       private:
        irr::f32 levelTopLeft = 0;
        irr::f32 levelTopRight = 0;
        irr::f32 levelBottomLeft = 0;
        irr::f32 levelBottomRight = 0;
        directions::Directions rampDirectionTopLeft = directions::Directions::Internal;
        directions::Directions rampDirectionTopRight = directions::Directions::Internal;
        directions::Directions rampDirectionBottomLeft = directions::Directions::Internal;
        directions::Directions rampDirectionBottomRight = directions::Directions::Internal;
        irr::f32 offsetTopLeft = 0;
        irr::f32 offsetTopRight = 0;
        irr::f32 offsetBottomLeft = 0;
        irr::f32 offsetBottomRight = 0;

        /// @brief return minimum of all 4 arguments
        inline irr::f32 min4(const irr::f32 x1, const irr::f32 x2, const irr::f32 x3, const irr::f32 x4)
        {
            if (x1 < x2 && x1 < x3 && x1 < x4)
            {
                return x1;
            }
            else if (x2 < x3 && x2 < x4)
            {
                return x2;
            }
            else if (x3 < x4)
            {
                return x3;
            }
            else
            {
                return x4;
            }
        }

        /// @brief function to be used to get the proportion a corner of the tile has to the total
        /// height
        /// @param distance distance from the point which height is needed a specific corner of the
        /// tile
        /// @return the proportion a height at a corner should be multiplied with
        inline irr::f32 getHeightProportion(const irr::f32 distance) const
        {
            // TODO: maybe recaclulate this function to use the squared distance which is faster
            // than std::sqrt(std::pow());
            // the constants of this function are chosen such as:
            // ghp(d = 0) = 1 // at each corner the height of the corner contributes fully
            // ghp(d = 0.5) = 0.5 // halfway between two corners each contributes half
            // ghp(d = sqrt(2)/2) = 1/4 // in the center of the tile each corner contributes 25%
            // ghp(d >= 1) = 0 // at each corner no other height contributes
            // this ensures that neighboring tiles only need to know the height of their shared
            // vertices instead of the height of all vertices for both tiles
            // dh/dx(d = 1) = 0 smooth transition to ghp(d) = 0 at the border ghp(1 \pm \delta)
            if (distance >= 1.0f)
            {
                debugOutLevel(Debug::DebugLevels::secondOrderLoop + 5, this, "getProportion called with distance=", distance, "-> returning 0");
                return 0;
            }
            // no "f" behind the numbers to ensure the compiler might use more precise math when
            // optimizing this passage. Only cast the final result to irr::f32.
            const irr::f32 retVal =
                static_cast<irr::f32>(1.0 - 1.70710678 * distance + 3.82842712 * distance * distance -
                                      6.53553391 * distance * distance * distance +
                                      3.41421356 * distance * distance * distance * distance);
            debugOutLevel(Debug::DebugLevels::secondOrderLoop + 5,
                          this,
                          "getProportion called with distance=",
                          distance,
                          "-> returning",
                          retVal);
            return retVal;
        }
    };
} // namespace tileMap

#endif /* ifndef LEVELS_H */
