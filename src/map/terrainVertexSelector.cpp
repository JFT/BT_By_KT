// TerrainVertexSelector based on the CTerrainTriangleSelector from Irrlicht.
// For the license of this file see licenses/irrlicht.

// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "terrainVertexSelector.h"

#include <irrlicht/S3DVertex.h>

#include "terrainscenenode.h"
#include "../utils/debug.h"
#include "../utils/static/error.h"

using namespace irr;
using namespace scene;

//! constructor
TerrainVertexSelector::TerrainVertexSelector(TerrainSceneNode* node, s32 LOD)
    : SceneNode(node)
    , TrianglePatches()
{
    setTriangleData(node, LOD);
}


//! destructor
TerrainVertexSelector::~TerrainVertexSelector()
{
    TrianglePatches.TrianglePatchArray.clear();
}


//! Clears and sets triangle data
void TerrainVertexSelector::setTriangleData(TerrainSceneNode* node, s32 LOD)
{
    // Get pointer to the GeoMipMaps vertices
    const video::S3DVertex2TCoords* vertices =
        static_cast<const video::S3DVertex2TCoords*>(node->getRenderBuffer()->getVertices());

    // Clear current data
    const irr::u32 patchCountX = node->TerrainData.PatchCountX;
    const irr::u32 patchCountZ = node->TerrainData.PatchCountZ;
    TrianglePatches.TotalTriangles = 0;
    TrianglePatches.NumPatches = patchCountX * patchCountZ;

    TrianglePatches.TrianglePatchArray.reallocate(TrianglePatches.NumPatches);
    for (s32 o = 0; o < TrianglePatches.NumPatches; ++o)
    {
        TrianglePatches.TrianglePatchArray.push_back(SGeoMipMapTrianglePatch());
    }

    core::triangle3df tri;
    core::array<u32> indices;
    u32 tIndex = 0;
    for (u32 x = 0; x < patchCountX; ++x)
    {
        for (u32 z = 0; z < patchCountZ; ++z)
        {
            TrianglePatches.TrianglePatchArray[tIndex].NumTriangles = 0;
            TrianglePatches.TrianglePatchArray[tIndex].Box = node->getBoundingBox(x, z);
            u32 indexCount = node->getIndicesForPatch(indices, x, z, LOD);

            TrianglePatches.TrianglePatchArray[tIndex].Triangles.reallocate(indexCount / 3);
            TrianglePatches.TrianglePatchArray[tIndex].VertexIndices.reallocate(indexCount);
            for (u32 i = 0; i < indexCount; i += 3)
            {
                tri.pointA = vertices[indices[i + 0]].Pos;
                tri.pointB = vertices[indices[i + 1]].Pos;
                tri.pointC = vertices[indices[i + 2]].Pos;
                TrianglePatches.TrianglePatchArray[tIndex].Triangles.push_back(tri);
                this->TrianglePatches.TrianglePatchArray[tIndex].VertexIndices.push_back(indices[i + 0]);
                this->TrianglePatches.TrianglePatchArray[tIndex].VertexIndices.push_back(indices[i + 1]);
                this->TrianglePatches.TrianglePatchArray[tIndex].VertexIndices.push_back(indices[i + 2]);
                ++TrianglePatches.TrianglePatchArray[tIndex].NumTriangles;
            }

            TrianglePatches.TotalTriangles += TrianglePatches.TrianglePatchArray[tIndex].NumTriangles;
            ++tIndex;
        }
    }
}


//! Gets all triangles.
void TerrainVertexSelector::getTriangles(core::triangle3df* triangles,
                                         u32* indices,
                                         s32 arraySize,
                                         s32& outTriangleCount,
                                         const core::matrix4* transform) const
{
    s32 count = TrianglePatches.TotalTriangles;

    if (count > arraySize)
    {
        count = arraySize;
    }

    core::matrix4 mat;

    if (transform)
    {
        mat = (*transform);
    }

    s32 tIndex = 0;

    for (s32 i = 0; i < TrianglePatches.NumPatches; ++i)
    {
        if (tIndex + TrianglePatches.TrianglePatchArray[i].NumTriangles <= count)
            for (s32 j = 0; j < TrianglePatches.TrianglePatchArray[i].NumTriangles; ++j)
            {
                triangles[tIndex] = TrianglePatches.TrianglePatchArray[i].Triangles[j];
                indices[tIndex * 3 + 0] = TrianglePatches.TrianglePatchArray[i].VertexIndices[j * 3 + 0];
                indices[tIndex * 3 + 1] = TrianglePatches.TrianglePatchArray[i].VertexIndices[j * 3 + 1];
                indices[tIndex * 3 + 2] = TrianglePatches.TrianglePatchArray[i].VertexIndices[j * 3 + 2];

                mat.transformVect(triangles[tIndex].pointA);
                mat.transformVect(triangles[tIndex].pointB);
                mat.transformVect(triangles[tIndex].pointC);

                ++tIndex;
            }
    }

    outTriangleCount = tIndex;
}


//! Gets all triangles which lie within a specific bounding box.
void TerrainVertexSelector::getTriangles(core::triangle3df* triangles,
                                         u32* indices,
                                         s32 arraySize,
                                         s32& outTriangleCount,
                                         const core::aabbox3d<f32>& box,
                                         const core::matrix4* transform) const
{
    s32 count = TrianglePatches.TotalTriangles;

    if (count > arraySize)
    {
        count = arraySize;
    }

    core::matrix4 mat;

    if (transform)
    {
        mat = (*transform);
    }

    s32 tIndex = 0;

    for (s32 i = 0; i < TrianglePatches.NumPatches; ++i)
    {
        if (tIndex >= count)
        {
            break;
        }
        if (not TrianglePatches.TrianglePatchArray[i].Box.intersectsWithBox(box))
        {
            continue;
        }
        for (s32 j = 0; j < TrianglePatches.TrianglePatchArray[i].NumTriangles; ++j)
        {
            if (tIndex >= count)
            {
                break;
            }
            if (not TrianglePatches.TrianglePatchArray[i].Triangles[j].isTotalOutsideBox(box))
            {
                triangles[tIndex] = TrianglePatches.TrianglePatchArray[i].Triangles[j];
                indices[tIndex * 3 + 0] = TrianglePatches.TrianglePatchArray[i].VertexIndices[j * 3 + 0];
                indices[tIndex * 3 + 1] = TrianglePatches.TrianglePatchArray[i].VertexIndices[j * 3 + 1];
                indices[tIndex * 3 + 2] = TrianglePatches.TrianglePatchArray[i].VertexIndices[j * 3 + 2];

                mat.transformVect(triangles[tIndex].pointA);
                mat.transformVect(triangles[tIndex].pointB);
                mat.transformVect(triangles[tIndex].pointC);

                ++tIndex;
            }
        }
    }

    outTriangleCount = tIndex;
}


//! Gets all triangles which have or may have contact with a 3d line.
void TerrainVertexSelector::getTriangles(core::triangle3df* triangles,
                                         u32* indices,
                                         s32 arraySize,
                                         s32& outTriangleCount,
                                         const core::line3d<f32>& line,
                                         const core::matrix4* transform) const
{
    const s32 count = core::min_(static_cast<s32>(TrianglePatches.TotalTriangles), arraySize);

    core::matrix4 mat;

    if (transform)
    {
        mat = (*transform);
    }

    s32 tIndex = 0;

    for (s32 i = 0; i < TrianglePatches.NumPatches; ++i)
    {
        if (tIndex + TrianglePatches.TrianglePatchArray[i].NumTriangles <= count &&
            TrianglePatches.TrianglePatchArray[i].Box.intersectsWithLine(line))
        {
            for (s32 j = 0; j < TrianglePatches.TrianglePatchArray[i].NumTriangles; ++j)
            {
                triangles[tIndex] = TrianglePatches.TrianglePatchArray[i].Triangles[j];
                indices[tIndex * 3 + 0] = TrianglePatches.TrianglePatchArray[i].VertexIndices[j * 3 + 0];
                indices[tIndex * 3 + 1] = TrianglePatches.TrianglePatchArray[i].VertexIndices[j * 3 + 1];
                indices[tIndex * 3 + 2] = TrianglePatches.TrianglePatchArray[i].VertexIndices[j * 3 + 2];

                mat.transformVect(triangles[tIndex].pointA);
                mat.transformVect(triangles[tIndex].pointB);
                mat.transformVect(triangles[tIndex].pointC);

                ++tIndex;
            }
        }
    }

    outTriangleCount = tIndex;
}


//! Returns amount of all available triangles in this selector
s32 TerrainVertexSelector::getTriangleCount() const
{
    return TrianglePatches.TotalTriangles;
}


ISceneNode* TerrainVertexSelector::getSceneNodeForTriangle(u32 /*triangleIndex*/) const
{
    return SceneNode;
}


/* Get the number of TriangleSelectors that are part of this one.
Only useful for MetaTriangleSelector others return 1
*/
u32 TerrainVertexSelector::getSelectorCount() const
{
    return 1;
}
