/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TILE_H
#define TILE_H

#include <vector>

#include <irrlicht/S3DVertex.h>
#include <irrlicht/irrArray.h>
#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>

#include "directions.h"
#include "levels_t.h"

// forward declarations
namespace irr
{
    namespace video
    {
        class IImage;
        class IVideoDriver;
    } // namespace video
    namespace scene
    {
        class IIndexBuffer;
    }
} // namespace irr


namespace tileMap
{
    /** @brief This class is mostly there to be able to generate a terrain mesh using as few as
     * possible vertices
     * this is done by saving all terrain vertices in one big vertexBuffer. Each individual tile
     * then gets the indices into this vertexBuffer (called absolute indices)
     * and translates between absolute indices and the indices which define the tile itself (called
     * relative indices) */
    class Tile
    {
       public:
        /** @brief: allowed difference between positions of 2 vertices before their positions are
         * considered equal */
        static constexpr irr::f32 positionTolerance =
            0.0001f; // blender uses the same tolerance and it is pretty small considering a tile is
                     // 1x1 units big. Used together with normalTolerance
        static constexpr irr::f32 normalTolerance =
            25.0f; // how much degree two normals can deviate before being considered the same. Used
                   // together with positionTolerance
        /// @brief width of the texture for each tile
        static constexpr irr::u32 textureWidth = 32;
        /// @brief height of the texture for each tile
        static constexpr irr::u32 textureHeight = textureWidth;

        /** @brief does this tile know the index into the final terrain buffer for a specific
         * internal vertex? */
        std::vector<bool> knowsAbsoluteIndex;
        /** @brief access via Tile::Directions e.g. via neighbors[Directions::Right] */
        std::vector<Tile*> neighbors;
        /** @brief Array to hold all the indices which generate polys for this tile. Indices from
         * other tiles and indices from this tile. */
        irr::core::array<irr::u32> indices;

        Tile();
        Tile(const Tile& other) = delete; // no copy constructor without the possibility to copy
                                          // over the image. If no copy of the image is needed use
                                          // the copy constructor with videoDriver set to nullptr.
        Tile& operator=(const Tile& other) = delete;

        /// @brief creates a deep copy of the other tile
        /// @param other
        /// @param videoDriver must be != nullptr to get a copy of the internal IImage* of the tile.
        /// Otherwise the IImage* will not be transferred and the tile being copied to will drop()
        /// it's old image if it had any and set it to nullptr.
        void copyFrom(const Tile& other, irr::video::IVideoDriver* videoDriver);

        /// @brief destructor frees al internally allocated memory. Also drop()s the texture image.
        ~Tile();

        void setLevels(const Levels_t levels_);
        const Levels_t& getLevels() const;
        void setTileOrigin(const irr::core::vector3df& coord);
        irr::core::vector3df getTileOrigin() const { return this->tileOrigin; }
        void setOriginLevel(const irr::f32 level);

        /// @brief scale all texture coordinates of all verticee
        /// @param scale texture coordinates will be multiplied with this (Note: doesn't affect the
        /// texture returned with getTexture())
        void scaleTextureCoordinates(const irr::core::vector2df scale);
        /// @brief move all texture coordinates of the vertices by offset and multiply them whith
        /// scale
        /// @param offset will be added to all texture coordinates of each vertex
        void moveTextureCoordinates(const irr::core::vector2df offset);

        /// @brief set string type of the tile (useful for e.g. debug information)
        /// @param type type of the tile. Can be any arbitrary string as no logic depends on this.
        inline void setType(const irr::core::stringw type) { this->typeName = type; }
        /// @brief return the name for the type of this tile. This name MUST NOT be used in any
        /// program logic as it can be arbitrarily set!
        inline irr::core::stringw getType() const { return this->typeName; }
        irr::video::IImage* getImage() const;
        /// @brief set image for the tile. Grabs a reference of image.
        /// @param image no nullptr checks are performed against this parameter. Use with care!
        void setImage(irr::video::IImage* image);

        /// @brief: append the vertices needed by that tile (not defined by it's neighbors) to the
        /// supplied std::vector
        /// @param vertices
        /// @param startIndex: index of the first vertex. = 0 if the inserting should start at the
        /// end. Even though the vertices are appended this is still needed to generate the indices.
        /// @return number of appended vertices
        irr::u32 appendVertices(std::vector<irr::video::S3DVertex2TCoords>& vertices, const irr::u32 startIndex);
        /**
         * @brief insert the indices for this tile into the supplied buffer. This assumes that the
         * buffer is large enough! Will insert nothing if called before Tile::appendVertices().
         * @param indexBuffer: !MUST! be large enough to hold the indices. Check beforehand with
         * Tile::indicesSize().
         * @param startIndex: insertion starts at this position in the buffer
         * @return number of inserted indices
         */
        irr::u32 insertIndices(irr::scene::IIndexBuffer& indexBuffer, const irr::u32 startIndex);
        irr::u32 insertIndices(irr::core::array<irr::u32>& array, const irr::u32 startIndex);
        /**
         * @brief same as insertIndices but appends them to the buffer instead
         * @param indexBuffer
         * @return number of appended indices
         */
        irr::u32 appendIndices(irr::scene::IIndexBuffer& indexBuffer);
        irr::u32 indicesSize() const;

        void addVertexDefinition(const irr::video::S3DVertex2TCoords& vertex);
        /**
         * @brief appends the index to the relativeIndices array. It will later be looked up if the
         * corresponding vertex was generated by a neighbor
         * @param index
         * @param direction
         */
        void addIndexDefinition(const irr::u32 index);

        /// @brief
        /// @param videoDriver needed to rotate the image holding the texture. If nullptr the
        /// texture image won't be rotated.
        void rotateClockwise(irr::video::IVideoDriver* videoDriver);

        /// @brief puts a red dot onto the places of the image used by texture coordinates. Useful
        /// for debugging texture warp.
        void printTextureCoordinatesOntoImage();

        /// @brief return the direction inside this tile the vector p lies in. e.g. vector(0,0,0) ->
        /// TopRight
        /// @param p
        /// @return the Tile::Directions the vector falls into. Returns NumDirections if not inside
        /// the tile.
        static directions::Directions getVectorDirection(const irr::core::vector3df& p);

        /// @brief checks if every position inside the area A = [x-dx, x+dx]x[z-dz, z+dz] can be
        /// reached from (x,z)
        ///
        /// The area A will be tested in steps of (dx/nx, dz/nz) defaulting to 100 testpoints inside
        /// the area.
        /// Walkability is only determined by levels and ramps between levels. Offsets have no
        /// influence on walkability.
        /// @param x x-coordinate to test walkability inside the tile (meaning x \in [0, 1])
        /// @param z z-coordinate to test walkabiltiy inside the tile (meaning z \in [0, 1])
        /// @param dx
        /// @param dz determine the size of the area around (x,z) which will be tested for
        /// walkability.
        /// @param nx granularity setting: how many points to test in x direction. Must be > 2
        /// (otherwise the borders of [x-dx, x+dx] can't be tested)
        /// @param nz granularity setting: how many points to test in z direction. Must be > 2
        /// (otherwise the borders of [z-dz, z+dz] can't be tested)
        /// @return true if every point in the area [x-dx, x+dz]x[z-dz, z+dz] can be reached from
        /// (x,z). Points outside the tile are treated als walkable. If any point inside the area
        /// can't be reached from (x,z) returns false
        bool isWalkable(const irr::f32 x,
                        const irr::f32 z,
                        const irr::f32 dx,
                        const irr::f32 dz,
                        const irr::u32 nx = 10,
                        const irr::u32 nz = 10) const;

       private:
        /** @brief position of the top right corner of the tile in worldspace */
        irr::core::vector3df tileOrigin = irr::core::vector3df(0.0f, 0.0f, 0.0f);
        /** @brief hold relative vertices */
        irr::core::array<irr::video::S3DVertex2TCoords> relativeVertices;
        /** @brief Indices array relative inside the tile. Copied over from the initial mesh. */
        irr::core::array<irr::u32> relativeIndices;
        /** @brief Array to convert from relative indices to absolute indices */
        irr::core::array<irr::u32> relativeToAbsoluteIndex;
        /** @brief: the actual levels on the map */
        Levels_t levels;
        /** @brief: the level of the vertex at x=0, z=0 relative to the origin of the mesh */
        irr::f32 originLevel = 0.0f;
        /// @brief the name for this type of tile. Can be an arbitrary string as no logic depends on
        /// it
        irr::core::stringw typeName;
        /// @brief the texture image for this tile. Needed because the textures themselves are
        /// rotated with the tile.
        irr::video::IImage* image = nullptr;

        irr::core::vector3df getPositionInsideNeighbor(const irr::core::vector3df& relativePosition,
                                                       const directions::Directions neighbor) const;
        /**
         * @brief convert the relative position (inside a tile) into a absolute position (world
         * position)
         * @param relativePosition
         * @return the absolute position
         */
        irr::core::vector3df getAbsolutePosition(const irr::core::vector3df relativePosition) const;

        void appendVertex(const irr::u32 arrayPosition, std::vector<irr::video::S3DVertex2TCoords>& vertices);
        irr::u32 generateIndices();

        /**
         * @brief which neighbor has generated the vertex v?
         * @param v vertex that a neighbor might have been generated. Compares position and normal.
         * @return: direction of the neighbor that generated the vertices. Directions::NumDirections
         * if not generated by a neighbor.
         */
        bool knowsAbsoluteIndexOf(const irr::video::S3DVertex2TCoords& v, irr::u32& absoluteIndex) const;
        directions::Directions neighborKnowsAbsoluteIndexOf(const irr::video::S3DVertex2TCoords& v,
                                                            irr::u32& absoluteIndex) const;
        /**
         * @brief ask the neighbor for the absolute index for the vertex at position p
         * @param p: position of the vertex
         * @param neighbor: neighbor to ask for the absolute index
         * @param absoluteIndex: will be set to the absolute index of the vertex if the neighbor
         * knows it
         * @return: true if the neighbor knew the absolute index, false otherwise.
         */
        bool specificNeighborKnowsAbsoluteIndexOf(const irr::video::S3DVertex2TCoords& v,
                                                  const directions::Directions neighbor,
                                                  irr::u32& absoluteIndex) const;
    };

} // namespace tileMap

#endif // TILE_H
