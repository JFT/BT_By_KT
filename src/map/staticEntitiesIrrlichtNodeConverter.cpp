/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "staticEntitiesIrrlichtNodeConverter.h"

#include <algorithm> // std::find

#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/irrArray.h>
#include <irrlicht/irrString.h>
#include "../ecs/components.h"
#include "../ecs/entityManager.h"
#include "../graphics/meshcombiner.h"
#include "../utils/HandleManager.h"
#include "../utils/Pow2Assert.h"
#include "../utils/static/error.h"

// forward declarations
namespace irr
{
    namespace video
    {
        class ITexture;
    }
    namespace scene
    {
        class IMesh;
        class IAnimatedMeshSceneNode;
    } // namespace scene
} // namespace irr

using ST = StaticEntitiesIrrlichtNodeConverter;

ST::StaticEntitiesIrrlichtNodeConverter(irr::scene::ISceneManager* const sceneManager_)
    : sceneManager(sceneManager_)
{
}

std::vector<irr::scene::IMesh*>
ST::generateMeshFromDisplayOnlyEntities(const std::vector<entityID_t>& displayOnlyEntities,
                                        EntityManager& entityManager,
                                        Handles::HandleManager& handleManager)
{
    // generate one static mesh for each mesh*/texture* pair that occurs in the graphic components
    // of the displayOnlyEntities

    struct MeshAndTextures
    {
        explicit MeshAndTextures(irr::scene::IAnimatedMesh* const mesh_)
            : mesh(mesh_)
        {
        }

        irr::scene::IAnimatedMesh* const mesh;
        std::vector<irr::video::ITexture*> textures;

        bool operator==(const MeshAndTextures& other)
        {
            if (this->mesh != other.mesh)
            {
                return false;
            }
            if (this->textures.size() != other.textures.size())
            {
                return false;
            }
            for (size_t i = 0; i < this->textures.size(); i++)
            {
                if (this->textures[i] != other.textures[i])
                {
                    return false;
                }
            }
            return true;
        }
    };

    std::vector<MeshAndTextures> alreadyExistingMaT;
    std::vector<irr::core::array<irr::scene::IAnimatedMeshSceneNode*>> sceneNodes;

    for (const auto id : displayOnlyEntities)
    {
        if (not entityManager.hasComponent<Components::Graphic>(id))
        {
            Error::errContinue("Entity",
                               id,
                               "is missing the neccecary component 'Graphic' and "
                               "can't be saved as a static object");
            continue;
        }
        const auto gHandle = entityManager.getComponent<Components::Graphic>(id);
        const auto gCmp = handleManager.getAsValue<Components::Graphic>(gHandle);
        MeshAndTextures newMaT(gCmp.mesh);
        newMaT.textures.push_back(gCmp.texture);
        const auto it = std::find(alreadyExistingMaT.begin(), alreadyExistingMaT.end(), newMaT);
        auto* const node = gCmp.node;
        if (it == alreadyExistingMaT.end())
        {
            alreadyExistingMaT.push_back(newMaT);
            auto sceneNodesForMaT = irr::core::array<irr::scene::IAnimatedMeshSceneNode*>();
            sceneNodesForMaT.push_back(node);
            sceneNodes.push_back(sceneNodesForMaT);
        }
        else
        {
            const auto index = std::distance(alreadyExistingMaT.begin(), it);
            sceneNodes[index].push_back(node);
        }
    }

    POW2_ASSERT(alreadyExistingMaT.size() == sceneNodes.size());

    MeshCombiner combiner;

    std::vector<irr::scene::IMesh*> combinedMeshes;
    for (irr::u32 i = 0; i < sceneNodes.size(); i++)
    {
        irr::core::stringc meshName = "StaticEntitiesIrrlichtNodeConverter_tmp_MaT";
        meshName += irr::core::stringc(std::to_string(i).c_str());
        irr::core::array<irr::s32> frames;
        frames.set_used(sceneNodes[i].size());
        for (irr::u32 j = 0; j < sceneNodes[i].size(); j++)
        {
            frames[j] = 0; // TODO: actually read correct frame?
        }
        combinedMeshes.push_back(combiner.combineMeshes(sceneManager, sceneNodes[i], frames, meshName, false));
    }

    return combinedMeshes;
}
