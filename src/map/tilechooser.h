/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TILECHOOSER_H
#define TILECHOOSER_H

#include <vector>

#include <irrlicht/irrTypes.h>
#include <irrlicht/path.h>

#include "directions.h"
#include "levels_t.h"
#include "../utils/static/error.h" // defines ErrCode

// forward declarations
namespace irr
{
    namespace scene
    {
        class ISceneManager;
    }
} // namespace irr


namespace tileMap
{
    class Tile;

    class TileChooser
    {
       public:
        TileChooser();
        ~TileChooser();

        ErrCode loadTileXML(const irr::io::path& filename, irr::scene::ISceneManager* const sceneManager);
        /// @brief get the fitting tile for the given levels
        /// @param levels
        /// @param tile this tile will be set with a tile fitting the levels, correctly rotated.
        /// Will create an error if no tile information has been loaded yet (see
        /// TileChooser::loadTileXML). Returns the first Tile loaded from the XML if no fitting tile
        /// could be found (this works around a bug in terrainscenenode.cpp).
        /// @param sceneManager used to create a copy of the mesh saved in the tile definitions to
        /// the new tile
        /// @return ErrCodes::NO_ERR on success and ErrCodes::GENERIC_ERR if no fitting tile could
        /// be found
        ErrCode getTileForLevels(const Levels_t levels, Tile& tile, irr::scene::ISceneManager* sceneManager);

        static directions::Directions parseDirection(const irr::core::stringw& str);

       private:
        /// @brief compares the levels 'level1' and 'level2'. Ignores ramp information in a specific
        /// direction if any of the levels have a directions::Directions::NumDirections-ramp in that
        /// direction
        /// @param level1
        /// @param level2
        /// @return true if all levels and non-ignored ramps are equal and in equal direction, false
        /// otherwise.
        bool compareLevels(const Levels_t& level1, const Levels_t& level2);

        /** @brief vector to hold tile definitions to compare against */
        std::vector<const Tile*> tiles;
        /// @brief number to count how often a tile is actually used and warn if it isn't
        std::vector<unsigned int> usedTile;
    };


} // namespace tileMap

#endif // TILECHOOSER_H
