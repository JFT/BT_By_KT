/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <string>

#include <irrlicht/IAnimatedMesh.h>
#include <irrlicht/IMesh.h>
#include <irrlicht/IMeshCache.h> // to remove the meshes from memory after they all have been copied to the map
#include <irrlicht/IMeshManipulator.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/IVideoDriver.h> // to load images and turn them into textures
#include <irrlicht/S3DVertex.h>
#include <irrlicht/SMesh.h>
#include <irrlicht/irrString.h>
#include <irrlicht/irrXML.h>

#include "../utils/stringwstream.h" // stringwstream.h needs to be included before debug.h and error.h because they use use operator<< on a stringw

#include "tilechooser.h"

#include "tile.h"
#include "../graphics/ImageManipulation.h"
#include "../utils/debug.h"
#include "../utils/printfunctions.h"
#include "../utils/stringwparser.h"

using namespace tileMap;

TileChooser::TileChooser()
    : tiles()
    , usedTile()
{
}

TileChooser::~TileChooser()
{
    // because each tile gets rotated around each tile takes up 4 slots in the this->tiles and
    // this->usedTile vector
    for (size_t i = 0; i + 3 < this->usedTile.size(); i += 4)
    {
        for (size_t rotation = 0; rotation <= 3; rotation++)
        {
            debugOutLevel(Debug::DebugLevels::stateInit + 2,
                          this->usedTile[i + rotation],
                          "uses for Tile",
                          this->tiles[i]->getType(),
                          "rotation=",
                          rotation);
        }

        debugOutLevel(Debug::DebugLevels::stateInit + 9,
                      this->usedTile[i] + this->usedTile[i + 1] + this->usedTile[i + 2] + this->usedTile[i + 3],
                      "total uses for Tile",
                      this->tiles[i]->getType());
        if (this->usedTile[i] == 0 and this->usedTile[i + 1] == 0 and this->usedTile[i + 2] == 0 and
            this->usedTile[i + 3] == 0)
        {
            debugOutLevel(Debug::DebugLevels::stateInit,
                          "Tile",
                          this->tiles[i]->getType(),
                          "wasn't used. Maybe remove it from this map to save space?");
        }
    }
}

ErrCode TileChooser::loadTileXML(const irr::io::path& filename, irr::scene::ISceneManager* const sceneManager)
{
    irr::io::IrrXMLReader* xml = irr::io::createIrrXMLReader(filename.c_str());
    if (xml == nullptr)
    {
        Error::errContinue("couldn't open file", filename.c_str());
        return ErrCode(ErrCodes::GENERIC_ERR);
    }
    if (sceneManager == nullptr)
    {
        Error::errTerminate("TileChooser needs a sceneManager to work");
    }
    if (sceneManager->getMeshManipulator() == nullptr)
    {
        Error::errTerminate("TileChooser needs a sceneManager with meshManipulator");
    }

    unsigned int lineNum = 0;
    // define vertex here instead of re-creating every loop traversal
    irr::video::S3DVertex2TCoords vertex;
    // tempTile and tileMesh hold information about the tile while parsing is still going on
    Tile* tempTile = nullptr;
    irr::scene::IMesh* tileMesh = nullptr;
    bool autoLevels = true;

    while (xml->read())
    {
        lineNum++;
        switch (xml->getNodeType())
        {
            case irr::io::EXN_ELEMENT:
            {
                if (irr::core::stringw(L"tile").equals_ignore_case(xml->getNodeName()))
                {
                    tempTile = new Tile;
                    const char* type = xml->getAttributeValueSafe("type");
                    if (type != nullptr)
                    {
                        tempTile->setType(irr::core::stringw(type));
                    }
                }
                else if (irr::core::stringw(L"ramps").equals_ignore_case(xml->getNodeName()))
                {
                    if (tempTile == nullptr)
                    {
                        Error::errTerminate("levels element outside of tile at line", lineNum);
                    }
                    for (directions::Directions direction :
                         std::vector<directions::Directions>({directions::Directions::TopLeft,
                                                              directions::Directions::TopRight,
                                                              directions::Directions::BottomLeft,
                                                              directions::Directions::BottomRight}))
                    {
                        const std::string rampString =
                            directions::directionToString(direction) + std::string("RampDirection");

                        if (xml->getAttributeValue(rampString.c_str()) == nullptr)
                        {
                            continue;
                        }
                        directions::Directions rampDirection = directions::Directions::Internal;
                        ErrCode parsingResult =
                            StringWParser::parse(xml->getAttributeValueSafe(rampString.c_str()),
                                                 rampDirection,
                                                 directions::Directions::Internal);
                        if (parsingResult != ErrCode(0))
                        {
                            Error::errContinue("couldn't parse ramp for",
                                               rampString,
                                               xml->getAttributeValueSafe(rampString.c_str()),
                                               "in file",
                                               filename,
                                               "at line",
                                               lineNum);
                        }
                        // because getLevels only returns a const copy we need to do a bit of
                        // temp-value manipulation
                        Levels_t tempLevels = tempTile->getLevels();
                        tempLevels.setRampDirection(direction, rampDirection);
                        tempTile->setLevels(tempLevels);
                    }
                }
                else if (irr::core::stringw(L"model").equals_ignore_case(xml->getNodeName()))
                {
                    if (tempTile == nullptr)
                    {
                        Error::errTerminate("model element outside of tile at line", lineNum);
                    }
                    const irr::core::stringw meshFilename = xml->getAttributeValueSafe("filename");
                    irr::scene::IAnimatedMesh* mesh = sceneManager->getMesh(meshFilename.c_str());
                    if (mesh == nullptr)
                    {
                        Error::errTerminate("couldn't open model file", meshFilename);
                    }
                    if (mesh->getMeshBufferCount() <= 0)
                    {
                        Error::errTerminate("mesh loaded from file", meshFilename, "has no meshbuffers associated with it");
                    }

                    // Sometimes the mesh loaded from the file can contain double vertices at the
                    // same position.
                    // To solve this put the mesh through the mesh-welder from the mesh manipulator
                    // which should fuse these vertices together
                    irr::scene::IMesh* m = mesh->getMesh(0); // because the tiles will have no
                                                             // animation associated with them the
                                                             // actual tile mesh is at index 0 of
                                                             // the animated mesh
                    tileMesh = sceneManager->getMeshManipulator()->createMeshWelded(m, Tile::positionTolerance);

                    const irr::u32 vertexCount = tileMesh->getMeshBuffer(0)->getVertexCount();
                    debugOutLevel(Debug::DebugLevels::firstOrderLoop, meshFilename, "vertices:", vertexCount);

                    // the IAnimatedMesh* returned from getMesh should NOT be dropped. Instead a
                    // mesh can be removed from the cache by calling
                    if (sceneManager->getMeshCache() == nullptr)
                    {
                        Error::errTerminate("Couldn't get the MeshCache!");
                    }
                    sceneManager->getMeshCache()->removeMesh(mesh);
                }
            }
            break;
            case irr::io::EXN_ELEMENT_END:
            {
                if (irr::core::stringw(L"tile").equals_ignore_case(xml->getNodeName()))
                {
                    if (tempTile == nullptr)
                    {
                        Error::errTerminate("tempTile == nullptr at end of tile definition at line", lineNum, "Check code!");
                    }
                    if (tileMesh == nullptr)
                    {
                        Error::errTerminate("tileMesh == nullptr at end of tile definition at line",
                                            lineNum,
                                            "Forgot to include <model filename=... in the xml?");
                    }

                    const irr::u32 vertexCount = tileMesh->getMeshBuffer(0)->getVertexCount();
                    // in case autolevels is set we need a temporary levels_t we can manipulate
                    Levels_t tempLevels = tempTile->getLevels();
                    for (irr::u32 i = 0; i < vertexCount; i++)
                    {
                        // cast the getVertices()[i] to S3DVertex* because getVertices() is agnostic
                        // to the vertex type. The OBJFileLoader uses S3DVertex*
                        vertex.Pos = static_cast<irr::video::S3DVertex*>(
                                         tileMesh->getMeshBuffer(0)->getVertices())[i]
                                         .Pos;
                        vertex.Normal = static_cast<irr::video::S3DVertex*>(
                                            tileMesh->getMeshBuffer(0)->getVertices())[i]
                                            .Normal;
                        vertex.TCoords = static_cast<irr::video::S3DVertex*>(
                                             tileMesh->getMeshBuffer(0)->getVertices())[i]
                                             .TCoords;
                        vertex.TCoords2 = static_cast<irr::video::S3DVertex*>(
                                              tileMesh->getMeshBuffer(0)->getVertices())[i]
                                              .TCoords;
                        tempTile->addVertexDefinition(vertex);
                        if (autoLevels)
                        {
                            const directions::Directions vertexDirection =
                                Tile::getVectorDirection(vertex.Pos);
                            switch (vertexDirection)
                            {
                                case directions::Directions::TopLeft:
                                case directions::Directions::TopRight:
                                case directions::Directions::BottomRight:
                                case directions::Directions::BottomLeft:
                                    tempLevels.setLevel(vertexDirection, vertex.Pos.Y);
                                    break;
                                case directions::Directions::Top:
                                case directions::Directions::Right:
                                case directions::Directions::Bottom:
                                case directions::Directions::Left:
                                case directions::Directions::Internal:
                                case directions::Directions::NumDirections:
                                default:
                                    // nothing to be done for all other cases because they aren't in
                                    // a corner of the tile
                                    break;
                            }
                        } // if (autolevels)
                    }     // for(irr::u32 i = 0; i < vertexCount; i++)

                    // if autolevels was defined set the read-in levels. If it wans't defined
                    // 'tempLevels' still contains the original levels and this doesn't change
                    // anything
                    tempTile->setLevels(tempLevels);
                    // copy over the texture or use the default one if no texture defined
                    irr::video::IImage* img = nullptr;
                    if (tileMesh->getMeshBuffer(0)->getMaterial().getTexture(0) == nullptr)
                    {
                        Error::errContinue("tile",
                                           tempTile->getType(),
                                           "does't contain any "
                                           "texture information! "
                                           "setting default "
                                           "'flat.png'");
                        img = sceneManager->getVideoDriver()->createImageFromFile(
                            "media/map/models/flat.png");
                    }
                    else
                    {
                        img = ImageManipulation::TextureToImage(
                            tileMesh->getMeshBuffer(0)->getMaterial().getTexture(0),
                            sceneManager->getVideoDriver());
                    }
                    tempTile->setImage(img);
                    // setImage grabs a reference
                    img->drop();

                    const irr::u32 indexCount = tileMesh->getMeshBuffer(0)->getIndexCount();
                    for (irr::u32 i = 0; i < indexCount; i++)
                    {
                        const irr::u32 index = tileMesh->getMeshBuffer(0)->getIndices()[i];
                        tempTile->addIndexDefinition(index);
                    }

                    // put the tile into the tile array with all it's rotations.
                    for (size_t rotation = 0; rotation < 4; rotation++)
                    {
                        // copy the temporary tile
                        this->tiles.push_back(tempTile);
                        this->usedTile.push_back(0);
                        debugOutLevel(Debug::firstOrderLoop + 1,
                                      "added tile with levels=",
                                      tempTile->getLevels().to_string(),
                                      "with",
                                      tempTile->indicesSize(),
                                      "indices");

                        Tile* rotatedTile = new Tile();
                        rotatedTile->copyFrom(*tempTile, sceneManager->getVideoDriver());
                        rotatedTile->rotateClockwise(sceneManager->getVideoDriver());
                        tempTile = rotatedTile;
                    }


                    // reset all temporary variables
                    delete tempTile;
                    tempTile = nullptr;
                    tileMesh->drop();
                    tileMesh = nullptr;
                    autoLevels = true;
                }
            }
            break;
            case irr::io::EXN_NONE:
            case irr::io::EXN_TEXT:
            case irr::io::EXN_COMMENT:
            case irr::io::EXN_CDATA:
            case irr::io::EXN_UNKNOWN:
            default:
                debugOutLevel(Debug::DebugLevels::maxLevel,
                              "unhandled xml entry type",
                              static_cast<int>(xml->getNodeType()));
                break;
        } // switch (xml->getNodeType())
    }     // while (xml->read())

    return ErrCode(ErrCodes::NO_ERR);
}

ErrCode TileChooser::getTileForLevels(const Levels_t wantedLevels, Tile& tile, irr::scene::ISceneManager* sceneManager)
{
    if (this->tiles.size() == 0)
    {
        Error::errTerminate("No tiles have been loaded. Did you forget to call loadTileXML()?");
    }
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "trying to get correct tile for levels:",
                  wantedLevels.to_string());

    bool foundFittingTile = false;
    size_t fittingTileIndex = 0;
    // this loops through all possible tiles and all their rotations
    for (size_t i = 0; i < this->tiles.size(); i++)
    {
        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                      "comparing this->tiles[",
                      i,
                      this->tiles[i]->getType(),
                      "].getLevels() =",
                      this->tiles[i]->getLevels().to_string(),
                      "with wanted levels",
                      wantedLevels.to_string());
        if (compareLevels(this->tiles[i]->getLevels(), wantedLevels))
        {
            foundFittingTile = true;
            fittingTileIndex = i;
            this->usedTile[i]++;
            debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                          "found fitting tile type =",
                          this->tiles[i]->getType(),
                          "at index i=",
                          i);
            // the rotation already fits so no need to rotate it around
            break;
        }
    }

    tile.copyFrom(*this->tiles[fittingTileIndex],
                  sceneManager->getVideoDriver()); // create a copy of the tile that fits this will
                                                   // always be filled with the first tile in the
                                                   // tile definiton file. Even if no fitting tile
                                                   // was found.
    // this will return the first tile in the list if no fitting tile was found. Returning an empty
    // tile leads to errors in terrainscenenenode.cpp:
    /* 0x0000000000467998 in TerrainSceneNode::calculatePatchData (this=this@entry=0x2948ed0)
     *     at
     * /home/themanual/stuff/progs/programming/c_cpp/battletanks/src/map/terrainscenenode.cpp:798
     *     798
     * patch.BoundingBox.reset(RenderBuffer->getPosition(this->tileMap[patch.tileStartX][patch.tileStartZ].indices[0]));
     *     (gdb) backtrace
     *     #0  0x0000000000467998 in TerrainSceneNode::calculatePatchData
     * (this=this@entry=0x2948ed0)
     *         at
     * /home/themanual/stuff/progs/programming/c_cpp/battletanks/src/map/terrainscenenode.cpp:798
     */
    // TODO: maybe fix that error in terrainscenenode and return an empty tile
    if (!foundFittingTile)
    {
        Error::errContinue("didn't find correct tile for levels:", wantedLevels.to_string());
        // not setting the levels will create visible holes in the map but that makes it easier to
        // spot the missing tiles
        return ErrCode(ErrCodes::GENERIC_ERR);
    }
    // set the levels inside the tile to the wantedLevels to make sure the vertices are at the
    // correct height
    // save the old origin level
    tile.setOriginLevel(tile.getLevels().getLevel(directions::Directions::TopRight));
    tile.setLevels(wantedLevels);
    return ErrCode(ErrCodes::NO_ERR);
}

/* ########################################################################
 * static functions
 * ######################################################################## */

/* ########################################################################
 * private functions
 * ######################################################################## */

bool TileChooser::compareLevels(const Levels_t& levels1, const Levels_t& levels2)
{
    // normalize changes the levels -> temporary copies
    Levels_t levels1N = levels1;
    levels1N.normalize();
    Levels_t levels2N = levels2;
    levels2N.normalize();

    for (directions::Directions direction :
         std::vector<directions::Directions>({directions::Directions::TopLeft,
                                              directions::Directions::TopRight,
                                              directions::Directions::BottomLeft,
                                              directions::Directions::BottomRight}))
    {
        if (std::fabs(levels1N.getLevel(direction) - levels2N.getLevel(direction)) > Tile::positionTolerance)
        {
            return false;
        }
        // ignore ramps if the direction is NumDirections. This works because the default
        // rampDirection is Internal and it can be changed by the tile definition file
        if (levels1N.getRampDirection(direction) != directions::Directions::NumDirections and
            levels2N.getRampDirection(direction) != directions::Directions::NumDirections)
        {
            if (levels1N.getRampDirection(direction) != levels2N.getRampDirection(direction))
            {
                return false;
            }
        }
    }
    return true;
}
