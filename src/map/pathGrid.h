/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PATHGRID_H
#define PATHGRID_H

#include <algorithm>
#include <cmath>
#include <cstdint> // uintX_t
#include <cstdlib>
#include <irrlicht/irrTypes.h>
#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>
#include <limits> // std::numeric_limits
#include <vector>


#include "../utils/Pow2Assert.h"
#include "../utils/static/error.h"

struct PathGrid
{
   public:
    typedef uint32_t PathGridDimension_t;
    static constexpr PathGridDimension_t InvalidIndex = static_cast<PathGridDimension_t>(-1);
    struct PathCellCoordinate_t
    {
        PathCellCoordinate_t() = default;
        PathCellCoordinate_t(const PathGridDimension_t X_, const PathGridDimension_t Z_)
            : X(X_)
            , Z(Z_)
        {
        }
        PathGridDimension_t X = InvalidIndex;
        PathGridDimension_t Z = InvalidIndex;
    };

    enum Walkability
    {
        Free = 0,
        StaticNonWalkable = 1, // an immovable object is in the way
        // important: if including more non-dynamic walkability states make sure to fix the
        // resetDynamics() function which uses bittwiddling to reset the dynamic states
        DynamicBlockedWillBecomeFree, // currently occupied by an entity which will move away
        DynamicNonwalkable, // contestet between two (or more) objects -> nobody will use this cell
                            // this turn
        DynamicFreeWillBecomeBlocked, // currently free but will be occupied by an entity
        DynamicBlocked, // an entity which occupies this cell will continue to occupy it
        DynamicBlockedBecomingBlockedByDifferentEntity
    };

    PathGrid()
        : map()
    {
    }

    inline int getPathCellX(const irr::f32 x)
    {
        // casting to int causes a cutoff (meaning x = 0.99 * unitSize is the same as x = 0)
        // which is corrected by moving the x-coordinate by unitSize/2 before casting to int
        return static_cast<int>((x + this->unitSize / 2.0f) / this->unitSize);
    }

    inline int getPathCellZ(const irr::f32 z)
    {
        // casting to int causes a cutoff (meaning z = 0.99 * unitSize is the same as z = 0)
        // which is corrected by moving the z-coordinate by unitSize/2 before casting to int
        return static_cast<int>((z + this->unitSize / 2.0f) / this->unitSize);
    }

    inline PathCellCoordinate_t getPathCellCoordinate(const irr::core::vector2df& p)
    {
        const PathGridDimension_t x = static_cast<PathGridDimension_t>(this->getPathCellX(p.X));
        const PathGridDimension_t z = static_cast<PathGridDimension_t>(this->getPathCellZ(p.Y));
        return PathCellCoordinate_t{x, z};
    }

    inline PathCellCoordinate_t getPathCellCoordinate(const irr::core::vector3df& p)
    {
        const PathGridDimension_t x = static_cast<PathGridDimension_t>(this->getPathCellX(p.X));
        const PathGridDimension_t z = static_cast<PathGridDimension_t>(this->getPathCellZ(p.Z));
        return PathCellCoordinate_t{x, z};
    }

    inline int getPathCellX(const irr::core::vector2df& pos) { return this->getPathCellX(pos.X); }
    inline int getPathCellZ(const irr::core::vector2df& pos) { return this->getPathCellZ(pos.Y); }
    inline int getPathCellX(const irr::core::vector3df& pos) { return this->getPathCellX(pos.X); }
    inline int getPathCellZ(const irr::core::vector3df& pos) { return this->getPathCellZ(pos.Z); }
    inline irr::core::vector2df getCenterPosition(const PathCellCoordinate_t coordinate) const
    {
        return irr::core::vector2df(this->unitSize * static_cast<irr::f32>(coordinate.X),
                                    this->unitSize * static_cast<irr::f32>(coordinate.Z));
    }

    inline irr::core::vector2df getCenterPosition(const PathGridDimension_t x, const PathGridDimension_t z) const
    {
        return irr::core::vector2df(this->unitSize * static_cast<irr::f32>(x),
                                    this->unitSize * static_cast<irr::f32>(z));
    }

    inline bool collidesWithStaticObject(const irr::core::vector2df& position,
                                         const irr::f32 staticObstacleSize,
                                         const irr::f32 radius)
    {
        // get all pathCells this entity could collide with at its new position
        PathGridDimension_t pathCellXMin =
            static_cast<PathGridDimension_t>(std::max(this->getPathCellX(position.X - radius), 0));
        PathGridDimension_t pathCellXMax = static_cast<PathGridDimension_t>(
            std::min(this->getPathCellX(position.X + radius), static_cast<int>(this->width - 1)));
        // y because vector2df only has X/Y-coordinates, but all functions call the coordinate Z
        // (because that is it's 3d coordinate)
        PathGridDimension_t pathCellZMin =
            static_cast<PathGridDimension_t>(std::max(this->getPathCellZ(position.Y - radius), 0));
        PathGridDimension_t pathCellZMax = static_cast<PathGridDimension_t>(
            std::min(this->getPathCellZ(position.Y + radius), static_cast<int>(this->height - 1)));

        for (PathGridDimension_t x = pathCellXMin; x <= pathCellXMax; x++)
        {
            for (PathGridDimension_t z = pathCellZMin; z <= pathCellZMax; z++)
            {
                if (this->get(x, z) != PathGrid::Walkability::StaticNonWalkable)
                {
                    continue;
                }

                const auto pathCellCenter = this->getCenterPosition(x, z);

                const bool xIntersection =
                    std::abs(pathCellCenter.X - position.X) - staticObstacleSize / 2.0f < radius;
                const bool zIntersection =
                    std::abs(pathCellCenter.Y - position.Y) - staticObstacleSize / 2.0f < radius; // z = y because all positions are vector2df (X,Y) but 3d coordinates are (X,Z) (and thus called X,Z everywhere)

                if (xIntersection && zIntersection)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /// @brief for Pathfinding
    /// @param x
    /// @param z
    /// @return true if terrain at (x, y) is not occupied by a static object. IGNORES DYNAMIC USAGE!
    inline bool operator()(const PathGridDimension_t x, const PathGridDimension_t z) const
    {
        // check boundaries
        if (x < width && z < height)
        {
            return static_cast<Walkability>(this->map[flatAccess(x, z)]) != Walkability::StaticNonWalkable;
        }
        else
        {
            return false;
        }
    }

    inline bool operator()(const PathCellCoordinate_t coordinate) const
    {
        const size_t cellID = this->flatAccess(coordinate.X, coordinate.Z);
        if (cellID <= maxCellID)
        {
            return static_cast<Walkability>(this->map[cellID]) == Walkability::Free;
        }
        return false;
    }

    inline Walkability get(const PathGridDimension_t x, const PathGridDimension_t z) const
    {
        if (x < width && z < height)
        {
            return static_cast<Walkability>(this->map[flatAccess(x, z)]);
        }
        else
        {
            return Walkability::StaticNonWalkable;
        }
    }

    inline Walkability get(const PathCellCoordinate_t coordinate) const
    {
        const size_t cellID = this->flatAccess(coordinate.X, coordinate.Z);
        if (cellID <= maxCellID)
        {
            return static_cast<Walkability>(this->map[cellID]);
        }
        return Walkability::StaticNonWalkable;
    }

    inline void set(const PathGridDimension_t x, const PathGridDimension_t z, const Walkability& value)
    {
        if (x < width and z < height)
        {
            this->map[flatAccess(x, z)] = static_cast<char>(value);
        }
    }

    inline void set(const PathCellCoordinate_t coordinate, const Walkability& value)
    {
        const size_t cellID = this->flatAccess(coordinate.X, coordinate.Z);
        if (cellID <= maxCellID)
        {
            this->map[cellID] = static_cast<char>(value);
        }
    }

    inline void resize(const PathGridDimension_t newWidth, const PathGridDimension_t newHeight)
    {
        if (static_cast<size_t>(newWidth) * static_cast<size_t>(newHeight) >
            static_cast<size_t>(std::numeric_limits<PathGridDimension_t>::max()))
        {
            // limits::max() + 1 because there are limits::max() + 1 unique values due to '0' being
            // a valid index
            Error::errTerminate("pathGrid of size (",
                                newWidth,
                                "x",
                                newHeight,
                                ") = ",
                                static_cast<size_t>(newWidth) * static_cast<size_t>(newHeight),
                                "entries is larger than the number of possible pathGridIDs",
                                static_cast<size_t>(std::numeric_limits<PathGridDimension_t>::max()));
        }
        if (static_cast<size_t>(newWidth) * static_cast<size_t>(newHeight) ==
            static_cast<size_t>(std::numeric_limits<PathGridDimension_t>::max()))
        {
            Error::errContinue("pathGrid of size (",
                               newWidth,
                               "x",
                               newHeight,
                               ") = ",
                               static_cast<size_t>(newWidth) * static_cast<size_t>(newHeight),
                               "entries means all",
                               static_cast<size_t>(std::numeric_limits<PathGridDimension_t>::max()) + 1,
                               "pathGridCellIDs are valid and the special 'InvalidCellID =",
                               this->InvalidIndex,
                               "' loses it's meaning");
        }
        this->map.resize(newWidth * newHeight);
        for (size_t i = 0; i < this->map.size(); i++)
        {
            this->map[i] = static_cast<decltype(this->map)::value_type>(Walkability::Free);
        }
        this->maxCellID = static_cast<PathGridDimension_t>(newWidth * newWidth - 1);
        this->gridSize = newWidth * newHeight;
    }

    /// @brief reset the whole grid to Walkability::Free and Walkability::StaticNonWalkable
    void resetDynamicWalkability()
    {
        for (PathGridDimension_t i = 0; i < this->gridSize; i++)
        {
            if (static_cast<Walkability>(this->map[i]) != Walkability::StaticNonWalkable)
            {
                this->map[i] = static_cast<char>(Walkability::Free);
            }
        }
    }

    // in our coordinates one unitsize is 64.fx64.f -> one forth of one square made up of two
    // triangles
    // so our array has to contain heightofGameMap/64 * widthofGameMap/64 entries -> for our typical
    // map: exactly 65536
    PathGridDimension_t width = 0;
    PathGridDimension_t height = 0;
    size_t gridSize = 0;
    size_t maxCellID = 0; // cellIDs range from 0 <= ID <= maxCellID but 0 <= ID < gridSize!
    static constexpr irr::f32 unitSize = 64.f;
    static constexpr irr::f32 staticObstacleSize = unitSize / 2.0f;

   private:
    // using a flattened array and a char vector instead of std::vector<bool> because the bool
    // vector-access is slower:
    // https://stackoverflow.com/questions/6461487/vectorbool-access

    std::vector<char> map;
    inline size_t flatAccess(const PathGridDimension_t x, const PathGridDimension_t z) const
    {
        return static_cast<size_t>(x) * static_cast<size_t>(this->height) + static_cast<size_t>(z);
    }
};

#endif // PATHGRID_H
