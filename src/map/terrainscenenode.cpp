// terrainscenenode for Battletanks. Heavily based on the CTerrainSceneNode from Irrlicht.
// For the license of this file see licenses/irrlicht.

// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

// The code for the TerrainSceneNode is based on the GeoMipMapSceneNode
// developed by Spintz. He made it available for Irrlicht and allowed it to be
// distributed under this licence. I only modified some parts. A lot of thanks go to him.

#include "../utils/stringwstream.h" // stringwstream.h needs to be included before debug.h and error.h because they use use operator<< on a stringw. (error.h and debug.h are included by various files)

#include "terrainscenenode.h"

#include <cmath>
#include <cstdlib>

#include <irrlicht/CDynamicMeshBuffer.h>
#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/IDynamicMeshBuffer.h>
#include <irrlicht/IFileSystem.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/ITimer.h>
#include <irrlicht/IVideoDriver.h>
#include <irrlicht/SMesh.h>
#include <irrlicht/SViewFrustum.h>
#include <irrlicht/irrMath.h> // to clamp numbers to min,max values
#include <irrlicht/path.h>

#include "mapSystemVersion.h"
#include "terrainVertexSelector.h"
#include "terraintriangleselector.h"
#include "../utils/binary.h"
#include "../utils/printfunctions.h"
#include "../utils/stringwparser.h"

using namespace irr;
using namespace scene;

TerrainSceneNode::TerrainSceneNode(irr::scene::ISceneNode* parent,
                                   irr::scene::ISceneManager* mgr,
                                   irr::io::IFileSystem* fs,
                                   irr::s32 id,
                                   irr::s32 maxLOD,
                                   irr::scene::E_TERRAIN_PATCH_SIZE patchSize,
                                   const irr::core::vector3df& position,
                                   const irr::core::vector3df& rotation,
                                   const irr::core::vector3df& scale)
    : ITerrainSceneNode(parent, mgr, id, position, rotation, scale)
    , TerrainData(patchSize, maxLOD, position, rotation, scale)
    , RenderBuffer(nullptr)
    , VerticesToRender(0)
    , IndicesToRender(0)
    , DynamicSelectorUpdate(false)
    , OverrideDistanceThreshold(false)
    , UseDefaultRotationPivot(true)
    , ForceRecalculation(true)
    , OldCameraFOV(0)
    , CameraMovementDelta(10.0f)
    , CameraRotationDelta(1.0f)
    , CameraFOVDelta(0.1f)
    , TCoordScale1(1.0f)
    , TCoordScale2(1.0f)
    , SmoothFactor(0)
    , FileSystem(fs)
    , levelMap()
{
#ifdef _DEBUG
    setDebugName("TerrainSceneNode");
#endif

    Mesh = new SMesh();
    RenderBuffer = new CDynamicMeshBuffer(video::EVT_2TCOORDS, video::EIT_16BIT);
    RenderBuffer->setHardwareMappingHint(scene::EHM_STATIC, scene::EBT_VERTEX);
    RenderBuffer->setHardwareMappingHint(scene::EHM_DYNAMIC, scene::EBT_INDEX);

    if (FileSystem)
    {
        FileSystem->grab();
    }

    setAutomaticCulling(scene::EAC_OFF);
}


//! destructor

TerrainSceneNode::~TerrainSceneNode()
{
    if (this->FileSystem)
    {
        this->FileSystem->drop();
    }

    if (this->Mesh)
    {
        this->Mesh->drop();
    }

    if (this->RenderBuffer)
    {
        this->RenderBuffer->drop();
    }
}

bool TerrainSceneNode::loadHeightMapRAW(io::IReadFile*, s32, bool, bool, s32, video::SColor, s32)
{
    Error::errTerminate("not implemented!");
}

bool TerrainSceneNode::loadSpecialMap(const irr::io::path& patchesFile)
{
    Mesh->MeshBuffers.clear();

    if (this->loadPatchesData(patchesFile) != ErrCodes::NO_ERR)
    {
        debugOutLevel(Debug::DebugLevels::stateInit + 1, "failed to correctly load patches data from", patchesFile);
        return false;
    }
    debugOutLevel(Debug::DebugLevels::stateInit + 1,
                  "loaded map with",
                  this->TerrainData.PatchCountX,
                  "x",
                  this->TerrainData.PatchCountZ,
                  "patches");

    // calculate all the necessary data for the patches and the terrain
    calculateDistanceThresholds();
    calculatePatchData();

    // set the default rotation pivot point to the terrain nodes center
    TerrainData.RotationPivot = TerrainData.Center;

    // Rotate the vertices of the terrain by the rotation
    // specified. Must be done after calculating the terrain data,
    // so we know what the current center of the terrain is.
    setRotation(TerrainData.Rotation);

    // the last thing we want to do is mark the buffer as dirty so that it will be redrawn.
    RenderBuffer->setDirty();
    return true;
}

ErrCode TerrainSceneNode::loadPatchesData(const irr::io::path& filename)
{
    if (filename.equals_substring_ignore_case(
            ".xml", static_cast<irr::s32>(filename.size() - irr::io::path(".xml").size())))
    {
        return this->loadPatchesDataXML(filename);
    }
    else
    {
        return this->loadPatchesDataBinary(filename);
    }
}

ErrCode TerrainSceneNode::loadPatchesDataXML(const irr::io::path& filename)
{
    irr::io::IrrXMLReader* xml = irr::io::createIrrXMLReader(filename.c_str());
    if (xml == nullptr)
    {
        Error::errContinue("couldn't open file", filename.c_str());
        return ErrCode(ErrCodes::GENERIC_ERR);
    }

    unsigned int lineNum = 0;

    // objects to hold temporary parsed stuff
    // all changes will only be applied at the succesfull end of parsing
    std::vector<std::vector<SPatch>> patches;
    std::vector<std::vector<tileMap::Levels_t>> levels;
    std::vector<irr::video::S3DVertex2TCoords> vertices;
    SPatch* currentPatch = nullptr;

    while (xml->read())
    {
        lineNum++;
        switch (xml->getNodeType())
        {
            case irr::io::EXN_ELEMENT:
            {
                if (irr::core::stringw(L"mapSystem").equals_ignore_case(xml->getNodeName()))
                {
                    irr::f32 version = -1.0;
                    if (StringWParser::parse(xml->getAttributeValueSafe("version"), version, -1.0f) == ErrCodes::NO_ERR)
                    {
                        if (std::fabs(version - tileMap::MapSystemVersion::version) > 1e-5)
                        {
                            Error::errContinue("file",
                                               filename,
                                               "was generated using map system version",
                                               version,
                                               "but the current version is",
                                               tileMap::MapSystemVersion::version,
                                               "re-generate the map using newest mapEditor");
                            return ErrCodes::GENERIC_ERR;
                        }
                    }
                    else
                    {
                        Error::errContinue("couldn't read version out of", filename, " on line", lineNum, "map loading might fail unexpectedly");
                    }
                }
                else if (irr::core::stringw(L"Map").equals_ignore_case(xml->getNodeName()))
                {
                    irr::u32 patchCountX = 0;
                    irr::u32 patchCountZ = 0;
                    if (StringWParser::parse(xml->getAttributeValueSafe("PatchCountX"),
                                             patchCountX,
                                             static_cast<irr::u32>(1)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse PatchCountX",
                                           repr<const char*>(
                                               xml->getAttributeValueSafe("PatchCountX")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    if (StringWParser::parse(xml->getAttributeValueSafe("PatchCountZ"),
                                             patchCountZ,
                                             static_cast<irr::u32>(1)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse PatchCountZ",
                                           repr<const char*>(
                                               xml->getAttributeValueSafe("PatchCountZ")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    patches = this->createPatches(patchCountX, patchCountZ);

                    irr::u32 tileCountX = 0;
                    irr::u32 tileCountZ = 0;
                    if (StringWParser::parse(xml->getAttributeValueSafe("TileCountX"),
                                             tileCountX,
                                             static_cast<irr::u32>(1)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse TileCountX",
                                           repr<const char*>(
                                               xml->getAttributeValueSafe("TileCountX")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    if (StringWParser::parse(xml->getAttributeValueSafe("TileCountZ"),
                                             tileCountZ,
                                             static_cast<irr::u32>(1)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse TileCountZ",
                                           repr<const char*>(
                                               xml->getAttributeValueSafe("TileCountZ")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    levels = this->createLevels(tileCountX, tileCountZ);
                }
                else if (irr::core::stringw(L"level").equals_ignore_case(xml->getNodeName()))
                {
                    unsigned int levelX = 0;
                    unsigned int levelZ = 0;
                    if (StringWParser::parse(xml->getAttributeValueSafe("LevelX"),
                                             levelX,
                                             static_cast<unsigned int>(0)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse LevelX",
                                           repr<const char*>(xml->getAttributeValueSafe("LevelX")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    if (StringWParser::parse(xml->getAttributeValueSafe("LevelZ"),
                                             levelZ,
                                             static_cast<unsigned int>(0)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse LevelZ",
                                           repr<const char*>(xml->getAttributeValueSafe("LevelZ")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }

                    if (levelX >= levels.size() or levelZ >= levels[0].size())
                    {
                        Error::errContinue("no level at position", levelX, levelZ);
                        delete xml;
                        return ErrCodes::GENERIC_ERR;
                    }
                    else
                    {
                        if (StringWParser::parse(xml->getAttributeValueSafe("levels"),
                                                 levels[levelX][levelZ],
                                                 tileMap::Levels_t()) != ErrCode(0))
                        {
                            Error::errContinue("couldn't parse 'levels'",
                                               repr<const char*>(
                                                   xml->getAttributeValueSafe("levels")),
                                               "in file",
                                               filename.c_str(),
                                               "at line",
                                               lineNum);
                        }
                        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                                      "read levels",
                                      levels[levelX][levelZ].to_string());
                    }
                }
                else if (irr::core::stringw(L"patch").equals_ignore_case(xml->getNodeName()))
                {
                    unsigned int patchX = 0;
                    unsigned int patchZ = 0;
                    if (StringWParser::parse(xml->getAttributeValueSafe("PatchX"),
                                             patchX,
                                             static_cast<unsigned int>(0)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse PatchX",
                                           repr<const char*>(xml->getAttributeValueSafe("PatchX")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    if (StringWParser::parse(xml->getAttributeValueSafe("PatchZ"),
                                             patchZ,
                                             static_cast<unsigned int>(0)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse PatchZ",
                                           repr<const char*>(xml->getAttributeValueSafe("PatchZ")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    if (patchX >= patches.size() and patchZ >= patches[0].size())
                    {
                        Error::errContinue("no patch at position", patchX, patchZ);
                        delete xml;
                        return ErrCodes::GENERIC_ERR;
                    }
                    else
                    {
                        currentPatch = &patches[patchX][patchZ];
                    }
                }
                else if (irr::core::stringw(L"vertex").equals_ignore_case(xml->getNodeName()))
                {
                    irr::video::S3DVertex2TCoords vertex;
                    if (StringWParser::parse(xml->getAttributeValueSafe("pos"),
                                             vertex.Pos,
                                             irr::core::vector3df(0.0f, 0.0f, 0.0f)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse vertex position (pos)",
                                           repr<const char*>(xml->getAttributeValueSafe("pos")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    if (StringWParser::parse(xml->getAttributeValueSafe("normal"),
                                             vertex.Normal,
                                             irr::core::vector3df(0.0f, 0.0f, 0.0f)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse vertex normal",
                                           repr<const char*>(xml->getAttributeValueSafe("normal")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    if (StringWParser::parse(xml->getAttributeValueSafe("tcoords"),
                                             vertex.TCoords,
                                             irr::core::vector2df(0.0f, 0.0f)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse vertex texture coordinates (tcoords)",
                                           repr<const char*>(xml->getAttributeValueSafe("tcoords")),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    if (StringWParser::parse(xml->getAttributeValueSafe("tcoords2"),
                                             vertex.TCoords2,
                                             irr::core::vector2df(0.0f, 0.0f)) != ErrCode(0))
                    {
                        Error::errContinue(
                            "couldn't parse vertex 2nd texture coordinates (tcoords2)",
                            repr<const char*>(xml->getAttributeValueSafe("tcoords2")),
                            "in file",
                            filename.c_str(),
                            "at line",
                            lineNum);
                    }
                    // the vertex has been parsed -> put it into the mesh
                    vertices.push_back(vertex);
                }
                else if (irr::core::stringw(L"index").equals_ignore_case(xml->getNodeName()))
                {
                    irr::u32 index = 0;
                    if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                             index,
                                             static_cast<irr::u32>(0)) != ErrCode(0))
                    {
                        Error::errContinue("couldn't parse index value=",
                                           xml->getAttributeValueSafe("value"),
                                           "in file",
                                           filename.c_str(),
                                           "at line",
                                           lineNum);
                    }
                    if (currentPatch == nullptr)
                    {
                        Error::errContinue("couldn't insert index",
                                           index,
                                           "into the current Patch "
                                           "because currentPatch "
                                           "== nullptr!");
                        delete xml;
                        return ErrCodes::GENERIC_ERR;
                    }
                    currentPatch->indices.push_back(index);
                }
            } // } after case irr::io::EXN_ELEMENT:
            break;
            case irr::io::EXN_ELEMENT_END:
            {
                if (irr::core::stringw(L"patch").equals_ignore_case(xml->getNodeName()))
                {
                    currentPatch = nullptr;
                }
            }
            break;
            case irr::io::EXN_NONE:
            case irr::io::EXN_TEXT:
            case irr::io::EXN_COMMENT:
            case irr::io::EXN_CDATA:
            case irr::io::EXN_UNKNOWN:
            default:
                debugOutLevel(Debug::DebugLevels::maxLevel,
                              "unhandled xml entry type",
                              static_cast<int>(xml->getNodeType()));
                break;
        } // switch (xml->getNodeType())
    }     // while (xml->read())
    delete xml;

    // copy over the patches
    this->TerrainData.Patches = patches;
    this->levelMap = levels;
    this->TerrainData.PatchCountX = static_cast<irr::u32>(patches.size());
    this->TerrainData.PatchCountZ = static_cast<irr::u32>(patches[0].size());
    this->TerrainData.tileCountX = static_cast<irr::u32>(levels.size());
    this->TerrainData.tileCountZ = static_cast<irr::u32>(levels[0].size());
    debugOutLevel(Debug::DebugLevels::stateInit + 2,
                  "parsed patches with PatchCountX=",
                  this->TerrainData.PatchCountX,
                  "PatchCountZ=",
                  this->TerrainData.PatchCountZ,
                  "levelCountX=",
                  this->TerrainData.tileCountX,
                  "levelCountZ=",
                  this->TerrainData.tileCountZ);

    const irr::u32 numVertices = static_cast<irr::u32>(vertices.size());
    irr::u32 numIndices = 0;
    for (size_t patchX = 0; patchX < patches.size(); patchX++)
    {
        for (size_t patchZ = 0; patchZ < patches[patchX].size(); patchZ++)
        {
            numIndices += patches[patchX][patchZ].indices.size();
        }
    }
    debugOutLevel(Debug::DebugLevels::stateInit + 2, "numVertices=", numVertices, "numIndices=", numIndices);

    irr::video::E_INDEX_TYPE indexType = irr::video::EIT_16BIT;
    // is the map small enough to fit 16 bit indices?
    if (numVertices != static_cast<irr::u32>(static_cast<irr::u16>(numVertices)))
    {
        indexType = irr::video::EIT_32BIT;
    }
    scene::CDynamicMeshBuffer* mb = new scene::CDynamicMeshBuffer(video::EVT_2TCOORDS, indexType);
    // allocate space for the vertices
    mb->getVertexBuffer().set_used(numVertices);

    for (irr::u32 i = 0; i < numVertices; i++)
    {
        static_cast<irr::video::S3DVertex2TCoords*>(mb->getVertices())[i] = vertices[i];
    }
    this->Mesh->addMeshBuffer(mb);

    // We copy the data to the renderBuffer, after the normals have been calculated.
    this->RenderBuffer->getIndexBuffer().setType(indexType);
    // Pre-allocate memory for indices
    this->RenderBuffer->getVertexBuffer().set_used(numVertices);

    for (u32 i = 0; i < numVertices; ++i)
    {
        static_cast<irr::video::S3DVertex2TCoords*>(this->RenderBuffer->getVertices())[i] = vertices[i];
        RenderBuffer->getVertexBuffer()[i].Pos *= TerrainData.Scale;
        RenderBuffer->getVertexBuffer()[i].Pos += TerrainData.Position;
    }

    // We no longer need the mb
    mb->drop();

    // set the correct size of the indexBuffer otherwise visual artifacts can happen because more
    // space was reserved than currently needed.
    RenderBuffer->getIndexBuffer().set_used(numIndices);

    debugOutLevel(Debug::DebugLevels::stateInit + 2, "patches data loading done succesfully!");
    return ErrCodes::NO_ERR;
}

ErrCode TerrainSceneNode::loadPatchesDataBinary(const irr::io::path& filename)
{
    std::fstream inFile;
    inFile.open(filename.c_str(), std::ios::in | std::ios::binary);
    if (not inFile.good())
    {
        Error::errContinue("couldn't open file", filename.c_str());
        return ErrCode(ErrCodes::GENERIC_ERR);
    }

    unsigned int lineNum = 0;

    // objects to hold temporary parsed stuff
    // all changes will only be applied at the succesfull end of parsing
    std::vector<std::vector<SPatch>> patches;
    std::vector<std::vector<tileMap::Levels_t>> levels;
    std::vector<irr::video::S3DVertex2TCoords> vertices;

    irr::f32 version = -1.0;
    Binary::readBinary(inFile, version, FLT_MAX);
    debugOutLevel(Debug::DebugLevels::secondOrderLoop, "map system version=", version);
    if (std::fabs(version - tileMap::MapSystemVersion::version) > 1e-5)
    {
        Error::errContinue("file",
                           filename,
                           "was generated using map system version",
                           version,
                           "but the current version is",
                           tileMap::MapSystemVersion::version,
                           "re-generate the map using newest mapEditor");
        return ErrCodes::GENERIC_ERR;
    }
    if (inFile.fail())
    {
        Error::errContinue("couldn't read version out of", filename, " on line", lineNum, "map loading might fail unexpectedly");
        return ErrCodes::GENERIC_ERR;
    }

    irr::u32 patchCountX = 0;
    irr::u32 patchCountZ = 0;
    if (Binary::readBinary(inFile, patchCountX, static_cast<irr::u32>(1)) != ErrCodes::NO_ERR)
    {
        Error::errContinue("couldn't read PatchCountX in file", filename.c_str());
        return ErrCodes::GENERIC_ERR;
    }
    debugOutLevel(Debug::DebugLevels::secondOrderLoop, "patchCountX=", patchCountX);
    if (Binary::readBinary(inFile, patchCountZ, static_cast<irr::u32>(1)) != ErrCodes::NO_ERR)
    {
        Error::errContinue("couldn't read PatchCountZ in file", filename.c_str());
        return ErrCodes::GENERIC_ERR;
    }
    patches = this->createPatches(patchCountX, patchCountZ);

    irr::u32 tileCountX = 0;
    irr::u32 tileCountZ = 0;
    if (Binary::readBinary(inFile, tileCountX, static_cast<irr::u32>(1)) != ErrCode(0))
    {
        Error::errContinue("couldn't read TileCountX", "in file", filename.c_str());
        return ErrCodes::GENERIC_ERR;
    }
    if (Binary::readBinary(inFile, tileCountZ, static_cast<irr::u32>(1)) != ErrCode(0))
    {
        Error::errContinue("couldn't read TileCountZ", "in file", filename.c_str());
        return ErrCodes::GENERIC_ERR;
    }
    levels = this->createLevels(tileCountX, tileCountZ);

    for (irr::u32 levelX = 0; levelX < tileCountX; levelX++)
    {
        for (irr::u32 levelZ = 0; levelZ < tileCountZ; levelZ++)
        {
            tileMap::Levels_t::BinaryData binaryData;
            inFile.read(reinterpret_cast<char*>(&binaryData), sizeof(binaryData));
            if (inFile.fail())
            {
                Error::errContinue("couldn't read levels at", levelX, levelZ, "in file", filename.c_str());
                return ErrCodes::GENERIC_ERR;
            }
            levels[levelX][levelZ].setFromBinary(binaryData);
            debugOutLevel(Debug::DebugLevels::secondOrderLoop, "read levels", levels[levelX][levelZ].to_string());
        }
    }

    uint32_t vertexCount;
    if (Binary::readBinary(inFile, vertexCount, static_cast<uint32_t>(0)) != ErrCodes::NO_ERR)
    {
        Error::errContinue("couldn't read vertex count in file", filename.c_str());
        return ErrCodes::GENERIC_ERR;
    }

    for (uint32_t v = 0; v < vertexCount; v++)
    {
        irr::video::S3DVertex2TCoords vertex;
        if (Binary::readBinary(inFile, vertex.Pos.X, 0.0f) != ErrCodes::NO_ERR)
        {
            Error::errContinue("couldn't read vertex position x value in file", filename.c_str());
            return ErrCodes::GENERIC_ERR;
        }
        if (Binary::readBinary(inFile, vertex.Pos.Y, 0.0f) != ErrCodes::NO_ERR)
        {
            Error::errContinue("couldn't read vertex position y value in file", filename.c_str());
            return ErrCodes::GENERIC_ERR;
        }
        if (Binary::readBinary(inFile, vertex.Pos.Z, 0.0f) != ErrCodes::NO_ERR)
        {
            Error::errContinue("couldn't read vertex position z value in file", filename.c_str());
            return ErrCodes::GENERIC_ERR;
        }

        if (Binary::readBinary(inFile, vertex.Normal.X, 0.0f) != ErrCodes::NO_ERR)
        {
            Error::errContinue("couldn't read vertex normal x value in file", filename.c_str());
            return ErrCodes::GENERIC_ERR;
        }
        if (Binary::readBinary(inFile, vertex.Normal.Y, 0.0f) != ErrCodes::NO_ERR)
        {
            Error::errContinue("couldn't read vertex normal y value in file", filename.c_str());
            return ErrCodes::GENERIC_ERR;
        }
        if (Binary::readBinary(inFile, vertex.Normal.Z, 0.0f) != ErrCodes::NO_ERR)
        {
            Error::errContinue("couldn't read vertex normal z value in file", filename.c_str());
            return ErrCodes::GENERIC_ERR;
        }

        if (Binary::readBinary(inFile, vertex.TCoords.X, 0.0f) != ErrCodes::NO_ERR)
        {
            Error::errContinue("couldn't read vertex tcoords 1 X value in file", filename.c_str());
            return ErrCodes::GENERIC_ERR;
        }
        if (Binary::readBinary(inFile, vertex.TCoords.Y, 0.0f) != ErrCodes::NO_ERR)
        {
            Error::errContinue("couldn't read vertex tcoords 1 Y value in file", filename.c_str());
            return ErrCodes::GENERIC_ERR;
        }

        if (Binary::readBinary(inFile, vertex.TCoords2.X, 0.0f) != ErrCodes::NO_ERR)
        {
            Error::errContinue("couldn't read vertex tcoords2 X value in file", filename.c_str());
            return ErrCodes::GENERIC_ERR;
        }
        if (Binary::readBinary(inFile, vertex.TCoords2.Y, 0.0f) != ErrCodes::NO_ERR)
        {
            Error::errContinue("couldn't read vertex tcoords2 Y value in file", filename.c_str());
            return ErrCodes::GENERIC_ERR;
        }

        // the vertex has been parsed -> put it into the mesh
        vertices.push_back(vertex);
    } // for (uint32_t v = 0; v < vertexCount; v++) {

    for (irr::u32 patchX = 0; patchX < patchCountX; patchX++)
    {
        for (irr::u32 patchZ = 0; patchZ < patchCountZ; patchZ++)
        {
            irr::u32 indexCount;
            if (Binary::readBinary(inFile, indexCount, 0) != ErrCodes::NO_ERR)
            {
                Error::errContinue(
                    "couldn't read index count for patch", patchX, patchZ, "in file", filename.c_str());
                return ErrCodes::GENERIC_ERR;
            }
            for (irr::u32 i = 0; i < indexCount; i++)
            {
                irr::u32 index;
                if (Binary::readBinary(inFile, index, 0) != ErrCodes::NO_ERR)
                {
                    Error::errContinue("couldn't read index nr.",
                                       i,
                                       "for patch",
                                       patchX,
                                       patchZ,
                                       "in file",
                                       filename.c_str());
                    return ErrCodes::GENERIC_ERR;
                }
                patches[patchX][patchZ].indices.push_back(index);
            }
        } // for (irr::u32 patchZ = 0; patchZ < patchCountZ; patchZ++)
    }     // for (irr::u32 patchX = 0; patchX < patchCountX; patchX++)

    // copy over the patches
    this->TerrainData.Patches = patches;
    this->levelMap = levels;
    this->TerrainData.PatchCountX = static_cast<irr::u32>(patches.size());
    this->TerrainData.PatchCountZ = static_cast<irr::u32>(patches[0].size());
    this->TerrainData.tileCountX = static_cast<irr::u32>(levels.size());
    this->TerrainData.tileCountZ = static_cast<irr::u32>(levels[0].size());
    debugOutLevel(Debug::DebugLevels::stateInit + 2,
                  "parsed patches with PatchCountX=",
                  this->TerrainData.PatchCountX,
                  "PatchCountZ=",
                  this->TerrainData.PatchCountZ,
                  "levelCountX=",
                  this->TerrainData.tileCountX,
                  "levelCountZ=",
                  this->TerrainData.tileCountZ);

    const irr::u32 numVertices = static_cast<irr::u32>(vertices.size());
    irr::u32 numIndices = 0;
    for (size_t patchX = 0; patchX < patches.size(); patchX++)
    {
        for (size_t patchZ = 0; patchZ < patches[patchX].size(); patchZ++)
        {
            numIndices += patches[patchX][patchZ].indices.size();
        }
    }
    debugOutLevel(Debug::DebugLevels::stateInit + 2, "numVertices=", numVertices, "numIndices=", numIndices);

    irr::video::E_INDEX_TYPE indexType = irr::video::EIT_16BIT;
    // is the map small enough to fit 16 bit indices?
    if (numVertices != static_cast<irr::u32>(static_cast<irr::u16>(numVertices)))
    {
        indexType = irr::video::EIT_32BIT;
    }
    scene::CDynamicMeshBuffer* mb = new scene::CDynamicMeshBuffer(video::EVT_2TCOORDS, indexType);
    // allocate space for the vertices
    mb->getVertexBuffer().set_used(numVertices);

    for (irr::u32 i = 0; i < numVertices; i++)
    {
        static_cast<irr::video::S3DVertex2TCoords*>(mb->getVertices())[i] = vertices[i];
    }
    this->Mesh->addMeshBuffer(mb);

    // We copy the data to the renderBuffer, after the normals have been calculated.
    this->RenderBuffer->getIndexBuffer().setType(indexType);
    // Pre-allocate memory for indices
    this->RenderBuffer->getVertexBuffer().set_used(numVertices);

    for (u32 i = 0; i < numVertices; ++i)
    {
        static_cast<irr::video::S3DVertex2TCoords*>(this->RenderBuffer->getVertices())[i] = vertices[i];
        RenderBuffer->getVertexBuffer()[i].Pos *= TerrainData.Scale;
        RenderBuffer->getVertexBuffer()[i].Pos += TerrainData.Position;
    }

    // We no longer need the mb
    mb->drop();

    // set the correct size of the indexBuffer otherwise visual artifacts can happen because more
    // space was reserved than currently needed.
    RenderBuffer->getIndexBuffer().set_used(numIndices);

    debugOutLevel(Debug::DebugLevels::stateInit + 2, "patches data loading done succesfully!");
    return ErrCodes::NO_ERR;
}

//! Initializes the terrain data. Loads the vertices from the heightMapFile

bool TerrainSceneNode::loadHeightMap(io::IReadFile*, video::SColor, s32)
{
    Error::errTerminate("not implemented");
}

//! Returns the mesh

irr::scene::IMesh* TerrainSceneNode::getMesh()
{
    return Mesh;
}


//! Returns the material based on the zero based index i.

video::SMaterial& TerrainSceneNode::getMaterial(u32 i)
{
    return Mesh->getMeshBuffer(i)->getMaterial();
}


//! Returns amount of materials used by this scene node ( always 1 )

u32 TerrainSceneNode::getMaterialCount() const
{
    return Mesh->getMeshBufferCount();
}


//! Sets the scale of the scene node.
//! \param scale: New scale of the node

void TerrainSceneNode::setScale(const core::vector3df& scale)
{
    TerrainData.Scale = scale;
    applyTransformation();
    ForceRecalculation = true;
}


//! Sets the rotation of the node. This only modifies
//! the relative rotation of the node.
//! \param rotation: New rotation of the node in degrees.

void TerrainSceneNode::setRotation(const core::vector3df& rotation)
{
    TerrainData.Rotation = rotation;
    applyTransformation();
    ForceRecalculation = true;
}


//! Sets the pivot point for rotation of this node. This is useful for the TiledTerrainManager to
//! rotate all terrain tiles around a global world point.
//! NOTE: The default for the RotationPivot will be the center of the individual tile.

void TerrainSceneNode::setRotationPivot(const core::vector3df& pivot)
{
    UseDefaultRotationPivot = false;
    TerrainData.RotationPivot = pivot;
}


//! Sets the position of the node.
//! \param newpos: New postition of the scene node.

void TerrainSceneNode::setPosition(const core::vector3df& newpos)
{
    TerrainData.Position = newpos;
    applyTransformation();
    ForceRecalculation = true;
}


//! Apply transformation changes(scale, position, rotation)

void TerrainSceneNode::applyTransformation()
{
    if (!Mesh->getMeshBufferCount())
    {
        return;
    }

    core::matrix4 rotMatrix;
    rotMatrix.setRotationDegrees(TerrainData.Rotation);

    const s32 vtxCount = Mesh->getMeshBuffer(0)->getVertexCount();
    for (s32 i = 0; i < vtxCount; ++i)
    {
        RenderBuffer->getVertexBuffer()[i].Pos =
            Mesh->getMeshBuffer(0)->getPosition(i) * TerrainData.Scale + TerrainData.Position;

        RenderBuffer->getVertexBuffer()[i].Pos -= TerrainData.RotationPivot;
        rotMatrix.inverseRotateVect(RenderBuffer->getVertexBuffer()[i].Pos);
        RenderBuffer->getVertexBuffer()[i].Pos += TerrainData.RotationPivot;
    }

    calculateDistanceThresholds(true);
    calculatePatchData();

    RenderBuffer->setDirty(EBT_VERTEX);
}


//! Updates the scene nodes indices if the camera has moved or rotated by a certain
//! threshold, which can be changed using the SetCameraMovementDeltaThreshold and
//! SetCameraRotationDeltaThreshold functions. This also determines if a given patch
//! for the scene node is within the view frustum and if it's not the indices are not
//! generated for that patch.

void TerrainSceneNode::OnRegisterSceneNode()
{
    if (!IsVisible || !SceneManager->getActiveCamera())
    {
        return;
    }

    SceneManager->registerNodeForRendering(this);

    preRenderCalculationsIfNeeded();

    // Do Not call ISceneNode::OnRegisterSceneNode(), this node should have no children (luke: is
    // this comment still true, as ISceneNode::OnRegisterSceneNode() is called?)

    ISceneNode::OnRegisterSceneNode();
    ForceRecalculation = false;
}

void TerrainSceneNode::preRenderCalculationsIfNeeded()
{
    scene::ICameraSceneNode* camera = SceneManager->getActiveCamera();
    if (!camera)
    {
        return;
    }

    // Determine the camera rotation, based on the camera direction.
    const core::vector3df cameraPosition = camera->getAbsolutePosition();
    const core::vector3df cameraRotation =
        core::line3d<f32>(cameraPosition, camera->getTarget()).getVector().getHorizontalAngle();
    core::vector3df cameraUp = camera->getUpVector();
    cameraUp.normalize();
    const f32 CameraFOV = SceneManager->getActiveCamera()->getFOV();

    // Only check on the Camera's Y Rotation
    if (!ForceRecalculation)
    {
        if ((fabsf(cameraRotation.X - OldCameraRotation.X) < CameraRotationDelta) &&
            (fabsf(cameraRotation.Y - OldCameraRotation.Y) < CameraRotationDelta))
        {
            if ((fabs(cameraPosition.X - OldCameraPosition.X) < CameraMovementDelta) &&
                (fabs(cameraPosition.Y - OldCameraPosition.Y) < CameraMovementDelta) &&
                (fabs(cameraPosition.Z - OldCameraPosition.Z) < CameraMovementDelta))
            {
                if (fabs(CameraFOV - OldCameraFOV) < CameraFOVDelta &&
                    cameraUp.dotProduct(OldCameraUp) > (1.f - (cos(core::DEGTORAD * CameraRotationDelta))))
                {
                    return;
                }
            }
        }
    }

    // we need to redo calculations...

    OldCameraPosition = cameraPosition;
    OldCameraRotation = cameraRotation;
    OldCameraUp = cameraUp;
    OldCameraFOV = CameraFOV;

    preRenderLODCalculations();
    preRenderIndicesCalculations();
}

void TerrainSceneNode::preRenderLODCalculations()
{
    scene::ICameraSceneNode* camera = SceneManager->getActiveCamera();

    if (!camera)
    {
        return;
    }

    const core::vector3df cameraPosition = camera->getAbsolutePosition();

    const SViewFrustum* frustum = camera->getViewFrustum();

    // Determine each patches LOD based on distance from camera (and whether or not they are in
    // the view frustum).
    for (unsigned int x = 0; x < TerrainData.PatchCountX; x++)
    {
        for (unsigned int z = 0; z < TerrainData.PatchCountZ; z++)
        {
            if (frustum->getBoundingBox().intersectsWithBox(TerrainData.Patches[x][z].BoundingBox))
            {
                const f32 distance = cameraPosition.getDistanceFromSQ(TerrainData.Patches[x][z].Center);
                TerrainData.Patches[x][z].CurrentLOD = 0;
                for (s32 i = TerrainData.MaxLOD - 1; i > 0; --i)
                {
                    if (distance >= TerrainData.LODDistanceThreshold[i])
                    {
                        TerrainData.Patches[x][z].CurrentLOD = i;
                        break;
                    }
                }
            }
            else
            {
                TerrainData.Patches[x][z].CurrentLOD = -1;
            }
        }
    }
}

void TerrainSceneNode::preRenderIndicesCalculations()
{
    scene::IIndexBuffer& indexBuffer = RenderBuffer->getIndexBuffer();
    IndicesToRender = 0;
    indexBuffer.set_used(0);

    // generate the indices for all patches that are visible.
    for (irr::u32 x = 0; x < TerrainData.PatchCountX; ++x)
    {
        for (irr::u32 z = 0; z < TerrainData.PatchCountZ; ++z)
        {
            if (TerrainData.Patches[x][z].CurrentLOD >= 0) // patch is visible
            {
                const irr::u32 indexCount = TerrainData.Patches[x][z].indices.size();
                // add indices from patch to indices to render
                for (irr::u32 i = 0; i < indexCount; i++)
                {
                    indexBuffer.push_back(this->TerrainData.Patches[x][z].indices[i]);
                }
                IndicesToRender += indexCount;
            }
        }
    }

    debugOutLevel(Debug::DebugLevels::onEvent,
                  "preRenderIndicesCalculations set",
                  IndicesToRender,
                  "indices to render, size=",
                  indexBuffer.size());

    RenderBuffer->setDirty(EBT_INDEX);

    if (DynamicSelectorUpdate && TriangleSelector)
    {
        TerrainTriangleSelector* selector = static_cast<TerrainTriangleSelector*>(TriangleSelector);
        selector->setTriangleData(this, -1);
    }
    if (DynamicSelectorUpdate && this->vertexSelector)
    {
        TerrainVertexSelector* selector = static_cast<TerrainVertexSelector*>(this->vertexSelector);
        selector->setTriangleData(this, -1);
    }
}


//! Render the scene node

void TerrainSceneNode::render()
{
    if (!IsVisible || !SceneManager->getActiveCamera())
    {
        return;
    }

    if (!Mesh->getMeshBufferCount())
    {
        return;
    }

    if (IndicesToRender == 0)
    {
        // don't submit an empty draw call
        return;
    }

    video::IVideoDriver* driver = SceneManager->getVideoDriver();

    driver->setTransform(video::ETS_WORLD, core::IdentityMatrix);
    driver->setMaterial(Mesh->getMeshBuffer(0)->getMaterial());

    debugOutLevel(Debug::DebugLevels::updateLoop - 1, "Sending IndicesToRender=", IndicesToRender, "to RenderBuffer");
    // TODO: check why there is such weird size setting before and after the rendering call
    RenderBuffer->getIndexBuffer().set_used(IndicesToRender);

    // For use with geomorphing
    driver->drawMeshBuffer(RenderBuffer);

    RenderBuffer->getIndexBuffer().set_used(RenderBuffer->getIndexBuffer().allocated_size());

    // for debug purposes only:
    if (DebugDataVisible)
    {
        video::SMaterial m;
        m.Lighting = false;
        driver->setMaterial(m);
        if (DebugDataVisible & scene::EDS_BBOX)
        {
            driver->draw3DBox(TerrainData.BoundingBox, video::SColor(255, 255, 255, 255));
        }

        s32 visible = 0;
        if (DebugDataVisible & scene::EDS_BBOX_BUFFERS)
        {
            for (irr::u32 patchX = 0; patchX < TerrainData.PatchCountX; patchX++)
            {
                for (irr::u32 patchZ = 0; patchZ < TerrainData.PatchCountZ; patchZ++)
                {
                    driver->draw3DBox(TerrainData.Patches[patchX][patchZ].BoundingBox,
                                      video::SColor(255, 255, 0, 0));
                    visible += (TerrainData.Patches[patchX][patchZ].CurrentLOD >= 0);
                }
            }
        }

        if (DebugDataVisible & scene::EDS_NORMALS)
        {
            // draw normals
            const f32 debugNormalLength = 0.1f * TerrainData.Scale.getLength(); // SceneManager->getParameters()->getAttributeAsFloat(DEBUG_NORMAL_LENGTH);
            const video::SColor debugNormalColor =
                SceneManager->getParameters()->getAttributeAsColor(DEBUG_NORMAL_COLOR);
            driver->drawMeshBufferNormals(RenderBuffer, debugNormalLength, debugNormalColor);
        }

        driver->setTransform(video::ETS_WORLD, AbsoluteTransformation);

        // static u32 lastTime = 0;
        // ITimer* timer = new ITimer();
        // const u32 now = timer->getRealTime();
        /*if (now - lastTime > 1000)
        {
                char buf[64];
                snprintf(buf, 64, "Count: %d, Visible: %d", count, visible);
                debugOut(buf);

                lastTime = now;
        }*/
    }
}


//! Return the bounding box of the entire terrain.

const core::aabbox3d<f32>& TerrainSceneNode::getBoundingBox() const
{
    return TerrainData.BoundingBox;
}


//! Return the bounding box of a patch

const core::aabbox3d<f32>& TerrainSceneNode::getBoundingBox(s32 patchX, s32 patchZ) const
{
    return TerrainData.Patches[patchX][patchZ].BoundingBox;
}

const core::aabbox3d<f32>& TerrainSceneNode::getBoundingBox(s32 patchIndex) const
{
    // patchIndex should invert what e.g. 'getCurrentLODOfPatches()' puts into the 'LODs' array
    // line from 'getCurrentLODOfPatches()'
    // LODs[x * TerrainData.PatchCountX + z] = TerrainData.Patches[x][z].CurrentLOD;

    const s32 patchZ = patchIndex % static_cast<s32>(TerrainData.PatchCountX);
    // substract out the z-value and get the x index by division
    const s32 patchX = (patchIndex - patchZ) / static_cast<s32>(TerrainData.PatchCountZ);
    return TerrainData.Patches[patchX][patchZ].BoundingBox;
}

//! Gets the meshbuffer data based on a specified Level of Detail.
//! \param mb: A reference to an SMeshBuffer object
//! \param LOD: The Level Of Detail you want the indices from.

void TerrainSceneNode::getMeshBufferForLOD(IDynamicMeshBuffer&, s32) const
{
    Error::errTerminate("getMeshBufferForLOD not implemented");
}


//! Gets the indices for a specified patch at a specified Level of Detail.
//! \param mb: A reference to an array of u32 indices.
//! \param patchX: Patch x coordinate.
//! \param patchZ: Patch z coordinate.
//! \param LOD: The level of detail to get for that patch. If -1, then get
//! the CurrentLOD. If the CurrentLOD is set to -1, meaning it's not shown,
//! then it will retrieve the triangles at the highest LOD (0).
//! \return: Number if indices put into the buffer.

s32 TerrainSceneNode::getIndicesForPatch(core::array<u32>& indices, s32 patchX, s32 patchZ, s32 LOD)
{
    if (patchX < 0 || static_cast<irr::u32>(patchX) + 1 > TerrainData.PatchCountX || patchZ < 0 ||
        static_cast<irr::u32>(patchZ) + 1 > TerrainData.PatchCountZ)
    {
        return -1;
    }

    if (LOD < -1 || LOD > TerrainData.MaxLOD - 1)
    {
        return -1;
    }

    // the patch
    const SPatch patch = TerrainData.Patches[patchX][patchZ];

    core::array<s32> cLODs;
    bool setLODs = false;

    // If LOD of -1 was passed in, use the CurrentLOD of the patch specified
    if (LOD == -1)
    {
        LOD = patch.CurrentLOD;
    }
    else
    {
        getCurrentLODOfPatches(cLODs);
        setCurrentLODOfPatches(LOD);
        setLODs = true;
    }

    if (LOD < 0)
    {
        return -2;
    } // Patch not visible, don't generate indices.

    const irr::u32 indexCount = this->TerrainData.Patches[patchX][patchZ].indices.size();

    indices = this->TerrainData.Patches[patchX][patchZ].indices;

    if (setLODs)
    {
        setCurrentLODOfPatches(cLODs);
    }

    return indexCount;
}


//! Populates an array with the CurrentLOD of each patch.
//! \param LODs: A reference to a core::array<s32> to hold the values
//! \return Returns the number of elements in the array

s32 TerrainSceneNode::getCurrentLODOfPatches(core::array<s32>& LODs) const
{
    LODs.clear();

    // the LODs array expects the patch lods flat with
    // index = patchX * TerrainData.PatchCountX + patchZ
    LODs.set_used(TerrainData.PatchCountX * TerrainData.PatchCountZ);
    for (irr::u32 x = 0; x < TerrainData.PatchCountX; x++)
    {
        for (irr::u32 z = 0; z < TerrainData.PatchCountZ; z++)
        {
            LODs[x * TerrainData.PatchCountX + z] = TerrainData.Patches[x][z].CurrentLOD;
        }
    }
    return LODs.size();
}


//! Manually sets the LOD of a patch
//! \param patchX: Patch x coordinate.
//! \param patchZ: Patch z coordinate.
//! \param LOD: The level of detail to set the patch to.

void TerrainSceneNode::setLODOfPatch(s32 patchX, s32 patchZ, s32 LOD)
{
    TerrainData.Patches[patchX][patchZ].CurrentLOD = LOD;
}


//! Override the default generation of distance thresholds for determining the LOD a patch
//! is rendered at.

bool TerrainSceneNode::overrideLODDistance(s32 LOD, f64 newDistance)
{
    OverrideDistanceThreshold = true;

    if (LOD < 0 || LOD > TerrainData.MaxLOD - 1)
    {
        return false;
    }

    TerrainData.LODDistanceThreshold[LOD] = newDistance * newDistance;

    return true;
}


//! Creates a planar texture mapping on the terrain
//! \param resolution: resolution of the planar mapping. This is the value
//! specifying the relation between world space and texture coordinate space.

void TerrainSceneNode::scaleTexture(f32, f32)
{
    Error::errTerminate("scale Texture not implemented!");
}

//! create patches, stuff that needs to be done only once for patches goes here.
std::vector<std::vector<TerrainSceneNode::SPatch>>
TerrainSceneNode::createPatches(irr::u32 patchCountX, irr::u32 patchCountZ) const
{
    if (patchCountX == 0)
    {
        patchCountX = 1;
    }
    if (patchCountZ == 0)
    {
        patchCountZ = 1;
    }
    std::vector<std::vector<SPatch>> patches(patchCountX);
    for (unsigned int i = 0; i < patchCountZ; i++)
    {
        patches[i].resize(patchCountZ);
    }
    return patches;
}


std::vector<std::vector<tileMap::Levels_t>>
TerrainSceneNode::createLevels(irr::u32 levelCountX, irr::u32 levelCountZ) const
{
    if (levelCountX == 0)
    {
        levelCountX = 1;
    }
    if (levelCountZ == 0)
    {
        levelCountZ = 1;
    }
    std::vector<std::vector<tileMap::Levels_t>> levels(levelCountX);
    for (unsigned int i = 0; i < levelCountZ; i++)
    {
        levels[i].resize(levelCountZ);
    }
    return levels;
}


//! used to calculate the internal STerrainData structure both at creation and after
//! scaling/position calls.

void TerrainSceneNode::calculatePatchData()
{
    // Reset the Terrains Bounding Box for re-calculation
    TerrainData.BoundingBox.reset(RenderBuffer->getPosition(0));

    for (u32 x = 0; x < TerrainData.PatchCountX; ++x)
    {
        for (u32 z = 0; z < TerrainData.PatchCountZ; ++z)
        {
            SPatch& patch = TerrainData.Patches[x][z];
            patch.CurrentLOD = 0;
            patch.BoundingBox.reset(RenderBuffer->getPosition(this->TerrainData.Patches[x][z].indices[0]));

            // the BBox has been reset to the first index in the array -> continue on with the
            // second index i = 1
            for (irr::u32 i = 1; i < TerrainData.Patches[x][z].indices.size(); i++)
            {
                patch.BoundingBox.addInternalPoint(
                    RenderBuffer->getVertexBuffer()[this->TerrainData.Patches[x][z].indices[i]].Pos);
            }

            // Reconfigure the bounding box of the terrain as a whole
            TerrainData.BoundingBox.addInternalBox(patch.BoundingBox);

            // get center of Patch
            patch.Center = patch.BoundingBox.getCenter();
        }
    }

    // get center of Terrain
    TerrainData.Center = TerrainData.BoundingBox.getCenter();

    // if the default rotation pivot is still being used, update it.
    if (UseDefaultRotationPivot)
    {
        TerrainData.RotationPivot = TerrainData.Center;
    }
}


//! used to calculate or recalculate the distance thresholds

void TerrainSceneNode::calculateDistanceThresholds(bool /*scalechanged*/) // scalechanged also
                                                                          // unused in original
                                                                          // irrlicht source file
{
    // Only update the LODDistanceThreshold if it's not manually changed
    if (!OverrideDistanceThreshold)
    {
        TerrainData.LODDistanceThreshold.set_used(0);
        // Determine new distance threshold for determining what LOD to draw patches at
        TerrainData.LODDistanceThreshold.reallocate(TerrainData.MaxLOD);
        debugOutLevel(Debug::DebugLevels::stateInit + 2,
                      "calculating distance thresholds with MaxLOD=",
                      TerrainData.MaxLOD);

        // cast to f64 first before multiplying to prevent overflows
        const f64 size = static_cast<irr::f64>(TerrainData.PatchSizeX) *
            static_cast<irr::f64>(TerrainData.PatchSizeZ) *
            static_cast<irr::f64>(TerrainData.Scale.X) * static_cast<irr::f64>(TerrainData.Scale.Z);
        for (s32 i = 0; i < TerrainData.MaxLOD; ++i)
        {
            TerrainData.LODDistanceThreshold.push_back(size * ((i + 1 + i / 2) * (i + 1 + i / 2)));
        }
    }
}

void TerrainSceneNode::setCurrentLODOfPatches(s32 lod)
{
    for (unsigned int x = 0; x < TerrainData.PatchCountX; x++)
    {
        for (unsigned int z = 0; z < TerrainData.PatchCountZ; z++)
        {
            TerrainData.Patches[x][z].CurrentLOD = lod;
        }
    }
}

void TerrainSceneNode::setCurrentLODOfPatches(const core::array<s32>& lodarray)
{
    // the LODs array expects the patch lods flat with
    // index = patchX * TerrainData.PatchCountX + patchZ
    for (irr::u32 x = 0; x < TerrainData.PatchCountX; x++)
    {
        for (irr::u32 z = 0; z < TerrainData.PatchCountZ; z++)
        {
            const u32 index = x * TerrainData.PatchCountX + z;
            TerrainData.Patches[x][z].CurrentLOD = lodarray[index];
        }
    }
}

irr::core::vector3df TerrainSceneNode::getNormalVector(f32 x, f32 z) const
{
    if (!Mesh->getMeshBufferCount())
    {
        return irr::core::vector3df(-FLT_MAX);
    }

    core::matrix4 rotMatrix;
    rotMatrix.setRotationDegrees(TerrainData.Rotation);
    core::vector3df pos(x, 0.0f, z);
    rotMatrix.rotateVect(pos);
    pos -= TerrainData.Position;
    // divide does component-wise: x / y = (x1/y1, x2/y2, x3/y3)^T
    pos /= TerrainData.Scale;

    const s32 levelX(core::floor32(pos.X));
    const s32 levelZ(core::floor32(pos.Z));

    irr::core::vector3df normal(-FLT_MAX);
    if (levelX >= 0 && levelX < static_cast<irr::s32>(TerrainData.tileCountX) && levelZ >= 0 &&
        levelZ < static_cast<irr::s32>(TerrainData.tileCountZ))
    {
        // position inside level
        const f32 dx = pos.X - static_cast<irr::f32>(levelX);
        const f32 dz = pos.Z - static_cast<irr::f32>(levelZ);

        normal = this->levelMap[static_cast<size_t>(levelX)][static_cast<size_t>(levelZ)].getNormalVector(dx, dz);
    }

    return normal;
}
//! Gets the height

f32 TerrainSceneNode::getHeight(f32 x, f32 z) const
{
    if (!Mesh->getMeshBufferCount())
    {
        return 0;
    }

    core::matrix4 rotMatrix;
    rotMatrix.setRotationDegrees(TerrainData.Rotation);
    core::vector3df pos(x, 0.0f, z);
    rotMatrix.rotateVect(pos);
    pos -= TerrainData.Position;
    // divide does component-wise: x / y = (x1/y1, x2/y2, x3/y3)^T
    pos /= TerrainData.Scale;

    const s32 levelX(core::floor32(pos.X));
    const s32 levelZ(core::floor32(pos.Z));

    f32 height = -FLT_MAX;
    if (levelX >= 0 && levelX < static_cast<irr::s32>(TerrainData.tileCountX) && levelZ >= 0 &&
        levelZ < static_cast<irr::s32>(TerrainData.tileCountZ))
    {
        // position inside level
        const f32 dx = pos.X - static_cast<irr::f32>(levelX);
        const f32 dz = pos.Z - static_cast<irr::f32>(levelZ);

        height = this->levelMap[static_cast<size_t>(levelX)][static_cast<size_t>(levelZ)].getHeight(dx, dz);

        height *= TerrainData.Scale.Y;
        height += TerrainData.Position.Y;
    }

    return height;
}

f32 TerrainSceneNode::getSmoothedHeight(f32 x, f32 z) const
{
    if (!Mesh->getMeshBufferCount())
    {
        return 0;
    }

    core::matrix4 rotMatrix;
    rotMatrix.setRotationDegrees(TerrainData.Rotation);
    core::vector3df pos(x, 0.0f, z);
    rotMatrix.rotateVect(pos);
    pos -= TerrainData.Position;
    // divide does component-wise: x / y = (x1/y1, x2/y2, x3/y3)^T
    pos /= TerrainData.Scale;

    const s32 levelX(core::floor32(pos.X));
    const s32 levelZ(core::floor32(pos.Z));

    f32 height = -FLT_MAX;
    if (levelX >= 0 && levelX < static_cast<irr::s32>(TerrainData.tileCountX) && levelZ >= 0 &&
        levelZ < static_cast<irr::s32>(TerrainData.tileCountZ))
    {
        // position inside level
        const f32 dx = pos.X - static_cast<irr::f32>(levelX);
        const f32 dz = pos.Z - static_cast<irr::f32>(levelZ);

        height = this->levelMap[static_cast<size_t>(levelX)][static_cast<size_t>(levelZ)].getSmoothedHeight(dx, dz);

        height *= TerrainData.Scale.Y;
        height += TerrainData.Position.Y;
    }

    return height;
}


//! Writes attributes of the scene node.

void TerrainSceneNode::serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const
{
    Error::errTerminate("not implemented!");
    ISceneNode::serializeAttributes(out, options);

    out->addFloat("TextureScale1", TCoordScale1);
    out->addFloat("TextureScale2", TCoordScale2);
    out->addInt("SmoothFactor", SmoothFactor);
}


//! Reads attributes of the scene node.

void TerrainSceneNode::deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options)
{
    io::path newHeightmap = in->getAttributeAsString("Heightmap");
    f32 tcoordScale1 = in->getAttributeAsFloat("TextureScale1");
    f32 tcoordScale2 = in->getAttributeAsFloat("TextureScale2");
    s32 smoothFactor = in->getAttributeAsInt("SmoothFactor");

    // set possible new heightmap

    if (newHeightmap.size() != 0) //  && newHeightmap != HeightmapFile)
    {
        io::IReadFile* file = FileSystem->createAndOpenFile(newHeightmap.c_str());
        if (file)
        {
            loadHeightMap(file, video::SColor(255, 255, 255, 255), smoothFactor);
            file->drop();
        }
        else
        {
            Error::errTerminate("could not open heightmap");
        } //, newHeightmap.c_str());
    }

    // set possible new scale

    if (core::equals(tcoordScale1, 0.f))
    {
        tcoordScale1 = 1.0f;
    }

    if (core::equals(tcoordScale2, 0.f))
    {
        tcoordScale2 = 1.0f;
    }

    if (!core::equals(tcoordScale1, TCoordScale1) || !core::equals(tcoordScale2, TCoordScale2))
    {
        scaleTexture(tcoordScale1, tcoordScale2);
    }

    ISceneNode::deserializeAttributes(in, options);
}


//! Creates a clone of this scene node and its children.

ISceneNode* TerrainSceneNode::clone(ISceneNode* newParent, ISceneManager* newManager)
{
    // TODO: implement correctly
    Error::errTerminate("not corrected for new system");
    if (!newParent)
    {
        newParent = Parent;
    }
    if (!newManager)
    {
        newManager = SceneManager;
    }

    TerrainSceneNode* nb = new TerrainSceneNode(
        newParent, newManager, FileSystem, ID, 4, ETPS_17, getPosition(), getRotation(), getScale());

    nb->cloneMembers(this, newManager);

    // instead of cloning the data structures, recreate the terrain.
    // (temporary solution)

    // load file

    io::IReadFile* file = nullptr; // FileSystem->createAndOpenFile(HeightmapFile.c_str());
    if (file)
    {
        nb->loadHeightMap(file, video::SColor(255, 255, 255, 255), 0);
        file->drop();
    }

    // scale textures

    nb->scaleTexture(TCoordScale1, TCoordScale2);

    // copy materials

    for (unsigned int m = 0; m < Mesh->getMeshBufferCount(); ++m)
    {
        if (nb->Mesh->getMeshBufferCount() > m && nb->Mesh->getMeshBuffer(m) && Mesh->getMeshBuffer(m))
        {
            nb->Mesh->getMeshBuffer(m)->getMaterial() = Mesh->getMeshBuffer(m)->getMaterial();
        }
    }

    nb->RenderBuffer->getMaterial() = RenderBuffer->getMaterial();

    // finish

    if (newParent)
    {
        nb->drop();
    }
    return nb;
}


void TerrainSceneNode::clearLevelMap()
{
    if (this->levelMap.size() == 0)
    {
        return;
    }
    const size_t maxX = this->levelMap[0].size();
    for (size_t x = 0; x < maxX; x++)
    {
        this->levelMap[x].clear();
    }
    this->levelMap.clear();
}
