/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <fstream> // save to binary format
#include <stack>   // needed in floodFill()

#include "../utils/stringwstream.h" // stringwstream.h needs to be included before debug.h and error.h (which are included by mapGenerator.h) because they use use operator<< on a stringw

#include "mapGenerator.h"

#include <irrlicht/CDynamicMeshBuffer.h>
#include <irrlicht/IFileSystem.h> // IrrlichtDevice and IFileSystem needed to create an xml writer
#include <irrlicht/IImage.h>
#include <irrlicht/IMeshManipulator.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/IVideoDriver.h>
#include <irrlicht/IXMLWriter.h>
#include <irrlicht/IrrlichtDevice.h> // IrrlichtDevice and IFileSystem needed to create an xml writer
#include <irrlicht/SMesh.h>

#include "../utils/stringwstream.h" // stringwstream.h needs to be included before debug.h and error.h because they use operator<< on a stringw
#include "mapSystemVersion.h"
#include "tilechooser.h"
#include "../utils/binary.h"
#include "../utils/debug.h"
#include "../utils/printfunctions.h"
#include "../utils/static/error.h"
#include "../utils/stringwparser.h"

// this line fixes a clang linking error. For its reasons and discussion see here:
// https://stackoverflow.com/questions/28264279/undefined-reference-when-accessing-static-constexpr-float-member
// and here:
// https://stackoverflow.com/questions/26196095/static-constexpr-odr-used-or-not
constexpr float PathGrid::unitSize;


using namespace tileMap;

MapGenerator::MapGenerator(const irr::video::IImage* const cliffMap_,
                           const irr::video::IImage* const rampMap_,
                           const irr::video::IImage* const offsetMap_,
                           const irr::io::path& tileDefinitionsFile,
                           irr::scene::ISceneManager* sceneManager_)
    : cliffMap(cliffMap_)
    , rampMap(rampMap_)
    , offsetMap(offsetMap_)
    , sceneManager(sceneManager_)
    , tileChooser()
    , tileArr()
{
    if (this->sceneManager == nullptr)
    {
        Error::errTerminate("MapGenerator called with sceneManager == nullprt");
    }

    if (this->cliffMap == nullptr)
    {
        Error::errTerminate("MapGenerator called with cliffMap == nullprt");
    }
    if (this->cliffMap->getDimension().Width < 2 || cliffMap->getDimension().Height < 2)
    {
        Error::errTerminate("cliffMap: at least one dimension smaller than 2 pixels");
    }
    cliffMap->grab();

    if (this->rampMap != nullptr)
    {
        rampMap->grab();
        if (this->cliffMap->getDimension() != this->rampMap->getDimension())
        {
            Error::errTerminate("this->cliffMap and this->rampMap have different sizes");
        }
    }

    if (this->offsetMap != nullptr)
    {
        this->offsetMap->grab();
        if (this->cliffMap->getDimension() != this->offsetMap->getDimension())
        {
            Error::errTerminate("this->cliffMap and offsetMap have different sizes");
        }
    }

    if (this->tileChooser.loadTileXML(tileDefinitionsFile, sceneManager) != ErrCode(ErrCodes::NO_ERR))
    {
        Error::errTerminate("couldn't open file", repr<irr::io::path>(tileDefinitionsFile));
    }
}

MapGenerator::~MapGenerator()
{
    this->cliffMap->drop();
    if (this->rampMap != nullptr)
    {
        this->rampMap->drop();
    }
    if (this->offsetMap != nullptr)
    {
        this->offsetMap->drop();
    }
}

ErrCode MapGenerator::generateAndSavePatches(const irr::io::path& filename,
                                             const unsigned int patchSizeX,
                                             const unsigned int patchSizeZ,
                                             irr::IrrlichtDevice* device)
{
    this->createTileMap();

    bool binaryFormat = true;
    if (filename.equals_substring_ignore_case(
            ".xml", static_cast<irr::s32>(filename.size() - irr::io::path(".xml").size())))
    {
        binaryFormat = false;
    }

    return this->savePatchesData(patchSizeX, patchSizeZ, filename, device, binaryFormat);
}

ErrCode MapGenerator::saveFullMapTexture(const irr::core::stringw filename)
{
    // create an image for the full map
    const irr::core::dimension2du fullMapSize(static_cast<irr::u32>(this->tileArr.size() * Tile::textureWidth),
                                              static_cast<irr::u32>(this->tileArr[0].size() * Tile::textureHeight));
    debugOutLevel(Debug::DebugLevels::stateInit,
                  "reserving space for a",
                  fullMapSize.Width,
                  "x",
                  fullMapSize.Height,
                  "texture");
    irr::video::IImage* fullMapTexture =
        this->sceneManager->getVideoDriver()->createImage(irr::video::ECF_A1R5G5B5, fullMapSize);
    if (fullMapTexture == nullptr)
    {
        Error::errContinue("couldn't allocate a", fullMapSize.Width, "x", fullMapSize.Height, "pixel texture for the total combined map!");
        return ErrCodes::GENERIC_ERR;
    }

    for (size_t tileX = 0; tileX < this->tileArr.size(); tileX++)
    {
        for (size_t tileZ = 0; tileZ < this->tileArr[0].size(); tileZ++)
        {
            // convert irrlicht coordinates to image coordinates so that the tile will be drawn on
            // the image where it will display in game
            const irr::s32 pixelCoordinateX = static_cast<irr::s32>(this->tileArr.size() * Tile::textureWidth) -
                static_cast<irr::s32>((tileX + 1) * Tile::textureWidth);
            const irr::s32 pixelCoordinateY = static_cast<irr::s32>(tileZ * Tile::textureHeight);
            debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                          "copying image from tile",
                          tileX,
                          tileZ,
                          "to position",
                          pixelCoordinateX,
                          pixelCoordinateY);
            if (this->tileArr[tileX][tileZ].getImage() == nullptr)
            {
                Error::errContinue("getImage() returned nullptr for tile at tileX=",
                                   tileX,
                                   "tileZ=",
                                   tileZ,
                                   "type=",
                                   this->tileArr[tileX][tileZ].getType());
                return ErrCodes::GENERIC_ERR;
            }
            this->tileArr[tileX][tileZ].getImage()->copyTo(
                fullMapTexture, irr::core::position2d<irr::s32>(pixelCoordinateX, pixelCoordinateY));
        }
    }
    debugOutLevel(Debug::DebugLevels::stateInit, "saving image to file", filename);
    if (not this->sceneManager->getVideoDriver()->writeImageToFile(fullMapTexture, filename))
    {
        Error::errContinue("couldn't save the full map texture to file", filename);
        fullMapTexture->drop();
        return ErrCodes::GENERIC_ERR;
    }
    fullMapTexture->drop();
    return ErrCodes::NO_ERR;
}

/* ########################################################################
 * private functions
 * ######################################################################## */

std::vector<irr::u32> MapGenerator::getUniquePixelRedValues(const irr::video::IImage* const image) const
{
    std::vector<irr::u32> seenPixelValues;
    if (image == nullptr)
    {
        seenPixelValues.push_back(0);
        return seenPixelValues;
    }

    for (irr::u32 pixelX = 0; pixelX < image->getDimension().Width; pixelX++)
    {
        for (irr::u32 pixelY = 0; pixelY < image->getDimension().Height; pixelY++)
        {
            irr::u32 pixelValue = image->getPixel(pixelX, pixelY).getRed();
            bool alreadySeen = false;
            for (size_t i = 0; i < seenPixelValues.size(); i++)
            {
                if (pixelValue == seenPixelValues[i])
                {
                    alreadySeen = true;
                    break;
                }
            }
            if (not alreadySeen)
            {
                seenPixelValues.push_back(pixelValue);
            }
        }
    }

    std::sort(seenPixelValues.begin(), seenPixelValues.end());
    return seenPixelValues;
}

irr::u32 MapGenerator::getLevelForPixelValue(const irr::u32 pixelValue, const std::vector<irr::u32>& seenPixelValues)
{
    if (seenPixelValues.size() == 0)
    {
        Error::errTerminate("seenPixelValues.size() == 0. Generate the correct array using "
                            "getUniquePixelRedValues()");
    }
    size_t levelAbove = 0;
    for (levelAbove = 0; levelAbove < seenPixelValues.size(); levelAbove++)
    {
        if (seenPixelValues[levelAbove] > pixelValue)
        {
            if (levelAbove == 0)
            {
                Error::errTerminate("seenPixelValues[0] > pixelValue which shouldn't have happened "
                                    "with a correctly created seenPixelValues. Use "
                                    "getUniquePixelRedValues() to generate it");
            }
            break;
        }
    }
    // after the loop levelAbove will be at least 1
    // even if seenPixelValues[levelAbove] was never > pixelValue (meaning pixelValue is the highest
    // level) levelAbove will be one too high because it't will have been the abort-condition of the
    // for-loop
    return static_cast<irr::u32>(levelAbove - 1);
}

bool MapGenerator::existsRampUpInDirection(const irr::s32 pixelX, const irr::s32 pixelY, const directions::Directions direction)
{
    // image coordinates:
    // (0,0) -------------> x
    //   |
    //   |   all image coordinates will have 'pixel' in the variable name
    //   |
    // z v
    //
    const irr::s32 maximumPixelX = static_cast<irr::s32>(this->cliffMap->getDimension().Width) - 1;
    const irr::s32 maximumPixelY = static_cast<irr::s32>(this->cliffMap->getDimension().Height) - 1;

    bool rampUpwards = false;
    irr::core::vector2d<irr::s32> currentPixel(pixelX, pixelY);
    // walk along the pixels and test if they are part of a ramp
    for (int rampLength = 0; rampLength < 3; rampLength++)
    {
        debugOutLevel(Debug::DebugLevels::secondOrderLoop, "testing current pixel", vector2dToStringW(currentPixel));
        if (currentPixel.X < 0 or currentPixel.X > maximumPixelX or currentPixel.Y < 0 or
            currentPixel.Y > maximumPixelY)
        {
            // the loop isn't finished (meaning the ramp isn't yet 3 pixels long) and the next pixel
            // in the test-direction left the image
            return false;
        }

        if (this->rampMap
                ->getPixel(static_cast<irr::u32>(currentPixel.X), static_cast<irr::u32>(currentPixel.Y))
                .getRed() < 128)
        {
            debugOutLevel(Debug::DebugLevels::secondOrderLoop + 1,
                          "ramp ended at length",
                          rampLength,
                          "before reaching length 3 at pixel",
                          vector2dToStringW(currentPixel));
            return false;
        }

        if (this->cliffMap
                ->getPixel(static_cast<irr::u32>(currentPixel.X), static_cast<irr::u32>(currentPixel.Y))
                .getRed() >
            this->cliffMap->getPixel(static_cast<irr::u32>(pixelX), static_cast<irr::u32>(pixelY)).getRed())
        {
            // only a ramp if the step up happens on the last pixel
            if (rampLength < 2)
            {
                return false;
            }
            else if (rampLength == 2)
            {
                rampUpwards = true;
            }
            debugOutLevel(Debug::DebugLevels::secondOrderLoop + 1,
                          "found upwards height change between value",
                          this->cliffMap
                              ->getPixel(static_cast<irr::u32>(pixelX), static_cast<irr::u32>(pixelY))
                              .getRed(),
                          "and current pixel value",
                          this->cliffMap
                              ->getPixel(static_cast<irr::u32>(currentPixel.X),
                                         static_cast<irr::u32>(currentPixel.Y))
                              .getRed());
        }
        currentPixel = nextPixelInDirection(static_cast<irr::u32>(currentPixel.X),
                                            static_cast<irr::u32>(currentPixel.Y),
                                            direction);
    }

    if (rampUpwards)
    {
        return true;
    }
    else
    {
        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                      "ramp detection done: no upwards height change detected -> no ramp");
        return false;
    }
}

void MapGenerator::generateRampData(std::vector<std::vector<float>>& pixelLevelMap,
                                    std::vector<std::vector<std::vector<directions::Directions>>>& pixelRampDirection,
                                    const std::vector<irr::u32> seenPixelValues)
{
    const irr::s32 maximumPixelX = static_cast<irr::s32>(this->cliffMap->getDimension().Width) - 1;
    const irr::s32 maximumPixelY = static_cast<irr::s32>(this->cliffMap->getDimension().Height) - 1;
    // the need for diagonal ramps poses a bit of a problem. The simple aproach to just search in
    // all directions and use whatever ramp is found
    // implicitly prioritises the ramp searched for last. Any simple approach fails with either
    // ramps sticking out or ramps in 'corners'
    // Instead this algorithm first searches 1) left <-> right and 2) top <-> bottom
    // if at 2) a pixel is found which already was used for a ramp at 1) then this pixel might be
    // part of a diagonal ramp
    // the 3)-rd pass then searches for diagonal ramps and accepts all those pixels which got a ramp
    // from 1) AND 2) (meaning a diagonal ramp fits) and
    // ALSO searches for ramp-pixels not used in any ramp (this finds ramps going outwards from a
    // corner)
    const std::vector<directions::Directions> directionsToTest({directions::Directions::Right,
                                                                directions::Directions::Left,
                                                                directions::Directions::Top,
                                                                directions::Directions::Bottom,
                                                                directions::Directions::TopRight,
                                                                directions::Directions::BottomLeft,
                                                                directions::Directions::BottomRight,
                                                                directions::Directions::TopLeft});
    // scanning the straight directions first because otherwise any ramp which is 3 or more across
    // (orthogonal to its direction of height change)
    // will be build by lots of diagonal pieces.
    for (directions::Directions direction : directionsToTest)
    {
        for (irr::s32 pixelY = 0; pixelY <= maximumPixelY; pixelY++)
        {
            for (irr::s32 pixelX = 0; pixelX <= maximumPixelX; pixelX++)
            {
                debugOutLevel(Debug::DebugLevels::secondOrderLoop, "pixel=", pixelX, pixelY, ": searching for ramps");
                // each pixelRampDirection has at least the default entry Internal -> the [0]-access
                // is always save
                if (this->rampMap->getPixel(pixelX, pixelY).getRed() < 128) // not a ramp pixel
                {
                    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 1,
                                  "pixel=",
                                  pixelX,
                                  pixelY,
                                  " no ramp because this->rampMap[x][y].red < 128:",
                                  this->rampMap->getPixel(pixelX, pixelY).getRed());
                    continue;
                }
                else if (pixelRampDirection[pixelX][pixelY][0] != directions::Directions::Internal)
                {
                    for (directions::Directions alreadyRampInDirection : pixelRampDirection[pixelX][pixelY])
                    {
                        if (alreadyRampInDirection == directions::invertDirection(direction))
                        {
                            debugOutLevel(Debug::DebugLevels::secondOrderLoop + 1, "pixel=", pixelX, pixelY, "is a ramp in the opposite direction");
                            continue;
                        }
                    }
                }
                // scan for an upwards facing ramp
                debugOutLevel(Debug::DebugLevels::secondOrderLoop + 3,
                              "pixel=",
                              pixelX,
                              pixelY,
                              "searching for ramp in direction",
                              directions::directionToString(direction));
                if (existsRampUpInDirection(pixelX, pixelY, direction))
                {
                    debugOutLevel(Debug::DebugLevels::secondOrderLoop + 3,
                                  "pixel=",
                                  pixelX,
                                  pixelY,
                                  "ramp in direction",
                                  directions::directionToString(direction));
                    // The ramp will only be build from the 2nd pixel along its 3 pixel range
                    irr::core::vector2d<irr::s32> secondPixel = nextPixelInDirection(pixelX, pixelY, direction);
                    if (pixelRampDirection[secondPixel.X][secondPixel.Y][0] == directions::Directions::Internal)
                    {
                        // set the level of the 2nd pixel between the one of the other two (this is
                        // easy because the ramp is guaranteed to go upwards in 'direction')
                        debugOutLevel(Debug::DebugLevels::secondOrderLoop + 4,
                                      "setting pixelRampDirection of pixel",
                                      vector2dToStringW(secondPixel));
                        // get the level data from the orininal pixel because the pixel where the
                        // ramp test started could be a ramp pixel
                        // which would add the ramp-offset of 0.5 twice
                        pixelLevelMap[secondPixel.X][secondPixel.Y] =
                            static_cast<float>(
                                getLevelForPixelValue(this->cliffMap->getPixel(pixelX, pixelY).getRed(),
                                                      seenPixelValues)) +
                            0.5f; // only set the level once!
                        pixelRampDirection[secondPixel.X][secondPixel.Y][0] =
                            direction; // there wasn't a ramp until now -> just overwrite
                    }
                    else
                    {
                        switch (direction)
                        {
                            case directions::Directions::Right:
                            case directions::Directions::Left:
                            case directions::Directions::Top:
                            case directions::Directions::Bottom:
                                // while moving in any of the non-diagonals just add the direction
                                // to the list of found directions
                                pixelRampDirection[secondPixel.X][secondPixel.Y].push_back(direction);
                                break;
                            case directions::Directions::TopLeft:
                            case directions::Directions::TopRight:
                            case directions::Directions::BottomRight:
                            case directions::Directions::BottomLeft:
                                // if there are two directions saved AND the two directions together
                                // form the direction we test our diagonal in -> replace them with
                                // the combination
                                if (pixelRampDirection[secondPixel.X][secondPixel.Y].size() == 2 and
                                    direction ==
                                        directions::combineDirections(
                                            pixelRampDirection[secondPixel.X][secondPixel.Y][0],
                                            pixelRampDirection[secondPixel.X][secondPixel.Y][1]))
                                {
                                    pixelRampDirection[secondPixel.X][secondPixel.Y].clear();
                                    pixelRampDirection[secondPixel.X][secondPixel.Y].push_back(direction);
                                }
                                // otherwise the straight direction takes precedence over the
                                // diagonal one -> ignore it
                                break;
                            case directions::Directions::Internal:
                            case directions::Directions::NumDirections:
                            default:
                                Error::errTerminate("found a ramp in direction",
                                                    directions::directionToString(direction),
                                                    "whish should never have been searched!");
                        } // switch(direction)
                    }     // else pixelRampDirection[2ndpx][2ndpy] == Internal
                }         // existsRampUpInDirection(px, py, rMap, direction)
            }             // for (irr::u32 pixelX = 0; pixelX <= maximumPixelX; pixelX++)
        }                 // for (irr::u32 pixelY = 0; pixelY <= maximumPixelY; pixelY++)
    }                     // for (directions::Directions direction : directionsToTest)
}

void MapGenerator::createTileMap(const irr::f32 offsetFactor, const irr::s32 offsetZero)
{
    debugOutLevel(Debug::DebugLevels::stateInit + 1, "generating tilemap from images");
    // create the tiles via resize
    // the centers between the pixels define a tile
    const irr::u32 tileCountX = this->cliffMap->getDimension().Width - 1;
    const irr::u32 tileCountZ = this->cliffMap->getDimension().Height - 1;
    // one less row/cloumn of tiles than pixels
    this->tileArr.resize(tileCountX);
    for (unsigned int i = 0; i < this->tileArr.size(); i++)
    {
        this->tileArr[i] = std::vector<Tile>(tileCountZ);
    }
    debugOutLevel(Debug::DebugLevels::stateInit + 1, "created tileArray with dimensions x=", tileCountX, "z=", tileCountZ);

    // image coordinates:
    // (0,0) -------------> x
    //   |
    //   |   all image coordinates will have 'pixel' in the variable name
    //   |
    // z v
    //
    // game coordinates:
    // x <----------------(0,0)
    //                      |
    //                      |    all other coordinates are game coordinates
    //                      |
    //                      v z

    const irr::s32 maximumPixelX = static_cast<irr::s32>(this->cliffMap->getDimension().Width) - 1;
    const irr::s32 maximumPixelY = static_cast<irr::s32>(this->cliffMap->getDimension().Height) - 1;

    const std::vector<irr::u32> seenPixelValues = this->getUniquePixelRedValues(this->cliffMap);

    // pixelLevelMap is in image coordinates but it contains levels, not pixelValues
    // float levels allow ramps to be encoded via \pm 0.5 level steps
    std::vector<std::vector<float>> pixelLevelMap;
    pixelLevelMap.resize(this->cliffMap->getDimension().Width);
    for (irr::s32 x = 0; x <= maximumPixelX; x++)
    {
        pixelLevelMap[x].resize(this->cliffMap->getDimension().Height);
        for (irr::s32 y = 0; y <= maximumPixelY; y++)
        {
            pixelLevelMap[x][y] = static_cast<float>(
                getLevelForPixelValue(this->cliffMap->getPixel(x, y).getRed(), seenPixelValues));
        }
    }

    // array in image coordinates that contanis if a ramp has already set the level of a specific
    // pixel or not
    std::vector<std::vector<std::vector<directions::Directions>>> pixelRampDirection;
    pixelRampDirection.resize(this->cliffMap->getDimension().Width);
    for (irr::s32 x = 0; x <= maximumPixelX; x++)
    {
        pixelRampDirection[x].resize(this->cliffMap->getDimension().Height);
        for (irr::s32 y = 0; y <= maximumPixelY; y++)
        {
            pixelRampDirection[x][y].push_back(directions::Directions::Internal);
        }
    }

    if (this->rampMap != nullptr)
    {
        this->generateRampData(pixelLevelMap, pixelRampDirection, seenPixelValues);
    }

    // set this to true to create a map with seperated tiles which will make debugging of
    // e.g. texture related stuff possible on a per-tile basis because no shared vertices will be
    // created.
    constexpr bool createSeperatedMap = false;

    for (irr::u32 tileX = 0; tileX < tileCountX; tileX++)
    {
        for (irr::u32 tileZ = 0; tileZ < tileCountZ; tileZ++)
        {
            debugOutLevel(Debug::DebugLevels::secondOrderLoop, "gathering data to get correct tile at tileX=", tileX, "tileZ=", tileZ);

            Levels_t wantedLevels;
            // gather all pixels for a tile and combine them into one levels_t
            for (directions::Directions direction :
                 std::vector<directions::Directions>({directions::Directions::TopLeft,
                                                      directions::Directions::TopRight,
                                                      directions::Directions::BottomRight,
                                                      directions::Directions::BottomLeft}))
            {
                irr::core::vector2d<irr::s32> pixelCoordinates =
                    getPixelForTile(tileX, tileZ, maximumPixelX, direction);
                // there might be cases when the pixel coordinates don't point into the image ->
                // clamp the values between [0, maximumPixelX/Y]
                pixelCoordinates.X = irr::core::s32_clamp(pixelCoordinates.X, 0, maximumPixelX);
                pixelCoordinates.Y = irr::core::s32_clamp(pixelCoordinates.Y, 0, maximumPixelY);
                // now some coordinates might point to the same pixel but that is okay the map will
                // just be coninued at the same height as before on those places

                debugOutLevel(Debug::DebugLevels::secondOrderLoop, "pixel coordinates:", vector2dToStringW(pixelCoordinates));

                wantedLevels.setLevel(direction, pixelLevelMap[pixelCoordinates.X][pixelCoordinates.Y]);
                for (size_t i = 1; i < pixelRampDirection[pixelCoordinates.X][pixelCoordinates.Y].size(); i++)
                {
                    Error::errContinue("Too much ramp information for pixel",
                                       vector2dToStringW(pixelCoordinates),
                                       "using only the first one:",
                                       directions::directionToString(
                                           pixelRampDirection[pixelCoordinates.X][pixelCoordinates.Y][0]),
                                       "and ignored ramp direction:",
                                       directions::directionToString(
                                           pixelRampDirection[pixelCoordinates.X][pixelCoordinates.Y][i]));
                }
                wantedLevels.setRampDirection(
                    direction, pixelRampDirection[pixelCoordinates.X][pixelCoordinates.Y][0]);

                if (this->offsetMap != nullptr)
                {
                    wantedLevels.setOffset(
                        direction,
                        offsetFactor *
                            static_cast<irr::f32>(
                                static_cast<irr::s32>(
                                    this->offsetMap->getPixel(pixelCoordinates.X, pixelCoordinates.Y)
                                        .getRed()) -
                                offsetZero));
                }
            }
            if (tileChooser.getTileForLevels(wantedLevels, this->tileArr[tileX][tileZ], this->sceneManager) !=
                ErrCodes::NO_ERR)
            {
                Error::errContinue("tileChooser didn't return a correct tile for position",
                                   tileX,
                                   tileZ,
                                   ". The map may contain visual artifacts!");
                debugOutLevel(
                    Debug::DebugLevels::stateInit,
                    "more information: pixelCoordinates: TopLeft:",
                    vector2dToStringW(getPixelForTile(tileX, tileZ, maximumPixelX, directions::Directions::TopLeft)),
                    "TopRight:",
                    vector2dToStringW(getPixelForTile(tileX, tileZ, maximumPixelX, directions::Directions::TopRight)),
                    "BottomLeft:",
                    vector2dToStringW(getPixelForTile(tileX, tileZ, maximumPixelX, directions::Directions::BottomLeft)),
                    "BottomRight:",
                    vector2dToStringW(getPixelForTile(tileX, tileZ, maximumPixelX, directions::Directions::BottomRight)));
            }
            if (createSeperatedMap)
            {
                this->tileArr[tileX][tileZ].setTileOrigin(irr::core::vector3df(
                    1.1f * static_cast<irr::f32>(tileX), 0, 1.1f * static_cast<irr::f32>(tileZ))); // the absolute ycoordinate is already set by the levels.
            }
            else
            {
                this->tileArr[tileX][tileZ].setTileOrigin(
                    irr::core::vector3df(static_cast<irr::f32>(tileX), 0, static_cast<irr::f32>(tileZ))); // the absolute ycoordinate is already set by the levels.
            }
            // move texture coordinates of each tile to point to the correct coordinates in a single
            // combined texture for all tiles and scale them
            // scaling is done first, moving in the second step. Otherwise the scaling would
            // overwrite the move and texture coordinates would be wrong.
            // the width w of a tile is w = 1 / tileCount
            const irr::f32 textureScaleX = static_cast<irr::f32>(1.0 / static_cast<double>(tileCountX));
            const irr::f32 textureScaleY = static_cast<irr::f32>(1.0 / static_cast<double>(tileCountZ));
            this->tileArr[tileX][tileZ].scaleTextureCoordinates(irr::core::vector2df(textureScaleX, textureScaleY));
            // now the texture coordinates are in the range [0, wx]
            // move the texture coordinates to the correct positions inside the combined texture
            // also converting between irrlicht coordinates and image coordinates in the process
            const irr::f32 textureOriginX =
                static_cast<irr::f32>(1.0 - textureScaleX * (static_cast<double>(tileX) + 1.0));
            const irr::f32 textureOriginY = static_cast<irr::f32>(textureScaleY * static_cast<double>(tileZ));
            this->tileArr[tileX][tileZ].moveTextureCoordinates(irr::core::vector2df(textureOriginX, textureOriginY));
        }
    }

    debugOutLevel(Debug::DebugLevels::stateInit + 1, "generating connected tile mesh");
    const irr::u32 maxTileX = tileCountX - 1;
    const irr::u32 maxTileZ = tileCountZ - 1;
    // now connect all the tiles
    for (irr::u32 tileX = 0; tileX <= maxTileX; tileX++)
    {
        for (irr::u32 tileZ = 0; tileZ <= maxTileZ; tileZ++)
        {
            if (createSeperatedMap)
            {
                break; // don't link up the tiles on a seperated map
            }
            Tile& currentTile = this->tileArr[tileX][tileZ];
            // topleft tile exists?
            if (tileX < maxTileX and tileZ > 0)
            {
                currentTile.neighbors[static_cast<size_t>(directions::Directions::TopLeft)] =
                    &this->tileArr[tileX + 1][tileZ - 1];
            }
            // top tile?
            if (tileZ > 0)
            {
                currentTile.neighbors[static_cast<size_t>(directions::Directions::Top)] =
                    &this->tileArr[tileX][tileZ - 1];
            }
            // topright tile?
            if (tileX > 0 and tileZ > 0)
            {
                currentTile.neighbors[static_cast<size_t>(directions::Directions::TopRight)] =
                    &this->tileArr[tileX - 1][tileZ - 1];
            }
            // right tile?
            if (tileX > 0)
            {
                currentTile.neighbors[static_cast<size_t>(directions::Directions::Right)] =
                    &this->tileArr[tileX - 1][tileZ];
            }
            // bottom right tile?
            if (tileX > 0 and tileZ < maxTileZ)
            {
                currentTile.neighbors[static_cast<size_t>(directions::Directions::BottomRight)] =
                    &this->tileArr[tileX - 1][tileZ + 1];
            }
            // bottom tile?
            if (tileZ < maxTileZ)
            {
                currentTile.neighbors[static_cast<size_t>(directions::Directions::Bottom)] =
                    &this->tileArr[tileX][tileZ + 1];
            }
            // bottom left tile?
            if (tileX < maxTileX and tileZ < maxTileZ)
            {
                currentTile.neighbors[static_cast<size_t>(directions::Directions::BottomLeft)] =
                    &this->tileArr[tileX + 1][tileZ + 1];
            }
            // left tile?
            if (tileX < maxTileX)
            {
                currentTile.neighbors[static_cast<size_t>(directions::Directions::Left)] =
                    &this->tileArr[tileX + 1][tileZ];
            }
        }
    }
}

std::vector<irr::video::S3DVertex2TCoords> MapGenerator::createVertices()
{
    std::vector<irr::video::S3DVertex2TCoords> vertices;
    irr::u32 numVertices = 0;
    for (size_t x = 0; x < this->tileArr.size(); x++)
    {
        for (size_t z = 0; z < this->tileArr[0].size(); z++)
        {
            debugOutLevel(Debug::DebugLevels::secondOrderLoop, "appending vertices from tile at x=", x, "z=", z);
            numVertices += this->tileArr[x][z].appendVertices(vertices, numVertices);
        }
    }
    return vertices;
}

ErrCode MapGenerator::savePatchesDataXML(const irr::io::path& filename,
                                         irr::IrrlichtDevice* const device,
                                         const irr::u32 patchSizeX,
                                         const irr::u32 patchSizeZ,
                                         const irr::u32 patchCountX,
                                         const irr::u32 patchCountZ,
                                         const irr::u32 tileCountX,
                                         const irr::u32 tileCountZ,
                                         const irr::scene::CDynamicMeshBuffer* meshBuffer)
{
    if (device == nullptr)
    {
        Error::errContinue("can't save to xml because the device was a nullptr");
        return ErrCodes::GENERIC_ERR;
    }
    irr::io::IXMLWriter* xml = device->getFileSystem()->createXMLWriter(filename);
    if (xml == nullptr)
    {
        Error::errContinue("couldn't open file", filename.c_str());
        return ErrCode(ErrCodes::GENERIC_ERR);
    }

    xml->writeXMLHeader();
    xml->writeElement(L"mapSystem",
                      true,
                      L"version",
                      irr::core::stringw(tileMap::MapSystemVersion::version).c_str());
    xml->writeLineBreak();

    xml->writeElement(L"Map",
                      true,
                      L"PatchCountX",
                      irr::core::stringw(patchCountX).c_str(),
                      L"PatchCountZ",
                      irr::core::stringw(patchCountZ).c_str(),
                      L"TileCountX",
                      irr::core::stringw(tileCountZ).c_str(),
                      L"TileCountZ",
                      irr::core::stringw(tileCountX).c_str());
    xml->writeLineBreak();

    // save the levels for all individual tiles
    for (size_t levelX = 0; levelX < tileCountX; levelX++)
    {
        for (size_t levelZ = 0; levelZ < tileCountZ; levelZ++)
        {
            xml->writeElement(
                L"level",
                true,
                L"LevelX",
                irr::core::stringw(static_cast<int>(levelX)).c_str(),
                L"LevelZ",
                irr::core::stringw(static_cast<int>(levelZ)).c_str(),
                L"levels",
                irr::core::stringw(this->tileArr[levelX][levelZ].getLevels().getParsableString()).c_str());
            xml->writeLineBreak();
        }
    }

    // save all vertices
    for (irr::u32 i = 0; i < meshBuffer->getVertexCount(); i++)
    {
        const irr::video::S3DVertex2TCoords vertex =
            static_cast<irr::video::S3DVertex2TCoords*>(meshBuffer->getVertexBuffer().pointer())[i];
        xml->writeElement(L"vertex",
                          true,
                          L"pos",
                          StringWParser::getParsableString<irr::core::vector3df>(vertex.Pos).c_str(),
                          L"normal",
                          StringWParser::getParsableString(vertex.Normal).c_str(),
                          L"tcoords",
                          StringWParser::getParsableString(vertex.TCoords).c_str(),
                          L"tcoords2",
                          StringWParser::getParsableString(vertex.TCoords2).c_str());
        xml->writeLineBreak();
    }

    for (irr::u32 patchX = 0; patchX < patchCountX; patchX++)
    {
        for (irr::u32 patchZ = 0; patchZ < patchCountZ; patchZ++)
        {
            xml->writeElement(L"patch",
                              false,
                              L"PatchX",
                              irr::core::stringw(patchX).c_str(),
                              L"PatchZ",
                              irr::core::stringw(patchZ).c_str());
            xml->writeLineBreak();
            irr::u32 levelStartX = patchX * patchSizeX;
            if (levelStartX > tileCountX)
            {
                levelStartX = tileCountX;
            }
            irr::u32 levelEndX = levelStartX + patchSizeX;
            if (levelEndX > tileCountX)
            {
                levelEndX = tileCountX;
            }
            irr::u32 levelStartZ = patchZ * patchSizeZ;
            if (levelStartZ > tileCountZ)
            {
                levelStartZ = tileCountZ;
            }
            irr::u32 levelEndZ = levelStartZ + patchSizeZ;
            if (levelEndZ > tileCountZ)
            {
                levelEndZ = tileCountZ;
            }
            // append the indices for each tile in this patch
            for (irr::u32 tileX = levelStartX; tileX < levelEndX; tileX++)
            {
                for (irr::u32 tileZ = levelStartZ; tileZ < levelEndZ; tileZ++)
                {
                    const irr::u32 indexCount = this->tileArr[tileX][tileZ].indicesSize();
                    for (irr::u32 i = 0; i < indexCount; i++)
                    {
                        xml->writeElement(L"index",
                                          true,
                                          L"value",
                                          irr::core::stringw(this->tileArr[tileX][tileZ].indices[i]).c_str());
                        xml->writeLineBreak();
                    }
                }
            }
            xml->writeClosingTag(L"patch");
            debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                          "patch (x=",
                          patchX,
                          "z=",
                          patchZ,
                          ") from levelX=",
                          levelStartX,
                          "-",
                          levelEndX,
                          "levelZ=",
                          levelStartZ,
                          "-",
                          levelEndZ);
        } // for (irr::u32 patchZ = 0; patchZ < patchCountZ; patchZ++)
    }     // for (irr::u32 patchX = 0; patchX < terrainData.patchCountX; patchX++)
    xml->drop();
    return ErrCodes::NO_ERR;
}


ErrCode MapGenerator::savePatchesData(const irr::u32 patchSizeX,
                                      const irr::u32 patchSizeZ,
                                      const irr::io::path& filename,
                                      irr::IrrlichtDevice* const device,
                                      const bool binaryFormat)
{
    const irr::u32 tileCountX = static_cast<irr::u32>(this->tileArr.size());
    const irr::u32 tileCountZ = static_cast<irr::u32>(tileCountX > 0 ? this->tileArr[0].size() : 0);

    // create the individual patches
    irr::u32 patchCountX =
        static_cast<irr::u32>(std::ceil(static_cast<double>(tileCountX) / static_cast<double>(patchSizeX)));
    if (patchCountX == 0)
    {
        patchCountX = 1;
    }
    irr::u32 patchCountZ =
        static_cast<irr::u32>(std::ceil(static_cast<double>(tileCountZ) / static_cast<double>(patchSizeZ)));
    if (patchCountZ == 0)
    {
        patchCountZ = 1;
    }

    // connect up the tiles and get the vertices
    std::vector<irr::video::S3DVertex2TCoords> vertices = this->createVertices();

    // applying the offsetMap changed the normals of the mesh -> fix by recalculating
    // the meshManipulator needs an IMesh to recalculate the normals
    irr::video::E_INDEX_TYPE indexType = irr::video::EIT_16BIT;
    // is the map small enough to fit 16 bit indices?
    if (static_cast<irr::u32>(vertices.size()) !=
        static_cast<irr::u32>(static_cast<irr::u16>(vertices.size())))
    {
        indexType = irr::video::EIT_32BIT;
    }
    irr::scene::CDynamicMeshBuffer* mb = new irr::scene::CDynamicMeshBuffer(irr::video::EVT_2TCOORDS, indexType);
    // allocate space for the vertices
    mb->getVertexBuffer().set_used(static_cast<irr::u32>(vertices.size()));
    mb->getIndexBuffer().set_used(0);

    for (irr::u32 i = 0; i < vertices.size(); i++)
    {
        static_cast<irr::video::S3DVertex2TCoords*>(mb->getVertices())[i] = vertices[i];
    }

    for (irr::u32 patchX = 0; patchX < patchCountX; patchX++)
    {
        for (irr::u32 patchZ = 0; patchZ < patchCountZ; patchZ++)
        {
            irr::u32 levelStartX = patchX * patchSizeX;
            if (levelStartX > tileCountX)
            {
                levelStartX = tileCountX;
            }
            irr::u32 levelEndX = levelStartX + patchSizeX;
            if (levelEndX > tileCountX)
            {
                levelEndX = tileCountX;
            }
            irr::u32 levelStartZ = patchZ * patchSizeZ;
            if (levelStartZ > tileCountZ)
            {
                levelStartZ = tileCountZ;
            }
            irr::u32 levelEndZ = levelStartZ + patchSizeZ;
            if (levelEndZ > tileCountZ)
            {
                levelEndZ = tileCountZ;
            }
            for (irr::u32 tileX = levelStartX; tileX < levelEndX; tileX++)
            {
                for (irr::u32 tileZ = levelStartZ; tileZ < levelEndZ; tileZ++)
                {
                    const irr::u32 indexCount = this->tileArr[tileX][tileZ].indicesSize();
                    for (irr::u32 i = 0; i < indexCount; i++)
                    {
                        mb->getIndexBuffer().push_back(this->tileArr[tileX][tileZ].indices[i]);
                    }
                }
            }
        } // for (irr::u32 patchZ = 0; patchZ < patchCountZ; patchZ++)
    }     // for (irr::u32 patchX = 0; patchX < terrainData.patchCountX; patchX++)

    irr::scene::SMesh* mesh = new irr::scene::SMesh();
    mesh->addMeshBuffer(mb);
    this->sceneManager->getMeshManipulator()->recalculateNormals(mesh, false, false);

    // after the indices are generated save each patch and the indices that belong to that patch
    debugOutLevel(Debug::DebugLevels::stateInit + 1,
                  "assigning levels (",
                  tileCountX,
                  "x",
                  tileCountZ,
                  ") to patches (",
                  patchCountX,
                  "x",
                  patchCountZ,
                  ") with sizes (",
                  patchSizeX,
                  "x",
                  patchSizeZ,
                  ")");

    if (not binaryFormat)
    {
        return this->savePatchesDataXML(
            filename, device, patchSizeX, patchSizeZ, patchCountX, patchCountZ, tileCountX, tileCountZ, mb);
    }
    else
    {
        return this->savePatchesDataBinary(
            filename, patchSizeX, patchSizeZ, patchCountX, patchCountZ, tileCountX, tileCountZ, mb);
    }
}

ErrCode MapGenerator::savePatchesDataBinary(const irr::io::path& filename,
                                            const irr::u32 patchSizeX,
                                            const irr::u32 patchSizeZ,
                                            const irr::u32 patchCountX,
                                            const irr::u32 patchCountZ,
                                            const irr::u32 tileCountX,
                                            const irr::u32 tileCountZ,
                                            const irr::scene::CDynamicMeshBuffer* meshBuffer)
{
    std::fstream outFile;
    outFile.open(filename.c_str(), std::ios::out | std::ios::binary);
    if (not outFile.good())
    {
        Error::errContinue("Couldn't open file", repr<irr::io::path>(filename));
        return ErrCodes::GENERIC_ERR;
    }

    Binary::writeBinary(outFile, tileMap::MapSystemVersion::version);
    Binary::writeBinary(outFile, patchCountX);
    Binary::writeBinary(outFile, patchCountZ);
    Binary::writeBinary(outFile, tileCountX);
    Binary::writeBinary(outFile, tileCountZ);

    // save the levels for all individual tiles
    for (size_t levelX = 0; levelX < tileCountX; levelX++)
    {
        for (size_t levelZ = 0; levelZ < tileCountZ; levelZ++)
        {
            const Levels_t::BinaryData binaryData = this->tileArr[levelX][levelZ].getLevels().getBinaryData();
            outFile.write(reinterpret_cast<const char*>(&binaryData), sizeof(binaryData));
        }
    }

    // save all vertices
    Binary::writeBinary(outFile, meshBuffer->getVertexCount()); // binary data needs to know the
                                                                // sizes of everything beforehand
                                                                // because there is no way to tell
                                                                // what data is what (xml has it's
                                                                // handy labels)
    for (irr::u32 i = 0; i < meshBuffer->getVertexCount(); i++)
    {
        const irr::video::S3DVertex2TCoords vertex =
            static_cast<irr::video::S3DVertex2TCoords*>(meshBuffer->getVertexBuffer().pointer())[i];
        Binary::writeBinary(outFile, vertex.Pos.X);
        Binary::writeBinary(outFile, vertex.Pos.Y);
        Binary::writeBinary(outFile, vertex.Pos.Z);
        Binary::writeBinary(outFile, vertex.Normal.X);
        Binary::writeBinary(outFile, vertex.Normal.Y);
        Binary::writeBinary(outFile, vertex.Normal.Z);
        Binary::writeBinary(outFile, vertex.TCoords.X);
        Binary::writeBinary(outFile, vertex.TCoords.Y);
        Binary::writeBinary(outFile, vertex.TCoords2.X);
        Binary::writeBinary(outFile, vertex.TCoords2.Y);
    }

    for (irr::u32 patchX = 0; patchX < patchCountX; patchX++)
    {
        for (irr::u32 patchZ = 0; patchZ < patchCountZ; patchZ++)
        {
            irr::u32 levelStartX = patchX * patchSizeX;
            if (levelStartX > tileCountX)
            {
                levelStartX = tileCountX;
            }
            irr::u32 levelEndX = levelStartX + patchSizeX;
            if (levelEndX > tileCountX)
            {
                levelEndX = tileCountX;
            }
            irr::u32 levelStartZ = patchZ * patchSizeZ;
            if (levelStartZ > tileCountZ)
            {
                levelStartZ = tileCountZ;
            }
            irr::u32 levelEndZ = levelStartZ + patchSizeZ;
            if (levelEndZ > tileCountZ)
            {
                levelEndZ = tileCountZ;
            }
            // binary output format must know the index count beforehand
            irr::u32 patchIndexCount = 0;
            for (irr::u32 tileX = levelStartX; tileX < levelEndX; tileX++)
            {
                for (irr::u32 tileZ = levelStartZ; tileZ < levelEndZ; tileZ++)
                {
                    patchIndexCount += this->tileArr[tileX][tileZ].indicesSize();
                }
            }
            Binary::writeBinary(outFile, patchIndexCount);
            // append the indices for each tile in this patch
            for (irr::u32 tileX = levelStartX; tileX < levelEndX; tileX++)
            {
                for (irr::u32 tileZ = levelStartZ; tileZ < levelEndZ; tileZ++)
                {
                    const irr::u32 tileIndexCount = this->tileArr[tileX][tileZ].indicesSize();
                    for (irr::u32 i = 0; i < tileIndexCount; i++)
                    {
                        Binary::writeBinary(outFile, this->tileArr[tileX][tileZ].indices[i]);
                    }
                }
            }
            debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                          "patch (x=",
                          patchX,
                          "z=",
                          patchZ,
                          ") from levelX=",
                          levelStartX,
                          "-",
                          levelEndX,
                          "levelZ=",
                          levelStartZ,
                          "-",
                          levelEndZ);
        } // for (irr::u32 patchZ = 0; patchZ < patchCountZ; patchZ++)
    }     // for (irr::u32 patchX = 0; patchX < terrainData.patchCountX; patchX++)
    outFile.close();
    return ErrCodes::NO_ERR;
}

irr::core::vector2d<irr::s32>
MapGenerator::nextPixelInDirection(const irr::u32 pixelX, const irr::u32 pixelY, const directions::Directions direction)
{
    irr::core::vector2d<irr::s32> nextPixel(pixelX, pixelY);
    switch (direction)
    {
        case directions::Directions::TopLeft:
            nextPixel.X--;
            nextPixel.Y--;
            break;
        case directions::Directions::Top:
            nextPixel.Y--;
            break;
        case directions::Directions::TopRight:
            nextPixel.X++;
            nextPixel.Y--;
            break;
        case directions::Directions::Right:
            nextPixel.X++;
            break;
        case directions::Directions::BottomRight:
            nextPixel.X++;
            nextPixel.Y++;
            break;
        case directions::Directions::Bottom:
            nextPixel.Y++;
            break;
        case directions::Directions::BottomLeft:
            nextPixel.X--;
            nextPixel.Y++;
            break;
        case directions::Directions::Left:
            nextPixel.X--;
            break;
        case directions::Directions::Internal:
        case directions::Directions::NumDirections:
            Error::errTerminate("nextPixelInDirection(pixelX=",
                                pixelX,
                                "pixelY=",
                                pixelY,
                                "called with invalid direction with id=",
                                static_cast<int>(direction));
    }
    return nextPixel;
}

irr::core::vector2d<irr::s32> MapGenerator::getPixelForTile(const irr::u32 tileX,
                                                            const irr::u32 tileZ,
                                                            const irr::u32 maximumPixelX,
                                                            const directions::Directions direction)
{
    irr::core::vector2d<irr::s32> returnValue;
    switch (direction)
    {
        case directions::Directions::TopLeft:
            returnValue.X = maximumPixelX - tileX - 1;
            returnValue.Y = tileZ;
            return returnValue;
        case directions::Directions::TopRight:
            returnValue.X = maximumPixelX - tileX;
            returnValue.Y = tileZ;
            return returnValue;
        case directions::Directions::BottomRight:
            returnValue.X = maximumPixelX - tileX;
            returnValue.Y = tileZ + 1;
            return returnValue;
        case directions::Directions::BottomLeft:
            returnValue.X = maximumPixelX - tileX - 1;
            returnValue.Y = tileZ + 1;
            return returnValue;
        case directions::Directions::Top:
        case directions::Directions::Right:
        case directions::Directions::Bottom:
        case directions::Directions::Left:
        case directions::Directions::Internal:
        case directions::Directions::NumDirections:
            Error::errTerminate("getPixelForTile(tileX=", tileX, "tileZ=", tileZ, "wrong direction", static_cast<int>(direction));
    }

    return returnValue;
}

ErrCode MapGenerator::generatePathMap(const irr::core::vector3df mapScale, PathGrid& pathGrid)
{
    const irr::core::vector2df mapMeshSizeXZ =
        irr::core::vector2df(static_cast<irr::f32>(this->tileArr.size()) * mapScale.X,
                             static_cast<irr::f32>(this->tileArr[0].size()) * mapScale.Z);
    // set the dimensions of our pathgrid
    // + 1 because the pathgrid starts at (0,0) and should cover the whole map up to the edge
    pathGrid.width = static_cast<irr::u32>(mapMeshSizeXZ.X / pathGrid.unitSize) + 1;
    pathGrid.height = static_cast<irr::u32>(mapMeshSizeXZ.Y / pathGrid.unitSize) + 1;
    debugOutLevel(Debug::DebugLevels::stateInit + 1,
                  "generating path map size",
                  pathGrid.width,
                  pathGrid.height,
                  "from terrainData size",
                  vector2dToStringW(mapMeshSizeXZ),
                  "unitSize =",
                  pathGrid.unitSize);

    pathGrid.resize(pathGrid.width, pathGrid.height);

    // size of a unit relative to the size of a Tile
    const irr::f32 tileScanAreaX = static_cast<irr::f32>(pathGrid.unitSize / mapScale.X / 2.0);
    const irr::f32 tileScanAreaZ = static_cast<irr::f32>(pathGrid.unitSize / mapScale.Z / 2.0);

    for (irr::u32 gridX = 0; gridX < pathGrid.width; gridX++)
    {
        for (irr::u32 gridZ = 0; gridZ < pathGrid.height; gridZ++)
        {
            // find out which Level_t is at that pathgrid position
            const irr::s32 levelX =
                irr::core::s32_clamp(static_cast<irr::s32>(static_cast<irr::f32>(gridX) *
                                                           pathGrid.unitSize / mapScale.X),
                                     0,
                                     static_cast<irr::s32>(this->tileArr.size()) - 1);
            const irr::s32 levelZ =
                irr::core::s32_clamp(static_cast<irr::s32>(static_cast<irr::f32>(gridZ) *
                                                           pathGrid.unitSize / mapScale.Z),
                                     0,
                                     static_cast<irr::s32>(this->tileArr[0].size()) - 1);

            // relative position inside the Level_t for that pathgrid position
            // Walkability is checked in sizes of a unit (64.0 x 64.0) but the Levels_t expects them
            // nomralized in the range [0, 1]
            const irr::f32 levelRelativeX =
                (static_cast<irr::f32>(gridX) * pathGrid.unitSize) / mapScale.X - static_cast<irr::f32>(levelX);
            const irr::f32 levelRelativeZ =
                (static_cast<irr::f32>(gridZ) * pathGrid.unitSize) / mapScale.Z - static_cast<irr::f32>(levelZ);

            debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                          "asking level",
                          levelX,
                          levelZ,
                          "for walkability around relative point",
                          levelRelativeX,
                          levelRelativeZ,
                          "in range",
                          tileScanAreaZ,
                          tileScanAreaZ);
            const PathGrid::Walkability isWalkable =
                this->tileArr[levelX][levelZ].isWalkable(levelRelativeX, levelRelativeZ, tileScanAreaX, tileScanAreaZ) ?
                PathGrid::Walkability::Free :
                PathGrid::Walkability::StaticNonWalkable;
            pathGrid.set(gridX, gridZ, isWalkable);
        }
    }

    // TODO: change this defaulting to the middle of the map to something that is set in an xml file
    return floodFill(pathGrid, irr::core::vector2d<uint32_t>(pathGrid.width / 2, pathGrid.height / 2));
}

ErrCode MapGenerator::floodFill(PathGrid& pathGrid, const irr::core::vector2d<uint32_t> knownWalkablePosition)
{
    debugOutLevel(Debug::DebugLevels::stateInit + 1, "flood-filling the pathmap to get walkable areas");
    irr::core::array<irr::core::array<bool>> mapcopy;
    for (uint32_t i = 0; i < pathGrid.width; ++i)
    {
        mapcopy.push_back(irr::core::array<bool>());
        for (uint32_t k = 0; k < pathGrid.height; ++k)
        {
            mapcopy[i].push_back(pathGrid(i, k));
            pathGrid.set(i, k, PathGrid::Walkability::StaticNonWalkable);
        }
    }
    std::stack<irr::core::vector2d<uint32_t>> stack;
    stack.push(knownWalkablePosition);

    while (!stack.empty())
    {
        irr::core::vector2d<uint32_t> topVector = stack.top();
        stack.pop();
        // Why is -1 necessary ?? - weird error
        if (topVector.X > pathGrid.width - 1 || topVector.Y > pathGrid.height - 1)
        {
            continue;
        }
        if (mapcopy[topVector.X][topVector.Y] && !pathGrid(topVector.X, topVector.Y))
        {
            // debugOutLevel(20,"FLoodFILL: stacksize: ",stack.size()," current vector: x:
            // ",topVector.X," current vector: y: ",topVector.Y,
            //              "Grid: h: ",pathGrid.height," w ", pathGrid.width);
            pathGrid.set(topVector.X, topVector.Y, PathGrid::Walkability::Free);
            // just look up, down, left, right -> left handed coordsystem
            stack.push(irr::core::vector2d<uint32_t>(topVector.X, topVector.Y + 1));
            stack.push(irr::core::vector2d<uint32_t>(topVector.X, topVector.Y - 1));
            stack.push(irr::core::vector2d<uint32_t>(topVector.X + 1, topVector.Y));
            stack.push(irr::core::vector2d<uint32_t>(topVector.X - 1, topVector.Y));
        }
    }

    debugOutLevel(Debug::DebugLevels::stateInit + 1, "flood-filling complete");
    return ErrCodes::NO_ERR;
}
