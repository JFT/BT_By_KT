/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GAMEMAP_H
#define GAMEMAP_H

#include <queue> // std::deque
#include <vector>

#include <irrlicht/irrXML.h>
#include <memory> // std::unique_ptr
#include "pathGrid.h"
#include "pathfinder.h"
#include "terrainscenenode.h"
#include "../ecs/entity.h"

// forward declarations
class Game;
class TerrainShader;
class EntityManager;
namespace Handles
{
    class HandleManager;
}
namespace irr
{
    namespace scene
    {
        class IMeshSceneNode;
    }
} // namespace irr


class GameMap
{
   public:
    explicit GameMap(Game& game,
                     const PathGrid pathGrid_,
                     std::unique_ptr<irr::scene::TerrainSceneNode>& terrainNode_,
                     TerrainShader* const terrainShader_);
    GameMap(const GameMap&) = delete;
    virtual ~GameMap();

    irr::scene::TerrainSceneNode* getTerrainNode() const;
    irr::scene::ITriangleSelector* getTriangleSelector() const;
    TerrainShader* getTerrainShader() const;

    GameMap operator=(const GameMap&) = delete;

    inline irr::core::vector3df get3dPosition(const irr::core::vector2df position) const
    {
        POW2_ASSERT(this->terrainNode != nullptr);
        return irr::core::vector3df(position.X,
                                    this->terrainNode->getHeight(position.X, position.Y),
                                    position.Y);
    }

    // PATHFINDING
    std::deque<irr::core::vector2df>
    getPath(const irr::core::vector2df& position, const irr::core::vector2df& target);
    PathGrid& getPathGrid() { return this->pathGrid; }
    ErrCode generateDebugCubes(const bool onlyUnWalkable = true);
    bool changeDebugCubesVisiblity();
    bool getDebugCubesVisible() const;

    void setDebugCubeColor(const irr::core::vector2df& pos, const irr::video::SColor& color);
    void setDebugCubeColor(const PathGrid::PathCellCoordinate_t pathCellCoordinate,
                           const irr::video::SColor& color);
    irr::video::SColor getDebugCubeColor(const PathGrid::PathCellCoordinate_t pathCellCoordinate) const;

    inline irr::core::vector2df getMapSize() { return this->mapSize; }
    inline irr::core::vector3df getMapPosition() { return this->mapPosition; }
    bool calculateCursorTo3DMapPosition(const irr::core::vector2d<int> cursorPosition,
                                        irr::core::vector3df& colPoint) const;
    bool calculateCursorTo2DMapPosition(const irr::core::vector2d<int> cursorPosition,
                                        irr::core::vector2df& colPoint) const;

    /// @brief flip the pathGrid point nearest to 'point' between walkable <-> unwalkable
    /// does nothing if 'point' is nowhere on the map.
    /// @param point
    /// also generates a new debugCube at that position if neccecary
    // TODO: move inside ifdef(MAKE_DEBUG_) ? Along with all the other debugCubes related code?
    void flipPathMapPoint(const irr::core::vector3df point);

    const irr::video::SColor pathingFree = irr::video::SColor(255, 0, 0, 255);
    const irr::video::SColor pathingStaticObject = irr::video::SColor(255, 255, 0, 0);

   protected:
   private:
    Game& game;
    std::unique_ptr<irr::scene::TerrainSceneNode> terrainNode;

    const irr::core::vector2df mapSize;
    const irr::core::vector3df mapPosition;

    TerrainShader* const terrainShader;

    // Pathfinding related
    PathGrid pathGrid;
    JPS::PathFinder<PathGrid> pathFinder;

    // generate our map of bools
    bool walkablePath(const irr::core::vector2df& start, const irr::core::vector2df& end);
    // ALL FOR DEBUG
    // TODO: PUT IT IN IFDEF
    irr::u32 singleDebugCubeVertexCount = 0;
    irr::scene::IMeshSceneNode* cubesNode = nullptr;
};

#endif // GameMap_H
