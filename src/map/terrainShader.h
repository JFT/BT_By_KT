/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TERRAINSHADER_H
#define TERRAINSHADER_H

#include <irrlicht/IShaderConstantSetCallBack.h>

#include "../utils/static/error.h" // defines ErrCode

// forward declarations
namespace irr
{
    namespace video
    {
        class SMaterial;
        class ITexture;
        class IVideoDriver;
        class IMaterialRendererServices;
    } // namespace video
} // namespace irr


/// \brief The TerrainShader uses a 2D TEXTURE ARRAY for texturing - caution you need to compile
/// irrlicht from irrlicht_extended yourself to be able to use it

class TerrainShader : public irr::video::IShaderConstantSetCallBack
{
   public:
    /// @brief contains the terrainShader material and callback function
    /// @param splatTexture_ will be grab()ed. Mustn't be nullptr
    /// @param textureArray_ will be grab()ed. Mustn't be nullptr
    /// @param bumpTextureArray_ will be grab()ed. Mustn't be nullptr
    /// @param blendTexture_ will be grab()ed. Mustn't be nullptr
    TerrainShader(irr::video::IVideoDriver* const driver,
                  irr::video::ITexture* const splatTexture_,
                  irr::video::ITexture* const textureArray_,
                  irr::video::ITexture* const bumpTextureArray_,
                  irr::video::ITexture* const blendTexture_);
    TerrainShader(const TerrainShader&) = delete;
    virtual ~TerrainShader();

    ErrCode loadShaders();

    /// @brief apply the shader onto the material by setting the textures and the material type
    /// @param material
    void applyToMaterial(irr::video::SMaterial& material);

    inline irr::video::ITexture* getSplatTexture() { return this->splatTexture; }
    inline irr::video::ITexture* getTextureArray() { return this->textureArray; }
    inline void setTextureArray(irr::video::ITexture* const newTextureArray_)
    {
        this->textureArray = newTextureArray_;
    }

    inline irr::video::ITexture* getBumpTextureArray() { return this->bumpTextureArray; }
    inline irr::video::ITexture* getBlendTexture() { return this->blendTexture; }
    ///
    /// \brief used to set the ShaderConstants
    /// \param services
    /// \param userData
    virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32 userData);

    TerrainShader operator=(const TerrainShader&) = delete;

   private:
    int shaderMaterial = -1;
    irr::video::IVideoDriver* const driver;

    irr::video::ITexture* const splatTexture;
    irr::video::ITexture* textureArray;
    irr::video::ITexture* const bumpTextureArray;
    irr::video::ITexture* const blendTexture; // TODO: Figure out which Textures are needed
};

#endif // TERRAINSHADER_H
