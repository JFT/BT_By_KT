/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// clang fails to compile if operator<<(irr::core::stringw) isn't defined, because a call to
// ErrTerminate outputs a stringw
// this definition has to occur before the function is ever referenced -> make it the first include
#include "../utils/stringwstream.h"
#include "../utils/debug.h"
#include "../utils/stringwparser.h"

#include "gamemap.h"


#include <irrlicht/IMesh.h>
#include <irrlicht/IMeshSceneNode.h>
#include <irrlicht/ISceneCollisionManager.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/SMesh.h>
#include <irrlicht/SMeshBuffer.h>
#include <math.h>
#include <memory> // std::unique_ptr
#include "terrainShader.h"
#include "terrainVertexSelector.h"
#include "terraintriangleselector.h"
#include "../core/game.h"
#include "../utils/static/error.h"

// forward declarations
namespace irr
{
    namespace io
    {
        class IWriteFile;
    }
} // namespace irr


// this line fixes a clang linking error. For its reasons and discussion see here:
// https://stackoverflow.com/questions/28264279/undefined-reference-when-accessing-static-constexpr-float-member
// and here:
// https://stackoverflow.com/questions/26196095/static-constexpr-odr-used-or-not
constexpr float PathGrid::unitSize;


GameMap::GameMap(Game& game_,
                 const PathGrid pathGrid_,
                 std::unique_ptr<irr::scene::TerrainSceneNode>& terrainNode_,
                 TerrainShader* const terrainShader_)
    : game(game_)
    , terrainNode(std::move(terrainNode_))
    , mapSize(terrainNode->getMeshSizeXZ())
    , mapPosition(terrainNode->getPosition())
    , terrainShader(terrainShader_)
    , pathGrid(pathGrid_)
    , pathFinder(this->pathGrid)
{
    POW2_ASSERT(this->terrainNode);
    // add the terrain node to the scene graph
    game.getSceneManager()->getRootSceneNode()->addChild(
        static_cast<irr::scene::ISceneNode*>(this->terrainNode.get()));
#if !defined(MAKE_SERVER_)
    this->terrainShader->applyToMaterial(this->terrainNode->getMaterial(0));
#endif
}

GameMap::~GameMap()
{
    if (this->cubesNode != nullptr)
    {
        this->cubesNode->remove(); // only remove on the sceneNodes called because irrlicht will
                                   // delete the object automatically if no more references exist
    }
    if (this->terrainNode != nullptr)
    {
        if (this->terrainNode->getVertexSelector())
        {
            delete this->terrainNode->getVertexSelector();
        }

        this->terrainNode->remove(); // only remove on the sceneNodes called because irrlicht will
                                     // delete the object automatically if no more references exist
        this->terrainNode.release();
    }
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
irr::scene::TerrainSceneNode* GameMap::getTerrainNode() const
{
    return this->terrainNode.get();
}

// ---------------------------------------------------------------------------
irr::scene::ITriangleSelector* GameMap::getTriangleSelector() const
{
    if (this->terrainNode == nullptr)
    {
        Error::errContinue("GameMap::getTriangleSelector(): terrainNode is nullptr");
        return nullptr;
    }
    return this->terrainNode->getTriangleSelector();
}

// ---------------------------------------------------------------------------
TerrainShader* GameMap::getTerrainShader() const
{
    return this->terrainShader;
}

std::deque<irr::core::vector2df>
GameMap::getPath(const irr::core::vector2df& position, const irr::core::vector2df& target)
{
    // calculate 2D position in our discretized 2d array from 3d target
    irr::u32 targetX = irr::u32((target.X - this->mapPosition.X) / this->pathGrid.unitSize);
    irr::u32 targetZ = irr::u32((target.Y - this->mapPosition.Z) / this->pathGrid.unitSize);
    irr::u32 posX = irr::u32(roundf((position.X - this->mapPosition.X) / this->pathGrid.unitSize));
    irr::u32 posZ = irr::u32(roundf((position.Y - this->mapPosition.Z) / this->pathGrid.unitSize));
    debugOutLevel(Debug::DebugLevels::firstOrderLoop - 1, "Pathfinder: POS X = ", posX, ", Y = ", posZ);
    JPS::PathVector pathVect;
    std::deque<irr::core::vector2df> waypoints;

    if (targetX == posX && targetZ == posZ)
    {
        waypoints.push_back(target);
        return waypoints;
    }

    JPS::ExposedResults found =
        pathFinder.findPath(pathVect, JPS::Pos(posX, posZ), JPS::Pos(targetX, targetZ), 0);
    switch (found)
    {
        case JPS::ExposedResults::DIRECT_PATH:
        case JPS::ExposedResults::NO_DIRECT_PATH: // found the path in our discretized 2d array ->
                                                  // now transform it back to 3d waypoints
        {
            for (size_t i = 0; i < pathVect.size(); ++i)
            {
                irr::f32 resX =
                    irr::f32(pathVect[i].x) * this->pathGrid.unitSize + this->mapPosition.X;
                irr::f32 resZ =
                    irr::f32(pathVect[i].y) * this->pathGrid.unitSize + this->mapPosition.Z;
                waypoints.push_back(irr::core::vector2df(resX, resZ));
                debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                              "Pathfinder: X = ",
                              resX,
                              ", Y = ",
                              terrainNode->getHeight(resX, resZ),
                              " Z = ",
                              resZ);
            }

            // overwrite last point with real target if we clicked on a walkable point.
            if (found == JPS::ExposedResults::DIRECT_PATH)
            {
                waypoints.back() = target;
            }
            // smoothing
            irr::core::vector2df checkPoint = position;
            std::deque<irr::core::vector2df> resWaypoints;
            irr::core::vector2df currentPoint = waypoints.front();
            irr::core::vector2df resTarget = waypoints.back();
            waypoints.pop_front();
            while (!waypoints.empty())
            {
                irr::core::vector2df nextPoint = waypoints.front();
                if (walkablePath(checkPoint, nextPoint))
                {
                    debugOutLevel(10,
                                  "Pathfinder Smoothing skip: X = ",
                                  currentPoint.X,
                                  ", Y = ",
                                  currentPoint.Y);
                }
                else
                {
                    resWaypoints.push_back(currentPoint);
                    debugOutLevel(10,
                                  "Pathfinder Smoothing Push_back: X = ",
                                  currentPoint.X,
                                  ", Y = ",
                                  currentPoint.Y);
                    checkPoint = currentPoint;
                }
                currentPoint = nextPoint;
                waypoints.pop_front();
            }
            resWaypoints.push_back(resTarget);

            debugOutLevel(10,
                          "Pathfinder: X = ",
                          waypoints.empty() ? 0 : waypoints.back().X,
                          ", Y = ",
                          waypoints.empty() ? 0 : waypoints.back().Y);
            return resWaypoints;
        }
        case JPS::ExposedResults::PATH_NON_EXISTING:
        default:
            debugOutLevel(10, "PATHFINDER: NO PATH FOUND -> returning current position");
            waypoints.push_back(position);
            return waypoints;
    }
}


bool GameMap::walkablePath(const irr::core::vector2df& start, const irr::core::vector2df& end)
{
    irr::core::vector2d<irr::u32> endGridVect;
    irr::core::vector2d<irr::u32> startGridVect;
    endGridVect.X = irr::u32((end.X - this->mapPosition.X) / this->pathGrid.unitSize);
    endGridVect.Y = irr::u32((end.Y - this->mapPosition.Z) / this->pathGrid.unitSize);
    startGridVect.X = irr::u32(roundf((start.X - this->mapPosition.X) / this->pathGrid.unitSize));
    startGridVect.Y = irr::u32(roundf((start.Y - this->mapPosition.Z) / this->pathGrid.unitSize));
    // calculate normals and slope of our 2d line in the pathgrid
    irr::s32 dx = endGridVect.X - startGridVect.X;
    irr::s32 dy = endGridVect.Y - startGridVect.Y;

    irr::core::vector2df grad = irr::core::vector2df(float(dx), float(dy));
    irr::core::vector2df normal1(float(-dx), float(dy));
    irr::core::vector2df normal2(float(dx), float(-dy));
    grad.normalize();
    normal1.normalize();
    normal2.normalize();

    // now check in steps of fifth of unitsizes on the line if the area is walkable
    irr::f32 connectionLength = sqrtf(irr::f32(dx) * irr::f32(dx) + irr::f32(dy) * irr::f32(dy));
    int iterations = int(roundf(5.0f * connectionLength));
    for (int i = 0; i < iterations; ++i)
    {
        irr::core::vector2df toCheck;
        toCheck.X = irr::f32(startGridVect.X) + grad.X / 5.0f * irr::f32(i);
        toCheck.Y = irr::f32(startGridVect.Y) + grad.Y / 5.0f * irr::f32(i);

        debugOutLevel(50, "Pathfinder CHECK WALKABLE middle : X = ", toCheck.X, ", Y = ", toCheck.Y);
        // check directly at the middle
        if (!this->pathGrid(irr::u32(roundf(toCheck.X)), irr::u32(roundf(toCheck.Y))))
        {
            return false;
        }

        // check left
        //        irr::core::vector2df toCheckl;
        //        toCheckl = toCheck+normal1;
        //        debugOutLevel(50, "Pathfinder CHECK WALKABLE left : X = ", toCheckl.X, ", Y = ",
        //        toCheckl.Y);
        //        if(!grid(irr::u32(roundf(toCheckl.X)),irr::u32(roundf(toCheckl.Y))))
        //        {
        //            return false;
        //        }

        // check right - we don't need to create a new vector here - it's also not necessary (put
        // all the code in the if-statement) above but to make it more readable
        // it's nicer to create one
        //        toCheck +=normal2;
        //        debugOutLevel(50, "Pathfinder CHECK WALKABLE right : X = ", toCheck.X, ", Y = ",
        //        toCheck.Y);
        //        if(!grid(irr::u32(roundf(toCheck.X)),irr::u32(roundf(toCheck.Y))))
        //        {
        //            return false;
        //        }
    }
    return true;
}

ErrCode GameMap::generateDebugCubes(const bool onlyUnWalkable)
{
    constexpr irr::f32 cubeSize = 10.0f;
    const irr::scene::IMesh* const defaultCube =
        game.getSceneManager()->getGeometryCreator()->createCubeMesh(irr::core::vector3df(cubeSize));
    const irr::u32 vertexCount = defaultCube->getMeshBuffer(0)->getVertexCount();
    this->singleDebugCubeVertexCount = vertexCount;
    const irr::u32 indexCount = defaultCube->getMeshBuffer(0)->getIndexCount();

    irr::core::array<irr::scene::SMeshBuffer*> mbs;
    mbs.push_back(new irr::scene::SMeshBuffer);

    irr::u32 currentMeshBufferVertexCount = 0;
    irr::u32 currentMeshBufferIndexCount = 0;
    for (PathGrid::PathGridDimension_t x = 0; x < this->pathGrid.width; x++)
    {
        for (PathGrid::PathGridDimension_t z = 0; z < this->pathGrid.height; z++)
        {
            currentMeshBufferVertexCount += vertexCount;
            currentMeshBufferIndexCount += indexCount;
            if (currentMeshBufferVertexCount > std::numeric_limits<irr::u16>::max())
            {
                mbs.getLast()->Vertices.set_used(currentMeshBufferVertexCount - vertexCount);
                mbs.getLast()->Indices.set_used(currentMeshBufferIndexCount - indexCount);
                mbs.push_back(new irr::scene::SMeshBuffer);
                currentMeshBufferVertexCount = vertexCount;
                currentMeshBufferIndexCount = indexCount;
            }
        }
    }
    mbs.getLast()->Vertices.set_used(currentMeshBufferVertexCount);
    mbs.getLast()->Indices.set_used(currentMeshBufferIndexCount);

    irr::u32 currentStartingVertex = 0;
    irr::u32 currentStartingIndex = 0;
    irr::u32 currentMeshBuffer = 0;

    POW2_ASSERT(mbs.getLast()->getVertexType() == defaultCube->getMeshBuffer(0)->getVertexType());
    POW2_ASSERT(mbs.getLast()->getIndexType() == defaultCube->getMeshBuffer(0)->getIndexType());

    debugOutLevel(Debug::DebugLevels::stateInit,
                  "generating pathfinding debug cubes of pathmap width=",
                  this->pathGrid.width,
                  "height=",
                  this->pathGrid.height);
    irr::core::array<irr::scene::IMeshSceneNode*> debugCubeList;
    for (unsigned int x = 0; x < this->pathGrid.width; ++x)
    {
        for (unsigned int z = 0; z < this->pathGrid.height; ++z)
        {
            if (onlyUnWalkable and this->pathGrid(x, z))
            {
                continue; // that position is walkable but only the un-walkable positions should be
                          // highligted
            }

            auto centerPos = this->pathGrid.getCenterPosition(x, z);

            centerPos.X += this->terrainNode->getPosition().X;
            centerPos.Y += this->terrainNode->getPosition().Z;
            // the grid position right at the edge of the map can lead to coordinates just outside
            // of the terrainNode mesh for which getHeight() returns -irr::FLT_MIN
            // -> make sure resX and resZ are inside the terrainNode mesh
            if (centerPos.X >=
                this->terrainNode->getMeshSizeXZ().X + this->terrainNode->getPosition().X)
            {
                centerPos.X = static_cast<irr::f32>(0.999 * this->terrainNode->getMeshSizeXZ().X +
                                                    this->terrainNode->getPosition().X);
            }
            if (centerPos.Y >=
                this->terrainNode->getMeshSizeXZ().Y + this->terrainNode->getPosition().Z)
            {
                centerPos.Y = static_cast<irr::f32>(0.999 * this->terrainNode->getMeshSizeXZ().Y +
                                                    this->terrainNode->getPosition().Z);
            }

            // not all vertices fit into this single irr::u16 meshbuffer -> if it would overflow
            // create a new one
            if (currentStartingVertex + vertexCount > std::numeric_limits<irr::u16>::max())
            {
                currentMeshBuffer++;
                currentStartingVertex = 0;
                currentStartingIndex = 0;
            }

            for (irr::u32 v = 0; v < vertexCount; v++)
            {
                mbs[currentMeshBuffer]->Vertices[currentStartingVertex + v] =
                    static_cast<irr::video::S3DVertex*>(defaultCube->getMeshBuffer(0)->getVertices())[v];
                const auto centerPos3d =
                    irr::core::vector3df(centerPos.X,
                                         this->terrainNode->getSmoothedHeight(centerPos.X, centerPos.Y),
                                         centerPos.Y);
                mbs[currentMeshBuffer]->Vertices[currentStartingVertex + v].Pos += centerPos3d;
                if (!pathGrid(x, z))
                {
                    mbs[currentMeshBuffer]->Vertices[currentStartingVertex + v].Color = this->pathingStaticObject;
                }
                else
                {
                    mbs[currentMeshBuffer]->Vertices[currentStartingVertex + v].Color = this->pathingFree;
                }
            }

            for (irr::u32 i = 0; i < indexCount; i++)
            {
                mbs[currentMeshBuffer]->Indices[currentStartingIndex + i] = static_cast<irr::u16>(
                    defaultCube->getMeshBuffer(0)->getIndices()[i] + currentStartingVertex);
            }

            currentStartingVertex += vertexCount;
            currentStartingIndex += indexCount;
        } // unsigned int z = 0; z < this->pathGrid.height; ++z
    }     // unsigned int x = 0; x < this->pathGrid.width; ++x

    if (this->cubesNode != nullptr)
    {
        this->cubesNode->remove();
    }

    irr::scene::SMesh* m = new irr::scene::SMesh;
    for (irr::u32 i = 0; i < mbs.size(); i++)
    {
        m->addMeshBuffer(mbs[i]);
        mbs[i]->drop();
    }
    POW2_ASSERT(m->getMeshBufferCount() == mbs.size());

    // TODO: somehow the cubesNode receives shadows? why?? oh why effectHandler?
    this->cubesNode = game.getSceneManager()->addMeshSceneNode(m);
    m->drop();
    this->cubesNode->getMaterial(0).Lighting = false;
    this->cubesNode->getMaterial(0).BackfaceCulling = true;

    // we no longer need the cube we copied all the vertices from
    defaultCube->drop();

    return ErrCode(ErrCodes::NO_ERR);
}

void GameMap::setDebugCubeColor(const irr::core::vector2df& pos, const irr::video::SColor& color)
{
    // 1st get the meshBuffer id the cube should be in
    if (this->cubesNode == nullptr)
    {
        return;
    }
    const PathGrid::PathCellCoordinate_t pathCellCoordinate = this->pathGrid.getPathCellCoordinate(pos);
    this->setDebugCubeColor(pathCellCoordinate, color);
}

void GameMap::setDebugCubeColor(const PathGrid::PathCellCoordinate_t pathCellCoordinate,
                                const irr::video::SColor& color)
{
    if (this->cubesNode == nullptr)
    {
        return;
    }

    if (pathCellCoordinate.X > this->pathGrid.width || pathCellCoordinate.Z > this->pathGrid.height)
    {
        return;
    }

    irr::u32 meshBufferID = 0;
    irr::u32 cubeBeginVertex = (pathCellCoordinate.X * this->pathGrid.height + pathCellCoordinate.Z) *
        this->singleDebugCubeVertexCount;
    while (cubeBeginVertex + singleDebugCubeVertexCount >
           this->cubesNode->getMesh()->getMeshBuffer(meshBufferID)->getVertexCount())
    {
        cubeBeginVertex -= this->cubesNode->getMesh()->getMeshBuffer(meshBufferID)->getVertexCount();
        meshBufferID++;
        POW2_ASSERT(meshBufferID < this->cubesNode->getMesh()->getMeshBufferCount());
    }

    for (irr::u32 v = 0; v < this->singleDebugCubeVertexCount; v++)
    {
        static_cast<irr::video::S3DVertex*>(
            this->cubesNode->getMesh()->getMeshBuffer(meshBufferID)->getVertices())[cubeBeginVertex + v]
            .Color = color;
    }
}

irr::video::SColor GameMap::getDebugCubeColor(const PathGrid::PathCellCoordinate_t pathCellCoordinate) const
{
    if (this->cubesNode == nullptr)
    {
        irr::video::SColor(0, 0, 0, 0);
    }

    irr::u32 meshBufferID = 0;
    irr::u32 cubeBeginVertex = (pathCellCoordinate.X * this->pathGrid.height + pathCellCoordinate.Z) *
        this->singleDebugCubeVertexCount;
    while (cubeBeginVertex + singleDebugCubeVertexCount >
           this->cubesNode->getMesh()->getMeshBuffer(meshBufferID)->getVertexCount())
    {
        cubeBeginVertex -= this->cubesNode->getMesh()->getMeshBuffer(meshBufferID)->getVertexCount();
        meshBufferID++;
    }

    return static_cast<irr::video::S3DVertex*>(
               this->cubesNode->getMesh()->getMeshBuffer(meshBufferID)->getVertices())[cubeBeginVertex]
        .Color;
}

bool GameMap::calculateCursorTo3DMapPosition(const irr::core::vector2d<int> cursorPosition,
                                             irr::core::vector3df& colPoint) const
{
    irr::core::triangle3df tri3d;
    irr::scene::ISceneNode* node;
    irr::core::line3df ray = this->game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
        cursorPosition, game.getSceneManager()->getActiveCamera());
    if (!this->game.getSceneManager()->getSceneCollisionManager()->getCollisionPoint(
            ray, this->terrainNode->getTriangleSelector(), colPoint, tri3d, node))
    {
        return false;
    }
    return true;
}

bool GameMap::calculateCursorTo2DMapPosition(const irr::core::vector2d<int> cursorPosition,
                                             irr::core::vector2df& colPoint2D) const
{
    irr::core::vector3df colPoint;
    if (this->calculateCursorTo3DMapPosition(cursorPosition, colPoint))
    {
        colPoint2D = irr::core::vector2df(colPoint.X, colPoint.Z);
        return true;
    }
    return false;
}

void GameMap::flipPathMapPoint(const irr::core::vector3df point)
{
    // hit the terrain at a certain spot -> find out which point in the pathMap corresponds to this
    // terrain point
    debugOutLevel(Debug::DebugLevels::onEvent, "point=", StringWParser::getParsableString(point));

    auto pathCell = this->pathGrid.getPathCellCoordinate(point);
    debugOutLevel(Debug::DebugLevels::onEvent, "pathX=", pathCell.X, "pathZ=", pathCell.Z);

    if (pathCell.X >= this->pathGrid.width or pathCell.Z >= this->pathGrid.height)
    {
        return;
    }

    const PathGrid::Walkability old = this->pathGrid.get(pathCell);

    if (old == PathGrid::StaticNonWalkable)
    {
        this->pathGrid.set(pathCell, PathGrid::Walkability::Free);
        this->setDebugCubeColor(pathCell, this->pathingFree);
    }
    else
    {
        this->pathGrid.set(pathCell, PathGrid::Walkability::StaticNonWalkable);
        this->setDebugCubeColor(pathCell, this->pathingStaticObject);
    }
}

bool GameMap::getDebugCubesVisible() const
{
    if (this->cubesNode == nullptr)
    {
        return false;
    }
    return this->cubesNode->isVisible();
}

// ---------------------------------------------------------------------------
bool GameMap::changeDebugCubesVisiblity()
{
    if (this->cubesNode != nullptr)
    {
        if (this->cubesNode->isVisible())
        {
            debugOutLevel(Debug::DebugLevels::stateInit + 1,
                          "Changing Visibility of Debug Cubes "
                          "to invisible because isVisible was "
                          "true");
            this->cubesNode->setVisible(false);
        }
        else
        {
            debugOutLevel(Debug::DebugLevels::stateInit + 1,
                          "Changing Visibility of Debug Cubes "
                          "to visible because isVisible was "
                          "false");
            this->cubesNode->setVisible(true);
        }
        return true;
    }
    else
    {
        debugOutLevel(Debug::DebugLevels::stateInit + 1, "no debug cubes to make visible/invisible!");
    }
    return false;
}
