/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mapLoader.h"
#include <ios> // ios_base::failure
#include <iostream>
#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/IFileSystem.h>
#include <irrlicht/IMeshWriter.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/IVideoDriver.h>
#include <irrlicht/IXMLReader.h>
#include <irrlicht/IXMLWriter.h>
#include <memory> // std::unique_ptr
#include <string>
#include "gamemap.h"
#include "mapSystemVersion.h"
#include "staticEntitiesIrrlichtNodeConverter.h"
#include "terrainShader.h"
#include "terrainVertexSelector.h"
#include "terrainscenenode.h"
#include "terraintriangleselector.h"
#include "../utils/debug.h"
#include "../utils/static/error.h"
#include "../utils/stringwparser.h"


MapLoader::MapLoader(irr::scene::ISceneManager* const sceneManager_,
                     irr::io::IFileSystem* const fileSystem_,
                     irr::video::IVideoDriver* const videoDriver_,
                     Game& game_)
    : sceneManager(sceneManager_)
    , fileSystem(fileSystem_)
    , videoDriver(videoDriver_)
    , game(game_)
{
}


std::tuple<MapLoader::MapLoadingParameters, GameMap*, std::vector<irr::scene::IAnimatedMeshSceneNode*>>
MapLoader::loadMap(const std::string filename, const bool allowSplatMapModification)
{
    MapLoadingParameters loadingParameters;
    loadingParameters.allowSplatMapModification = allowSplatMapModification;

    // Init TAGS in map and other necessary loading variables

    enum xmlTags
    {
        NONE,
        TERRAIN,
        TEXTURE_ARRAY_FILENAME,
        STATIC_MESH_FILENAME
    };

    // XML TAGS
    const irr::core::stringw terrainTag(L"terrain");
    const irr::core::stringw propertiesTag(L"properties");
    const irr::core::stringw textureArrayFilenameTag(L"textureArrayFilenames");
    const irr::core::stringw staticMeshFilenameTag(L"staticMeshFilenames");

    // create the reader using one of the factory functions
    irr::io::IrrXMLReader* xml = irr::io::createIrrXMLReader(filename.c_str());
    if (xml == nullptr)
    {
        throw std::ios_base::failure("couldn't load map xml-file '" + filename + "'");
    }

    size_t lineNum = 0;

    xmlTags currentTag = xmlTags::NONE;
    while (xml && xml->read())
    {
        lineNum++;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                      "Handling xml line",
                      lineNum,
                      "with NodeType=",
                      xml->getNodeType(),
                      "NodeData=",
                      repr<const char*>(xml->getNodeData()));
        // check the node type
        switch (xml->getNodeType())
        {
            // we found a new element
            case irr::io::EXN_ELEMENT:
            {
                switch (currentTag)
                {
                    case xmlTags::NONE:
                        if (irr::core::stringw(L"mapGenerator").equals_ignore_case(xml->getNodeName()))
                        {
                            irr::f32 version = -1.0;
                            if (StringWParser::parse(xml->getAttributeValueSafe("version"), version, -1.0f) ==
                                ErrCodes::NO_ERR)
                            {
                                if (std::fabs(version - tileMap::MapSystemVersion::version) > 1e-5)
                                {
                                    throw std::invalid_argument(
                                        "file '" + filename + "' was generated using map system version " +
                                        std::to_string(version) + " but the current version is " +
                                        std::to_string(tileMap::MapSystemVersion::version) +
                                        " re-generate the map using newest mapGenerator");
                                }
                            }
                            else
                            {
                                Error::errContinue("couldn't read version out of",
                                                   filename,
                                                   " on line",
                                                   lineNum,
                                                   "map loading might fail unexpectedly");
                            }
                        }
                        // we currently are in the empty -> set our section to terrainTag
                        else if (terrainTag.equals_ignore_case(xml->getNodeName()))
                        {
                            // TODO: get rid of the shotgun parsing and do that beforehand -> this
                            // also sets save defaults
                            if (StringWParser::parse(xml->getAttributeValueSafe("position"),
                                                     loadingParameters.position,
                                                     irr::core::vector3df(0.0f, 0.0f, 0.0f)) != ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't read position out of", filename, ". Assuming position = 0.0, 0.0, 0.0");
                            }
                            if (StringWParser::parse(xml->getAttributeValueSafe("scale"),
                                                     loadingParameters.scale,
                                                     irr::core::vector3df(1.0f, 1.0f, 1.0f)) != ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't read scale out of", filename, ". Assuming scale = 1.0, 1.0, 1.0");
                            }
                            loadingParameters.splatMapFilename =
                                xml->getAttributeValueSafe("splatMapFile");
                            if (loadingParameters.splatMapFilename.equals_ignore_case(L""))
                            {
                                throw std::invalid_argument(
                                    "didn't find 'splatMapFile' in terrain definiton in on line " +
                                    std::to_string(lineNum));
                            }
                            loadingParameters.patchesFile =
                                xml->getAttributeValueSafe("patchesFile");
                            if (loadingParameters.patchesFile.equals_ignore_case(L""))
                            {
                                throw std::invalid_argument(
                                    "didn't find 'patchesFile' in terrain definiton on line" +
                                    std::to_string(lineNum));
                            }
                            loadingParameters.pathMapFile =
                                xml->getAttributeValueSafe("pathMapFile");
                        }
                        else if (textureArrayFilenameTag.equals_ignore_case(xml->getNodeName()))
                        {
                            currentTag = xmlTags::TEXTURE_ARRAY_FILENAME;
                        }
                        else if (staticMeshFilenameTag.equals_ignore_case(xml->getNodeName()))
                        {
                            currentTag = xmlTags::STATIC_MESH_FILENAME;
                        }
                        else
                        {
                            Error::errContinue("unknown xml tag", xml->getNodeName(), "on line", lineNum);
                        }
                        break;
                    case xmlTags::TERRAIN:
                        break; // terrain already loaded on the first tag
                        break;
                    case xmlTags::TEXTURE_ARRAY_FILENAME:
                    {
                        if (irr::core::stringw(xml->getNodeName()).equals_ignore_case(L"file"))
                        {
                            loadingParameters.textureArrayFilenames.push_back(
                                xml->getAttributeValueSafe("filename"));
                            debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                                          "Parsed element 'file' filename=",
                                          repr<const char*>(
                                              xml->getAttributeValueSafe("filename")));
                        }
                        else
                        {
                            Error::errContinue("expected element of type 'file' on line",
                                               lineNum,
                                               "but got",
                                               repr<const char*>(xml->getNodeName()));
                        }
                    }
                    break;
                    case xmlTags::STATIC_MESH_FILENAME:
                    {
                        if (irr::core::stringw(xml->getNodeName()).equals_ignore_case(L"file"))
                        {
                            loadingParameters.staticMeshFilenames.push_back(xml->getAttributeValueSafe("filename"));
                            debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                                          "Parsed element 'file' filename=",
                                          repr<const char*>(
                                              xml->getAttributeValueSafe("filename")));
                        }
                        else
                        {
                            Error::errContinue("expected element of type 'file' on line",
                                               lineNum,
                                               "but got",
                                               repr<const char*>(xml->getNodeName()));
                        }
                    }
                    break;
                } // switch (currentTag)

            } // case irr::io::EXN_ELEMENT:
            break;

            // end of an element
            case irr::io::EXN_ELEMENT_END:
                switch (currentTag)
                {
                    case xmlTags::TERRAIN:
                        break;
                    case xmlTags::TEXTURE_ARRAY_FILENAME:
                        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "end of texture array list on line", lineNum);
                        break;
                    case xmlTags::NONE:
                    default:
                        debugOutLevel(Debug::DebugLevels::secondOrderLoop - 1,
                                      "unhandled currentTag in end-elemet in map xml with id=",
                                      currentTag,
                                      "on line",
                                      lineNum);
                        break;
                }
                // at the end of the section ->reset the tag
                currentTag = xmlTags::NONE;
                break;
            case irr::io::EXN_NONE:
            case irr::io::EXN_TEXT:
            case irr::io::EXN_COMMENT:
            case irr::io::EXN_CDATA:
            case irr::io::EXN_UNKNOWN:
            default:
                debugOutLevel(Debug::DebugLevels::maxLevel,
                              "unhandled xml entry type",
                              static_cast<int>(xml->getNodeType()));
                break;
        } // switch(xml->getNodeType)

    } // while(xml && xml->read())
    if (xml != nullptr)
    {
        delete xml;
    }

    std::unique_ptr<irr::scene::TerrainSceneNode> terrainNode = this->loadTerrain(loadingParameters);

    const auto pathGrid = this->loadPathGrid(loadingParameters);

    debugOutLevel(Debug::DebugLevels::stateInit,
                  "loaded",
                  pathGrid.unitSize,
                  "grid",
                  pathGrid.width,
                  "x",
                  pathGrid.height,
                  "sideLength",
                  "maxID=",
                  pathGrid.maxCellID);

    TerrainShader* const shader = this->addShader(loadingParameters);

    auto meshes = this->loadStaticMeshes(loadingParameters);

    GameMap* gameMap = new GameMap(game, pathGrid, terrainNode, shader);

    return std::make_tuple(loadingParameters, gameMap, meshes);
}

ErrCode MapLoader::saveMap(const std::string filename,
                           MapLoader::MapLoadingParameters& parameters,
                           GameMap& gameMap,
                           const std::vector<entityID_t>& displayOnlyEntitiesToBakeIntoMap,
                           EntityManager& entityManager,
                           Handles::HandleManager& handleManager)
{
    ErrCode code = PathMapXML::savePathMap(gameMap.getPathGrid(),
                                           irr::io::path(parameters.pathMapFile.c_str()),
                                           this->fileSystem);
    if (code != ErrCodes::NO_ERR)
    {
        Error::errContinue("Couldn't save pathmap file", parameters.pathMapFile);
        return code;
    }

    // create static meshes out of entities which will only be displayed
    StaticEntitiesIrrlichtNodeConverter combiner(this->sceneManager);
    auto meshes =
        combiner.generateMeshFromDisplayOnlyEntities(displayOnlyEntitiesToBakeIntoMap, entityManager, handleManager);

    parameters.staticMeshFilenames.clear();

    auto* const meshWriter = this->sceneManager->createMeshWriter(irr::scene::EMESH_WRITER_TYPE::EMWT_OBJ);
    for (size_t i = 0; i < meshes.size(); i++)
    {
        const irr::io::path mapFileNameWithoutExtension =
            this->fileSystem->getFileBasename(filename.c_str(), false);
        const irr::io::path meshFileName = irr::io::path("media/map/") + mapFileNameWithoutExtension +
            irr::io::path("/static_") + irr::io::path(std::to_string(i).c_str()) + ".obj";
        parameters.staticMeshFilenames.push_back(meshFileName);
        auto* const file = this->sceneManager->getFileSystem()->createAndWriteFile(meshFileName);
        if (file == nullptr)
        {
            Error::errTerminate("couldn't write to file", meshFileName);
        }
        meshWriter->writeMesh(file, meshes[i]);
    }
    meshWriter->drop();

    irr::io::IXMLWriter* xwriter = this->fileSystem->createXMLWriter(filename.c_str());

    if (!xwriter)
    {
        return ErrCode(1);
    }

    // write out the obligatory xml header. Each xml-file needs to have exactly one of those.
    xwriter->writeXMLHeader();
    xwriter->writeComment(L"(encoding='UTF-32')");
    xwriter->writeLineBreak();

    // write element mapGenerator version
    xwriter->writeElement(L"mapGenerator",
                          true,
                          L"version",
                          irr::core::stringw(tileMap::MapSystemVersion::version).c_str());
    xwriter->writeLineBreak();

    // start element terrain
    xwriter->writeLineBreak();

    // start section with terrain settings
    xwriter->writeElement(L"terrain",
                          true,
                          L"position",
                          StringWParser::getParsableString(gameMap.getTerrainNode()->getPosition()).c_str(),
                          L"scale",
                          StringWParser::getParsableString(gameMap.getTerrainNode()->getScale()).c_str(),
                          L"splatMapFile",
                          parameters.splatMapFilename.c_str(),
                          L"pathMapFile",
                          parameters.pathMapFile.c_str(),
                          L"patchesFile",
                          parameters.patchesFile.c_str());
    xwriter->writeLineBreak();
    xwriter->writeLineBreak();

    xwriter->writeElement(L"textureArrayFilenames");
    xwriter->writeLineBreak();

    for (unsigned int i = 0; i < parameters.textureArrayFilenames.size(); i++)
    {
        xwriter->writeElement(L"file",
                              true,
                              L"filename",
                              irr::core::stringw(parameters.textureArrayFilenames[i]).c_str());
        xwriter->writeLineBreak();
    }

    xwriter->writeClosingTag(L"textureArrayFilenames");
    xwriter->writeLineBreak();

    xwriter->writeElement(L"staticMeshFilenames");
    xwriter->writeLineBreak();

    for (size_t i = 0; i < meshes.size(); i++)
    {
        const auto fileName = parameters.staticMeshFilenames[i];
        xwriter->writeElement(L"file", true, L"filename", irr::core::stringw(fileName).c_str());
        xwriter->writeLineBreak();
    }

    xwriter->writeClosingTag(L"staticMeshFilenames");
    xwriter->writeLineBreak();

    // delete xml writer
    xwriter->drop();

    return ErrCode(0);
}

std::unique_ptr<irr::scene::TerrainSceneNode> MapLoader::loadTerrain(const MapLoadingParameters& loadingParameters)
{
    std::unique_ptr<irr::scene::TerrainSceneNode> terrain(
        new irr::scene::TerrainSceneNode(nullptr,
                                         this->sceneManager,
                                         this->fileSystem,
                                         0,
                                         4,
                                         irr::scene::ETPS_17,
                                         loadingParameters.position,
                                         loadingParameters.rotation,
                                         loadingParameters.scale));

    if (not terrain->loadSpecialMap(loadingParameters.patchesFile))
    {
        throw std::invalid_argument(
            "the TerrainSceneNode couldn't load the patches from file " +
            std::string(irr::core::stringc(loadingParameters.patchesFile).c_str()) + "correlctly!");
    }

    terrain->setMaterialFlag(irr::video::EMF_LIGHTING, true);
    irr::scene::TerrainTriangleSelector* terrainTriangleSelector =
        new irr::scene::TerrainTriangleSelector(terrain.get(), 1);
    terrain->setTriangleSelector(terrainTriangleSelector);
    terrainTriangleSelector->drop();

    constexpr irr::s32 LOD = 1;
    irr::scene::TerrainVertexSelector* terrainVertexSelector =
        new irr::scene::TerrainVertexSelector(terrain.get(), LOD);
    terrain->setVertexSelector(terrainVertexSelector);

    return terrain;
}


PathGrid MapLoader::loadPathGrid(const MapLoadingParameters& loadingParameters)
{
    PathGrid pathGrid;

    if (loadingParameters.pathMapFile.equals_ignore_case(L""))
    {
        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                      "no 'pathMapFile' found in map "
                      "definition xml -> all pathfinding will "
                      "work to every point");
    }
    else if (PathMapXML::loadPathMap(loadingParameters.pathMapFile, pathGrid) != ErrCodes::NO_ERR)
    {
        Error::errContinue("couldn't load 'pathMapFile'",
                           repr<irr::io::path>(loadingParameters.pathMapFile),
                           "pathfinding likely doesn't function");
    }

    return pathGrid;
}

TerrainShader* MapLoader::addShader(const MapLoadingParameters& loadingParameters)
{
#if defined(MAKE_SERVER_) /** Just for the Server **/
    return nullptr;       /** Nothing to do for the Server **/
#else                     /** Just for the Client **/

    std::unique_ptr<irr::video::ITexture> texArray(this->createTextureArray(loadingParameters.textureArrayFilenames));
    if (!texArray)
    {
        throw std::invalid_argument("couldn't create texture array!");
    }

    // Use this to avoid problems with lock and unlock - can be removed when we don't need to edit
    // the map in the mapeditor
    const bool oldFlag = this->videoDriver->getTextureCreationFlag(irr::video::ETCF_ALLOW_MEMORY_COPY);
    this->videoDriver->setTextureCreationFlag(irr::video::ETCF_ALLOW_MEMORY_COPY,
                                              loadingParameters.allowSplatMapModification);
    irr::video::ITexture* const splatMap = this->videoDriver->getTexture(loadingParameters.splatMapFilename);
    irr::video::ITexture* const blendMap =
        this->videoDriver->getTexture("media/map/splat_blend_test.png");
    this->videoDriver->setTextureCreationFlag(irr::video::ETCF_ALLOW_MEMORY_COPY, oldFlag);

    if (!blendMap)
    {
        throw std::invalid_argument("couldn't open blending texture!");
    }
    if (!splatMap)
    {
        throw std::invalid_argument(
            "couldn't open splat texture " +
            std::string(irr::core::stringc(loadingParameters.splatMapFilename).c_str()));
    }

    std::unique_ptr<TerrainShader> shader(
        new TerrainShader(this->videoDriver, splatMap, texArray.get(), texArray.get(), blendMap)); // TODO check texArray == bumpTextureArray
    if (shader->loadShaders() != ErrCodes::NO_ERR)
    {
        throw std::invalid_argument("couldn't load shaders for the terrain.!");
    }

    texArray.release();
    return shader.release();
#endif
}

irr::video::ITexture* MapLoader::createTextureArray(std::vector<irr::core::stringw> textureArrayFilenames)
{
    debugOut("createTextureArray with size ", textureArrayFilenames.size());
    irr::core::array<irr::video::IImage*> imageArray;
    for (size_t i = 0; i < textureArrayFilenames.size(); ++i)
    {
        irr::video::IImage* image = videoDriver->createImageFromFile(textureArrayFilenames[i]);
        if (image == nullptr)
        {
            Error::errContinue("couldn't load texture", repr<irr::core::stringw>(textureArrayFilenames[i]));
            continue;
        }
#pragma warning(disable : 4996)
        if (image->isCompressed())
#pragma warning(default : 4996)
        {
            Error::errContinue("irrlicht doesn't support decompressing texture",
                               repr<irr::core::stringw>(textureArrayFilenames[i]));
            continue;
        }
        imageArray.push_back(image);
    }
    // TODO: when its finally working make sure to drop all loaded images after creating the
    // textureArray
    debugOut("created Imagearray with size ", imageArray.size());

    debugOutLevel(Debug::DebugLevels::stateInit + 1,
                  "creating TexArray:\n Size of imageArray:\t",
                  imageArray.size());

    irr::video::ITexture* const textureArray = videoDriver->addTextureArray("textureArray", imageArray);

    for (size_t i = 0; i < imageArray.size(); i++)
    {
        imageArray[i]->drop();
    }

    return textureArray;
}

std::vector<irr::scene::IAnimatedMeshSceneNode*> MapLoader::loadStaticMeshes(const MapLoadingParameters& loadingParameters)
{
    std::vector<irr::scene::IAnimatedMeshSceneNode*> nodes;
    for (const auto fname : loadingParameters.staticMeshFilenames)
    {
        auto* const mesh = this->sceneManager->getMesh(fname);
        if (mesh == nullptr)
        {
            for (auto& node : nodes)
            {
                node->remove();
            }
            throw std::ios_base::failure("couldn't load mesh " + std::string(fname.c_str()));
        }
        else
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop, "adding mesh from file", fname);
            nodes.push_back(this->sceneManager->addAnimatedMeshSceneNode(mesh));
        }
    }
    return nodes;
}
