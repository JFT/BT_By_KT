/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef STATICENTITIESIRRLICHTNODECONVERTER_H
#define STATICENTITIESIRRLICHTNODECONVERTER_H

#include <vector>
#include "../ecs/entity.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}
namespace irr
{
    namespace scene
    {
        class ISceneManager;
        class IMesh;
    } // namespace scene
} // namespace irr

class StaticEntitiesIrrlichtNodeConverter
{
   public:
    explicit StaticEntitiesIrrlichtNodeConverter(irr::scene::ISceneManager* const sceneManager);

    std::vector<irr::scene::IMesh*>
    generateMeshFromDisplayOnlyEntities(const std::vector<entityID_t>& displayOnlyEntities,
                                        EntityManager& entityManager,
                                        Handles::HandleManager& handleManager);

   private:
    irr::scene::ISceneManager* sceneManager;
};

#endif /* ifndef STATICENTITIESIRRLICHTNODECONVERTER_H */
