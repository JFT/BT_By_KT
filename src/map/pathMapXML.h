/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PATHMAPXML_H
#define PATHMAPXML_H

#include <irrlicht/path.h> // can't be forward declared because path is a typedef

#include "../utils/stringwstream.h" // defines operator<< for stringw into a stream. Needed by error output. Otherwise this file doesn't compile with clang. Clang also won't compile if this include comes after '#include error.h'
#include "../utils/static/error.h"  // defines ErrCode

// forward declarations
struct PathGrid;
namespace irr
{
    namespace io
    {
        class IFileSystem;
    }
} // namespace irr


class PathMapXML
{
   public:
    static ErrCode loadPathMap(const irr::io::path& filename, PathGrid& loadedPathGrid);
    static ErrCode
    savePathMap(const PathGrid& pathGrid, const irr::io::path& filename, irr::io::IFileSystem* const fileSystem);
};

#endif // PATHMAPXML_H
