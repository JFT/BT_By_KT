#pragma once

#ifndef MAKE_SERVER_
#include "soundEngine.h"

#include "../scripting/scriptBindingHelper.hpp"

#include <sol.hpp>

bool createAudioBindings(sol::table& btTable)
{
    auto audio_table = btTable.create_named("audio");
    audio_table.new_usertype<sf::Music>("MusicSource",
                                        "new",
                                        sol::no_constructor,
                                        SOL_FUN(sf::Music, play),
                                        SOL_FUN(sf::Music, pause),
                                        SOL_FUN(sf::Music, stop));

    audio_table.new_usertype<BT::audio::SoundEngine>(
        "SoundEngine",
        "new",
        sol::no_constructor,
        SOL_FUN(BT::audio::SoundEngine, setGlobalVolume),
        SOL_FUN(BT::audio::SoundEngine, setMusicVolume),
        SOL_FUN(BT::audio::SoundEngine, setEffectsVolume),
        SOL_FUN(BT::audio::SoundEngine, playMusic),
        SOL_FUN(BT::audio::SoundEngine, isCurrentlyPlaying),
        SOL_FUN(BT::audio::SoundEngine, getMainMusicSource),
        SOL_FUN(BT::audio::SoundEngine, getMusicSource),
        SOL_FUN(BT::audio::SoundEngine, getSoundSource),
        SOL_FUN(BT::audio::SoundEngine, getListenerPosition),
        SOL_FUN(BT::audio::SoundEngine, setListenerUpVector),
        SOL_FUN(BT::audio::SoundEngine, setListenerDirection),
        "setListenerPosition",
        sol::overload(static_cast<void (BT::audio::SoundEngine::*)(float, float, float)>(
                          &BT::audio::SoundEngine::setListenerPosition),
                      static_cast<void (BT::audio::SoundEngine::*)(const irr::core::vector3df&)>(
                          &BT::audio::SoundEngine::setListenerPosition)));

    return true;
}
#endif