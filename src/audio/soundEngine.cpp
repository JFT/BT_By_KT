/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2018 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "soundEngine.h"
#include <algorithm>
#include <stdexcept>
#include "../utils/Pow2Assert.h"
#include "../utils/debug.h"
#include "../utils/exceptions/ioExceptions.hpp"

namespace BT
{
    template <typename T>
    const T clamp(const T val, const T max, const T min)
    {
        if (val > max)
            return max;
        else if (val < min)
            return min;
        else
            return val;
    }

    namespace audio
    {
        SoundEngine::SoundEngine()
        {
            mainMusicSource.setAttenuation(0);
            createdMusicObjects.push_back(&mainMusicSource);
        }

        SoundEngine::~SoundEngine() {}

        void SoundEngine::setGlobalVolume(const float volume)
        {
            sf::Listener::setGlobalVolume(clamp(volume, 100.f, 0.f));
        }

        void SoundEngine::setMusicVolume(const float volume)
        {
            musicVolume = clamp(volume, 100.f, 0.f);
            for (auto& musicObj : createdMusicObjects)
            {
                musicObj->setVolume(musicVolume);
            }
        }

        void SoundEngine::setEffectsVolume(const float volume)
        {
            effectsVolume = clamp(volume, 100.f, 0.f);
            for (auto& soundObj : createdSoundObjects)
            {
                soundObj->setVolume(effectsVolume);
            }
        }

        void SoundEngine::playMusic(const std::string& filename)
        {
            if (!mainMusicSource.openFromFile(filename))
            {
                std::string errorStr("Could not open File: ");
                errorStr += filename;
                throw IOEXCEPTION(errorStr.c_str());
            }
            currentlyPlayedTitle = filename;
            mainMusicSource.play();
        }

        bool SoundEngine::isCurrentlyPlaying(const std::string& filename)
        {
            return (filename == currentlyPlayedTitle) &&
                (mainMusicSource.getStatus() == sf::SoundSource::Playing);
        }

        sf::Vector3f SoundEngine::getSFVector(const irr::core::vector3df& vect)
        {
            return sf::Vector3f(vect.X, vect.Y, vect.Z);
        }

        irr::core::vector3df SoundEngine::getIrrVector(const sf::Vector3f& vect)
        {
            return irr::core::vector3df(vect.x, vect.y, vect.z);
        }

        sf::Music& SoundEngine::getMainMusicSource() { return mainMusicSource; }

        sf::Music& SoundEngine::getMusicSource()
        {
            for (auto& musicSource : createdMusicObjects)
            {
                if (musicSource->getStatus() == sf::SoundSource::Stopped)
                {
                    return *musicSource;
                }
            }

            createdMusicObjects.emplace_back();
            return *(createdMusicObjects.back());
        }

        sf::Sound& SoundEngine::getSoundSource()
        {
            for (auto& soundSource : createdSoundObjects)
            {
                if (soundSource->getStatus() == sf::SoundSource::Stopped)
                {
                    return *soundSource;
                }
            }

            createdSoundObjects.emplace_back();
            return *(createdSoundObjects.back());
        }

        void SoundEngine::setListenerPosition(float x, float y, float z)
        {
            sf::Listener::setPosition(x, y, z);
        }

        void SoundEngine::setListenerPosition(const irr::core::vector3df& pos)
        {
            sf::Listener::setPosition(getSFVector(pos));
        }

        irr::core::vector3df SoundEngine::getListenerPosition()
        {
            return getIrrVector(sf::Listener::getPosition());
        }

        void SoundEngine::setListenerDirection(const irr::core::vector3df& dir)
        {
            sf::Listener::setDirection(getSFVector(dir));
        }

        void SoundEngine::setListenerUpVector(const irr::core::vector3df& upVect)
        {
            sf::Listener::setUpVector(getSFVector(upVect));
        }

    } // namespace audio
} // namespace BT