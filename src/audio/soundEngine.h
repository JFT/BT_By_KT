/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2018 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <SFML/Audio.hpp>
#include <irrlicht/vector3d.h>
#include <string>
#include <vector>

namespace BT
{
    namespace audio
    {
        class SoundEngine
        {
           public:
            SoundEngine();

            ~SoundEngine();

            void setGlobalVolume(const float volume);

            void setMusicVolume(const float volume);

            void setEffectsVolume(const float volume);

            void playMusic(const std::string& filename);

            bool isCurrentlyPlaying(const std::string& filename);

            sf::Music& getMainMusicSource();
            ///
            ///@brief Get a Music Source object reference
            /// Important Note: Only use getMusicSource when you intend
            ///                to play something with it. In SFML a maximum of 256
            ///                existing soundsources is allowed. So when a Source is in
            ///                the state: 'stopped' it will be reused.
            ///@return sf::Music& - a music source in the state 'stopped'
            ///
            sf::Music& getMusicSource();


            ///
            ///@brief Get a Sound Source object reference
            /// Important Note: Only use getSoundSource when you intend to play a sound.
            ///                In SFML a maximum of 256 existing soundsources is allowed.
            ///                So when a Source is in the state: 'stopped'
            ///                it will be reused.
            ///@return sf::Sound& - a sound source in the state 'stopped'
            ///
            sf::Sound& getSoundSource();

            // Listener Properties

            ///
            ///@brief Set the Listener Position in the 3D World
            ///       Default Position is (0,0,0)
            ///@param x X Coordinate
            ///@param y Y Coordinate
            ///@param z Z Coordinate
            ///
            void setListenerPosition(float x, float y, float z);

            void setListenerPosition(const irr::core::vector3df& pos);

            irr::core::vector3df getListenerPosition();

            ///
            ///@brief Set the Listener Direction
            ///       Together with the Listener UpVector you can manipulate
            ///       the listening direction in the 3D World
            ///@param dir - direction of the listener
            ///
            void setListenerDirection(const irr::core::vector3df& dir);

            ///
            ///@brief Set the Listener Up Vector object
            ///       Together with the Listener UpVector you can manipulate
            ///       the listening direction in the 3D World
            ///@param upVect - up vector of the listener
            ///
            void setListenerUpVector(const irr::core::vector3df& upVect);

           private:
            sf::Vector3f getSFVector(const irr::core::vector3df& vect);
            irr::core::vector3df getIrrVector(const sf::Vector3f& vect);
            float musicVolume = 100.f;
            float effectsVolume = 100.f;
            // We can create a max of 256 Audio Sources but we already use
            // one as our main music source. So max is 255 now
            static constexpr size_t MAX_AUDIO_OBJECTS = 255;
            std::vector<sf::Sound*> createdSoundObjects;
            std::vector<sf::Music*> createdMusicObjects;

            sf::Music mainMusicSource;

            std::string currentlyPlayedTitle;
        };


    } // namespace audio
} // namespace BT