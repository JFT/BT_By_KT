/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../utils/stringwstream.h" // stringwstream.h needs to be included before debug.h and error.h (which are included by examplestate.h) because they use use operator<< on a stringw

#include "testLoadingState.h"
#include "../graphics/graphicsPipeline.h"

#include <CEGUI/RendererModules/Irrlicht/Renderer.h>
#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/IMeshSceneNode.h>
#include <irrlicht/ISceneCollisionManager.h>
#include <irrlicht/ISceneManager.h>

#include "../utils/stringwstream.h" // should be included first because it defines operator<< for strigw which other files need
#include "../core/game.h"
#include "../core/rtscameraanimator.h"
#include "../ecs/componentParsers.h"
#include "../ecs/componentSerializer.h"
#include "../ecs/components.h"
#include "../ecs/entityBlueprint.h"
#include "../ecs/entityFactory.h"
#include "../ecs/events/eventManager.h"
#include "../ecs/events/events.h"
#include "../ecs/events/networkEventSyncronizationClientStub.h"
#include "../ecs/savingLoading/savingLoading.h"
#include "../ecs/storageSystems/allEntitiesComponentValueStorage.h"
#include "../ecs/systems/automaticWeaponSystem.h"
#include "../ecs/systems/cooldownSystem.h"
#include "../ecs/systems/damageSystem.h"
#include "../ecs/systems/graphicSystem.h"
#include "../ecs/systems/guiSystems/healthBarSystem.h"
#include "../ecs/systems/inventorySystem.h"
#include "../ecs/systems/moneyTransferSystem.h"
#include "../ecs/systems/movement/movementSystem.h"
#include "../ecs/systems/projectileSystem.h"
#include "../ecs/systems/selectionSystem.h"
#include "../ecs/systems/shopSystem.h"
#include "../ecs/systems/testSystem.h"
#include "../ecs/userinput/inputContextManager.h"
#include "../graphics/particles/particleSystemGPU.h"
#include "../grid/grid.h"
#include "../gui/ceGuiEnvironment.h"
#include "../map/gamemap.h"
#include "../map/mapLoader.h"
#include "../scripting/scriptingModule.h"
#include "../utils/HandleManager.h"
#include "../utils/fgaFileLoader.h"

testLoadingState::testLoadingState(Game& game_)
    : State(game_)
    , handleManager()
    , inputContextManager(new InputContextManager())
{
    // ctor
}

testLoadingState::~testLoadingState()
{
    // before adding anything to be freed on destruction consider adding it in
    // testLoadingState::onLeave() because most
    // of the memory should be freed on state change instead of on destruction (as states
    // exist in memory much longer than they are active)
}

void testLoadingState::onEnter()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Entering testLoadingState");

    game.getGraphics()->init();
    // THIS stops the driver from using mipmaps -> no seams between the tiles
    // this->game.getVideoDriver()->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);


    MapLoader mapLoader(this->game.getSceneManager(),
                        this->game.getDevice()->getFileSystem(),
                        this->game.getVideoDriver(),
                        this->game);
    const irr::core::stringc mapFile("data/map/map_small.xml"); // TODO use map.xml
    const auto loaded = mapLoader.loadMap(mapFile.c_str());
    this->gameMap = std::get<1>(loaded);
    game.getGraphics()->addNodeToStage(gameMap->getTerrainNode(), RenderStageType::STATIC);
    game.getGraphics()->addNodeToStage(gameMap->getTerrainNode(), RenderStageType::LIGHTING);

    //set some ambientlight //note r,g,b is color - alpha is power of ambient light not real alpha
    game.getGraphics()->getLighting().setAmbientColor(irr::video::SColorf(1.f, 1.f, 1.f, 0.2f).toSColor());
    // TODO: undo this, only for testing
    // make the map fully walkable (by resizing it to the same size which clears the map)
    // gameMap->getPathGrid().resize(gameMap->getPathGrid().width, gameMap->getPathGrid().height);
    gameMap->generateDebugCubes(false);

    // create Grid
    this->grid = new grid::Grid(this->gameMap->getMapSize(), 1024.0f);

    // needed this->gameMap
    this->initCamera();

    this->myPlayerProfileID = 0; // this->game.getPlayerCollectionReplica()->mySlotID();

    this->initEntitySystem();

    this->initLuaScriptEngine();


    debugOut("test");

    this->eventSyncronization =
        new NetworkEventSyncronizationClientStub(*this->eventManager, *this->game.getThreadPool());

    // ParticleSystemTest#
    ParticleEmitterGPU* testEmitterGPU = this->game.getGraphics()->getParticleSystemGPU()->createEmitter(
        irr::core::vector3df(1200, 250, 1000), 0.1f, 1 << 15);
    testEmitterGPU->setTexture(
        this->game.getVideoDriver()->getTexture("media/particles/water.bmp"));
    testEmitterGPU->addVectorField(this->game.getGraphics()->getParticleSystemGPU()->getVectorField(
        "media/particles/vectorFieldTest.fga"));
    testEmitterGPU->setSpawnCuboid(100.f, 100.f, 100.f);
    irr::scene::ISceneNodeAnimator* partAnim = this->game.getSceneManager()->createFlyCircleAnimator(
        irr::core::vector3df(2000, 750, 2000), 200.f, 0.001f, irr::core::vector3df(0, 1.f, 0));

    testEmitterGPU->addAnimator(partAnim);
    partAnim->drop();


    irr::scene::IParticleSystemSceneNode* ps = this->game.getSceneManager()->addParticleSystemSceneNode(false);
    this->addedSceneNodesToRemove.push_back(ps);

    irr::scene::IParticleEmitter* em =
        ps->createBoxEmitter(irr::core::aabbox3d<irr::f32>(-7, 0, -7, 7, 1, 7), // emitter size
                             irr::core::vector3df(0.0f, 0.06f, 0.0f),           // initial direction
                             80,
                             100,                                  // emit rate
                             irr::video::SColor(0, 255, 255, 255), // darkest color
                             irr::video::SColor(0, 255, 255, 255), // brightest color
                             800,
                             2000,
                             0,                                    // min and max age, angle
                             irr::core::dimension2df(10.f, 10.f),  // min size
                             irr::core::dimension2df(20.f, 20.f)); // max size

    ps->setEmitter(em); // this grabs the emitter
    em->drop();         // so we can drop it here without deleting it

    irr::scene::IParticleAffector* paf = ps->createFadeOutParticleAffector();

    ps->addAffector(paf); // same goes for the affector
    paf->drop();

    ps->setPosition(irr::core::vector3df(1200, 250, 1000));
    ps->setScale(irr::core::vector3df(2, 2, 2));
    ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
    ps->setMaterialTexture(0, this->game.getVideoDriver()->getTexture("media/particles/fire.bmp"));
    ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
    game.getGraphics()->addNodeToStage(ps, RenderStageType::PARTICLE);

    SavingLoading::SaveLoadDependencies dependencies;
    dependencies.sceneManager = this->game.getSceneManager();
    SavingLoading::loadEntitySystem("quicksave.save", *this->entityManager, this->handleManager, dependencies);

    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "testLoadingState is initialized");
}

void testLoadingState::onLeave()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Leaving testLoadingState");

    for (auto& node : this->addedSceneNodesToRemove)
    {
        node->remove();
    }
    this->addedSceneNodesToRemove.clear();

    // TODO: Add drop() methods for gamemap and drop all objects created in onEnter()
    delete this->gameMap;
    this->gameMap = nullptr;
    delete this->grid;
    this->grid = nullptr;

    // must delete the blueprintCollection before the entityManager because
    // a blueprint deletes its components values upon destruction
    // (for which they need the entityManager)
    this->blueprintCollection_testing.clear();
    this->blueprintNameToIDMap.clear();
    this->blueprintCollection.clear();

    delete this->entityManager;
    this->entityManager = nullptr;
    delete this->eventManager;
    this->eventManager = nullptr;

    this->movementSystem = nullptr;
    this->graphicSystem = nullptr;
    this->selectionSystem = nullptr;
    this->projectileSystem = nullptr;
    this->cooldownSystem = nullptr;
    this->damageSystem = nullptr;
    this->automaticWeaponSystem = nullptr;
    this->shopSystem = nullptr;
    this->inventorySystem = nullptr;
    this->moneyTransferSystem = nullptr;
    this->healthBarSystem = nullptr;
    this->testSystem = nullptr;

    delete this->eventSyncronization;
    this->eventSyncronization = nullptr;

    this->game.getCamera()->removeAnimator(this->rtsanim);
    this->game.getGraphics()->clear(); // Clear graphicspipeline last, to drop() remaining nodes
                                       // that were not cleared by other calls.
}

void testLoadingState::update()
{
    // std::exit(4);

    this->eventSyncronization->throwServerAckedEvents();

    debugOutLevel(Debug::DebugLevels::updateLoop + 1, "Updating ec-Systems");

    this->entityManager->updateSystems(0,
                                       this->game.getScriptingModule()->getScriptEngine(),
                                       this->game.getDeltaTime());
}

bool testLoadingState::onEvent(const irr::SEvent& event)
{
    this->game.getSceneManager()->getActiveCamera()->OnEvent(event);
    if (this->game.getGuiEnvironment()->getRenderer()->injectEvent(event))
    {
        // todo: a gui event should be signaled to the eventSystem as well --> find a general and
        // good way to pass gui input

        return true;
    }

    if (irr::EET_MOUSE_INPUT_EVENT == event.EventType)
    {
        switch (event.MouseInput.Event)
        {
            case irr::EMIE_RMOUSE_PRESSED_DOWN:
            {
                // setTarget(game);
                return true;
            }
            break;
        }
    }

    if (event.EventType == irr::EET_KEY_INPUT_EVENT)
    {
        if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_W && !event.KeyInput.PressedDown)
        {
            gameMap->getTerrainNode()->setMaterialFlag(irr::video::EMF_WIREFRAME,
                                                       !gameMap->getTerrainNode()->getMaterial(0).Wireframe);
            return true;
        }
        else if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_D && !event.KeyInput.PressedDown)
        {
            gameMap->changeDebugCubesVisiblity();
            return true;
        }
        else if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_A && !event.KeyInput.PressedDown)
        {
            if (gameMap->getTerrainNode()->isDebugDataVisible())
            {
                gameMap->getTerrainNode()->setDebugDataVisible(irr::scene::E_DEBUG_SCENE_TYPE::EDS_OFF);
            }
            else
            {
                gameMap->getTerrainNode()->setDebugDataVisible(irr::scene::E_DEBUG_SCENE_TYPE::EDS_FULL);
            }
            return true;
        }
        else if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_P && !event.KeyInput.PressedDown)
        {
            if (selectedNode)
            {
            }
        }
        else if (event.KeyInput.Key == irr::KEY_F5 && !event.KeyInput.PressedDown)
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Pressed KEY F5 -> saving game");
            SavingLoading::SaveLoadDependencies dependencies;
            dependencies.sceneManager = this->game.getSceneManager();
            SavingLoading::saveEntitySystem("quicksave.save", *this->entityManager, this->handleManager, dependencies);
            return true;
        }
        else if (event.KeyInput.Key == irr::KEY_F9 && !event.KeyInput.PressedDown)
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Pressed KEY F9 -> loading game");
            this->game.changeState(Game::States_e::testLoading);
            return true;
        }
    }
    return false;
}

void testLoadingState::selectNode()
{
    irr::core::line3df ray = this->game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
        this->game.getDevice()->getCursorControl()->getPosition(),
        this->game.getSceneManager()->getActiveCamera());
    irr::scene::ISceneNode* node =
        this->game.getSceneManager()->getSceneCollisionManager()->getSceneNodeFromRayBB(ray, 1);

    if (node != nullptr)
    {
        printf("NODE SELECT ID:%i\n", int(node->getID()));

        if (node->getID() == 1)
        {
            node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
            selectedNode = static_cast<irr::scene::IAnimatedMeshSceneNode*>(node);
        }
        else if (node->getID() == -1)
        {
            if (selectedNode)
            {
                selectedNode->setMaterialFlag(irr::video::EMF_LIGHTING, true);
                selectedNode = nullptr;
            }
        }

        if (selectedNode)
        {
            printf("selected\n");
            selectedNode->setMD2Animation(irr::scene::EMAT_SALUTE);
        }
    }
    else
    {
        printf("NO NODE Selected\n");
        if (selectedNode)
        {
            selectedNode->setMaterialFlag(irr::video::EMF_LIGHTING, true);
        }

        selectedNode = nullptr;
        // lastnode->setMaterialFlag(irr::video::EMF_LIGHTING, true);
    }
}

void testLoadingState::initCamera()
{
    this->rtsanim =
        new RTSCameraAnimator(this->game.getDevice()->getCursorControl(), 1.0f, 10.0f, 30.0f, 2000.0f);

    this->rtsanim->setBounds(
        irr::core::vector2df(this->gameMap->getMapPosition().X,
                             this->gameMap->getMapSize().X + this->gameMap->getMapPosition().X),
        irr::core::vector2df(this->gameMap->getMapPosition().Z,
                             this->gameMap->getMapSize().Y + this->gameMap->getMapPosition().Z));
    this->game.getCamera()->setPosition(irr::core::vector3df(0, 600, 300));
    this->game.getCamera()->setTarget(irr::core::vector3df(0, 0, 0));
    this->game.getCamera()->addAnimator(this->rtsanim);
    this->game.getSceneManager()->setActiveCamera(this->game.getCamera());
    this->rtsanim->drop();

    this->game.getSceneManager()->setAmbientLight(irr::video::SColorf(0.6f, 0.6f, 0.6f));
}

void testLoadingState::initEntitySystem()
{
    this->entityManager = new EntityManager();

    // Init components parsing and memory management
    std::map<componentID_t, ComponentParsing::IComponentParser*> parsers;

    // Components::DebugColor
    parsers[Components::ComponentID<Components::DebugColor>::CID] =
        new ComponentParsing::GenericComponentParser<Components::DebugColor>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::DebugColor>(this->handleManager)));
    // this->entityManager->defineComponent(debugColorVS);

    // Components::Position
    parsers[Components::ComponentID<Components::Position>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Position>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Position>(this->handleManager)));

    // Components::Movement
    parsers[Components::ComponentID<Components::Movement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Movement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Movement>(this->handleManager)));

    // Components::Graphic
    parsers[Components::ComponentID<Components::Graphic>::CID] =
        new ComponentParsing::GraphicComponentParser<Components::Graphic>(this->handleManager,
                                                                          this->game.getSceneManager());
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Graphic>(this->handleManager)));

    // Components::ProjectileMovement
    parsers[Components::ComponentID<Components::ProjectileMovement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::ProjectileMovement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::ProjectileMovement>(this->handleManager)));

    // Components::Rotation
    parsers[Components::ComponentID<Components::Rotation>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Rotation>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Rotation>(this->handleManager)));

    // Components::EntityName
    parsers[Components::ComponentID<Components::EntityName>::CID] =
        new ComponentParsing::GenericComponentParser<Components::EntityName>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::EntityName>(this->handleManager)));

    // Components::Price
    parsers[Components::ComponentID<Components::Price>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Price>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Price>(this->handleManager)));

    // Components::Damage
    parsers[Components::ComponentID<Components::Damage>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Damage>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Damage>(this->handleManager)));

    // Components::Range
    parsers[Components::ComponentID<Components::Range>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Range>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Range>(this->handleManager)));

    // Components::Ammo
    parsers[Components::ComponentID<Components::Ammo>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::Ammo>(this->handleManager,
                                                                           this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Ammo>(this->handleManager)));

    // Components::Icon
    parsers[Components::ComponentID<Components::Icon>::CID] =
        new ComponentParsing::GraphicComponentParser<Components::Icon>(this->handleManager,
                                                                       this->game.getSceneManager());
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Icon>(this->handleManager)));

    // Components::Inventory
    parsers[Components::ComponentID<Components::Inventory>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Inventory>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Inventory>(this->handleManager)));

    // Components::PurchasableItems
    parsers[Components::ComponentID<Components::PurchasableItems>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::PurchasableItems>(this->handleManager,
                                                                                       this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PurchasableItems>(this->handleManager)));

    // Components::Requirement
    parsers[Components::ComponentID<Components::Requirement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Requirement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Requirement>(this->handleManager)));

    // Components::SelectionID
    parsers[Components::ComponentID<Components::SelectionID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::SelectionID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::SelectionID>(this->handleManager)));

    // Components::Cooldown
    parsers[Components::ComponentID<Components::Cooldown>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Cooldown>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Cooldown>(this->handleManager)));

    // Components::Physical
    parsers[Components::ComponentID<Components::Physical>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Physical>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Physical>(this->handleManager)));

    // Components::OwnerEntityID
    parsers[Components::ComponentID<Components::OwnerEntityID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::OwnerEntityID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::OwnerEntityID>(this->handleManager)));

    // Components::WeaponEntityID
    parsers[Components::ComponentID<Components::WeaponEntityID>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::WeaponEntityID>(this->handleManager,
                                                                                     this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::WeaponEntityID>(this->handleManager)));

    // Components::DroppedItemEntityID
    parsers[Components::ComponentID<Components::DroppedItemEntityID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::DroppedItemEntityID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::DroppedItemEntityID>(this->handleManager)));

    // Components::PlayerStats
    parsers[Components::ComponentID<Components::PlayerStats>::CID] =
        new ComponentParsing::GenericComponentParser<Components::PlayerStats>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PlayerStats>(this->handleManager)));

    // Components::PlayerBase
    parsers[Components::ComponentID<Components::PlayerBase>::CID] =
        new ComponentParsing::GenericComponentParser<Components::PlayerBase>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PlayerBase>(this->handleManager)));

    // create ECS Systems and bind them to components
    this->eventManager = new EventManager();

    this->movementSystem = new MovementSystem(*this->game.getThreadPool(),
                                              *this->eventManager,
                                              *this->entityManager,
                                              this->handleManager,
                                              this->gameMap,
                                              *(this->grid),
                                              this->game);
    std::unique_ptr<SystemBase> movementSystemTransfer(this->movementSystem);
    this->entityManager->bindSystem(movementSystemTransfer);

    this->graphicSystem = new GraphicSystem(*this->game.getThreadPool(),
                                            *this->eventManager,
                                            *this->entityManager,
                                            this->handleManager,
                                            this->gameMap,
                                            *this->grid,
                                            this->game);
    std::unique_ptr<SystemBase> graphicSystemTransfer(this->graphicSystem);
    this->entityManager->bindSystem(graphicSystemTransfer);

    this->selectionSystem = new SelectionSystem(*this->game.getThreadPool(),
                                                *this->eventManager,
                                                *this->entityManager,
                                                this->handleManager,
                                                *this->inputContextManager,
                                                this->gameMap,
                                                *this->grid,
                                                this->game);
    std::unique_ptr<SystemBase> selectionSystemTransfer(this->selectionSystem);
    this->entityManager->bindSystem(selectionSystemTransfer);

    this->projectileSystem = new ProjectileSystem(*this->game.getThreadPool(),
                                                  *this->eventManager,
                                                  *this->entityManager,
                                                  this->handleManager,
                                                  this->gameMap);
    std::unique_ptr<SystemBase> projectileSystemTransfer(this->projectileSystem);
    this->entityManager->bindSystem(projectileSystemTransfer);

    this->cooldownSystem =
        new CooldownSystem(*this->game.getThreadPool(), *this->eventManager, this->handleManager);
    std::unique_ptr<SystemBase> cooldownSystemTransfer(this->cooldownSystem);
    this->entityManager->bindSystem(cooldownSystemTransfer);

    this->damageSystem = new DamageSystem(*this->game.getThreadPool(), *this->eventManager, this->handleManager);
    std::unique_ptr<SystemBase> damageSystemTransfer(this->damageSystem);
    this->entityManager->bindSystem(damageSystemTransfer);

    this->automaticWeaponSystem = new AutomaticWeaponSystem(*this->game.getThreadPool(),
                                                            *this->eventManager,
                                                            *this->entityManager,
                                                            this->handleManager,
                                                            this->blueprintCollection);
    std::unique_ptr<SystemBase> automaticWeaponSystemTransfer(this->automaticWeaponSystem);
    this->entityManager->bindSystem(automaticWeaponSystemTransfer);

    this->shopSystem = new ShopSystem(*this->game.getThreadPool(),
                                      *this->eventManager,
                                      *this->entityManager,
                                      this->handleManager,
                                      this->blueprintCollection);
    std::unique_ptr<SystemBase> shopSystemTransfer(this->shopSystem);
    this->entityManager->bindSystem(shopSystemTransfer);

    this->inventorySystem = new InventorySystem(*this->game.getThreadPool(),
                                                *this->eventManager,
                                                *this->entityManager,
                                                this->handleManager,
                                                this->blueprintCollection);
    std::unique_ptr<SystemBase> inventorySystemTransfer(this->inventorySystem);
    this->entityManager->bindSystem(inventorySystemTransfer);

    this->moneyTransferSystem =
        new MoneyTransferSystem(*this->game.getThreadPool(), *this->eventManager, *this->entityManager, this->handleManager);
    std::unique_ptr<SystemBase> moneyTransferSystemTransfer(this->moneyTransferSystem);
    this->entityManager->bindSystem(moneyTransferSystemTransfer);

    // GUI-ec-Systems
    this->healthBarSystem = new HealthBarSystem(*this->game.getThreadPool(),
                                                *this->eventManager,
                                                *this->entityManager,
                                                this->handleManager,
                                                this->game);
    std::unique_ptr<SystemBase> healthBarSystemTransfer(this->healthBarSystem);
    this->entityManager->bindSystem(healthBarSystemTransfer);

    // Just for testing stuff
    // Always update testSystem at last at the end of all systems, which is why it is bound last
    this->testSystem = new TestSystem(*this->game.getThreadPool(),
                                      *this->eventManager,
                                      *this->entityManager,
                                      this->handleManager,
                                      this->gameMap);
    std::unique_ptr<SystemBase> testSystemTransfer(this->testSystem);
    this->entityManager->bindSystem(testSystemTransfer);

    // Load blueprints
    irr::io::path pre_path = "data/ecs/";
    irr::io::path const dafaultWorkingDirectory =
        this->game.getDevice()->getFileSystem()->getWorkingDirectory().c_str();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory + "/" + pre_path);
    irr::io::IFileList* fileList = this->game.getDevice()->getFileSystem()->createFileList();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory);

    irr::io::path file = "";
    std::string file_ = "";
    irr::io::path extension = "";
    for (unsigned int i = 2; i < fileList->getFileCount(); i++)
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".json")
            {
                file_ = std::string(irr::core::stringc(file).c_str());
                debugOut("Found json-file '", file_, "'");
                entityFactory->registerBlueprintsFromJson(file_, this->blueprintNameToIDMap);
            }
        }
    }
    for (unsigned int i = 2; i < fileList->getFileCount(); i++)
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".json")
            {
                file_ = std::string(irr::core::stringc(file).c_str());
                debugOut("load json-file '", file_, "'");
                entityFactory->createBlueprintsFromJson(file_, *this->entityManager, parsers, this->blueprintCollection);
                debugOut("BlueprintCount: ", this->blueprintCollection.size());
            }
        }
    }

    for (auto& parser : parsers)
    {
        delete parser.second;
    }
}

void testLoadingState::initLuaScriptEngine()
{
    ScriptEngine& scriptEngine = this->game.getScriptingModule()->getScriptEngine();
    // define lua variables
    scriptEngine.loopOverInstances(
        [this](sol::state& instance) {
            // std::ref for non-ptr members
            instance["entityManager"] = std::ref(this->entityManager);
            instance["handleManager"] = std::ref(this->handleManager);
            instance["eventManager"] = std::ref(this->eventManager);
            instance["terrainNode"] = this->gameMap->getTerrainNode();
            instance["grid"] = this->grid;
            instance["testSystem"] = this->testSystem;
        },
        ScriptEngine::ForceLoop::True);

    irr::io::path pre_path = "scripts/";
    irr::io::path const dafaultWorkingDirectory =
        this->game.getDevice()->getFileSystem()->getWorkingDirectory().c_str();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory + "/" + pre_path);
    irr::io::IFileList* fileList = this->game.getDevice()->getFileSystem()->createFileList();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory);

    std::string fileString = "";
    irr::io::path file = "";
    irr::io::path extension = "";
    for (unsigned int i = 2; i < fileList->getFileCount(); i++)
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".lua")
            {
                fileString = std::string(irr::core::stringc(file).c_str());
                debugOut("Load lua-file '", fileString, "'");
                this->game.getScriptingModule()->loadScriptFile(fileString);
            }
        }
    }

    fileList->drop();
}
