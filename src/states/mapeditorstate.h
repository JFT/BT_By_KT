/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAPEDITORSTATE_H
#define MAPEDITORSTATE_H

#include <array>
#include <functional>
#include <map>
#include <unordered_map>
#include <vector>

//#include <irrlicht/IEventReceiver.h>
#include <irrlicht/SColor.h>
#include <irrlicht/aabbox3d.h>
#include <irrlicht/irrArray.h>
#include <irrlicht/irrTypes.h>
#include <irrlicht/path.h>
#include <irrlicht/position2d.h>
#include <irrlicht/triangle3d.h>
#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>
#include "state.h"
#include "../ecs/entity.h" // defines entityID_t
#include "../ecs/entityBlueprint.h"
#include "../map/mapLoader.h"
#include "../mapeditor/modifiableParameters.h"
#include "../utils/HandleManager.h"


// forward declarations
class EntityManager;
class Game;
class GameMap;
class DecalSceneNode;
class MapEditorCameraAnimator;
class MapEditorGui;
class MessageBoxGui;
class EventManager;
class SystemBase;
class TextureDrawer;
class DecalManager;
namespace irr
{
    namespace scene
    {
        class IAnimatedMeshSceneNode;
        class TerrainCollisionManager;
        class ICameraSceneNode;
    } // namespace scene
    namespace video
    {
        class IImage;
    }
} // namespace irr
namespace ComponentParsing
{
    class IComponentParser;
}
namespace grid
{
    class Grid;
}

namespace sol
{
    class state;
}

template <typename T>
class GuiSyncedValue
{
   public:
    GuiSyncedValue()
        : value()
        , callbacks()
    {
    }

    void addUser(void* const user, std::function<void(const T&)> callbackOnChange)
    {
        this->callbacks[user] = callbackOnChange;
    }

    void setFrom(const void* const setter, const T& newValue)
    {
        this->value = newValue;
        for (auto& entry : this->callbacks)
        {
            if (entry.first != setter) // don't inform the setter that the value is changing
            {
                entry.second(this->value);
            }
        }
    }

    const T& get() { return this->value; }

   private:
    T value;
    std::map<void* const, std::function<void(const T&)>> callbacks;
};


class MapEditorState : public State
{
   public:
    explicit MapEditorState(Game& game);
    MapEditorState(const MapEditorState&) = delete;
    virtual ~MapEditorState();

    void onEnter();
    void onLeave();
    void update();

    virtual bool onEvent(const irr::SEvent& event);

    bool loadMap(std::string file_);
    bool saveMap();

    bool updateTextureArray(std::vector<irr::core::stringw> newTextureArray_);

    void setSelectedBlueprint(const entityID_t index);
    void setSelectedEntity(const entityID_t entityID);

    void selectEntity(const entityID_t entityID);
    void deselectSelectedEntity();
    void deleteSelectedEntity();

    void setEditorStateTo(const MAPEDIT_STATE newState);
    MAPEDIT_STATE getEditorState() const { return this->editorState; }
    void switchCameraPosition(const entityID_t entityID);
    void switchCameraPosition(const irr::core::vector3df camTarget_);

    GameMap* getGameMap() const { return this->gameMap; }

    entityID_t createEntityFromBlueprint(const entityID_t bluprintIndex);

    MapEditorState operator=(const MapEditorState&) = delete;

   private:
    sol::state& guiLuaState;
    std::map<componentID_t, ComponentParsing::IComponentParser*> parsers = {};

    void loadGui();
    void initEntitySystem();
    void createMapDependentSystems();

    MapEditorGui* mapEditorGuiObj = nullptr;
    MapLoader* mapLoader = nullptr;
    GameMap* gameMap = nullptr;
    grid::Grid* grid = nullptr;
    irr::io::path loadedMapFile = "";

    irr::io::path basisWorkingDirectory = "";

    MAPEDIT_STATE editorState = MAPEDIT_STATE::MES_DONOTHING;

    void updateEntityGui(const entityID_t entityID);

    entityID_t selectedEntity = ECS::INVALID_ENTITY;
    irr::core::vector2df offsetToSelectedEntity = irr::core::vector2df(irr::f32(0), irr::f32(0));
    struct SelectedBlueprint_s
    {
        SelectedBlueprint_s() = default;
        SelectedBlueprint_s(const entityID_t index_, const std::string name_)
            : index(std::move(index_))
            , name(std::move(name_))
        {
        }

        entityID_t index = ECS::INVALID_ENTITY;
        std::string name = "";
    } selectedBlueprint;

    Handles::HandleManager handleManager;
    EntityManager* entityManager = nullptr;
    EventManager* eventManager = nullptr;

    SystemBase* graphicSystem = nullptr;
    SystemBase* testSystem = nullptr;

    // parser stuff
    std::map<std::string, blueprintID_t> blueprintNameToIDMap = {};
    std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>> blueprintCollection = {};

    /// @brief translates between irrlicht nodes (which are returned when doing colission checks
    /// [e.g. getting an entity clicked on]) and entityIDs
    std::unordered_map<irr::scene::IAnimatedMeshSceneNode*, entityID_t> entityIDFromNode = {};

    void placeEntityAtCursorPosition(const entityID_t entityID);

    /// @brief used to syncronize the position of a selected entity between the MapEditorState and
    /// the MapEditorGui
    GuiSyncedValue<irr::core::vector2df> posSync;
    /// @brief used to syncronize the offset of a selected entity between the MapEditorState and the
    /// MapEditorGui
    GuiSyncedValue<irr::f32> offsetSync;
    /// @brief used to syncronize the rotation of a selected entity between the MapEditorState and
    /// the MapEditorGui
    GuiSyncedValue<irr::core::vector3df> rotationSync;
    /// @brief used to syncronize the scale of a selected entity between the MapEditorState and the
    /// MapEditorGui
    GuiSyncedValue<irr::core::vector3df> scaleSync;
    /// @brief used to sync texture brush settings, etc... between MapEditorState and MapEditorGui
    ModifiableParameters modifiableParameters = ModifiableParameters();
    MapLoader::MapLoadingParameters mapLoadingParameters = MapLoader::MapLoadingParameters();

    entityID_t getEntityIDUnderCursor() const;

    irr::scene::ICameraSceneNode* mapeditorcam = nullptr;
    MapEditorCameraAnimator* camAnim = nullptr;

    struct sMouseState
    {
        irr::core::position2d<irr::s32> currentCursorPosition;
        irr::core::position2d<irr::s32> oldCursorPosition;
        bool leftButtonDown; // true if left mouse button still pressed
        bool middleButtonDown;
        bool rightButtonDown;
        bool guiHovered;
        sMouseState()
            : currentCursorPosition()
            , oldCursorPosition()
            , leftButtonDown(false)
            , middleButtonDown(false)
            , rightButtonDown(false)
            , guiHovered(false)
        {
        }
    } mouseState;
    void refreshMousestate(const irr::SEvent& event);

    DecalManager* decalManager = nullptr;
    DecalSceneNode* decalNode = nullptr;

    // objects/functions for terrain drawing
    static constexpr int maxIntensity = 15;
    static constexpr irr::f32 maxFloatIntensity = static_cast<irr::f32>(maxIntensity);
    static constexpr size_t numFreelyMixableTextures = 4;
    static constexpr size_t backgroundTextureMixingIndex = numFreelyMixableTextures;
    static constexpr size_t numTotalMixableTextures = numFreelyMixableTextures + 1;

    /// @brief because texture intensities on the splatMap can only be drawn in 16 discrete levels
    /// incrementally drawing onto a point is made very difficult. Thus we keep a copy of all pixels
    /// and their intensities as floats in memory. Drawing is done on the float intensities and the
    /// actual pixel color is then determined by converting the floats to irr::u8
    std::vector<std::vector<std::array<irr::f32, 5>>> floatIntensities;

    std::vector<std::vector<irr::core::vector3df>> pixelWorldCoords;
    std::vector<std::vector<irr::core::vector3df>> pixelWorldNormals;

    /// @brief gather the intensities from the splatMap and save them into the internal
    /// floatIntensities array
    /// @param splatTextureImage image of the splatMap. Results if using anything else than the
    /// splatMap (as used by the terrainShader) are unspecified. MUSTN'T be nullptr!
    void initFloatIntensities(const irr::video::IImage* const splatTextureImage);

    /// @brief create the pixelWorldCoordinates and pixelWorldNormals arrays and fill them with
    /// 'empty' (FLT_MIN, FLT_MIN, FLT_MIN) data.
    ///
    /// filling the arrays with the correct data on state enter takes way too much time. The
    /// coordinates are filled bit-by-bit during drawing and used, if they already exist.
    /// @param splatTextureImage image of the splatMap. Results if using anything else than the
    /// splatMap (as used by the terrainShader) are unspecified. MUSTN'T be nullptr!
    void initPixelWorldCoordsAndNormals(const irr::video::IImage* splatTextureImage);

    irr::scene::TerrainCollisionManager* terrainCollisionManager = nullptr;
    /// @brief list of triangles hit by drawing brush
    irr::core::array<irr::core::triangle3df> selectedTriangles;
    /// @brief vertex indices into the gamemap->terrainscenenode->renderbuffer vertex buffer
    irr::core::array<irr::u32> selectedTriangleVertexIndices;
    bool drawing = true; // TODO check needed
    TextureDrawer* texDrawer = nullptr;
    irr::u32 layerID = 0; //-will be changed to a irr::array of ids once we have options to edit multiple splatmaps
    irr::f32 secondsSinceLastDrawn = 0.0f;

    void drawTextures(const irr::core::aabbox3df brushBox,
                      const irr::u32 brushSelectedTrianglesCount,
                      const irr::core::vector2df baseTexCoords,
                      const irr::core::vector3df baseNormal);

    /// @brief gets the intensities of the mixed textures in splatTexturePixel
    /// @param splatTexturePixel
    /// @return intensities array as read from the pixel. Doesn't validate that e.g. the sum over
    /// all intensities isn't greater than maxIntensity!
    std::array<irr::u8, 5> getIntensities(const irr::video::SColor splatTexturePixel);

    /// @brief gets the texture indices into the textureArray for the textures mixed in
    /// splatTexturePixel
    /// @param splatTexturePixel
    std::array<size_t, 5> getTexturesInPixel(const irr::video::SColor splatTexturePixel);

    /// @brief set intensity for texture at textureMixingIndex to newIntensity
    /// corrects other intensities downwards to make sure the sum of all intensities isn't more than
    /// maxIntensity
    /// @param pixelX
    /// @param pixelY
    /// @param textureMixingIndex index of the texture component for the splatMap. WARGING: not the
    /// same as the index into the textureArray
    /// @param newIntensity
    void updateIntensities(const irr::u32 pixelX,
                           const irr::u32 pixelY,
                           const size_t textureMixingIndex,
                           const irr::f32 newIntensity);

    /// @brief get the color for a pixel on the splatMap by mixing 'textures'. Intensities are
    /// determined by floatIntensities array
    /// makes sure the sum over all intensities isn't greater than maxIntensity and no intensity is
    /// < 0
    /// @param pixelX
    /// @param pixelY
    /// @param mixedTextures indices (into the textureArray of the gamemap terrainShader) for the
    /// textures which should be mixed
    /// @return the color for the mixed textures
    irr::video::SColor
    getColor(const irr::u32 pixelX, const irr::u32 pixelY, const std::array<size_t, 5> mixedTextures);

    /// @brief draw on the floatIntensities map for the selected texture at pixelX, pixelY. Checks
    /// all gui settings to see if the values for that pixel actually change.
    /// @param selectedTexture the index of the texture to draw
    /// @param pixelX image x coordinate
    /// @param pixelY image y coordinate
    /// @param oldSplatTextureColor old color at that pixel
    /// @param relativeDistanceToBrushCenter distance from the center of the brush to the pixel to
    /// be drawn / brushSize (values > 1.0 will be clamped to 1.0)
    /// @return new color that will be drawn with the updated intensities (might still be the old
    /// color if no drawing was performed or due to rouding the floatIntensities)
    irr::video::SColor drawOnFloatIntensities(const size_t selectedTexture,
                                              const irr::u32 pixelX,
                                              const irr::u32 pixelY,
                                              const irr::video::SColor oldSplatTextureColor,
                                              const irr::f32 relativeDistanceToBrushCenter);

    irr::core::vector2df getTexCoordsForPointInTriangle(const irr::core::vector3df& colpoint,
                                                        const irr::core::triangle3df& triangle,
                                                        const irr::core::array<irr::u32>& vertexIndices) const;
    /// @brief get barycentric coefficients for a point inside a triangle
    /// @param point assumed to be on the same plane as the triangle. if not on the same plane the
    /// coefficients will be wrong.
    /// @param triangle
    /// @return barycentric coefficients for (pointA, pointB, pointC)
    irr::core::vector3df getBarycentricCoefficients(const irr::core::vector3df point,
                                                    const irr::core::triangle3df triangle) const;

    /// @brief return world space positon on the gamemap for a texture coordinate.
    /// @param texCoords texture coordinate for which world space position should be searched
    /// @param triangles triangles which contain the texture coordinate
    /// @param vertexIndices indices into the terrainscenenode vertex-renderbuffer. Size MUST be at
    /// least 3 * triangles.size()
    /// @param triangleIndex the index of the triangle which determined the world coordinates.
    /// triangles.size() if no fitting triangle found
    /// @return the world coordinates for the texture coordinates. (FLT_MAX, FLT_MAX, FLT_MAX) if no
    /// triangle contained the texture coordinates
    irr::core::vector3df reverseUVLookup(const irr::core::vector2df texCoords,
                                         const irr::u32 selectedTriangleCount,
                                         irr::u32& triangleIndex) const;
};

#endif // MAPEDITORSTATE_H
