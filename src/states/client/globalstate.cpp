/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "globalstate.h"
#include <algorithm>

#include <CEGUI/RendererModules/Irrlicht/Renderer.h>
#include <CEGUI/ScriptModules/Lua/ScriptModule.h>
#include <CEGUI/Window.h>
#include <CEGUI/WindowManager.h>
#include <irrlicht/IBgfxManipulator.h>
#include <sol.hpp>

#include "../../core/game.h"
#include "../../gui/ceGuiEnvironment.h"
#include "../../scripting/scriptingModule.h"
#include "../../utils/debug.h"
#include "../../utils/printfunctions.h"
#include "../../utils/scriptconsole.h"

GlobalState::GlobalState(Game& game_)
    : State(game_)
    , terminalGuiObj(nullptr)
    , lastFPS(0)
{
}

// ---------------------------------------------------------------------------
GlobalState::~GlobalState() {}

// ---------------------------------------------------------------------------
void GlobalState::onEnter()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Entering GlobalState");
//this->game.setGlobalFont("Times New Roman", 28); // setGlobalFont for this State
#if defined(MAKE_DEBUG_)
    debugFlag = irr::video::E_BGFX_DEBUG_FLAGS::E_STATS;
#endif

    // TODO find a better place
    auto runBaseScript = [&](sol::state& s) {
        try
        {
            auto result = s.safe_script_file("./scripts/core/base.lua");
        }
        catch (const sol::error& e)
        {
            std::cout << "Error while running base script has occurred: " << e.what() << std::endl;
        }
    };
    this->game.getScriptingModule()->getScriptEngine().loopOverInstances(runBaseScript);
    sol::state& solState = this->game.getScriptingModule()->getScriptEngine().getSolState(0);
    CEGUI::LuaScriptModule& scriptMod(CEGUI::LuaScriptModule::create(solState.lua_state()));
    CEGUI::System::getSingleton().setScriptingModule(&scriptMod);


    /**  ------ ------  START  ------ ------**/
    /** ------ [created GUI-Objects] ------ **/
    debugOutLevel(20, "Initialise TerminalGui");
    // Implement here to run terminal in every State
    this->game.getGuiEnvironment()->initGuiElement("TerminalGui");

    debugOutLevel(20, "Initialise MessageBoxGui");
    // Implement here to run messageBox in every State

    this->game.getGuiEnvironment()->initGuiElement("MessageBoxGui");
    /** ------ [created GUI-Objects] ------ **/
    /**  ------ ------   END   ------ ------**/
}

// ---------------------------------------------------------------------------
void GlobalState::onLeave()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Leaving GlobalState");
    // delete created GUI-Objects
    this->game.getGuiEnvironment()->destructGuiElement("TerminalGui");
    this->game.getGuiEnvironment()->destructGuiElement("MessageBoxGui");
}

// ---------------------------------------------------------------------------
void GlobalState::update()
{
    SCRIPTING_CONSOLE_OPTIMIZE_OUT(
        // fetch input for the console
        // if the listener shouldn't listen then there will be no input
        // so no need to check explicitly
        if (not this->game.consoleListener.queueEmpty()) {
            debugOutLevel(Debug::DebugLevels::updateLoop, "reading input from console");
            const std::string input = this->game.consoleListener.popInput();
            debugOutLevel(Debug::DebugLevels::updateLoop, "read input from console:", repr<std::string>(input));
            // output will be printed to stdout by default
            this->game.scriptConsole->evalInputLine(input);
        })
}

// ---------------------------------------------------------------------------
bool GlobalState::onEvent(const irr::SEvent& event)
{
    /** *** Shell Handling Start *** **/
    // act as every event except F12 was handled by the console
    // this stops weird input side-effects when a state uses a command while the console is open
    // (e.g. camera scroll on ButtonPressed while the console waits for !ButtonPressed
    if (event.EventType == irr::EET_KEY_INPUT_EVENT and event.KeyInput.Key == irr::KEY_F12 and
        !event.KeyInput.PressedDown)
    {
        CEGUI::System::getSingleton().getScriptingModule()->executeString(
            "TerminalGui.updateVisibility()");
        return true;
    }
    if (this->game.getGuiEnvironment()->getRoot()->getChild("WindowConsole")->isVisible())
    {
        try
        {
            this->game.getGuiEnvironment()->getRenderer()->injectEvent(event);
        }
        catch (const CEGUI::ScriptException& ex)
        {
            std::string exS(ex.getMessage().c_str());
            std::string toFind("event handler:\n\n");
            auto offset = exS.find(toFind);
            exS = exS.substr(offset + toFind.length());
            //         std::replace(exS.begin(), exS.end(), '\\', '/');
            //         //std::replace(exS.begin(), exS.end(), '\"', '\"');
            //         std::replace(exS.begin(), exS.end(), '\'', '\"');
            //         std::replace(exS.begin(), exS.end(), '\n', ' ');
            //
            // std::string err = "TerminalGui.inputError(\'";
            //         err.append(exS);
            //         err.append("\')");
            //
            // CEGUI::System::getSingleton().getScriptingModule()->executeString(err.c_str());
            game.getScriptingModule()->getScriptEngine().singleInstance()["TerminalGui.inputError"](exS);
        }
        return true;
    }
    /** *** Shell Handling End *** **/


    /// ONLY FOR TESTING PURPOSES!!
    if (event.EventType == irr::EET_KEY_INPUT_EVENT)
    {
        if (event.KeyInput.Key == irr::KEY_F1 && !event.KeyInput.PressedDown) // load MenuState by F1 Push
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Pushesd KEY F1 -> switchover to MenuState");
            this->game.changeState(Game::States_e::Menu);
            return true;
        }
        else if (event.KeyInput.Key == irr::KEY_F2 && !event.KeyInput.PressedDown) // load GameState by F2 Push
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Pressed KEY F2 -> switchover to ServerBrowserState");
            this->game.changeState(Game::States_e::ServerBrowser);
            return true;
        }
        else if (event.KeyInput.Key == irr::KEY_F3 && !event.KeyInput.PressedDown) // load GameState by F3 Push
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Pressed KEY F3 -> switchover to LobbyState");
            this->game.changeState(Game::States_e::Lobby);
            return true;
        }
        else if (event.KeyInput.Key == irr::KEY_F6 && !event.KeyInput.PressedDown) // load GameState by F6 Push
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Pressed KEY F6 -> switchover to GameState");
            this->game.changeState(Game::States_e::Game);
            return true;
        }
        else if (event.KeyInput.Key == irr::KEY_ESCAPE and not event.KeyInput.PressedDown) // load ExampleState by KEY_ESCAPE Push
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Pressed KEY ESCAPE -> switchover to ExampleState");
            this->game.changeState(Game::States_e::Example);
            return true;
        }
        // load ExampleState by KEY_ESCAPE Push
        else if (event.KeyInput.Key == irr::KEY_F10 and not event.KeyInput.PressedDown) // load MapeditorState by F10 Push
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Pushesd F10 -> switchover to MapEditor");
            this->game.changeState(Game::States_e::Mapeditor);
            return true;
        }
        else if (event.KeyInput.Key == irr::KEY_KEY_H and not event.KeyInput.PressedDown)
        {
            irr::video::IBgfxManipulator* bgfxManipulator =
                static_cast<irr::video::IBgfxManipulator*>(this->game.getVideoDriver());


            debugFlag = (debugFlag & irr::video::E_BGFX_DEBUG_FLAGS::E_STATS ?
                             irr::video::E_BGFX_DEBUG_FLAGS::E_NONE :
                             irr::video::E_BGFX_DEBUG_FLAGS::E_STATS);
            bgfxManipulator->setBgfxDebugMode(debugFlag);
        }
        /*else if (event.KeyInput.Key == irr::KEY_LMENU and event.KeyInput.Key == irr::KEY_F4) // load ExampleState by KEY_ESCAPE Push //TODO fix error
        {
            event.KeyInput.Key == irr::KEY_LMENU and
            debugOutLevel(3,"Pressed KEY alt + F4 -> exit game");
            this->game.getDevice()->closeDevice();
            return true;
        }*/
    }

    return false;
}
