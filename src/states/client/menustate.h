/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MenuState_H
#define MenuState_H

#include <irrlicht/IEventReceiver.h>

#include "../state.h"

class Game;

class MenuState : public State
{
   public:
    MenuState(Game& game);
    MenuState(const MenuState&) = delete;
    MenuState operator=(const MenuState&) = delete;
    virtual ~MenuState();

    void onEnter();
    void onLeave();

    void update(); // Empty
    virtual bool onEvent(const irr::SEvent& event);

   private:
};

#endif //  MenuState_H
