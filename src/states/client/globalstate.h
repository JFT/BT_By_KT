/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLOBALSTATE_H_
#define GLOBALSTATE_H_

#include <irrlicht/IEventReceiver.h>
#include <irrlicht/IVideoDriver.h> // used by the debugFlag

#include "../state.h"

class Game;
class TerminalGui;

class GlobalState : public State
{
   public:
    GlobalState(Game& game);
    GlobalState(const GlobalState&) = delete;
    virtual ~GlobalState();

    virtual void onEnter();
    virtual void onLeave();

    virtual void update();
    virtual bool onEvent(const irr::SEvent& event);

    GlobalState operator=(const GlobalState&) = delete;

   private:
    TerminalGui* terminalGuiObj; // DONT use Delete -> Use CEGUIEnvironment::deleteTerminalGui() to
                                 // prevent Pointer Errors
    irr::s32 lastFPS;
    unsigned int debugFlag = irr::video::E_BGFX_DEBUG_FLAGS::E_NONE;
};

#endif /*MYGLOBALSTATE_H_*/
