/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <irrlicht/IEventReceiver.h>
#include <irrlicht/matrix4.h>
#include <irrlicht/vector3d.h>
#include <map>
#include <memory> // std::unique_ptr

#include "../state.h"
#include "../../ecs/entityBlueprint.h"
#include "../../utils/HandleManager.h"

// forward declarations
class Game;
class GameNetwork;
class GameMap;
class RTSCameraAnimator;
class InputContextManager;
class EventManager;
class EntityFactory;
class NetworkEventSyncronizationClient;
class NetworkComponentSyncronizationClient;
class MovementSystem;
class GraphicSystem;
class SelectionSystem;
class ProjectileSystem;
class CooldownSystem;
class DamageSystem;
class AutomaticWeaponSystem;
class ShopSystem;
class InventorySystem;
class MoneyTransferSystem;
class HealthBarSystem;
class ShopGuiSystem;
class InventoryGuiSystem;
class PathingSystem;
namespace grid
{
    class Grid;
}
namespace irr
{
    namespace scene
    {
        class IAnimatedMeshSceneNode;
    }
} // namespace irr


class GameState : public State
{
   public:
    explicit GameState(Game& game);
    GameState(const GameState&) = delete;
    virtual ~GameState();

    void onEnter();
    void onLeave();

    void update();
    virtual bool onEvent(const irr::SEvent& event);

    GameState operator=(const GameState&) = delete;

    inline GameMap* getGameMap() const { return this->gameMap; }

   private:
    void initCamera();
    void initEntitySystem();
    void initLuaScriptEngine();
    void initNetwork();

    entityID_t myPlayerProfileID = ECS::INVALID_ENTITY;

    GameMap* gameMap = nullptr;
    grid::Grid* grid = nullptr;

    RTSCameraAnimator* rtsanim = nullptr;
    irr::scene::IAnimatedMeshSceneNode* selectednode = nullptr;

    EntityFactory* entityFactory = nullptr;
    EntityManager* entityManager = nullptr;

    Handles::HandleManager handleManager;

    std::unique_ptr<EventManager> eventManager;

    std::unique_ptr<InputContextManager> inputContextManager;

    MovementSystem* movementSystem = nullptr;
    GraphicSystem* graphicSystem = nullptr;
    SelectionSystem* selectionSystem = nullptr;
    ProjectileSystem* projectileSystem = nullptr;
    CooldownSystem* cooldownSystem = nullptr;
    DamageSystem* damageSystem = nullptr;
    AutomaticWeaponSystem* automaticWeaponSystem = nullptr;
    ShopSystem* shopSystem = nullptr;
    InventorySystem* inventorySystem = nullptr;
    MoneyTransferSystem* moneyTransferSystem = nullptr;
    PathingSystem* pathingSystem = nullptr;

    HealthBarSystem* healthBarSystem = nullptr;
    ShopGuiSystem* shopGuiSystem = nullptr;
    InventoryGuiSystem* inventoryGuiSystem = nullptr;

    GameNetwork* gameNetworkObj = nullptr;
    NetworkEventSyncronizationClient* networkEventSyncronization = nullptr;
    NetworkComponentSyncronizationClient* networkComponentSyncronization = nullptr;


    void selectNode();

    // parser stuff
    std::map<std::string, blueprintID_t> blueprintNameToIDMap = {};
    std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>> blueprintCollection = {};

    // Networking replica handling...
    irr::u32 lastReplicaListSize = 0;
};

#endif // GAMESTATE_H
