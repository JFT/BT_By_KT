/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lobbystate.h"

// ----------------------------------------------------------------------------
#include <CEGUI/DefaultResourceProvider.h>
#include <CEGUI/RendererModules/Irrlicht/Renderer.h>
#include <CEGUI/ScriptModule.h>
#include <CEGUI/Window.h>
#include <CEGUI/WindowManager.h>

#include "../../core/config.h"
#include "../../core/game.h"
#include "../../gui/ceGuiEnvironment.h"
#include "../../network/client/lobbyState/lobbyChatReplica.h"
#include "../../network/client/lobbyState/lobbyNetwork.h"
#include "../../network/client/networkclient.h" // only forward-declared in game
#include "../../network/client/playerCollectionReplica.h"
#include "../../scripting/scriptingModule.h"
#include "../../utils/static/error.h"


// ----------------------------------------------------------------------------
LobbyState::LobbyState(Game& game_)
    : State(game_)
{
}

// ---------------------------------------------------------------------------
LobbyState::~LobbyState() {}

// ---------------------------------------------------------------------------
void LobbyState::onEnter()
{
    debugOutLevel(Debug::DebugLevels::stateHandling, "Entering LobbyState");

    if (this->game.getPlayerCollectionReplica() == nullptr)
    {
        Error::errTerminate(
            "this->game.getPlayerCollectionReplica() was nullptr but it should have "
            "been set by the serverBrowserNetwork!");
    }
    if (this->game.getLobbyChatReplica() == nullptr)
    {
        Error::errTerminate(
            "this->game.getLobbyChatReplica() was nullptr but it should have been set "
            "by the serverBrowserNetwork!");
    }

    debugOutLevel(Debug::DebugLevels::stateInit, "Create LobbyNetwork");
    this->lobbyNetworkObj = new LobbyNetwork(&this->game);

    // expose the script resource path to lua before loading the lobby,
    // so it can find and execute the chat.lua script
    CEGUI::DefaultResourceProvider* const rp =
        static_cast<CEGUI::DefaultResourceProvider*>(CEGUI::System::getSingleton().getResourceProvider());
    // the 'CEGUI.ScriptModule.ResourceGroupDirectory' name is choosen to stay as close as possible to the CEGUI naming convention
    CEGUI::System::getSingleton().getScriptingModule()->executeString(
        "CEGUI.ScriptModule = {[\"ResourceGroupDirectory\"] = \"" +
        rp->getResourceGroupDirectory(CEGUI::ScriptModule::getDefaultResourceGroup()) + "\"}");

    debugOutLevel(20, "Loading 'lobbyGui.layout'");
    this->game.getGuiEnvironment()->initGuiElement("LobbyGui");

    this->connectEvents();
}

void LobbyState::onLeave()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Leaving LobbyState");

    // normally only make the lobbyGui invisible to cut down on loading time while switching
    // between states. Except when switching to the gamestate. The gui will already have been destroyed then.
    if (!this->lobbyNetworkObj->hasServerChangedToGameState())
    {
        auto& lua = this->game.getScriptingModule()->getScriptEngine().singleInstance();
        CEGUI::Window* root = lua["LobbyGui"]["root"];
        root->setVisible(false);
    }

    this->disconnectEvents(*this->game.getLobbyChatReplica());

    delete this->lobbyNetworkObj;
    this->lobbyNetworkObj = nullptr;

    // LobbyChatReplica will be re-created by the ServerBrowserNetwork when joining a new server.
    delete this->game.getLobbyChatReplica();
    this->game.setLobbyChatReplica(nullptr);
}

// ---------------------------------------------------------------------------
void LobbyState::clearGui()
{
    debugOutLevel(20, "clearGui: Delete GUI-windows from Menu-, ServerBrowser- and LobbyState");
    this->game.getGuiEnvironment()->destructGuiElement("MainMenuGui");
    this->game.getGuiEnvironment()->destructGuiElement("ServerBrowserGui");
    this->game.getGuiEnvironment()->destructGuiElement("LobbyGui");
}

// ---------------------------------------------------------------------------
void LobbyState::update()
{
    if (this->game.getNetwork()->isInitialized())
    {
        if (this->lobbyNetworkObj->processPackets())
        {
            // something changed -> update the gui
            debugOutLevel(Debug::DebugLevels::updateLoop, "processed packets!");

            auto& lua = this->game.getScriptingModule()->getScriptEngine().singleInstance();
            sol::function f = lua["LobbyGui"]["updateSlotButton"];

            std::vector<std::tuple<int, std::wstring, bool, bool>> playerStatus;
            int playerId = 1;
            for (const auto& slot : this->game.getPlayerCollectionReplica()->getSlots())
            {
                f(playerId, std::wstring(slot.playerName), slot.isInUse, slot.ready);
                playerId++;
            }
        }

        if (not this->lobbyNetworkObj->isStillConnected())
        {
            this->game.getConfiguration().resetServerBackup();
            this->game.getConfiguration().saveServerBackup("config/serverBackup.xml", this->game.getDevice());

            this->game.changeState(Game::States_e::ServerBrowser);
            return; // need to leave update loop after changing state because thate change calls
                    // onLeave and the rest of the update loop might need stuff destructed in
                    // onLeave
        }

        if (this->lobbyNetworkObj->hasServerChangedToGameState())
        {
            // delete the gui elements no longer needed in the gamestate to
            // free as much memory as possible
            this->clearGui();

            // the server will check the 'ready' flag of the PlayerCollectionReplica to test if we
            // have finished loading
            // but the ready flag is currently 'true' otherwise the server wouldn't have switched
            // states
            this->game.getPlayerCollectionReplica()->setReadyFlag(false);
            this->game.changeState(Game::States_e::Game);
            return; // need to leave update loop after changing state because thate change calls
                    // onLeave and the rest of the update loop might need stuff destructed in
                    // onLeave
        }
    }
}

// ---------------------------------------------------------------------------
bool LobbyState::onEvent(const irr::SEvent& event)
{
    if (event.EventType != irr::EET_KEY_INPUT_EVENT or event.KeyInput.Key <= irr::KEY_KEY_CODES_COUNT)
    {
        if (this->game.getGuiEnvironment()->getRenderer()->injectEvent(event)) // CEGUI-EventReceiver
        {
            return true; // use CEGUI EVENTHANDLER
        }
    }
    else
    {
        debugOutLevel(20, "For CEGUI invalid Keycode NOT in CEGUI injected: ", event.KeyInput.Key);
        return true; // No key we need to handle (mostly Volume up and down keys)
    }

    return false;
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void LobbyState::connectEvents()
{
    auto& lua = this->game.getScriptingModule()->getScriptEngine().singleInstance();
    CEGUI::Window* root = lua["LobbyGui"]["root"];

    this->connectedEvents.push_back(root->subscribeEvent("DisconnectFromServer", [this]() {
        this->game.getNetwork()->disconnectFromServer();
        return true;
    }));

    this->connectedEvents.push_back(root->subscribeEvent("Ready", [this]() {
        debugOutLevel(11, "changing ready flag");
        this->game.getPlayerCollectionReplica()->changeReadyToStartGameFlag();
        return true;
    }));

    this->connectedEvents.push_back(root->subscribeEvent("RequestStart", [this]() {
        if (this->game.getPlayerCollectionReplica()->allPlayersReady()) // no need to send out a request that
                                                                        // we know beforehand will be ignored
        {
            this->game.getNetwork()->requestStartPauseOrResumeGame(PauseRequest::RequestType::Running);
        }
        return true;
    }));

    this->connectedEvents.push_back(root->subscribeEvent("RequestSlotChange", [this](const CEGUI::EventArgs& args) {
        const CEGUI::WindowEventArgs& window = static_cast<const CEGUI::WindowEventArgs&>(args);
        const unsigned int id = window.window->getID();

        this->game.getPlayerCollectionReplica()->setRequestPosition(id);
        debugOutLevel(Debug::DebugLevels::onEvent, "requested position change to slot", id);
        return true;
    }));

    // Connect chat input/output events

    this->game.getScriptingModule()
        ->getScriptEngine()
        .singleInstance()["LobbyGui"]["Chat"]["onSendChatMessage"] = [this](std::wstring message) {
        this->game.getLobbyChatReplica()->writeMessage(RakNet::RakWString(message.c_str()));
    };

    this->game.getLobbyChatReplica()->messageReceived = [this](std::wstring message) {
        auto& lua = this->game.getScriptingModule()->getScriptEngine().singleInstance();
        sol::protected_function f = lua["LobbyGui"]["Chat"]["onMessageReceived"];
        f.error_handler = lua["log.fatal"];
        sol::protected_function_result result = f(message);
        if (!result.valid())
        {
            sol::error err = result;
            std::string what = err.what();
            std::cerr << "lua error = " << what << std::endl;
        }
    };

    sol::function f = lua["LobbyGui"]["onConnectedToServer"];
    f(this->game.getConfiguration().getSettings().connectedServerName,
      this->game.getConfiguration().getSettings().numberOfSlots);
}

void LobbyState::disconnectEvents(LobbyChatReplica& chat)
{
    for (auto& connection : this->connectedEvents)
    {
        connection->disconnect();
    }
    chat.messageReceived = {};
}
