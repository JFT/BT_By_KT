/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LobbyState_H
#define LobbyState_H

#include <CEGUI/BoundSlot.h>
#include <CEGUI/RefCounted.h>
#include <irrlicht/IEventReceiver.h>

#include <vector>

#include "../state.h"


class Game;
class LobbyNetwork;
class LobbyChatReplica;

class LobbyState : public State
{
   public:
    LobbyState(Game& game);
    LobbyState(const LobbyState&) = delete;
    virtual ~LobbyState();

    void onEnter();
    void onLeave();
    void clearGui();

    void update();
    virtual bool onEvent(const irr::SEvent& event);

    LobbyState operator=(const LobbyState&) = delete;

   private:
    LobbyNetwork* lobbyNetworkObj = nullptr;

    void connectEvents();
    void disconnectEvents(LobbyChatReplica& chat);

    std::vector<CEGUI::RefCounted<CEGUI::BoundSlot>> connectedEvents;
};

#endif // LobbyState_H
