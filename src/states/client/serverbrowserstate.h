/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERVERBROWSERSTATE_H
#define SERVERBROWSERSTATE_H

#include <irrlicht/IEventReceiver.h>

#include "../state.h"

class Game;
class ServerBrowserNetwork;

class ServerBrowserState : public State
{
   public:
    ServerBrowserState(Game& game);
    ServerBrowserState(const ServerBrowserState&) = delete;
    virtual ~ServerBrowserState();

    void onEnter();
    void onLeave();

    void update();
    virtual bool onEvent(const irr::SEvent& event);

    ServerBrowserState operator=(const ServerBrowserState&) = delete;

   private:
    ServerBrowserNetwork* serverBrowserNetworkObj = nullptr;
};

#endif // SERVERBROWSERSTATE_H
