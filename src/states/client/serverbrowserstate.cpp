/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "serverbrowserstate.h"
#include <iostream>

// ----------------------------------------------------------------------------
#include <CEGUI/RendererModules/Irrlicht/Renderer.h>
#include <CEGUI/ScriptModules/Lua/ScriptModule.h>
#include <CEGUI/Window.h>

#include "../../core/game.h"
#include "../../gui/ceGuiEnvironment.h"
#include "../../network/client/networkclient.h" // only forward-declared in game
#include "../../network/client/serverBrowserState/serverBrowserNetwork.h"
#include "../../scripting/scriptingModule.h"
#include "../../utils/static/error.h"


// ----------------------------------------------------------------------------
ServerBrowserState::ServerBrowserState(Game& game_)
    : State(game_)
{
    // ctor
}

// ---------------------------------------------------------------------------
ServerBrowserState::~ServerBrowserState()
{
    // dtor
}

void ServerBrowserState::onEnter()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Entering ServerBrowserState");

    //--START-- Network modification
    if (not this->game.getNetwork()->isInitialized()) // Create Network mask if not done before
    {
        this->game.getNetwork()->init();
    }

    debugOutLevel(20, "Create ServerBrowserNetworkObj");
    this->serverBrowserNetworkObj = new ServerBrowserNetwork(&game);

    this->serverBrowserNetworkObj->checkStillPartOfARunningGame();


    // define lua variables
    this->game.getScriptingModule()->getScriptEngine().loopOverInstances(
        [this](sol::state& instance) {
            // std::ref for non-ptr members
            instance["game"] = std::ref(this->game);
            instance["scriptingModule"] = std::ref(*this->game.getScriptingModule());
        },
        ScriptEngine::ForceLoop::True);

    //--START-- Create GUI-Object
    debugOutLevel(20, "Initialise ServerBrowserGui");
    this->game.getGuiEnvironment()->initGuiElement("ServerBrowserGui");


    // connect gui to backend network object callbacks
    this->serverBrowserNetworkObj->serverAdded = [this](int id, struct ServerInfos info) {
        auto& lua = this->game.getScriptingModule()->getScriptEngine().singleInstance();

        sol::protected_function f = lua["ServerBrowserGui"]["onServerAdded"];
        f.error_handler = lua["log.fatal"];
        sol::protected_function_result result =
            f(id, std::string(info.name), info.address.ToString(true, ':'), info.filledSlots, info.maxSlots, info.passwordProtected);
        if (!result.valid())
        {
            // Call failed
            sol::error err = result;
            std::string what = err.what();
            std::cerr << "lua error = " << what << std::endl;
        }
    };

    this->serverBrowserNetworkObj->serverChanged = [this](int id, ServerInfos info) {
        auto& lua = this->game.getScriptingModule()->getScriptEngine().singleInstance();
        sol::protected_function f = lua["ServerBrowserGui"]["onServerChanged"];
        f.error_handler = lua["log.fatal"];
        sol::protected_function_result result = f(id, info.filledSlots, info.maxSlots, info.passwordProtected);
        if (!result.valid())
        {
            sol::error err = result;
            std::string what = err.what();
            std::cerr << "lua error = " << what << std::endl;
        }
    };

    this->serverBrowserNetworkObj->serverRemoved = [this](int id) {
        auto& lua = this->game.getScriptingModule()->getScriptEngine().singleInstance();
        sol::protected_function f = lua["ServerBrowserGui"]["onServerRemoved"];
        f.error_handler = lua["log.fatal"];
        sol::protected_function_result result = f(id);
        if (!result.valid())
        {
            sol::error err = result;
            std::string what = err.what();
            std::cerr << "lua error = " << what << std::endl;
        }
    };


    this->serverBrowserNetworkObj->serverPingChanged = [this](int id, int ping) {
        auto& lua = this->game.getScriptingModule()->getScriptEngine().singleInstance();

        sol::protected_function f = lua["ServerBrowserGui"]["onServerPingChanged"];
        f.error_handler = lua["log.fatal"];
        sol::protected_function_result result = f(id, ping);
        if (!result.valid())
        {
            // Call failed
            sol::error err = result;
            std::string what = err.what();
            std::cerr << "lua error = " << what << std::endl;
        }
    };

    CEGUI::Window* root =
        this->game.getScriptingModule()->getScriptEngine().singleInstance()["ServerBrowserGui"]["ro"
                                                                                                "o"
                                                                                                "t"];

    this->game.getScriptingModule()
        ->getScriptEngine()
        .singleInstance()["ServerBrowserGui"]["onJoinRequest"] = [this, root](int id, std::string password) {
        const auto server = std::find_if(this->serverBrowserNetworkObj->getServerList().cbegin(),
                                         this->serverBrowserNetworkObj->getServerList().cend(),
                                         [id](const auto& e) { return e.first == id; });

        if (server != this->serverBrowserNetworkObj->getServerList().cend())
        {
            if (server->second.passwordProtected && password == "")
            {
                CEGUI::EventArgs e;
                root->getChild("Window_EnterServerPassword")->fireEvent("AskForPassword", e);
            }
            else
            {
                this->serverBrowserNetworkObj->tryToConnectToServer(server->second.address, password);
            }
        }
    };

    this->serverBrowserNetworkObj->invalidPassword = [this, root](int id) {
        auto& lua = this->game.getScriptingModule()->getScriptEngine().singleInstance();
        sol::protected_function f =
            lua["ServerBrowserGui"]["PasswordDialog"]["onServerPasswordRejected"];
        f.error_handler = lua["log.fatal"];
        sol::protected_function_result result = f(id);
        if (!result.valid())
        {
            sol::error err = result;
            std::string what = err.what();
            std::cerr << "lua error = " << what << std::endl;
        }
    };
}

// ---------------------------------------------------------------------------
void ServerBrowserState::onLeave()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Leaving ServerBrowserState");

    delete this->serverBrowserNetworkObj;
    this->serverBrowserNetworkObj = nullptr;

    // Dont delete created GUI-Objects, we dont wont reload the Gui
    // while switching between the Menu-,Lobby, and ServerbrowserState
    // Just set it invisible
    CEGUI::System::getSingleton().getScriptingModule()->executeString(
        "ServerBrowserGui.PasswordDialog.hide()");
    CEGUI::System::getSingleton().getScriptingModule()->executeString(
        "ServerBrowserGui.root:setVisible(false)");
}

// ---------------------------------------------------------------------------
void ServerBrowserState::update()
{
    // collect all server responses and show them in server browser
    this->serverBrowserNetworkObj->processPackets();

    this->serverBrowserNetworkObj->pingAndClearKnownServers();

    if (this->serverBrowserNetworkObj->switch_)
    {
        debugOutLevel(20,
                      "Connection successfully and ServerBackup saved-> Exit "
                      "Server-Browser-State to Lobby-State");
        this->serverBrowserNetworkObj->switch_ = false;
        this->game.changeState(Game::States_e::Lobby);
    }
}

// ---------------------------------------------------------------------------
bool ServerBrowserState::onEvent(const irr::SEvent& event)
{
    if (event.EventType != irr::EET_KEY_INPUT_EVENT or event.KeyInput.Key <= irr::KEY_KEY_CODES_COUNT)
    {
        if (this->game.getGuiEnvironment()->getRenderer()->injectEvent(event)) // CEGUI-EventReceiver
        {
            return true; // use CEGUI EVENTHANDLER
        }
    }
    else
    {
        debugOutLevel(20, "For CEGUI invalid Keycode NOT in CEGUI injected: ", event.KeyInput.Key);
        return true; // No key we need to handle (mostly Volume up and down keys)
    }

    return false;
}

/**************************************************************
 * private memberfunctions
 **************************************************************/
