/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gamestate.h"

// ----------------------------------------------------------------------------
#include <CEGUI/RendererModules/Irrlicht/Renderer.h>
#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/IMeshSceneNode.h>
#include <irrlicht/ISceneCollisionManager.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/IrrlichtDevice.h>
#include <raknet/GetTime.h>
#include <raknet/RakPeerInterface.h>
#include <raknet/ReplicaManager3.h>
#include "../../utils/stringwstream.h" // should be included first because it defines operator<< for strigw which other files need
#include "../multipleStatesConstants.h"
#include "../../core/config.h"
#include "../../core/game.h"
#include "../../core/rtscameraanimator.h"
#include "../../ecs/componentParsers.h"
#include "../../ecs/componentSerializer.h"
#include "../../ecs/components.h"
#include "../../ecs/entity.h" // entityID_t, componentID_t
#include "../../ecs/entityFactory.h"
#include "../../ecs/entityManager.h"
#include "../../ecs/events/eventManager.h"
#include "../../ecs/events/events.h"
#include "../../ecs/events/networkEventSyncronizationClient.h"
#include "../../ecs/networkComponentSyncronizationClient.h"
#include "../../ecs/storageSystems/allEntitiesComponentValueStorage.h"
#include "../../ecs/systems/automaticWeaponSystem.h"
#include "../../ecs/systems/cooldownSystem.h"
#include "../../ecs/systems/damageSystem.h"
#include "../../ecs/systems/graphicSystem.h"
#include "../../ecs/systems/guiSystems/healthBarSystem.h"
#include "../../ecs/systems/guiSystems/inventoryGuiSystem.h"
#include "../../ecs/systems/guiSystems/shopGuiSystem.h"
#include "../../ecs/systems/inventorySystem.h"
#include "../../ecs/systems/moneyTransferSystem.h"
#include "../../ecs/systems/movement/movementSystem.h"
#include "../../ecs/systems/pathingSystem.h"
#include "../../ecs/systems/projectileSystem.h"
#include "../../ecs/systems/selectionSystem.h"
#include "../../ecs/systems/shopSystem.h"
#include "../../ecs/userinput/inputContextManager.h"
#include "../../graphics/graphicsPipeline.h"
#include "../../grid/grid.h"
#include "../../gui/ceGuiEnvironment.h"
#include "../../map/gamemap.h"
#include "../../map/mapLoader.h"
#include "../../network/client/gameState/gameNetwork.h"
#include "../../network/client/networkclient.h"
#include "../../network/client/playerCollectionReplica.h"
#include "../../network/replicamanager3bt.h"
#include "../../scripting/scriptingModule.h" // needed for the system updates
#include "../../utils/HandleManager.h"

// ----------------------------------------------------------------------------
GameState::GameState(Game& game_)
    : State(game_)
    , handleManager()
    , eventManager(std::make_unique<EventManager>())
    , inputContextManager(std::make_unique<InputContextManager>())
{
    // ctor
}

// ---------------------------------------------------------------------------
GameState::~GameState()
{
    // dtor
}

// ---------------------------------------------------------------------------
void GameState::onEnter()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Entering GameState");

    this->game.getGraphics()->init();


    this->gameNetworkObj = new GameNetwork(&this->game);

    MapLoader mapLoader(this->game.getSceneManager(),
                        this->game.getDevice()->getFileSystem(),
                        this->game.getVideoDriver(),
                        this->game);
    const std::string mapFile("data/map/map.xml"); // TODO use map.xml
    const auto loaded = mapLoader.loadMap(mapFile);
    this->gameMap = std::get<1>(loaded);
    game.getGraphics()->addNodeToStage(gameMap->getTerrainNode(), RenderStageType::STATIC);
    game.getGraphics()->addNodeToStage(gameMap->getTerrainNode(), RenderStageType::LIGHTING);

    this->game.getGuiEnvironment()->initGuiElement("GameGui");

    this->grid = new grid::Grid(this->gameMap->getMapSize(), 1024.0f);

    // initCamera can only be done after this->gameMap exists
    this->initCamera();

    this->myPlayerProfileID = this->game.getPlayerCollectionReplica()->mySlotID();

    this->initEntitySystem();

    this->initLuaScriptEngine();

    this->initNetwork();

    debugOut("Finished onEnter GameState");
}

// ---------------------------------------------------------------------------
void GameState::onLeave()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Leaving GameState");

    this->game.getConfiguration().resetServerBackup();
    this->game.getConfiguration().saveServerBackup("config/serverBackup.xml", this->game.getDevice());

    delete this->gameMap;
    this->game.getCamera()->removeAnimator(this->rtsanim);

    delete this->entityManager;
    delete this->gameNetworkObj;
}


// ---------------------------------------------------------------------------
void GameState::update()
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "tick:", this->game.getTickNumber(), "time (ms) =", RakNet::GetTimeMS());

    // simple test of the event syncronization: throw a TESTEVENT which the server only accepts from
    // client 1 the NetworkEventSyncronizationClient then changes the graphic scale of the entity
    // with the X component of the TESTEVENT vector
    {
        static double a = this->game.getDeltaTime();
        a += this->game.getDeltaTime();
        const entityID_t entity = 11;
        Events::TESTEVENT e;
        e.vec = irr::core::vector3df(entity, 2, 3);
        e.vec.Y = 10 * (2.0 + std::cos(a));
        //        this->eventManager->emit(e);
    }
    {
        // testing dynamically sized event networking
        Events::TESTEVENT_DYNAMICSIZE e;
        e.vecs.push_back(irr::core::vector3df(0.0, 0.2, 0.3));
        e.vecs.push_back(irr::core::vector3df(0.4, 0.5, 0.6));
        e.vecs.push_back(irr::core::vector3df(0.7, 0.8, 0.9));
        //        this->eventManager->emit(e);
    }

    if (this->myPlayerProfileID == 0)
    {
        Events::MovementTargetChanged e;
        this->eventManager->emit(e);
    }


    // Only for testing
    if (this->game.getNetwork()->isConnected(this->game.getConfiguration().getSettings().connectedServerAddress))
    {
        this->gameNetworkObj->processPackets(); // TODO define

        DataStructures::List<RakNet::Replica3*> rep3List;
        this->game.getNetwork()->getReplicaManager()->GetReferencedReplicaList(rep3List);
        if (lastReplicaListSize < rep3List.Size())
        {
            for (irr::u32 i = lastReplicaListSize; i < rep3List.Size(); i++)
            {
            }
            lastReplicaListSize = rep3List.Size();
        }

        if (this->game.getGameStarted())
        {
        }
        else
        {
            /*
            if
            (this->game.getNetwork()->getLobbyNetwork()->getPlayerCollectionReplica()->getPlayersLoadingComplete())
            {
                debugOutLevel(12, "Got Loading complete command - start simulating");
                RakSleep(1000 -
            this->game.getNetwork()->getPeer()->GetLastPing(this->game.getConfiguration().getSettings().connectedServerAddress)
            / 2);
                this->game.setGameStarted(true);
            }*/
        }
    }
    else // TODO handle dc
    {
        Error::errContinue("Connection is broken");
    }
    // gameChatObj->checkShouldMoveChat();
    // gameChatObj->checkShouldSetChatTextUnvisible();

    const uint32_t tickNumber = 0;

    this->networkComponentSyncronization->applyReceivedComponents();

    this->networkEventSyncronization->throwNetworkReceivedEvents(tickNumber);

    debugOutLevel(Debug::DebugLevels::updateLoop + 1, "Updating ec-Systems");
    this->movementSystem->update(tickNumber,
                                 this->game.getScriptingModule()->getScriptEngine(),
                                 this->game.getDeltaTime());

    this->graphicSystem->update(tickNumber,
                                this->game.getScriptingModule()->getScriptEngine(),
                                this->game.getDeltaTime());

    this->selectionSystem->update(tickNumber,
                                  this->game.getScriptingModule()->getScriptEngine(),
                                  this->game.getDeltaTime());

    this->projectileSystem->update(tickNumber,
                                   this->game.getScriptingModule()->getScriptEngine(),
                                   this->game.getDeltaTime());

    this->cooldownSystem->update(tickNumber,
                                 this->game.getScriptingModule()->getScriptEngine(),
                                 this->game.getDeltaTime());

    this->damageSystem->update(tickNumber,
                               this->game.getScriptingModule()->getScriptEngine(),
                               this->game.getDeltaTime());

    this->automaticWeaponSystem->update(tickNumber,
                                        this->game.getScriptingModule()->getScriptEngine(),
                                        this->game.getDeltaTime());

    this->shopSystem->update(tickNumber,
                             this->game.getScriptingModule()->getScriptEngine(),
                             this->game.getDeltaTime());

    this->inventorySystem->update(tickNumber,
                                  this->game.getScriptingModule()->getScriptEngine(),
                                  this->game.getDeltaTime());

    this->moneyTransferSystem->update(tickNumber,
                                      this->game.getScriptingModule()->getScriptEngine(),
                                      this->game.getDeltaTime());

    this->healthBarSystem->update(tickNumber,
                                  this->game.getScriptingModule()->getScriptEngine(),
                                  this->game.getDeltaTime());

    this->shopGuiSystem->update(tickNumber,
                                this->game.getScriptingModule()->getScriptEngine(),
                                this->game.getDeltaTime());

    this->inventoryGuiSystem->update(tickNumber,
                                     this->game.getScriptingModule()->getScriptEngine(),
                                     this->game.getDeltaTime());

    this->pathingSystem->update(tickNumber,
                                this->game.getScriptingModule()->getScriptEngine(),
                                this->game.getDeltaTime());
}

// ---------------------------------------------------------------------------
bool GameState::onEvent(const irr::SEvent& event)
{
    this->game.getSceneManager()->getActiveCamera()->OnEvent(event);

    // is a different than the default context active which should get handled the event?
    if (this->inputContextManager->onActiveContextEvent(event))
    {
        return true;
    }

    // Pass the event on to cegui, see if it wants to handle it.
    // CEGUI crashes on key codes > irr::KEY_KEY_CODES_COUNT, so don't pass them on.
    if (event.EventType != irr::EET_KEY_INPUT_EVENT or event.KeyInput.Key <= irr::KEY_KEY_CODES_COUNT)
    {
        if (this->game.getGuiEnvironment()->getRenderer()->injectEvent(event)) // CEGUI-EventReceiver
        {
            debugOutLevel(100, "handled by cegui");
            return true; // use CEGUI EVENTHANDLER
        }
    }

    if (this->inputContextManager->onDefaultContextEvent(event))
    {
        return true;
    }

    if (event.KeyInput.Key == irr::KEY_RETURN and not event.KeyInput.PressedDown)
    {
        /*
           if (gameChatObj->isVisibleGui)
           {
               debugOutLevel(50, "close Chat Window");
               gameChatObj->setVisible(false);
               gameChatObj->setVisible(true, "StaticText", L"ChatMessageViewer"); //text should be still visible
           }
           else
           {
               debugOutLevel(50, "open Chat Window");
               gameChatObj->setVisible(true);
               gameChatObj->setFocusOn(game, L"EditBox", L"Text_Box");
           }*/
    }
    if (event.EventType == irr::EET_KEY_INPUT_EVENT)
    {
        if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_R && !event.KeyInput.PressedDown)
        {
            debugOutLevel(20, "Create Replica");
        }
        else if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_T && !event.KeyInput.PressedDown)
        {
            debugOutLevel(20, "Create Replica");
        }
    }

    if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
    {
        if (event.MouseInput.Event == irr::EMIE_LMOUSE_PRESSED_DOWN)
        {
            this->selectNode();
            return true;
        }
        else if (event.MouseInput.Event == irr::EMIE_RMOUSE_PRESSED_DOWN)
        {
            return true;
        }
    }

    return false;
}

/**************************************************************
 * private memberfunctions
 **************************************************************/


void GameState::initCamera()
{
    this->rtsanim =
        new RTSCameraAnimator(this->game.getDevice()->getCursorControl(), 1.0f, 10.0f, 30.0f, 2000.0f);

    this->rtsanim->setBounds(
        irr::core::vector2df(this->gameMap->getMapPosition().X,
                             this->gameMap->getMapSize().X + this->gameMap->getMapPosition().X),
        irr::core::vector2df(this->gameMap->getMapPosition().Z,
                             this->gameMap->getMapSize().Y + this->gameMap->getMapPosition().Z));
    this->game.getCamera()->setPosition(irr::core::vector3df(0, 600, 300));
    this->game.getCamera()->setTarget(irr::core::vector3df(0, 0, 0));
    this->game.getCamera()->addAnimator(this->rtsanim);
    this->game.getSceneManager()->setActiveCamera(this->game.getCamera());
    this->rtsanim->drop();

    this->game.getSceneManager()->setAmbientLight(irr::video::SColorf(0.6f, 0.6f, 0.6f));
    // LIGHTINGSOURCE
    this->game.getSceneManager()->addLightSceneNode(nullptr,
                                                    irr::core::vector3df(150.0f, 800.0f, 0),
                                                    irr::video::SColorf(1.0f, 1.0f, 1.0f, 1.0f),
                                                    500.0f);

    /// TODO: Implement EVSM instead of VSM
    gameMap->getTerrainNode()->getMaterial(0).Lighting = false;

    // this->game.getGraphics()->effectHandler->addShadowToNode(gameMap->getTerrainNode(),
    // filterType);

    // this->game.getGraphics()->effectHandler->addShadowToNode(gc->node, filterType);

    // this->game.getGraphics()->effectHandler->setAmbientColor(irr::video::SColor(255, 32, 32,
    // 32));
    // Set the background clear color to black.
    // this->game.getGraphics()->effectHandler->setClearColour(irr::video::SColor(255, 0, 0, 0));

    // Add ShadowLight
    // The first parameter specifies the shadow map resolution for the shadow light.
    // The shadow map is always square, so you need only pass 1 dimension, preferably
    // a power of two between 512 and 2048, maybe larger depending on your quality
    // requirements and target hardware. We will just pass the value the user picked.
    // The second parameter is the light position, the third parameter is the (look at)
    // target, the next is the light color, and the next two are very important
    // values, the nearValue and farValue, be sure to set them appropriate to the current
    // scene. The last parameter is the FOV (Field of view), since the light is similar to
    // a spot light, the field of view will determine its area of influence. Anything that
    // is outside of a lights frustum (Too close, too far, or outside of it's field of view)
    // will be unlit by this particular light, similar to how a spot light works.
    //    irr::core::vector3df lightDirection =
    //        irr::core::vector3df(1500, 0, 1500) - irr::core::vector3df(2100, 2000, 1900);
    // irr::f32 viewFrustumExtent =
    // this->game.getCamera()->getViewFrustum()->getBoundingBox().getExtent().getLength()/2;
    // irr::f32 angleToXZPlane = (this->game.getCamera()->getTarget() -
    // this->game.getCamera()->getPosition())
    //                              .getHorizontalAngle()
    //                              .X;
    // irr::f32 lightBoxExtent = fabsf(4 * tanf(this->game.getCamera()->getFOV() / 2.f) *
    //                                this->game.getCamera()->getPosition().Y /
    //                                (cosf(irr::core::PI / 2.f - angleToXZPlane *
    //                                irr::core::DEGTORAD +
    //                                      this->game.getCamera()->getFOV() / 2.f)));
    // irr::core::matrix4 lightProjMatrix = {};
    // lightProjMatrix.buildProjectionMatrixOrthoLH(lightBoxExtent, lightBoxExtent, -1.f, 6000.f);
    // debugOutLevel(Debug::DebugLevels::stateInit,
    //              "Extent of ViewFrustum BBox: ",
    //              lightBoxExtent,
    //              "AngleToXZPlane: ",
    //              angleToXZPlane,
    //              "FOV: ",
    //              this->game.getCamera()->getFOV());
    // this->game.getGraphics()->effectHandler->addShadowLight(
    //    SShadowLight(2048,
    //                 irr::core::vector3df(2100, 2000, 1900),
    //                 irr::core::vector3df(1500, 0, 1500),
    //                 irr::video::SColor(0, 255, 150, 40),
    //                 -1.f,
    //                 6000.0f,
    //                 lightBoxExtent,
    //                 true)); // 150.0f * irr::core::DEGTORAD
}

// ---------------------------------------------------------------------------
void GameState::initEntitySystem()
{
    // Init components parsing and memory management
    std::map<componentID_t, ComponentParsing::IComponentParser*> parsers;

    this->entityManager = new EntityManager();

    // Components::DebugColor
    parsers[Components::ComponentID<Components::DebugColor>::CID] =
        new ComponentParsing::GenericComponentParser<Components::DebugColor>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::DebugColor>(this->handleManager)));

    // Components::Position
    parsers[Components::ComponentID<Components::Position>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Position>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Position>(this->handleManager)));

    // Components::Movement
    parsers[Components::ComponentID<Components::Movement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Movement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Movement>(this->handleManager)));

    // Components::Graphic
    parsers[Components::ComponentID<Components::Graphic>::CID] =
        new ComponentParsing::GraphicComponentParser<Components::Graphic>(this->handleManager,
                                                                          this->game.getSceneManager());
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Graphic>(this->handleManager)));

    // Components::ProjectileMovement
    parsers[Components::ComponentID<Components::ProjectileMovement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::ProjectileMovement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::ProjectileMovement>(this->handleManager)));

    // Components::Rotation
    parsers[Components::ComponentID<Components::Rotation>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Rotation>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Rotation>(this->handleManager)));

    // Components::EntityName
    parsers[Components::ComponentID<Components::EntityName>::CID] =
        new ComponentParsing::GenericComponentParser<Components::EntityName>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::EntityName>(this->handleManager)));

    // Components::Price
    parsers[Components::ComponentID<Components::Price>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Price>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Price>(this->handleManager)));

    // Components::Damage
    parsers[Components::ComponentID<Components::Damage>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Damage>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Damage>(this->handleManager)));

    // Components::Range
    parsers[Components::ComponentID<Components::Range>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Range>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Range>(this->handleManager)));

    // Components::Ammo
    parsers[Components::ComponentID<Components::Ammo>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::Ammo>(this->handleManager,
                                                                           this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Ammo>(this->handleManager)));

    // Components::Icon
    parsers[Components::ComponentID<Components::Icon>::CID] =
        new ComponentParsing::GraphicComponentParser<Components::Icon>(this->handleManager,
                                                                       this->game.getSceneManager());
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Icon>(this->handleManager)));

    // Components::Inventory
    parsers[Components::ComponentID<Components::Inventory>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Inventory>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Inventory>(this->handleManager)));

    // Components::PurchasableItems
    parsers[Components::ComponentID<Components::PurchasableItems>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::PurchasableItems>(this->handleManager,
                                                                                       this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PurchasableItems>(this->handleManager)));

    // Components::Requirement
    parsers[Components::ComponentID<Components::Requirement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Requirement>(this->handleManager);
    ;
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Requirement>(this->handleManager)));

    // Components::SelectionID
    parsers[Components::ComponentID<Components::SelectionID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::SelectionID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::SelectionID>(this->handleManager)));

    // Components::Cooldown
    parsers[Components::ComponentID<Components::Cooldown>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Cooldown>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Cooldown>(this->handleManager)));

    // Components::Physical
    parsers[Components::ComponentID<Components::Physical>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Physical>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Physical>(this->handleManager)));

    // Components::OwnerEntityID
    parsers[Components::ComponentID<Components::OwnerEntityID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::OwnerEntityID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::OwnerEntityID>(this->handleManager)));

    // Components::WeaponEntityID
    parsers[Components::ComponentID<Components::WeaponEntityID>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::WeaponEntityID>(this->handleManager,
                                                                                     this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::WeaponEntityID>(this->handleManager)));

    // Components::DroppedItemEntityID
    parsers[Components::ComponentID<Components::DroppedItemEntityID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::DroppedItemEntityID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::DroppedItemEntityID>(this->handleManager)));

    // Components::PlayerStats
    parsers[Components::ComponentID<Components::PlayerStats>::CID] =
        new ComponentParsing::GenericComponentParser<Components::PlayerStats>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PlayerStats>(this->handleManager)));

    // Components::PlayerBase
    parsers[Components::ComponentID<Components::PlayerBase>::CID] =
        new ComponentParsing::GenericComponentParser<Components::PlayerBase>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PlayerBase>(this->handleManager)));

    // create ECS Systems and bind them to components
    this->movementSystem = new MovementSystem(*this->game.getThreadPool(),
                                              *this->eventManager,
                                              *this->entityManager,
                                              this->handleManager,
                                              this->gameMap,
                                              *(this->grid),
                                              this->game);
    std::unique_ptr<SystemBase> movementSystemTransfer(this->movementSystem);
    this->entityManager->bindSystem(movementSystemTransfer);

    this->pathingSystem = new PathingSystem(*this->game.getThreadPool(),
                                            *this->eventManager,
                                            *this->entityManager,
                                            this->handleManager,
                                            this->gameMap,
                                            *(this->grid),
                                            this->game);
    std::unique_ptr<SystemBase> pathingSystemTransfer(this->pathingSystem);
    this->entityManager->bindSystem(pathingSystemTransfer);

    this->graphicSystem = new GraphicSystem(*this->game.getThreadPool(),
                                            *this->eventManager,
                                            *this->entityManager,
                                            this->handleManager,
                                            this->gameMap,
                                            *this->grid,
                                            this->game);
    std::unique_ptr<SystemBase> graphicSystemTransfer(this->graphicSystem);
    this->entityManager->bindSystem(graphicSystemTransfer);

    this->selectionSystem = new SelectionSystem(*this->game.getThreadPool(),
                                                *this->eventManager,
                                                *this->entityManager,
                                                this->handleManager,
                                                *this->inputContextManager,
                                                this->gameMap,
                                                *this->grid,
                                                this->game);
    std::unique_ptr<SystemBase> selectionSystemTransfer(this->selectionSystem);
    this->entityManager->bindSystem(selectionSystemTransfer);

    this->projectileSystem = new ProjectileSystem(*this->game.getThreadPool(),
                                                  *this->eventManager,
                                                  *this->entityManager,
                                                  this->handleManager,
                                                  this->gameMap);
    std::unique_ptr<SystemBase> projectileSystemTransfer(this->projectileSystem);
    this->entityManager->bindSystem(projectileSystemTransfer);

    this->cooldownSystem =
        new CooldownSystem(*this->game.getThreadPool(), *this->eventManager, this->handleManager);
    std::unique_ptr<SystemBase> cooldownSystemTransfer(this->cooldownSystem);
    this->entityManager->bindSystem(cooldownSystemTransfer);

    this->damageSystem = new DamageSystem(*this->game.getThreadPool(), *this->eventManager, this->handleManager);
    std::unique_ptr<SystemBase> damageSystemTransfer(this->damageSystem);
    this->entityManager->bindSystem(damageSystemTransfer);

    this->automaticWeaponSystem = new AutomaticWeaponSystem(*this->game.getThreadPool(),
                                                            *this->eventManager,
                                                            *this->entityManager,
                                                            this->handleManager,
                                                            this->blueprintCollection);
    std::unique_ptr<SystemBase> automaticWeaponSystemTransfer(this->automaticWeaponSystem);
    this->entityManager->bindSystem(automaticWeaponSystemTransfer);

    this->shopSystem = new ShopSystem(*this->game.getThreadPool(),
                                      *this->eventManager,
                                      *this->entityManager,
                                      this->handleManager,
                                      this->blueprintCollection);
    std::unique_ptr<SystemBase> shopSystemTransfer(this->shopSystem);
    this->entityManager->bindSystem(shopSystemTransfer);

    this->inventorySystem = new InventorySystem(*this->game.getThreadPool(),
                                                *this->eventManager,
                                                *this->entityManager,
                                                this->handleManager,
                                                this->blueprintCollection);
    std::unique_ptr<SystemBase> inventorySystemTransfer(this->inventorySystem);
    this->entityManager->bindSystem(inventorySystemTransfer);

    this->moneyTransferSystem =
        new MoneyTransferSystem(*this->game.getThreadPool(), *this->eventManager, *this->entityManager, this->handleManager);
    std::unique_ptr<SystemBase> moneyTransferSystemTransfer(this->moneyTransferSystem);
    this->entityManager->bindSystem(moneyTransferSystemTransfer);

    // GUI-ec-Systems
    this->healthBarSystem = new HealthBarSystem(*this->game.getThreadPool(),
                                                *this->eventManager,
                                                *this->entityManager,
                                                this->handleManager,
                                                this->game);
    std::unique_ptr<SystemBase> healthBarSystemTransfer(this->healthBarSystem);
    this->entityManager->bindSystem(healthBarSystemTransfer);

    this->shopGuiSystem = new ShopGuiSystem(*this->game.getThreadPool(),
                                            *this->eventManager,
                                            *this->entityManager,
                                            this->handleManager,
                                            this->blueprintCollection,
                                            this->myPlayerProfileID);
    std::unique_ptr<SystemBase> shopGuiSystemTransfer(this->shopGuiSystem);
    this->entityManager->bindSystem(shopGuiSystemTransfer);

    this->inventoryGuiSystem = new InventoryGuiSystem(*this->game.getThreadPool(),
                                                      *this->eventManager,
                                                      *this->entityManager,
                                                      this->handleManager,
                                                      this->myPlayerProfileID,
                                                      this->gameMap,
                                                      *this->game.getGuiEnvironment());
    std::unique_ptr<SystemBase> inventoryGuiSystemTransfer(this->inventoryGuiSystem);
    this->entityManager->bindSystem(inventoryGuiSystemTransfer);


    // Load blueprints
    irr::io::path pre_path = "data/ecs/";
    irr::io::path const dafaultWorkingDirectory =
        this->game.getDevice()->getFileSystem()->getWorkingDirectory().c_str();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory + "/" + pre_path);
    irr::io::IFileList* fileList = this->game.getDevice()->getFileSystem()->createFileList();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory);

    std::string fileString = "";
    irr::io::path file = "";
    irr::io::path extension = "";
    for (unsigned int i = 2; i < fileList->getFileCount(); i++)
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".json")
            {
                fileString = std::string(irr::core::stringc(file).c_str());
                debugOut("Found jason-file '", fileString, "'");
                entityFactory->registerBlueprintsFromJson(fileString, this->blueprintNameToIDMap);
            }
        }
    }
    for (unsigned int i = 2; i < fileList->getFileCount(); i++)
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".json")
            {
                fileString = std::string(irr::core::stringc(file).c_str());
                debugOut("Load jason-file '", fileString, "'");
                entityFactory->createBlueprintsFromJson(fileString, *this->entityManager, parsers, this->blueprintCollection);
                debugOut("BlueprintCount: ", this->blueprintCollection.size());
            }
        }
    }

    // the fist 10 entitys should be the playerProfiles
    // i think the best way should be to create them by the server/ load them from the map
    for (int i = 0; i < 10; i++)
    {
        entityID_t playerProfileID = this->entityManager->addEntity();
        debugOut("playerProfileID", playerProfileID);
        this->entityManager->addComponent<Components::PlayerBase>(playerProfileID);
        this->entityManager->addComponent<Components::PlayerStats>(playerProfileID);
        this->entityManager->addComponent<Components::Inventory>(playerProfileID);
    }
    // the next 10 entitys should be the start tanks
    // i think the best way should be to create them by the server/ load them from the map
    for (int i = 0; i < 10; i++) // 10
    {
        debugOut("place start unit/tank for player", i + 1);
        const auto blueprintID = this->blueprintNameToIDMap["Titan"];

        Components::SelectionID selectionID;
        selectionID.team = Components::SelectionID::Team(int(i / 5 + 1));
        selectionID.selectionType = Components::SelectionID::SelectionType(int(i + 1));

        Components::OwnerEntityID ownerEntityID;
        ownerEntityID.ownerEntityID = i;

        Components::Position tankPosition;
        tankPosition.position.X = 1500.f - i * 150.0f;
        tankPosition.position.Y = 150.f + i * 150.0f;

        Components::Rotation rotation;
        rotation.rotation = {};

        std::unordered_map<componentID_t, const void* const> initialValues;
        initialValues.insert(std::pair<componentID_t, const void* const>(
            Components::ComponentID<Components::SelectionID>::CID, &selectionID));
        initialValues.insert(std::pair<componentID_t, const void* const>(
            Components::ComponentID<Components::OwnerEntityID>::CID, &ownerEntityID));
        initialValues.insert(
            std::pair<componentID_t, const void* const>(Components::ComponentID<Components::Position>::CID,
                                                        &tankPosition));
        initialValues.insert(
            std::pair<componentID_t, const void* const>(Components::ComponentID<Components::Rotation>::CID,
                                                        &rotation));

        constexpr bool actAsIfCreatedByServer =
            true; // TODO: remove this dirty hack once there is a better method to make sure
                  // client/server are loading the same data (e.g. by making them both load the same
                  // map to initialize the ECS
        const entityID_t tankEntityID =
            this->blueprintCollection[int(blueprintID)].first.createEntity(initialValues, actAsIfCreatedByServer);

        const auto playserH = this->entityManager->getComponent<Components::PlayerBase>(i);
        auto& player = this->handleManager.getAsRef<Components::PlayerBase>(playserH);
        player.tankEntityID = tankEntityID;
    }


    // place shops on the map //TESTING --> later placed by map loading
    std::string shops[] = {std::string("Tank Construction Hall"),
                           std::string("Weapon Factory I"),
                           std::string("Weapon Factory II")};

    float pos = 1000.f;
    float deltaPos = 1000.f;
    for (const std::string& blueprintKey : shops)
    {
        debugOut("place shops: ", blueprintKey);
        auto blueprintID = this->blueprintNameToIDMap[blueprintKey];

        Components::SelectionID selectionID;
        selectionID.team = Components::SelectionID::Team::Team1;
        if (blueprintKey == std::string("Weapon Factory II"))
        {
            selectionID.team = Components::SelectionID::Team::Team2;
        }
        selectionID.selectionType = Components::SelectionID::SelectionType::ItemShop;

        Components::Position shopPosition;
        shopPosition.position.X = pos;
        shopPosition.position.Y = pos;
        pos = pos + deltaPos;

        std::unordered_map<componentID_t, const void* const> initialValues;
        initialValues.insert(std::pair<componentID_t, const void* const>(
            Components::ComponentID<Components::SelectionID>::CID, &selectionID));
        initialValues.insert(
            std::pair<componentID_t, const void* const>(Components::ComponentID<Components::Position>::CID,
                                                        &shopPosition));

        constexpr bool actAsIfCreatedByServer = true;
        this->blueprintCollection[int(blueprintID)].first.createEntity(initialValues, actAsIfCreatedByServer);
    }
}

// ---------------------------------------------------------------------------
void GameState::initLuaScriptEngine()
{
    ScriptEngine& scriptEngine = this->game.getScriptingModule()->getScriptEngine();
    // define lua variables
    scriptEngine.loopOverInstances(
        [this](sol::state& instance) {
            // std::ref for non-ptr members
            instance["entityManager"] = std::ref(this->entityManager);
            instance["handleManager"] = std::ref(this->handleManager);
            instance["eventManager"] = std::ref(this->eventManager);
            instance["terrainNode"] = this->gameMap->getTerrainNode();
            instance["grid"] = this->grid;
        },
        ScriptEngine::ForceLoop::True);

    irr::io::path pre_path = "scripts/";
    irr::io::path const dafaultWorkingDirectory =
        this->game.getDevice()->getFileSystem()->getWorkingDirectory().c_str();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory + "/" + pre_path);
    irr::io::IFileList* fileList = this->game.getDevice()->getFileSystem()->createFileList();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory);

    std::string fileString = "";
    irr::io::path file = "";
    irr::io::path extension = "";
    for (unsigned int i = 2; i < fileList->getFileCount(); i++)
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".lua")
            {
                fileString = std::string(irr::core::stringc(file).c_str());
                debugOut("Load lua-file '", fileString, "'");
                this->game.getScriptingModule()->loadScriptFile(fileString);
            }
        }
    }
}

// ---------------------------------------------------------------------------
void GameState::initNetwork()
{
    this->lastReplicaListSize = this->game.getNetwork()->getReplicaManager()->GetReplicaCount();

    this->game.getNetwork()->getReplicaManager()->registerAllocationCallback(
        Networking::AllocationID::NetworkEventSyncronization, [this]() {
            debugOutLevel(Debug::DebugLevels::stateInit,
                          "received allocation of network event "
                          "syncronization -> creating local copy of "
                          "network event syncronization");
            this->networkEventSyncronization =
                new NetworkEventSyncronizationClient(this->game.getNetwork()->getReplicaManager(),
                                                     *this->eventManager,
                                                     *this->game.getThreadPool());
            return this->networkEventSyncronization;
        });

    std::vector<IComponentSerializerBase*> componentSerializers(Components::NumComponents, nullptr);
    componentSerializers[Components::ComponentID<Components::Position>::CID] =
        new TrivialComponentSerializer<Components::Position>(); // TODO: stop leaking this memory
    componentSerializers[Components::ComponentID<Components::Rotation>::CID] =
        new TrivialComponentSerializer<Components::Rotation>(); // TODO: stop leaking this memory

    this->game.getNetwork()->getReplicaManager()->registerAllocationCallback(
        Networking::AllocationID::NetworkComponentSyncronization, [this, componentSerializers]() {
            debugOutLevel(Debug::DebugLevels::stateInit,
                          "received allocation of NetworkComponentSyncronization -> "
                          "creating local copy of NetworkComponentSyncronization");
            this->networkComponentSyncronization =
                new NetworkComponentSyncronizationClient(*this->entityManager, this->handleManager, componentSerializers);
            return this->networkComponentSyncronization;
        });

    // Loading Complete
    // this->game.getNetwork()->getPlayer()->setLoadingComplete(true);

    // the server will read the 'ready' flag in the playerCollectionReplica and send us a command if
    // everyone else is ready, too
    this->game.getPlayerCollectionReplica()->setReadyFlag(true);

    // until the server has send us the command to start the game refresh the network with a higher
    // tickrate and check for that command
    while (this->gameNetworkObj->getServerRequestedPause().type == PauseRequest::Paused)
    {
        this->gameNetworkObj->processPackets();
        this->game.sleep(static_cast<uint32_t>(1000.0f / this->game.getTickrate() / 3.0f)); // update 3 times
                                                                                            // as fast as the
                                                                                            // normal update
                                                                                            // loop
        debugOutLevel(Debug::DebugLevels::updateLoop + 1, "waiting for the server and other clients to become ready");
    }
    // TODO:
    // if the updates align unlucky the update() call might come up to 1/tickrate too late -> write
    // the time we have received the command to start and wait the difference inside the update loop
    // of the 0-th tick

    // we got the command to start in X sec. But that command took at least half a ping time to get
    // here -> substract that away
    const RakNet::TimeMS ping =
        this->game.getNetwork()->getPeer()->GetAveragePing(this->game.getNetwork()->serverGUID);
    const RakNet::TimeMS serverWait = this->gameNetworkObj->getServerRequestedPause().waitTime;
    RakNet::TimeMS waitTime = serverWait - ping / 2;
    if (ping / 2 > serverWait)
    {
        Error::errContinue("ping to the server was higher than server waited to start the game -> "
                           "the server has already started the game at this poin -> starting "
                           "immediately");
        waitTime = 0;
    }
    if (waitTime > BattleTanks::MultipleStatesConstants::AbsoluteMaximumStartOrPauseWaitTime)
    {
        Error::errContinue("Ping-adjusted wait time to start the game is",
                           waitTime,
                           "ms but the absolute maximum wait time is",
                           BattleTanks::MultipleStatesConstants::AbsoluteMaximumStartOrPauseWaitTime,
                           "ms. Ignoring server request and waiting the maximum!");
        waitTime = BattleTanks::MultipleStatesConstants::AbsoluteMaximumStartOrPauseWaitTime;
    }

    debugOutLevel(Debug::DebugLevels::stateInit,
                  "sleeping for",
                  waitTime,
                  "before starting the state because our ping was",
                  this->game.getNetwork()->getPeer()->GetAveragePing(this->game.getNetwork()->serverGUID));
    this->game.sleep(waitTime);

    this->game.setTickNumber(0);
    this->game.setGameStarted(true);
}

// ---------------------------------------------------------------------------
void GameState::selectNode()
{
    irr::core::line3df ray = this->game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
        this->game.getDevice()->getCursorControl()->getPosition(),
        this->game.getSceneManager()->getActiveCamera());
    irr::scene::ISceneNode* node =
        this->game.getSceneManager()->getSceneCollisionManager()->getSceneNodeFromRayBB(ray, 1);

    if (node != nullptr)
    {
        printf("NODE SELECT ID:%i\n", int(node->getID()));

        if (node->getID() == 1)
        {
            node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
            selectednode = static_cast<irr::scene::IAnimatedMeshSceneNode*>(node);
        }
        else if (node->getID() == -1)
        {
            if (selectednode)
            {
                selectednode->setMaterialFlag(irr::video::EMF_LIGHTING, true);
                selectednode = nullptr;
            }
        }

        if (selectednode)
        {
            printf("selected\n");
            selectednode->setMD2Animation(irr::scene::EMAT_SALUTE);
        }
    }
    else
    {
        printf("NO NODE Selected\n");
        if (selectednode)
        {
            selectednode->setMaterialFlag(irr::video::EMF_LIGHTING, true);
        }

        selectednode = nullptr;
        // lastnode->setMaterialFlag(irr::video::EMF_LIGHTING, true);
    }
}
