/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menustate.h"

// ----------------------------------------------------------------------------
#include <CEGUI/RendererModules/Irrlicht/Renderer.h>
#include <CEGUI/ScriptModules/Lua/ScriptModule.h>
#include <CEGUI/Window.h>
#include <irrlicht/IrrlichtDevice.h>
#include <sol.hpp>

#include "../../audio/soundEngine.h"
#include "../../core/config.h"
#include "../../core/game.h"
#include "../../gui/ceGuiEnvironment.h"
#include "../../scripting/scriptingModule.h"
#include "../../utils/static/error.h"

// ----------------------------------------------------------------------------
MenuState::MenuState(Game& game_)
    : State(game_)
{
    // ctor
}

// ---------------------------------------------------------------------------
MenuState::~MenuState()
{
    // dtor
}

// ---------------------------------------------------------------------------
void MenuState::onEnter()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Entering MenuState");

    //--START-- Network modification // nothing to do, no Network needed here
    //--ENDE--

    // define lua variables
    this->game.getScriptingModule()->getScriptEngine().loopOverInstances(
        [this](sol::state& instance) {
            // std::ref for non-ptr members
            instance["game"] = std::ref(this->game);
            instance["config"] = std::ref(this->game.getConfiguration());
            instance["device"] = std::ref(*this->game.getDevice());
            instance["driver"] = std::ref(*this->game.getVideoDriver());
            instance["scriptingModule"] = std::ref(*this->game.getScriptingModule());
        },
        ScriptEngine::ForceLoop::True);

    //--START-- Create GUI-Object
    debugOutLevel(20, "Initialise MainMenuGui");
    this->game.getGuiEnvironment()->initGuiElement("MainMenuGui");
    //--ENDE--

    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "------------------Menustate initialized");
}

// ---------------------------------------------------------------------------
void MenuState::onLeave()
// Dont delete created GUI-Objects here, we dont wont reload the Gui
// while switching between the Menu-,Lobby, and ServerbrowserState
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Leaving MenuState");
    // Just set Invisible
    CEGUI::System::getSingleton().getScriptingModule()->executeString(
        "MainMenuGui.root:setVisible(false)");
}

// ---------------------------------------------------------------------------
void MenuState::update() // Empty
{
}

// ---------------------------------------------------------------------------
bool MenuState::onEvent(const irr::SEvent& event)
{
    if (event.EventType != irr::EET_KEY_INPUT_EVENT or event.KeyInput.Key <= irr::KEY_KEY_CODES_COUNT)
    {
        if (this->game.getGuiEnvironment()->getRenderer()->injectEvent(event)) // CEGUI-EventReceiver
        {
            return true; // use CEGUI EVENTHANDLER
        }
    }
    else
    {
        debugOutLevel(20, "For CEGUI invalid Keycode NOT in CEGUI injected: ", event.KeyInput.Key);
        return true; // No key we need to handle (mostly Volume up and down keys)
    }

    return false;
}

/**************************************************************
 * private memberfunctions
 **************************************************************/
