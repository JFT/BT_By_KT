/*Code based on the
game states / ODE / freeflight / XML io all - in - one demo from randomMesh:
http://irrlicht.sourceforge.net/forum/viewtopic.php?f=9&t=26526
released under the Irrlicht license.
Read the licenses/irrlicht.license.txt for more information
*/

#ifndef STATE_H
#define STATE_H

class Game;
namespace irr
{
    struct SEvent;
}

/*! \class StateT
 *
 *  This is the state base class.
 *  All states that are used with the state machine will inherit from this state, and the template
 * parameters for
 *  the state and the state graph [machine] will be the same.
 */
class State
{
   public:
    State(Game& game_)
        : game(game_)
    {
    }

    State(const State&) = delete;
    State& operator=(const State&) = delete;

    virtual ~State() {}
    /*! Enter this state.
     *
     * Called by the state machine when entity is entering this state.
     *
     * \param entity The entity that is entering this state.
     */
    virtual void onEnter() {}
    /*! Leave this state.
     *
     * Called by the state machine when entity is leaving this state.
     *
     * \param entity The entity that is leaving this state.
     */
    virtual void onLeave() {}
    /*! Update the entity state.
     *
     * \param entity The entity that is to be updated.
     */
    virtual void update() {}
    /*! Process a message.
     *
     * Called by the state machine when a message is to be processed for
     * an entity.
     *
     * \param entity The entity that message is destined for.
     * \param event The event to process.
     * \return Returns true if the message was processed, otherwise returns
     * false.
     */
    virtual bool onEvent(const irr::SEvent&) { return false; }

   protected:
    Game& game;
};

#endif // STATE_H
