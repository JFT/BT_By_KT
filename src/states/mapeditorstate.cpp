/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../utils/stringwstream.h" // stringwstream.h needs to be included before debug.h and error.h because they use use operator<< on a stringw. (error.h and debug.h are included by various files)

#include "mapeditorstate.h"
// ---------------------------------------------------------------------------

#include <CEGUI/widgets/Editbox.h>
#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/ISceneCollisionManager.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/IrrlichtDevice.h>
#include <irrlicht/dimension2d.h>
#include <irrlicht/line3d.h>
#include "../core/game.h"
#include "../ecs/componentParsers.h"
#include "../ecs/componentSerializer.h"
#include "../ecs/components.h"
#include "../ecs/entityBlueprint.h"
#include "../ecs/entityFactory.h"
#include "../ecs/entityManager.h"
#include "../ecs/events/eventManager.h"
#include "../ecs/savingLoading/savingLoading.h"
#include "../ecs/storageSystems/allEntitiesComponentValueStorage.h"
#include "../ecs/systems/graphicSystem.h"
#include "../ecs/systems/testSystem.h"
#include "../ecs/userinput/inputContextManager.h"
#include "../graphics/ImageManipulation.h" // needed to create an IImage* from the ITexture* of the splatMap (for the TextureDrawer)
#include "../graphics/decals/DecalManager.h"
#include "../graphics/graphicsPipeline.h"
#include "../graphics/textureArray.h"
#include "../grid/grid.h"
#include "../gui/ceGuiEnvironment.h"
#include "../gui/mapeditorstate/mapEditorGui.h"
#include "../gui/multiplestates/messageBoxGui.h"
#include "../map/gamemap.h"
#include "../map/mapLoader.h"
#include "../map/terrainShader.h"
#include "../map/terrainVertexSelector.h"
#include "../map/terrainscenenode.h"
#include "../mapeditor/mapEditorBindings.hpp"
#include "../mapeditor/mapeditorcameraanimator.h"
#include "../mapeditor/terrainCollisionManager.h"
#include "../mapeditor/texturedrawer.h"
#include "../scripting/scriptingModule.h"
#include "../utils/printfunctions.h"
#include "../utils/static/conversions.h"


// ---------------------------------------------------------------------------
MapEditorState::MapEditorState(Game& game_)
    : State(game_)
    , guiLuaState(game_.getScriptingModule()->getScriptEngine().getSolState(0))
    , selectedBlueprint()
    , handleManager()
    , posSync()
    , offsetSync()
    , rotationSync()
    , scaleSync()
    , mouseState()
    , floatIntensities()
    , pixelWorldCoords()
    , pixelWorldNormals()
    , selectedTriangles()
    , selectedTriangleVertexIndices()

{
    /// TODO: change location of the intialization of lambda functions
    this->posSync.addUser(this, [this](const irr::core::vector2df newPos) {
        if (this->selectedEntity != ECS::INVALID_ENTITY and this->entityManager != nullptr)
        {
            const auto pHandle = this->entityManager->getComponent<Components::Position>(this->selectedEntity);
            if (pHandle != Handles::InvalidHandle)
            {
                auto& pos = this->handleManager.getAsRef<Components::Position>(pHandle).position;
                pos = newPos;
            }
        }
    });

    this->offsetSync.addUser(this, [this](const irr::f32 newOffset) {
        if (this->selectedEntity != ECS::INVALID_ENTITY and this->entityManager != nullptr)
        {
            const auto gHandle = this->entityManager->getComponent<Components::Graphic>(this->selectedEntity);
            if (gHandle != Handles::InvalidHandle)
            {
                auto& graphic = this->handleManager.getAsRef<Components::Graphic>(gHandle);
                graphic.offset = newOffset;
            }
        }
    });

    this->rotationSync.addUser(this, [this](const irr::core::vector3df newRotation) {
        if (this->selectedEntity != ECS::INVALID_ENTITY and this->entityManager != nullptr)
        {
            const auto rHandle = this->entityManager->getComponent<Components::Rotation>(this->selectedEntity);
            if (rHandle != Handles::InvalidHandle)
            {
                auto& rotation = this->handleManager.getAsRef<Components::Rotation>(rHandle);
                rotation.rotation = newRotation;
            }
        }
    });

    this->scaleSync.addUser(this, [this](const irr::core::vector3df newScale) {
        if (this->selectedEntity != ECS::INVALID_ENTITY and this->entityManager != nullptr)
        {
            const auto gHandle = this->entityManager->getComponent<Components::Graphic>(this->selectedEntity);
            if (gHandle != Handles::InvalidHandle)
            {
                auto& graphic = this->handleManager.getAsRef<Components::Graphic>(gHandle);
                graphic.scale = newScale;
            }
        }
    });

    // ctor
}

// ---------------------------------------------------------------------------
MapEditorState::~MapEditorState()
{
    // dtor
}

// ---------------------------------------------------------------------------
void MapEditorState::onEnter()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Entering MapEditorState");

    this->game.getGraphics()->init();

    // enter scriptbindings for Mapeditor
    createMapEditorBindings(&game.getScriptingModule()->getScriptEngine().getSolState(0));
    // expose objects to lua
    game.getScriptingModule()->getScriptEngine().runInSingleInstance(0, [this](sol::state& instance) {
        instance["mapEditorState"] = this;
        instance["modifiableParameters"] = std::ref(this->modifiableParameters);
    });

    //--START-- Create GUI-Object
    debugOutLevel(20, "Loading 'mapEditorGui.layout'");
    this->mapEditorGuiObj =
        this->game.getGuiEnvironment()->createMapEditorGui("mapEditorGuiV2.layout", &this->game);
    //--ENDE--

    this->mapEditorGuiObj->transferModifiableParameters(&this->modifiableParameters);

    this->mapEditorGuiObj->setPosSync(&this->posSync);
    this->mapEditorGuiObj->setOffsetSync(&this->offsetSync);
    this->mapEditorGuiObj->setRotationSync(&this->rotationSync);
    this->mapEditorGuiObj->setScaleSync(&this->scaleSync);
    this->mapeditorcam = this->game.getCamera();

    /// Lighting
    game.getGraphics()->getLighting().setAmbientColor(irr::video::SColorf(1.f, 1.f, 1.f, 0.2f).toSColor());

    irr::core::vector3df lightDirection =
        irr::core::vector3df(1500, 0, 1500) - irr::core::vector3df(2100, 1500, 1900);

    DirectionalLight& light = this->game.getGraphics()->getLight();
    light.setLightDirection(lightDirection);
    light.setLightColor(irr::video::SColor(255, 240, 240, 200));
    light.setNearValue(0.1f);
    light.setFarValue(2000.0f);


    this->camAnim =
        new MapEditorCameraAnimator(this->game.getDevice()->getCursorControl(), 10.0f, 20.0f, 30.0f, 5000.0f);

    this->mapeditorcam->addAnimator(camAnim);

    this->mapeditorcam->setPosition(irr::core::vector3df(0, 600, 300));
    this->mapeditorcam->setTarget(irr::core::vector3df(0, 0, 0));

    this->game.getSceneManager()->setActiveCamera(mapeditorcam);


    this->terrainCollisionManager =
        new irr::scene::TerrainCollisionManager(this->game.getSceneManager(), this->game.getVideoDriver());

    // define components
    this->initEntitySystem();

    EntityFactory entityFactory;
    entityFactory.createBlueprintsFromJson("data/ecs/tests/test.json",
                                           *this->entityManager,
                                           this->parsers,
                                           this->blueprintCollection);
    for (size_t i = 0; i < blueprintCollection.size(); i++)
    {
        auto entityType = this->blueprintCollection[i]
                              .first.BlueprintTypeNames[size_t(this->blueprintCollection[i].second)];
        this->mapEditorGuiObj->addEntityBlueprintToTree(entityType,
                                                        this->blueprintCollection[i].first.name,
                                                        i);
    }

    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "onEnter ENDE");

    this->mapLoader = new MapLoader(this->game.getSceneManager(),
                                    this->game.getDevice()->getFileSystem(),
                                    this->game.getVideoDriver(),
                                    this->game);
}

// ---------------------------------------------------------------------------
void MapEditorState::onLeave() // TODO clean every thing
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Leaving MapEditorState");
    // delete created GUI-Object
    this->mapEditorGuiObj->getRoot()->setVisible(false);
    //this->game.getGuiEnvironment()->deleteMapEditorGui(); //DONT use Delete to prevent Pointer Errors

    if (this->gameMap != nullptr)
    {
        delete this->gameMap;
        this->gameMap = nullptr;
    }
    this->game.getCamera()->removeAnimator(this->camAnim);
    this->camAnim->drop();
    this->game.getGraphics()->clear(); // Clear graphicspipeline last, to drop() remaining nodes
                                       // that were not cleared by other calls.
}

// ---------------------------------------------------------------------------
void MapEditorState::update()
{
    if (this->graphicSystem != nullptr)
    {
        this->graphicSystem->update(0,
                                    this->game.getScriptingModule()->getScriptEngine(),
                                    this->game.getDeltaTime());
    }

    // always increment the time since last drawn. If this is done anywhere else clicking the mouse
    // after waiting some time doesn't draw which is very weird to use
    this->secondsSinceLastDrawn += this->game.getDeltaTime();

    if (this->gameMap == nullptr)
    {
        return;
    }

    switch (this->editorState)
    {
        case MAPEDIT_STATE::MES_GROUND_TEXTURING:
        {
            const irr::core::line3df ray =
                this->game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
                    mouseState.currentCursorPosition, this->game.getSceneManager()->getActiveCamera());
            constexpr std::array<irr::f32, 3> displaceFactors = {0.0f, -1.0f, 1.0f};

            bool mousePointsAtTriangle = false;
            irr::core::vector3df baseNormal;
            irr::core::vector2df baseTexCoords;
            irr::core::aabbox3df brushBox(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);

            // check center + 6 edges around for a hit with the terrain
            // do the y displacement last (outermost loop) because sometimes the mouse might be only
            // slightly outside of the map and the Y displaced ray still hits the map
            // but because the brushBox will be moved by the displacement the brush ends up ontop of
            // the terrain instead of going through it
            // TODO: maybe add code to check for the best displacement to do first depending on the
            // relative orientation of camera vs. map
            // doing the Y displacement last works because most of the time the brush is outside of
            // the map (lying in the X-Z-plane) in X or Z direction rather than ontop of the map.
            for (irr::f32 Yd : displaceFactors)
            {
                if (not brushBox.isEmpty())
                {
                    break;
                }
                for (irr::f32 Xd : displaceFactors)
                {
                    if (not brushBox.isEmpty())
                    {
                        break;
                    }
                    for (irr::f32 Zd : displaceFactors)
                    {
                        const irr::core::vector3df displaceVector(this->modifiableParameters.brushSize * Xd,
                                                                  this->modifiableParameters.brushSize * Yd,
                                                                  this->modifiableParameters.brushSize * Zd);

                        irr::core::triangle3df baseTriangle;
                        irr::core::vector3df basePoint;
                        irr::scene::ISceneNode* node;
                        irr::core::array<irr::u32> baseTriangleVertexIndices;
                        if (not this->terrainCollisionManager->getCollisionPoint(
                                ray + displaceVector,
                                static_cast<irr::scene::TerrainSceneNode*>(this->gameMap->getTerrainNode())
                                    ->getVertexSelector(),
                                basePoint,
                                baseTriangle,
                                baseTriangleVertexIndices,
                                node) or
                            node == nullptr)
                        {
                            continue;
                        }
                        debugOutLevel(Debug::DebugLevels::updateLoop,
                                      "base point =",
                                      vector3dToStringW(basePoint),
                                      "at displacement",
                                      vector3dToStringW(displaceVector));
                        // draw a line from (over) the camera to the base point
                        this->game.line2Draw.push_back(line2Draw_s(this->game.getCamera()->getPosition() *
                                                                       irr::core::vector3df(1.0f, 1.1f, 1.0f),
                                                                   basePoint,
                                                                   irr::video::SColor(255, 255, 0, 0)));

                        // one of the displacements has hit the terrain -> calculate the full
                        // bounding box around that hit
                        brushBox.MinEdge.X = basePoint.X - this->modifiableParameters.brushSize * (1.0f + Xd);
                        brushBox.MinEdge.Y = basePoint.Y - this->modifiableParameters.brushSize * (1.0f + Yd);
                        brushBox.MinEdge.Z = basePoint.Z - this->modifiableParameters.brushSize * (1.0f + Zd);
                        brushBox.MaxEdge = brushBox.MinEdge +
                            irr::core::vector3df(2.0f * this->modifiableParameters.brushSize,
                                                 2.0f * this->modifiableParameters.brushSize,
                                                 2.0f * this->modifiableParameters.brushSize);
                        debugOutLevel(Debug::DebugLevels::updateLoop,
                                      "box around base point =",
                                      vector3dToStringW(brushBox.MinEdge),
                                      vector3dToStringW(brushBox.MaxEdge));

                        baseNormal = baseTriangle.getNormal().normalize();

#pragma GCC diagnostic ignored "-Wfloat-equal"
                        if (Xd == 0 and Yd == 0 and Zd == 0) // direct hit by mouse
                        {
                            mousePointsAtTriangle = true;
                            baseTexCoords =
                                this->getTexCoordsForPointInTriangle(basePoint, baseTriangle, baseTriangleVertexIndices);
                        }
                        else
                        {
                            // no triangle hit by the mouse directly -> the mouse is outside of the
                            // terrain mesh
                            // simple guess to get the base texture coordinates: use the position in
                            // X,Z of the center of the brushBox
                            // this might fail if the terrain is tilted but works in the (normal)
                            // flat case
                            const irr::core::vector3df baseTexCoords3d =
                                (brushBox.getCenter() + this->gameMap->getMapPosition());
                            baseTexCoords.X = baseTexCoords3d.X / this->gameMap->getMapSize().X;
                            baseTexCoords.Y = baseTexCoords3d.Z / this->gameMap->getMapSize().Y;
                        }
                        debugOutLevel(Debug::DebugLevels::updateLoop, "base texture coords =", vector2dToStringW(baseTexCoords));
#pragma GCC diagnostic pop
                        irr::video::SColor highlightColor(255, 0, 0, 255);
                        if (mousePointsAtTriangle)
                        {
                            // change highligting color to signal that base triangle is determined
                            // by mouse pointing
                            highlightColor = irr::video::SColor(255, 0, 255, 0);
                        }
                        const irr::core::triangle3df drawTriangle(baseTriangle.pointA + baseNormal,
                                                                  baseTriangle.pointB + baseNormal,
                                                                  baseTriangle.pointC + baseNormal);
                        // move the base triangle slightly up to make the hightlight more visible
                        this->game.tri2Draw.push_back(tri2Draw_s(drawTriangle, highlightColor));

                        break; // only breaks out of the z displacement loop
                    }          // for (irr::f32 zd : displaceFactors)
                }              // for (irr::f32 yd : displaceFactors)
            }                  // for (irr::f32 xd : displaceFactors)

            if (brushBox.isEmpty())
            {
                return; // no collision with terrain
            }

            box2Draw_s btd(brushBox, irr::video::SColor(255, 0, 0, 255));
            this->game.box2Draw.push_back(btd);

            irr::s32 brushSelectedTrianglesCount = 0;

            static_cast<irr::scene::TerrainSceneNode*>(this->gameMap->getTerrainNode())
                ->getVertexSelector()
                ->getTriangles(this->selectedTriangles.pointer(),
                               this->selectedTriangleVertexIndices.pointer(),
                               this->selectedTriangles.size(),
                               brushSelectedTrianglesCount,
                               brushBox);
            if (brushSelectedTrianglesCount <= 0)
            {
                // trianglecount should't be 0 because then the brushBox would have been empty. But
                // better save than sorry :-)
                return;
            }

            for (int i = 0; i < brushSelectedTrianglesCount; i++)
            {
                // color the triangles according to their normals
                const irr::core::vector3df normal = this->selectedTriangles[i].getNormal().normalize();
                this->game.tri2Fill.push_back(tri2Fill_s(
                    this->selectedTriangles[i],
                    irr::video::SColor(50 + 50 * static_cast<irr::u32>(baseNormal.dotProduct(normal)), 255, 0, 0)));
            }

            if (mouseState.leftButtonDown && drawing)
            {
                // check at what time the last draw was. Only draw every 1/10 of a second
                if (this->secondsSinceLastDrawn >= 1.0 / this->modifiableParameters.drawsPerSecond)
                {
                    drawTextures(brushBox, brushSelectedTrianglesCount, baseTexCoords, baseNormal);
                    this->secondsSinceLastDrawn = 0.0f;
                }
            }
        }
        break;
        case MAPEDIT_STATE::MES_OBJECT_PLACING:
        {
            if (this->selectedEntity != ECS::INVALID_ENTITY)
            {
                this->placeEntityAtCursorPosition(this->selectedEntity);
            }
        }
        break;
        case MAPEDIT_STATE::MES_OBJECT_MANIPULATION:
        {
            if (this->mouseState.leftButtonDown)
            {
                if (this->selectedEntity != ECS::INVALID_ENTITY)
                {
                    this->placeEntityAtCursorPosition(this->selectedEntity);
                }
            }
        }
        break;
        case MAPEDIT_STATE::MES_GROUND_NOTEXTURESELECTED:
        case MAPEDIT_STATE::MES_OBJECT_NOTHINGSELECTED:
        case MAPEDIT_STATE::MES_PATHMAP_MANIPULATION:
        case MAPEDIT_STATE::MES_COUNT:
        case MAPEDIT_STATE::MES_DONOTHING:
        default:
        {
            break;
        }
    }
}

// ---------------------------------------------------------------------------
bool MapEditorState::loadMap(std::string file_)
{
    if (this->gameMap != nullptr)
    {
        debugOut("GameMap already defined");
        this->mapEditorGuiObj->getMessageBox()->ask("Attention!",
                                                    "Done modification will not be "
                                                    "stored! Are you sure you want "
                                                    "to quit the current Map without "
                                                    "saving?");
        // TODO: anything to do here? What happens in the messagebox?
        return false;
    }

    try
    {
        auto loaded = this->mapLoader->loadMap(file_, true);
        this->gameMap = std::get<1>(loaded);
        this->game.getGraphics()->addNodeToStage(gameMap->getTerrainNode(), RenderStageType::STATIC);
        // this->game.getGraphics()->addNodeToStage(gameMap->getTerrainNode(), RenderStageType::LIGHTING);
        gameMap->getTerrainNode()->getMaterial(0).Lighting = false;
        this->mapLoadingParameters = std::get<0>(loaded);
        this->modifiableParameters.textureArrayFilenames = std::get<0>(loaded).textureArrayFilenames;
    }
    catch (const std::exception& error)
    {
        Error::errContinue("couldn't load map: error message: ", error.what());
        return false;
    }

    this->mapEditorGuiObj->getRoot()->getChild("Menubar/MenuItem_ToolBox")->setDisabled(false);

    this->mapEditorGuiObj->updateTextureButtons();


    /** testing**/
    // this->gameMap->getTerrainNode()->getMaterial(1).Lighting = false;

    // allow to select the whole map at once (this is needed by initPixelWorldCoordsAndNormals!)
    const irr::u32 maxTriangleCount =
        static_cast<irr::u32>(static_cast<irr::scene::TerrainSceneNode*>(this->gameMap->getTerrainNode())
                                  ->getVertexSelector()
                                  ->getTriangleCount());
    this->selectedTriangles.set_used(maxTriangleCount);
    this->selectedTriangleVertexIndices.set_used(3 * maxTriangleCount);

    irr::video::ITexture* const splatTexture = this->gameMap->getTerrainShader()->getSplatTexture();
    irr::video::IImage* splatTextureImage =
        ImageManipulation::TextureToImage(splatTexture, this->game.getVideoDriver());
    this->texDrawer = new TextureDrawer(this->gameMap->getTerrainShader()->getSplatTexture(), splatTextureImage);
    this->initFloatIntensities(splatTextureImage);
    this->initPixelWorldCoordsAndNormals(splatTextureImage);
    // texDrawer grabs a reference to the IImage* and it isn't needed here anymore
    splatTextureImage->drop();

    this->grid = new grid::Grid(this->gameMap->getMapSize(), 1024.0f);

    /*decalManager = new DecalManager(this->game.getSceneManager());
    decalManager->setTerrain(gameMap->getTerrainNode());
    decalNode = decalManager->addDecal("media/map/textures/gras.png", irr::core::vector3df(0, 0,
    0));
    decalNode->setVisible(false);*/
    /** testing END **/

    this->createMapDependentSystems();

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorState::saveMap()
{
    if ((not this->gameMap) or (not this->entityManager))
    {
        return false;
    }

    std::vector<entityID_t> displayOnlyEntitiesToBakeIntoMap;

    this->mapLoadingParameters.textureArrayFilenames = this->modifiableParameters.textureArrayFilenames;
    if (this->mapLoader->saveMap(std::string(this->loadedMapFile.c_str()),
                                 this->mapLoadingParameters,
                                 *this->gameMap,
                                 displayOnlyEntitiesToBakeIntoMap,
                                 *this->entityManager,
                                 this->handleManager) != ErrCodes::NO_ERR)
    {
        return false;
    }

    // Save the SplatMap
    this->texDrawer->saveTexture(irr::io::path(this->gameMap->getTerrainShader()->getSplatTexture()->getName()),
                                 this->game.getSceneManager()->getVideoDriver());


    std::string ecsData(this->loadedMapFile.c_str());
    ecsData += ".components.bin";
    SavingLoading::SaveLoadDependencies dependencies;
    dependencies.sceneManager = this->game.getSceneManager();
    SavingLoading::saveEntitySystem(ecsData, *this->entityManager, this->handleManager, dependencies);

    return true;
}

// ---------------------------------------------------------------------------
void MapEditorState::updateEntityGui(const entityID_t entityID)
{
    const auto positionHandle = this->entityManager->getComponent<Components::Position>(entityID);
    POW2_ASSERT(this->handleManager.isValid(positionHandle));
    const auto& pos = this->handleManager.getAsRef<Components::Position>(positionHandle);
    this->posSync.setFrom(this, pos.position);

    const auto graphicHandle = this->entityManager->getComponent<Components::Graphic>(entityID);
    POW2_ASSERT(this->handleManager.isValid(graphicHandle));
    const auto& graphic = this->handleManager.getAsRef<Components::Graphic>(graphicHandle);
    this->offsetSync.setFrom(this, graphic.offset);

    this->scaleSync.setFrom(this, graphic.scale);
}

// ---------------------------------------------------------------------------
entityID_t MapEditorState::createEntityFromBlueprint(const entityID_t blueprintIndex)
{
    debugOut("createEntityFromBlueprint");
    POW2_ASSERT(blueprintIndex < this->blueprintCollection.size());

    const auto entityID = this->blueprintCollection[blueprintIndex].first.createEntity();

    const auto eHandle = this->entityManager->getComponent<Components::EntityName>(entityID);
    const auto& eCmp = this->handleManager.getAsValue<Components::EntityName>(eHandle);
    debugOut(eCmp.entityName);

    const auto gHandle = this->entityManager->getComponent<Components::Graphic>(entityID);
    const auto& gCmp = this->handleManager.getAsRef<Components::Graphic>(gHandle);
    this->entityIDFromNode[gCmp.node] = entityID;
    debugOut(this->blueprintCollection[blueprintIndex].first.name, "with ID ", entityID);

    return entityID;
}

// ---------------------------------------------------------------------------
void MapEditorState::placeEntityAtCursorPosition(const entityID_t entityID)
{
    POW2_ASSERT(entityID != ECS::INVALID_ENTITY);
    irr::core::line3df ray = this->game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
        mouseState.currentCursorPosition, this->game.getSceneManager()->getActiveCamera());
    irr::core::vector3df colpoint;
    irr::core::triangle3df tri3d;
    irr::scene::ISceneNode* node;
    if (!this->game.getSceneManager()->getSceneCollisionManager()->getCollisionPoint(
            ray, gameMap->getTriangleSelector(), colpoint, tri3d, node))
    {
        return;
    }
    irr::core::vector2df relativePos = irr::core::vector2df(colpoint.X, colpoint.Z) - this->offsetToSelectedEntity;
    if (node)
    {
        const auto pHandle = this->entityManager->getComponent<Components::Position>(entityID);
        POW2_ASSERT(this->handleManager.isValid(pHandle));
        auto& pos = this->handleManager.getAsRef<Components::Position>(pHandle);

        pos.position = relativePos;

        this->updateEntityGui(entityID);
    }
}

// ---------------------------------------------------------------------------
#pragma GCC diagnostic ignored "-Wswitch"
void MapEditorState::refreshMousestate(const irr::SEvent& event)
{
    if (irr::EET_MOUSE_INPUT_EVENT == event.EventType)
    {
        switch (event.MouseInput.Event)
        {
            case irr::EMIE_MOUSE_MOVED:
            {
                mouseState.currentCursorPosition.X = event.MouseInput.X;
                mouseState.currentCursorPosition.Y = event.MouseInput.Y;
            }
            break;
            case irr::EMIE_LMOUSE_PRESSED_DOWN:
            {
                mouseState.leftButtonDown = true;
                switch (this->editorState)
                {
                    case MAPEDIT_STATE::MES_OBJECT_NOTHINGSELECTED:
                    {
                        debugOut("MES_OBJECT_NOTHINGSELECTED");
                        const auto newSelectedEntityID = this->getEntityIDUnderCursor();
                        if (newSelectedEntityID != ECS::INVALID_ENTITY)
                        {
                            this->setEditorStateTo(MAPEDIT_STATE::MES_OBJECT_MANIPULATION);
                            this->setSelectedEntity(newSelectedEntityID);

                            irr::core::line3df ray =
                                this->game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
                                    mouseState.currentCursorPosition,
                                    this->game.getSceneManager()->getActiveCamera());
                            irr::core::vector3df colpoint;
                            irr::core::triangle3df tri3d;
                            irr::scene::ISceneNode* node;
                            if (this->game.getSceneManager()->getSceneCollisionManager()->getCollisionPoint(
                                    ray, gameMap->getTriangleSelector(), colpoint, tri3d, node))
                            {
                                const auto pHandle =
                                    this->entityManager->getComponent<Components::Position>(newSelectedEntityID);
                                POW2_ASSERT(this->handleManager.isValid(pHandle));
                                auto& pos = this->handleManager.getAsRef<Components::Position>(pHandle);
                                this->offsetToSelectedEntity =
                                    irr::core::vector2df(colpoint.X, colpoint.Z) - pos.position;
                            }
                            else
                            {
                                this->offsetToSelectedEntity =
                                    irr::core::vector2df(irr::f32(0), irr::f32(0));
                            }
                        }
                    }
                    break;
                    case MAPEDIT_STATE::MES_OBJECT_MANIPULATION:
                    {
                        debugOut("MES_OBJECT_MANIPULATION");
                        POW2_ASSERT(this->selectedEntity != ECS::INVALID_ENTITY); // must have an
                                                                                  // entity selected
                                                                                  // when in
                                                                                  // manipulation
                                                                                  // mode
                        const auto newSelectedEntityID = this->getEntityIDUnderCursor();

                        if (newSelectedEntityID != this->selectedEntity)
                        { // change the selected entity
                            if (newSelectedEntityID == ECS::INVALID_ENTITY)
                            {
                                this->setEditorStateTo(MAPEDIT_STATE::MES_OBJECT_NOTHINGSELECTED);
                                return;
                            }
                            else
                            {
                                this->setSelectedEntity(newSelectedEntityID);
                            }
                        }
                        irr::core::line3df ray =
                            this->game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
                                mouseState.currentCursorPosition,
                                this->game.getSceneManager()->getActiveCamera());
                        irr::core::vector3df colpoint;
                        irr::core::triangle3df tri3d;
                        irr::scene::ISceneNode* node;
                        if (this->game.getSceneManager()->getSceneCollisionManager()->getCollisionPoint(
                                ray, gameMap->getTriangleSelector(), colpoint, tri3d, node))
                        {
                            const auto pHandle =
                                this->entityManager->getComponent<Components::Position>(newSelectedEntityID);
                            POW2_ASSERT(this->handleManager.isValid(pHandle));
                            auto& pos = this->handleManager.getAsRef<Components::Position>(pHandle);
                            this->offsetToSelectedEntity =
                                irr::core::vector2df(colpoint.X, colpoint.Z) - pos.position;
                            debugOut("o",
                                     this->offsetToSelectedEntity.X,
                                     this->offsetToSelectedEntity.Y);
                        }
                        else
                        {
                            this->offsetToSelectedEntity = irr::core::vector2df(irr::f32(0), irr::f32(0));
                            debugOut("u",
                                     this->offsetToSelectedEntity.X,
                                     this->offsetToSelectedEntity.Y);
                        }
                    }
                    break;
                    case MAPEDIT_STATE::MES_OBJECT_PLACING:
                    {
                        debugOut("MES_OBJECT_PLACING");
                        this->mapEditorGuiObj->addEntityFromBlueprintToTree(this->selectedEntity,
                                                                            this->selectedBlueprint.name);
                        // after placing the entity create a new entity from the same blueprint
                        const auto newEntity =
                            this->createEntityFromBlueprint(this->selectedBlueprint.index);
                        this->selectedEntity = newEntity;
                        this->placeEntityAtCursorPosition(newEntity); // initially place the
                                                                      // entity at the cursor
                                                                      // (otherwise the entity
                                                                      // is displayed at its
                                                                      // initial position (0,
                                                                      // 0) before being
                                                                      // warped to the cursor)
                    }
                    break;
                    case MAPEDIT_STATE::MES_PATHMAP_MANIPULATION:
                    {
                        debugOutLevel(Debug::DebugLevels::onEvent, "pathmap manipulation click:");

                        // only do something if the debugcubes are visible
                        irr::core::line3df ray =
                            this->game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
                                mouseState.currentCursorPosition,
                                this->game.getSceneManager()->getActiveCamera());
                        irr::core::vector3df colpoint;
                        irr::core::triangle3df tri3d;
                        irr::scene::ISceneNode* node;
                        if (this->gameMap and gameMap->getDebugCubesVisible())
                        {
                            if (this->game.getSceneManager()->getSceneCollisionManager()->getCollisionPoint(
                                    ray, gameMap->getTriangleSelector(), colpoint, tri3d, node))
                            {
                                if (node)
                                {
                                    this->gameMap->flipPathMapPoint(colpoint);
                                }
                            }
                        }
                    }
                    break;
                }
            }
            break;
            case irr::EMIE_LMOUSE_LEFT_UP:
            {
                mouseState.leftButtonDown = false;
            }
            break;
            case irr::EMIE_MMOUSE_PRESSED_DOWN:
            {
                mouseState.middleButtonDown = true;
            }
            break;
            case irr::EMIE_MMOUSE_LEFT_UP:
            {
                mouseState.middleButtonDown = false;
            }
            break;
            case irr::EMIE_RMOUSE_PRESSED_DOWN:
            {
                mouseState.rightButtonDown = true;
            }
            break;
            case irr::EMIE_RMOUSE_LEFT_UP:
            {
                mouseState.rightButtonDown = false;
            }
            break;
        }
    }

    if (event.EventType == irr::EET_GUI_EVENT)
    {
        if (event.GUIEvent.EventType == irr::gui::EGUI_EVENT_TYPE::EGET_ELEMENT_HOVERED)
        {
            mouseState.guiHovered = true;
        }
        else if (event.GUIEvent.EventType == irr::gui::EGUI_EVENT_TYPE::EGET_ELEMENT_LEFT)
        {
            mouseState.guiHovered = false;
        }
    }
}
#pragma GCC diagnostic pop

// ---------------------------------------------------------------------------
#pragma GCC diagnostic ignored "-Wswitch"
bool MapEditorState::onEvent(const irr::SEvent& event)
{
    if (event.EventType != irr::EET_KEY_INPUT_EVENT or event.KeyInput.Key <= irr::KEY_KEY_CODES_COUNT)
    {
        if (this->game.getGuiEnvironment()->getRenderer()->injectEvent(event)) // CEGUI-EventReceiver
        {
            return true; // use CEGUI EVENTHANDLER
        }
        else if (event.EventType == irr::EET_KEY_INPUT_EVENT and event.KeyInput.Key <= irr::KEY_KEY_CODES_COUNT) // TODO find a way to set this into the guipart
        {
            CEGUI::Window* activeChild = this->mapEditorGuiObj->getRoot()->getActiveChild();
            if ((activeChild != nullptr) and activeChild->getType() == "BT/Editbox" and
                static_cast<CEGUI::Editbox*>(activeChild)->hasInputFocus())
            {
                return true;
            }
        }
    }
    else
    {
        debugOutLevel(20, "For CEGUI invalid Keycode NOT in CEGUI injected: ", event.KeyInput.Key);
        return true; // No key we need to handle (mostly Volume up and down keys)
    }

    this->refreshMousestate(event);

    if (event.EventType == irr::EET_GUI_EVENT)
    {
        switch (event.GUIEvent.EventType)
        {
            case irr::gui::EGET_FILE_SELECTED:
            {
                debugOutLevel(50, "loading selected File");
                if (this->mapEditorGuiObj->Map_FILE_SELECTED)
                {
                    this->mapEditorGuiObj->Map_FILE_SELECTED = false;

                    irr::io::path mapFile = this->mapEditorGuiObj->getSelectedPath();

                    irr::io::path extension = "";
                    irr::core::getFileNameExtension(extension, mapFile);
                    extension.make_lower();

                    if (extension != ".xml")
                    {
                        this->mapEditorGuiObj->getMessageBox()->inform("Cant load Map",
                                                                       "MapFile-extension '" +
                                                                           std::string(extension.c_str()) +
                                                                           "' is unsupported");
                        Error::errContinue("MapFile-extension '", extension.c_str(), "' is unsupported");
                        return true;
                    }

                    debugOutLevel(50, "loading Map-File: '", mapFile, "'");
                    if (this->loadMap(mapFile.c_str()))
                    {
                        loadedMapFile = mapFile;
                    }

                    debugOutLevel(Debug::DebugLevels::onEvent, "file selected event handled");
                    return true;
                }
                else if (this->mapEditorGuiObj->Texture_FILE_SELECTED)
                {
                    irr::io::path textureFile = this->mapEditorGuiObj->getSelectedPath();
                    this->mapEditorGuiObj->Texture_FILE_SELECTED = false;
                    std::vector<irr::core::stringw> tempTextureArray =
                        this->modifiableParameters.textureArrayFilenames;
                    tempTextureArray.push_back(irr::core::stringw(textureFile.c_str()));
                    if (this->updateTextureArray(tempTextureArray))
                    {
                        this->mapEditorGuiObj->updateTextureButtons();
                    }

                    debugOutLevel(Debug::DebugLevels::onEvent, "object file selected event handled");
                    return true;
                }
            }
            break;
        }
    }

    // not returning if the camera handled the event to allow mouse movement to still be handled by
    // the state
    this->game.getSceneManager()->getActiveCamera()->OnEvent(event);


    if (event.EventType == irr::EET_KEY_INPUT_EVENT)
    {
        if (event.KeyInput.Key == irr::EKEY_CODE::KEY_DELETE && !event.KeyInput.PressedDown)
        {
            debugOut("KEY_DELETE pressed");
            if (this->editorState == MAPEDIT_STATE::MES_OBJECT_MANIPULATION)
            {
                this->deleteSelectedEntity();
                this->setEditorStateTo(MAPEDIT_STATE::MES_OBJECT_NOTHINGSELECTED);
                return true;
            }
        }
        if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_W && !event.KeyInput.PressedDown)
        {
            if (gameMap != nullptr)
            {
                gameMap->getTerrainNode()->setMaterialFlag(
                    irr::video::EMF_WIREFRAME, !gameMap->getTerrainNode()->getMaterial(0).Wireframe);
                return true;
            }
        }
        else if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_D && !event.KeyInput.PressedDown)
        {
            if (gameMap != nullptr)
            {
                if (gameMap->changeDebugCubesVisiblity())
                {
                    // the debug cubes have been made visible -> turn pathmap manipulation on
                    this->setEditorStateTo(MAPEDIT_STATE::MES_PATHMAP_MANIPULATION);
                }
                return true;
            }
        }
        else if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_A && !event.KeyInput.PressedDown)
        {
            if (gameMap != nullptr)
            {
                if (gameMap->getTerrainNode()->isDebugDataVisible())
                {
                    gameMap->getTerrainNode()->setDebugDataVisible(irr::scene::E_DEBUG_SCENE_TYPE::EDS_OFF);
                }
                else
                {
                    gameMap->getTerrainNode()->setDebugDataVisible(irr::scene::E_DEBUG_SCENE_TYPE::EDS_FULL);
                }
                return true;
            }
        }
    }

    debugOutLevel(Debug::DebugLevels::onEvent + 1, "MapEditorState not handling event!");
    return false;
}
#pragma GCC diagnostic pop

// ---------------------------------------------------------------------------
bool MapEditorState::updateTextureArray(std::vector<irr::core::stringw> newTextureArray)
{
    debugOut("updateTextureArray");
    irr::video::ITexture* texArray = Graphics::addTextureArray(this->game.getVideoDriver(), newTextureArray);
    if (!texArray)
    {
        return false;
    }
    this->modifiableParameters.textureArrayFilenames = newTextureArray;
    this->gameMap->getTerrainShader()->setTextureArray(texArray);
    this->gameMap->getTerrainShader()->applyToMaterial(this->gameMap->getTerrainNode()->getMaterial(0));

    return true;
}

// ---------------------------------------------------------------------------
void MapEditorState::setSelectedBlueprint(const entityID_t index)
{
    POW2_ASSERT(this->editorState == MAPEDIT_STATE::MES_OBJECT_PLACING);
    POW2_ASSERT(index != ECS::INVALID_ENTITY);

    POW2_ASSERT(index < this->blueprintCollection.size());
    if (this->selectedBlueprint.index != index)
    {
        // selected another blueprint -> delete the old entity
        if (this->selectedEntity != ECS::INVALID_ENTITY)
        {
            this->deleteSelectedEntity();
        }
        this->selectedBlueprint = SelectedBlueprint_s(index, this->blueprintCollection[index].first.name);
        this->selectedEntity = this->createEntityFromBlueprint(index);
    }

    this->mapEditorGuiObj->setSelectedBlueprint(index);
}

// ---------------------------------------------------------------------------
void MapEditorState::setSelectedEntity(const entityID_t entityID)
{
    POW2_ASSERT(this->editorState == MAPEDIT_STATE::MES_OBJECT_MANIPULATION);
    POW2_ASSERT(entityID != ECS::INVALID_ENTITY);


    if (this->selectedEntity != entityID)
    {
        if (this->selectedEntity != ECS::INVALID_ENTITY)
        {
            this->deselectSelectedEntity();
        }
        this->selectEntity(entityID);
    }
}


// ---------------------------------------------------------------------------
void MapEditorState::selectEntity(const entityID_t entityID)
{
    debugOut("selectEntity");

    POW2_ASSERT(entityID != ECS::INVALID_ENTITY);
    POW2_ASSERT(entityID != this->selectedEntity); // trying to select an already selected entity!
    POW2_ASSERT(this->selectedEntity == ECS::INVALID_ENTITY); // must deselect the selected entity first!
    POW2_ASSERT(this->editorState == MAPEDIT_STATE::MES_OBJECT_MANIPULATION);

    const auto graphicComponentHandle = this->entityManager->getComponent<Components::Graphic>(entityID);
    POW2_ASSERT(this->handleManager.isValid(graphicComponentHandle));
    auto& graphicComponent = this->handleManager.getAsRef<Components::Graphic>(graphicComponentHandle);

    graphicComponent.node->setDebugDataVisible(irr::scene::E_DEBUG_SCENE_TYPE::EDS_MESH_WIRE_OVERLAY);

    this->selectedEntity = entityID;

    this->mapEditorGuiObj->setSelectedEntity(entityID);
    this->updateEntityGui(entityID);
}

// ---------------------------------------------------------------------------
void MapEditorState::deselectSelectedEntity()
{
    POW2_ASSERT(this->editorState ==
                MAPEDIT_STATE::MES_OBJECT_MANIPULATION); // this function should only be called when
                                                         // in MES_PATHMAP_MANIPULATION mode
    debugOut("deselectSelectedEntity");
    if (this->selectedEntity != ECS::INVALID_ENTITY)
    {
        const auto gHandle = this->entityManager->getComponent<Components::Graphic>(this->selectedEntity);
        auto& graphicC = this->handleManager.getAsRef<Components::Graphic>(gHandle);
        graphicC.node->setDebugDataVisible(irr::scene::E_DEBUG_SCENE_TYPE::EDS_OFF);

        this->mapEditorGuiObj->setSelectedEntity(ECS::INVALID_ENTITY);

        this->selectedEntity = ECS::INVALID_ENTITY;
    }
    this->offsetToSelectedEntity = irr::core::vector2df(irr::f32(0), irr::f32(0));
}

// ---------------------------------------------------------------------------
void MapEditorState::deleteSelectedEntity()
{
    debugOut("deleteSelectedEntity");
    POW2_ASSERT(this->selectedEntity != ECS::INVALID_ENTITY);

    this->mapEditorGuiObj->deleteEntityFromTree(this->selectedEntity);

    this->entityManager->deleteEntity(this->selectedEntity);

    this->selectedEntity = ECS::INVALID_ENTITY;
}

// ---------------------------------------------------------------------------
void MapEditorState::setEditorStateTo(const MAPEDIT_STATE newState)
{
    switch (this->editorState)
    {
        case MAPEDIT_STATE::MES_OBJECT_NOTHINGSELECTED:
            this->mapEditorGuiObj->setSelectedEntity(ECS::INVALID_ENTITY); // deselect everything
            this->editorState = newState;
            break;
        case MAPEDIT_STATE::MES_OBJECT_MANIPULATION:
            if (newState != MAPEDIT_STATE::MES_OBJECT_MANIPULATION)
            {
                this->deselectSelectedEntity();
            }
            this->editorState = newState;
            break;
        case MAPEDIT_STATE::MES_OBJECT_PLACING:
            if (newState != MAPEDIT_STATE::MES_OBJECT_PLACING)
            {
                this->deleteSelectedEntity(); // the selected entity is just a floating entity which
                                              // should be deleted
                this->selectedBlueprint = {ECS::INVALID_ENTITY, ""};
            }
            this->editorState = newState;
            break;
        case MAPEDIT_STATE::MES_COUNT:
            Error::errTerminate("tried to select MAPEDIT_STATE::MES_COUNT as editor state");
        case MAPEDIT_STATE::MES_GROUND_NOTEXTURESELECTED:
        case MAPEDIT_STATE::MES_PATHMAP_MANIPULATION:
        case MAPEDIT_STATE::MES_GROUND_TEXTURING:
        case MAPEDIT_STATE::MES_DONOTHING:
        default:
            this->editorState = newState;
    }
}

// ---------------------------------------------------------------------------
void MapEditorState::switchCameraPosition(const entityID_t entityID)
{
    const auto positionHandle = this->entityManager->getComponent<Components::Position>(entityID);
    POW2_ASSERT(this->handleManager.isValid(positionHandle));
    const auto targetPosition2d =
        this->handleManager.getAsValue<Components::Position>(positionHandle).position;
    const auto targetPosition3d = this->gameMap->get3dPosition(targetPosition2d);
    this->switchCameraPosition(targetPosition3d);
}

// ---------------------------------------------------------------------------
void MapEditorState::switchCameraPosition(const irr::core::vector3df camTarget_)
{
    this->mapeditorcam->setPosition(
        camTarget_ + (this->mapeditorcam->getPosition() - this->mapeditorcam->getTarget()));
    this->mapeditorcam->setTarget(camTarget_); // Important set the target after relocate the camera-position (;
}

// ---------------------------------------------------------------------------
entityID_t MapEditorState::getEntityIDUnderCursor() const
{
    irr::core::line3df ray = this->game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
        this->game.getDevice()->getCursorControl()->getPosition(),
        this->game.getSceneManager()->getActiveCamera());
    irr::scene::ISceneNode* node =
        this->game.getSceneManager()->getSceneCollisionManager()->getSceneNodeFromRayBB(ray);

    if (node != nullptr)
    {
        const auto entry =
            this->entityIDFromNode.find(static_cast<irr::scene::IAnimatedMeshSceneNode*>(node));

        if (entry != this->entityIDFromNode.end())
        {
            return entry->second;
        }
    }
    else
    {
        debugOutLevel(20, "NO NODE Selected\n");
    }
    return ECS::INVALID_ENTITY;
}

// ---------------------------------------------------------------------------
irr::core::vector3df MapEditorState::getBarycentricCoefficients(const irr::core::vector3df point,
                                                                const irr::core::triangle3df triangle) const
{
    // barycentric coordinate calculation from https://gamedev.stackexchange.com/questions/23743/whats-the-most-efficient-way-to-find-barycentric-coordinates
    irr::core::vector3df v0 = triangle.pointB - triangle.pointA;
    irr::core::vector3df v1 = triangle.pointC - triangle.pointA;
    irr::core::vector3df v2 = point - triangle.pointA;
    irr::f32 d00 = v0.dotProduct(v0);
    irr::f32 d01 = v0.dotProduct(v1);
    irr::f32 d11 = v1.dotProduct(v1);
    irr::f32 d20 = v2.dotProduct(v0);
    irr::f32 d21 = v2.dotProduct(v1);
    irr::f32 denom = d00 * d11 - d01 * d01;
    const irr::f32 v = (d11 * d20 - d01 * d21) / denom;
    const irr::f32 w = (d00 * d21 - d01 * d20) / denom;
    const irr::f32 u = 1.0f - v - w;

    return irr::core::vector3df(u, v, w);
}

// ---------------------------------------------------------------------------
irr::core::vector2df
MapEditorState::getTexCoordsForPointInTriangle(const irr::core::vector3df& colpoint,
                                               const irr::core::triangle3df& triangle,
                                               const irr::core::array<irr::u32>& vertexIndices) const
{
    const irr::video::S3DVertex2TCoords* const vertexArray = static_cast<irr::video::S3DVertex2TCoords*>(
        this->gameMap->getTerrainNode()->getRenderBuffer()->getVertices());
    const irr::video::S3DVertex2TCoords& v1 = vertexArray[vertexIndices[0]];
    const irr::video::S3DVertex2TCoords& v2 = vertexArray[vertexIndices[1]];
    const irr::video::S3DVertex2TCoords& v3 = vertexArray[vertexIndices[2]];
    debugOutLevel(Debug::DebugLevels::onEvent,
                  "colpoint",
                  vector3dToStringW(colpoint),
                  "in triangle",
                  vector3dToStringW(v1.Pos),
                  vector3dToStringW(v2.Pos),
                  vector3dToStringW(v3.Pos));
    const irr::core::vector3df weights = getBarycentricCoefficients(colpoint, triangle);
    debugOutLevel(Debug::DebugLevels::onEvent,
                  "weighting texcoords1:",
                  vector2dToStringW(v1.TCoords),
                  "with weight =",
                  weights.X);
    debugOutLevel(Debug::DebugLevels::onEvent,
                  "weighting texcoords2:",
                  vector2dToStringW(v2.TCoords),
                  "with weight =",
                  weights.Y);
    debugOutLevel(Debug::DebugLevels::onEvent,
                  "weighting texcoords3:",
                  vector2dToStringW(v3.TCoords),
                  "with weight =",
                  weights.Z);
    return v1.TCoords * weights.X + v2.TCoords * weights.Y + v3.TCoords * weights.Z;
}

irr::core::vector3df MapEditorState::reverseUVLookup(const irr::core::vector2df texCoords,
                                                     const irr::u32 selectedTriangleCount,
                                                     irr::u32& triangleIndex) const
{
    if (selectedTriangleCount > this->selectedTriangles.size() ||
        3 * selectedTriangleCount > this->selectedTriangleVertexIndices.size())
    {
        Error::errTerminate("MapEditorState::reverseUVLookup: passed invalid selectedTriangleCount",
                            selectedTriangleCount,
                            "while the triangles array (indices array) aro only of size",
                            this->selectedTriangles.size(),
                            "(",
                            this->selectedTriangleVertexIndices.size(),
                            ")");
    }
    debugOutLevel(Debug::DebugLevels::updateLoop + 6,
                  "reverseUVLookup for",
                  vector2dToStringW(texCoords),
                  "in array of size",
                  selectedTriangleCount);
    triangleIndex = selectedTriangleCount;
    // use the irrlicht collision check methods -> turn the 2d triangle into a 3d one (Z component =
    // 0)
    const irr::core::vector3df texCoords3d(texCoords.X, texCoords.Y, 0.0f);
    // look at every triangle and determine if the texCoords are inside that triangle
    for (irr::u32 i = 0; i < selectedTriangleCount; i++)
    {
        const irr::video::S3DVertex2TCoords* const vertexArray = static_cast<irr::video::S3DVertex2TCoords*>(
            this->gameMap->getTerrainNode()->getRenderBuffer()->getVertices());
        const irr::video::S3DVertex2TCoords& v1 =
            vertexArray[this->selectedTriangleVertexIndices[3 * i + 0]];
        const irr::video::S3DVertex2TCoords& v2 =
            vertexArray[this->selectedTriangleVertexIndices[3 * i + 1]];
        const irr::video::S3DVertex2TCoords& v3 =
            vertexArray[this->selectedTriangleVertexIndices[3 * i + 2]];
        // generate a triangle of texture coordinates with the z coordinate set to 0 in order to use
        // irrlicht triangle functions rather than implementing our own
        const irr::core::vector3df pointA(v1.TCoords.X, v1.TCoords.Y, 0.0f);
        const irr::core::vector3df pointB(v2.TCoords.X, v2.TCoords.Y, 0.0f);
        const irr::core::vector3df pointC(v3.TCoords.X, v3.TCoords.Y, 0.0f);
        const irr::core::triangle3df textureSpaceTriangle(pointA, pointB, pointC);
        // is the supplied texCoords point inside the triangle?
        if (not textureSpaceTriangle.isPointInside(texCoords3d))
        {
            continue; // test next triangle
        }
        triangleIndex = i;
        const irr::core::vector3df weights = getBarycentricCoefficients(texCoords3d, textureSpaceTriangle);
        debugOutLevel(Debug::DebugLevels::updateLoop + 6,
                      "found texture coordinates in triangle",
                      vector3dToStringW(pointA),
                      vector3dToStringW(pointB),
                      vector3dToStringW(pointC),
                      "with weights",
                      vector3dToStringW(weights));
        debugOutLevel(Debug::DebugLevels::updateLoop + 6,
                      "returning point in triangle",
                      vector3dToStringW(this->selectedTriangles[i].pointA),
                      vector3dToStringW(this->selectedTriangles[i].pointB),
                      vector3dToStringW(this->selectedTriangles[i].pointC));
        if (pointA == pointB or pointA == pointC or pointB == pointC)
        {
            Error::errContinue("degenerate triangle at selected index",
                               i,
                               "with indices",
                               selectedTriangleVertexIndices[3 * i + 0],
                               selectedTriangleVertexIndices[3 * i + 1],
                               selectedTriangleVertexIndices[3 * i + 2]);
            return irr::core::vector3df(0.0f, 0.0f, 0.0f);
        }
        return weights.X * v1.Pos + weights.Y * v2.Pos + weights.Z * v3.Pos;
    }
    return irr::core::vector3df(FLT_MAX, FLT_MAX, FLT_MAX);
}

// ---------------------------------------------------------------------------
std::array<irr::u8, 5> MapEditorState::getIntensities(const irr::video::SColor splatTexturePixel)
{
    std::array<irr::u8, 5> intensities;
    // the upper 4 bits of each color are the intensity for that color. 4 bits means 15 visible
    // levels (and 0 = invisible)
    intensities[0] = static_cast<irr::u8>(splatTexturePixel.getRed() >> 4);
    intensities[1] = static_cast<irr::u8>(splatTexturePixel.getGreen() >> 4);
    intensities[2] = static_cast<irr::u8>(splatTexturePixel.getBlue() >> 4);
    intensities[3] = static_cast<irr::u8>(splatTexturePixel.getAlpha() >> 4);
    intensities[4] = static_cast<irr::u8>(maxIntensity - intensities[0] - intensities[1] -
                                          intensities[2] - intensities[3]);
    return intensities;
}

// ---------------------------------------------------------------------------
std::array<size_t, 5> MapEditorState::getTexturesInPixel(const irr::video::SColor splatTexturePixel)
{
    std::array<size_t, 5> textures;
    // because the texture at index 0 is actually selected by all other textures being transparent
    // none of the selectable textures needs to have index 0.
    // -> this allows the selection of a total of 17 textures with just 4 bits
    textures[0] = static_cast<size_t>((splatTexturePixel.getRed() & 15) + 1);
    textures[1] = static_cast<size_t>((splatTexturePixel.getGreen() & 15) + 1);
    textures[2] = static_cast<size_t>((splatTexturePixel.getBlue() & 15) + 1);
    textures[3] = static_cast<size_t>((splatTexturePixel.getAlpha() & 15) + 1);
    textures[4] = 0; // background texture always present
    return textures;
}

// ---------------------------------------------------------------------------
void MapEditorState::updateIntensities(const irr::u32 pixelX,
                                       const irr::u32 pixelY,
                                       const size_t textureMixingIndex,
                                       const irr::f32 newIntensity)
{
    // this method assumes that the old intensities were already normalized
    debugOutLevel(Debug::DebugLevels::maxLevel,
                  "pixel",
                  pixelX,
                  pixelY,
                  "updateIntensities for textureMixingIndex",
                  textureMixingIndex,
                  "and newIntensity =",
                  newIntensity);
    // maximum intensity is an easy corner case. All other possibilities require normalization
    if (newIntensity >= maxIntensity)
    {
        for (size_t i = 0; i < this->floatIntensities[pixelX][pixelY].size(); i++)
        {
            this->floatIntensities[pixelX][pixelY][i] = 0;
        }
        this->floatIntensities[pixelX][pixelY] = {0, 0, 0, 0, 0};
        this->floatIntensities[pixelX][pixelY][textureMixingIndex] = maxIntensity;
        return;
    }

    // count how many textures are already displayed
    irr::u32 numOtherTextures = 0;
    for (size_t i = 0; i < this->floatIntensities[pixelX][pixelY].size(); i++)
    {
        if (this->floatIntensities[pixelX][pixelY][i] > 0 and i != textureMixingIndex)
        {
            numOtherTextures++;
        }
    }
    // numOtherTextures can be zero if one mixingTexture is already at maxIntensity and it's
    // intensity should be changed
    // if the intensity of the texture should be lowered the intensity of the background texture
    // must be raised
    // if the background texture has maxIntensity and it's intensity should be lowered abort
    // (because there is no other color visible to take that intensity and the intensity can't be
    // raised because it is already at maximum)
    // -> ignore that case
    if (textureMixingIndex == backgroundTextureMixingIndex and numOtherTextures == 0)
    {
        return;
    }
    irr::f32 intensityDifference = this->floatIntensities[pixelX][pixelY][textureMixingIndex] - newIntensity;
    const irr::f32 intensityDifferencePerTexture =
        std::fabs(intensityDifference / static_cast<irr::f32>(numOtherTextures));
    debugOutLevel(Debug::DebugLevels::updateLoop + 9, "pixel", pixelX, pixelY, "intensityDifference =", intensityDifference, "per texture=", intensityDifferencePerTexture);

    size_t index = 0;
    /// @brief in case of a corrupt splatMap (meaning e.g the intensities don't add up to
    /// maxIntensity) these loops might go on forever -> check against that.
    size_t loopIterations = 0;
    this->floatIntensities[pixelX][pixelY][textureMixingIndex] = newIntensity;
    while (intensityDifference > 0)
    {
        // the old texture was too intense -> move intensity to other textures
        // but make sure to not increment textures which weren't there in the first place (intensity
        // 0) as to not crate textures out of thin air
        if (numOtherTextures == 0)
        {
            // only 1 texture is there with maximum intensity -> move the intensity to the
            // background texture (this case can only happen if the old texture was too intense. If
            // the old texture wasn't intense enough intensity can always be stolen from the
            // background texture) -> this case only in the while(intensityDifference > 0)
            this->floatIntensities[pixelX][pixelY][this->backgroundTextureMixingIndex] +=
                static_cast<irr::f32>(std::fabs(intensityDifference));
            intensityDifference = 0;
        }
        else if (index != textureMixingIndex and this->floatIntensities[pixelX][pixelY][index] > 0 and
                 this->floatIntensities[pixelX][pixelY][index] < maxIntensity)
        {
            this->floatIntensities[pixelX][pixelY][index] += intensityDifferencePerTexture;
            intensityDifference -= intensityDifferencePerTexture;
        }
        index = (index + 1) % numTotalMixableTextures;
        loopIterations++;
        if (loopIterations > 10000)
        {
            Error::errContinue("intensities on splatMap for pixel", pixelX, pixelY, "are corrupted. Resetting to default");
            this->floatIntensities[pixelX][pixelY] = {0.0f, 0.0f, 0.0f, 0.f};
            return;
        }
    }
    while (intensityDifference < 0)
    {
        // the old texture was not intense enough -> steal intensity from other textures
        if (index != textureMixingIndex and this->floatIntensities[pixelX][pixelY][index] > 0)
        {
            this->floatIntensities[pixelX][pixelY][index] -= intensityDifferencePerTexture;
            intensityDifference += intensityDifferencePerTexture;
        }
        index = (index + 1) % numTotalMixableTextures;
        loopIterations++;
        if (loopIterations > 10000)
        {
            Error::errContinue("intensities on splatMap for pixel", pixelX, pixelY, "are corrupted. Resetting to default");
            this->floatIntensities[pixelX][pixelY] = {0.0f, 0.0f, 0.0f, 0.f};
            return;
        }
    }
}

// ---------------------------------------------------------------------------
irr::video::SColor
MapEditorState::getColor(const irr::u32 pixelX, const irr::u32 pixelY, const std::array<size_t, 5> mixedTextures)
{
    debugOutLevel(Debug::DebugLevels::updateLoop + 9,
                  "pixel",
                  pixelX,
                  pixelY,
                  "getColor called "
                  "with");
    for (size_t i = 0; i < numTotalMixableTextures; i++)
    {
        debugOutLevel(Debug::DebugLevels::maxLevel,
                      "pixel",
                      pixelX,
                      pixelY,
                      "texture=",
                      static_cast<int>(mixedTextures[i]),
                      "intensity=",
                      this->floatIntensities[pixelX][pixelY][i]);
    }
    irr::video::SColor color(0, 0, 0, 0);

    // turn the float intensities into irr::u8 intensities because these are used by the shader
    // making sure that the sum of the selectable-texture intensities isn't more than maxIntensity
    // (because otherwise the intensity spills over into the texture selection bits)
    // we actually don't care about the intensity of the 5th color because that will be calculated
    // based on the intensities of the others
    // thus the background texture sucks up all floating point inaccuracies of the other intensities
    irr::s32 totalIntensity = 0;
    irr::f32 correction = 0.0f;
    do
    {
        totalIntensity = 0;
        for (size_t i = 0; i < numFreelyMixableTextures; i++)
        {
            const irr::s32 castIntensity =
                static_cast<irr::s32>(std::round(this->floatIntensities[pixelX][pixelY][i]));
            if (static_cast<irr::f32>(castIntensity) > maxFloatIntensity)
            {
                this->floatIntensities[pixelX][pixelY][i] = maxFloatIntensity;
            }
            else if (castIntensity < 0)
            {
                this->floatIntensities[pixelX][pixelY][i] = 0;
            }
            totalIntensity += castIntensity;
            // just in case the totalIntensity is too large correct all single intensities downwards
            // (correction will be 0 in the first loop)
            this->floatIntensities[pixelX][pixelY][i] -= correction;
        }
        correction = 0.1f;
    } while (totalIntensity > maxIntensity);
    // no need to make sure the sum over all intensities is maxIntensity because the intensity of
    // the background texture will be determined by the shader by substracting away all other
    // intensities

    // because the background texture is selected by setting all selectable intensities to 0 we can
    // expand the range of selectable textures to 17
    // by adding 1 to the texture indices in the shader (meaning index 0 in the color becomes the
    // texture at index 1)
    // -> thus subtract that 1 before putting the index into the color
    color.setRed(static_cast<irr::u32>(mixedTextures[0] - 1) |
                 (static_cast<irr::u32>(std::round(this->floatIntensities[pixelX][pixelY][0])) << 4));
    color.setGreen(static_cast<irr::u32>(mixedTextures[1] - 1) |
                   (static_cast<irr::u32>(std::round(this->floatIntensities[pixelX][pixelY][1])) << 4));
    color.setBlue(static_cast<irr::u32>(mixedTextures[2] - 1) |
                  (static_cast<irr::u32>(std::round(this->floatIntensities[pixelX][pixelY][2])) << 4));
    color.setAlpha(static_cast<irr::u32>(mixedTextures[3] - 1) |
                   (static_cast<irr::u32>(std::round(this->floatIntensities[pixelX][pixelY][3])) << 4));
    // the 5th component always is the same texture (background texture) and the intensity is
    // calculated from the other 4
    return color;
}

// ---------------------------------------------------------------------------
irr::f32 getDistanceFactor(const irr::f32 relativeDistanceToBrushCenter, const bool smoothDrawing_)
{
    if (not smoothDrawing_)
    {
        return 1.0f;
    }
    else if (relativeDistanceToBrushCenter >= 1.0f)
    {
        // because the brush is square and relativeDistanceToBrushCenter is measured compared to one
        // side it might be > 1
        return 0.0f;
    }
    else
    {
        // 3rd order polynom with f(0) = 1 f(1) = 0
        irr::f32 xdFactor = 0.5f * (1.0f - relativeDistanceToBrushCenter) - 0.5f;
        xdFactor = xdFactor * xdFactor * xdFactor;
        return 8.0f * xdFactor + 1.0f;
    }
}

// ---------------------------------------------------------------------------
irr::video::SColor MapEditorState::drawOnFloatIntensities(const size_t selectedTexture,
                                                          const irr::u32 pixelX,
                                                          const irr::u32 pixelY,
                                                          const irr::video::SColor oldSplatTextureColor,
                                                          const irr::f32 relativeDistanceToBrushCenter)
{
    debugOutLevel(Debug::DebugLevels::updateLoop + 9,
                  "pixel",
                  pixelX,
                  pixelY,
                  "drawOnFloatIntensities called for texture",
                  static_cast<int>(selectedTexture),
                  "and old color",
                  colorToStringW(oldSplatTextureColor),
                  "at distance",
                  relativeDistanceToBrushCenter);

    const irr::f32 distanceFactor =
        getDistanceFactor(relativeDistanceToBrushCenter, this->modifiableParameters.smoothDrawing);

    // while incremental drawing is activated the brushIntensity is used as the target which will be
    // drawn to.
    // intensities greater than the brushIntensity will be lowered (by 1) and intensities smaller
    // than the brushIntensity will be raised (by 1)

    // first check if the texture is already there and change the intensity if it is
    std::array<size_t, 5> mixedTextures = this->getTexturesInPixel(oldSplatTextureColor);
    for (size_t i = 0; i < this->numTotalMixableTextures; i++)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 9,
                      "pixel",
                      pixelX,
                      pixelY,
                      "texture =",
                      static_cast<int>(mixedTextures[i]),
                      "intensity =",
                      this->floatIntensities[pixelX][pixelY][i]);
    }

    // begin the tests with the background texture (because that is 1) always there and 2) no
    // selectable texture index should ever be 0 because that would just select the last texture due
    // to the way the texture index is moved up by 1 in the shader)
    for (int i = mixedTextures.size() - 1; i >= 0;
         i--) // save index as int because the loop progresses downwards (and including) zero
    {
        if (mixedTextures[i] == selectedTexture) // texture already present -> only intensity change might be needed
        {
            irr::f32 targetIntensity = this->modifiableParameters.brushIntensity * distanceFactor;
            const irr::f32 textureIntensity = this->floatIntensities[pixelX][pixelY][i];
            if (targetIntensity < textureIntensity and (not this->modifiableParameters.allowIntensityDecrease))
            {
                targetIntensity = textureIntensity; // decreasing not allowed -> set to current intensity
            }
            else if (this->modifiableParameters.incrementalIntensity)
            {
                // either do a 0.25 intensity levels step or step the rest of the way to the target
                // intensity (otherwise the intensity oscillates in range +-0.25 around the target
                // intensity)
                const irr::f32 intensityStep =
                    std::min(0.25f * distanceFactor, std::fabs(textureIntensity - targetIntensity));
                if (std::fabs(textureIntensity - targetIntensity) < 0.01)
                {
                    return oldSplatTextureColor; // we are already close enough at the target
                                                 // intensity
                }
                else if (targetIntensity > textureIntensity)
                {
                    targetIntensity = textureIntensity + intensityStep;
                }
                else
                {
                    targetIntensity = textureIntensity - intensityStep;
                }
            }
            debugOutLevel(Debug::DebugLevels::updateLoop + 9,
                          "pixel",
                          pixelX,
                          pixelY,
                          "texture index",
                          static_cast<int>(i),
                          "updating intensity from",
                          this->floatIntensities[pixelX][pixelY][i],
                          "to",
                          targetIntensity);
            this->updateIntensities(pixelX, pixelY, i, targetIntensity);
            return getColor(pixelX, pixelY, mixedTextures);
        }
    }

    // texture wasn't there yet. If the targetIntensity is 0 then no need to paint anything because
    // that is already reached
    if (this->modifiableParameters.brushIntensity * distanceFactor <= 0.0f)
    {
        return oldSplatTextureColor;
    }

    // if this part of the code is reached the texture wasn't already selected
    // this can only happen if the texture wasn't the background texture -> all following code can
    // only deal with the non-background textures (all mixingTexture indices up to
    // numFreelyMixableTextures)

    // find out the lowest intensity and replace that one
    size_t lowestIndex = 0;
    irr::f32 lowestIntensity = this->floatIntensities[pixelX][pixelY][lowestIndex];
    for (size_t i = 0; i < numFreelyMixableTextures; i++)
    {
        if (this->floatIntensities[pixelX][pixelY][i] < lowestIntensity)
        {
            lowestIndex = i;
            lowestIntensity = this->floatIntensities[pixelX][pixelY][i];
        }
    }
    if (lowestIntensity > 0.0f and not this->modifiableParameters.overwriteLowestIntensity)
    {
        // all indices to mix textures of the pixel where used
        // note: this can never happen for the background texture (because it can't be removed from
        // the selection)
        return oldSplatTextureColor;
    }

    // the color at lowestIndex will be overwritten
    mixedTextures[lowestIndex] = selectedTexture;
    if (this->modifiableParameters.incrementalIntensity)
    {
        updateIntensities(pixelX, pixelY, lowestIndex, distanceFactor);
    }
    else
    {
        updateIntensities(pixelX, pixelY, lowestIndex, this->modifiableParameters.brushIntensity * distanceFactor);
    }

    return getColor(pixelX, pixelY, mixedTextures);
}

// ---------------------------------------------------------------------------
void MapEditorState::drawTextures(const irr::core::aabbox3df brushBox,
                                  const irr::u32 brushSelectedTrianglesCount,
                                  const irr::core::vector2df baseTexCoords,
                                  const irr::core::vector3df baseNormal)
{
    // beginning with the collision point's texture coordinates walk around in PIXEL steps on the
    // texture, do a reverse UV-lookup to find out which triangle contains that pixel
    // if that triangles normal and the baseVertex normal are too much apart this marks an edge on
    // the splatMap that won't be traversed any farther
    // continue for all triangles inside the box and for all 4 cardinal directions
    // -> what remains is a square in pixel coordinates that can be drawn upon

    const irr::u32 width = this->gameMap->getTerrainShader()->getSplatTexture()->getOriginalSize().Width;
    const irr::u32 height = this->gameMap->getTerrainShader()->getSplatTexture()->getOriginalSize().Height;
    // get the texture position from the vertices in the map
    const irr::s32 centerX = static_cast<irr::s32>(static_cast<irr::f32>(width) * baseTexCoords.X);
    const irr::s32 centerY = static_cast<irr::s32>(static_cast<irr::f32>(height) * baseTexCoords.Y);
    debugOutLevel(Debug::DebugLevels::onEvent, "base pixel coords =", centerX, centerY);

    // scale the brushsize from world coordinates to image coordinates
    // the brush is square in world space but that doesn't mean the map texture must be square
    irr::s32 brushPixelSizeX =
        static_cast<irr::s32>(this->modifiableParameters.brushSize * static_cast<irr::f32>(width) /
                              this->gameMap->getMapSize().X);
    irr::s32 brushPixelSizeY =
        static_cast<irr::s32>(this->modifiableParameters.brushSize * static_cast<irr::f32>(height) /
                              this->gameMap->getMapSize().Y);

    // array to hold the pixels to be drawn upon (allocated to the maximum possible needed size)
    // TODO: use pre-crated array and resize if needed (if brushsize changes)
    irr::core::array<irr::core::vector2d<irr::u32>> pixels((2 * brushPixelSizeX + 1) * (2 * brushPixelSizeY + 1));
    irr::core::array<irr::video::SColor> colors((2 * brushPixelSizeX + 1) * (2 * brushPixelSizeY + 1));

    const irr::video::IImage* const textureImage = this->texDrawer->getTextureImage();
    // always draw exactly where the mouse points at if that color doesn't already match
    if (centerX >= 0 && centerX < static_cast<irr::s32>(width) && centerY >= 0 &&
        centerY < static_cast<irr::s32>(height))
    {
        const irr::video::SColor colorToDraw =
            this->drawOnFloatIntensities(this->modifiableParameters.textureIndexToDraw,
                                         centerX,
                                         centerY,
                                         textureImage->getPixel(centerX, centerY),
                                         0);
        if (textureImage->getPixel(centerX, centerY) != colorToDraw)
        {
            // this creates a minimum brush size of 1 pixel on the splatmap even if brushPixelSizeX
            // andY are 0
            pixels.push_back(irr::core::vector2d<irr::u32>(centerX, centerY));
            colors.push_back(colorToDraw);
        }
    }

    for (irr::s32 x = centerX - brushPixelSizeX; x < centerX + brushPixelSizeX; ++x)
    {
        for (irr::s32 y = centerY - brushPixelSizeY; y < centerY + brushPixelSizeY; ++y)
        {
            if (x == 0 and y == 0)
            {
                continue; // already drawn by default. No need to do the expensive reverse-UV-lookup
                          // there
            }
            if (x >= 0 && x < static_cast<irr::s32>(width) && y >= 0 && y < static_cast<irr::s32>(width)) // inside the image coordinates
            {
                debugOutLevel(Debug::DebugLevels::updateLoop + 5, "pixel", x, y, "testing if drawing needs to be done");
                if (this->pixelWorldCoords[x][y].X == FLT_MIN) // testing for unset coordinate on
                                                               // only one component (because no
                                                               // sensible world position is at
                                                               // FLT_MIN)
                {
                    // pixel coordinates back to texture coordinates
                    // all calculations for the texture and pixels done in image coordinates -> no
                    // translation needed.
                    const irr::core::vector2df pixelTexCoords(static_cast<irr::f32>(x) /
                                                                  static_cast<irr::f32>(width - 1),
                                                              static_cast<irr::f32>(y) /
                                                                  static_cast<irr::f32>(height - 1));
                    // get the world coordinates and the triangle that holds that particular pixel
                    irr::u32 triangleIndex = 0;
                    const irr::core::vector3df singlePixelWorldCoords =
                        this->reverseUVLookup(pixelTexCoords, brushSelectedTrianglesCount, triangleIndex);
                    if (triangleIndex >= brushSelectedTrianglesCount)
                    {
                        debugOutLevel(Debug::DebugLevels::updateLoop + 5, "pixel", x, y, "texcoords not on selected triangles");
                        continue; // texCoords wleren't part of any triangle
                    }
                    // update the pixel world coordinates
                    this->pixelWorldCoords[x][y] = singlePixelWorldCoords;
                    this->pixelWorldNormals[x][y] =
                        this->selectedTriangles[triangleIndex].getNormal().normalize();
                }
                // check if the triangle that the pixel is part of has a normal in a range that is
                // to be drawn upon
                // using \vec{x} \dot \vec{y} = |x||y| cos(<angle between(x, y)>)
                if (this->pixelWorldNormals[x][y].dotProduct(baseNormal) < std::cos(3.14 / 2.0)) // TODO: remove hardcoded angle limit of 90 degree
                {
                    debugOutLevel(Debug::DebugLevels::updateLoop + 5, "pixel", x, y, "angle of normal and baseTriangle normal too big");
                    continue;
                }
                if (not brushBox.isPointInside(this->pixelWorldCoords[x][y])) // worldspace
                                                                              // coordinate not part
                                                                              // of worldspace brush
                {
                    debugOutLevel(Debug::DebugLevels::updateLoop + 5,
                                  "pixel",
                                  x,
                                  y,
                                  "point",
                                  vector3dToStringW(this->pixelWorldCoords[x][y]),
                                  "not in brushBox");
                    continue;
                }
                // drawing needs to be done. But rounding from the floatIntensities to irr::u8 might
                // produce the same color.
                const irr::f32 relativeDistanceToBrushCenter =
                    brushBox.getCenter().getDistanceFrom(this->pixelWorldCoords[x][y]) /
                    this->modifiableParameters.brushSize;
                // static_cast<irr::f32>((centerX - x) * (centerX - x) + (centerY - y) * (centerY -
                // y)) / (this->modifiableParameters.brushSize *
                // this->modifiableParameters.brushSize);
                const irr::video::SColor colorToDraw =
                    this->drawOnFloatIntensities(this->modifiableParameters.textureIndexToDraw,
                                                 x,
                                                 y,
                                                 textureImage->getPixel(x, y),
                                                 relativeDistanceToBrushCenter);
                debugOutLevel(Debug::DebugLevels::updateLoop + 5,
                              "pixel",
                              x,
                              y,
                              "comparing drawcolor:",
                              colorToStringW(colorToDraw),
                              "with pixel color",
                              colorToStringW(textureImage->getPixel(x, y)));
                if (colorToDraw == textureImage->getPixel(x, y))
                {
                    // pixel already has the color we would give it and locking the graphics card
                    // costs performance
                    continue;
                }
                debugOutLevel(Debug::DebugLevels::updateLoop + 4,
                              "pixel",
                              x,
                              y,
                              "DRAWING ON "
                              "PIXEL");
                pixels.push_back(irr::core::vector2d<irr::u32>(x, y));
                colors.push_back(colorToDraw);
            }
        }
    }

    if (pixels.size() == 0)
    {
        return;
    }
    // batching the drawcoll onto the texture is faster than drawing every pixel because the texture
    // has to be locked from the graphics card befor drawing.
    // this stops rendering for some time until the texture is unlocked again
    this->texDrawer->drawOnTexture(pixels, colors);
}

// ---------------------------------------------------------------------------
void MapEditorState::initPixelWorldCoordsAndNormals(const irr::video::IImage* splatTextureImage)
{
    this->pixelWorldCoords.resize(splatTextureImage->getDimension().Width);
    this->pixelWorldNormals.resize(splatTextureImage->getDimension().Width);

    for (irr::u32 x = 0; x < splatTextureImage->getDimension().Width; x++)
    {
        this->pixelWorldCoords[x].resize(splatTextureImage->getDimension().Height);
        this->pixelWorldNormals[x].resize(splatTextureImage->getDimension().Height);
        for (irr::u32 y = 0; y < splatTextureImage->getDimension().Height; y++)
        {
            this->pixelWorldCoords[x][y] = irr::core::vector3df(FLT_MIN, FLT_MIN, FLT_MIN);
            this->pixelWorldNormals[x][y] = irr::core::vector3df(FLT_MIN, FLT_MIN, FLT_MIN);
        } // for (irr::u32 y = 0; y < splatTextureImage->getDimension().Height; y++)
    }     // for (irr::u32 x = 0; x < splatTextureImage->getDimension().Width; x++)
}

// ---------------------------------------------------------------------------
void MapEditorState::initFloatIntensities(const irr::video::IImage* const splatTextureImage)
{
    // temp std::array to convert the intensities to floats
    std::array<irr::u8, 5> intensities;
    // set the internal floating point intensities array to the same size as the splatMap
    this->floatIntensities.resize(splatTextureImage->getDimension().Width);
    for (irr::u32 x = 0; x < splatTextureImage->getDimension().Width; x++)
    {
        this->floatIntensities[x].resize(splatTextureImage->getDimension().Height);
        for (irr::u32 y = 0; y < splatTextureImage->getDimension().Height; y++)
        {
            intensities = this->getIntensities(splatTextureImage->getPixel(x, y));
            this->floatIntensities[x][y][0] = static_cast<irr::f32>(intensities[0]);
            this->floatIntensities[x][y][1] = static_cast<irr::f32>(intensities[1]);
            this->floatIntensities[x][y][2] = static_cast<irr::f32>(intensities[2]);
            this->floatIntensities[x][y][3] = static_cast<irr::f32>(intensities[3]);
            this->floatIntensities[x][y][4] = static_cast<irr::f32>(intensities[4]);
        }
    }
}


// ---------------------------------------------------------------------------
void MapEditorState::initEntitySystem()
{
    // POW2_ASSERT(this->entityManager == nullptr);
    // POW2_ASSERT(this->eventManager == nullptr);
    if (!entityManager) this->entityManager = new EntityManager();
    if (!eventManager) this->eventManager = new EventManager();

    // Components::DebugColor
    this->parsers[Components::ComponentID<Components::DebugColor>::CID] =
        new ComponentParsing::GenericComponentParser<Components::DebugColor>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::DebugColor>(this->handleManager)));

    // Components::Position
    this->parsers[Components::ComponentID<Components::Position>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Position>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Position>(this->handleManager)));

    // Components::Movement
    this->parsers[Components::ComponentID<Components::Movement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Movement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Movement>(this->handleManager)));

    // Components::Graphic
    this->parsers[Components::ComponentID<Components::Graphic>::CID] =
        new ComponentParsing::GraphicComponentParser<Components::Graphic>(this->handleManager,
                                                                          this->game.getSceneManager());
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Graphic>(this->handleManager)));

    // Components::ProjectileMovement
    this->parsers[Components::ComponentID<Components::ProjectileMovement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::ProjectileMovement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::ProjectileMovement>(this->handleManager)));

    // Components::Rotation
    this->parsers[Components::ComponentID<Components::Rotation>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Rotation>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Rotation>(this->handleManager)));

    // Components::EntityName
    this->parsers[Components::ComponentID<Components::EntityName>::CID] =
        new ComponentParsing::GenericComponentParser<Components::EntityName>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::EntityName>(this->handleManager)));

    // Components::Price
    this->parsers[Components::ComponentID<Components::Price>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Price>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Damage>(this->handleManager)));

    // Components::Range
    this->parsers[Components::ComponentID<Components::Range>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Range>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Range>(this->handleManager)));

    // Components::Ammo
    this->parsers[Components::ComponentID<Components::Ammo>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::Ammo>(this->handleManager,
                                                                           this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Ammo>(this->handleManager)));

    // Components::Icon
    this->parsers[Components::ComponentID<Components::Icon>::CID] =
        new ComponentParsing::GraphicComponentParser<Components::Icon>(this->handleManager,
                                                                       this->game.getSceneManager());
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Inventory>(this->handleManager)));

    // Components::PurchasableItems
    this->parsers[Components::ComponentID<Components::PurchasableItems>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::PurchasableItems>(this->handleManager,
                                                                                       this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PurchasableItems>(this->handleManager)));

    // Components::Requirement
    this->parsers[Components::ComponentID<Components::Requirement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Requirement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Requirement>(this->handleManager)));

    // Components::SelectionID
    this->parsers[Components::ComponentID<Components::SelectionID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::SelectionID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::SelectionID>(this->handleManager)));

    // Components::Cooldown
    this->parsers[Components::ComponentID<Components::Cooldown>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Cooldown>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Cooldown>(this->handleManager)));

    // Components::Physical
    this->parsers[Components::ComponentID<Components::Physical>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Physical>(this->handleManager);

    // Components::OwnerEntityID
    this->parsers[Components::ComponentID<Components::OwnerEntityID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::OwnerEntityID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::OwnerEntityID>(this->handleManager)));

    // Components::WeaponEntityID
    this->parsers[Components::ComponentID<Components::WeaponEntityID>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::WeaponEntityID>(this->handleManager,
                                                                                     this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::WeaponEntityID>(this->handleManager)));

    // Components::DroppedItemEntityID
    this->parsers[Components::ComponentID<Components::DroppedItemEntityID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::DroppedItemEntityID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::DroppedItemEntityID>(this->handleManager)));

    // Components::PlayerStats
    this->parsers[Components::ComponentID<Components::PlayerStats>::CID] =
        new ComponentParsing::GenericComponentParser<Components::PlayerStats>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PlayerStats>(this->handleManager)));

    // Components::PlayerBase
    this->parsers[Components::ComponentID<Components::PlayerBase>::CID] =
        new ComponentParsing::GenericComponentParser<Components::PlayerBase>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PlayerBase>(this->handleManager)));

    // create ECS Systems and bind them to components

    // GUI-ec-Systems
    /*this->healthBarSystem =
        new HealthBarSystem(this->game.getThreadPool(), *this->eventManager, *this->entityManager,
    this->handleManager, *game); this->entityManager->bindSystem(*(this->healthBarSystem));*/

    // Load blueprints
    irr::io::path pre_path = "data/ecs/";
    irr::io::path const dafaultWorkingDirectory =
        this->game.getDevice()->getFileSystem()->getWorkingDirectory().c_str();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory + "/" + pre_path);
    irr::io::IFileList* fileList = this->game.getDevice()->getFileSystem()->createFileList();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory);

    irr::io::path file = "";
    std::string file_ = "";
    irr::io::path extension = "";
    EntityFactory entityFactory;
    for (unsigned int i = 0; i < fileList->getFileCount(); i++) // TODO: check if starting from 0 is correct
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".json")
            {
                file_ = std::string(irr::core::stringc(file).c_str());
                debugOut("Found json-file '", file_, "'");
                entityFactory.registerBlueprintsFromJson(file_, this->blueprintNameToIDMap);
            }
        }
    }
    for (unsigned int i = 0; i < fileList->getFileCount(); i++) // TODO: check if startig from 0 is correct
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".json")
            {
                file_ = std::string(irr::core::stringc(file).c_str());
                debugOut("load json-file '", file_, "'");
                entityFactory.createBlueprintsFromJson(file_, *this->entityManager, parsers, this->blueprintCollection);
                debugOut("BlueprintCount: ", this->blueprintCollection.size());
            }
        }
    }
}

void MapEditorState::createMapDependentSystems()
{
    POW2_ASSERT(this->grid != nullptr);
    POW2_ASSERT(this->gameMap != nullptr);
    POW2_ASSERT(this->entityManager != nullptr);
    POW2_ASSERT(this->eventManager != nullptr);

    this->graphicSystem = new GraphicSystem(*this->game.getThreadPool(),
                                            *this->eventManager,
                                            *this->entityManager,
                                            this->handleManager,
                                            this->gameMap,
                                            *this->grid,
                                            this->game);
    std::unique_ptr<SystemBase> graphicSystemTransfer(this->graphicSystem);
    this->entityManager->bindSystem(graphicSystemTransfer);

    // Just for testing stuff
    this->testSystem = new TestSystem(*this->game.getThreadPool(),
                                      *this->eventManager,
                                      *this->entityManager,
                                      this->handleManager,
                                      this->gameMap);
    std::unique_ptr<SystemBase> testSystemTransfer(this->testSystem);
    this->entityManager->bindSystem(testSystemTransfer);
}
