/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GAMESTATESERVER_H
#define GAMESTATESERVER_H

#include <irrlicht/IEventReceiver.h>
#include <map>
#include "../state.h"
#include "../../ecs/entityBlueprint.h"

class Game;
class GameMap;
class GameNetworkServer;
class EventManager;
class EntityManager;
class NetworkEventSyncronizationServer;
class NetworkComponentSyncronizationServer;
class MovementSystem;
class ProjectileSystem;
class CooldownSystem;
class DamageSystem;
class AutomaticWeaponSystem;
class EntityFactory;
class ShopSystem;
class InventorySystem;
class MoneyTransferSystem;
namespace Handles
{
    class HandleManager;
}
namespace grid
{
    class Grid;
}


class GameStateServer : public State
{
   public:
    GameStateServer(Game& game);
    GameStateServer(const GameStateServer&) = delete;
    virtual ~GameStateServer();

    void onEnter();
    void onLeave();
    void update();
    virtual bool onEvent(const irr::SEvent& event);

    GameStateServer operator=(const GameStateServer&) = delete;

   private:
    void initEntitySystem();

    GameNetworkServer* gameNetworkObj = nullptr;

    EventManager* eventManager = nullptr;
    Handles::HandleManager* handleManager = nullptr;
    EntityFactory* entityFactory = nullptr;
    EntityManager* entityManager = nullptr;

    MovementSystem* movementSystem = nullptr;
    ProjectileSystem* projectileSystem = nullptr;
    CooldownSystem* cooldownSystem = nullptr;
    DamageSystem* damageSystem = nullptr;
    AutomaticWeaponSystem* automaticWeaponSystem = nullptr;
    ShopSystem* shopSystem = nullptr;
    InventorySystem* inventorySystem = nullptr;
    MoneyTransferSystem* moneyTransferSystem = nullptr;

    NetworkEventSyncronizationServer* networkEventSyncronization = nullptr;
    NetworkComponentSyncronizationServer* networkComponentSyncronization = nullptr;

    GameMap* gameMap = nullptr;
    grid::Grid* grid = nullptr;

    // Networking replica handling...
    irr::u32 lastReplicaListSize = 0;

    // parser stuff
    std::map<std::string, blueprintID_t> blueprintNameToIDMap = {};
    std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>> blueprintCollection = {};
};

#endif // GAMESTATESERVER_H
