/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lobbystate_server.h"


// ----------------------------------------------------------------------------
#include <raknet/GetTime.h>

#include "../../core/game.h"
#include "../../network/server/lobbyState/chatReplica_server.h"
#include "../../network/server/lobbyState/lobbyNetwork_server.h"
#include "../../network/server/network_server.h"
#include "../../network/server/playerCollectionReplica_server.h" // only forward declared in game.h
#include "../../utils/debug.h"
#include "../../utils/static/error.h"

// ----------------------------------------------------------------------------
LobbyStateServer::LobbyStateServer(Game& game_)
    : State(game_)
{
}

// ----------------------------------------------------------------------------
LobbyStateServer::~LobbyStateServer() {}

// ----------------------------------------------------------------------------
void LobbyStateServer::onEnter()
{
    debugOutLevel(25, "Entering LobbyStateServer");

    this->game.getNetwork()->init(); // Network::init() must be called before creating the
                                     // PlayerCollectionServer replica because init() creates the
                                     // ReplicaManager which PlayerCollectionServer needs

    // if there is still an old PlayerCollectionReplica delete it
    if (this->game.getPlayerCollectionReplica() != nullptr)
    {
        delete this->game.getPlayerCollectionReplica();
    }
    this->game.setPlayerCollectionReplica(
        new PlayerCollectionServer(this->game.getNetwork()->getReplicaManager(), this->game.getNetwork()));

    //--START-- Network modification

    debugOutLevel(20, "Create LobbyNetwork");
    this->lobbyNetworkObj =
        new LobbyNetworkServer(&this->game); // LobbyNetworkServer must be created after the
                                             // PlayerCollectionServer replica because the chatReplica
                                             // needs the playerCollectionServer Replica to work

    this->chatReplica = this->lobbyNetworkObj->getChatReplica();
}

// ----------------------------------------------------------------------------
void LobbyStateServer::onLeave()
{
    debugOutLevel(25, "Leaving LobbyStateServer");
}

// ----------------------------------------------------------------------------
void LobbyStateServer::update()
{
    // Network is initialized
    if (this->game.getNetwork()->isInitialized())
    {
        // ...then process all packets
        this->lobbyNetworkObj->processPackets();

        // display welcome messages fo all new clients
        for (size_t i = 0; i < this->lobbyNetworkObj->newPlayerSlots.size(); i++)
        {
            // check if the player already has synced their name using the playerCollectionReplica
            // (they might just have connected with the last processed packet)
            if (this->game.getPlayerCollectionReplica()->getSlots()[i].playerName.value.StrCmp(
                    BattleTanks::Networking::SlotInfos().playerName.value) != 0)
            {
                this->chatReplica->writeOnEnterMessage(
                    this->game.getNetwork()->thisServer.name,
                    this->game.getPlayerCollectionReplica()->getSlots()[i].playerName.value);
                this->lobbyNetworkObj->newPlayerSlots.erase(this->lobbyNetworkObj->newPlayerSlots.begin() + i);
                i--;
            }
        }

        this->lobbyNetworkObj->advertiseToLAN();

        this->lobbyNetworkObj->replyToUnconnectedClients();

        // if the network received a gameStartRequest check once again if all players are ready
        // because the request could have arrived before a packet which set a player to not ready
        if (this->lobbyNetworkObj->clientRequestedGameStart and
            this->game.getPlayerCollectionReplica()->allPlayersReady())
        {
            /// @brief send all client that we are going to change to the gameState
            PauseRequest startRequest;
            startRequest.type =
                PauseRequest::RequestType::Running; // the other flags don't matter as the client
                                                    // only looks for the packetID anyways. The type
                                                    // was just set in case the clients behaviour
                                                    // ever changes
            // the server determines that all players have loaded by checking the 'ready' flag
            // inside the playerCollectionReplica. Because everyone is read ready right now that
            // could immediately start the game -> manually set the flag the server sees to 'not
            // ready' BEFORE sending out the command for the clients to change state and let them
            // serialize the replica once they are ready again
            // (this method might still produce false positives if they changed their 'ready' flag
            // just before we send out the start packet. TODO: maybe ditch the checking of the
            // playerCollectionReplica alltogether and only use specifically send packets and a
            // counter?)
            for (size_t i = 0; i < this->game.getPlayerCollectionReplica()->getSlots().size(); i++)
            {
                this->game.getPlayerCollectionReplica()->getSlots()[i].ready.value =
                    false; // not setting the serialize flag because that isn't needed (and might
                           // actually introduce more problems)
            }
            this->game.getNetwork()->requestStartPauseOrResumeGame(startRequest);
            debugOutLevel(15, "canStartGame");
            this->game.changeState(Game::States_e::Game);
            return; // need to leave update loop after changing state because thate change calls
                    // onLeave and the rest of the update loop might need stuff destructed in
                    // onLeave
        }
    }
}

// ----------------------------------------------------------------------------
bool LobbyStateServer::onEvent(const irr::SEvent& event)
{
    (void) event;
    return false;
}
