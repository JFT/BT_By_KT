/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gamestate_server.h"
// ----------------------------------------------------------------------------
#include <irrlicht/IFileSystem.h>
#include <irrlicht/IrrlichtDevice.h>
#include <raknet/GetTime.h>
#include <raknet/RakSleep.h>
#include <stdint.h>
#include "../../core/game.h"
#include "../../ecs/componentParsers.h"
#include "../../ecs/componentSerializer.h"
#include "../../ecs/components.h"
#include "../../ecs/entityFactory.h"
#include "../../ecs/entityManager.h"
#include "../../ecs/events/eventManager.h"
#include "../../ecs/events/events.h"
#include "../../ecs/events/networkEventReplaySystemServer.h"
#include "../../ecs/events/networkEventSyncronizationServer.h"
#include "../../ecs/networkComponentSyncronizationServer.h"
#include "../../ecs/storageSystems/allEntitiesComponentValueStorage.h"
#include "../../ecs/systems/automaticWeaponSystem.h"
#include "../../ecs/systems/cooldownSystem.h"
#include "../../ecs/systems/damageSystem.h"
#include "../../ecs/systems/guiSystems/healthBarSystem.h"
#include "../../ecs/systems/guiSystems/inventoryGuiSystem.h"
#include "../../ecs/systems/guiSystems/shopGuiSystem.h"
#include "../../ecs/systems/inventorySystem.h"
#include "../../ecs/systems/moneyTransferSystem.h"
#include "../../ecs/systems/movement/movementSystem.h"
#include "../../ecs/systems/projectileSystem.h"
#include "../../ecs/systems/shopSystem.h"
#include "../../map/gamemap.h"
#include "../../map/mapLoader.h"
#include "../../network/replicamanager3bt.h"
#include "../../network/server/gameState/gameNetwork_server.h"
#include "../../network/server/network_server.h"
#include "../../network/server/playerCollectionReplica_server.h" // only forward declared in game.h
#include "../../scripting/scriptingModule.h"
#include "../../utils/HandleManager.h"
#include "../../utils/debug.h"
#include "../../utils/static/error.h"

namespace
{
    NetworkEventSyncronizationServer* enableReplay(const std::string& filename,
                                                   const unsigned int numberOfPlayers,
                                                   ReplicaManager3BT* replicaManager,
                                                   EventManager& eventManager,
                                                   ThreadPool& threadPool)
    {
        std::unique_ptr<std::ifstream> readFile(new std::ifstream());
        readFile->open(filename.c_str(), std::ios_base::in | std::ios_base::binary);
        if (readFile->good())
        {
            return new NetworkEventReplayPlayerServer(readFile, numberOfPlayers, replicaManager, eventManager, threadPool);
        }

        // TODO: handle file reading/writing errors
        std::unique_ptr<std::ofstream> writeFile(new std::ofstream);
        writeFile->open(filename.c_str(), std::ios_base::out | std::ios_base::binary);
        return new NetworkEventReplayRecorderServer(writeFile, numberOfPlayers, replicaManager, eventManager, threadPool);
    }
} // end of anonymous namespace

// ----------------------------------------------------------------------------
GameStateServer::GameStateServer(Game& game_)
    : State(game_)
{
    // ctor
}

// ---------------------------------------------------------------------------
GameStateServer::~GameStateServer()
{
    // dtor
}

// ---------------------------------------------------------------------------
void GameStateServer::onEnter()
{
    debugOutLevel(25, "Entering GameStateServer");

    //--START-- Network modification
    if (this->gameNetworkObj == nullptr) // create the State specific part of the Network
    {
        debugOutLevel(20, "Create GameNetwork");
        this->gameNetworkObj = new GameNetworkServer(&this->game);
    }

    this->eventManager = new EventManager();
    this->handleManager = new Handles::HandleManager();

    MapLoader mapLoader(this->game.getSceneManager(),
                        this->game.getDevice()->getFileSystem(),
                        this->game.getVideoDriver(),
                        this->game);
    const auto loaded = mapLoader.loadMap("data/map/map.xml");
    this->gameMap = std::get<1>(loaded);

    this->grid = new grid::Grid(this->gameMap->getMapSize(), 1024.0f);

    this->initEntitySystem();

    // TODO: add seting before game starts to enable/disable recording
    // TODO: add setting to change file which is recorded to
    this->networkEventSyncronization =
        new NetworkEventSyncronizationServer(this->game.getPlayerCollectionReplica()->getPlayerCount(),
                                             this->game.getNetwork()->getReplicaManager(),
                                             *this->eventManager,
                                             *this->game.getThreadPool());

    std::vector<IComponentSerializerBase*> componentSerializers;
    componentSerializers.push_back(new TrivialComponentSerializer<Components::Position>()); // TODO: stop leaking this memory
    componentSerializers.push_back(new TrivialComponentSerializer<Components::Rotation>()); // TODO: stop leaking this memory

    this->networkComponentSyncronization =
        new NetworkComponentSyncronizationServer(this->game.getNetwork()->getReplicaManager(),
                                                 *this->entityManager,
                                                 *this->handleManager,
                                                 componentSerializers,
                                                 *this->game.getThreadPool(),
                                                 *this->eventManager,
                                                 this->game.getPlayerCollectionReplica()->getPlayerCount());
    std::unique_ptr<SystemBase> networkComponentSyncronizationServerSystemTransfer(this->networkComponentSyncronization);
    this->entityManager->bindSystem(networkComponentSyncronizationServerSystemTransfer);

    game.getPlayerCollectionReplica()->setAllPlayerIDs();
    // wait until all clients have set the 'ready' flag inside the PlayerCollectionReplica and send
    // out a command to start the game in 1 sec if they have
    while (not this->game.getPlayerCollectionReplica()->allPlayersReady())
    {
        this->gameNetworkObj->processPackets(); // needed to update the replicas
        this->game.sleep(static_cast<uint32_t>(1000.0f / this->game.getTickrate() / 3.0f)); // update 3 times
                                                                                            // as fast as the
                                                                                            // normal update
                                                                                            // loop
    }

    // send out the command to start the game in 1 second
    PauseRequest startRequest;
    startRequest.requestTick = 0;
    startRequest.type = PauseRequest::RequestType::Running;
    startRequest.waitTime = 1000;
    this->game.getNetwork()->requestStartPauseOrResumeGame(startRequest); // send out to all connected clients
    this->game.sleep(startRequest.waitTime);

    this->game.setGameStarted(true);

    this->game.getNetwork()->getReplicaManager()->SetDefaultPacketPriority(PacketPriority::HIGH_PRIORITY);

    lastReplicaListSize = this->game.getNetwork()->getReplicaManager()->GetReplicaCount();

    const entityID_t networkSyncronizationTestEntity = 12;
    Components::Movement& mov = this->handleManager->getAsRef<Components::Movement>(
        this->entityManager->getComponent<Components::Movement>(networkSyncronizationTestEntity));
    mov.waypoints.emplace_back(irr::core::vector2df(1000.0f, 1000.0f));
    mov.speed = 200;
    Components::NetworkPriority prio;
    prio.priority = Components::NetworkPriority::BasePriority::High;
    this->entityManager->addInitializedComponent<Components::NetworkPriority>(networkSyncronizationTestEntity,
                                                                              &prio);

    debugOut("Finished onEnter GameStateServer");
}

// ---------------------------------------------------------------------------
void GameStateServer::onLeave()
{
    (void) game;
    debugOutLevel(25, "Leaving GameStateServer");
}

// ---------------------------------------------------------------------------
void GameStateServer::update()
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "tick:", this->game.getTickNumber(), "time (ms) =", RakNet::GetTimeMS());


    // Position syncronization test: move a random entity around.
    const auto withPos =
        this->entityManager->entitiesWithComponent(Components::ComponentID<Components::Position>::CID);
    if (!withPos.empty())
    {
        const auto e = withPos[rand() % withPos.size()];
        if (!this->entityManager->hasComponent<Components::NetworkPriority>(e))
        {
            Components::NetworkPriority prio;
            prio.priority = Components::NetworkPriority::BasePriority::High;
            this->entityManager->addInitializedComponent<Components::NetworkPriority>(e, &prio);
        }
        const auto h =
            this->entityManager->getComponent(e, Components::ComponentID<Components::Position>::CID);
        Components::Position& pos = this->handleManager->getAsRef<Components::Position>(h);

        const float xShift = 300.0 * ((float(rand()) / RAND_MAX) - 0.5);
        const float yShift = 300.0 * ((float(rand()) / RAND_MAX) - 0.5);

        pos.position.X += xShift;
        pos.position.Y += yShift;

        if (this->entityManager->hasComponent<Components::Rotation>(e))
        {
            const auto h =
                this->entityManager->getComponent(e, Components::ComponentID<Components::Rotation>::CID);
            Components::Rotation& rot = this->handleManager->getAsRef<Components::Rotation>(h);
            if (rot.rotation.getLengthSQ() == 0.0f)
                rot.rotation = irr::core::vector3df(180.0f, 0.0f, 0.0f);
            rot.rotation.rotateXYBy(180.0 * xShift / 300.0);
            rot.rotation.rotateXZBy(180.0 * yShift / 300.0);
        }
    }

    this->networkComponentSyncronization->update(0,
                                                 this->game.getScriptingModule()->getScriptEngine(),
                                                 this->game.getDeltaTime());

    this->movementSystem->update(0, this->game.getScriptingModule()->getScriptEngine(), this->game.getDeltaTime());

    this->networkEventSyncronization->throwNetworkReceivedEvents(this->game.getTickNumber());

    if (this->game.getNetwork()->isInitialized())
    {
        this->game.getNetwork()->setReUpdateNecessary(false, this->game.getTickNumber());

        // packet processing is running in the same thread as the game loop,
        // so we do not need to mutex lock our gameobject updates
        this->gameNetworkObj->processPackets(); // TODO check need times???

        DataStructures::List<RakNet::Replica3*> rep3List;
        this->game.getNetwork()->getReplicaManager()->GetReferencedReplicaList(rep3List);
        if (lastReplicaListSize < rep3List.Size())
        {
            for (irr::u32 i = lastReplicaListSize; i < rep3List.Size(); i++)
            {
                // addReplicaToContainers(rep3List[i]);
            }
            lastReplicaListSize = rep3List.Size();
        }

        if (this->game.getGameStarted())
        {
            // uphandler->updateObjects(this->game.getDeltaTime());
        }
    }
}


// ---------------------------------------------------------------------------
bool GameStateServer::onEvent(const irr::SEvent& event)
{
    (void) game;
    (void) event;
    return false;
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void GameStateServer::initEntitySystem()
{
    // Init components parsing and memory management
    std::map<componentID_t, ComponentParsing::IComponentParser*> parsers;

    this->entityManager = new EntityManager();

    // Components::DebugColor
    parsers[Components::ComponentID<Components::DebugColor>::CID] =
        new ComponentParsing::GenericComponentParser<Components::DebugColor>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::DebugColor>(*this->handleManager)));

    // Components::Position
    parsers[Components::ComponentID<Components::Position>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Position>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Position>(*this->handleManager)));

    // Components::Movement
    parsers[Components::ComponentID<Components::Movement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Movement>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Movement>(*this->handleManager)));

    // Components::Graphic
    parsers[Components::ComponentID<Components::Graphic>::CID] =
        new ComponentParsing::GraphicComponentParser<Components::Graphic>(*this->handleManager,
                                                                          this->game.getSceneManager());
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Graphic>(*this->handleManager)));

    // Components::ProjectileMovement
    parsers[Components::ComponentID<Components::ProjectileMovement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::ProjectileMovement>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::ProjectileMovement>(*this->handleManager)));

    // Components::Rotation
    parsers[Components::ComponentID<Components::Rotation>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Rotation>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Rotation>(*this->handleManager)));

    // Components::EntityName
    parsers[Components::ComponentID<Components::EntityName>::CID] =
        new ComponentParsing::GenericComponentParser<Components::EntityName>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::EntityName>(*this->handleManager)));

    // Components::Price
    parsers[Components::ComponentID<Components::Price>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Price>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Price>(*this->handleManager)));

    // Components::Damage
    parsers[Components::ComponentID<Components::Damage>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Damage>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Damage>(*this->handleManager)));

    // Components::Range
    parsers[Components::ComponentID<Components::Range>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Range>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Range>(*this->handleManager)));

    // Components::Ammo
    parsers[Components::ComponentID<Components::Ammo>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::Ammo>(*this->handleManager,
                                                                           this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Ammo>(*this->handleManager)));

    // Components::Icon
    parsers[Components::ComponentID<Components::Icon>::CID] =
        new ComponentParsing::GraphicComponentParser<Components::Icon>(*this->handleManager,
                                                                       this->game.getSceneManager());
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Icon>(*this->handleManager)));

    // Components::Inventory
    parsers[Components::ComponentID<Components::Inventory>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Inventory>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Inventory>(*this->handleManager)));

    // Components::PurchasableItems
    parsers[Components::ComponentID<Components::PurchasableItems>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::PurchasableItems>(*this->handleManager,
                                                                                       this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PurchasableItems>(*this->handleManager)));

    // Components::Requirement
    parsers[Components::ComponentID<Components::Requirement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Requirement>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Requirement>(*this->handleManager)));

    // Components::SelectionID
    parsers[Components::ComponentID<Components::SelectionID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::SelectionID>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::SelectionID>(*this->handleManager)));

    // Components::Cooldown
    parsers[Components::ComponentID<Components::Cooldown>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Cooldown>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Cooldown>(*this->handleManager)));

    // Components::Physical
    parsers[Components::ComponentID<Components::Physical>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Physical>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Physical>(*this->handleManager)));

    // Components::OwnerEntityID
    parsers[Components::ComponentID<Components::OwnerEntityID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::OwnerEntityID>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::OwnerEntityID>(*this->handleManager)));

    // Components::WeaponEntityID
    parsers[Components::ComponentID<Components::WeaponEntityID>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::WeaponEntityID>(*this->handleManager,
                                                                                     this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::WeaponEntityID>(*this->handleManager)));

    // Components::DroppedItemEntityID
    parsers[Components::ComponentID<Components::DroppedItemEntityID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::DroppedItemEntityID>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::DroppedItemEntityID>(*this->handleManager)));

    // Components::PlayerStats
    parsers[Components::ComponentID<Components::PlayerStats>::CID] =
        new ComponentParsing::GenericComponentParser<Components::PlayerStats>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PlayerStats>(*this->handleManager)));

    // Components::PlayerBase
    parsers[Components::ComponentID<Components::PlayerBase>::CID] =
        new ComponentParsing::GenericComponentParser<Components::PlayerBase>(*this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PlayerBase>(*this->handleManager)));

    // Components::NetworkPriority
    /*parsers[Components::ComponentID<Components::NetworkPriority>::CID] =
        new ComponentParsing::GenericComponentParser<Components::NetworkPriority>(*this->handleManager);*/ // TODO: write parser
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::NetworkPriority>(*this->handleManager)));

    // create ECS Systems and bind them to components
    this->movementSystem = new MovementSystem(*this->game.getThreadPool(),
                                              *this->eventManager,
                                              *this->entityManager,
                                              *this->handleManager,
                                              this->gameMap,
                                              *(this->grid),
                                              this->game);
    std::unique_ptr<SystemBase> movementSystemTransfer(this->movementSystem);
    this->entityManager->bindSystem(movementSystemTransfer);

    this->projectileSystem = new ProjectileSystem(*this->game.getThreadPool(),
                                                  *this->eventManager,
                                                  *this->entityManager,
                                                  *this->handleManager,
                                                  this->gameMap);
    std::unique_ptr<SystemBase> projectileSystemTransfer(this->projectileSystem);
    this->entityManager->bindSystem(projectileSystemTransfer);

    this->cooldownSystem =
        new CooldownSystem(*this->game.getThreadPool(), *this->eventManager, *this->handleManager);
    std::unique_ptr<SystemBase> cooldownSystemTransfer(this->cooldownSystem);
    this->entityManager->bindSystem(cooldownSystemTransfer);

    this->damageSystem =
        new DamageSystem(*this->game.getThreadPool(), *this->eventManager, *this->handleManager);
    std::unique_ptr<SystemBase> damageSystemTransfer(this->damageSystem);
    this->entityManager->bindSystem(damageSystemTransfer);

    this->automaticWeaponSystem = new AutomaticWeaponSystem(*this->game.getThreadPool(),
                                                            *this->eventManager,
                                                            *this->entityManager,
                                                            *this->handleManager,
                                                            this->blueprintCollection);
    std::unique_ptr<SystemBase> automaticWeaponSystemTransfer(this->automaticWeaponSystem);
    this->entityManager->bindSystem(automaticWeaponSystemTransfer);

    this->shopSystem = new ShopSystem(*this->game.getThreadPool(),
                                      *this->eventManager,
                                      *this->entityManager,
                                      *this->handleManager,
                                      this->blueprintCollection);
    std::unique_ptr<SystemBase> shopSystemTransfer(this->shopSystem);
    this->entityManager->bindSystem(shopSystemTransfer);

    this->inventorySystem = new InventorySystem(*this->game.getThreadPool(),
                                                *this->eventManager,
                                                *this->entityManager,
                                                *this->handleManager,
                                                this->blueprintCollection);
    std::unique_ptr<SystemBase> inventorySystemTransfer(this->inventorySystem);
    this->entityManager->bindSystem(inventorySystemTransfer);

    this->moneyTransferSystem = new MoneyTransferSystem(*this->game.getThreadPool(),
                                                        *this->eventManager,
                                                        *this->entityManager,
                                                        *this->handleManager);
    std::unique_ptr<SystemBase> moneyTransferSystemTransfer(this->moneyTransferSystem);
    this->entityManager->bindSystem(moneyTransferSystemTransfer);

    // Load blueprints
    irr::io::path pre_path = "data/ecs/";
    irr::io::path const dafaultWorkingDirectory =
        this->game.getDevice()->getFileSystem()->getWorkingDirectory().c_str();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory + "/" + pre_path);
    irr::io::IFileList* fileList = this->game.getDevice()->getFileSystem()->createFileList();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory);

    std::string fileString = "";
    irr::io::path file = "";
    irr::io::path extension = "";
    for (unsigned int i = 2; i < fileList->getFileCount(); i++)
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".json")
            {
                fileString = std::string(irr::core::stringc(file).c_str());
                debugOut("Found jason-file '", fileString, "'");
                entityFactory->registerBlueprintsFromJson(fileString, this->blueprintNameToIDMap);
            }
        }
    }
    for (unsigned int i = 2; i < fileList->getFileCount(); i++)
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".json")
            {
                fileString = std::string(irr::core::stringc(file).c_str());
                debugOut("Load jason-file '", fileString, "'");
                entityFactory->createBlueprintsFromJson(fileString, *this->entityManager, parsers, this->blueprintCollection);
                debugOut("BlueprintCount: ", this->blueprintCollection.size());
            }
        }
    }

    // the fist 10 entitys should be the playerProfiles
    // i think the best way should be to create them by the server/ load them from the map
    for (int i = 0; i < 10; i++)
    {
        entityID_t playerProfileID = this->entityManager->addEntity();
        debugOut("playerProfileID", playerProfileID);
        this->entityManager->addComponent<Components::PlayerBase>(playerProfileID);
        this->entityManager->addComponent<Components::PlayerStats>(playerProfileID);
        this->entityManager->addComponent<Components::Inventory>(playerProfileID);
    }

    // the next 10 entitys should be the start tanks
    // i think the best way should be to create them by the server/ load them from the map
    for (int i = 0; i < 10; i++) // 10
    {
        debugOut("place start unit/tank for player", i + 1);
        const auto blueprintID = this->blueprintNameToIDMap["Titan"];

        Components::SelectionID selectionID;
        selectionID.team = Components::SelectionID::Team(int(i / 5 + 1));
        selectionID.selectionType = Components::SelectionID::SelectionType(int(i + 1));

        Components::OwnerEntityID ownerEntityID;
        ownerEntityID.ownerEntityID = i;

        Components::Position tankPosition;
        tankPosition.position.X = 1500.f - i * 150.0f;
        tankPosition.position.Y = 150.f + i * 150.0f;

        Components::Rotation rotation;
        rotation.rotation = {};

        std::unordered_map<componentID_t, const void* const> initialValues;
        initialValues.insert(std::pair<componentID_t, const void* const>(
            Components::ComponentID<Components::SelectionID>::CID, &selectionID));
        initialValues.insert(std::pair<componentID_t, const void* const>(
            Components::ComponentID<Components::OwnerEntityID>::CID, &ownerEntityID));
        initialValues.insert(
            std::pair<componentID_t, const void* const>(Components::ComponentID<Components::Position>::CID,
                                                        &tankPosition));
        initialValues.insert(
            std::pair<componentID_t, const void* const>(Components::ComponentID<Components::Rotation>::CID,
                                                        &rotation));
        const entityID_t tankEntityID =
            this->blueprintCollection[int(blueprintID)].first.createEntity(initialValues);

        const auto playserH = this->entityManager->getComponent<Components::PlayerBase>(i);
        auto& player = this->handleManager->getAsRef<Components::PlayerBase>(playserH);
        player.tankEntityID = tankEntityID;
    }


    // place shops on the map //TESTING --> later placed by map loading
    std::string shops[] = {std::string("Tank Construction Hall"),
                           std::string("Weapon Factory I"),
                           std::string("Weapon Factory II")};

    float pos = 1000.f;
    float deltaPos = 1000.f;
    for (const std::string& blueprintKey : shops)
    {
        debugOut("place shops: ", blueprintKey);
        auto blueprintID = this->blueprintNameToIDMap[blueprintKey];

        Components::SelectionID selectionID;
        selectionID.team = Components::SelectionID::Team::Team1;
        if (blueprintKey == std::string("Weapon Factory II"))
        {
            selectionID.team = Components::SelectionID::Team::Team2;
        }
        selectionID.selectionType = Components::SelectionID::SelectionType::ItemShop;

        Components::Position shopPosition;
        shopPosition.position.X = pos;
        shopPosition.position.Y = pos;
        pos = pos + deltaPos;

        std::unordered_map<componentID_t, const void* const> initialValues;
        initialValues.insert(std::pair<componentID_t, const void* const>(
            Components::ComponentID<Components::SelectionID>::CID, &selectionID));
        initialValues.insert(
            std::pair<componentID_t, const void* const>(Components::ComponentID<Components::Position>::CID,
                                                        &shopPosition));

        this->blueprintCollection[int(blueprintID)].first.createEntity(initialValues);
    }
}
