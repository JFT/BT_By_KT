/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../utils/stringwstream.h" // stringwstream.h needs to be included before debug.h and error.h (which are included by examplestate.h) because they use use operator<< on a stringw

#include "examplestate.h"
#include "../graphics/graphicsPipeline.h"

#include <CEGUI/RendererModules/Irrlicht/Renderer.h>
#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/IMeshSceneNode.h>
#include <irrlicht/ISceneCollisionManager.h>
#include <irrlicht/ISceneManager.h>

#include "../graphics/debugdraw/debugDrawHelper.hpp"
#include "../graphics/debugdraw/debug_draw.hpp"

#include "../utils/stringwstream.h" // should be included first because it defines operator<< for strigw which other files need
#include "../core/game.h"
#include "../core/rtscameraanimator.h"
#include "../ecs/componentParsers.h"
#include "../ecs/components.h"
#include "../ecs/entityBlueprint.h"
#include "../ecs/entityFactory.h"
#include "../ecs/events/eventManager.h"
#include "../ecs/events/events.h"
#include "../ecs/events/networkEventSyncronizationClientStub.h"
#include "../ecs/savingLoading/savingLoading.h"
#include "../ecs/storageSystems/allEntitiesComponentValueStorage.h"
#include "../ecs/systems/automaticWeaponSystem.h"
#include "../ecs/systems/cooldownSystem.h"
#include "../ecs/systems/damageSystem.h"
#include "../ecs/systems/graphicSystem.h"
#include "../ecs/systems/guiSystems/healthBarSystem.h"
#include "../ecs/systems/inventorySystem.h"
#include "../ecs/systems/moneyTransferSystem.h"
#include "../ecs/systems/movement/movementSystem.h"
#include "../ecs/systems/pathingSystem.h"
#include "../ecs/systems/projectileSystem.h"
#include "../ecs/systems/selectionSystem.h"
#include "../ecs/systems/shopSystem.h"
#include "../ecs/systems/testSystem.h"
#include "../ecs/userinput/inputContextManager.h"
#include "../graphics/particles/particleSystemGPU.h"
#include "../grid/grid.h"
#include "../gui/ceGuiEnvironment.h"
#include "../map/gamemap.h"
#include "../map/mapLoader.h"
#include "../scripting/scriptingModule.h"
#include "../utils/HandleManager.h"
#include "../utils/fgaFileLoader.h"
#include "../utils/threadPool.hpp"

ExampleState::ExampleState(Game& game_)
    : State(game_)
    , handleManager()
    , inputContextManager(new InputContextManager())
{
    // ctor
}

ExampleState::~ExampleState()
{
    // before adding anything to be freed on destruction consider adding it in
    // ExampleState::onLeave() because most
    // of the memory should be freed on state change instead of on destruction (as states
    // exist in memory much longer than they are active)
}

void ExampleState::onEnter()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Entering ExampleState");

    // THIS stops the driver from using mipmaps -> no seams between the tiles
    // this->game.getVideoDriver()->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);
    game.getGraphics()->init();

    MapLoader mapLoader(this->game.getSceneManager(),
                        this->game.getDevice()->getFileSystem(),
                        this->game.getVideoDriver(),
                        this->game);
    const irr::core::stringc mapFile("data/map/map.xml"); // TODO use map.xml
    const auto loaded = mapLoader.loadMap(mapFile.c_str());
    this->gameMap = std::get<1>(loaded);
    game.getGraphics()->addNodeToStage(gameMap->getTerrainNode(), RenderStageType::STATIC);
    game.getGraphics()->addNodeToStage(gameMap->getTerrainNode(), RenderStageType::LIGHTING);

    //set some ambientlight //note r,g,b is color - alpha is power of ambient light not real alpha
    game.getGraphics()->getLighting().setAmbientColor(irr::video::SColorf(1.f, 1.f, 1.f, 0.2f).toSColor());
    // TODO: undo this, only for testing
    // make the map fully walkable (by resizing it to the same size which clears the map)
    // gameMap->getPathGrid().resize(gameMap->getPathGrid().width, gameMap->getPathGrid().height);
    gameMap->generateDebugCubes(false);

    // create Grid
    this->grid = new grid::Grid(this->gameMap->getMapSize(), 1024.0f);

    // needed this->gameMap
    this->initCamera();

    this->myPlayerProfileID = 0; // this->game.getPlayerCollectionReplica()->mySlotID();

    this->initEntitySystem();

    this->initLuaScriptEngine();

    debugOut("test");
    // Performance Test:

    irr::scene::IAnimatedMesh* mesh = this->game.getSceneManager()->getMesh("media/sydney.md2");
    irr::video::ITexture* tex = this->game.getVideoDriver()->getTexture("media/sydney.bmp");

    std::vector<entityID_t> etids;
    for (int i = 0; i < 10; i++)
    {
        entityID_t etid = this->entityManager->addEntity();
        etids.push_back(etid);
    }

    Components::Graphic g;
    g.mesh = mesh;
    g.texture = tex;
    g.offset = 25.0;
    g.scale = irr::core::vector3df(5, 5, 5);

    Components::SelectionID selectionID;
    selectionID.team = Components::SelectionID::Team::Team1;
    selectionID.selectionType = Components::SelectionID::SelectionType::Tank1;

    Components::Physical p;
    p.maxHealth = 2000.0f;
    p.health = p.maxHealth;
    p.armor = 100.0f;
    p.unitTypeFlag = flag_t(UnitTypeE::Creep) | flag_t(UnitTypeE::Ground);
    debugOut("p.unitTypeFlag", p.unitTypeFlag);

    // random number generator for random colors
    std::mt19937 colorTwister;
    std::uniform_int_distribution<irr::u32> colorDist(0, 255);
    irr::video::SColor nextDebugColor =
        irr::video::SColor(255, colorDist(colorTwister), colorDist(colorTwister), colorDist(colorTwister));
    int xof = 0;
    int zof = 0;
    for (int i = 0; i < etids.size(); i++)
    {
        const entityID_t etid = etids[i];

        irr::core::vector2df pos;
        // positioning entities outside the map leads to segfault because they access invalid
        // pathGrid cells -> make sure the entities are always inside the map
        // by checking where the next entity would be placed and start a new row if it would be
        // placed outside of the map
        do
        {
            pos.X = this->gameMap->getPathGrid().unitSize * 3 * xof;
            pos.Y = this->gameMap->getPathGrid().unitSize * 3 * zof;
            xof++;
            if (this->gameMap->getPathGrid().unitSize * 3 * (xof + 1) >=
                0.99 * this->gameMap->getPathGrid().unitSize * 3 * this->gameMap->getPathGrid().width)
            {
                xof = 0;
                zof++;
            }
        } while (this->gameMap->getPathGrid().collidesWithStaticObject(pos, 32, PathGrid::staticObstacleSize));
        Handles::Handle h_pos = this->entityManager->addInitializedComponent<Components::Position>(etid, &pos); // must be an initialized component because if setting the component after adding it the entities will already be assigned to a cell inside the systems (other way: throwing a gridCellChanged() after the position is changed outside of the MovementSystem)

        Handles::Handle h_mov = this->entityManager->addComponent<Components::Movement>(etid);
        this->entityManager->addComponent<Components::Rotation>(etid);

        std::cerr << i << "/" << etids.size() << " added to entity " << etid << ": position component with handle "
                  << h_pos << " and movement component with handle h_mov = " << h_mov
                  << " with ptr address " << this->handleManager.get(h_mov) << std::endl;

        Components::Movement* mp = static_cast<Components::Movement*>(this->handleManager.get(h_mov));
        mp->speed = 500;
        mp->waypoints.push_back(pos); // by targeting the current position all entities will get a
                                      // random target (in the movementSystem) and start pathfinding
                                      // to it

        this->entityManager->addInitializedComponent<Components::DebugColor>(etid, &nextDebugColor);
        nextDebugColor =
            irr::video::SColor(255, colorDist(colorTwister), colorDist(colorTwister), colorDist(colorTwister));

        this->entityManager->addInitializedComponent<Components::Graphic>(etid, &g);
        this->entityManager->addInitializedComponent<Components::Physical>(etid, &p);

        debugOut("add SelectionID", int(selectionID.team), int(selectionID.selectionType), etid);
        this->entityManager->addInitializedComponent<Components::SelectionID>(etid, &selectionID);

        Components::Icon ic;
        ic.texture = g.texture;
        this->entityManager->addInitializedComponent<Components::Icon>(etid, &ic);
    }

    // CREATE two weapon-entities for entity 0 and 1 START

    // TODO make selection id const ->you should not be able to switch between teams and co.
    Components::SelectionID newSelectionID;
    newSelectionID.team = Components::SelectionID::Team::Team2;
    newSelectionID.selectionType = Components::SelectionID::SelectionType::Tank2;

    this->entityManager->removeComponent<Components::SelectionID>(1);
    this->entityManager->addInitializedComponent<Components::SelectionID>(1, &newSelectionID);

    mesh = this->game.getSceneManager()->getMesh("media/models/irrlicht/dwarf.x");
    tex = this->game.getVideoDriver()->getTexture("media/models/irrlicht/dwarf.jpg");

    Components::Graphic graphic_;
    graphic_.mesh = mesh;
    graphic_.texture = tex;
    graphic_.scale = irr::core::vector3df(10, 10, 10);

    this->entityManager->removeComponent<Components::Graphic>(0);
    this->entityManager->addInitializedComponent<Components::Graphic>(0, &graphic_);

    this->entityManager->removeComponent<Components::Graphic>(1);
    this->entityManager->addInitializedComponent<Components::Graphic>(1, &graphic_);

    auto physicalH_0 = this->entityManager->getComponent<Components::Physical>(0);
    auto& physical_0 = this->handleManager.getAsRef<Components::Physical>(physicalH_0);
    physical_0.unitTypeFlag = (int(UnitTypeE::Tank) | int(UnitTypeE::Ground));

    const auto physicalH_1 = this->entityManager->getComponent<Components::Physical>(1);
    auto& physical_1 = this->handleManager.getAsRef<Components::Physical>(physicalH_1);
    physical_1.unitTypeFlag = (int(UnitTypeE::Tank) | int(UnitTypeE::Air));


    entityID_t weaponID = this->entityManager->addEntity();
    etids.push_back(weaponID);

    Components::OwnerEntityID o;
    o.ownerEntityID = 0;

    Components::Ammo a;
    a.nrProjectiles = 1;
    a.projectilID = this->blueprintNameToIDMap["testProjectile"];

    Components::Range r;
    r.squaredRange = 1000 * 1000;

    Components::Damage d;
    d.damage = 25.0f;
    d.attacksUnitTypeFlag = flag_t(UnitTypeE::Tank); //-1 for all

    Components::Cooldown c;
    c.initial = 0.5f;

    this->entityManager->addInitializedComponent<Components::OwnerEntityID>(weaponID, &o);
    this->entityManager->addInitializedComponent<Components::Ammo>(weaponID, &a);
    this->entityManager->addInitializedComponent<Components::Range>(weaponID, &r);
    this->entityManager->addInitializedComponent<Components::Damage>(weaponID, &d);
    this->entityManager->addInitializedComponent<Components::Cooldown>(weaponID, &c);

    weaponID = this->entityManager->addEntity();
    etids.push_back(weaponID);
    o.ownerEntityID = 1;
    r.squaredRange = 750 * 750;
    d.damage = 50.0f;
    this->entityManager->addInitializedComponent<Components::OwnerEntityID>(weaponID, &o);
    this->entityManager->addInitializedComponent<Components::Ammo>(weaponID, &a);
    this->entityManager->addInitializedComponent<Components::Range>(weaponID, &r);
    this->entityManager->addInitializedComponent<Components::Damage>(weaponID, &d);
    this->entityManager->addInitializedComponent<Components::Cooldown>(weaponID, &c);

    // CREATE entity 0 and 1 as weapon END

    // have two entities in a head-on collision
    /*irr::core::vector3df pos1(300, 0, 300);
    pos1.Y = this->gameMap->getTerrainNode()->getHeight(pos1.X, pos1.Z);
    irr::core::vector3df pos2(1000, 0, 0);
    pos2.Y = this->gameMap->getTerrainNode()->getHeight(pos2.X, pos2.Z);
    this->entityManager->addInitializedComponent<Components::Position>(0, &pos1); // must be an initialized component because if setting the component after adding it the entities will already be assigned to a cell inside the systems (other way: throwing a gridCellChanged() after the position is changed outside of the MovementSystem)
    Handles::Handle h_mov = this->entityManager->addComponent<Components::Movement>(0);
    Components::Movement* mp = //static_cast<Components::Movement*>(this->handleManager.get(h_mov));
    mp->speed = 500;
    mp->waypoints.push_back(pos2);
    Components::Graphic g;
    g.mesh = m;
    g.texture = tex;
    this->entityManager->addInitializedComponent<Components::Graphic>(0, &g);

    this->entityManager->addInitializedComponent<Components::Position>(1, &pos2); // must be an initialized component because if setting the component after adding it the entities will already be assigned to a cell inside the systems (other way: throwing a gridCellChanged() after the position is changed outside of the MovementSystem)
    h_mov = this->entityManager->addComponent<Components::Movement>(1);
    mp = static_cast<Components::Movement*>(this->handleManager.get(h_mov));
    mp->speed = 500;
    mp->waypoints.push_back(pos1);
    this->entityManager->addInitializedComponent<Components::Graphic>(1, &g);*/


    this->eventSyncronization =
        new NetworkEventSyncronizationClientStub(*this->eventManager, *this->game.getThreadPool());

    // ParticleSystemTest#
    ParticleEmitterGPU* testEmitterGPU = this->game.getGraphics()->getParticleSystemGPU()->createEmitter(
        irr::core::vector3df(1200, 250, 1000), 0.1f, 1 << 15);
    testEmitterGPU->setTexture(
        this->game.getVideoDriver()->getTexture("media/particles/water.png"));
    testEmitterGPU->addVectorField(this->game.getGraphics()->getParticleSystemGPU()->getVectorField(
        "media/particles/vectorFieldTest.fga"));
    testEmitterGPU->setSpawnCuboid(100.f, 100.f, 100.f);
    irr::scene::ISceneNodeAnimator* partAnim = this->game.getSceneManager()->createFlyCircleAnimator(
        irr::core::vector3df(2000, 750, 2000), 200.f, 0.001f, irr::core::vector3df(0, 1.f, 0));

    testEmitterGPU->addAnimator(partAnim);
    partAnim->drop();

    game.getGraphics()->addNodeToStage(testEmitterGPU, RenderStageType::PARTICLE);

    irr::scene::IParticleSystemSceneNode* ps = this->game.getSceneManager()->addParticleSystemSceneNode(false);
    // this->addedSceneNodesToRemove.push_back(ps);

    irr::scene::IParticleEmitter* em =
        ps->createBoxEmitter(irr::core::aabbox3d<irr::f32>(-7, 0, -7, 7, 1, 7), // emitter size
                             irr::core::vector3df(0.0f, 0.06f, 0.0f),           // initial direction
                             80,
                             100,                                  // emit rate
                             irr::video::SColor(0, 255, 255, 255), // darkest color
                             irr::video::SColor(0, 255, 255, 255), // brightest color
                             800,
                             2000,
                             0,                                    // min and max age, angle
                             irr::core::dimension2df(10.f, 10.f),  // min size
                             irr::core::dimension2df(20.f, 20.f)); // max size

    ps->setEmitter(em); // this grabs the emitter
    em->drop();         // so we can drop it here without deleting it

    irr::scene::IParticleAffector* paf = ps->createFadeOutParticleAffector();

    ps->addAffector(paf); // same goes for the affector
    paf->drop();

    ps->setPosition(irr::core::vector3df(1200, 250, 1000));
    ps->setScale(irr::core::vector3df(2, 2, 2));
    ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, true);
    ps->setMaterialTexture(0, this->game.getVideoDriver()->getTexture("media/particles/fire.bmp"));
    ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

    game.getGraphics()->addNodeToStage(ps, RenderStageType::PARTICLE);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "ExampleState is initialized");
}

void ExampleState::onLeave()
{
    debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Leaving ExampleState");

    // for (auto& node : this->addedSceneNodesToRemove)
    //{
    //    node->remove();
    //}
    // this->addedSceneNodesToRemove.clear();

    // this->game.getGraphics()->effectHandler->clearLightsAndNodes();

    // TODO: Add drop() methods for gamemap and drop all objects created in onEnter()
    delete this->gameMap;
    this->gameMap = nullptr;
    delete this->grid;
    this->grid = nullptr;

    // must delete the blueprintCollection before the entityManager because
    // a blueprint deletes its components values upon destruction
    // (for which they need the entityManager)
    this->blueprintCollection_testing.clear();
    this->blueprintNameToIDMap.clear();
    this->blueprintCollection.clear();

    this->movementSystem = nullptr;
    this->graphicSystem = nullptr;
    this->selectionSystem = nullptr;
    this->projectileSystem = nullptr;
    this->cooldownSystem = nullptr;
    this->damageSystem = nullptr;
    this->automaticWeaponSystem = nullptr;
    this->shopSystem = nullptr;
    this->inventorySystem = nullptr;
    this->moneyTransferSystem = nullptr;
    this->pathingSystem = nullptr;
    this->healthBarSystem = nullptr;
    this->testSystem = nullptr;

    delete this->eventSyncronization;
    this->eventSyncronization = nullptr;

    delete this->entityManager;
    this->entityManager = nullptr;
    delete this->eventManager;
    this->eventManager = nullptr;

    this->game.getCamera()->removeAnimator(this->rtsanim);
    this->game.getGraphics()->clear(); // Clear graphicspipeline last, to drop() remaining nodes
                                       // that were not cleared by other calls.
    this->game.setTickNumber(0);
}

void ExampleState::update()
{
    {
        // test event emission with size_t type (instead of class)
        // let one of the entities oscillate
        static double loopTime = 0.0;
        loopTime += this->game.getDeltaTime();
        Events::ChangeGraphicScale e;
        e.entityID = 1;
        e.newScale = irr::core::vector3df(5.f) + std::sin(loopTime) * irr::core::vector3df(15.f);
        this->eventManager->emit(e);
    }

    {
        Events::TESTEVENT e;
        e.vec.X = -1000;
        this->eventManager->emit(e);
    }

    this->eventSyncronization->throwServerAckedEvents();

    debugOutLevel(Debug::DebugLevels::updateLoop + 1, "Updating ec-Systems");

    this->entityManager->updateSystems(0,
                                       this->game.getScriptingModule()->getScriptEngine(),
                                       this->game.getDeltaTime());

    this->game.setTickNumber(this->game.getTickNumber() + 1);

    dd::arrow(irrVec3ToStdVec(irr::core::vector3df(1200.f, 150.f, 1000.f)).data(),
              irrVec3ToStdVec(irr::core::vector3df(1200.f, 500.f, 1000.f)).data(),
              irrVec3ToStdVec(irr::core::vector3df(0, 0, 1)).data(),
              100.f);
    /*dd::sphere(irrVec3ToStdVec(irr::core::vector3df(1200.f, 1000.f, 1000.f)).data(),
               irrVec3ToStdVec(irr::core::vector3df(1, 1, 1)).data(),
               150.f);*/
    auto currentProjView = game.getSceneManager()->getActiveCamera()->getProjectionMatrix();
    currentProjView *= game.getSceneManager()->getActiveCamera()->getViewMatrix();

    dd::projectedText("Testing Text",
                      irrVec3ToStdVec(irr::core::vector3df(1200.f, 700.f, 1000.f)).data(),
                      irrVec3ToStdVec(irr::core::vector3df(1.f, 0.1f, 0.7f)).data(),
                      currentProjView.pointer(),
                      0.0,
                      0.0,
                      game.getVideoDriver()->getViewPort().getWidth(),
                      game.getVideoDriver()->getViewPort().getHeight());

    dd::point(irrVec3ToStdVec(irr::core::vector3df(900.f, 700.f, 1000.f)).data(),
              irrVec3ToStdVec(irr::core::vector3df(1.f, 0.1f, 0.1f)).data(),
              5.f);

    dd::point(irrVec3ToStdVec(irr::core::vector3df(1100.f, 700.f, 1000.f)).data(),
              irrVec3ToStdVec(irr::core::vector3df(1.f, 0.1f, 0.1f)).data(),
              5.f);

    dd::triangle(irrVec3ToStdVec(irr::core::vector3df(1100.f, 700.f, 1000.f)).data(),
                 irrVec3ToStdVec(irr::core::vector3df(1400.f, 700.f, 1000.f)).data(),
                 irrVec3ToStdVec(irr::core::vector3df(500.f, 700.f, 500.f)).data(),
                 irrVec3ToStdVec(irr::core::vector3df(0.f, 0.5f, 0.7f)).data(),
                 0.3f);


    dd::flush();
}

bool ExampleState::onEvent(const irr::SEvent& event)
{
    this->game.getSceneManager()->getActiveCamera()->OnEvent(event);
    if (event.EventType != irr::EET_KEY_INPUT_EVENT or event.KeyInput.Key <= irr::KEY_KEY_CODES_COUNT)
    {
        if (this->game.getGuiEnvironment()->getRenderer()->injectEvent(event))
        {
            // todo: a gui event should be signaled to the eventSystem as well --> find a general
            return true;
        }
    }

    if (irr::EET_MOUSE_INPUT_EVENT == event.EventType)
    {
        switch (event.MouseInput.Event)
        {
            case irr::EMIE_RMOUSE_PRESSED_DOWN:
            {
                // setTarget(game);
                return true;
            }
            break;
        }
    }

    if (event.EventType == irr::EET_KEY_INPUT_EVENT)
    {
        if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_W && !event.KeyInput.PressedDown)
        {
            gameMap->getTerrainNode()->setMaterialFlag(irr::video::EMF_WIREFRAME,
                                                       !gameMap->getTerrainNode()->getMaterial(0).Wireframe);
            return true;
        }
        else if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_D && !event.KeyInput.PressedDown)
        {
            gameMap->changeDebugCubesVisiblity();
            return true;
        }
        else if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_A && !event.KeyInput.PressedDown)
        {
            if (gameMap->getTerrainNode()->isDebugDataVisible())
            {
                gameMap->getTerrainNode()->setDebugDataVisible(irr::scene::E_DEBUG_SCENE_TYPE::EDS_OFF);
            }
            else
            {
                gameMap->getTerrainNode()->setDebugDataVisible(irr::scene::E_DEBUG_SCENE_TYPE::EDS_FULL);
            }
            return true;
        }
        else if (event.KeyInput.Key == irr::EKEY_CODE::KEY_KEY_P && !event.KeyInput.PressedDown)
        {
            if (selectedNode)
            {
            }
        }
        else if (event.KeyInput.Key == irr::KEY_F5 && !event.KeyInput.PressedDown)
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Pressed KEY F5 -> saving game");
            SavingLoading::SaveLoadDependencies dependencies;
            dependencies.sceneManager = this->game.getSceneManager();
            SavingLoading::saveEntitySystem("quicksave.save", *this->entityManager, this->handleManager, dependencies);
            return true;
        }
        else if (event.KeyInput.Key == irr::KEY_F9 && !event.KeyInput.PressedDown)
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1, "Pressed KEY F9 -> loading game");
            this->game.changeState(Game::States_e::testLoading);
            return true;
        }
    }
    return false;
}

void ExampleState::selectNode()
{
    irr::core::line3df ray = this->game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
        this->game.getDevice()->getCursorControl()->getPosition(),
        this->game.getSceneManager()->getActiveCamera());
    irr::scene::ISceneNode* node =
        this->game.getSceneManager()->getSceneCollisionManager()->getSceneNodeFromRayBB(ray, 1);

    if (node != nullptr)
    {
        printf("NODE SELECT ID:%i\n", int(node->getID()));

        if (node->getID() == 1)
        {
            node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
            selectedNode = static_cast<irr::scene::IAnimatedMeshSceneNode*>(node);
        }
        else if (node->getID() == -1)
        {
            if (selectedNode)
            {
                selectedNode->setMaterialFlag(irr::video::EMF_LIGHTING, true);
                selectedNode = nullptr;
            }
        }

        if (selectedNode)
        {
            printf("selected\n");
            selectedNode->setMD2Animation(irr::scene::EMAT_SALUTE);
        }
    }
    else
    {
        printf("NO NODE Selected\n");
        if (selectedNode)
        {
            selectedNode->setMaterialFlag(irr::video::EMF_LIGHTING, true);
        }

        selectedNode = nullptr;
        // lastnode->setMaterialFlag(irr::video::EMF_LIGHTING, true);
    }
}

void ExampleState::initCamera()
{
    this->rtsanim =
        new RTSCameraAnimator(this->game.getDevice()->getCursorControl(), 1.0f, 10.0f, 400.0f, 2000.0f);

    this->rtsanim->setBounds(
        irr::core::vector2df(this->gameMap->getMapPosition().X,
                             this->gameMap->getMapSize().X + this->gameMap->getMapPosition().X),
        irr::core::vector2df(this->gameMap->getMapPosition().Z,
                             this->gameMap->getMapSize().Y + this->gameMap->getMapPosition().Z));
    this->game.getCamera()->setPosition(irr::core::vector3df(0, 600, 300));
    this->game.getCamera()->setTarget(irr::core::vector3df(0, 0, 0));

    // calculate far value
    // assuming map is in the xz plane
    irr::core::vector3df pos = this->game.getCamera()->getPosition();
    irr::core::vector3df target = this->game.getCamera()->getTarget();
    irr::core::vector3df direction(pos - target);

    irr::f32 hypotenuse = direction.getLength();
    irr::f32 yCam = pos.Y;
    irr::f32 angleV = acosf(yCam / hypotenuse);
    irr::f32 camAspect = this->game.getCamera()->getAspectRatio();
    irr::f32 camFovV = this->game.getCamera()->getFOV() / sqrtf(1.0f + (camAspect * camAspect));
    irr::f32 distance = yCam / (cosf(camFovV + angleV));
    this->game.getCamera()->setFarValue(distance);
    this->game.getCamera()->setNearValue(std::max(0.1f * distance, 1.f));


    this->game.getCamera()->addAnimator(this->rtsanim);
    this->game.getSceneManager()->setActiveCamera(this->game.getCamera());
    this->rtsanim->drop();


    gameMap->getTerrainNode()->getMaterial(0).Lighting = false;

    // this->game.getGraphics()->effectHandler->addShadowToNode(gameMap->getTerrainNode(),
    // filterType);

    // this->game.getGraphics()->effectHandler->addShadowToNode(gc->node, filterType);

    // this->game.getGraphics()->effectHandler->setAmbientColor(irr::video::SColor(255, 32, 32,
    // 32));
    // Set the background clear color to black.
    // this->game.getGraphics()->effectHandler->setClearColour(irr::video::SColor(255, 0, 0, 0));

    // Add ShadowLight
    // The first parameter specifies the shadow map resolution for the shadow light.
    // The shadow map is always square, so you need only pass 1 dimension, preferably
    // a power of two between 512 and 2048, maybe larger depending on your quality
    // requirements and target hardware. We will just pass the value the user picked.
    // The second parameter is the light position, the third parameter is the (look at)
    // target, the next is the light color, and the next two are very important
    // values, the nearValue and farValue, be sure to set them appropriate to the current
    // scene. The last parameter is the FOV (Field of view), since the light is similar to
    // a spot light, the field of view will determine its area of influence. Anything that
    // is outside of a lights frustum (Too close, too far, or outside of it's field of view)
    // will be unlit by this particular light, similar to how a spot light works.
    irr::core::vector3df lightDirection =
        irr::core::vector3df(1500, 0, 1500) - irr::core::vector3df(2100, 1500, 1900);

    DirectionalLight& light = this->game.getGraphics()->getLight();
    light.setLightDirection(lightDirection);
    light.setLightColor(irr::video::SColor(255, 240, 240, 200));
    light.setNearValue(0.1f);
    light.setFarValue(2000.0f);
}

void ExampleState::initEntitySystem()
{
    this->entityManager = new EntityManager();

    // Init components parsing and memory management
    std::map<componentID_t, ComponentParsing::IComponentParser*> parsers;

    // Components::DebugColor
    parsers[Components::ComponentID<Components::DebugColor>::CID] =
        new ComponentParsing::GenericComponentParser<Components::DebugColor>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::DebugColor>(this->handleManager)));
    // this->entityManager->defineComponent(debugColorVS);

    // Components::Position
    parsers[Components::ComponentID<Components::Position>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Position>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Position>(this->handleManager)));

    // Components::Movement
    parsers[Components::ComponentID<Components::Movement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Movement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Movement>(this->handleManager)));

    // Components::Graphic
    parsers[Components::ComponentID<Components::Graphic>::CID] =
        new ComponentParsing::GraphicComponentParser<Components::Graphic>(this->handleManager,
                                                                          this->game.getSceneManager());
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Graphic>(this->handleManager)));

    // Components::ProjectileMovement
    parsers[Components::ComponentID<Components::ProjectileMovement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::ProjectileMovement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::ProjectileMovement>(this->handleManager)));

    // Components::Rotation
    parsers[Components::ComponentID<Components::Rotation>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Rotation>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Rotation>(this->handleManager)));

    // Components::EntityName
    parsers[Components::ComponentID<Components::EntityName>::CID] =
        new ComponentParsing::GenericComponentParser<Components::EntityName>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::EntityName>(this->handleManager)));

    // Components::Price
    parsers[Components::ComponentID<Components::Price>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Price>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Price>(this->handleManager)));

    // Components::Damage
    parsers[Components::ComponentID<Components::Damage>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Damage>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Damage>(this->handleManager)));

    // Components::Range
    parsers[Components::ComponentID<Components::Range>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Range>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Range>(this->handleManager)));

    // Components::Ammo
    parsers[Components::ComponentID<Components::Ammo>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::Ammo>(this->handleManager,
                                                                           this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Ammo>(this->handleManager)));

    // Components::Icon
    parsers[Components::ComponentID<Components::Icon>::CID] =
        new ComponentParsing::GraphicComponentParser<Components::Icon>(this->handleManager,
                                                                       this->game.getSceneManager());
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Icon>(this->handleManager)));

    // Components::Inventory
    parsers[Components::ComponentID<Components::Inventory>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Inventory>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Inventory>(this->handleManager)));

    // Components::PurchasableItems
    parsers[Components::ComponentID<Components::PurchasableItems>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::PurchasableItems>(this->handleManager,
                                                                                       this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PurchasableItems>(this->handleManager)));

    // Components::Requirement
    parsers[Components::ComponentID<Components::Requirement>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Requirement>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Requirement>(this->handleManager)));

    // Components::SelectionID
    parsers[Components::ComponentID<Components::SelectionID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::SelectionID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::SelectionID>(this->handleManager)));

    // Components::Cooldown
    parsers[Components::ComponentID<Components::Cooldown>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Cooldown>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Cooldown>(this->handleManager)));

    // Components::Physical
    parsers[Components::ComponentID<Components::Physical>::CID] =
        new ComponentParsing::GenericComponentParser<Components::Physical>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::Physical>(this->handleManager)));

    // Components::OwnerEntityID
    parsers[Components::ComponentID<Components::OwnerEntityID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::OwnerEntityID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::OwnerEntityID>(this->handleManager)));

    // Components::WeaponEntityID
    parsers[Components::ComponentID<Components::WeaponEntityID>::CID] =
        new ComponentParsing::BlueprintIDComponentParser<Components::WeaponEntityID>(this->handleManager,
                                                                                     this->blueprintNameToIDMap);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::WeaponEntityID>(this->handleManager)));

    // Components::DroppedItemEntityID
    parsers[Components::ComponentID<Components::DroppedItemEntityID>::CID] =
        new ComponentParsing::GenericComponentParser<Components::DroppedItemEntityID>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::DroppedItemEntityID>(this->handleManager)));

    // Components::PlayerStats
    parsers[Components::ComponentID<Components::PlayerStats>::CID] =
        new ComponentParsing::GenericComponentParser<Components::PlayerStats>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PlayerStats>(this->handleManager)));

    // Components::PlayerBase
    parsers[Components::ComponentID<Components::PlayerBase>::CID] =
        new ComponentParsing::GenericComponentParser<Components::PlayerBase>(this->handleManager);
    this->entityManager->defineComponent(std::unique_ptr<IComponentValueStorage>(
        new AllEntitiesComponentValueStorage<Components::PlayerBase>(this->handleManager)));

    // create ECS Systems and bind them to components
    this->eventManager = new EventManager();

    this->movementSystem = new MovementSystem(*this->game.getThreadPool(),
                                              *this->eventManager,
                                              *this->entityManager,
                                              this->handleManager,
                                              this->gameMap,
                                              *(this->grid),
                                              this->game);
    std::unique_ptr<SystemBase> movementSystemTransfer(this->movementSystem);
    this->entityManager->bindSystem(movementSystemTransfer);

    this->graphicSystem = new GraphicSystem(*this->game.getThreadPool(),
                                            *this->eventManager,
                                            *this->entityManager,
                                            this->handleManager,
                                            this->gameMap,
                                            *this->grid,
                                            this->game);
    std::unique_ptr<SystemBase> graphicSystemTransfer(this->graphicSystem);
    this->entityManager->bindSystem(graphicSystemTransfer);

    this->selectionSystem = new SelectionSystem(*this->game.getThreadPool(),
                                                *this->eventManager,
                                                *this->entityManager,
                                                this->handleManager,
                                                *this->inputContextManager,
                                                this->gameMap,
                                                *this->grid,
                                                this->game);
    std::unique_ptr<SystemBase> selectionSystemTransfer(this->selectionSystem);
    this->entityManager->bindSystem(selectionSystemTransfer);

    this->projectileSystem = new ProjectileSystem(*this->game.getThreadPool(),
                                                  *this->eventManager,
                                                  *this->entityManager,
                                                  this->handleManager,
                                                  this->gameMap);
    std::unique_ptr<SystemBase> projectileSystemTransfer(this->projectileSystem);
    this->entityManager->bindSystem(projectileSystemTransfer);

    this->cooldownSystem =
        new CooldownSystem(*this->game.getThreadPool(), *this->eventManager, this->handleManager);
    std::unique_ptr<SystemBase> cooldownSystemTransfer(this->cooldownSystem);
    this->entityManager->bindSystem(cooldownSystemTransfer);

    this->damageSystem = new DamageSystem(*this->game.getThreadPool(), *this->eventManager, this->handleManager);
    std::unique_ptr<SystemBase> damageSystemTransfer(this->damageSystem);
    this->entityManager->bindSystem(damageSystemTransfer);

    this->automaticWeaponSystem = new AutomaticWeaponSystem(*this->game.getThreadPool(),
                                                            *this->eventManager,
                                                            *this->entityManager,
                                                            this->handleManager,
                                                            this->blueprintCollection);
    std::unique_ptr<SystemBase> automaticWeaponSystemTransfer(this->automaticWeaponSystem);
    this->entityManager->bindSystem(automaticWeaponSystemTransfer);

    this->shopSystem = new ShopSystem(*this->game.getThreadPool(),
                                      *this->eventManager,
                                      *this->entityManager,
                                      this->handleManager,
                                      this->blueprintCollection);
    std::unique_ptr<SystemBase> shopSystemTransfer(this->shopSystem);
    this->entityManager->bindSystem(shopSystemTransfer);

    this->inventorySystem = new InventorySystem(*this->game.getThreadPool(),
                                                *this->eventManager,
                                                *this->entityManager,
                                                this->handleManager,
                                                this->blueprintCollection);
    std::unique_ptr<SystemBase> inventorySystemTransfer(this->inventorySystem);
    this->entityManager->bindSystem(inventorySystemTransfer);

    this->moneyTransferSystem =
        new MoneyTransferSystem(*this->game.getThreadPool(), *this->eventManager, *this->entityManager, this->handleManager);
    std::unique_ptr<SystemBase> moneyTransferSystemTransfer(this->moneyTransferSystem);
    this->entityManager->bindSystem(moneyTransferSystemTransfer);

    this->pathingSystem = new PathingSystem(*this->game.getThreadPool(),
                                            *this->eventManager,
                                            *this->entityManager,
                                            this->handleManager,
                                            this->gameMap,
                                            *(this->grid),
                                            this->game);

    // GUI-ec-Systems
    this->healthBarSystem = new HealthBarSystem(*this->game.getThreadPool(),
                                                *this->eventManager,
                                                *this->entityManager,
                                                this->handleManager,
                                                this->game);
    std::unique_ptr<SystemBase> healthBarSystemTransfer(this->healthBarSystem);
    this->entityManager->bindSystem(healthBarSystemTransfer);

    // Just for testing stuff
    // Always update testSystem at last at the end of all systems, which is why it is bound last
    this->testSystem = new TestSystem(*this->game.getThreadPool(),
                                      *this->eventManager,
                                      *this->entityManager,
                                      this->handleManager,
                                      this->gameMap);
    std::unique_ptr<SystemBase> testSystemTransfer(this->testSystem);
    this->entityManager->bindSystem(testSystemTransfer);

    // Load blueprints
    irr::io::path pre_path = "data/ecs/";
    irr::io::path const dafaultWorkingDirectory =
        this->game.getDevice()->getFileSystem()->getWorkingDirectory().c_str();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory + "/" + pre_path);
    irr::io::IFileList* fileList = this->game.getDevice()->getFileSystem()->createFileList();
    this->game.getDevice()->getFileSystem()->changeWorkingDirectoryTo(dafaultWorkingDirectory);

    irr::io::path file = "";
    std::string file_ = "";
    irr::io::path extension = "";
    for (unsigned int i = 2; i < fileList->getFileCount(); i++)
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".json")
            {
                file_ = std::string(irr::core::stringc(file).c_str());
                debugOut("Found json-file '", file_, "'");
                entityFactory->registerBlueprintsFromJson(file_, this->blueprintNameToIDMap);
            }
        }
    }
    for (unsigned int i = 2; i < fileList->getFileCount(); i++)
    {
        if (!fileList->isDirectory(i))
        {
            file = pre_path + fileList->getFileName(i);
            irr::core::getFileNameExtension(extension, file);
            extension.make_lower();
            if (extension == ".json")
            {
                file_ = std::string(irr::core::stringc(file).c_str());
                debugOut("load json-file '", file_, "'");
                entityFactory->createBlueprintsFromJson(file_, *this->entityManager, parsers, this->blueprintCollection);
                debugOut("BlueprintCount: ", this->blueprintCollection.size());
            }
        }
    }

    for (auto& parser : parsers)
    {
        delete parser.second;
    }
}

void ExampleState::initLuaScriptEngine()
{
    ScriptEngine& scriptEngine = this->game.getScriptingModule()->getScriptEngine();
    // define lua variables
    scriptEngine.loopOverInstances(
        [this](sol::state& instance) {
            // std::ref for non-ptr members
            instance["entityManager"] = std::ref(this->entityManager);
            instance["handleManager"] = std::ref(this->handleManager);
            instance["eventManager"] = std::ref(*this->eventManager);
            instance["terrainNode"] = this->gameMap->getTerrainNode();
            instance["grid"] = this->grid;
            instance["testSystem"] = this->testSystem;
            instance["scriptingModule"] = std::ref(*game.getScriptingModule());
            instance["threadPool"] = std::ref(*this->game.getThreadPool());
            instance["graphicsPipeline"] = this->game.getGraphics();
        },
        ScriptEngine::ForceLoop::True);
}
