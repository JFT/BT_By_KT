/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MULTIPLESTATESCONSTANTS_H
#define MULTIPLESTATESCONSTANTS_H

#include <raknet/RakNetTime.h>

namespace BattleTanks
{
    namespace MultipleStatesConstants
    {
        /// @brief used in multiple places to e.g. place an upper bound on memory consumption or to
        /// distribute raknet ids
        constexpr unsigned int AbsoluteMaxNumberOfPlayers = 100;
        /// @brief the server sends us how long to wait inside a special packet (defined in
        /// customPackets.h). But we maximally wait this long
        constexpr RakNet::TimeMS AbsoluteMaximumStartOrPauseWaitTime = 20000;
        constexpr RakNet::TimeMS ServerBrowserTimeoutTime =
            1000; // used by the client in the serverBrowser state and by the server in the
                  // lobbyState. Should be the same for both states because otherwise a client might
                  // timeout from the servers point of view but the client still things it is
                  // connected.
    }             // namespace MultipleStatesConstants
} // namespace BattleTanks

#endif /* ifndef MULTIPLESTATESCONSTANTS_H */
