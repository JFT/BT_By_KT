/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ExampleState_H
#define ExampleState_H

#include <irrlicht/IEventReceiver.h>
#include <irrlicht/irrTypes.h>
#include <irrlicht/matrix4.h>
#include <irrlicht/vector3d.h>
#include <map>

#include "state.h"
#include "../ecs/entityBlueprint.h"
#include "../ecs/entityManager.h"
#include "../utils/HandleManager.h"
// forward declarations
class Game;
class GameMap;
class RTSCameraAnimator;

class InputContextManager;
class EventManager;
class EntityFactory;

class MovementSystem;
class GraphicSystem;
class SelectionSystem;
class ProjectileSystem;
class CooldownSystem;
class DamageSystem;
class AutomaticWeaponSystem;
class ShopSystem;
class InventorySystem;
class MoneyTransferSystem;
class PathingSystem;
class HealthBarSystem;

class TestSystem;

class NetworkEventSyncronizationClientStub;
namespace grid
{
    class Grid;
}
namespace irr
{
    namespace scene
    {
        class IAnimatedMeshSceneNode;
        class ISceneNode;
    } // namespace scene
    namespace gui
    {
        class IGUIStaticText;
    }
} // namespace irr

// actual class declaration
class ExampleState : public State
{
   public:
    explicit ExampleState(Game& game);
    ExampleState(const ExampleState&) = delete;
    virtual ~ExampleState();

    void onEnter();
    void onLeave();
    void update();
    virtual bool onEvent(const irr::SEvent& event);

    ExampleState operator=(const ExampleState&) = delete;

    inline GameMap* getGameMap() { return this->gameMap; }


   private:
    void initCamera();
    void initEntitySystem();
    void initLuaScriptEngine();

    entityID_t myPlayerProfileID = ECS::INVALID_ENTITY;

    GameMap* gameMap = nullptr;
    grid::Grid* grid = nullptr;

    RTSCameraAnimator* rtsanim = nullptr;
    irr::scene::IAnimatedMeshSceneNode* selectedNode = nullptr;

    EntityFactory* entityFactory = nullptr;
    EntityManager* entityManager = nullptr;

    Handles::HandleManager handleManager;

    EventManager* eventManager = nullptr;

    InputContextManager* inputContextManager = nullptr;

    MovementSystem* movementSystem = nullptr;
    GraphicSystem* graphicSystem = nullptr;
    SelectionSystem* selectionSystem = nullptr;
    ProjectileSystem* projectileSystem = nullptr;
    CooldownSystem* cooldownSystem = nullptr;
    DamageSystem* damageSystem = nullptr;
    AutomaticWeaponSystem* automaticWeaponSystem = nullptr;
    ShopSystem* shopSystem = nullptr;
    InventorySystem* inventorySystem = nullptr;
    MoneyTransferSystem* moneyTransferSystem = nullptr;
    PathingSystem* pathingSystem = nullptr;

    HealthBarSystem* healthBarSystem = nullptr;

    TestSystem* testSystem = nullptr;

    void selectNode();

    NetworkEventSyncronizationClientStub* eventSyncronization = nullptr;

    std::vector<EntityBlueprint> blueprintCollection_testing = {};

    // parser stuff
    std::map<std::string, blueprintID_t> blueprintNameToIDMap = {};
    std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>> blueprintCollection = {};

    int uloopctr = 0;

    std::vector<irr::scene::ISceneNode*> addedSceneNodesToRemove = {};

    // Lighting
    irr::core::vector3df lightDirection = irr::core::vector3df(0, 0, 0);
    irr::core::matrix4 lightProjMatrix = irr::core::matrix4();
};

#endif // ExampleState_H
