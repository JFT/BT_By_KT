/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <raknet/RakSleep.h> // Game::sleep(uint32_t timeMS)

#include <irrlicht/IrrlichtDevice.h>
#include "game.h" // class's header file
#include "../network/network.h"
#include "../scripting/scriptingModule.h"
#include "../utils/threadPool.hpp"

#if defined(MAKE_SERVER_) /**just for the Server **/
#include "../network/server/network_server.h"
#else /**just for the Client **/
#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/ISceneManager.h>
#include "../audio/soundEngine.h"
#include "../graphics/Views.h"
#include "../graphics/graphicsPipeline.h"
#include "../gui/ceGuiEnvironment.h"
#include "../network/client/networkclient.h"
#include "../utils/scriptconsole.h"
#endif

/** class constructor for the Server **/
#if defined(MAKE_SERVER_)
Game::Game(Config& configuration_, irr::IrrlichtDevice* device_)
/** class constructor for the Client **/
#else
Game::Game(Config& configuration_, irr::IrrlichtDevice* device_, BT::audio::SoundEngine* soundEngine_, const bool scriptInputOnConsole)
#endif
    // clang-format off
    /** Init-List(Private-Elements) for Client and Server **/
    : StateManager()

#if defined(MAKE_SERVER_)
#else
    , consoleListener(scriptInputOnConsole)
    , scriptConsole(nullptr)
    , tri2Fill()
    , tri2Draw()
    , line2Draw()
    , box2Draw()
    , tri2Fill_2d()
    , tri2Draw_2d()
    , line2Draw_2d()
    , box2Draw_2d()

#endif
    , configuration(configuration_)
    , device(device_)
    , videoDriver(device->getVideoDriver())
    , sceneManager(device->getSceneManager())
    , threadPool(std::make_unique<ThreadPool>())
    , deltaTime(0.0f)
    , frequency(30)
    , scriptingModule(std::make_unique<ScriptingModule>(this->threadPool.get()))

/** Init-List just for the Server **/
#if defined(MAKE_SERVER_)
    , initialized(false)
    , network(std::make_unique<NetworkServer>(this))

/** Init-List just for the Client **/
#else
    , frameRate(120.f) // TODO: expose this setting in config
    , soundEngine(soundEngine_)
    , camera(sceneManager->addCameraSceneNode())
    , graphics(std::make_unique<GraphicsPipeline>(device,sceneManager)) //We can also set it to use 32bit depth buffers/could be an option in the settingsmenu
    , guiEnvironment(new CEGUIEnvironment(device))
    // TODO: find out where to set this and what happens between state transitions
    , canDraw(true)
    , lastFPS(0)
    , frameTime(0)
    , network(std::make_unique<NetworkClient>(this))

#endif
{
    // clang-format on
    /** class constructor for Client and Server **/
    this->device->grab();
    this->device->setEventReceiver(this);
/**just for the Server **/
#if defined(MAKE_SERVER_)
#else /**just for the Client **/
    scriptConsole = std::make_unique<ScriptConsole>(scriptInputOnConsole, this->scriptingModule.get());
    auto& luaState = this->scriptingModule->getScriptEngine().singleInstance();
    this->guiEnvironment->initGui(this->device, &luaState);
    camera->setFarValue(25000.0f);
    camera->setID(0);
#endif
}

/** class destructor **/
Game::~Game()
{
    /** For Client and Server **/
    this->device->drop();
#if defined(MAKE_SERVER_)

#else /** Just for the Client **/
    delete guiEnvironment;
#endif
}

void Game::update()
{
    StateManager::update();
    if (gamestarted)
    {
        tickNumber++;
    }
#if defined(MAKE_SERVER_)
    this->device->getTimer()->tick();
#endif
}

void Game::setTickrate(irr::f32 tickrate)
{
    this->frequency = tickrate;
}

void Game::setTickNumber(uint32_t tickNum)
{
    this->tickNumber = tickNum;
}


void Game::setGameStarted(bool start)
{
    this->gamestarted = start;
}

void Game::quit()
{
    this->getCurrentState()->onLeave();
    this->device->closeDevice();
}


void Game::sleep(uint32_t timeMS)
{
    RakSleep(timeMS);
    this->device->getTimer()->tick();
}

#if defined(MAKE_SERVER_) /** Just for the Server **/

bool Game::isRunning() const
{
    return this->network->isRunning();
}

void Game::setInitialized(bool val)
{
    initialized = val;
}

#else /** For Client and Server **/

bool Game::isRunning() const
{
    return this->device->run();
}

void Game::render()
{
    // (old)viewPort used to notify gui about window size changes
    irr::core::rect<irr::s32> viewPort;
    irr::core::rect<irr::s32> oldViewPort;

    if (device->run())
    {
        // Draw Scene
        this->videoDriver->beginScene(short((irr::video::ECBF_COLOR | irr::video::ECBF_DEPTH)),
                                      irr::video::SColor(255, 0, 0, 0));


        // TODO:THREADING: find out if drawAll is thread save e.g. while creating objects
        if (canDraw)
        {
            graphics->render();

            irr::scene::ICameraSceneNode* activeCam = this->sceneManager->getActiveCamera();

            viewPort = this->videoDriver->getViewPort();

            this->videoDriver->setActiveView(ViewNumbers::CEGUI_VIEW, true);
            this->guiEnvironment->drawAll(this->getFrameTime() / 1000.0f); // TODO check are (1000.f
                                                                           // / game.getFrameRate())
                                                                           // and deltaTime the same

            this->videoDriver->setActiveView(ViewNumbers::SIMPLE_2D_VIEW, true);
            this->videoDriver->setViewPort(viewPort);
            this->videoDriver->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);
            this->videoDriver->setTransform(irr::video::E_TRANSFORMATION_STATE::ETS_PROJECTION,
                                            irr::core::IdentityMatrix);
            this->videoDriver->setTransform(irr::video::E_TRANSFORMATION_STATE::ETS_VIEW, irr::core::IdentityMatrix);
            // draw all 2d things
            irr::video::SMaterial material;
            material.setTexture(0, nullptr);
            material.Lighting = false;
            material.BlendOperation = irr::video::E_BLEND_OPERATION::EBO_ADD;
            this->videoDriver->setMaterial(material);

            for (irr::u32 i = 0; i < this->tri2Draw_2d.size(); i++)
            {
                this->videoDriver->draw3DTriangle(tri2Draw_2d[i].tri, tri2Draw_2d[i].color);
            }
            for (irr::u32 i = 0; i < this->line2Draw_2d.size(); i++)
            {
                this->videoDriver->draw3DLine(line2Draw_2d[i].start,
                                              line2Draw_2d[i].finish,
                                              line2Draw_2d[i].color);
            }
            for (irr::u32 i = 0; i < this->box2Draw_2d.size(); i++)
            {
                this->videoDriver->draw3DBox(box2Draw_2d[i].box, box2Draw_2d[i].color);
            }

            // reset view to default -11
            this->videoDriver->setActiveView(ViewNumbers::DEFAULT_3D_VIEW, false);
        }

        this->videoDriver->endScene();

        // Outside of rendering we can resize the gui (if necessary)
        if (oldViewPort != viewPort)
        {
            this->guiEnvironment->notifyDisplaySizeChanged(viewPort.getWidth(), viewPort.getHeight());
            oldViewPort = viewPort;
        }

        const irr::s32 fps = videoDriver->getFPS();

        if (lastFPS != fps)
        {
            irr::core::stringw tmp(L"BT-Alpha v0.0.1");
            tmp += videoDriver->getName();
            tmp += L"] fps: ";
            tmp += fps;
            tmp += L" polycount: ";
            tmp += videoDriver->getPrimitiveCountDrawn();
            lastFPS = fps;
            device->setWindowCaption(tmp.c_str());
        }

        //device->yield(); //we need to evaluate if this is a good idea
    }
}

irr::video::ITexture* Game::scaleTexture(irr::video::ITexture* SrcTexture,
                                         const irr::core::dimension2d<irr::u32>& destSize)
{
    static irr::u32 counter = 0;
    irr::video::IImage* SrcImage;
    irr::video::IImage* DestImage;
    irr::video::ITexture* IntTexture;
    irr::core::stringc SrcName = "NewTexture";
    SrcName += counter;

    IntTexture = this->videoDriver->addTexture(destSize, "IntermediateTexture");
    SrcImage = this->videoDriver->createImageFromData(SrcTexture->getColorFormat(),
                                                      SrcTexture->getSize(),
                                                      SrcTexture->lock());
    DestImage =
        this->videoDriver->createImageFromData(SrcTexture->getColorFormat(), destSize, IntTexture->lock());

    IntTexture->unlock();
    SrcImage->copyToScaling(DestImage);
    SrcTexture->unlock();

    this->videoDriver->removeTexture(IntTexture);
    IntTexture = this->videoDriver->addTexture(SrcName.c_str(), DestImage);
    SrcImage->drop();
    DestImage->drop();

    ++counter;

    return IntTexture;
}

void Game::setFrameTime(double val)
{
    this->frameTime = val;
}

void Game::setDrawable(const bool val)
{
    this->canDraw = val;
}

#endif /** Just for the client **/
