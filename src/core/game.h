/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GAME_H
#define GAME_H

/** Header for Client and Server **/
#include <chrono>
#include <irrlicht/IEventReceiver.h>
#include <irrlicht/SColor.h>
#include <irrlicht/dimension2d.h>
#include <irrlicht/irrArray.h>
#include <irrlicht/triangle3d.h>
#include <memory> // std::unique_ptr

#include "statemanager.h"

#if defined(MAKE_SERVER_)
/** includes just for the Server **/
#else
/** includes just for the Client **/
#include "../utils/consolelistener.h"
#endif /** include seperation client/server **/

// debug structs to draw triangles in any location.
// TODO: remove on actual release build
struct tri2Draw_s
{
    tri2Draw_s(irr::core::triangle3df tri_, irr::video::SColor color_)
        : tri(tri_)
        , color(color_)
    {
    }
    tri2Draw_s()
        : tri()
        , color(){};
    irr::core::triangle3df tri;
    irr::video::SColor color;
};
struct tri2Fill_s
{
    tri2Fill_s(irr::core::triangle3df tri_, irr::video::SColor color_)
        : tri(tri_)
        , color(color_)
    {
    }
    tri2Fill_s()
        : tri()
        , color(){};
    irr::core::triangle3df tri;
    irr::video::SColor color;
};
struct box2Draw_s
{
    box2Draw_s(irr::core::aabbox3df box_, irr::video::SColor color_)
        : box(box_)
        , color(color_)
    {
    }
    box2Draw_s()
        : box()
        , color(){};
    irr::core::aabbox3df box;
    irr::video::SColor color;
};
struct line2Draw_s
{
    line2Draw_s(const irr::core::vector3df start_, const irr::core::vector3df finish_, const irr::video::SColor color_)
        : start(start_)
        , finish(finish_)
        , color(color_)
    {
    }
    line2Draw_s()
        : start()
        , finish()
        , color(){};
    irr::core::vector3df start;
    irr::core::vector3df finish;
    irr::video::SColor color;
};


/** Forward declaratinos for Client and Server **/
class ScriptConsole;
class ThreadPool;
class ScriptingModule;
class PlayerCollectionReplica;
class LobbyChatReplica;
namespace BT
{
    namespace audio
    {
        class SoundEngine;
    }
} // namespace BT
namespace irr
{
    class IrrlichtDevice;
    class ITimer;
    namespace scene
    {
        class ICameraSceneNode;
        class ISceneManager;
    } // namespace scene
    namespace video
    {
        class IVideoDriver;
        class ITexture;
    } // namespace video
} // namespace irr
class Config;

#if defined(MAKE_SERVER_)
class PlayerCollectionServer;
class NetworkServer;
#else  /** Forward declarations just for the Client **/
class NetworkClient;
class CEGUIEnvironment;
// class EffectHandler;
class GraphicsPipeline;
#endif /** Forward declarations **/

class Game : public irr::IEventReceiver, public StateManager
{
   public:
#if defined(MAKE_SERVER_)
    /// \brief class constructor for the Server
    ///
    /// \param configuration reference to the configuration
    /// \param device a pointer to the Irrlichtdevice
    Game(Config& configuration, irr::IrrlichtDevice* device);
#else /** Just for the Client **/
    /// TODO update
    /// \brief class constructor for the client
    ///
    /// \param configuration reference to the configuration
    /// \param device a pointer to the Irrlichtdevice
    /// \param soundEngine pointer to the BT::audio::SoundEngine
    /// \param stdinToConsole set to true to forward input from stdin to the ingame console
    Game(Config& configuration, irr::IrrlichtDevice* device, BT::audio::SoundEngine* soundEngine, const bool scriptInputOnConsole);

#endif /** Just for the Client **/

    virtual ~Game(); /** class destructor (must be virtual because it needs to overwrite
                        IEventReceiver) **/
    Game(const Game& other) = delete;

    /// \brief shutdown the game
    /// this calls onLeave of the state and tries to free all memory etc.
    void quit();

    inline Config& getConfiguration() const { return this->configuration; };
    inline irr::IrrlichtDevice* getDevice() const { return this->device; };
    inline irr::video::IVideoDriver* getVideoDriver() const { return this->videoDriver; };
    inline irr::scene::ISceneManager* getSceneManager() const { return this->sceneManager; };
    /** Event-handling method OnEvent must be declared because Game is derived from IEventReciever
     * **/
    inline bool OnEvent(const irr::SEvent& event)
    {
        return this->onEvent(event);
    }; /** call onEvent of the StateManager parent **/

    inline ThreadPool* getThreadPool() const { return this->threadPool.get(); };
    void update();
    /// \brief get the fixed deltatime in seconds between two updates
    /// \return irr::f32 in seconds
    inline irr::f32 getDeltaTime() const
    {
        return 1.f / this->frequency;
    } //return this->deltaTime;}; //-> we have a fixed tickrate now

    void sleep(uint32_t timeMS);

    inline double getRuntimeMS() const
    {
        static const auto fixedStart = std::chrono::steady_clock::now();
        const auto now = std::chrono::steady_clock::now();
        using ms = std::chrono::duration<double, std::milli>;
        return std::chrono::duration_cast<ms>(now - fixedStart).count();
    }

    inline irr::f32 getTickrate() { return this->frequency; }
    ///  \brief set the tickrate
    ///
    /// \param tickrate in ticks per second use positive integers
    void setTickrate(irr::f32 tickrate);

    inline uint32_t getTickNumber() const { return this->tickNumber; }
    /// \brief set the tickNumber() - use e.g. when reconnecting
    /// \param tickNum
    void setTickNumber(uint32_t tickNum);

    inline bool getGameStarted() { return this->gamestarted; }
    void setGameStarted(bool start);

    Game operator=(const Game& other) = delete;

    inline ScriptingModule* getScriptingModule() const { return this->scriptingModule.get(); }
    bool isRunning() const;

#if defined(MAKE_SERVER_) /** Just for the Server **/

    void setInitialized(bool val);
    inline bool isInitialized() const { return this->initialized; }
    inline NetworkServer* getNetwork() const { return this->network.get(); }
    PlayerCollectionServer* playerCollection = nullptr;
    inline PlayerCollectionServer* getPlayerCollectionReplica() const
    {
        return this->playerCollection;
    }
    inline void setPlayerCollectionReplica(PlayerCollectionServer* const playerCollection_)
    {
        this->playerCollection = playerCollection_;
    }

#else  /** Just for the Client **/

    inline BT::audio::SoundEngine* getSoundEngine() const { return soundEngine; };
    inline irr::scene::ICameraSceneNode* getCamera() const { return this->camera; };
    inline GraphicsPipeline* getGraphics() const { return this->graphics.get(); };
    inline CEGUIEnvironment* getGuiEnvironment() const { return this->guiEnvironment; };
    PlayerCollectionReplica* playerCollection = nullptr;
    inline PlayerCollectionReplica* getPlayerCollectionReplica() const
    {
        return this->playerCollection;
    }
    inline void setPlayerCollectionReplica(PlayerCollectionReplica* const playerCollection_)
    {
        this->playerCollection = playerCollection_;
    }

    LobbyChatReplica* lobbyChat = nullptr;
    inline LobbyChatReplica* getLobbyChatReplica() const { return this->lobbyChat; }
    inline void setLobbyChatReplica(LobbyChatReplica* const lobbyChat_)
    {
        this->lobbyChat = lobbyChat_;
    }

    void render();
    void setDrawable(bool val);
    inline bool isDrawable() const { return this->canDraw; };
    irr::video::ITexture*
    scaleTexture(irr::video::ITexture* SrcTexture, const irr::core::dimension2d<irr::u32>& destSize);

    inline NetworkClient* getNetwork() const { return this->network.get(); };
    inline irr::f32 getFrameRate() const { return this->frameRate; }
    /// \brief set FrameTime in milliseconds
    void setFrameTime(double val);

    /// \brief get frameTime in milliseconds
    inline double getFrameTime() const { return this->frameTime; }
    ConsoleListener consoleListener;
    std::unique_ptr<ScriptConsole> scriptConsole;

    // arrays to hold the debug triangles
    // TODO: remove on actual release build
    irr::core::array<tri2Fill_s> tri2Fill;
    irr::core::array<tri2Draw_s> tri2Draw;
    irr::core::array<line2Draw_s> line2Draw;
    irr::core::array<box2Draw_s> box2Draw;

    irr::core::array<tri2Fill_s> tri2Fill_2d;
    irr::core::array<tri2Draw_s> tri2Draw_2d;
    irr::core::array<line2Draw_s> line2Draw_2d;
    irr::core::array<box2Draw_s> box2Draw_2d;
#endif /** Just for the client **/
   private:
    /** Konfigurations Datei wird in Application erstellt **/
    Config& configuration;

    irr::IrrlichtDevice* device;

    irr::video::IVideoDriver* videoDriver;

    irr::scene::ISceneManager* sceneManager;

    std::unique_ptr<ThreadPool> threadPool;

    /** Time management **/
    irr::f32 deltaTime;
    /** Tickrate **/
    irr::f32 frequency; // irr::f32 -> no cast necessary

    uint32_t tickNumber = 0;
    bool gamestarted = false;

    std::unique_ptr<ScriptingModule> scriptingModule;

#if defined(MAKE_SERVER_) /** Just for the Server **/

    bool initialized;

    std::unique_ptr<NetworkServer> network;

#else /** Just for the Client **/
    irr::f32 frameRate;

    BT::audio::SoundEngine* soundEngine;

    irr::scene::ICameraSceneNode* camera;

    std::unique_ptr<GraphicsPipeline> graphics;

    CEGUIEnvironment* guiEnvironment;

    // Render management
    bool canDraw;
    irr::s32 lastFPS;
    double frameTime;

    std::unique_ptr<NetworkClient> network;

#endif /** Just for the Client **/
};

#endif // GAME_H
