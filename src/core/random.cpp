/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "random.h"

Random::Random()
    : Q(new uint32_t[CMWC_CYCLE])
{
}

// Make 32 bit random number (some systems use 16 bit RAND_MAX)
uint32_t Random::rand32(void)
{
    uint32_t result = 0;
    result = rand();
    result <<= 16;
    result |= rand();
    return result;
}

// Init all engine parts with seed
void Random::initCMWC(unsigned int seed)
{
    srand(seed);
    for (unsigned int i = 0; i < CMWC_CYCLE; i++)
    {
        Q[i] = rand32();
    }
    do
    {
        c = rand32();
    } while (c >= CMWC_C_MAX);
}

// CMWC engine
uint32_t Random::randCMWC(void)
{
    static uint32_t i = CMWC_CYCLE - 1;
    uint64_t t = 0;
    uint64_t a = 18782;      // as Marsaglia recommends
    uint32_t r = 0xfffffffe; // as Marsaglia recommends
    uint32_t x = 0;

    i = (i + 1) & (CMWC_CYCLE - 1);
    t = a * Q[i] + c;
    c = uint32_t(t >> 32);
    x = uint32_t(t + c);
    if (x < c)
    {
        x++;
        c++;
    }

    return Q[i] = r - x;
}
