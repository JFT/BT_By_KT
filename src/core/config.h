/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONFIG_H
#define CONFIG_H

#include <functional>
#include <string>
#include <unordered_map>

#include <irrlicht/SColor.h>
#include <irrlicht/SIrrCreationParameters.h>
#include <raknet/RakNetTypes.h>

#include "../utils/static/error.h"

#include "../utils/json.h"
// forward declarations
namespace irr
{
    class IrrlichtDevice;
}

class Config
{
   public:
    Config();

    struct Settings
    {
        // GlobalSettings
        bool soundEnabled = true;
        float soundVolume = 50.0f;
        irr::SIrrlichtCreationParameters irrlichtParams;

        // ClientSettings
        std::string playerName = std::string("sad kid without a name :-(");
        irr::video::SColor playerColor = irr::video::SColor(255, 0, 0, 255);

        // ServerSettings
        std::string serverName = std::string("BattleTanks Server");
        short unsigned int serverPort = 1234;
        std::string serverPassword = std::string("");
        short unsigned int numberOfSlots = 10;

        // ConnectedServerBackup
        std::string connectedServerName = std::string("");
        RakNet::SystemAddress connectedServerAddress = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
        std::string connectedServerPassword = std::string("");
        unsigned int myIdentificationKey = 0;


        // constructor starts with default values
        Settings()
            : irrlichtParams(irr::SIrrlichtCreationParameters())
        {
            this->irrlichtParams.DriverType = irr::video::E_DRIVER_TYPE::EDT_BGFX_OPENGL;
            this->irrlichtParams.WindowSize = irr::core::dimension2du(800, 600);
            this->irrlichtParams.Bits = static_cast<irr::u8>(32);
            this->irrlichtParams.Fullscreen = false;
            this->irrlichtParams.Vsync = false;
            this->irrlichtParams.Stencilbuffer = false;
            this->irrlichtParams.AntiAlias = false;
            this->irrlichtParams.Doublebuffer = true;
            this->irrlichtParams.Stereobuffer = false;
        }
    };

    ErrCode loadConfig(const std::string filename);
    ErrCode loadConfigJSON(const std::string filename);
    ErrCode saveConfig(const std::string filename, irr::IrrlichtDevice* device) const;

    ErrCode loadServerBackup(const std::string filename);
    ErrCode saveServerBackup(const std::string filename, irr::IrrlichtDevice* device) const;
    void resetServerBackup();

    inline Settings getSettings() const { return this->currentSettings; }
    inline Settings& getModifiableSettings() { return this->currentSettings; }
    void setSettings(const Settings settings);

    const Settings defaultSettings = Settings();

    ErrCode processVideoConfig(Json::Value& videoConfig);

   private:
    //TODO: find out if moving this around is faster (read http://www.agner.org/optimize/optimizing_cpp.pdf)
    Settings currentSettings = Settings();
    std::unordered_map<std::string, std::function<ErrCode(Json::Value&)>> configHandler;
};

#endif
