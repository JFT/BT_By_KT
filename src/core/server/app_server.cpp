/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "app_server.h"

#include <irrlicht/irrlicht.h> // createDevice()/createDeviceEx()

#include "../game.h"
#include "../../states/server/gamestate_server.h"
#include "../../states/server/lobbystate_server.h"
#include "../../utils/static/error.h"

AppServer::AppServer()
    : config()
    , device(nullptr)
{
}

AppServer::~AppServer() {}

ErrCode AppServer::init()
{
    if (config.loadConfig("config/config.xml") != ErrCodes::NO_ERR)
    {
        Error::errContinue("can't load config.xml!");
        // config will use default values
    }

    irr::SIrrlichtCreationParameters params;
    params.DriverType = irr::video::EDT_NULL;
    device = irr::createDeviceEx(params);

    if (device == nullptr)
    {
        Error::errTerminate("Can't create Irrlicht Device.");
        return 1; // TODO: tell me what the hell is going on!
    }
    return 0;
}

void AppServer::run()
{
    Game game(this->config, this->device);

    // Define states
    LobbyStateServer lobby(game);
    GameStateServer gameState(game);
    this->device->drop();

    // add all states
    game.addState(Game::States_e::Lobby, &lobby);
    game.addState(Game::States_e::Game, &gameState);

    // no Globalstate

    // Start with ...state
    game.setInitialState(&lobby);

    // Time handling for fixed update tickrate
    // rendering is now a lot more independent from logic/movement etc. updating
    double previous = game.getRuntimeMS();
    double lastTick = 0.0f;
    bool loopDidNothing = true;
    while (game.isRunning())
    {
        const double current = game.getRuntimeMS();
        const double elapsed = current - previous;

        lastTick += elapsed;
        while (lastTick >= (1000.0 / game.getTickrate()))
        {
            game.update();
            loopDidNothing = false;
            lastTick -= 1000.0 / game.getTickrate();
        }

        if (loopDidNothing)
        {
            // if we didn't have to update anything we sleep for 1 ms
            // also it calls device->tick to update the irrlicht timer
            game.sleep(1);
        }
        loopDidNothing = true;
        previous = current;
    }
}
