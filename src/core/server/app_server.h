/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef APPSERVER_H
#define APPSERVER_H

#include "../config.h"
#include "../../utils/static/error.h" // defines ErrCode

// forward declarations
namespace irr
{
    class IrrlichtDevice;
}


class AppServer
{
   public:
    AppServer();
    AppServer(const AppServer&) = delete;
    virtual ~AppServer();
    ErrCode init();
    void run();

    AppServer operator=(const AppServer&) = delete;

   private:
    Config config;
    irr::IrrlichtDevice* device;
};

#endif
