/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RTSCAMERAANIMATOR_H
#define RTSCAMERAANIMATOR_H

#include "../utils/irrlicht/eventSimplification.h"

#include <irrlicht/ISceneNodeAnimator.h>
#include <irrlicht/irrArray.h>
#include <irrlicht/position2d.h>
#include <irrlicht/vector2d.h>

#include <array>
#include <functional>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace irr
{
    namespace gui
    {
        class ICursorControl;
    }
} // namespace irr


//! enumeration for key actions for RTS_CAM
enum RTS_CAM_ACTION
{
    // movement directions are strongly set to ints because they are used as array indices into
    // 'movementDirections'.
    RTS_CAM_MOVE_FORWARD = 0,
    RTS_CAM_MOVE_BACKWARD = 1,
    RTS_CAM_MOVE_LEFT = 2,
    RTS_CAM_MOVE_RIGHT = 3,
    RTS_CAM_ZOOM_IN,
    RTS_CAM_ZOOM_OUT,
    RTS_CAM_ROTATE_CW,
    RTS_CAM_COUNT,

    RTS_CAM_FORCE_32BIT = 0x7fffffff
};


class RTSCameraAnimator : public irr::scene::ISceneNodeAnimator
{
   public:
    using KeyboardControlMapping = std::unordered_map<BT::irrlichtHelpers::UniqueEventValue_t, RTS_CAM_ACTION>;

    explicit RTSCameraAnimator(irr::gui::ICursorControl* cursorControl,
                               irr::f32 moveSpeed = 1.0f,
                               irr::f32 zoomingSpeed = 5.0f,
                               irr::f32 lowerZoomLimit = 10.0f,
                               irr::f32 upperZoomLimit = 500.0f,
                               irr::core::vector2df xBounds = irr::core::vector2df(0, 0),
                               irr::core::vector2df yBounds = irr::core::vector2df(0, 0),
                               const KeyboardControlMapping& eventToAction_ = {},
                               irr::f32 borderScrollPercent = 0.1f);

    RTSCameraAnimator(const RTSCameraAnimator&) = delete;

    virtual ~RTSCameraAnimator();

    //! Animates the scene node, currently only works on cameras
    virtual void animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs);

    //! Event receiver
    virtual bool OnEvent(const irr::SEvent& event);

    //! Returns the speed of movement in units per millisecond
    virtual irr::f32 getMoveSpeed() const;

    //! Sets the speed of movement in units per millisecond
    virtual void setMoveSpeed(const irr::f32 moveSpeed);


    //! Sets the keyboard mapping for this animator (old style)
    /** \param map Array of keyboard mappings, see irr::SKeyMap
    \param count Size of the keyboard map array. */
    virtual void setKeyMap(const KeyboardControlMapping& newKeyboardMapping);

    /*    //! Sets the keyboard mapping for this animator
        //!	\param keymap The new keymap array
        virtual void setKeyMap(const irr::core::array<RTSCameraAnimator::MyKeyMap>& keymap);*/

    //! Gets the keyboard mapping for this animator
    virtual const KeyboardControlMapping& getKeyMap() const;

    //! This animator will receive events when attached to the active camera
    virtual bool isEventReceiverEnabled() const { return true; }
    virtual void setBounds(const irr::core::vector2df& xBounds, const irr::core::vector2df yBounds);

    //! Creates a clone of this animator.
    /** Please note that you will have to drop
    (IReferenceCounted::drop()) the returned pointer once you're
    done with it. */
    virtual irr::scene::ISceneNodeAnimator*
    createClone(irr::scene::ISceneNode* node, irr::scene::ISceneManager* newManager = nullptr);

    RTSCameraAnimator operator=(const RTSCameraAnimator&) = delete;

   protected:
   private:
    irr::gui::ICursorControl* CursorControl;

    std::unordered_map<BT::irrlichtHelpers::UniqueEventValue_t, std::function<bool(const irr::SEvent&)>> actionMap = {};

    // mapping between keys -> camera actions
    // clang-format off
    KeyboardControlMapping keyboardControls = {
        {BT::irrlichtHelpers::toCombined(irr::EEVENT_TYPE::EET_KEY_INPUT_EVENT, irr::KEY_UP),    RTS_CAM_MOVE_FORWARD},
        {BT::irrlichtHelpers::toCombined(irr::EEVENT_TYPE::EET_KEY_INPUT_EVENT, irr::KEY_DOWN),  RTS_CAM_MOVE_BACKWARD},
        {BT::irrlichtHelpers::toCombined(irr::EEVENT_TYPE::EET_KEY_INPUT_EVENT, irr::KEY_LEFT),  RTS_CAM_MOVE_LEFT},
        {BT::irrlichtHelpers::toCombined(irr::EEVENT_TYPE::EET_KEY_INPUT_EVENT, irr::KEY_RIGHT), RTS_CAM_MOVE_RIGHT},
        {BT::irrlichtHelpers::toCombined(irr::EEVENT_TYPE::EET_KEY_INPUT_EVENT, irr::KEY_PLUS),  RTS_CAM_ZOOM_IN},
        {BT::irrlichtHelpers::toCombined(irr::EEVENT_TYPE::EET_KEY_INPUT_EVENT, irr::KEY_MINUS), RTS_CAM_ZOOM_OUT},
    };

    // translate desired movement directions to irrlicht vectors
    const std::array<irr::core::vector3df, 4> movementDirections
    {{
        {  0.0f, 0.0f, -1.0f}, // RTS_CAM_MOVE_FORWARD 
        {  0.0f, 0.0f, +1.0f}, // RTS_CAM_MOVE_BACKWARD
        { +1.0f, 0.0f,  0.0f}, // RTS_CAM_MOVE_LEFT
        { -1.0f, 0.0f,  0.0f}, // RTS_CAM_MOVE_RIGHT
    }};
    // clang-format on

    irr::f32 wheelzoom;
    irr::f32 zoomspeed;
    irr::f32 upperzoomlimit;
    irr::f32 lowerzoomlimit;

    // vect2df: X->x/y lower limit - Y: upper limit
    irr::core::vector2df xBoundaries;
    irr::core::vector2df yBoundaries;

    irr::f32 MoveSpeed;

    // how close to the border the mouse needs to be to start scrolling
    irr::f32 borderScrollPercent;

    irr::s32 LastAnimationTime;

    irr::core::vector3df originalCameraDirection = {};

    // Because inside OnEvent() we don't have access to the camera we cannot handle events directly.
    // Instead they are stored inside these arrays and used inside 'animateNode()'
    // Because irrlicht doesn't send correct mouse states for the middle mouse button we cannot
    // simply capture and re-throw the events. Some Actions also depend on the update rate (e.g.
    // zoom amount, scrolling) Which shouldn't be buffered and re-applied inside the next update,
    // thus preventing e.g. scrolling all the way over the map because the framerate stuttered.
    //
    // A value of '0.0f' singals an inactive action.
    //
    // ALSO: seperating the 2nd kind of actions between mouse + keyboard so they can "fight" against
    // one another (e.g. stop mouse-movement with keyboard)
    std::array<float, RTS_CAM_ACTION::RTS_CAM_COUNT> keyboardActions;
    std::array<float, RTS_CAM_ACTION::RTS_CAM_COUNT> mouseActions;

    // functions

    void clampToXZBounds(irr::core::vector3df& pos, irr::core::vector3df& target);

    bool firstUpdate = true;
};

#endif // RTSCAMERAANIMATOR_H
