/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"

#include <fstream>
#include <irrlicht/IFileSystem.h>
#include <irrlicht/IXMLReader.h>
#include <irrlicht/IXMLWriter.h>
#include <irrlicht/IrrlichtDevice.h>
#include <irrlicht/irrArray.h>
#include "../utils/stringwstream.h" // stringwstream needs to be included before debug.h and error.h because they use operator<< on a stringw
#include "../utils/debug.h"
#include "../utils/json.h"
#include "../utils/platform_independent/static/folderoperations.h"
#include "../utils/stringwparser.h"
Config::Config()
{
    configHandler["video"] = [this](Json::Value& p) { return this->processVideoConfig(p); };
}

namespace
{
    const std::unordered_map<std::string, irr::video::E_DRIVER_TYPE> stringToDriverMapping = {
        {"software", irr::video::E_DRIVER_TYPE::EDT_SOFTWARE},
        {"opengl", irr::video::E_DRIVER_TYPE::EDT_OPENGL},
        {"bgfx_opengl", irr::video::E_DRIVER_TYPE::EDT_BGFX_OPENGL},
        {"bgfx_d3d9", irr::video::E_DRIVER_TYPE::EDT_BGFX_D3D9},
        {"bgfx_d3d11", irr::video::E_DRIVER_TYPE::EDT_BGFX_D3D11},
        {"bgfx_metal", irr::video::E_DRIVER_TYPE::EDT_BGFX_METAL},
        {"null", irr::video::E_DRIVER_TYPE::EDT_NULL},
        {"direct3d9", irr::video::E_DRIVER_TYPE::EDT_DIRECT3D9},
        {"burningsvideo", irr::video::E_DRIVER_TYPE::EDT_BURNINGSVIDEO}};
}

ErrCode Config::processVideoConfig(Json::Value& videoConfig)
{
    try
    {
        for (const auto setting : videoConfig.getMemberNames())
        {
            if (setting == "driver")
            {
                auto it = stringToDriverMapping.find(videoConfig[setting].asString());
                if (it != stringToDriverMapping.end())
                {
                    currentSettings.irrlichtParams.DriverType = it->second;
                }
                else
                {
                    throw std::runtime_error("Invalid video driver in config");
                }
            }
            else if (setting == "resolution")
            {
                std::uint32_t width = 0, height = 0;
                width = videoConfig[setting][0].asUInt();
                height = videoConfig[setting][1].asUInt();
                if (!width || !height)
                {
                    throw std::runtime_error("Invalid Height or Width value in resolution setting");
                }
                currentSettings.irrlichtParams.WindowSize = irr::core::dimension2du(width, height);
            }
            else if (setting == "fullscreen")
            {
                currentSettings.irrlichtParams.Fullscreen = videoConfig[setting].asBool();
            }
            else if (setting == "videodepth")
            {
                if (videoConfig[setting].asUInt() > 32)
                {
                    throw std::runtime_error("Invalid BitDepth - more than 32 Bits not supported!");
                }
                currentSettings.irrlichtParams.Bits = uint8_t(videoConfig[setting].asUInt());
            }
            else if (setting == "vsync")
            {
                currentSettings.irrlichtParams.Vsync = videoConfig[setting].asBool();
            }
            else if (setting == "stencilbuffer")
            {
                currentSettings.irrlichtParams.Stencilbuffer = videoConfig[setting].asBool();
            }
            else if (setting == "antialias")
            {
                if (videoConfig[setting].asUInt() > 16)
                {
                    throw std::runtime_error(
                        "Unsupported Antialiasing Value in Config - more than 16 not supported!");
                }
                currentSettings.irrlichtParams.AntiAlias = uint8_t(videoConfig[setting].asUInt());
            }
            else if (setting == "doublebuffer")
            {
                currentSettings.irrlichtParams.Doublebuffer = videoConfig[setting].asBool();
            }
            else if (setting == "stereobuffer")
            {
                currentSettings.irrlichtParams.Stereobuffer = videoConfig[setting].asBool();
            }
            else
            {
                throw std::runtime_error(std::string("Unrecognized Config Setting: ") + setting);
                return ErrCodes::GENERIC_ERR;
            }
        }
    }
    catch (std::exception& e)
    {
        Error::errContinue("Caught Error while parsing video config!\nError: ", e.what());
        currentSettings.irrlichtParams = defaultSettings.irrlichtParams;
        return ErrCodes::GENERIC_ERR;
    }

    return ErrCodes::NO_ERR;
}

ErrCode Config::loadConfigJSON(const std::string filename)
{
    debugOutLevel(Debug::DebugLevels::stateInit, "loading config file", filename);
    std::ifstream file;
    file.open(filename.c_str());
    if (!file.is_open())
    {
        return ErrCodes::GENERIC_ERR;
    }

    Json::Value p;
    file >> p;

    for (const auto configTypeName : p.getMemberNames())
    {
        auto it = configHandler.find(configTypeName);
        if (it != configHandler.end())
        {
            if (it->second(p[configTypeName]) != ErrCodes::NO_ERR)
            {
                return ErrCodes::GENERIC_ERR;
            }
        }
    }
    return ErrCodes::NO_ERR;
}

ErrCode Config::loadConfig(const std::string filename)
{
    debugOutLevel(Debug::DebugLevels::stateInit, "loading config file", filename);
    irr::io::IrrXMLReader* xml = irr::io::createIrrXMLReader(filename.c_str());

    if (!xml)
    {
        Error::errContinue("failed to open config file", repr<std::string>(filename));
        return ErrCodes::GENERIC_ERR;
    }

    enum configurationParts
    {
        PLAYER,
        VIDEO,
        AUDIO,
        SERVER,
        NONE
    };
    configurationParts configurationPart = configurationParts::NONE;

    while (xml->read())
    {
        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                      "read node name:",
                      repr<const char*>(xml->getNodeName()),
                      "data=",
                      repr<const char*>(xml->getNodeData()));
        switch (xml->getNodeType())
        {
            case irr::io::EXN_ELEMENT:
                switch (configurationPart)
                {
                    case configurationParts::NONE:
                    {
                        if (irr::core::stringw(L"PLAYER").equals_ignore_case(xml->getNodeName()))
                        {
                            configurationPart = configurationParts::PLAYER;
                        }
                        else if (irr::core::stringw(L"VIDEO").equals_ignore_case(xml->getNodeName()))
                        {
                            configurationPart = configurationParts::VIDEO;
                        }
                        else if (irr::core::stringw(L"AUDIO").equals_ignore_case(xml->getNodeName()))
                        {
                            configurationPart = configurationParts::AUDIO;
                        }
                        else if (irr::core::stringw(L"SERVER").equals_ignore_case(xml->getNodeName()))
                        {
                            configurationPart = configurationParts::SERVER;
                        }
                        else if (irr::core::stringw(L"configuration").equals_ignore_case(xml->getNodeName()))
                        {
                            // ignore because that is the main element of the file
                        }
                        else
                        {
                            Error::errContinue("unknown configuration part",
                                               repr<const char*>(xml->getNodeName()));
                            configurationPart = configurationParts::NONE;
                        }
                    }
                    break;
                    case configurationParts::PLAYER:
                    {
                        if (irr::core::stringw(L"Name").equals_ignore_case(xml->getNodeName()))
                        {
                            this->currentSettings.playerName =
                                std::string(xml->getAttributeValueSafe("value"));
                        }
                        else if (irr::core::stringw(L"Color").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.playerColor,
                                                     this->defaultSettings.playerColor) != ErrCodes::NO_ERR)
                            {
                                Error::errContinue(
                                    "couldn't parse playerColor from element 'Color' value:",
                                    repr<const char*>(xml->getAttributeValueSafe("value")));
                            }
                        }
                        else
                        {
                            Error::errContinue("unknown element with name",
                                               repr<const char*>(xml->getNodeName()),
                                               "in element 'PLAYER'");
                        }
                    }
                    break;
                    case configurationParts::VIDEO:
                    {
                        if (irr::core::stringw(L"Driver").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.irrlichtParams.DriverType,
                                                     this->defaultSettings.irrlichtParams.DriverType) !=
                                ErrCodes::NO_ERR)
                            {
                                Error::errContinue(
                                    "couldn't parse video driver from element 'Driver' value:",
                                    repr<const char*>(xml->getAttributeValueSafe("value")));
                            }
                        }
                        else if (irr::core::stringw(L"Resolution").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.irrlichtParams.WindowSize,
                                                     this->defaultSettings.irrlichtParams.WindowSize) !=
                                ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'Resolution' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else if (irr::core::stringw(L"Fullscreen").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.irrlichtParams.Fullscreen,
                                                     this->defaultSettings.irrlichtParams.Fullscreen) !=
                                ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'Fullscreen' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else if (irr::core::stringw(L"VideoDepth").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.irrlichtParams.Bits,
                                                     this->defaultSettings.irrlichtParams.Bits) != ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'VideoDepth' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else if (irr::core::stringw(L"VSync").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.irrlichtParams.Vsync,
                                                     this->defaultSettings.irrlichtParams.Vsync) != ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'VSync' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else if (irr::core::stringw(L"Stencilbuffer").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.irrlichtParams.Stencilbuffer,
                                                     this->defaultSettings.irrlichtParams.Stencilbuffer) !=
                                ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'Stencilbuffer' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else if (irr::core::stringw(L"AntiAlias").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.irrlichtParams.AntiAlias,
                                                     this->defaultSettings.irrlichtParams.AntiAlias) !=
                                ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'AntiAlias' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else if (irr::core::stringw(L"Doublebuffer").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.irrlichtParams.Doublebuffer,
                                                     this->defaultSettings.irrlichtParams.Doublebuffer) !=
                                ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'Doublebuffer' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else if (irr::core::stringw(L"Stereobuffer").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.irrlichtParams.Stereobuffer,
                                                     this->defaultSettings.irrlichtParams.Stereobuffer) !=
                                ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'Stereobuffer' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else
                        {
                            Error::errContinue("unknown element with name",
                                               repr<const char*>(xml->getNodeName()),
                                               "in element 'VIDEO'");
                        }
                    }
                    break;
                    case configurationParts::AUDIO:
                    {
                        if (irr::core::stringw(L"SoundEnabled").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.soundEnabled,
                                                     this->defaultSettings.soundEnabled) != ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'SoundEnabled' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else if (irr::core::stringw(L"SoundVolume").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.soundVolume,
                                                     this->defaultSettings.soundVolume) != ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'SoundVolume' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else
                        {
                            Error::errContinue("unknown element with name",
                                               repr<const char*>(xml->getNodeName()),
                                               "in element 'AUDIO'");
                        }
                    }
                    break;
                    case configurationParts::SERVER:
                    {
                        if (irr::core::stringw(L"Name").equals_ignore_case(xml->getNodeName()))
                        {
                            this->currentSettings.serverName =
                                std::string(xml->getAttributeValueSafe("value"));
                        }
                        else if (irr::core::stringw(L"Port").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.serverPort,
                                                     this->defaultSettings.serverPort) != ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'Port' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else if (irr::core::stringw(L"Password").equals_ignore_case(xml->getNodeName()))
                        {
                            this->currentSettings.serverPassword =
                                std::string(xml->getAttributeValueSafe("value"));
                        }
                        else if (irr::core::stringw(L"NumberOfSlots").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.numberOfSlots,
                                                     this->defaultSettings.numberOfSlots) != ErrCodes::NO_ERR)
                            {
                                Error::errContinue("couldn't parse element 'NumberOfSlots' value:",
                                                   repr<const char*>(
                                                       xml->getAttributeValueSafe("value")));
                            }
                        }
                        else
                        {
                            Error::errContinue("unknown element with name",
                                               repr<const char*>(xml->getNodeName()),
                                               "in element 'SERVER'");
                        }
                    }
                    break;
                } // switch(configurationPart)
                break;
            case irr::io::EXN_ELEMENT_END:
                debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                              "end of element",
                              repr<const char*>(xml->getNodeName()));
                configurationPart = configurationParts::NONE;
                break;
            case irr::io::EXN_NONE:
            case irr::io::EXN_TEXT:
            case irr::io::EXN_COMMENT:
            case irr::io::EXN_CDATA:
            case irr::io::EXN_UNKNOWN:
            default:
                debugOutLevel(Debug::DebugLevels::maxLevel,
                              "unhandled xml entry type",
                              static_cast<int>(xml->getNodeType()));
                break;
        } // switch(xml->getNodeType)
    }     // while (xml->read())

    delete xml;

    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "config file loading succesfully finished");
    return ErrCodes::NO_ERR;
}


ErrCode Config::saveConfig(const std::string filename, irr::IrrlichtDevice* device) const
{
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "save config");
    // TODO: add test if directory creation failed or not
    PlatformIndependent::createDirectory(filename.c_str());

    irr::io::IXMLWriter* writer = device->getFileSystem()->createXMLWriter(filename.c_str());

    if (writer == nullptr)
    {
        Error::errContinue("Couldn't open config xml file", repr<std::string>(filename));
        return ErrCodes::GENERIC_ERR;
    }

    writer->writeXMLHeader();
    writer->writeComment(L"(encoding=\"UTF-32\")");
    writer->writeLineBreak();
    writer->writeElement(L"configuration");
    writer->writeLineBreak();
    writer->writeLineBreak();

    // start section with player settings
    writer->writeElement(L"PLAYER");
    writer->writeLineBreak();
    writer->writeElement(
        L"Name",
        true,
        L"value",
        irr::core::stringw(irr::core::stringc(this->currentSettings.playerName.c_str())).c_str());
    writer->writeLineBreak();
    writer->writeElement(L"Color",
                         true,
                         L"value",
                         StringWParser::getParsableString(this->currentSettings.playerColor).c_str());
    writer->writeLineBreak();
    writer->writeClosingTag(L"PLAYER");
    writer->writeLineBreak();
    writer->writeLineBreak();

    writer->writeElement(L"VIDEO");
    writer->writeLineBreak();
    writer->writeElement(
        L"Driver",
        true,
        L"value",
        StringWParser::getParsableString(this->currentSettings.irrlichtParams.DriverType).c_str());
    writer->writeLineBreak();
    writer->writeElement(
        L"Resolution",
        true,
        L"value",
        StringWParser::getParsableString(this->currentSettings.irrlichtParams.WindowSize).c_str());
    writer->writeLineBreak();
    writer->writeElement(
        L"Fullscreen",
        true,
        L"value",
        StringWParser::getParsableString(this->currentSettings.irrlichtParams.Fullscreen).c_str());
    writer->writeLineBreak();
    writer->writeElement(L"VideoDepth",
                         true,
                         L"value",
                         irr::core::stringw(this->currentSettings.irrlichtParams.Bits).c_str());
    writer->writeLineBreak();
    writer->writeElement(
        L"VSync",
        true,
        L"value",
        StringWParser::getParsableString(this->currentSettings.irrlichtParams.Vsync).c_str());
    writer->writeLineBreak();
    writer->writeElement(
        L"Stencilbuffer",
        true,
        L"value",
        StringWParser::getParsableString(this->currentSettings.irrlichtParams.Stencilbuffer).c_str());
    writer->writeLineBreak();
    writer->writeElement(L"AntiAlias",
                         true,
                         L"value",
                         irr::core::stringw(this->currentSettings.irrlichtParams.AntiAlias).c_str());
    writer->writeLineBreak();
    writer->writeElement(
        L"Doublebuffer",
        true,
        L"value",
        StringWParser::getParsableString(this->currentSettings.irrlichtParams.Doublebuffer).c_str());
    writer->writeLineBreak();
    writer->writeElement(
        L"Stereobuffer",
        true,
        L"value",
        StringWParser::getParsableString(this->currentSettings.irrlichtParams.Stereobuffer).c_str());
    writer->writeLineBreak();
    writer->writeClosingTag(L"VIDEO");
    writer->writeLineBreak();
    writer->writeLineBreak();

    writer->writeElement(L"AUDIO");
    writer->writeLineBreak();
    writer->writeElement(L"SoundEnabled",
                         true,
                         L"value",
                         StringWParser::getParsableString(this->currentSettings.soundEnabled).c_str());
    writer->writeLineBreak();
    writer->writeElement(L"SoundVolume",
                         true,
                         L"value",
                         irr::core::stringw(this->currentSettings.soundVolume).c_str());
    writer->writeLineBreak();
    writer->writeClosingTag(L"AUDIO");
    writer->writeLineBreak();
    writer->writeLineBreak();

    // start section with player settings
    writer->writeElement(L"SERVER");
    writer->writeLineBreak();
    writer->writeElement(
        L"Name",
        true,
        L"value",
        irr::core::stringw(irr::core::stringc(this->currentSettings.serverName.c_str())).c_str());
    writer->writeLineBreak();
    writer->writeElement(L"Port",
                         true,
                         L"value",
                         irr::core::stringw(this->currentSettings.serverPort).c_str());
    writer->writeLineBreak();
    writer->writeElement(
        L"Password",
        true,
        L"value",
        irr::core::stringw(irr::core::stringc(this->currentSettings.serverPassword.c_str())).c_str());
    writer->writeLineBreak();
    writer->writeElement(L"NumberOfSlots",
                         true,
                         L"value",
                         irr::core::stringw(this->currentSettings.numberOfSlots).c_str());
    writer->writeLineBreak();
    writer->writeClosingTag(L"SERVER");
    writer->writeLineBreak();
    writer->writeLineBreak();

    writer->writeClosingTag(L"configuration");
    writer->drop();

    return ErrCodes::NO_ERR;
}


ErrCode Config::loadServerBackup(const std::string filename)
{
    debugOutLevel(Debug::DebugLevels::stateInit, "loading Server-Backup file", filename);
    irr::io::IrrXMLReader* xml = irr::io::createIrrXMLReader(filename.c_str());

    if (!xml)
    {
        Error::errContinue("failed to open Server-Backup file", repr<std::string>(filename));
        return ErrCodes::GENERIC_ERR;
    }

    enum configurationParts
    {
        SERVERBACKUP,
        NONE
    };
    configurationParts configurationPart = configurationParts::NONE;

    while (xml->read())
    {
        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                      "read node name:",
                      repr<const char*>(xml->getNodeName()),
                      "data=",
                      repr<const char*>(xml->getNodeData()));
        switch (xml->getNodeType())
        {
            case irr::io::EXN_ELEMENT:
                switch (configurationPart)
                {
                    case configurationParts::NONE:
                    {
                        if (irr::core::stringw(L"SERVERBACKUP").equals_ignore_case(xml->getNodeName()))
                        {
                            configurationPart = configurationParts::SERVERBACKUP;
                        }
                        else if (irr::core::stringw(L"configuration").equals_ignore_case(xml->getNodeName()))
                        {
                            // ignore because that is the main element of the file
                        }
                        else
                        {
                            Error::errContinue("unknown configuration part",
                                               repr<const char*>(xml->getNodeName()));
                            configurationPart = configurationParts::NONE;
                        }
                    }
                    break;
                    case configurationParts::SERVERBACKUP:
                    {
                        if (irr::core::stringw(L"connectedServerName").equals_ignore_case(xml->getNodeName()))
                        {
                            this->currentSettings.connectedServerName =
                                std::string(xml->getAttributeValueSafe("value"));
                        }
                        else if (irr::core::stringw(L"connectedServerAddress").equals_ignore_case(xml->getNodeName()))
                        {
                            // RakNet will call DomainNameToIP on converting a string to an ip (as
                            // with the constructor from const char*)
                            // this leads to an dns lookup if the string isn't simply convertable to
                            // an ip which blocks until finished
                            // meaning SystemAddress(SystemAddress.ToString()) doesn't return the
                            // same adress for e.g. UNASSINGED_SYSTEM_ADDRESS
                            // even if connected to the internet this call can take ~2 seconds
                            // -> have a special case for UNASSIGNED_SYSTEM_ADDRESS
                            // TODO: maybe put any server-pinging into a different thread and only
                            // use it's results when they come in?
                            if (irr::core::stringw(xml->getAttributeValueSafe("value"))
                                    .equals_ignore_case(
                                        irr::core::stringw(L"UNASSIGNED_SYSTEM_ADDRESS")))
                            {
                                this->currentSettings.connectedServerAddress = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
                            }
                            else
                            {
                                this->currentSettings.connectedServerAddress =
                                    RakNet::SystemAddress(xml->getAttributeValueSafe("value"));
                            }
                        }
                        else if (irr::core::stringw(L"connectedServerPassword").equals_ignore_case(xml->getNodeName()))
                        {
                            this->currentSettings.connectedServerPassword =
                                std::string(xml->getAttributeValueSafe("value"));
                        }
                        else if (irr::core::stringw(L"myIdentificationKey").equals_ignore_case(xml->getNodeName()))
                        {
                            if (StringWParser::parse(xml->getAttributeValueSafe("value"),
                                                     this->currentSettings.myIdentificationKey,
                                                     this->defaultSettings.myIdentificationKey) != ErrCodes::NO_ERR)
                            {
                                Error::errContinue(
                                    "couldn't parse element 'myIdentificationKey' value:",
                                    repr<const char*>(xml->getAttributeValueSafe("value")));
                            }
                        }
                        else
                        {
                            Error::errContinue("unknown element with name",
                                               repr<const char*>(xml->getNodeName()),
                                               "in element 'NETWORK'");
                        }
                    }
                    break;
                } // switch(configurationPart)
                break;
            case irr::io::EXN_ELEMENT_END:
                debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                              "end of element",
                              repr<const char*>(xml->getNodeName()));
                configurationPart = configurationParts::NONE;
                break;
            case irr::io::EXN_NONE:
            case irr::io::EXN_TEXT:
            case irr::io::EXN_COMMENT:
            case irr::io::EXN_CDATA:
            case irr::io::EXN_UNKNOWN:
            default:
                debugOutLevel(Debug::DebugLevels::maxLevel,
                              "unhandled xml entry type",
                              static_cast<int>(xml->getNodeType()));
                break;
        } // switch(xml->getNodeType)
    }     // while (xml->read())

    delete xml;

    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "config file loading succesfully finished");
    return ErrCodes::NO_ERR;
}

ErrCode Config::saveServerBackup(const std::string filename, irr::IrrlichtDevice* device) const
{
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "save Server-Backup");
    // TODO: add test if directory creation failed or not
    PlatformIndependent::createDirectory(filename.c_str());

    irr::io::IXMLWriter* writer = device->getFileSystem()->createXMLWriter(filename.c_str());

    if (writer == nullptr)
    {
        Error::errContinue("Couldn't open Server-Backup xml file", repr<std::string>(filename));
        return ErrCodes::GENERIC_ERR;
    }

    writer->writeXMLHeader();
    writer->writeLineBreak();
    writer->writeElement(L"configuration");
    writer->writeLineBreak();
    writer->writeLineBreak();

    // start section with ServerBackup settings
    writer->writeElement(L"SERVERBACKUP");
    writer->writeLineBreak();
    writer->writeElement(
        L"connectedServerName",
        true,
        L"value",
        irr::core::stringw(irr::core::stringc(this->currentSettings.connectedServerName.c_str())).c_str());
    writer->writeLineBreak();
    writer->writeElement(
        L"connectedServerAddress",
        true,
        L"value",
        irr::core::stringw(this->currentSettings.connectedServerAddress.ToString(true)).c_str());
    writer->writeLineBreak();
    writer->writeElement(L"connectedServerPassword",
                         true,
                         L"value",
                         irr::core::stringw(
                             irr::core::stringc(this->currentSettings.connectedServerPassword.c_str()))
                             .c_str());
    writer->writeLineBreak();
    writer->writeElement(L"myIdentificationKey",
                         true,
                         L"value",
                         irr::core::stringw(this->currentSettings.myIdentificationKey).c_str());
    writer->writeLineBreak();
    writer->writeClosingTag(L"SERVERBACKUP");
    writer->writeLineBreak();
    writer->writeLineBreak();

    writer->writeClosingTag(L"configuration");
    writer->drop();

    return ErrCodes::NO_ERR;
}

void Config::resetServerBackup()
{
    this->currentSettings.connectedServerName = this->defaultSettings.connectedServerName;
    this->currentSettings.connectedServerAddress = this->defaultSettings.connectedServerAddress;
    this->currentSettings.connectedServerPassword = this->defaultSettings.connectedServerPassword;
    this->currentSettings.myIdentificationKey = this->defaultSettings.myIdentificationKey;
}

void Config::setSettings(const Settings settings)
{
    this->currentSettings = settings;
}
