/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rtscameraanimator.h"

#include <algorithm>
#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/ICursorControl.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/ISceneNodeAnimatorCollisionResponse.h>
#include <irrlicht/IVideoDriver.h>
#include <irrlicht/Keycodes.h>
#include "../utils/Pow2Assert.h"
#include "../utils/algos.h"

RTSCameraAnimator::RTSCameraAnimator(irr::gui::ICursorControl* cursorControl,
                                     irr::f32 moveSpeed,
                                     irr::f32 zoomingSpeed,
                                     irr::f32 lowerZoomLimit,
                                     irr::f32 upperZoomLimit,
                                     irr::core::vector2df xBounds,
                                     irr::core::vector2df yBounds,
                                     const std::unordered_map<BT::irrlichtHelpers::UniqueEventValue_t, RTS_CAM_ACTION>& keyboardControls_,
                                     irr::f32 borderScrollPercent_)
    : CursorControl(cursorControl)
    , wheelzoom(0)
    , zoomspeed(zoomingSpeed)
    , upperzoomlimit(upperZoomLimit)
    , lowerzoomlimit(lowerZoomLimit)
    , xBoundaries(xBounds)
    , yBoundaries(yBounds)
    , MoveSpeed(moveSpeed)
    , borderScrollPercent(borderScrollPercent_)
    , LastAnimationTime(0)
{
    POW2_ASSERT(xBounds == irr::core::vector2df(0.0f, 0.0f) || xBoundaries.X != xBoundaries.Y);
    POW2_ASSERT(yBounds == irr::core::vector2df(0.0f, 0.0f) || yBoundaries.X != yBoundaries.Y);
    // TRY if needed
    if (CursorControl != nullptr)
    {
        CursorControl->grab();
    }

    std::fill(this->mouseActions.begin(), this->mouseActions.end(), 0.0f);
    std::fill(this->keyboardActions.begin(), this->keyboardActions.end(), 0.0f);

    // use the overriden keyboard controls if supplied
    if (keyboardControls_.size() > 0)
    {
        this->keyboardControls = keyboardControls_;
    }
}

RTSCameraAnimator::~RTSCameraAnimator()
{
    if (CursorControl != nullptr)
    {
        CursorControl->drop();
    }
}

#pragma GCC diagnostic ignored "-Wswitch-enum"
bool RTSCameraAnimator::OnEvent(const irr::SEvent& evt)
{
    using namespace BT::irrlichtHelpers;

    const auto action = this->keyboardControls.find(toCombined(evt));
    if (action != this->keyboardControls.end())
    {
        switch (action->second)
        {
            case RTS_CAM_MOVE_FORWARD:
            case RTS_CAM_MOVE_BACKWARD:
            case RTS_CAM_MOVE_LEFT:
            case RTS_CAM_MOVE_RIGHT:
                // copying the value of 'PressedDown' means we move as long as the button is pressed
                this->keyboardActions[static_cast<size_t>(action->second)] =
                    evt.KeyInput.PressedDown ? 1.0f : 0.0f;
                break;
            case RTS_CAM_ZOOM_IN:
            case RTS_CAM_ZOOM_OUT:
                // todo
                ;
        }
        return true;
    }

    switch (toCombined(evt))
    {
        case toCombined(irr::EET_MOUSE_INPUT_EVENT, irr::EMIE_MOUSE_MOVED):
        {
            const auto CursorPos = CursorControl->getRelativePosition();
            // default to no movement, set if mouse at border
            // -> clear the actions which signal movement only, to still catch clicks, even if the
            // mouse moves afterwards
            constexpr size_t numDirections = 4;
            std::fill(this->mouseActions.begin(), this->mouseActions.begin() + numDirections, 0.0f);

            if (CursorPos.Y < this->borderScrollPercent)
            {
                this->mouseActions[RTS_CAM_MOVE_FORWARD] = 1.0f;
            }
            else if (CursorPos.Y > 1.0f - this->borderScrollPercent)
            {
                this->mouseActions[RTS_CAM_MOVE_BACKWARD] = 1.0f;
            }

            if (CursorPos.X < this->borderScrollPercent)
            {
                this->mouseActions[RTS_CAM_MOVE_LEFT] = 1.0f;
            }
            else if (CursorPos.X > 1.0 - this->borderScrollPercent)
            {
                this->mouseActions[RTS_CAM_MOVE_RIGHT] = 1.0f;
            }
            return true;
        }
        break;
        case toCombined(irr::EET_MOUSE_INPUT_EVENT, irr::EMIE_MMOUSE_LEFT_UP):
            if (this->CursorControl)
            {
                this->mouseActions[static_cast<size_t>(RTS_CAM_ACTION::RTS_CAM_ROTATE_CW)] = 1.0f;
                return true;
            }
            break;
        case toCombined(irr::EET_MOUSE_INPUT_EVENT, irr::EMIE_MOUSE_WHEEL):
            if (this->CursorControl)
            {
                if (evt.MouseInput.Wheel > 0)
                {
                    this->mouseActions[static_cast<std::size_t>(RTS_CAM_ACTION::RTS_CAM_ZOOM_IN)] =
                        evt.MouseInput.Wheel;
                }
                else
                {
                    this->mouseActions[static_cast<std::size_t>(RTS_CAM_ACTION::RTS_CAM_ZOOM_OUT)] =
                        evt.MouseInput.Wheel;
                }
                return true;
            }
            break;
    } // switch (toCombined(evt))

    return false;
}
#pragma GCC diagnostic pop

void RTSCameraAnimator::animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs)
{
    if (!node || node->getType() != irr::scene::ESNT_CAMERA)
    {
        return;
    }

    irr::scene::ICameraSceneNode* camera = static_cast<irr::scene::ICameraSceneNode*>(node);

    // If the camera isn't the active camera, and receiving input, then don't process it.
    if (!camera->isInputReceiverEnabled())
    {
        return;
    }

    if (this->firstUpdate)
    {
        camera->updateAbsolutePosition();

        LastAnimationTime = timeMs;
        originalCameraDirection = (camera->getPosition() - camera->getTarget());

        this->firstUpdate = false;
    }

    irr::scene::ISceneManager* smgr = camera->getSceneManager();
    if (smgr && smgr->getActiveCamera() != camera)
    {
        return;
    }

    // get time
    irr::f32 timeDiff = irr::f32(timeMs - LastAnimationTime);
    LastAnimationTime = timeMs;

    irr::core::vector3df pos = camera->getPosition();

    irr::core::vector3df target = camera->getTarget();

    irr::core::matrix4 mXZAxis;
    irr::core::vector3df offset(0, 0, 0);

    constexpr std::size_t numDirections = 4;
    irr::core::vector3df combined;
    // Combining mouse and keyboard in this way has one important result:
    // The user can move the mouse in one direction and combine scrolling over the map with the
    // keyboard and can use the keyboard to stop the movement with the mouse, independent
    // for each direction.
    for (std::size_t i = 0; i < numDirections; ++i)
    {
        // apply each movement direction the amount specified inside the action
        combined += this->movementDirections[i] * this->keyboardActions[i];
        combined += this->movementDirections[i] * this->mouseActions[i];
    }

    const irr::core::vector3df camAngle = (pos - target).getHorizontalAngle();
    float xzAngle = camAngle.Y;

    const bool shouldRotate =
        this->mouseActions[static_cast<size_t>(RTS_CAM_ACTION::RTS_CAM_ROTATE_CW)] != 0.0f ||
        this->keyboardActions[static_cast<size_t>(RTS_CAM_ACTION::RTS_CAM_ROTATE_CW)] != 0.0f;
    if (shouldRotate)
    {
        xzAngle += 90;
        if (xzAngle > 360.f)
        {
            xzAngle -= 360.f;
        }
        mXZAxis.setRotationDegrees(irr::core::vector3df(0, xzAngle, 0));
        irr::core::vector3df newDir(originalCameraDirection.normalize() * (pos - target).getLength());
        mXZAxis.rotateVect(newDir);

        pos = target + newDir;
    }

    mXZAxis.setRotationDegrees(irr::core::vector3df(0, xzAngle, 0));

    offset += MoveSpeed * timeDiff * combined;

    // move offset from screen coordinates to actual camera view coordinates
    mXZAxis.rotateVect(offset);
    pos += offset;
    target += offset;

    const float zoomDirection = this->mouseActions[static_cast<size_t>(RTS_CAM_ACTION::RTS_CAM_ZOOM_IN)] +
        this->mouseActions[static_cast<size_t>(RTS_CAM_ACTION::RTS_CAM_ZOOM_OUT)] +
        this->keyboardActions[static_cast<size_t>(RTS_CAM_ACTION::RTS_CAM_ZOOM_IN)] +
        this->keyboardActions[static_cast<size_t>(RTS_CAM_ACTION::RTS_CAM_ZOOM_OUT)];

    if (zoomDirection != 0.0f)
    {
        // We want to zoom along the direction we are currently looking but only until we hit the lower/upper Y limit.

        // `target - pos` because for positive 'zoomDirection' (zooming in) using
        // `pos = zoomFactor * (target - pos)` moves towards `target` (think setting zoomFactor = 1).
        const auto zoom = timeDiff * zoomspeed * zoomDirection * (target - pos).normalize();

        // how far can we move until hitting the limit?
        const auto clampedZoomY = std::clamp(zoom.Y, lowerzoomlimit - pos.Y, upperzoomlimit - pos.Y);

        // Scale so we are not overshooting the limit (scale == 1 if full movement possible
        // and < 1 if zoom was clamped)
        // Note: in case we are outside of the limits and trying to zoom even more outside
        // (zooming back into the limits is obviously okay) `zoomFactor` will get the opposite sign of `zoom`.
        // This turns zooming outside the limit more into zooming back towards the limit.
        // Consider e.g. (looking only at Y coordinate):
        // zoom.Y = -10, limit = (-5, 100), pos.Y = -7
        // clampedZoomY = clamp(-10, -5 - (-7), 100) = clamp(-10, 12, 100) = 2
        // zoomFactor = 2 / -10 = -0.2
        // zoomFactor * zoom = -0.2 * -10 = 2
        // -> pos += 2, moving upwards towards the lower limit
        //
        // But in the rare case of being a lot outside the valid position (or a wrongly-initialized camera)
        // `lowerzoomlimit - pos.Y` could be bigger than `upperzoomlimit - pos.Y` (undefined std::clamp min > max case)
        // and `zoomFactor` could be < -1 (zooming into the opposite direction fast).
        // thus we clamp it to -1 in that case meaning we take a "normal" zoom step towards the zoomlimit.
        const auto zoomFactor = std::max(-1.f, clampedZoomY / zoom.Y);


        // Actually update our position
        pos += zoomFactor * zoom;

        // calculate far/near value
        // assuming map is in the xz plane
        const auto hypotenuse = (pos - target).getLength();
        const auto yCam = pos.Y;
        const auto angleV = acosf(yCam / hypotenuse);
        const auto camAspect = camera->getAspectRatio();
        const auto camFovV = camera->getFOV() / sqrtf(1.0f + (camAspect * camAspect));
        const float newFarValue = yCam / (cosf(camFovV + angleV));

        camera->setFarValue(newFarValue);
        camera->setNearValue(1.f);
    }

    // 'consume' consumable events
    // WARNING: this are only non-movement actions.
    // If all actions are reset movement is like typing letters: holding down does a single
    // movement, then stops, then starts moving again. Thus only non-movement actions are reset.
    // Instead movement is stopped by lifting up the key
    for (const size_t action :
         {RTS_CAM_ACTION::RTS_CAM_ROTATE_CW, RTS_CAM_ACTION::RTS_CAM_ZOOM_IN, RTS_CAM_ACTION::RTS_CAM_ZOOM_OUT})
    {
        this->mouseActions[action] = 0.0f;
        this->keyboardActions[action] = 0.0f;
    }

    clampToXZBounds(pos, target);

    camera->setPosition(pos);
    camera->setTarget(target);
}

//! Sets the movement speed
void RTSCameraAnimator::setMoveSpeed(const irr::f32 speed)
{
    MoveSpeed = speed;
}

//! Gets the movement speed
irr::f32 RTSCameraAnimator::getMoveSpeed() const
{
    return MoveSpeed;
}

//! Sets the keyboard mapping for this animator
void RTSCameraAnimator::setKeyMap(const KeyboardControlMapping& newKeyboardMapping)
{
    this->keyboardControls = newKeyboardMapping;
}

void RTSCameraAnimator::setBounds(const irr::core::vector2df& xBounds, const irr::core::vector2df yBounds)
{
    xBoundaries = xBounds;
    yBoundaries = yBounds;
}

const RTSCameraAnimator::KeyboardControlMapping& RTSCameraAnimator::getKeyMap() const
{
    return this->keyboardControls;
}

irr::scene::ISceneNodeAnimator* RTSCameraAnimator::createClone(irr::scene::ISceneNode*, irr::scene::ISceneManager*)
{
    RTSCameraAnimator* newAnimator = new RTSCameraAnimator(CursorControl, MoveSpeed, 0, 0);
    newAnimator->setKeyMap(this->keyboardControls);
    return newAnimator;
}

void RTSCameraAnimator::clampToXZBounds(irr::core::vector3df& pos, irr::core::vector3df& target)
{
    if (xBoundaries == irr::core::vector2df(0, 0) || yBoundaries == irr::core::vector2df(0, 0))
    {
        return; // DO NOTHING
    }
    // check X
    irr::core::vector3df connection = pos - target;
    target.X = clamp(target.X, xBoundaries.X, xBoundaries.Y);
    target.Z = clamp(target.Z, yBoundaries.X, yBoundaries.Y);

    pos = target + connection;
}
