/*Code based on the
game states / ODE / freeflight / XML io all - in - one demo from randomMesh:
http://irrlicht.sourceforge.net/forum/viewtopic.php?f=9&t=26526
released under the Irrlicht license.
Read the licenses/irrlicht.license.txt for more information
*/

#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include <array>
#include "../states/state.h"
#include "../utils/algos.h"
#include "../utils/debug.h"
#include "../utils/static/error.h"

class Game;
namespace irr
{
    struct SEvent;
}

/*! \class StateManager
 *
 * This template represents a state machine graph. It is used along with
 * the state template to allow code for independent game states to be
 * written and managed independently.
 */
class StateManager
{
   private:
    State* currentState;  //!< Current state of the state machine
    State* previousState; //!< Previous state of the state machine
    State* globalState;   //!< Global state
    bool currentStateInitialized;
    bool globalStateInitialized;

   public:
    enum class States_e : size_t
    {
        Menu = 0,
        ServerBrowser,
        Lobby,
        Game,
        Example,
        Mapeditor,
        testLoading,
        NumberOfStates
    };
    static constexpr std::array<const char* const, static_cast<size_t>(States_e::NumberOfStates)> stateNames = {
        "Menu", "ServerBrowser", "Lobby", "Game", "Example", "MapEditor", "load"};

    explicit StateManager()
        : currentState(nullptr)
        , previousState(nullptr)
        , globalState(nullptr)
        , currentStateInitialized(false)
        , globalStateInitialized(false)
    {
        // pointers don't get default-initialized to nullptr
        for (auto& state : this->states)
        {
            state = nullptr;
        }
    }

    StateManager(const StateManager&) = delete;

    virtual ~StateManager() {}
    /*! Set the current state.
     *
     * Usually only used for setting the initial state. Could be dangerous
     * if you are arbitrarily setting states without correctly entering or
     * exiting the states. Use with caution. Normally you would call the
     * changeState() method to properly transition through states.
     *
     * \param state The state to use as the initial current state.
     */
    void setInitialState(State* state)
    {
        debugOutLevel(Debug::DebugLevels::stateHandling, "set initial state");
        this->currentState = state;
        this->currentState->onEnter();
        this->currentStateInitialized = true;
    }

    /*! Retrieve the current state.
     *
     * \return Pointer to the current state. nullptr if there is none
     */
    State* getCurrentState() const { return this->currentState; }
    /*! Set the previous state.
     *
     * Usually only used for setting the initial state. Could be dangerous
     * if you are arbitrarily setting states without correctly entering or
     * exiting the states. Use with caution. Normally you would call the
     * changeState() method to properly transition through states.
     *
     * \param state The state to use as the new previous state.
     */
    void setPreviousState(State* state) { this->previousState = state; }
    /*! Retrieve the previous state.
     *
     * \return Pointer to the previous state.
     */
    State* getPreviousState() const { return this->previousState; }
    /*! Set the global state.
     *
     * The global state is updated before the current state
     * on every update tick.
     *
     * \param state The new global state
     */
    void setGlobalState(State* state)
    {
        this->globalState = state;
        if (this->globalState)
        {
            this->globalState->onEnter();
        }
        globalStateInitialized = true;
    }

    /*! Retrieve the global state
     *
     * \return Pointer to the global state. Returns nullptr if there is none
     */
    State* getGlobalState() { return this->globalState; }
    /*! Process an incoming message.
     *
     * Forwards the message to the current state. If the current state
     * does not process the message, it is forwarded to the global state.
     *
     * \param event The message to process.
     * \return Returns true if the message was processed, otherwise false.
     */
    bool onEvent(const irr::SEvent& event) const
    {
        // onEvent is called via game.isRunning() because that calls irr::device->run() which
        // processes events

        // global state gets to process first
        if (this->globalState && globalStateInitialized && this->globalState->onEvent(event))
        {
            //debugOutLevel(Debug::DebugLevels::onEvent, "event was handled by globalState"); //TODO check why "debugout" dosent work here
            return true;
        }
        // if event not handled by global state try the current state
        else if (this->currentState && currentStateInitialized && this->currentState->onEvent(event))
        {
            //debugOutLevel(Debug::DebugLevels::onEvent, "event was handled by currentState"); //TODO check why "debugout" dosent work here
            return true;
        }
        return false; // event not handled at all
    }

    /*! Update the state for this machine.
     *
     * Updates the global state and then the current state.
     */
    void update() const
    {
        if (this->globalState)
        {
            debugOutLevel(Debug::DebugLevels::updateLoop, "globalState->update()");
            this->globalState->update();
        }

        if (this->currentState)
        {
            debugOutLevel(Debug::DebugLevels::updateLoop, "currentState->update()");
            this->currentState->update();
        }
    }

    void addState(States_e state_e, State* state)
    {
        const size_t s = static_cast<size_t>(state_e);
        if (this->states[s])
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1,
                          "overwriting previous pointer for state",
                          this->stateNames[s],
                          static_cast<void*>(this->states[s]),
                          "with",
                          static_cast<void*>(state));
        }
        else
        {
            debugOutLevel(Debug::DebugLevels::stateHandling + 1,
                          "adding state",
                          this->stateNames[s],
                          "at",
                          static_cast<void*>(state));
        }
        this->states[s] = state;
    }

    int findState(const std::string& toFind) const
    {
        for (size_t i = 0; i < this->stateNames.size(); i++)
        {
            if (equalIgnoreCase(toFind, this->stateNames[i]))
            {
                return static_cast<int>(i);
            }
        }
        return -1;
    }

    /// @brief Transiton to a different state
    /// Exits the current state and enters the provided state. This is the
    /// preferred method for transitioning states. Please use this method
    // instead of setCurrentState.
    /// @param state_e which state to transition to. If no state is set for this state enum this
    /// function prints an error and returns
    void changeState(const States_e state_e)
    {
        const size_t s = static_cast<size_t>(state_e);
        if (s > this->states.size())
        {
            Error::errContinue("no state at index", s, "forgot to call 'addState()'?");
            return;
        }
        State* const targetState = this->states[s];
        if (not targetState)
        {
            Error::errContinue("couldn't find state", this->stateNames[s], "forgot to call 'addState()'?");
            return;
        }

        debugOutLevel(Debug::DebugLevels::stateHandling, "changing state");

        // Previous = Current;
        setPreviousState(this->currentState);

        if (this->currentState)
        {
            if (not this->currentStateInitialized)
            {
                // bad things happening. Something tried to change the state while the current state
                // wasn't fully initialized (maybe something from another thread?)
                // as this generally isn't recoverable (everyone expects state changes to work) the
                // only way out is to throw an error
                Error::errTerminate("current state",
                                    static_cast<void*>(this->currentState),
                                    "wasn't fully initialized but something requested a change to "
                                    "state",
                                    static_cast<void*>(targetState));
            }
            // don't handle events for that state while it is destructed
            this->currentStateInitialized = false;
            this->currentState->onLeave();
        }

        this->currentState = targetState;

        // don't handle events for that state while it is constructed
        this->currentStateInitialized = false;
        this->currentState->onEnter();
        // start handling events
        this->currentStateInitialized = true;
    }

    /// @brief return true if machine is in the given state
    bool isInState(const States_e state) const
    {
        return this->states[static_cast<size_t>(state)] == this->currentState;
    }

    //###########################################################
    // OPERATOR OVERLOADINGS
    //###########################################################

    StateManager operator=(const StateManager&) = delete;

   private:
    std::array<State*, static_cast<size_t>(States_e::NumberOfStates)> states = {};
};

#endif // STATEMANAGER_H
