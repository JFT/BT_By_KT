#ifndef ALLOCMEM_H_INCLUDED
#define ALLOCMEM_H_INCLUDED

#include <algorithm>
#include <cstdint>
#include <cstring>
#include <iostream>


#define TRAILING_ 0x55555555
#define HEADING_ 0x55555555

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"

template <class T>
class MemoryAllocator
{
   public:
    ~MemoryAllocator()
    {
        for (size_t k = 0; k < numallocatedBlocks; ++k)
        {
            ::operator delete(allocatedBlocks[k]);
        }
        ::operator delete[](allocatedBlocks);
    }

    MemoryAllocator()
        : // memoryallocator allocating one object at at time
        requestedSize(sizeof(T))
        , allocatedSize(std::max(sizeof(T), sizeof(Free)))
        , blockSize(1)
        , numallocatedBlocks(0)
        , allocatedBlocks(nullptr)
        , free(nullptr)
    //            debug (false)
    {
    }

    MemoryAllocator(size_t block_size)
        : // allocates block_size units at a time
        requestedSize(sizeof(T))
        , allocatedSize(std::max(sizeof(T), sizeof(Free)))
        , blockSize(block_size)
        , numallocatedBlocks(0)
        , allocatedBlocks(nullptr)
        , free(nullptr)
    //            debug (false)
    {
    }

    MemoryAllocator(size_t block_size, bool debug_)
        : // same as above with debugging functionality added
        requestedSize(sizeof(T))
        , allocatedSize(std::max(sizeof(T), sizeof(Free)))
        , blockSize(block_size)
        , numallocatedBlocks(0)
        , allocatedBlocks(nullptr)
        , free(nullptr)
    //            debug (false)
    {
        //            if (debug)
        //            { allocatedSize += 2 * sizeof(int); }
    }

    inline void* allocate() //
    {
        if (!free)
        {
            more();
        }
        void* storage = free;
        free = free->next;
        //            return debug ? debugCorrect(storage) : storage;
        return storage;
    }

    inline void deallocate(void* storage)
    {
        //            if (debug)
        //            { debugCheck(storage); }
        Free* newfree = static_cast<Free*>(storage);
        newfree->next = free;
        free = newfree;
    }

    //        bool  isdebug () const
    //        {
    //            return debug;
    //        }


   private:
    struct Free
    {
        Free* next;
    };
    size_t requestedSize;      // memory units requested for allocation
    size_t allocatedSize;      // memory units to be allocated
    size_t blockSize;          // The number of units in blocks
    size_t numallocatedBlocks; // Number of allocated blocks
    void** allocatedBlocks;    // Array of void* pointers to blocks
    Free* free;                // Linked list of free units
    //        bool debug;

    void more() throw(char*)
    {
        Free* newBlock = new Free[allocatedSize * blockSize];
        void** newBlocks = new void*[sizeof(void*) * (numallocatedBlocks + 1)];
        int last_element = static_cast<int>(blockSize) - 1;
        if (!newBlock || !newBlocks)
        {
            throw("Memory allocation failed.");
        }

        if (allocatedBlocks)
        {
            std::memcpy(newBlocks, allocatedBlocks, sizeof(void*) * numallocatedBlocks);
            ::operator delete[](allocatedBlocks);
        }

        allocatedBlocks = newBlocks;
        allocatedBlocks[numallocatedBlocks++] = newBlock;
        free = newBlock;

        for (int k = 0; k < last_element; ++k, newBlock = newBlock->next)
        {
            newBlock->next = newBlock + allocatedSize;
        }

        newBlock->next = nullptr;
    }

    //        void* debugCheck(void*& storage) throw(char*)
    //        {
    //            int* tail = reinterpret_cast<int*>(reinterpret_cast<char*>(storage) +
    //            requestedSize);
    //            int* head = static_cast<int*>(storage = static_cast<int*>(storage) - 1);
    //
    //            if (*tail != TRAILING_)
    //            { throw ("Block tail has been overrun."); }
    //            if (*head !=  HEADING_)
    //            { throw ("Block header has been overrun."); }
    //            return storage;
    //        }
    //
    //        void* debugCorrect(void*& storage)
    //        {
    //            storage = reinterpret_cast<void*>(HEADING_);
    //            storage = static_cast<int*>(storage + 1);
    //            ((char*) storage + requestedSize) = reinterpret_cast<void*>(TRAILING_);
    //            return storage;
    //        }
};


// End of File

#pragma GCC diagnostic pop
#endif // ALLOCMEM_H_INCLUDED
