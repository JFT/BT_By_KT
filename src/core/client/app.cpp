/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "app.h"

#include <irrlicht/irrlicht.h> // createDevice()/createDeviceEx()

#include "../../audio/soundEngine.h"
#include "../../core/game.h"
#include "../../states/client/gamestate.h"
#include "../../states/client/globalstate.h"
#include "../../states/client/lobbystate.h"
#include "../../states/client/menustate.h"
#include "../../states/client/serverbrowserstate.h"
#include "../../states/examplestate.h"
#include "../../states/mapeditorstate.h"
#include "../../states/testLoadingState.h"


App::App()
    : config()
    , device(nullptr)
    , soundEngine(nullptr)
{
    static_assert(IRRLICHT_VERSION_MAJOR == 1 and IRRLICHT_VERSION_MINOR == 9 and IRRLICHT_VERSION_REVISION >= 3,
                  "Irrlicht_extended needs to be updated!");
}

App::~App()
{
    delete soundEngine;
}

ErrCode App::init()
{
    if (config.loadConfigJSON("config/config.json") != ErrCodes::NO_ERR)
    {
        // config will use default
        Error::errContinue("Cant load config/config.json. Using default values.");
    }

    device = irr::createDeviceEx(config.getSettings().irrlichtParams);

    if (device == nullptr)
    {
        Error::errTerminate("Can't create Irrlicht Device.");
        return ErrCodes::GENERIC_ERR; // TODO: tell me what the hell is going on!
    }

    // Split the incoming irrlicht string
    auto split = [](const char* str, char c = ' ') {
        std::vector<int> result;
        do
        {
            const char* begin = str;

            while (*str != c && *str) str++;

            result.push_back(std::stoi(std::string(begin, str)));
        } while (0 != *str++);

        return result;
    };


    auto irrlichtVersion = split(device->getVersion(), '.');
    if (!(irrlichtVersion[0] >= 1 && irrlichtVersion[1] >= 9 && irrlichtVersion[2] >= 3))
    {
        Error::errTerminate("Minimal needed Irrlicht_extended version is 1.9.3, Irrlicht reports "
                            "version",
                            device->getVersion());
    }

    // Initialize Soundengine
    this->soundEngine = new BT::audio::SoundEngine();

    if (!soundEngine)
    {
        this->config.getModifiableSettings().soundEnabled = false;
        Error::errContinue("Can't create Audio Device.");
        return ErrCodes::GENERIC_ERR;
    }
    // else
    //{
    //    float soundVolume = 0.0;
    //    if (this->config.getSettings().soundEnabled)
    //    {
    //        soundVolume = float(this->config.getSettings().soundVolume);
    //    }
    //    soundEngine->setGlobalVolume(soundVolume);
    //}

    return ErrCodes::NO_ERR;
}

void App::run(const bool stdinToConsole)
{
    Game game(this->config, this->device, this->soundEngine, stdinToConsole);

    // Global Gamestate
    GlobalState global(game);
    MenuState menu(game);
    ServerBrowserState serverbrowser(game);
    LobbyState lobby(game);
    GameState gameState(game);
    MapEditorState mapeditor(game);
    ExampleState example(game);
    testLoadingState load(game);

    this->device->drop();

    game.addState(Game::States_e::Menu, &menu);
    game.addState(Game::States_e::ServerBrowser, &serverbrowser);
    game.addState(Game::States_e::Lobby, &lobby);
    game.addState(Game::States_e::Mapeditor, &mapeditor);
    game.addState(Game::States_e::Game, &gameState);
    game.addState(Game::States_e::Example, &example);
    game.addState(Game::States_e::testLoading, &load);


    // Globalstate
    game.setGlobalState(&global);

    // Start with ...state
    game.setInitialState(&menu);

    // Time handling for fixed update tickrate
    // rendering is now a lot more independent from logic/movement etc. updating
    double previous = game.getRuntimeMS();
    double timeSinceLastTick = 0.0f;
    double lastFrame = 0.0f;
    double expectNextFrame = lastFrame + 1000.0 / game.getFrameRate();
    bool loopDidNothing = true;

    while (game.isRunning()) // call to game.isRunning() calls device->run() which does event handling
    {
        const double current = game.getRuntimeMS();
        const double elapsed = current - previous;
        const double targetTickduration = 1000.0 / game.getTickrate();

        timeSinceLastTick += elapsed;

        while (timeSinceLastTick >= targetTickduration)
        {
            game.tri2Fill.clear();
            game.tri2Draw.clear();
            game.line2Draw.clear();
            game.box2Draw.clear();
            game.tri2Fill_2d.clear();
            game.tri2Draw_2d.clear();
            game.line2Draw_2d.clear();
            game.box2Draw_2d.clear();
            game.update();
            loopDidNothing = false;
            timeSinceLastTick -= targetTickduration;

            // make sure we render with at least ~ 1fps
            // useful for debug purposes - hopefully will never
            // happen in release state of game
            if ((game.getRuntimeMS() - lastFrame) > 1000.0) break;
        }

        if (current >= expectNextFrame)
        {
            game.setFrameTime(current - lastFrame);
            game.render();
            loopDidNothing = false;

            // the navie way to time the next frame would be to set
            // expectNextFrame = current + 1000.0f / game.getFrameRate();
            // but this will only yield the desired framerate if
            // 'current' happens to be exactly == 'expectNextFrame' all the time
            // otherwise the next frame will come slightly later than it should have come
            // (it will come in the correct timing from the perspective of the current frame,
            // but not from the perspective of the last (and earlyer) frames)
            // These small timing differences can add up and can lead to an actual framerate that is
            // lower than the one desired by 'game.getFrameRate()'.
            // Solution: correct for this difference by doing the next frame a little earlier if
            // neccecray
            // This however introduces another failure possibility:
            // if differenceToExpectedTime becomes too large (meaning the current frame is very
            // late)
            // it might become larger than 1000 / game.getFrameRate() which will lead to the
            // paradoxical situation
            // that 'expectNextFrame' is _before_ current. Then differenceToExpectedTime will be
            // even larger and expectNextFrame will move towards the past.
            // Solution: correct by limiting it to half a frametime
            const double differenceToExpectedTime =
                std::min(current - expectNextFrame, 1000.0 / game.getFrameRate() / 2.0);

            expectNextFrame = current + 1000.0 / game.getFrameRate() - differenceToExpectedTime;
            lastFrame = current;
        }
        if (loopDidNothing)
        {
            game.sleep(1);
        }
        loopDidNothing = true;
        previous = current;
    }
}
