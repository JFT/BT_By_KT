/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "grid.h"

#include <cmath> // std::floor
#include "../utils/debug.h"

using namespace grid;
using namespace std;

Grid::Grid(const irr::core::vector2df mapDimensions, const irr::f32 sideLength_)
    : dimension(mapDimensions)
    , xNumberOfCells(static_cast<irr::u16>(std::ceil(mapDimensions.X / sideLength_)))
    , // std::ceil to make sure all positions on the map lead to a valid gridCell even if the
      // mapSize isn't an exact multiple of sideLength
    zNumberOfCells(static_cast<irr::u16>(std::ceil(mapDimensions.Y / sideLength_)))
    , sideLength(sideLength_)
    , maxCellID(xNumberOfCells * zNumberOfCells - 1)
{
    // ctor
}

Grid::~Grid()
{
    // dtor
}

void Grid::print()
{
    debugOutLevel(50, "Grid:");
    debugOutLevel(50, "xNumberOfCells: ", xNumberOfCells, "\tyNumberOfCells: ", zNumberOfCells);
}
