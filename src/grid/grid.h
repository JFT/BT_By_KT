/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GRID_H
#define GRID_H

#include <array>
#include <utility> // std::pair

#include <irrlicht/irrMath.h> // clamp()
#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>

#include "gridTypes.h"
#include "../utils/optimization.h" // MACRO_(UN)LIKELY


namespace grid
{
    class Grid
    {
       public:
        /// @brief absolute dimension of the whole map
        const irr::core::vector2df dimension;
        /// @brief number of cells in x direction
        const irr::u16 xNumberOfCells;
        const irr::u16 zNumberOfCells;
        const irr::f32 sideLength;
        const gridCellID_t maxCellID;

        Grid(const irr::core::vector2df mapDimensions, const irr::f32 sideLength);
        virtual ~Grid();

        Grid(const Grid&) = delete;
        Grid operator=(const Grid&) = delete;

        void print();

        inline gridCellID_t getXIndex(const irr::f32& f) const
        {
            return static_cast<gridCellID_t>(
                irr::core::clamp(static_cast<int>(f / sideLength), 0, this->xNumberOfCells - 1));
        }
        inline gridCellID_t getXIndex(const irr::core::vector2df& v) const
        {
            return static_cast<gridCellID_t>(
                irr::core::clamp(static_cast<int>(v.X / sideLength), 0, this->xNumberOfCells - 1));
        }
        inline gridCellID_t getXIndex(const irr::core::vector3df& v) const
        {
            return static_cast<gridCellID_t>(
                irr::core::clamp(static_cast<int>(v.X / sideLength), 0, this->xNumberOfCells - 1));
        }

        inline gridCellID_t getZIndex(const irr::f32& f) const
        {
            return static_cast<gridCellID_t>(
                irr::core::clamp(static_cast<int>(f / sideLength), 0, this->zNumberOfCells - 1));
        }
        inline gridCellID_t getZIndex(const irr::core::vector2df& v) const
        {
            return static_cast<gridCellID_t>(
                irr::core::clamp(static_cast<int>(v.Y / sideLength), 0, this->zNumberOfCells - 1));
        }
        inline gridCellID_t getZIndex(const irr::core::vector3df& v) const
        {
            return static_cast<gridCellID_t>(
                irr::core::clamp(static_cast<int>(v.Z / sideLength), 0, this->zNumberOfCells - 1));
        }

        inline gridCellID_t getCellID(const gridCellID_t cellX, const gridCellID_t cellZ) const
        {
            return static_cast<gridCellID_t>(this->xNumberOfCells * cellZ + cellX);
        }

        inline gridCellID_t getCellID(const irr::core::vector2df& v) const
        {
            return static_cast<gridCellID_t>(this->xNumberOfCells * getZIndex(v) + getXIndex(v));
        }
        inline gridCellID_t getCellID(const irr::core::vector3df& v) const
        {
            return static_cast<gridCellID_t>(this->xNumberOfCells * getZIndex(v) + getXIndex(v));
        }

        // inverse function to 'getCellID'
        inline std::pair<gridCellID_t, gridCellID_t> getXZCoordinate(const gridCellID_t cellID) const
        {
            // invert ID = xNum * z + x
            const gridCellID_t x = static_cast<gridCellID_t>(cellID % this->xNumberOfCells);
            const gridCellID_t z = static_cast<gridCellID_t>((cellID - x) / this->xNumberOfCells);
            return std::make_pair(x, z);
        }

        inline std::array<grid::gridCellID_t, 9> get3x3Around(const grid::gridCellID_t gridCellID) const
        {
            auto coordinates = this->getXZCoordinate(gridCellID);

            std::array<grid::gridCellID_t, 9> retVal;

            int idx = 0;
            for (int xOffset = -1; xOffset <= 1; xOffset++)
            {
                for (int zOffset = -1; zOffset <= 1; zOffset++)
                {
                    if (MACRO_UNLIKELY(static_cast<int>(coordinates.first) + xOffset < 0 ||
                                       static_cast<int>(coordinates.first) + xOffset + 1 > this->xNumberOfCells ||
                                       static_cast<int>(coordinates.second) + zOffset < 0 ||
                                       static_cast<int>(coordinates.second) + zOffset + 1 > this->zNumberOfCells))
                    {
                        retVal[static_cast<size_t>(idx)] = InvalidCellID;
                    }
                    else
                    {
                        retVal[static_cast<size_t>(idx)] =
                            this->getCellID(static_cast<gridCellID_t>(coordinates.first + xOffset),
                                            static_cast<gridCellID_t>(coordinates.second + zOffset));
                    }
                    idx++;
                }
            }
            return retVal;
        }

        inline std::array<grid::gridCellID_t, 8> getNeighbors(const grid::gridCellID_t gridCellID) const
        {
            auto coordinates = this->getXZCoordinate(gridCellID);

            std::array<grid::gridCellID_t, 8> neighbors;

            int idx = 0;
            for (int xOffset = -1; xOffset <= 1; xOffset++)
            {
                for (int zOffset = -1; zOffset <= 1; zOffset++)
                {
                    if (xOffset == 0 && zOffset == 0)
                    {
                        continue;
                    }
                    if (MACRO_UNLIKELY(static_cast<int>(coordinates.first) + xOffset < 0 ||
                                       static_cast<int>(coordinates.first) + xOffset + 1 > this->xNumberOfCells ||
                                       static_cast<int>(coordinates.second) + zOffset < 0 ||
                                       static_cast<int>(coordinates.second) + zOffset + 1 > this->zNumberOfCells))
                    {
                        neighbors[static_cast<size_t>(idx)] = InvalidCellID;
                    }
                    else
                    {
                        neighbors[static_cast<size_t>(idx)] =
                            this->getCellID(static_cast<gridCellID_t>(coordinates.first + xOffset),
                                            static_cast<gridCellID_t>(coordinates.second + zOffset));
                    }
                    idx++;
                }
            }
            return neighbors;
        }
    };
} // namespace grid
#endif // GRID_H
