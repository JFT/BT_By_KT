#pragma once

#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>

#include <iostream>

namespace irr
{
    namespace core
    {
        // ostream overloads must be in namespace the class is defined because of ADL rules
        template <typename T>
        std::ostream& operator<<(std::ostream& out, const irr::core::vector2d<T>& vec)
        {
            out << vec.X << "," << vec.Y;
            return out;
        }

        template <typename T>
        std::ostream& operator<<(std::ostream& out, const irr::core::vector3d<T>& vec)
        {
            out << vec.X << "," << vec.Y << "," << vec.Z;
            return out;
        }
    } // namespace core
} // namespace irr
