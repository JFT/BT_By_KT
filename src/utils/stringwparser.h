/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRINGWPARSER_H
#define STRINGWPARSER_H

#include <algorithm> // std::replace
#include <clocale>   // needed because sometimes the parsing of numbers depends on the locale
#include <exception> // c++ parsing
#include <stdexcept> // std::invalid_argument

#include "stringwstream.h" // stringwstream.h needs to be included before debug.h and error.h because they use use operator<< on a stringw
#include "static/error.h"
#include "static/repr.h"

// TODO: make parsing thread save
class StringWParser
{
   public:
    template <typename T>
    static ErrCode parse(const irr::core::stringw str_,
                         T& tRef,
                         const T defaultValue,
                         const wchar_t seperator = L' ',
                         const bool printOnError = true)
    {
        // internal parsing is done with c++ std::wstring to use c++ parsing functions
        std::wstring str(str_.c_str());
        std::replace(str.begin(), str.end(), seperator, L' ');
        char* locale = setlocale(LC_NUMERIC, nullptr);
        setlocale(LC_NUMERIC, "C");

        ErrCode retVal(0);
        try
        {
            size_t readTo = 0;
            tRef = parseInternal<T>(str, readTo);
            if (readTo != str.size())
            {
                throw std::invalid_argument(std::string("some elements left in string after "
                                                        "parsing, beginning from position ") +
                                            std::to_string(readTo));
            }
        }
        catch (std::exception& ex)
        {
            if (printOnError)
            {
                Error::errContinue("couldn't parse string",
                                   repr<irr::core::stringw>(str_),
                                   "message:",
                                   ex.what(),
                                   "rest after parsing:",
                                   repr<std::wstring>(str));
            }
            retVal = ErrCode(1);
            tRef = defaultValue;
        }

        setlocale(LC_NUMERIC, locale);
        return retVal;
    }

    template <typename T>
    static irr::core::stringw getParsableString(const T& toSave);

   private:
    template <typename T>
    static T parseInternal(std::wstring str, size_t& readTo);
};


#endif // STRINGWPARSER_H
