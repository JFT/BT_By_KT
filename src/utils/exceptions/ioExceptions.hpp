#pragma once

#include <stdexcept>

#define IOEXCEPTION(msg) BT::exceptions::IOException(msg, __FILE__, __LINE__, __func__)
namespace BT
{
    namespace exceptions
    {
        class IOException : public std::runtime_error
        {
            const char* file;
            int line;
            const char* func;
            const char* info;

           public:
            IOException(const char* msg, const char* file_ = "", int line_ = 0, const char* func_ = "", const char* info_ = "")
                : std::runtime_error(msg)
                , file(file_)
                , line(line_)
                , func(func_)
                , info(info_)
            {
            }

            const char* what() const noexcept
            {
                std::string what_str = std::string(file) + std::string(": ");
                what_str += std::string(", ") + std::to_string(line) + std::string(" - ");
                what_str += std::string(func) + std::string(": ") + std::string(std::runtime_error::what());

                return what_str.c_str();
            }

            const char* get_file() const { return file; }
            int get_line() const { return line; }
            const char* get_func() const { return func; }
            const char* get_info() const { return info; }
        };
    } // namespace exceptions
} // namespace BT