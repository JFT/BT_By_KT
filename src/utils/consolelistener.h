/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONSOLELISTENER_H
#define CONSOLELISTENER_H

// starts a new thread which listens on stdin and writes the input into a queue
// should be thread save

#include <mutex>
#include <queue>
#include <string> // std::getline
#include <thread>
// forwad declarations


class ConsoleListener
{
   public:
    explicit ConsoleListener(const bool shouldListen);
    ConsoleListener(const ConsoleListener&) = delete;
    ~ConsoleListener();

    // startListening has no effect if already listening
    void startListening();

    bool queueEmpty();
    std::string popInput();

    ConsoleListener operator=(const ConsoleListener&) = delete;

   private:
    std::queue<std::string> inputQueue;
    std::mutex queueAccess;
    std::thread* listenerThread;

    void readInput();
    void enqueueInput(const std::string input);
};

#endif // CONSOLELISTENER_H
