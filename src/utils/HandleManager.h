/*
 * Copyright (c) 2008, Power of Two Games LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Power of Two Games LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY POWER OF TWO GAMES LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL POWER OF TWO GAMES LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 **Edited by: Julius Tilly, Hannes Franke
 */


#ifndef pow2_datastructures_HandleManager_h
#define pow2_datastructures_HandleManager_h

#include "Handle.h"
#include "Pow2Assert.h"

#include "debug.h"
#if defined(MAKE_DEBUG_)
#include <typeinfo>
#endif

namespace Handles
{
    class HandleManager
    {
       public:
        enum
        {
            maxEntries = 1 << 16
        }; // 2^16 = 65536

        HandleManager();

        void reset();
        Handle add(void* p);
        void update(Handle handle, void* p);
        void remove(Handle handle);

        bool isValid(const Handle handle) const;

        inline void* get(Handle handle) const;
        inline bool get(Handle handle, void*& out) const;
        template <typename T>
        bool GetAs(Handle handle, T* out) const;

        template <typename T>
        T& getAsRef(Handle handle) const
        {
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "trying to get value for handle",
                          handle,
                          typeid(T).name());
            void* outAsVoid = nullptr;
            get(handle, outAsVoid);
            POW2_ASSERT(outAsVoid != nullptr);
            return *reinterpret_cast<T*>(outAsVoid);
        }

        template <typename T>
        T getAsValue(Handle handle) const;

        template <typename T>
        T* getAsPtr(Handle handle) const;

        template <typename T>
        void setAs(Handle handle, const T& newValue) const;

        int getCount() const;

       private:
        // TODO: implement the invalid handle and make sure to check for it when using the Get*
        // functions
        HandleManager(const HandleManager&);
        HandleManager& operator=(const HandleManager&);

        struct HandleEntry
        {
            HandleEntry();
            explicit HandleEntry(uint32_t nextFreeIndex);

            uint32_t m_nextFreeIndex : 16;
            uint32_t m_counter : 16;
            uint32_t m_active : 1;
            uint32_t m_endOfList : 1;
            void* m_entry;
        };

        HandleEntry m_entries[maxEntries];

        int m_activeEntryCount;
        uint32_t m_firstFreeEntry;
    };

    template <typename T>
    inline T* HandleManager::getAsPtr(Handle handle) const
    {
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "trying to get pointer for handle",
                      handle,
                      "type",
                      typeid(T).name());
        void* outAsVoid = nullptr;
        get(handle, outAsVoid);
        POW2_ASSERT(outAsVoid != nullptr);
        return reinterpret_cast<T*>(outAsVoid);
    }

    template <typename T>
    inline T HandleManager::getAsValue(Handle handle) const
    {
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "trying to get value for handle",
                      handle,
                      typeid(T).name());
        void* outAsVoid = nullptr;
        get(handle, outAsVoid);
        POW2_ASSERT(outAsVoid != nullptr);
        return *reinterpret_cast<T*>(outAsVoid);
    }


    inline bool HandleManager::get(const Handle handle, void*& out) const
    {
        const int index = handle;
        if (m_entries[index].m_counter <= 0 || m_entries[index].m_active == false) return false;
        // TODO: there is a counter + active in the handleEntry. Leave it that way?

        out = m_entries[index].m_entry;
        return true;
    }


    inline void* HandleManager::get(Handle handle) const
    {
        void* p = nullptr;
        if (!get(handle, p)) return nullptr;
        return p;
    }


    template <typename T>
    void HandleManager::setAs(Handle handle, const T& newValue) const
    {
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "trying to set value for handle",
                      handle,
                      typeid(T).name());
        void* outAsVoid = nullptr;
        get(handle, outAsVoid);
        POW2_ASSERT(outAsVoid != nullptr);
        *reinterpret_cast<T*>(outAsVoid) = newValue;
    }

} // namespace Handles

#endif
