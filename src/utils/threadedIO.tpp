
template <typename data_t>
ThreadedIoReader<data_t>::ThreadedIoReader(std::unique_ptr<std::ifstream>& file_, const size_t maxPrefetch_)
    : file(std::move(file_))
    , maxPrefetch(maxPrefetch_)
{
}

template <typename data_t>
std::vector<data_t> ThreadedIoReader<data_t>::read(const size_t num)
{
    std::vector<data_t> data(num);
    this->file->read(reinterpret_cast<char*>(data.data()), num * sizeof(data_t));
    return data;
}

template <typename data_t>
ThreadedIoWriter<data_t>::ThreadedIoWriter(std::unique_ptr<std::ofstream>& file_)
    : file(std::move(file_))
{
}

template <typename data_t>
void ThreadedIoWriter<data_t>::write(const std::vector<data_t>& toWrite)
{
    this->file->write(reinterpret_cast<const char* const>(toWrite.data()), toWrite.size() * sizeof(data_t));
}
