/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BT_UTILS_SERIALIZATION_SPECIALIZATIONS_TPP
#define BT_UTILS_SERIALIZATION_SPECIALIZATIONS_TPP

#pragma GCC diagnostic ignored "-Wunused-function"

template <>
void Serialization::serialize<std::string>(std::vector<Serialization::Buffer_t>& buffer, const std::string& value)
{
    size_t pos = buffer.size();
    buffer.resize(pos + sizeof(uint32_t) + sizeof(char) * value.length());
    Serialization::serializeAt<uint32_t>(buffer, pos, static_cast<uint32_t>(value.length()));
    pos += sizeof(uint32_t);
    for (const char& c : value)
    {
        Serialization::serializeAt<char>(buffer, pos, c);
        pos += sizeof(char);
    }
}

template <>
std::string
Serialization::deserialize<std::string>(const std::vector<Serialization::Buffer_t>& buffer, size_t& pos)
{
    if (pos + sizeof(uint32_t) > buffer.size())
    {
        throw serialization_buffer_overrun(sizeof(uint32_t), pos, buffer.size(), "serialized std::string size");
    }
    // TODO: maybe add a noexcept function which doesn' do this error checking again??
    const auto length = deserialize<uint32_t>(buffer, pos);
    std::string s(static_cast<size_t>(length), '-');

    for (size_t i = 0; i < length; ++i)
    {
        s[i] = deserialize<char>(buffer, pos);
    }
    return s;
}

// must explicitly write (de)serialization functions for size_t
// instead of simply calling (de)serialize<uint32_t> because
// on systems where size_t == uint32_t
// this leads to the specialization function calling itself
template <>
void Serialization::serialize<size_t>(std::vector<Serialization::Buffer_t>& buffer, const size_t& value)
{
    const size_t oldSize = buffer.size();
    buffer.resize(oldSize + sizeof(uint32_t));
    *reinterpret_cast<uint32_t* const>(&buffer[oldSize]) = static_cast<uint32_t>(value);
}
template <>
size_t Serialization::deserialize<size_t>(const std::vector<Serialization::Buffer_t>& buffer, size_t& pos)
{
    if (pos + sizeof(uint32_t) > buffer.size())
    {
        throw serialization_buffer_overrun(1, pos, buffer.size(), "size_t (as uint32_t)");
    }
    const uint32_t value = *reinterpret_cast<const uint32_t*>(&buffer[pos]);
    pos += sizeof(uint32_t);
    return static_cast<size_t>(value);
}

template <>
void Serialization::serialize<irr::core::stringw>(std::vector<Serialization::Buffer_t>& buffer,
                                                  const irr::core::stringw& value)
{
    size_t pos = buffer.size();
    buffer.resize(pos + sizeof(uint32_t) + sizeof(wchar_t) * value.size());
    Serialization::serializeAt<uint32_t>(buffer, pos, static_cast<uint32_t>(value.size()));
    pos += sizeof(uint32_t);
    for (irr::u32 i = 0; i < value.size(); ++i)
    {
        Serialization::serializeAt<wchar_t>(buffer, pos, value[i]);
        pos += sizeof(wchar_t);
    }
}

template <>
irr::core::stringw
Serialization::deserialize<irr::core::stringw>(const std::vector<Serialization::Buffer_t>& buffer, size_t& pos)
{
    if (pos + sizeof(uint32_t) > buffer.size())
    {
        throw serialization_buffer_overrun(sizeof(uint32_t), pos, buffer.size(), "serialized std::string size");
    }
    // TODO: maybe add a noexcept function which doesn' do this error checking again??
    const auto length = deserialize<uint32_t>(buffer, pos);
    irr::core::stringw s;
    for (size_t i = 0; i < length; ++i)
    {
        s.append(deserialize<wchar_t>(buffer, pos));
    }
    return s;
}

#pragma GCC diagnostic pop

#endif /* ifndef BT_UTILS_SERIALIZATION_SPECIALIZATIONS_TPP */
