#pragma once

#ifndef THREADEDIO_H
#define THREADEDIO_H

#include <fstream>
#include <memory>
#include <vector>

/// @brief reads data from a file and also prefetches new data in an extra reading thread.
/// @tparam data_t
template <typename data_t>
class ThreadedIoReader
{
   public:
    /// @brief
    /// @param file_
    /// @param maxPrefetch how many data_t elements are prefetched and stored. Total neccecary
    /// memory size is sizeof(data_t) * maxPrefetch.
    ThreadedIoReader(std::unique_ptr<std::ifstream>& file_, const size_t maxPrefetch_ = 1024);

    /// @brief read 'num' elements of 'data_t' out of the file and starts prefetching in the
    /// background.
    /// @param num how many data elements to read
    /// @return data read from the file or from prefetched memory
    std::vector<data_t> read(const size_t num);

   private:
    const std::unique_ptr<std::ifstream> file;

    std::vector<data_t> buffer = {};

    const size_t maxPrefetch = 0;
};

/// @brief writes data to a file using a designated writer thread
/// All data will be written into the file in the destructor of this class and the file will be
/// closed.
/// @tparam data_t
template <typename data_t>
class ThreadedIoWriter
{
   public:
    /// @brief
    /// @param file_
    /// memory size is sizeof(data_t) * maxPrefetch.
    ThreadedIoWriter(std::unique_ptr<std::ofstream>& file_);

    /// @brief writes 'data' into the file using a dedicated writer thread.
    void write(const std::vector<data_t>& data);

   private:
    const std::unique_ptr<std::ofstream> file;

    std::vector<data_t> buffer;
};

#include "threadedIO.tpp"

#endif // THREADEDIO_H
