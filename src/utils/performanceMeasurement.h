/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef PERFORMANCE_MEASUREMENT_H
#define PERFORMANCE_MEASUREMENT_H

#include <algorithm> // std::min/max
#include <iostream>

template <typename T>
class PerformanceMeasurement
{
   public:
    struct PerformanceResult
    {
        T min = std::numeric_limits<T>::max();
        float average = 0.0f;
        T max = std::numeric_limits<T>::lowest();
    };

    PerformanceResult getPerformanceResult() const { return this->currentPerformance; }
    void addMeasurement(const T m)
    {
        this->currentPerformance.min = std::min(m, this->currentPerformance.min);
        this->currentPerformance.max = std::max(m, this->currentPerformance.max);
        // calculate rolling average
        const auto tmp1 = this->currentPerformance.average / (cntInAverage + 1.0f);
        const auto tmp2 = 1.0f / (cntInAverage + 1.0f) * static_cast<float>(m);
        this->currentPerformance.average = this->cntInAverage * tmp1 + tmp2;
        this->cntInAverage += 1.0f;
    }

   private:
    float cntInAverage = 0.0f;

    PerformanceResult currentPerformance = PerformanceResult();
};

std::ostream& operator<<(std::ostream& stream, const PerformanceMeasurement<float>::PerformanceResult& result);

#endif /* ifndef PERFORMANCE_MEASUREMENT_H */
