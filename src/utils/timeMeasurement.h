/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TIME_MEASUREMENT_H
#define TIME_MEASUREMENT_H

#include <chrono>

// template <std::chrono::duration<T1, T2>>
class TimeMeasurement
{
   public:
    TimeMeasurement()
        : start(std::chrono::steady_clock::now())
    {
    }

    inline float durationMS() const
    {
        const auto end = std::chrono::steady_clock::now();
        const auto duration = end - start;
        using ms = std::chrono::duration<float, std::milli>;
        return std::chrono::duration_cast<ms>(duration).count();
    }

   private:
    std::chrono::steady_clock::time_point start;
};

#endif /* ifndef TIME_MEASUREMENT_H */
