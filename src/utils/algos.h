#pragma once

#include <algorithm>
#include <locale>
#include <string>

template <typename T>
T clamp(const T& x, const T& min, const T& max)
{
    return std::min(std::max(x, min), max);
}

inline bool equalIgnoreCase(const std::string& a, const std::string& b)
{
    return std::equal(a.begin(), a.end(), b.begin(), b.end(), [](const char& ca, const char& cb) {
        return std::tolower(ca, std::locale()) == std::tolower(cb, std::locale());
    });
}

template <typename Container, typename T>
inline auto find(const Container& container, T&& f)
{
    return std::find(std::begin(container), std::end(container), std::forward<T>(f));
}

template <typename Container, typename Value>
inline bool contains(Container&& container, Value&& v)
{
    return std::find(std::begin(container), std::end(container), std::forward<Value>(v)) != std::end(container);
}
