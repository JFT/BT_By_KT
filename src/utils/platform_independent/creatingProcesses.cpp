/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if defined(__unix__)
#include <string.h>    // get error as string
#include <sys/types.h> // waidpid
#include <sys/wait.h>  // waitpid
#include <unistd.h>    // contains fork()
#include <unistd.h>    // execv
#elif defined(__WIN32__) || defined(WIN32)
#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#endif

#include "creatingProcesses.h"

#include "../debug.h"
#include "../static/error.h"

using namespace PlatformIndependent;

void ProcessAbstraction::waitForExit()
{
    if (!this->isRunning_)
    {
        // nothing to do because the process didn't even start
        return;
    }
#if defined(__unix__)
    waitpid(this->childProcessID, nullptr, 0);
#elif defined(__WIN32__) || defined(WIN32)
    // Wait until child process exits.
    WaitForSingleObject(this->processInformation->hProcess, INFINITE);
    // Close process and thread handles.
    CloseHandle(this->processInformation->hProcess);
    CloseHandle(this->processInformation->hThread);
#endif
    this->isRunning_ = false;
}

bool ProcessAbstraction::executeProcess(const std::string pathToExe_)
{
    debugOutLevel(Debug::DebugLevels::stateInit + 1, "executeProcess:", pathToExe_);

#if defined(__unix__)
    // fork a new process
    pid_t childPID = fork();
    if (childPID < 0)
    {
        // TODO: maybe use thread-save strerror_r() version?
        std::string errorString(strerror(errno));
        Error::errContinue("fork() failed! Error:", errorString);
        return false;
    }
    if (childPID == 0)
    {
        // inside the child
        // HACK: fork() doesn't close open file descriptors. This leads to a problem when testing
        // network related stuff: launching the server from within the client the server inherits
        // the open socket listening on 'BattleTanks::Networking::advertisePortLAN' which the client
        // needs to find the server it just started.
        // This is especially bad if the client leaves the serverBrowserState and tries to re-enter
        // it because it can't bind to that port while it is still used by the server.
        // Thus: try to close all open file descriptors
        // But there is no really platform independent way to do this -> ugly hack
        // see: https://stackoverflow.com/questions/1315382/closing-all-open-files-in-a-process
        // and: https://stackoverflow.com/questions/899038/getting-the-highest-allocated-file-descriptor/918469#918469
        for (int fd = 3; fd < 65536; fd++) // fd = 3 -> leave stdin(fd=0)/out(1)/err(2) open
        {
            close(fd); // closing a fd which isn't valid generates 'EBADF' which can be ignored
        }

        // run the server executable (this replaces the child process completely with the supplied
        // exepath)
        char* processName = strdup(pathToExe_.c_str());
        char* argv[] = {processName, nullptr}; // execv wants a nullptr terminated list of additional arguments
        int execRetVal = execv(pathToExe_.c_str(), argv);
        // this error handling code only executes if the execv syscall fails. Otherwise the child
        // process does't exist
        // TODO: maybe use thread-save strerror_r() version?
        std::string errorString(strerror(errno));
        // inside the child so we can call errTerminate without killing the initial caller of
        // executeProcess
        Error::errTerminate("execv() failed!, execRetVal=", execRetVal, "Error:", errorString);
    } // the client can never reach any code outside this bracket
    this->childProcessID = childPID;
#elif defined(__WIN32__) || defined(_WIN32) || defined(WIN32)
    LPSTARTUPINFOA si;

    ZeroMemory(&si, sizeof(si));
    si->cb = sizeof(si);
    ZeroMemory(&(this->processInformation), sizeof(this->processInformation));

    LPSTR pathToExe = const_cast<LPSTR>((pathToExe_ + std::string(".exe")).c_str());

    // Start the child process.
    if (!CreateProcessA(nullptr,                    // No module name (use command line)
                        pathToExe,                  // Command line
                        nullptr,                    // Process handle not inheritable
                        nullptr,                    // Thread handle not inheritable
                        FALSE,                      // Set handle inheritance to FALSE
                        CREATE_NEW_CONSOLE,         // No creation flags
                        nullptr,                    // Use parent's environment block
                        nullptr,                    // Use parent's starting directory
                        si,                         // Pointer to STARTUPINFO structure
                        (this->processInformation)) // Pointer to PROCESS_INFORMATION structure
    )
    {
        Error::errContinue("CreateProcess '", pathToExe, "' failed ", GetLastError());
        return false;
    }
#endif
    this->isRunning_ = true;
    return true;
}
