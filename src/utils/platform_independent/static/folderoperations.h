/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FOLDEROPERATIONS_H
#define FOLDEROPERATIONS_H

#if defined(__unix__)
#include <sys/stat.h>
#include <sys/types.h>
#elif defined(__MINGW32__)
#include <dir.h>
#elif defined(__WIN32__) || defined(WIN32)
#include <direct.h>
#endif

#include <irrlicht/irrArray.h>
#include <irrlicht/irrString.h>

#include "../../stringwparser.h"

namespace PlatformIndependent
{
    static void createFolder(const char* foldername)
    {
#if defined(__unix__)
        // TODO: find out correct write mode to use
        mkdir(foldername, S_IRWXU);
#elif defined(__MINGW32__)
        mkdir(foldername);
#elif defined(__WIN32__) || defined(WIN32)
        _mkdir(foldername);
#endif
    }

    static void createDirectory(const char* path) // create folder by folder
    {
        irr::core::array<irr::core::stringw> folder;
        ErrCode parsingResult =
            StringWParser::parse(path, folder, irr::core::array<irr::core::stringw>(), L'/', false);
        if (parsingResult != ErrCode(ErrCodes::NO_ERR)) // check for parsing Errors //means here no folder given, so its no real error
        {
            return; // every thing fine, no folder given, so go on
            // errTerminate(irr::core::stringw("couldn't parse '") + irr::core::stringw(path) +
            // irr::core::stringw("'"));
        }
        irr::core::stringc directory = L"";
        for (unsigned int i = 0; i < folder.size() - 1;
             i++) // folder.size()-1 because last element in folder is a document and no folder
        {
            directory.append(folder[i].c_str());
            const char* newDirectory = directory.c_str();
            createFolder(newDirectory);
            directory.append(L'/');
        }
    }
} // namespace PlatformIndependent

#endif // FOLDER_OPERATIONS_H
