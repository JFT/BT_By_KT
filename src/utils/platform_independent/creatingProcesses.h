/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CREATINGPROCESSES_H_INCLUDED
#define CREATINGPROCESSES_H_INCLUDED

#include <string>

#if defined(__unix__)
#include <string.h> // get error as string
#include <unistd.h> // contains fork()
#elif defined(__WIN32__) || defined(_WIN32) || defined(WIN32)
#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#endif

namespace PlatformIndependent
{
    /// @brief abstraction layer for started processes
    class ProcessAbstraction
    {
       public:
        /// @brief wait for the started process to exit and set isRunning to false. If the process
        /// isn't running return immediately.
        void waitForExit();

        /// @brief executes the process pathToExe_
        /// @param pathToExe_ if on windows a '.exe' extension will be added automatically
        /// @return true if started succesfully, false otherwise.
        bool executeProcess(const std::string pathToExe_);

        bool isRunning() const { return this->isRunning_; }

       private:
        bool isRunning_ = false;

#if defined(__unix__)
        pid_t childProcessID = 0;
#elif defined(__WIN32__) || defined(_WIN32) || defined(WIN32)
        LPPROCESS_INFORMATION processInformation;
#endif
    };

} // namespace PlatformIndependent

#endif // CREATINGPROCESSES_H_INCLUDED
