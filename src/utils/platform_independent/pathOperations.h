/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015-2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef UTILS_PLATFORM_INDEPENDENT_PATHOPERATIONS
#define UTILS_PLATFORM_INDEPENDENT_PATHOPERATIONS

#include <string>

namespace PlatformIndependent
{
    /// @brief return the relative path based at 'root'.
    /// e.g.:
    /// /a/b/c  b  -> b/c
    /// /a/b/   b  -> b/
    /// /a/b    b  -> b/        (b assumet do be a directory)
    /// /b/     b  -> b/        (beginning '/' are stripped)
    /// /c/d/e  b  -> /c/d/e    (no subpath 'b')
    /// /a/b/c  /  -> /a/b/c    (root was '/')
    /// @param path
    /// @param root
    /// @return the relative path from (and including) 'root' or 'path' if 'root' isn't a subpath of
    /// 'path' or 'root' == '/'
    std::string getRelativePathWithRoot(std::string path, std::string root)
    {
        const std::string unModifiledPath = path;

        if (root.size() == 0 or path.size() == 0)
        {
            return path;
        }

        if (root.compare("/") == 0)
        {
            return path;
        }

        if (*root.rbegin() != '/')
        {
            root = root + '/';
        }
        if (*root.begin() != '/')
        {
            root = '/' + root;
        }

        const bool pathEndedWithSlash = *path.rbegin() == '/';
        if (not pathEndedWithSlash)
        {
            path = path + '/';
        }
        if (*path.begin() != '/')
        {
            path = "/" + path;
        }

        auto rootAt = path.rfind(root);
        if (rootAt != std::string::npos)
        {
            if (rootAt + root.size() == path.size())
            {
                // root is completely at the end of the path -> return including the appended '/'
                // + 1 to strip the prepended '/'
                return std::string(path, rootAt + 1, std::string::npos);
            }
            if (not pathEndedWithSlash)
            {
                // strip prepended AND appended '/'
                return std::string(path, rootAt + 1, path.size() - rootAt - 2);
            }
        }
        return unModifiledPath;
    }
} // namespace PlatformIndependent

#endif /* ifndef UTILS_PLATFORM_INDEPENDENT_PATHOPERATIONS */
