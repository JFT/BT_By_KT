/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "debugging.h"

namespace platformIndependent
{
    void debugBreak()
    {
// debugging signal selection code shamelessly stolen from bx (git://github.com/bkaradzic/bx.git)
#if defined(_MSC_VER)
        __debugbreak();
/*#elif BX_CPU_ARM
          __builtin_trap();
        //		asm("bkpt 0");
#elif !BX_PLATFORM_NACL && BX_CPU_X86 && (BX_COMPILER_GCC || BX_COMPILER_CLANG)
        // NaCl doesn't like int 3:
        // NativeClient: NaCl module load failed: Validation failure. File violates Native Client safety rules.
        __asm__ ("int $3");
        */
#else // cross platform implementation
        int* int3 = reinterpret_cast<int*>(3L);
        *int3 = 3;
#endif
    }
} // namespace platformIndependent
