/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef BT_UTILS_SERIALIZATIONTYPES_H
#define BT_UTILS_SERIALIZATIONTYPES_H

#include <cstdint> // (u)intX_t

namespace Serialization
{
    typedef uint16_t SerializationVersion_t; // there should probably be no more than 65535 versions
                                             // of anything
    typedef int8_t Buffer_t;
} // namespace Serialization

#endif /* ifndef BT_UTILS_SERIALIZATIONTYPES_H */
