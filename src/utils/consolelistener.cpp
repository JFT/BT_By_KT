/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "consolelistener.h"
#include <iostream> // std::cin
#include <thread>

ConsoleListener::ConsoleListener(const bool shouldListen)
    : inputQueue()
    , queueAccess()
    , listenerThread(nullptr)
{
    if (shouldListen)
    {
        // actual initialization in constructor body to make sure the thread will be started after
        // everything else is initialized
        // normally threads start static methods so we also need to give the thread 'this' and the
        // function
        // https://stackoverflow.com/questions/18383600/c-stdthread-of-a-member-function
        listenerThread = new std::thread(&ConsoleListener::readInput, this);
    }
}

ConsoleListener::~ConsoleListener()
{
    if (listenerThread != nullptr)
    {
        listenerThread->join();
    }
}

void ConsoleListener::startListening()
{
    // already listening?
    if (listenerThread != nullptr)
    {
        return;
    }
    // normally threads start static methods so we also need to give the thread 'this' and the
    // function
    // https://stackoverflow.com/questions/18383600/c-stdthread-of-a-member-function
    listenerThread = new std::thread(&ConsoleListener::readInput, this);
}

bool ConsoleListener::queueEmpty()
{
    // TODO: does this actually need locking? enqueue and append have locks, so reading the state
    // should be harmless
    bool retval = false;
    queueAccess.lock();
    if (inputQueue.empty())
    {
        retval = true;
    }
    queueAccess.unlock();
    return retval;
}

std::string ConsoleListener::popInput()
{
    queueAccess.lock();
    // we need to test if there is anything there inside the critical area
    std::string retval = "";
    if (!inputQueue.empty())
    {
        // get the first element in the queue and pop it
        retval = inputQueue.front();
        inputQueue.pop();
    }
    queueAccess.unlock();
    return retval;
}

void ConsoleListener::readInput()
{
    while (true)
    {
        std::string toAppend = "";
        std::getline(std::cin, toAppend);
        enqueueInput(toAppend);
    }
}

void ConsoleListener::enqueueInput(const std::string input)
{
    if (input.compare("") == 0)
    {
        return;
    }
    queueAccess.lock();
    inputQueue.push(input);
    queueAccess.unlock();
}
