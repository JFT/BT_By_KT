/*
 * Copyright (c) 2008, Power of Two Games LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Power of Two Games LLC nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY POWER OF TWO GAMES LLC ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL POWER OF TWO GAMES LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 **Edited by: Julius Tilly
 */

#include "HandleManager.h"
#include <cstddef>
#include "Pow2Assert.h"

using namespace Handles;

HandleManager::HandleEntry::HandleEntry()
    : m_nextFreeIndex(0)
    , m_counter(1)
    , m_active(0)
    , m_endOfList(0)
    , m_entry(nullptr)
{
}

HandleManager::HandleEntry::HandleEntry(uint32_t nextFreeIndex)
    : m_nextFreeIndex(nextFreeIndex)
    , m_counter(1)
    , m_active(0)
    , m_endOfList(0)
    , m_entry(nullptr)
{
}


HandleManager::HandleManager()
{
    reset();
}


void HandleManager::reset()
{
    m_activeEntryCount = 0;
    m_firstFreeEntry = 0;

    for (int i = 0; i < maxEntries - 1; ++i) m_entries[i] = HandleEntry(i + 1);
    m_entries[maxEntries - 1] = HandleEntry();
    m_entries[maxEntries - 1].m_endOfList = true;
}


Handle HandleManager::add(void* p)
{
    POW2_ASSERT(m_activeEntryCount < maxEntries - 1);

    const int newIndex = m_firstFreeEntry;
    POW2_ASSERT(newIndex < maxEntries);
    POW2_ASSERT(m_entries[newIndex].m_active == false);
    POW2_ASSERT(!m_entries[newIndex].m_endOfList);

    m_firstFreeEntry = m_entries[newIndex].m_nextFreeIndex;
    m_entries[newIndex].m_nextFreeIndex = 0;
    m_entries[newIndex].m_counter = m_entries[newIndex].m_counter + 1;
    if (m_entries[newIndex].m_counter == 0) m_entries[newIndex].m_counter = 1;
    m_entries[newIndex].m_active = true;
    m_entries[newIndex].m_entry = p;

    ++m_activeEntryCount;

    return Handle(newIndex);
}


void HandleManager::update(Handle handle, void* p)
{
    const int index = handle;
    //	POW2_ASSERT(m_entries[index].m_counter == handle.m_counter);
    POW2_ASSERT(m_entries[index].m_active == true);

    m_entries[index].m_entry = p;
}


void HandleManager::remove(const Handle handle)
{
    const uint32_t index = handle;
    //	POW2_ASSERT(m_entries[index].m_counter == handle.m_counter);
    POW2_ASSERT(m_entries[index].m_active == true);

    m_entries[index].m_nextFreeIndex = m_firstFreeEntry;
    m_entries[index].m_active = 0;
    m_firstFreeEntry = index;

    --m_activeEntryCount;
}

bool HandleManager::isValid(const Handle handle) const
{
    const int index = handle;
    if (m_entries[index].m_counter <= 0 || m_entries[index].m_active == false) return false;
    // TODO: there is a counter + active in the handleEntry. Leave it that way?
    return true;
}


int HandleManager::getCount() const
{
    return m_activeEntryCount;
}
