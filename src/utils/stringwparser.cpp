/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <irrlicht/EDriverTypes.h>
#include <irrlicht/SColor.h>
#include <irrlicht/irrArray.h>
#include <irrlicht/irrString.h>
#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>

#include <unordered_map>

#include "Pow2Assert.h"
#include "stringwparser.h"

#include "debug.h"

// int

template <>
int StringWParser::parseInternal<int>(std::wstring str, size_t& readTo)
{
    return std::stoi(str, &readTo);
}

// float

template <>
float StringWParser::parseInternal<float>(std::wstring str, size_t& readTo)
{
    return std::stof(str, &readTo);
}

// unsigned int

template <>
unsigned int StringWParser::parseInternal<unsigned int>(std::wstring str, size_t& readTo)
{
    // there is no method to parse to unsigned int directly. So we parse to unsigned long, cast to
    // uint and compare the values to see if the long fits into the uint
    // https://stackoverflow.com/questions/18037835/c-converting-string-to-unsigned-int
    const unsigned long parsedULong = std::stoul(str, &readTo);
    const unsigned int parsedUint = static_cast<unsigned int>(parsedULong);
    if (parsedUint != parsedULong) // the unsigned int was to small to fit the long value
    {
        throw std::out_of_range(std::string("argument out of range for unsigned int: ") +
                                std::to_string(parsedULong));
    }
    return parsedUint;
}

// irr::u16

template <>
irr::u16 StringWParser::parseInternal<irr::u16>(std::wstring str, size_t& readTo)
{
    // do the same as done in the unsigned int parsing
    const unsigned long parsedULong = std::stoul(str, &readTo);
    const irr::u16 parsedU16 = static_cast<irr::u16>(parsedULong);
    if (parsedU16 != parsedULong) // check for overflow error
    {
        throw std::out_of_range(std::string("argument out of range for irr::u16: ") + std::to_string(parsedULong));
    }
    return parsedU16;
}

// irr::u8

template <>
irr::u8 StringWParser::parseInternal<irr::u8>(std::wstring str, size_t& readTo)
{
    // do the same as done in the unsigned int parsing
    const unsigned long parsedULong = std::stoul(str, &readTo);
    const irr::u8 parsedU8 = static_cast<irr::u8>(parsedULong);
    if (parsedU8 != parsedULong) // check for overflow error
    {
        throw std::out_of_range(std::string("argument out of range for irr::u8: ") + std::to_string(parsedULong));
    }
    return parsedU8;
}

// irr::core::vector2df

template <>
irr::core::vector2df StringWParser::parseInternal<irr::core::vector2df>(std::wstring str, size_t& readTo)
{
    float X, Y;
    size_t readToLocal;
    X = std::stof(str, &readToLocal);
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parsed out X=", X, "rest=", repr<std::wstring>(str), "readToLocal=", readToLocal, "readTo=", readTo);
    Y = std::stof(str, &readToLocal);
    readTo += readToLocal;
    str = str.substr(readToLocal);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parsed out Y=", Y, "rest=", repr<std::wstring>(str), "readToLocal=", readToLocal, "readTo=", readTo);
    return irr::core::vector2df(X, Y);
}

template <>
irr::core::stringw StringWParser::getParsableString<irr::core::vector2df>(const irr::core::vector2df& toSave)
{
    return irr::core::stringw(toSave.X) + irr::core::stringw(L" ") + irr::core::stringw(toSave.Y);
}

// irr::core::dimension2du

template <>
irr::core::dimension2du StringWParser::parseInternal<irr::core::dimension2du>(std::wstring str, size_t& readTo)
{
    irr::u32 width, height;
    size_t readToLocal = 0;
    width = parseInternal<irr::u32>(str, readToLocal);
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parsed out width=", width, "rest=", repr<std::wstring>(str), "readToLocal=", readToLocal, "readTo=", readTo);
    readToLocal = 0;
    height = parseInternal<irr::u32>(str, readToLocal);
    readTo += readToLocal;
    str = str.substr(readToLocal);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "parsed out height=",
                  height,
                  "rest=",
                  repr<std::wstring>(str),
                  "readToLocal=",
                  readToLocal,
                  "readTo=",
                  readTo);
    return irr::core::dimension2du(width, height);
}

template <>
irr::core::stringw
StringWParser::getParsableString<irr::core::dimension2du>(const irr::core::dimension2du& toSave)
{
    return irr::core::stringw(toSave.Width) + irr::core::stringw(L" ") + irr::core::stringw(toSave.Height);
}

// irr::core::vector3df

template <>
irr::core::vector3df StringWParser::parseInternal<irr::core::vector3df>(std::wstring str, size_t& readTo)
{
    float X, Y, Z;
    size_t readToLocal = 0;
    X = std::stof(str, &readToLocal);
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parsed out X=", X, "rest=", repr<std::wstring>(str), "readToLocal=", readToLocal, "readTo=", readTo);
    readToLocal = 0;
    Y = std::stof(str, &readToLocal);
    readTo += readToLocal;
    str = str.substr(readToLocal);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parsed out Y=", Y, "rest=", repr<std::wstring>(str), "readToLocal=", readToLocal, "readTo=", readTo);
    readToLocal = 0;
    Z = std::stof(str, &readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parsed out Z=", Z, "rest=", repr<std::wstring>(str), "readToLocal=", readToLocal, "readTo=", readTo);
    return irr::core::vector3df(X, Y, Z);
}

template <>
irr::core::stringw StringWParser::getParsableString<irr::core::vector3df>(const irr::core::vector3df& toSave)
{
    return irr::core::stringw(toSave.X) + irr::core::stringw(L" ") + irr::core::stringw(toSave.Y) +
        irr::core::stringw(L" ") + irr::core::stringw(toSave.Z);
}

// irr::core::array<unsigned int>

template <>
irr::core::array<unsigned int>
StringWParser::parseInternal<irr::core::array<unsigned int>>(std::wstring str, size_t& readTo)
{
    irr::core::array<unsigned int> arr;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parse internal -> irr UInt array str=", repr<std::wstring>(str), "readTo=", readTo);
    while (str.size() > 0)
    {
        // beacause the string keeps shrinking with each loop iteration the total readTo variable
        // bust be updatet in every step
        size_t readToLocal = 0;
        unsigned int parsed = parseInternal<unsigned int>(str, readToLocal);
        arr.push_back(parsed);
        str = str.substr(readToLocal);
        readTo += readToLocal;
        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                      "parse internal -> irr UInt array str=",
                      repr<std::wstring>(str),
                      "readTo=",
                      readTo,
                      "readToLocal=",
                      readToLocal);
    }
    return arr;
}

// bool

template <>
bool StringWParser::parseInternal(std::wstring str, size_t& readTo)
{
    if (str.compare(L"true") == 0)
    {
        readTo = str.size(); // this is needed to pass the check in the calling parse() function
        return true;
    }
    else if (str.compare(L"false") == 0)
    {
        readTo = str.size();
        return false;
    }
    else
    {
        readTo = 0;
        throw std::invalid_argument("argument not parsable as bool");
    }
}

template <>
irr::core::stringw StringWParser::getParsableString(const bool& toSave)
{
    if (toSave)
    {
        return irr::core::stringw(L"true");
    }
    else
    {
        return irr::core::stringw(L"false");
    }
}

// irr::core::vector2d<unsigned int>

template <>
irr::core::vector2d<unsigned int> StringWParser::parseInternal(std::wstring str, size_t& readTo)
{
    irr::core::array<unsigned int> arr;
    arr = parseInternal<irr::core::array<unsigned int>>(str, readTo);
    if (arr.size() != 2)
    {
        throw std::invalid_argument("must be exactly two unsigned integers");
    }
    irr::core::vector2d<unsigned int> retVal = {arr[0], arr[1]};
    return retVal;
}


template <> // TODO: bad parse-style
irr::core::stringw StringWParser::parseInternal<irr::core::stringw>(std::wstring str, size_t& readTo)
{
    readTo = str.find(L' '); // set constant delimiter here
    unsigned int delta = 0;
    while (readTo == 0)
    {
        str.erase(0, 1); // kill left delimiter
        delta++;         // count killed delimiter
        if ((readTo = str.find(L" ")) > str.size())
        {
            readTo = str.size(); // no delimiter left in string set readTo to string end
        }
    }
    str = str.substr(0, readTo);

    readTo = readTo + delta; // add killed delimiter to local string length
    return irr::core::stringw(str.c_str());
}


template <>
irr::core::array<irr::core::stringw>
StringWParser::parseInternal<irr::core::array<irr::core::stringw>>(std::wstring str, size_t& readTo)
{
    irr::core::array<irr::core::stringw> arr;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parse internal->IrrArr<stringw> str=", repr<std::wstring>(str), "readTo=", readTo);
    while (str.size() > 0)
    {
        // beacause the string keeps shrinking with each loop iteration the total readTo variable
        // bust be updatet in every step
        size_t readToLocal = 0;
        irr::core::stringw parsed = parseInternal<irr::core::stringw>(str, readToLocal);
        arr.push_back(parsed);
        str = str.substr(readToLocal);
        readTo += readToLocal;
        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                      "parse internal->IrrArr<stringw>=",
                      repr<std::wstring>(str),
                      "readTo=",
                      readTo,
                      "readToLocal=",
                      readToLocal);
    }
    return arr;
}

template <>
irr::core::vector2d<irr::core::stringw> StringWParser::parseInternal(std::wstring str, size_t& readTo)
{
    irr::core::array<irr::core::stringw> arr;
    arr = parseInternal<irr::core::array<irr::core::stringw>>(str, readTo);
    if (arr.size() != 2)
    {
        throw std::invalid_argument("must be exactly two stringw");
    }
    irr::core::vector2d<irr::core::stringw> retVal = {arr[0], arr[1]};
    return retVal;
}

// SColor

template <>
irr::video::SColor StringWParser::parseInternal(std::wstring str, size_t& readTo)
{
    irr::u32 alpha, red, green, blue;
    size_t readToLocal;

    alpha = parseInternal<irr::u32>(str, readToLocal);
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parsed out alpha=", alpha, "rest=", repr<std::wstring>(str), "readToLocal=", readToLocal, "readTo=", readTo);

    red = parseInternal<irr::u32>(str, readToLocal);
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parsed out red=", red, "rest=", repr<std::wstring>(str), "readToLocal=", readToLocal, "readTo=", readTo);

    green = parseInternal<irr::u32>(str, readToLocal);
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parsed out green=", green, "rest=", repr<std::wstring>(str), "readToLocal=", readToLocal, "readTo=", readTo);

    blue = parseInternal<irr::u32>(str, readToLocal);
    str = str.substr(readToLocal);
    readTo += readToLocal;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "parsed out blue=", blue, "rest=", repr<std::wstring>(str), "readToLocal=", readToLocal, "readTo=", readTo);

    return irr::video::SColor(alpha, red, green, blue);
}

template <>
irr::core::stringw StringWParser::getParsableString<irr::video::SColor>(const irr::video::SColor& toSave)
{
    irr::core::stringw retVal;
    retVal += toSave.getAlpha();
    retVal += L' ';
    retVal += toSave.getRed();
    retVal += L' ';
    retVal += toSave.getGreen();
    retVal += L' ';
    retVal += toSave.getBlue();
    return retVal;
}

// irr::video::E_DRIVER_TYPE

namespace
{
    const std::unordered_map<irr::video::E_DRIVER_TYPE, const std::wstring> drivers = {
        {irr::video::E_DRIVER_TYPE::EDT_SOFTWARE, L"software"},
        {irr::video::E_DRIVER_TYPE::EDT_OPENGL, L"opengl"},
        {irr::video::E_DRIVER_TYPE::EDT_BGFX_OPENGL, L"bgfx_opengl"},
        {irr::video::E_DRIVER_TYPE::EDT_BGFX_D3D9, L"bgfx_d3d9"},
        {irr::video::E_DRIVER_TYPE::EDT_BGFX_D3D11, L"bgfx_d3d11"},
        {irr::video::E_DRIVER_TYPE::EDT_BGFX_METAL, L"bgfx_metal"},
        {irr::video::E_DRIVER_TYPE::EDT_NULL, L"null"},
        {irr::video::E_DRIVER_TYPE::DEPRECATED_EDT_DIRECT3D8_NO_LONGER_EXISTS, L"direct3d8"},
        {irr::video::E_DRIVER_TYPE::EDT_DIRECT3D9, L"direct3d9"},
        {irr::video::E_DRIVER_TYPE::EDT_BURNINGSVIDEO, L"burningsvideo"},
    };
}

template <>
irr::video::E_DRIVER_TYPE StringWParser::parseInternal(std::wstring str, size_t& readTo)
{
    for (const auto& driver : drivers)
    {
        if (driver.second.compare(str) == 0)
        {
            readTo = str.size();
            return driver.first;
        }
    }
    throw std::invalid_argument("argument not parsable as E_DRIVER_TYPE");
}

template <>
irr::core::stringw
StringWParser::getParsableString<irr::video::E_DRIVER_TYPE>(const irr::video::E_DRIVER_TYPE& toSave)
{
    POW2_ASSERT(drivers.count(toSave) == 1);

    return irr::core::stringw(drivers.at(toSave).c_str());
}
