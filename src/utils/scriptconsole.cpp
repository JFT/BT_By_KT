/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scriptconsole.h"

// headers containing only static functions can be included in .cpp files
#include "static/conversions.h"

#include <array>
#include <sstream>

#include "../scripting/scriptingModule.h"

#if defined(MAKE_SCRIPTING_CONSOLE_)

// Store String output from lua to send it to the ingame console later
std::vector<std::string> stringBuffer;

ScriptConsole::ScriptConsole(const bool useStdoutForOutput_, ScriptingModule* const scriptModule_)
    : useStdoutForOutput(useStdoutForOutput_)
    , scriptModule(scriptModule_)
{
    // redirect output to print() into the console
    // TODO: implemet for multiple threads (currently only done for thread0, maybe later for
    // multiple threads)
    if (!useStdoutForOutput)
    {
        scriptModule->getScriptEngine().singleInstance()["stringBuffer"] = std::ref(stringBuffer);
        //        scriptModule->getScriptEngine().singleInstance().safe_script(
        /*            "oldPrint = print\nprint = function(...)"
                    "\n\tfor i = 1, select(\"#\", "
                    "...) do\n\tstringBuffer:add(tostring(select(i, ...)))\n\tstringBuffer:add(\"
           \"); " "end\n\tstringBuffer:add(\"\\n\")\nend");*/
        /*
        "oldPrint = print\nprint = function(...)\n\toldPrint(\"capturing to stringbufffer in "
            "single Instance:\")\n\toldPrint(...)\n\tfor i = 1, select(\"#\", "
            "...) do\n\tstringBuffer:add(tostring(select(i, ...)))\n\tstringBuffer:add(\" \"); "
            "end\n\tstringBuffer:add(\"\\n\")\nend");
        */
    }
}

ScriptConsole::~ScriptConsole() {}

std::string ScriptConsole::evalInputLine(const std::string input)
{
    std::string retval;

    try
    {
        // TODO: enable selecting for single instance or all instances
        if (!scriptModule->getScriptEngine().singleInstance()["ApplyToAllInstances"])
        {
            scriptModule->getScriptEngine().singleInstance().safe_script(input);
        }
        else
        {
            scriptModule->getScriptEngine().loopOverInstances(
                [input](sol::state& instance) { instance.safe_script(input); });
        }

        // scriptModule->getScriptEngine().singleInstance().script(input);
    }
    catch (const std::exception& e)
    {
        retval = std::string("Error: ") + e.what();
    }

    for (auto s : stringBuffer)
    {
        retval.append(s);
    }
    stringBuffer.clear();

    return retval;
}

irr::core::stringw ScriptConsole::evalInputLine(const irr::core::stringw input)
{
    // there doesn't seem to be a way to pass a wchar_t* as command string
    // so we convert to string, run the command, and convert back
    std::string inputStr = convertToString(input);
    std::string output = evalInputLine(inputStr);
    return irr::core::stringw(output.c_str());
}


#endif // defined(MAKE_PYCONSOLE_)
