/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// default to building with scripting console support
#if !defined(MAKE_NO_SCRIPTING_CONSOLE_) && !defined(MAKE_SCRIPTING_CONSOLE_)
#define MAKE_SCRIPTING_CONSOLE_ 1
#endif


#if defined(MAKE_SCRIPTING_CONSOLE_)
#if !defined(SCRIPTING_CONSOLE_OPTIMIZE_OUT)
#define SCRIPTING_CONSOLE_OPTIMIZE_OUT(code) code
#endif


#ifndef SCRIPTCONSOLE_H
#define SCRIPTCONSOLE_H

#include <string>

#include <irrlicht/irrString.h>

// forward declarations
class ScriptingModule;


class ScriptConsole
{
   public:
    ScriptConsole(const bool useStdoutForOutput_, ScriptingModule* const scriptModule_);
    ScriptConsole(const ScriptConsole&) = delete;
    ~ScriptConsole();

    std::string evalInputLine(const std::string input);
    irr::core::stringw evalInputLine(const irr::core::stringw input);


    ScriptConsole operator=(const ScriptConsole&) = delete;


   private:
    const bool useStdoutForOutput;
    ScriptingModule* scriptModule;


    // std::string getLastErrorAsString();
};

#endif // SCRIPTCONSOLE_H

#else

// all calls to to PyConsole are optimized away
// make the PyConsole the smalles possible type to use very little memory on things that still
// create one
typedef bool ScriptConsole;

#endif // else of: defined(MAKE_NO_SCRIPTING_CONSOLE_)
