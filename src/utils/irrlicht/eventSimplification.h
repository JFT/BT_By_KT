/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2018 Julius Tilly, Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <bitset>
#include <iostream>
#include <irrlicht/IEventReceiver.h>
#include <stdexcept>
#include <stdint.h>
#include <string>
#include <utility>

namespace BT
{
    namespace irrlichtHelpers
    {
        using UniqueEventValue_t = irr::u32;

        /// create unique value for a specific event with bitmagic for now only process mouse and
        /// Keyevents correctly
        /// From front to back of bitchain: first 4 bits contain the eventtype
        /// next 8 bits cotain the mouse or keyvalue
        constexpr UniqueEventValue_t toCombined(const irr::SEvent& event)
        {
            UniqueEventValue_t retVal =
                static_cast<UniqueEventValue_t>(static_cast<UniqueEventValue_t>(event.EventType) << 10);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
#pragma GCC diagnostic ignored "-Wswitch-enum"
            switch (event.EventType)
            {
                case irr::EET_MOUSE_INPUT_EVENT:
                {
                    retVal |= event.MouseInput.Event;
                }
                break;
                case irr::EET_KEY_INPUT_EVENT:
                {
                    // 256 KEYS!! --> 8 bits
                    retVal |= event.KeyInput.Key;
                }
                break;
                /// TODO: add more cases if we need to handle other events
                default:
                    retVal |= event.EventType; // just add eventtype at the end again ;)
                    break;
            }
#pragma GCC diagnostic pop

            return retVal;
        }

        /// Input eventType and corresponding eventEnum Descriptor
        /// e.g. EET_MOUSE_INPUT_EVENT and EMIE_LMOUSE_LEFT_UP
        constexpr UniqueEventValue_t toCombined(const irr::EEVENT_TYPE eventType, const uint8_t eventDescriptor)
        {
            return static_cast<UniqueEventValue_t>(static_cast<UniqueEventValue_t>(eventType << 10) |
                                                   static_cast<UniqueEventValue_t>(eventDescriptor));
        }

        constexpr irr::SEvent toEvent(UniqueEventValue_t combined)
        {
            irr::SEvent event = {};
            event.EventType = static_cast<irr::EEVENT_TYPE>((combined >> 10) & 0xFF);
            combined &= 0x1FF;
            switch (event.EventType)
            {
                case irr::EET_MOUSE_INPUT_EVENT:
                    event.MouseInput.Event = static_cast<decltype(event.MouseInput.Event)>(combined);
                    break;
                case irr::EET_KEY_INPUT_EVENT:
                    // 256 KEYS!! --> 8 bits
                    event.KeyInput.Key = static_cast<decltype(event.KeyInput.Key)>(combined);
                    break;
                /// TODO: add more cases if we need to handle other events
                default:
                    throw std::invalid_argument("unhandled event type " +
                                                std::to_string(static_cast<size_t>(event.EventType)));
            }

            return event;
        }

    } // namespace irrlichtHelpers

} // namespace BT
