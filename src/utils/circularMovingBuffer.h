/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CIRCULARMOVINGBUFFER
#define CIRCULARMOVINGBUFFER

#include <array>
#include <string>

/// @brief circular buffer which overwrites the oldest element if inserting a new one
/// @tparam size size of the buffer
/// @tparam T datatype to store in buffer
template <typename T, size_t bufferSize>
class CircularMovingBuffer
{
   public:
    CircularMovingBuffer()
        : elements()
    {
    }

    /// @brief get element which certain age
    /// @param age 0 for the newest element in the ciruclar buffer, size-1 for the oldest
    /// @return element at age age inside the circular buffer or oldest element if age >= size
    inline T& get(const size_t age)
    {
        if (age >= bufferSize)
        {
            return this->elements[(this->current + bufferSize - 1) % bufferSize];
        }
        return this->elements[(this->current - age) % bufferSize];
    }

    /// @brief put the newest element into the buffer overwriting the oldest element
    /// @param newestElement
    inline void push_back(const T& newestElement)
    {
        this->current = (this->current + 1) % bufferSize;
        this->elements[current] = newestElement;
    }

    std::string to_string()
    {
        std::string retVal = "(";
        for (size_t i = 0; i < bufferSize; i++)
        {
            retVal += std::to_string(this->get(i)) + ", ";
        }
        return retVal + ")";
    }

    inline constexpr size_t size() const { return bufferSize; }

   private:
    std::array<T, bufferSize> elements;

    size_t current = bufferSize;
};

#endif /* ifndef CIRCULARMOVINGBUFFER */
