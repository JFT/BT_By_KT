/*
Copyright (c) 2012 Jakob Progsch, Václav Zeman modified by Youka:
https://github.com/Youka/ThreadPool/blob/master/ThreadPool.hpp
Further modified for the BTanks Project by Julius Tilly
This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:
   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
   3. This notice may not be removed or altered from any source
   distribution.
*/

#ifndef THREAD_POOL_HPP
#define THREAD_POOL_HPP

// containers
#include <map>
#include <queue>
#include <vector>
// threading
#include <atomic>
#include <condition_variable>
#include <future>
#include <mutex>
#include <thread>
// utility wrappers
#include <functional>
#include <memory>
// exceptions
#include <iostream>
#include <stdexcept>

#include "Pow2Assert.h"
#include "debug.h"

// std::thread pool for resources recycling
class ThreadPool
{
   public:
    /// Create a Threadpool with n workerthreads
    /// @param threads_n number of workerthreads. Defaults to std::thread::hardware_concurrency() which can be zero
    /// if std::thread::hardware_concurrency() returns zero it will create 2 threads
    explicit ThreadPool(size_t threads_n = std::thread::hardware_concurrency())
        : workers()
        , tasks()
        , queue_mutex()
        , condition()
        , stop(false)
        , waitingThreads(0)
        , threadMapping()
    {
        if (!threads_n)
        {
            if (!std::thread::hardware_concurrency())
            {
                threads_n = 2;
            }
            else
            {
                throw std::invalid_argument("more than zero threads expected");
            }
        }

        this->workers.reserve(threads_n);
        for (; threads_n; --threads_n)
            this->workers.emplace_back([this] {
                while (true)
                {
                    std::function<void()> task;

                    {
                        std::unique_lock<std::mutex> lock(this->queue_mutex);
                        this->waitingThreads++;
                        this->condition.wait(lock,
                                             [this] { return this->stop || !this->tasks.empty(); });
                        if (this->stop && this->tasks.empty()) return;
                        this->waitingThreads--;
                        task = std::move(this->tasks.front());
                        this->tasks.pop();
                    }

                    task();
                }
            });

        size_t count = 0;
        for (auto& thread : workers)
        {
            threadMapping[thread.get_id()] = count;
            count++;
            debugOutLevel(Debug::DebugLevels::stateInit,
                          "mapping thread",
                          thread.get_id(),
                          "to array index",
                          this->threadMapping[thread.get_id()]);
        }
        threadMapping[std::this_thread::get_id()] = count;
        debugOutLevel(Debug::DebugLevels::stateInit,
                      "mapping main thread",
                      std::this_thread::get_id(),
                      "to array index",
                      this->threadMapping[std::this_thread::get_id()]);
    }


    // deleted copy&move ctors&assignments
    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;
    ThreadPool(ThreadPool&&) = delete;
    ThreadPool& operator=(ThreadPool&&) = delete;

    /// Add new work item to the pool - enqueue it
    /// @param f movable of function to excecute in thread -> avoid copy
    /// @param args arguments to pass to the function passed to excecute
    /// @return returntype/returnvalue of the specified function as a future which allows async operation
    template <class F, class... Args>
    std::future<typename std::result_of<F(Args...)>::type> enqueue(F&& f, Args&&... args)
    {
        using packaged_task_t = std::packaged_task<typename std::result_of<F(Args...)>::type()>;

        std::shared_ptr<packaged_task_t> task(
            new packaged_task_t(std::bind(std::forward<F>(f), std::forward<Args>(args)...)));
        auto res = task->get_future();
        {
            std::unique_lock<std::mutex> lock(this->queue_mutex);
            this->tasks.emplace([task]() { (*task)(); });
        }
        // TODO: Check if we need busy waiting
        // 1) somewhere the main thread needs to make sure that all tasks are executed. Might as well do it while creating the tasks.
        while (waitingThreads <= 0)
        {
            std::this_thread::yield();
        }

        this->condition.notify_one();
        return res;
    }

    // the destructor joins all threads
    virtual ~ThreadPool()
    {
        this->stop = true;
        this->condition.notify_all();
        for (std::thread& worker : this->workers) worker.join();
    }

    /// @brief number of worker threads
    /// @return size of the worker thread array. Does NOT include the main thread into this count.
    size_t getNumberOfWorkerThreads() const { return workers.size(); }

    /// @brief return the threadID of the specific worker index. (the reverse to 'getMappedIndex')
    /// @param workerIndex In contrast to 'getMappedIndex()' 'workerIndex' must be 0 < workerIndex <
    /// getNumberOfWorkerThreads() and can't return the threadID of the main thread.
    /// @return threadID of the worker at index 'workerIndex'.
    std::thread::id getThreadIDOfWorker(const size_t workerIndex) const
    {
        POW2_ASSERT(workerIndex < this->workers.size());
        return workers[workerIndex].get_id();
    }

    /// @brief get the index into the worker thread array from the threadID (std::this_thread::get_id())
    /// @param threadID
    /// @return index into the worker array of the specific thread. If threadID is the threadID of the main thread returns workers.size() (meaning an index one over the worker array)
    size_t getMappedIndex(const std::thread::id threadID) const
    {
        decltype(this->threadMapping)::const_iterator it = this->threadMapping.find(threadID);
        POW2_ASSERT(it != this->threadMapping.end());
        return it->second;
    }

   private:
    // need to keep track of threads so we can join them
    std::vector<std::thread> workers;
    // the task queue
    std::queue<std::function<void()>> tasks;

    // synchronization
    std::mutex queue_mutex;
    std::condition_variable condition;
    // workers finalization flag
    std::atomic_bool stop;
    std::atomic_int waitingThreads;

    // mapping from thread_id to 0,1,2,..
    std::map<const std::thread::id, size_t> threadMapping;
};

#endif // THREAD_POOL_HPP
