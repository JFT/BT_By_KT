/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VARIABLE_UNORDERED_QUICK_BUFFER_H
#define VARIABLE_UNORDERED_QUICK_BUFFER_H

#include <cstddef> // defines size_t
#include <vector>

// OOOOOOOOOOXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXOOOOOOOOOOOOOO
//           ^                               ^
//         begin                            end
//
// inserting after the end
// and before the beginning if trying to insert behind the vectors size


template <typename T>
class VariableUnorderedQuickBuffer
{
   public:
    VariableUnorderedQuickBuffer()
        : buffer()
    {
    }

    void push(const T& value)
    {
        // is there any place before the beginning of the internal vector?
        if (this->beginIndex > 0)
        {
            this->buffer[this->beginIndex - 1] = value;
            this->beginIndex--;
            return;
        }

        if (this->endIndex == this->buffer.size())
        {
            this->buffer.push_back(value); // let the interal vector do the growing itself
        }
        else
        {
            this->buffer[this->endIndex] = value;
        }

        this->endIndex++;
    }

    void pop() { this->beginIndex++; }
    void clear()
    {
        this->buffer.clear();
        this->beginIndex = 0;
        this->endIndex = 0;
    }

    T& front()
    {
        POW2_ASSERT(this->beginIndex < this->endIndex);
        return this->buffer[this->beginIndex];
    }

    const T& front() const
    {
        POW2_ASSERT(this->beginIndex < this->endIndex);
        return this->buffer[this->beginIndex];
    }

    size_t size() { return this->endIndex - this->beginIndex; }
    typename std::vector<T>::iterator begin() { return this->buffer.begin() + this->beginIndex; }
    typename std::vector<T>::iterator end() { return this->buffer.begin() + this->endIndex; }

   private:
    size_t beginIndex = 0;
    size_t endIndex = 0;

    std::vector<T> buffer;
};

#endif /* ifndef VARIABLE_UNORDERED_QUICK_BUFFER_H */
