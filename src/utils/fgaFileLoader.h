/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <vector>

#include <irrlicht/irrlicht.h>


struct FgaVectorField
{
    irr::core::array<irr::video::IImage*> createImageArrayFromVectorField(irr::video::IVideoDriver* driver);

    std::vector<irr::core::vector3df> vectorVoxels = std::vector<irr::core::vector3df>();
    irr::core::vector3d<irr::u32> resolution = irr::core::vector3d<irr::u32>();
    irr::core::vector3df minimum = irr::core::vector3df(), maximum = irr::core::vector3df();
    std::string name = "0";
};

///! read Files containing Vector Field Data in the fga format
///
/// fga format:
/// line #
/// 1:    Resolution3D (resolution in x, y,  z) of 3D Grid points
/// 2:    MinVector3D of Vectorfield box
/// 3:    MaxVector3D of Vectorfield box
/// 4-N:  Vectors3D - voxellike data;

class FgaFileLoader
{
   public:
    FgaFileLoader();

    FgaVectorField* getFgaVectorFieldFromFile(const irr::io::path& file);

   private:
    std::vector<double> getDoublesFromStringLine(std::string line);

    irr::core::vector3d<irr::u32> convertToVect3du(const std::vector<double>& inVect);
    irr::core::vector3df convertToVect3df(const std::vector<double>& inVect);
};