/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PRINTFUNCTIONS_H
#define PRINTFUNCTIONS_H

#include <irrlicht/SColor.h>
#include <irrlicht/dimension2d.h>
#include <irrlicht/irrString.h>
#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>
#include <ostream>

/****************************************************************************************
 * convesion functions: type_t a = conversionFunction(b)
 * **************************************************************************************/
template <typename T>
static irr::core::stringw vector3dToStringW(const irr::core::vector3d<T> vec)
{
    irr::core::stringw retVal(L"(");
    // stringw defines operator += for lots of types. stringw.append only works for wchar_t or other
    // stringw
    retVal += vec.X;
    retVal.append(L", ");
    retVal += vec.Y;
    retVal.append(L", ");
    retVal += vec.Z;
    retVal.append(L')');
    return retVal;
}

template <typename T>
static irr::core::stringw vector2dToStringW(const irr::core::vector2d<T> vec)
{
    irr::core::stringw retval(L"(");
    retval += vec.X;
    retval.append(L", ");
    retval += vec.Y;
    retval.append(L')');
    return retval;
}

template <typename T>
static irr::core::stringc dimension2dToStringC(const irr::core::dimension2d<T> dim)
{
    irr::core::stringc retVal("(");
    retVal += dim.Width;
    retVal.append("x");
    retVal += dim.Height;
    retVal.append(')');
    return retVal;
}

irr::core::stringw colorToStringW(const irr::video::SColor& color);

irr::core::stringw boolToStringW(const bool b);

template <typename T>
std::ostream& operator<<(std::ostream& out, const irr::core::vector2d<T>& v)
{
    out << "[" << v.X << ", " << v.Y << "]";
    return out;
}


#endif // PRINTFUNCTIONS_H
