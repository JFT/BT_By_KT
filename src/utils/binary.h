/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BINARY_H
#define BINARY_H

#include <fstream>

#include <irrlicht/irrTypes.h>

#include "static/error.h" // defines ErrCode

namespace Binary
{
    /// @brief wrapper function to write binary data to a file without having to write
    /// reinterpret_cast<const char*> and sizeof() for everything
    /// implemented only for types which are save to write/read to/from binary files on multiple
    /// platforms (because e.g. sizeof(size_t) isn't the same on win/linux)
    void writeBinary(std::fstream& file, const uint32_t data);
    void writeBinary(std::fstream& file, const irr::f32 data);
    void writeBinary(std::fstream& file, const int32_t data);

    /// @brief wrapper functions to read binary data from a file without having to write
    /// reinterpret_cast<char*> and sizeof() for everything
    /// implemented only for types which are save to write/read to/from binary files on multiple
    /// platforms (because e.g. sizeof(size_t) isn't the same on win/linux)
    /// @param defaultValue if reading fails 'data' will be set to this value
    /// @return
    ErrCode readBinary(std::fstream& file, uint32_t& data, const uint32_t defaultValue);
    ErrCode readBinary(std::fstream& file, irr::f32& data, const irr::f32 defaultValue);
    ErrCode readBinary(std::fstream& file, int32_t& data, const int32_t defaultValue);
} // namespace Binary

#endif /* ifndef BINARY_H */
