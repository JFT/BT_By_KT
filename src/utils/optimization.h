/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

// this code is loosely based on platform.h from the bx library
// http://github.com/bkaradzic/bx
// http://sourceforge.net/apps/mediawiki/predef/index.php?title=Compilers
#if defined(__GCC__)
#define MACRO_LIKELY(x) __builtin_expect((x), 1)
#define MACRO_UNLIKELY(x) __builtin_expect((x), 0)
#else
#define MACRO_LIKELY(x) x
#define MACRO_UNLIKELY(x) x
// TODO: lookup these macros for clang/VS
//#elif defined(_MSC_VER)
//#elif defined(__clang__)
#endif
