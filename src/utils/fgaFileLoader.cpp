/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "fgaFileLoader.h"

#include <fstream>
#include <sstream>
#include <string>

#include "debug.h"
#include "static/error.h"

FgaFileLoader::FgaFileLoader() {}

FgaVectorField* FgaFileLoader::getFgaVectorFieldFromFile(const irr::io::path& file)
{
    std::ifstream ifs;

    ifs.open(file.c_str(), std::ifstream::in);

    if (ifs.good())
    {
        std::string line;
        std::getline(ifs, line);
        FgaVectorField* vectField = new FgaVectorField();
        vectField->name = file.c_str();
        // first line is dimension
        vectField->resolution = convertToVect3du(getDoublesFromStringLine(line));

        // second and third lines are minimum and maximum of the vectorfield box
        std::getline(ifs, line);
        vectField->minimum = convertToVect3df(getDoublesFromStringLine(line));
        std::getline(ifs, line);
        vectField->maximum = convertToVect3df(getDoublesFromStringLine(line));
        // the rest is the vectorfield data (each line is a 3dvector)
        while (std::getline(ifs, line))
        {
            vectField->vectorVoxels.push_back(convertToVect3df(getDoublesFromStringLine(line)));
        }
        if (vectField->vectorVoxels.size() !=
            (vectField->resolution.X * vectField->resolution.Y * vectField->resolution.Z))
        {
            Error::errContinue("Failed to load Fga File: ", file.c_str(), " : lineCount does not fit Resolution of Field");
            delete vectField;
            return nullptr;
        }
        debugOutLevel(Debug::DebugLevels::stateInit,
                      "Loaded Fga File with ",
                      vectField->vectorVoxels.size(),
                      " vectorVoxels");

        return vectField;
    }
    else
    {
        Error::errContinue("Failed to open Fga File: ", file.c_str());
        return nullptr;
    }
}

std::vector<double> FgaFileLoader::getDoublesFromStringLine(const std::string line)
{
    std::stringstream ss(line);
    std::string field;
    std::vector<double> retVector;
    while (std::getline(ss, field, ','))
    {
        // for each field we wish to convert it to a double
        std::stringstream fs(field);
        double f = 0.0;
        fs >> f;
        retVector.push_back(f);
    }

    return retVector;
}

irr::core::vector3d<irr::u32> FgaFileLoader::convertToVect3du(const std::vector<double>& inVect)
{
    if (inVect.size() != 3)
    {
        Error::errContinue("Failed to read convert To Vect3du from fga line");
        return irr::core::vector3d<irr::u32>();
    }

    return irr::core::vector3d<irr::u32>(int(inVect[0]), int(inVect[1]), int(inVect[2]));
}

irr::core::vector3df FgaFileLoader::convertToVect3df(const std::vector<double>& inVect)
{
    if (inVect.size() != 3)
    {
        Error::errContinue("Failed to read convert To Vect3df from fga line");
        return irr::core::vector3df();
    }

    return irr::core::vector3df(float(inVect[0]), float(inVect[1]), float(inVect[2]));
}

irr::core::array<irr::video::IImage*>
FgaVectorField::createImageArrayFromVectorField(irr::video::IVideoDriver* driver)
{
    irr::core::array<irr::video::IImage*> retImagArr;
    for (size_t z = 0; z < resolution.Z; ++z)
    {
        retImagArr.push_back(driver->createImage(irr::video::ECOLOR_FORMAT::ECF_A32B32G32R32F,
                                                 irr::core::dimension2du(resolution.X, resolution.Y)));
        float* data = static_cast<float*>(retImagArr[z]->getData());

        for (size_t y = 0; y < resolution.Y; ++y)
        {
            for (size_t x = 0; x < resolution.X; ++x)
            {
                data[4 * (x + y * resolution.X)] =
                    vectorVoxels[x + y * resolution.X + z * (resolution.X * resolution.Y)].X;
                data[4 * (x + y * resolution.X) + 1] =
                    vectorVoxels[x + y * resolution.X + z * (resolution.X * resolution.Y)].Y;
                data[4 * (x + y * resolution.X) + 2] =
                    vectorVoxels[x + y * resolution.X + z * (resolution.X * resolution.Y)].Z;
                data[4 * (x + y * resolution.X) + 3] =
                    1.f; // We could pack one more float per pixel - for now just set to 1
            }
        }
    }

    return retImagArr;
}
