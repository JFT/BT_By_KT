/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "binary.h"

/// @brief wrapper function to write binary data to a file without having to write
/// reinterpret_cast<const char*> and sizeof() for everything
/// implemented only for types which are save to write/read to/from binary files on multiple
/// platforms (because e.g. sizeof(size_t) isn't the same on win/linux)
void Binary::writeBinary(std::fstream& file, const uint32_t data)
{
    file.write(reinterpret_cast<const char*>(&data), sizeof(data));
}

void Binary::writeBinary(std::fstream& file, const irr::f32 data)
{
    file.write(reinterpret_cast<const char*>(&data), sizeof(data));
}

void Binary::writeBinary(std::fstream& file, const int32_t data)
{
    file.write(reinterpret_cast<const char*>(&data), sizeof(data));
}

ErrCode Binary::readBinary(std::fstream& file, uint32_t& data, const uint32_t defaultValue)
{
    file.read(reinterpret_cast<char*>(&data), sizeof(data));
    if (file.fail())
    {
        data = defaultValue;
        return ErrCodes::GENERIC_ERR;
    }
    else
    {
        return ErrCodes::NO_ERR;
    }
}

ErrCode Binary::readBinary(std::fstream& file, irr::f32& data, const irr::f32 defaultValue)
{
    file.read(reinterpret_cast<char*>(&data), sizeof(data));
    if (file.fail())
    {
        data = defaultValue;
        return ErrCodes::GENERIC_ERR;
    }
    else
    {
        return ErrCodes::NO_ERR;
    }
}

ErrCode Binary::readBinary(std::fstream& file, int32_t& data, const int32_t defaultValue)
{
    file.read(reinterpret_cast<char*>(&data), sizeof(data));
    if (file.fail())
    {
        data = defaultValue;
        return ErrCodes::GENERIC_ERR;
    }
    else
    {
        return ErrCodes::NO_ERR;
    }
}
