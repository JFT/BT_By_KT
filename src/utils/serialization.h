/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef BT_UTILS_SERIALIZATION_H
#define BT_UTILS_SERIALIZATION_H

#include <irrlicht/irrString.h>
#include <string>
#include <typeinfo> // typeid()
#include <vector>
#include "serializationTypes.h"
#include "../ecs/componentID.h" // used to determine if special component serialization calls need to be used

namespace Serialization
{
    class serialization_exception : public std::exception
    {
       public:
        explicit serialization_exception(const std::string& what_arg)
            : m_what(what_arg)
        {
        }

        virtual const char* what() const noexcept { return this->m_what.c_str(); }

       protected:
        std::string m_what;
    };

    class deserialization_version_mismatch : public serialization_exception
    {
       public:
        deserialization_version_mismatch(const SerializationVersion_t current, const SerializationVersion_t other)
            : serialization_exception("can't deserialize data from version " + std::to_string(other) +
                                      "(current version is " + std::to_string(current) + ")")
        {
        }
    };

    class serialization_buffer_overrun : public serialization_exception
    {
       public:
        serialization_buffer_overrun(const size_t readSize,
                                     const size_t pos,
                                     const size_t bufferSize,
                                     const std::string& what_arg)
            : serialization_exception("couldn't read " + std::to_string(readSize) + " bytes past " +
                                      std::to_string(pos) + " in buffer size " +
                                      std::to_string(bufferSize) + " while reading " + what_arg)
        {
        }

        serialization_buffer_overrun& append(const std::string& what_arg)
        {
            this->m_what += what_arg;
            return *this;
        }
    };

    // Components often haven std::is_trivially_copyable<T> false but are still
    // trivially copyable (e.g. Position (vector3df))
    // Some on the other hand are trivially copyable (e.g. Graphic)
    // but shouldn't be serialized that way because they contain pointers
    // Thus there needs to be a way to detect if a type to be serialized is a component
    // and switch to an appropiate serialization method.
    // This is done using SFINAE [https://en.wikipedia.org/wiki/Substitution_failure_is_not_an_error]
    // and std::enable_if.
    // using SFIANE we detect the presence of ComponentID<T>::CID (which is undefined if T isn't a
    // component) and switch a specific serialization function on/off

    // template helper functions for component detection (TODO: remove if we switch to C++17)
    template <typename... Ts>
    struct make_void
    {
        typedef void type;
    };
    template <typename... Ts>
    using void_t = typename make_void<Ts...>::type;

    class DefaultType
    {
    };
    class ComponentType
    {
    };
    class VectorType
    {
    };

    // default template declaration : T isn't a component
    template <typename T, class Enabled = void>
    struct SerializationType
    {
        typedef DefaultType type;
    };

    // SFIANE: this template only exists if Components::ComponentID<T>::CID is defined (otherwise
    // 'decltype' doesn't work) otherwise the default is used
    template <typename T>
    struct SerializationType<T, void_t<decltype(Components::ComponentID<T>::CID)>>
    {
        typedef ComponentType type;
    };

    // SFIANE: this template only exists if T has a member value_type, and is actually a
    // std::vector<T::value_type> (this is done by the enable_if_t)
    template <typename T>
    struct SerializationType<T, void_t<std::enable_if_t<std::is_same<T, std::vector<typename T::value_type>>::value>>>
    {
        typedef VectorType type;
    };

    /// @brief some types are trivially copyable for serialization purposes but don't fullfill
    /// std::is_trivially_copyable This override ensures they can still be serialized correctly.
    /// Usage: define in some header:
    /// template<> struct Is_trivially_copyable_override<T> : std::true_type {};
    /// @tparam T
    template <typename T>
    struct Is_trivially_copyable_override : std::false_type
    {
    };

    /// @brief Serialize 'value' and append the serialized bytes to 'buffer'.
    /// This is the default version of this function (only serializing trivially copyable values),
    /// enabled by std::enable_if if 'T' is a non-component, non-vector type
    /// @tparam T type to be serialized
    /// @param buffer buffer to append 'value' to. Will be resized to make space for the value.
    /// @param value
    template <typename T>
    typename std::enable_if<std::is_same<typename SerializationType<T>::type, DefaultType>::value, void>::type static serialize(
        std::vector<Buffer_t>& buffer, const T& value)
    {
        static_assert(std::is_trivially_copyable<T>::value or Is_trivially_copyable_override<T>::value,
                      "Supplied type must be trivially copyable (include the correct serialization "
                      "header or define Is_trivially_copyable_override!");
        const size_t oldSize = buffer.size();
        buffer.resize(oldSize + sizeof(T));
        *reinterpret_cast<T* const>(&buffer[oldSize]) = value;
    }

    // forward declaration of component (de)serialization function.
    // if compilation produces undefined references you forgot to include
    // ecs/componentSerialization.h"
    namespace ComponentSerialization
    {
        template <typename T>
        static void serialize(std::vector<Buffer_t>& buffer, const T& value);
        template <typename T>
        static T deserialize(const std::vector<Buffer_t>& buffer, size_t& pos);
    } // namespace ComponentSerialization

    /// @brief Serialize 'value' and append the serialized bytes to 'buffer'.
    /// This is the Component serialization version of this function,
    /// enabled by std::enable_if if 'T' is a component type (based on the existence of
    /// 'ComponentID<T>::CID') passing call to specialized serialization function
    /// @tparam T type to be serialized
    /// @param buffer buffer to append 'value' to. Will be resized to make space for the value.
    /// @param value
    template <typename T>
    typename std::enable_if<std::is_same<typename SerializationType<T>::type, ComponentType>::value, void>::type static serialize(
        std::vector<Buffer_t>& buffer, const T& value)
    {
        Serialization::ComponentSerialization::serialize<T>(buffer, value);
    }

    /// @brief Serialize 'values' and append the serialized bytes to 'buffer'.
    /// This is the vector version of this function (only serializing trivially copyable vector
    /// value_types), enabled by std::enable_if if 'T' is a std::vector
    /// @tparam T type to be serialized
    /// @param buffer buffer to append 'values' to. Will be resized to make space for the values.
    /// @param values the std::vector to serialize.
    template <typename T>
    typename std::enable_if<std::is_same<typename SerializationType<T>::type, VectorType>::value, void>::type static serialize(
        std::vector<Buffer_t>& buffer, const T& values)
    {
        serialize<uint32_t>(buffer, static_cast<uint32_t>(values.size()));
        for (const auto& item : values)
        {
            serialize<typename T::value_type>(buffer, item);
        }
    }

    /// @brief Serialize 'value' at specified position in 'buffer'
    /// OVERWRITES data already there, expanding the buffer if it isn't large enough.
    /// This can be used to write size information which isn't known before serializing the data.
    /// @tparam T type to be serialized
    /// @param buffer buffer to write 'value' to. Will be resized to make space for the value.
    /// @param pos first byte of the buffer the value should be written at.
    /// 'serialize(buffer, value)' is equivalent to 'serializeAt(buffer, buffer.size(), value)'
    /// @param value
    template <typename T>
    static void serializeAt(std::vector<Buffer_t>& buffer, const size_t pos, const T& value)
    {
        static_assert(std::is_trivially_copyable<T>::value or Is_trivially_copyable_override<T>::value,
                      "Supplied type must be trivially copyable (include the correct serialization "
                      "header or define Is_trivially_copyable_override!");
        if (pos + sizeof(T) > buffer.size())
        {
            buffer.resize(buffer.size() + sizeof(T));
        }
        *reinterpret_cast<T* const>(&buffer[pos]) = value;
    }

    /// @brief deserialize specified type out of the buffer at 'pos'.
    /// Throws std::seraialization_exception if encountering an error
    /// (e.g. if buffer isn't large enough)
    /// @tparam T type to be deserialized
    /// @param buffer
    /// @param pos byte offset into 'buffer' where the data to be interpreted as 'T' begins.
    /// 'pos' will be advanced by amount of bytes read after succesfull deserialization.
    /// @return the deserialized value.
    template <typename T>
    typename std::enable_if<std::is_same<typename SerializationType<T>::type, DefaultType>::value, T>::type static deserialize(
        const std::vector<Buffer_t>& buffer, size_t& pos)
    {
        static_assert(std::is_trivially_copyable<T>::value or Is_trivially_copyable_override<T>::value,
                      "Supplied type must be trivially copyable (include the correct serialization "
                      "header or define Is_trivially_copyable_override!");
        if (pos + sizeof(T) > buffer.size())
        {
            throw serialization_buffer_overrun(1, pos, buffer.size(), std::string(typeid(T).name()) + " unknown type in serialization.h");
        }
        const T value = *reinterpret_cast<const T*>(&buffer[pos]);
        pos += sizeof(T);
        return value;
    }

    /// @brief deserialization template overload for components (done by testing if
    /// ComponendID<T>::CID exists) passing call to specialized deserialization function
    /// see: normal 'deserialize' call
    template <typename T>
    typename std::enable_if<std::is_same<typename SerializationType<T>::type, ComponentType>::value, T>::type static deserialize(
        const std::vector<Buffer_t>& buffer, size_t& pos)
    {
        return Serialization::ComponentSerialization::deserialize<T>(buffer, pos);
    }

    /// @brief deserialization template overload for std::vectors, deserializes size and values.
    /// see: normal 'deserialize' call
    template <typename T>
    typename std::enable_if<std::is_same<typename SerializationType<T>::type, VectorType>::value, T>::type static deserialize(
        const std::vector<Buffer_t>& buffer, size_t& pos)
    {
        const auto size = static_cast<size_t>(deserialize<uint32_t>(buffer, pos));
        T values;
        values.reserve(size);
        for (size_t i = 0; i < size; ++i)
        {
            const typename T::value_type value = deserialize<typename T::value_type>(buffer, pos);
            values.emplace_back(value);
        }
        return values;
    }

    // specialization for size_t because sizeof(size_t) is platform (especially 32/64 bit) dependent
    // (serialized as uint32_t instead)
    template <>
    void serialize<size_t>(std::vector<Buffer_t>& buffer, const size_t& value);
    template <>
    size_t deserialize<size_t>(const std::vector<Buffer_t>& buffer, size_t& pos);

    // specialization for std::string
    template <>
    void serialize<std::string>(std::vector<Buffer_t>& buffer, const std::string& value);
    template <>
    std::string deserialize<std::string>(const std::vector<Buffer_t>& buffer, size_t& pos);

    template <>
    void serialize<irr::core::stringw>(std::vector<Buffer_t>& buffer, const irr::core::stringw& value);
    template <>
    irr::core::stringw deserialize<irr::core::stringw>(const std::vector<Buffer_t>& buffer, size_t& pos);

    static_assert(sizeof(Buffer_t) == 1, "BufferType must be a byte-sized type!");
} // namespace Serialization

#include "serialization.specializations.tpp"

#endif /* ifndef BT_UTILS_SERIALIZATION_H */
