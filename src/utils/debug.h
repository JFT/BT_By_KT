/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEBUG_H
#define DEBUG_H


#if defined(MAKE_DEBUG_)
// the normal debugOut will still always print
#define debugOut(...) Debug::debugOutHandler(0, __FILE__, __LINE__, __VA_ARGS__)
#define debugOutLevel(level, ...) Debug::debugOutHandler(level, __FILE__, __LINE__, __VA_ARGS__)
#define loadDebugLevels(...) Debug::loadDebugLevelsInternal()
#define DEBUG_ONLY_(x) x
#define BT_RELEASE_ONLY_(x) ;
#else
// all public methods must be overwritten by no-operations (no-ops) to prevent copmiler errors in
// release mode
// this way the debug code will be completely optimized away
#define debugOut(...) ;
#define debugOutLevel(...) ;
#define loadDebugLevels(...) ;
#define DEBUG_ONLY_(x)
#define BT_RELEASE_ONLY_(x) x
#endif

#if defined(MAKE_DEBUG_) // generate this code only if we are in the debug build

#include <algorithm> // std::replace
#include <fstream>   // needed to load the debugLevel file
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string> // std::getline, std::stoul
#include <vector>

#include "static/error.h"
#include "static/repr.h" // to print parsing errors in debugLevel file

class Debug
{
   public:
    // this debugLevel is used to make sure that certain debug messages will always be printed (e.g.
    // when loading the debugLevel file)
    static constexpr unsigned int maxDebugLevel = 100;

    // an enum to hold some debugLevelList for simple use
    enum DebugLevels
    {
        always = 0,
        stateHandling = 2,
        stateInit = 5,
        networking = 25,
        firstOrderLoop = 30,
        secondOrderLoop = 50,
        onEvent = 80,
        updateLoop = 90,
        maxLevel = maxDebugLevel
    };

    template <typename... Args>
    static std::string generateString(std::stringstream& ss, const Args... args)
    {
        // using a fold-expression of type
        // '(pack op ...)' where
        // * pack = (ss << args << ' ')
        // * op = , (the comma operator)
        //
        // which expands to
        // (ss << args_1 << ' ') , (((ss << args_2 << ' ') , (ss << args_3 << ' ')), ...)
        // and because the comma operator executes its inputs left-before-right
        // (ss << args_1 << ' ') is executed before (ss << args_2 << ' '), etc...
        // resulting in a total output of
        // ss << args_1 << ' ' << args_2 << ' ' << args_3 << ' ' ...
        // argument in 'args'
        ((ss << args << ' '), ...);
        ss << std::endl;
        return ss.str();
    }

    // variadic template method which calls itself recursively until the base template method is
    // reached
    template <typename... Args>
    static void debugOutHandler(const unsigned int level, const char* file, const int line, Args... args)
    {
        std::string filename("");
        if (file != nullptr)
        {
            filename = Debug::stripPath(std::string(file));
        }
        if (!Debug::shouldPrint(level, filename))
        {
            return;
        }

        std::stringstream ss;
        ss << "DEBUG:";
        // left-pad the level to be at least 2 characters long,
        // so the levels are formatted below each other when viewing the log
        ss << std::setw(2) << level << std::setw(0);
        ss << ": " << filename << ":" << line << ": ";
        // actual printing of the message
        std::cerr << generateString(ss, args...);
    }

    // read the file 'debugLevel' which contains the golbal debug level and the debug levels for
    // individual files
    /* expected format of the file:
    -----------------------------------------
     * globalDebugLevel (unsigned int)
     * filename1 debugLevelForFilename1
     * filename2 debugLevelForFilename2
     * ...
    -----------------------------------------
     *
     * filename can also be a folder e.g.
     * utils/
     *
     * the full path of the source file will be matched against the filename.
     *
     * example:
    -----------------------------------------
     * 0
     * gamemap.cpp 1
     * utils/ 5
    ----------------------------------------- */
    // all printing in this function must be done with level 0 to make sure it is always printed
    static void loadDebugLevelsInternal()
    {
        debugOutLevel(0, "loadDebugLevelsInternal()");
        std::ifstream debugFile;
        debugFile.open("debugLevel", std::ifstream::in);

        if (debugFile.fail())
        {
            // assume maximum debug level but warn about it
            debugOutLevel(0, "no file 'debugLevel' found. Assuming maximum debug level=", Debug::maxDebugLevel);
            debugFile.close();
            globalDebugLevel = Debug::maxDebugLevel;
            return;
        }

        bool onFirstLine = true;
        while (debugFile.good())
        {
            std::string line("");
            std::getline(debugFile, line);
            if (line.size() == 0) // ignore empty lines
            {
                continue;
            }
            if (onFirstLine)
            {
                // try to parse the line as the global debug level
                onFirstLine = false;
                try
                {
                    // ignore lines starting with a '#'
                    if (line.find('#') == 0)
                    {
                        continue;
                    }
                    globalDebugLevel = static_cast<unsigned int>(std::stoul(line));
                    if (globalDebugLevel > Debug::maxDebugLevel)
                    {
                        Error::errTerminate(
                            "Reading file 'debugLevel'. The given global debugLevel of",
                            globalDebugLevel,
                            "is higher than the current maximal possible debugLevel of",
                            Debug::maxDebugLevel);
                    }
                    continue;
                }
                catch (std::exception&)
                {
                    globalDebugLevel = 0;
                    debugOutLevel(0,
                                  "first line in 'debugLevel' doesn't set a global debug level. "
                                  "Setting global debugLevel = 0");
                }
            }

            // ignore lines starting with a '#'
            if (line.find('#') == 0)
            {
                continue;
            }
            // code to parse filenameX debugLevelForFilenameX
            if (line.find(std::string(" ")) == std::string::npos)
            {
                debugOutLevel(0, "couldn't parse string", repr<std::string>(line), "as filenameX(string) debugLevel(unsigned int)");
                continue;
            }
            std::string filename = line.substr(0, line.find(" "));
            unsigned int fileDebugLevel = 0;
            try
            {
                fileDebugLevel = static_cast<unsigned int>(stoul(line.substr(line.find(" "))));
                if (fileDebugLevel > Debug::maxDebugLevel)
                {
                    Error::errTerminate("Reading file 'debugLevel': The given debugLevel =",
                                        fileDebugLevel,
                                        "for the file",
                                        repr<std::string>(filename),
                                        "is higher than the current maximal possible debugLevel =",
                                        Debug::maxDebugLevel);
                }
            }
            catch (std::exception&)
            {
                debugOutLevel(0, "couldn't parse unsigned int in string", line, "as 'filenameX(string) debugLevel(unsigned int)");
                continue;
            }
            // add the filename and debugLevel to the arrays
            Debug::debugFileList.push_back(filename);
            Debug::debugLevelList.push_back(fileDebugLevel);
        } // while(debugFile.good())

        // now print the read-in output
        debugOutLevel(DebugLevels::always, "global debug level =", globalDebugLevel);
        for (size_t i = 0; i < debugFileList.size(); i++)
        {
            if (globalDebugLevel > debugLevelList[i])
            {
                debugOutLevel(DebugLevels::always,
                              "filename:",
                              repr<std::string>(debugFileList[i]),
                              "debugLevel =",
                              debugLevelList[i],
                              "(overwrites global debugLevel)");
            }
            else
            {
                debugOutLevel(DebugLevels::always, "filename:", repr<std::string>(debugFileList[i]), "debugLevel =", debugLevelList[i]);
            }
        }

        debugFile.close();
    }

   private:
    static unsigned int globalDebugLevel;
    static std::vector<std::string> debugFileList;
    static std::vector<unsigned int> debugLevelList;

    // compare the given debugLevel in the message with the debugLevel for that filename (if any is
    // given) and with the global debugLevel
    static bool shouldPrint(unsigned int debugLevel, const std::string& filename)
    {
        if (debugLevel > maxDebugLevel)
        {
            // clamp the debugLevel to the maximum possible debug level
            // this way if any of the configured debug levels are the max debug level these messages
            // will also be printed
            debugLevel = maxDebugLevel;
        }
        // actual checks if the message should be printed
        for (unsigned int i = 0; i < debugFileList.size(); i++)
        {
            if (filename.find(debugFileList[i]) != std::string::npos)
            {
                // there is a specific debug level for this file -> check it (and allow overwriting
                // of the global one)
                return debugLevel <= debugLevelList[i];
            }
        }
        // no special rule for this file -> compare against the global debugLevel
        return debugLevel <= globalDebugLevel;
    }

    // remove everything up to and including /src/ from debug path output
    static std::string stripPath(const std::string& filename)
    {
// get beginnig of the current file in the full path
#if defined(__unix__)
        const std::string srcPath("/src/");
#elif defined(__MINGW32__)
        // mingw-w64's g++ uses the pathname provided to the Compiler as _FILE__
        // so srcpath can be both "/src/" and "\\src\\"
        // we look for both in the string and use the one we find
        std::string srcPath("");
        if (filename.find("\\src\\") != std::string::npos)
        {
            srcPath = std::string("\\src\\");
        }
        else
        {
            srcPath = std::string("/src/");
        }
#elif defined(__WIN32__) || defined(WIN32)
        const std::string srcPath("\\src\\");
#endif
        const size_t pos = filename.find(srcPath) + srcPath.size();
        return filename.substr(pos);
    }
};

#endif // MAKE_DEBUG_

#endif // DEBUG_H
