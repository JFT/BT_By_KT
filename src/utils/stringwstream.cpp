/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stringwstream.h"

std::ostream& operator<<(std::ostream& stream, const std::wstring& str)
{
    stream << std::string(str.begin(), str.end());
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const irr::core::stringw& str)
{
    stream << std::wstring(str.c_str());
    return stream;
}

// without this ifndef the mapGenerator would need to link against raknet
#ifndef MAKE_MAPGENERATOR_
std::ostream& operator<<(std::ostream& stream, const RakNet::RakWString& str)
{
    stream << std::wstring(str.C_String());
    return stream;
}
#endif
