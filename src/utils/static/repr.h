/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REPR_H
#define REPR_H

#include <iostream>

// repr is used to add ' to the printing of any type so that
// std::cout << repr<const char*>("test123");
// prints:
// 'test123'
template <typename T>
class repr
{
   public:
    const T& tRef;

    explicit repr(const T& t)
        : tRef(t)
    {
    }
};

// to enable printing of that type we overload the stream operator for the repr<T> type
// note that the stream operator for the original type still has to exist for this to work
template <typename T>
std::ostream& operator<<(std::ostream& stream, const repr<T>& rpr)
{
    stream << "'" << rpr.tRef << "'";
    return stream;
}

#endif // REPR_H
