/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONVERSIONS_H
#define CONVERSIONS_H

#include <string>

#include <irrlicht/irrString.h>
#include <irrlicht/vector3d.h>

/****************************************************************************************
 * implicit conversions: type_t a = b
 * **************************************************************************************/

// define implicit conversion of vector3df to stringw -> useful for printing it everywhere
/*irr::core::vector3df::operator irr::core::stringw() const
{
    irr::core::stringw retVal;
    retVal.append(L"vector3df[");
    retVal.append(this->X);
    retVal.append(L' ');
    retVal.append(this->Y);
    retVal.append(L' ');
    retVal.append(this->Z);
    retVal.append(L']');
    return retVal;
}*/

/****************************************************************************************
 * convesion functions: type_t a = conversionFunction(b)
 * **************************************************************************************/


static std::string convertToString(irr::core::stringw str)
{
    std::wstring wstr(str.c_str());
    return std::string(wstr.begin(), wstr.end());
}

static irr::core::stringc convertToStringC(irr::core::stringw str)
{
    // really stupid conversion stringw -> wstring -> string -> stringc
    return irr::core::stringc(convertToString(str).c_str());
}

#endif // CONVERSIONS_H
