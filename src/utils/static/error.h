/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ERROR_H
#define ERROR_H

#include <cstdlib> // std::exit
#include <iostream>
#include "../stringwstream.h"

#include "../Pow2Assert.h" // defines macro POW2_HALT

#define errContinue(...) errContinueHandler(__FILE__, __LINE__, __VA_ARGS__)
#define errTerminate(...) errTerminateHandler(__FILE__, __LINE__, __VA_ARGS__)
#define errTerminateCode(...) errTerminateCodeHandler(__FILE__, __LINE__, __VA_ARGS__)

typedef int ErrCode;
enum ErrCodes
{
    NO_ERR = 0,
    GENERIC_ERR = 1
};

// varidic templates used from here:
// https://stackoverflow.com/questions/1657883/variable-number-of-arguments-in-c
// these allow errX to be called with any number of arguments (at least 1) and process them
// recursively
class Error
{
    // separation into public and private functions because we want to prepend "ERROR" before the
    // message. But only on the first argument of the function
   public:
    // the varidic templates 'Args...' also handle the case of only one message to print
    // in which case Args... is simply not there (meaning the called function has one argument less)
    template <typename T, typename... Args>
    static void errContinueHandler(const char* file, const int line, const T& message, Args... args)
    {
        std::string filename("");
        if (file != nullptr)
        {
            filename = std::string(file);
        }

        std::cerr << "ERROR: " << filename.c_str() << ":" << line << ": " << message << ' ';
        errOut(args...);
    }

    template <typename T, typename... Args>
    [[noreturn]] static void errTerminateHandler(const char* file, const int line, const T& message, Args... args)
    {
        std::string filename("");
        if (file != nullptr)
        {
            filename = std::string(file);
        }

        std::cerr << "FATAL ERROR: " << filename << ":" << line << ": " << message << ' ';
        errOutTerminate(EXIT_FAILURE, args...);
    }

    // function to be able to set the exitCode on failure
    template <typename T, typename... Args>
    [[noreturn]] static void
    errTerminateCodeHandler(const char* file, const int line, const int exitCode, const T& message, Args... args)
    {
        if (exitCode == 0)
        {
            errTerminate(file, line, "FATAL ERROR IN ERROR HANDLING: exitCode = 0 for message", message, args...);
        }
        std::string filename("");
        if (file != nullptr)
        {
            filename = std::string(file);
        }
        std::cerr << "FATAL ERROR: " << filename << ":" << line << ": " << message << ' ';
        errOutTerminate(exitCode, args...);
    }

   private:
    // private functions for printing out the rest of the message. Because errContinue and
    // errTerminate use the same function we need to pass a possible exitCode

    static void errOut()
    {
        // this function will be called if all arguments args... have been consumed,
        // which means everything has been printed.
        // except the newline
        std::cerr << std::endl;
    }

    template <typename T, typename... Args>
    static void errOut(const T& message, Args... args)
    {
        std::cerr << message << " ";
        // recursively call ourselves until args... is empty and we call errOut()
        errOut(args...);
    }

    // we need to re-implement these functions with the [[noreturn]] attribute to get rid of
    // compiler warnings
    [[noreturn]] static void errOutTerminate(const int exitCode)
    {
        errOut();
        POW2_HALT();
        std::exit(exitCode);
    }

    template <typename T, typename... Args>
    [[noreturn]] static void errOutTerminate(const int exitCode, const T& message, Args... args)
    {
        std::cerr << message << ' ';
        // recursively call ourselves until args... is empty and we call errOutTerminate(exitCode)
        errOutTerminate(exitCode, args...);
    }
};

#endif // ERROR_H
