/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef AABBBOX2D_H
#define AABBBOX2D_H

#include <algorithm> // std::min/max
#include <cmath>     //std::abs
#include <cstdlib>
#include <irrlicht/vector2d.h>

template <typename T>
class aabbox2d
{
   public:
    irr::core::vector2d<T> MaxEdge;
    irr::core::vector2d<T> MinEdge;

    explicit aabbox2d(const irr::core::vector2d<T>& initial)
        : MaxEdge(initial)
        , MinEdge(initial)
    {
    }

    aabbox2d(const irr::core::vector2d<T>& minEdge, const irr::core::vector2d<T>& maxEdge)
        : MaxEdge(maxEdge)
        , MinEdge(minEdge)
    {
    }

    inline void reset(const irr::core::vector2d<T>& newBase)
    {
        MinEdge = newBase;
        MaxEdge = newBase;
    }

    inline void reset(const irr::core::vector2d<T>& minEdge, const irr::core::vector2d<T>& maxEdge)
    {
        MinEdge = minEdge;
        MaxEdge = maxEdge;
    }

    inline void addInternalBox(const aabbox2d& box)
    {
        this->addInternalPoint(box.MinEdge);
        this->addInternalPoint(box.MaxEdge);
    }

    inline void addInternalPoint(const irr::core::vector2d<T>& p)
    {
        MinEdge.X = std::min(MinEdge.X, p.X);
        MinEdge.Y = std::min(MinEdge.Y, p.Y);
        MaxEdge.X = std::max(MaxEdge.X, p.X);
        MaxEdge.Y = std::max(MaxEdge.Y, p.Y);
    }

    inline bool isPointInside(const irr::core::vector2d<T>& p) const
    {
        return (p.X >= MinEdge.X && p.X <= MaxEdge.X && p.Y >= MinEdge.Y && p.Y <= MaxEdge.Y);
    }

    /// @brief get the squared distance of the box to the point 'p'
    /// @param p
    /// @return 0 if the point is inside the box, distance to the edges otherwise.
    /// If the point is between two edges in one direction but not the other (e.g. MinX <= X <= MaxX
    /// but outside the Box in the Y direction)
    /// the squared distance to the closest outside-edge (e.g. the closest Y-edge) is returned.
    /// Otherwise the squared distance to the closest corner of the box is returned
    inline T getDistanceToPointSQ(const irr::core::vector2d<T>& p) const
    {
        const T xDistance = this->getXDistance(p.X);
        const T yDistance = this->getYDistance(p.Y);
        return xDistance * xDistance + yDistance * yDistance;
    }

    inline T getArea() const { return (MaxEdge.X - MinEdge.X) * (MaxEdge.Y - MinEdge.Y); }
    /// @brief return the intersection box this box has with 'other'
    /// @param other
    /// @return intersecting box. A box with size zero around the point (0,0) otherwise.
    aabbox2d<T> getIntersection(const aabbox2d<T>& other) const
    {
        aabbox2d<T> out(irr::core::vector2d<T>(static_cast<T>(0)));

        if (!this->intersectsWithBox(other))
        {
            return out;
        }

        out.MaxEdge.X = std::min(MaxEdge.X, other.MaxEdge.X);
        out.MaxEdge.Y = std::min(MaxEdge.Y, other.MaxEdge.Y);
        out.MinEdge.X = std::max(MinEdge.X, other.MinEdge.X);
        out.MinEdge.Y = std::max(MinEdge.Y, other.MinEdge.Y);

        return out;
    }

    bool intersectsWithBox(const aabbox2d<T>& other) const
    {
        return MinEdge.X <= other.MaxEdge.X && MinEdge.Y <= other.MaxEdge.Y &&
            MaxEdge.X >= other.MinEdge.X && MaxEdge.Y >= other.MinEdge.Y;
    }

   private:
    inline T getXDistance(const T& X) const
    {
        if (X >= MinEdge.X && X <= MaxEdge.X)
        {
            return static_cast<T>(0);
        }

        return std::min(std::abs(MinEdge.X - X), std::abs(MaxEdge.X - X));
    }

    inline T getYDistance(const T& Y) const
    {
        if (Y >= MinEdge.Y && Y <= MaxEdge.Y)
        {
            return static_cast<T>(0);
        }

        return std::min(std::abs(MinEdge.Y - Y), std::abs(MaxEdge.Y - Y));
    }
};

typedef aabbox2d<irr::s32> aabbox2di;

#endif // AABBBOX2D_H
