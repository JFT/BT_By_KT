/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2016 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>

#include <irrlicht/irrlicht.h> // createDevice()/createDeviceEx()

#include "utils/stringwstream.h" // stringwstream.h needs to be included before debug.h and error.h because they use use operator<< on a stringw

#include "map/mapGenerator.h"
#include "map/pathMapXML.h"
#include "utils/debug.h"
#include "utils/static/error.h"
#include "utils/stringwparser.h"

int main(int argumentCount, char* arguments[])
{
    irr::IrrlichtDevice* const device = irr::createDevice();

    constexpr int patchesFileArgument = 1;
    constexpr int tileDefinitionsFileArgument = patchesFileArgument + 1;
    constexpr int cliffMapImageFileArgument = tileDefinitionsFileArgument + 1;
    constexpr int pathMapXMLFileArgument = cliffMapImageFileArgument + 1;
    constexpr int scaleXArgument = pathMapXMLFileArgument + 1;
    constexpr int scaleYArgument = scaleXArgument + 1;
    constexpr int scaleZArgument = scaleYArgument + 1;
    constexpr int rampMapImageFileArgument = scaleZArgument + 1;
    constexpr int offsetMapImageFileArgument = rampMapImageFileArgument + 1;

    const std::string usage("usage: mapGenerator <patchesFile> <tileDefinitionsFile> "
                            "<cliffMapImage> <pathMapXMLFile> <scaleX> <scaleY> <scaleZ> "
                            "[<rampMapImage>] [<offsetMapImage>]\n\nif <patchesFile> ends in .xml "
                            "generating xml text file, otherwise generating binary map file");

    if (argumentCount < scaleZArgument + 1) // + 1 to get from index -> array size
    {
        Error::errTerminate(usage, '\n', '\n', "mapGenerator must be called with at least 3 arguments!");
    }

    irr::video::IImage* cliffMapImage =
        device->getVideoDriver()->createImageFromFile(arguments[cliffMapImageFileArgument]);
    if (cliffMapImage == nullptr)
    {
        Error::errTerminate("cliffMapImage", arguments[cliffMapImageFileArgument], "couldn't be loaded");
    }

    irr::f32 scaleX = 0.0f;
    if (StringWParser::parse(irr::core::stringw(arguments[scaleXArgument]), scaleX, 0.0f) != ErrCodes::NO_ERR)
    {
        Error::errTerminate("couldn't parse <scaleX> = ", repr<const char*>(arguments[scaleXArgument]), "as float!");
    }
    irr::f32 scaleY = 0.0f;
    if (StringWParser::parse(irr::core::stringw(arguments[scaleYArgument]), scaleY, 0.0f) != ErrCodes::NO_ERR)
    {
        Error::errTerminate("couldn't parse <scaleY> = ", repr<const char*>(arguments[scaleYArgument]), "as float!");
    }
    irr::f32 scaleZ = 0.0f;
    if (StringWParser::parse(irr::core::stringw(arguments[scaleZArgument]), scaleZ, 0.0f) != ErrCodes::NO_ERR)
    {
        Error::errTerminate("couldn't parse <scaleZ> = ", repr<const char*>(arguments[scaleZArgument]), "as float!");
    }

    // rampMap must't be set if no ramps are needed.
    irr::video::IImage* rampMapImage = nullptr;
    if (argumentCount >= rampMapImageFileArgument + 1) // + 1 to get from index -> array size
    {
        rampMapImage = device->getVideoDriver()->createImageFromFile(arguments[rampMapImageFileArgument]);
        if (rampMapImage == nullptr)
        {
            Error::errContinue("rampMapImage", arguments[rampMapImageFileArgument], "couldn't be loaded");
        }
    }
    else
    {
        debugOutLevel(Debug::DebugLevels::stateInit + 1, "no 'rampMapImage' argument given on command line");
    }


    // offsetMap must't be set if no offsets are needed.
    irr::video::IImage* offsetMapImage = nullptr;
    if (argumentCount >= offsetMapImageFileArgument + 1) // + 1 to get from index -> array size
    {
        offsetMapImage = device->getVideoDriver()->createImageFromFile(arguments[offsetMapImageFileArgument]);
        if (offsetMapImage == nullptr)
        {
            Error::errContinue("offsetMapImage", arguments[offsetMapImageFileArgument], "couldn't be loaded");
        }
    }
    else
    {
        debugOutLevel(Debug::DebugLevels::stateInit + 1, "no 'offsetMapImage' argument given on command line");
    }

    const irr::io::path tileDefinitionsFile(arguments[tileDefinitionsFileArgument]);
    tileMap::MapGenerator mapGen(
        cliffMapImage, rampMapImage, offsetMapImage, tileDefinitionsFile, device->getSceneManager());
    // the MapGenerator grab()'s the images and they aren't needed here any more...
    cliffMapImage->drop();
    if (rampMapImage != nullptr)
    {
        rampMapImage->drop();
    }
    if (offsetMapImage != nullptr)
    {
        offsetMapImage->drop();
    }

    // TODO: don't hardcode the patchsize of 10x10
    if (mapGen.generateAndSavePatches(arguments[patchesFileArgument], 10, 10, device) != ErrCodes::NO_ERR)
    {
        Error::errContinue("map gereration failed. See previuos errors for an indication why");
        return 2;
    }

    PathGrid pathGrid;
    if (mapGen.generatePathMap(irr::core::vector3df(scaleX, scaleY, scaleZ), pathGrid) == ErrCodes::NO_ERR)
    {
        PathMapXML::savePathMap(pathGrid, irr::io::path(arguments[pathMapXMLFileArgument]), device->getFileSystem());
    }
    else
    {
        Error::errContinue("pathMap generation failed. Pathfinding might not work. See previous "
                           "errors for an indication why it failed");
    }

    return 0;
}
