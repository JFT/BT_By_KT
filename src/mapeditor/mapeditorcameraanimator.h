/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RTSCAMERAANIMATOR_H
#define RTSCAMERAANIMATOR_H

#include <irrlicht/ISceneNodeAnimator.h>
#include <irrlicht/Keycodes.h>
#include <irrlicht/SKeyMap.h>
#include <irrlicht/irrArray.h>
#include <irrlicht/position2d.h>
#include <irrlicht/vector2d.h>

#include <cmath>

// forward declarations
namespace irr
{
    namespace sce
    {
        class ISceneManager;
    }
    namespace gui
    {
        class ICursorControl;
    }
} // namespace irr


//! enumeration for key actions for RTS_CAM
enum MAPEDIT_CAM_ACTION
{
    CAM_MOVE_FORWARD = 0,
    CAM_MOVE_BACKWARD,
    CAM_MOVE_LEFT,
    CAM_MOVE_RIGHT,
    CAM_PLUS,
    CAM_MINUS,
    CAM_CTRL,

    // PUT COUNT UNDER ALL OTHER INPUTS other than CAM_FORCE_32BIT !!
    CAM_COUNT,


    CAM_FORCE_32BIT = 0x7fffffff
};


class MapEditorCameraAnimator : public irr::scene::ISceneNodeAnimator
{
   public:
    //! Struct storing which key belongs to which action.
    struct MyKeyMap
    {
        MyKeyMap(MAPEDIT_CAM_ACTION action, irr::EKEY_CODE keyCode)
            : Action(action)
            , KeyCode(keyCode)
        {
        }

        MAPEDIT_CAM_ACTION Action;
        irr::EKEY_CODE KeyCode;
    };

    explicit MapEditorCameraAnimator(irr::gui::ICursorControl* cursorControl,
                                     irr::f32 moveSpeed = 1.0f,
                                     irr::f32 zoomingSpeed = 5.0f,
                                     irr::f32 lowerZoomLimit = 10.0f,
                                     irr::f32 upperZoomLimit = 500.0f,
                                     irr::core::vector2df xBounds = irr::core::vector2df(0, 0),
                                     irr::core::vector2df yBounds = irr::core::vector2df(0, 0),
                                     MyKeyMap* keyMapArray = nullptr,
                                     irr::u32 keyMapSize = 0);

    MapEditorCameraAnimator(const MapEditorCameraAnimator&) = delete;

    virtual ~MapEditorCameraAnimator();

    //! Animates the scene node, currently only works on cameras
    virtual void animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs);

    //! Event receiver
    virtual bool OnEvent(const irr::SEvent& event);

    //! Returns the speed of movement in units per millisecond
    virtual irr::f32 getMoveSpeed() const;

    //! Sets the speed of movement in units per millisecond
    virtual void setMoveSpeed(const irr::f32 moveSpeed);


    //! Sets the keyboard mapping for this animator (old style)
    /** \param map Array of keyboard mappings, see irr::SKeyMap
    \param count Size of the keyboard map array. */
    virtual void setKeyMap(const MapEditorCameraAnimator::MyKeyMap* map, const irr::u32 count);

    //! Sets the keyboard mapping for this animator
    //!	\param keymap The new keymap array
    virtual void setKeyMap(const irr::core::array<MapEditorCameraAnimator::MyKeyMap>& keymap);

    //! Gets the keyboard mapping for this animator
    virtual const irr::core::array<MapEditorCameraAnimator::MyKeyMap>& getKeyMap() const;

    //! This animator will receive events when attached to the active camera
    virtual bool isEventReceiverEnabled() const { return true; }
    virtual void setBounds(const irr::core::vector2df& xBounds, const irr::core::vector2df yBounds);

    //! Creates a clone of this animator.
    /** Please note that you will have to drop
    (IReferenceCounted::drop()) the returned pointer once you're
    done with it. */
    virtual irr::scene::ISceneNodeAnimator*
    createClone(irr::scene::ISceneNode* node, irr::scene::ISceneManager* newManager = nullptr);

    MapEditorCameraAnimator operator=(const MapEditorCameraAnimator&) = delete;

   protected:
   private:
    irr::gui::ICursorControl* CursorControl;

    irr::f32 wheelzoom;
    irr::f32 zoomspeed;
    bool wheelmoved;
    irr::f32 upperzoomlimit;
    irr::f32 lowerzoomlimit;
    // vect2df: X->x/y lower limit - Y: upper limit
    irr::core::vector2df xBoundaries;
    irr::core::vector2df yBoundaries;

    irr::f32 MoveSpeed;

    irr::s32 LastAnimationTime;

    irr::core::array<MyKeyMap> KeyMap;
    irr::core::position2d<irr::f32> CenterCursor, CursorPos, CursorRClick;

    bool CursorKeys[CAM_COUNT];

    bool firstUpdate;
    bool firstInput;

    bool midMouseClick;
    int midMouseCnt;

    float rotAngleXZ;
    float rotAngleY;

    bool rMouseDown;
    bool mMouseDown;
    // functions

    void allKeysUp();

    void checkBounds(irr::core::vector3df& pos, irr::core::vector3df& target);
};

#endif // RTSCAMERAANIMATOR_H
