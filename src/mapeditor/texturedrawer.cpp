/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "texturedrawer.h"

#include <irrlicht/ITexture.h>
#include <irrlicht/IVideoDriver.h>

#include "../utils/stringwstream.h" // stringwstream must be included before debug.h because the code in debug.h needs operator<< for stringws in this file
#include "../graphics/ImageManipulation.h"
#include "../utils/debug.h"
#include "../utils/printfunctions.h"

TextureDrawer::TextureDrawer(irr::video::ITexture* const texture_, irr::video::IImage* const textureImage_)
    : texture(texture_)
    , textureImage(textureImage_)
{
    if (this->texture != nullptr)
    {
        this->texture->grab();
    }
    if (this->textureImage != nullptr)
    {
        this->textureImage->grab();
    }
}

TextureDrawer::~TextureDrawer()
{
    if (this->texture != nullptr)
    {
        this->texture->drop();
    }
    if (this->textureImage != nullptr)
    {
        this->textureImage->drop();
    }
}

void TextureDrawer::drawOnTexture(const irr::core::array<irr::core::vector2d<irr::u32>>& coords,
                                  const irr::core::array<irr::video::SColor>& colors)
{
    if (this->texture == nullptr)
    {
        return;
    }
    if (coords.size() != colors.size())
    {
        Error::errContinue("coordinates and colors have diffiering size");
        return;
    }

    // get the pointer to the texture data which in this case is basicly an array of u32 values,
    // we need to cast since the function returns a void pointer, however we already know the color
    // format and thus what to cast to
    // TODO: maybe add a check on construction to look up the color format and make sure this
    // function is never used on an unsuspected format
    irr::u32* textureDataPtr = static_cast<irr::u32*>(this->texture->lock());


    // use that pointer to acces the array
    for (irr::u32 i = 0; i < coords.size(); ++i)
    {
        textureDataPtr[coords[i].Y * this->texture->getOriginalSize().Height + coords[i].X] =
            colors[i].color;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                      "drawing on texture at",
                      vector2dToStringW(coords[i]),
                      "with color",
                      colorToStringW(colors[i]));
        if (this->textureImage != nullptr)
        {
            this->textureImage->setPixel(coords[i].X, coords[i].Y, colors[i]);
        }
    }

    // unlock the textures so they can be used by the graphics card again
    this->texture->unlock();
}

ErrCode TextureDrawer::saveTexture(const irr::io::path& filename, irr::video::IVideoDriver* driver)
{
    irr::video::IImage* image = nullptr;
    if (this->textureImage == nullptr)
    {
        image = ImageManipulation::TextureToImage(this->texture, driver);
        if (image == nullptr)
        {
            return ErrCodes::GENERIC_ERR;
        }
    }
    else
    {
        image = this->textureImage;
    }
    driver->writeImageToFile(image, filename);
    if (this->textureImage == nullptr)
    {
        // TextureToImage created a new image which needs to be dropped.
        image->drop();
    }
    return ErrCodes::NO_ERR;
}

irr::u32 TextureDrawer::createRGBA(int r, int g, int b, int a)
{
    return static_cast<irr::u32>(((r & 0xff) << 24) + ((g & 0xff) << 16) + ((b & 0xff) << 8) + (a & 0xff));
}
