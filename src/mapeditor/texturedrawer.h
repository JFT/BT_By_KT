/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TEXTUREDRAWER_H
#define TEXTUREDRAWER_H

#include <string>

#include <irrlicht/SColor.h>
#include <irrlicht/irrArray.h>
#include <irrlicht/path.h>
#include <irrlicht/vector2d.h>

#include "../utils/static/error.h" // defines ErrCode

namespace irr
{
    namespace video
    {
        class IVideoDriver;
        class ITexture;
        class IImage;
    } // namespace video
} // namespace irr

class TextureDrawer
{
   public:
    /// @brief create a texture drawer with texture to draw on
    /// @param texture ITexture* of the texture to draw on. grab()s a reference while the
    /// TextureDrawer exists. The texture MUST be created with the TextureCreationFlag
    /// ETCF_ALLOW_MEMORY_COPY set to true!
    /// @param TextureToImage pointer to the texture as an IImage. If set drawOnTexture will also
    /// draw on the textureImage and getTextureImage() can be used to get the same data as on the
    /// texture without the need to lock the graphics card. Grab()s a reference while the
    /// TextureDrawer exists. MUST be at least the same size as the texture.
    explicit TextureDrawer(irr::video::ITexture* const texture_, irr::video::IImage* const textureImage_ = nullptr);
    TextureDrawer(const TextureDrawer& orig) = delete;
    TextureDrawer operator=(const TextureDrawer& orig) = delete;
    virtual ~TextureDrawer();

    /// @returns the IImage with the same data as the texture if textureImage was set on
    /// construction. Otherwise returns nullptr.
    inline irr::video::IImage* getTextureImage() const { return this->textureImage; }
    /// @brief draw on the texture  with a sepecific color.
    ///
    /// If the TextureDrawer was created with textureImage will also draw on textureImage
    /// @param coords array of pixel coordinates which should be drawn upon. MUST be inside the
    /// texture. No bounds checks will be performed!
    /// @param colors the color for each pixel. MUST be the same size as the coords array, otherwise
    /// nothing will be drawn.
    void drawOnTexture(const irr::core::array<irr::core::vector2d<irr::u32>>& coords,
                       const irr::core::array<irr::video::SColor>& colors);

    /// @brief save the texture under the supplied filename
    /// @param filename filename for the texture to be saved as
    /// @param driver function will fail if this is nullptr
    /// @return ErrCodes::NO_ERROR on success, errorcode otherwise.
    ErrCode saveTexture(const irr::io::path& filename,
                        irr::video::IVideoDriver* driver); // TODO maybe include in save map

   private:
    irr::video::ITexture* const texture = nullptr;
    irr::video::IImage* const textureImage = nullptr;

    irr::u32 createRGBA(int r, int g, int b, int a);
};

#endif /* TEXTUREDRAWER_H */
