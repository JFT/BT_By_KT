/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MODIFIABLEPARAMETERS_H
#define MODIFIABLEPARAMETERS_H

#include <irrlicht/irrString.h>
#include <irrlicht/irrTypes.h>


enum MAPEDIT_STATE
{
    MES_GROUND_NOTEXTURESELECTED,
    MES_GROUND_TEXTURING,

    MES_OBJECT_NOTHINGSELECTED,
    MES_OBJECT_PLACING,
    MES_OBJECT_MANIPULATION,

    MES_PATHMAP_MANIPULATION,

    MES_DONOTHING,
    MES_COUNT

};

struct ModifiableParameters
{
    // Textures
    std::vector<irr::core::stringw> textureArrayFilenames = std::vector<irr::core::stringw>();

    irr::u8 textureIndexToDraw = 1;

    bool incrementalIntensity = false;
    bool smoothDrawing = true;
    bool overwriteLowestIntensity = true;
    bool allowIntensityDecrease = false;

    irr::f32 brushSize = 100.0f;
    irr::f32 drawsPerSecond = 5.0f;
    irr::f32 brushIntensity = 16;
};

#endif // MODIFIABLEPARAMETERS_H
