/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2018

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <functional>
#include <queue>

#include "mapEditorBindings.hpp"

#include <sol.hpp>

#include <irrlicht/irrTypes.h>
#include <irrlicht/vector3d.h>
#include <vector>
#include "modifiableParameters.h"
#include "../ecs/entity.h"
#include "../states/mapeditorstate.h"


#define SOL_FUN(Class, Name) #Name, &Class::Name
#define SOL_ENUM(EnumName, Member) #Member, EnumName::Member

bool createMapEditorBindings(sol::state* const luastate)
{
    luastate->new_enum("MAPEDIT_STATE",
                       SOL_ENUM(MAPEDIT_STATE, MES_GROUND_NOTEXTURESELECTED),
                       SOL_ENUM(MAPEDIT_STATE, MES_GROUND_TEXTURING),
                       SOL_ENUM(MAPEDIT_STATE, MES_OBJECT_NOTHINGSELECTED),
                       SOL_ENUM(MAPEDIT_STATE, MES_OBJECT_PLACING),
                       SOL_ENUM(MAPEDIT_STATE, MES_OBJECT_MANIPULATION),
                       SOL_ENUM(MAPEDIT_STATE, MES_PATHMAP_MANIPULATION),
                       SOL_ENUM(MAPEDIT_STATE, MES_DONOTHING),
                       SOL_ENUM(MAPEDIT_STATE, MES_COUNT));

    luastate->new_usertype<ModifiableParameters>("ModifiableParameters",
                                                 "new",
                                                 sol::no_constructor,
                                                 SOL_FUN(ModifiableParameters, textureIndexToDraw),
                                                 SOL_FUN(ModifiableParameters, incrementalIntensity),
                                                 SOL_FUN(ModifiableParameters, smoothDrawing),
                                                 SOL_FUN(ModifiableParameters, overwriteLowestIntensity),
                                                 SOL_FUN(ModifiableParameters, allowIntensityDecrease),
                                                 SOL_FUN(ModifiableParameters, brushSize),
                                                 SOL_FUN(ModifiableParameters, drawsPerSecond),
                                                 SOL_FUN(ModifiableParameters, brushIntensity));

    auto overloadedSwitchCamPos =
        sol::overload(sol::resolve<void(const entityID_t)>(&MapEditorState::switchCameraPosition),
                      sol::resolve<void(const irr::core::vector3df)>(&MapEditorState::switchCameraPosition));
    luastate->new_usertype<MapEditorState>("MapEditorState",
                                           "new",
                                           sol::no_constructor,
                                           SOL_FUN(MapEditorState, onLeave),
                                           SOL_FUN(MapEditorState, loadMap),
                                           SOL_FUN(MapEditorState, saveMap),
                                           SOL_FUN(MapEditorState, selectEntity),
                                           SOL_FUN(MapEditorState, deselectSelectedEntity),
                                           SOL_FUN(MapEditorState, deleteSelectedEntity),
                                           "switchCameraPosition",
                                           overloadedSwitchCamPos,
                                           SOL_FUN(MapEditorState, createEntityFromBlueprint),
                                           SOL_FUN(MapEditorState, setEditorStateTo),
                                           SOL_FUN(MapEditorState, getEditorState));

    return true;
}
