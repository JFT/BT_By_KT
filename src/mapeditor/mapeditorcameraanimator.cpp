/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mapeditorcameraanimator.h"

#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/ICursorControl.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/IVideoDriver.h>

MapEditorCameraAnimator::MapEditorCameraAnimator(irr::gui::ICursorControl* cursorControl,
                                                 irr::f32 moveSpeed,
                                                 irr::f32 zoomingSpeed,
                                                 irr::f32 lowerZoomLimit,
                                                 irr::f32 upperZoomLimit,
                                                 irr::core::vector2df xBounds,
                                                 irr::core::vector2df yBounds,
                                                 MyKeyMap* keyMapArray,
                                                 irr::u32 keyMapSize)
    : CursorControl(cursorControl)
    , wheelzoom(0)
    , zoomspeed(zoomingSpeed)
    , wheelmoved(false)
    , upperzoomlimit(upperZoomLimit)
    , lowerzoomlimit(lowerZoomLimit)
    , xBoundaries(xBounds)
    , yBoundaries(yBounds)
    , MoveSpeed(moveSpeed)
    , LastAnimationTime(0)
    , KeyMap(irr::core::array<MyKeyMap>())
    , CenterCursor(irr::core::position2d<irr::f32>(0, 0))
    , CursorPos(irr::core::position2d<irr::f32>(0, 0))
    , CursorRClick(irr::core::position2df(0, 0))
    , firstUpdate(true)
    , firstInput(true)
    , midMouseClick(false)
    , midMouseCnt(0)
    , rotAngleXZ(0.f)
    , rotAngleY(0.f)
    , rMouseDown(false)
    , mMouseDown(false)
{
    // TRY if needed
    if (CursorControl != nullptr)
    {
        CursorControl->grab();
    }


    // create key map
    if (!keyMapArray || !keyMapSize)
    {
        // create default key map
        KeyMap.push_back(MyKeyMap(CAM_MOVE_FORWARD, irr::KEY_UP));
        KeyMap.push_back(MyKeyMap(CAM_MOVE_BACKWARD, irr::KEY_DOWN));
        KeyMap.push_back(MyKeyMap(CAM_MOVE_LEFT, irr::KEY_LEFT));
        KeyMap.push_back(MyKeyMap(CAM_MOVE_RIGHT, irr::KEY_RIGHT));
        KeyMap.push_back(MyKeyMap(CAM_PLUS, irr::KEY_PLUS));
        KeyMap.push_back(MyKeyMap(CAM_MINUS, irr::KEY_MINUS));
        KeyMap.push_back(MyKeyMap(CAM_CTRL, irr::KEY_LCONTROL));
    }
    else
    {
        // create custom key map
        setKeyMap(keyMapArray, keyMapSize);
    }
    allKeysUp();
}

MapEditorCameraAnimator::~MapEditorCameraAnimator()
{
    if (CursorControl != nullptr)
    {
        CursorControl->drop();
    }
}

#pragma GCC diagnostic ignored "-Wswitch"
#pragma GCC diagnostic ignored "-Wswitch-enum"
bool MapEditorCameraAnimator::OnEvent(const irr::SEvent& evt)
{
    switch (evt.EventType)
    {
        case irr::EET_KEY_INPUT_EVENT:
            for (irr::u32 i = 0; i < KeyMap.size(); ++i)
            {
                if (KeyMap[i].KeyCode == evt.KeyInput.Key)
                {
                    CursorKeys[KeyMap[i].Action] = evt.KeyInput.PressedDown;
                    return true;
                }
            }
            break;

        case irr::EET_MOUSE_INPUT_EVENT:
            if (evt.MouseInput.Event == irr::EMIE_RMOUSE_PRESSED_DOWN)
            {
                CursorRClick = CursorControl->getRelativePosition();
                rMouseDown = true;
            }
            else if (evt.MouseInput.Event == irr::EMIE_RMOUSE_LEFT_UP)
            {
                rMouseDown = false;
            }
            if (evt.MouseInput.Event == irr::EMIE_MOUSE_MOVED)
            {
                CursorPos = CursorControl->getRelativePosition();
                return true;
            }
            if (evt.MouseInput.Event == irr::EMIE_MOUSE_WHEEL)
            {
                wheelzoom = evt.MouseInput.Wheel;
                wheelmoved = true;
                return true;
            }
            if (evt.MouseInput.Event == irr::EMIE_MMOUSE_PRESSED_DOWN)
            {
                mMouseDown = true;
                return true;
            }
            else if (evt.MouseInput.Event == irr::EMIE_MMOUSE_LEFT_UP)
            {
                mMouseDown = false;
                return true;
            }
            break;

        default:
            break;
    }
    return false;
}
#pragma GCC diagnostic pop
#pragma GCC diagnostic pop

void MapEditorCameraAnimator::animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs)
{
    if (!node || node->getType() != irr::scene::ESNT_CAMERA)
    {
        return;
    }

    irr::scene::ICameraSceneNode* camera = static_cast<irr::scene::ICameraSceneNode*>(node);

    camera->updateAbsolutePosition();
    if (firstUpdate)
    {
        if (CursorControl)
        {
            CursorControl->setPosition(0.5f, 0.5f);
            CursorPos = CenterCursor = CursorControl->getRelativePosition();
        }
        LastAnimationTime = timeMs;
        firstUpdate = false;
    }

    // If the camera isn't the active camera, and receiving input, then don't process it.
    if (!camera->isInputReceiverEnabled())
    {
        firstInput = true;
        return;
    }

    if (firstInput)
    {
        allKeysUp();
        firstInput = false;
    }

    irr::scene::ISceneManager* smgr = camera->getSceneManager();
    if (smgr && smgr->getActiveCamera() != camera)
    {
        return;
    }

    // get time
    irr::f32 timeDiff = irr::f32((timeMs - LastAnimationTime));
    LastAnimationTime = timeMs;

    // update position
    irr::core::vector3df pos = camera->getPosition();


    // Update target
    irr::core::vector3df target = camera->getTarget();

    irr::core::matrix4 mYAxis;
    irr::core::matrix4 mXZAxis;

    irr::f32 speedFactorX = irr::f32(smgr->getVideoDriver()->getScreenSize().Width);
    irr::f32 speedFactorZ = irr::f32(smgr->getVideoDriver()->getScreenSize().Height / 2);

    irr::core::vector3df offset;
    //! The rotAngles are named the wrong way..
    if (CursorControl)
    {
        if (rMouseDown && CursorKeys[CAM_CTRL])
        {
            irr::core::vector3df dir(pos - target);
            irr::core::vector3df camAngle = dir.getHorizontalAngle();

            irr::f32 diffx = (CursorPos - CursorRClick).X * timeDiff;
            irr::f32 diffz = (CursorPos - CursorRClick).Y * timeDiff;

            rotAngleXZ = diffx * speedFactorX / 10.f + camAngle.Y;
            rotAngleY = -diffz * speedFactorZ / 10.f + camAngle.X;

            if (rotAngleY >= 90 && rotAngleY < 180)
            {
                rotAngleY = 89;
            }
            else if (rotAngleY < 271 && rotAngleY >= 180)
            {
                rotAngleY = 271;
            }

            mYAxis.setRotationDegrees(irr::core::vector3df(0, rotAngleXZ, 0));
            mXZAxis.setRotationDegrees(irr::core::vector3df(rotAngleY, 0, 0));

            irr::core::vector3df newDir(0, 0, dir.getLength());
            (mYAxis * mXZAxis).rotateVect(newDir);

            pos = target + newDir;
        }
        else if (rMouseDown && !CursorKeys[CAM_CTRL])
        {
            irr::f32 diffx = (CursorPos - CursorRClick).X;
            irr::f32 diffz = (CursorPos - CursorRClick).Y;

            offset = irr::core::vector3df(speedFactorX * diffx * timeDiff, 0, -diffz * timeDiff * speedFactorZ);
            mYAxis.setRotationDegrees(irr::core::vector3df(0, rotAngleXZ, 0));
            mYAxis.rotateVect(offset);

            pos += offset;
            target += offset;
        }
    }
    mYAxis.setRotationDegrees(irr::core::vector3df(0, rotAngleXZ, 0));
    if (CursorKeys[CAM_MOVE_LEFT])
    {
        offset = irr::core::vector3df(MoveSpeed * timeDiff, 0, 0);

        mYAxis.rotateVect(offset);

        pos += offset;
        target += offset;
    }
    else if (CursorKeys[CAM_MOVE_RIGHT])
    {
        offset = irr::core::vector3df(-MoveSpeed * timeDiff, 0, 0);

        mYAxis.rotateVect(offset);

        pos += offset;
        target += offset;
    }

    if (CursorKeys[CAM_MOVE_FORWARD])
    {
        offset = irr::core::vector3df(0, 0, -MoveSpeed * timeDiff);

        mYAxis.rotateVect(offset);

        pos += offset;
        target += offset;
    }
    else if (CursorKeys[CAM_MOVE_BACKWARD])
    {
        offset = irr::core::vector3df(0, 0, MoveSpeed * timeDiff);

        mYAxis.rotateVect(offset);

        pos += offset;
        target += offset;
    }

    if (wheelmoved)
    {
        if (wheelmoved && (pos.Y - (timeDiff * zoomspeed * wheelzoom)) > lowerzoomlimit &&
            (pos.Y - (timeDiff * zoomspeed * wheelzoom)) < upperzoomlimit)
        {
            irr::core::vector3df dir(pos - target);

            irr::f32 distance = dir.getLength();
            distance -= timeDiff * zoomspeed * wheelzoom;
            dir = dir.normalize() * distance;
            pos = dir + target;
        }
        wheelmoved = false;
    }

    if (CursorKeys[CAM_PLUS])
    {
        pos.Y -= timeDiff * zoomspeed * 0.1f;
        if (pos.Y < lowerzoomlimit)
        {
            pos.Y = lowerzoomlimit;
        }
    }
    else if (CursorKeys[CAM_MINUS])
    {
        pos.Y += timeDiff * zoomspeed * 0.1f;
        if (pos.Y > upperzoomlimit)
        {
            pos.Y = upperzoomlimit;
        }
    }

    if (mMouseDown)
    {
        pos = (irr::core::vector3df(0, 600, 300)) + target;
        rotAngleY = 0.f;
        rotAngleXZ = 0.f;
        mMouseDown = false;
    }

    CursorRClick = CursorPos;
    checkBounds(pos, target);
    // write translation
    camera->setPosition(pos);

    // write right target
    camera->setTarget(target);
}

void MapEditorCameraAnimator::allKeysUp()
{
    for (irr::u32 i = 0; i < MAPEDIT_CAM_ACTION::CAM_COUNT; ++i)
    {
        CursorKeys[i] = false;
    }
}


//! Sets the movement speed

void MapEditorCameraAnimator::setMoveSpeed(const irr::f32 speed)
{
    MoveSpeed = speed;
}

//! Gets the movement speed

irr::f32 MapEditorCameraAnimator::getMoveSpeed() const
{
    return MoveSpeed;
}

//! Sets the keyboard mapping for this animator

void MapEditorCameraAnimator::setKeyMap(const MapEditorCameraAnimator::MyKeyMap* map, const irr::u32 count)
{
    // clear the keymap
    KeyMap.clear();

    // add actions
    for (irr::u32 i = 0; i < count; ++i)
    {
        KeyMap.push_back(map[i]);
    }
}

void MapEditorCameraAnimator::setKeyMap(const irr::core::array<MapEditorCameraAnimator::MyKeyMap>& keymap)
{
    KeyMap = keymap;
}

const irr::core::array<MapEditorCameraAnimator::MyKeyMap>& MapEditorCameraAnimator::getKeyMap() const
{
    return KeyMap;
}

void MapEditorCameraAnimator::setBounds(const irr::core::vector2df& xBounds, const irr::core::vector2df yBounds)
{
    xBoundaries = xBounds;
    yBoundaries = yBounds;
}

irr::scene::ISceneNodeAnimator*
MapEditorCameraAnimator::createClone(irr::scene::ISceneNode*, irr::scene::ISceneManager*)
{
    MapEditorCameraAnimator* newAnimator = new MapEditorCameraAnimator(CursorControl, MoveSpeed, 0, 0);
    newAnimator->setKeyMap(KeyMap);
    return newAnimator;
}

void MapEditorCameraAnimator::checkBounds(irr::core::vector3df& pos, irr::core::vector3df& target)
{
    if (xBoundaries == irr::core::vector2df(0, 0) || yBoundaries == irr::core::vector2df(0, 0))
    {
        return; // DO NOTHING
    }
    // check X
    irr::core::vector3df connection = pos - target;
    if (target.X < xBoundaries.X)
    {
        target.X = xBoundaries.X;
    }
    else if (target.X > xBoundaries.Y)
    {
        target.X = xBoundaries.Y;
    }

    if (target.Z < yBoundaries.X)
    {
        target.Z = yBoundaries.X;
    }
    else if (target.Z > yBoundaries.Y)
    {
        target.Z = yBoundaries.Y;
    }
    pos = target + connection;
}
