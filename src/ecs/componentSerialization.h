/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef BT_ECS_COMPONENTSERILAIZATION_H
#define BT_ECS_COMPONENTSERILAIZATION_H

#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>
#include <string>      // std::to_string
#include <type_traits> // std::is_trivially_copyable
#include "componentID.h"
#include "components.h"
#include "../utils/serialization.h"

namespace Serialization
{
    // components ALWAYS need to explicitly override std::is_trivially_copyable because
    // some components are trivially copyable but can't be (de)serialized that way
    // (e.g. Components::Icon)
    // clang-format off
    template <> struct Is_trivially_copyable_override<irr::core::vector2df> : std::true_type {};
    template <> struct Is_trivially_copyable_override<irr::core::vector3df> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::DebugColor> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::Position> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::ProjectileMovement> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::Rotation> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::Price> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::Damage> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::Range> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::Ammo> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::Inventory> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::Requirement> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::SelectionID> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::Cooldown> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::Physical> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::OwnerEntityID> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::WeaponEntityID> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::DroppedItemEntityID> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::PlayerStats> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::PlayerBase> : std::true_type {};
    template <> struct Is_trivially_copyable_override<Components::NetworkPriority> : std::true_type {};
    // clang-format on

    /// @brief this namespace is used to serialize components.
    /// Either to store different components into a single buffer (e.g. for saving to disk)
    /// or to transmit them over the network
    /// The functions defined here should only be used to (de)serialize trivially copyable types.
    /// For dynamically-sized components (e.g. components containing std::vectors) see
    /// 'dynamicallySizedComponentSerialization.h'
    namespace ComponentSerialization
    {
        template <typename ComponentType>
        struct SerializationVersion
        {
            static constexpr Serialization::SerializationVersion_t value = 0;
        };

        /// @brief Serialize 'value' and append the serialized bytes to 'buffer'.
        /// Enabled by template specialization in utils/serialization.h
        /// @tparam ComponentType type to be serialized
        /// @param buffer buffer to append 'value' to. Will be resized to make space for the value.
        /// @param value
        template <typename ComponentType>
        static void serialize(std::vector<Buffer_t>& buffer, const ComponentType& value)
        {
            // make sure this is a component (otherwise the following line produces an error)
            static_assert(Components::ComponentID<ComponentType>::CID != -1,
                          "this line will produce an error if the supplied type isn't a component "
                          "type");
            static_assert(Is_trivially_copyable_override<ComponentType>::value,
                          "Supplied ComponentType hasn't set "
                          "Is_trivially_copyable_override<ComponentType>. Include "
                          "ecs/dynamicallySizedComponentSerialization.h or set "
                          "Is_trivially_copyable_override<> in ecs/componentSerialization.h!");
            const auto oldSize = buffer.size();
            buffer.resize(oldSize + sizeof(ComponentType));
            *reinterpret_cast<ComponentType* const>(&buffer[oldSize]) = value;
        }

        /// @brief deserialize specified component out of the buffer at 'pos'.
        /// Throws std::seraialization_exception if encountering an error
        /// (e.g. if buffer isn't large enough)
        /// @tparam ComponentType
        /// @param buffer
        /// @param pos byte offset into 'buffer' where the data to be interpreted as 'T' begins.
        /// 'pos' will be advanced by amount of bytes read after succesfull deserialization.
        /// @return the deserialized value.
        template <typename ComponentType>
        static ComponentType deserialize(const std::vector<Buffer_t>& buffer, std::size_t& pos)
        {
            static_assert(Components::ComponentID<ComponentType>::CID != -1,
                          "this line will produce an error if the supplied type isn't a component "
                          "type");
            static_assert(Is_trivially_copyable_override<ComponentType>::value,
                          "Supplied ComponentType hasn't set "
                          "Is_trivially_copyable_override<ComponentType>. Include "
                          "ecs/dynamicallySizedComponentSerialization.h or set "
                          "Is_trivially_copyable_override<> in ecs/componentSerialization.h!");
            static_assert(sizeof(ComponentType) > 1,
                          "component size must be at least 1 byte (because that "
                          "is assumed by various users of this function)!");
            if (pos + sizeof(ComponentType) > buffer.size())
            {
                throw serialization_buffer_overrun(sizeof(ComponentType),
                                                   pos,
                                                   buffer.size(),
                                                   Components::ComponentID<ComponentType>::name);
            }
            const ComponentType value = *reinterpret_cast<const ComponentType* const>(&buffer[pos]);
            pos += sizeof(ComponentType);
            return value;
        }

        namespace Networking
        {
            inline unsigned char* toNetworkDataType(Buffer_t* const buffer)
            {
                static_assert(sizeof(Buffer_t) == sizeof(unsigned char),
                              "SerializedComponentBuffer_t isn't byte-sized!");
                return reinterpret_cast<unsigned char* const>(buffer);
            }
        } // namespace Networking

    } // namespace ComponentSerialization
} // namespace Serialization


#endif /* ifndef BT_ECS_COMPONENTSERILAIZATION_H */
