/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "networkComponentSyncronizationClient.h"
#include "componentSerializer.h"
#include "../network/allocationIDs.h"
#include "../utils/debug.h"
#include "../utils/static/error.h"


NetworkComponentSyncronizationClient::NetworkComponentSyncronizationClient(
    EntityManager& entityManager_,
    Handles::HandleManager& handleManager_,
    const std::vector<IComponentSerializerBase*>& componentSerializers_)
    : INetworkComponentSyncronization(entityManager_, handleManager_)
    , deserializedComponentBuffer()
    , componentSerializers(componentSerializers_)
{
}

NetworkComponentSyncronizationClient::~NetworkComponentSyncronizationClient() {}

void NetworkComponentSyncronizationClient::applyReceivedComponents()
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "applyReceivedComponents() with buffer size",
                  this->deserializedComponentBuffer.size());
    BT_RELEASE_ONLY_(try)
    {
        size_t pos = 0;
        while (pos < this->deserializedComponentBuffer.size())
        {
            const SerializationVariant_e variant =
                static_cast<SerializationVariant_e>(this->deserializedComponentBuffer[pos]);
            ++pos;

            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "read serialization variant",
                          static_cast<size_t>(variant),
                          "at",
                          pos,
                          "in bufffer size",
                          this->deserializedComponentBuffer.size());

            switch (variant)
            {
                case SerializationVariant_e::SingleEntity:
                    this->deserializeEntity(pos);
                    break;
                case SerializationVariant_e::SingleComponent:
                    this->deserializeSingleComponent(pos);
                    break;
                default:
                    throw std::invalid_argument("unhandled serialization variant " +
                                                std::to_string(static_cast<int>(variant)) +
                                                " in buffer at position " + std::to_string(pos));
            }
        } // while (pos < this->deserializedComponentBuffer.size())
    }
    BT_RELEASE_ONLY_(catch (const std::exception& e) {
        // POW2_ASSERT_FAIL(e.what(), 0);
        Error::errTerminate("couldn't deserialize components. Error:", e.what());
        // TODO: maybe ask the server to re-send? Or count up an 'unexpected' couter and disconnect
        // if that is too high?
    })
    this->deserializedComponentBuffer.clear();
}

void NetworkComponentSyncronizationClient::Deserialize(RakNet::DeserializeParameters* deserializeParameters)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "before deserialization for connection",
                  deserializeParameters->sourceConnection,
                  ": deserializedComponentBuffer is",
                  this->deserializedComponentBuffer.size(),
                  "bytes");

    // TODO: check if we are expecting components from this connection
    const uint32_t dataSizeInBuffer = deserializeParameters->serializationBitstream->GetNumberOfBytesUsed();
    size_t oldSize = this->deserializedComponentBuffer.size();
    // TODO: set acceptable limits which make sure the client doesn't reserve GBs of memory due to
    // invalid data
    this->deserializedComponentBuffer.resize(oldSize + dataSizeInBuffer);

    // writing new data starts at 'oldSize' which is the first index of the now created free space
    static_assert(sizeof(const unsigned char) == sizeof(decltype(this->deserializedComponentBuffer)::value_type),
                  "deserializedComponentBuffer isn't a byte-sized-type");
    if (not deserializeParameters->serializationBitstream->ReadAlignedBytes(
            Serialization::ComponentSerialization::Networking::toNetworkDataType(
                &this->deserializedComponentBuffer[oldSize]),
            dataSizeInBuffer))
    {
        Error::errContinue("couldn't read component bytes out of the deserialization bitstream");
        // new space contains invalid data
        this->deserializedComponentBuffer.resize(oldSize);
        return;
    }

    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "deserialized",
                  dataSizeInBuffer,
                  "bytes. New deserializedComponentBuffer is",
                  this->deserializedComponentBuffer.size(),
                  "bytes");

    DEBUG_ONLY_(deserializeParameters->serializationBitstream->AssertStreamEmpty());
}

void NetworkComponentSyncronizationClient::deserializeEntity(size_t& pos)
{
    decltype(this->deserializedComponentBuffer)& buffer = this->deserializedComponentBuffer;

    const entityID_t entity = Serialization::deserialize<entityID_t>(buffer, pos);

    this->createOrMoveEntityIfNeeded(entity);

    const componentID_t numComponents = Serialization::deserialize<componentID_t>(buffer, pos);

    for (componentID_t c = 0; c < numComponents; ++c)
    {
        const componentID_t component = Serialization::deserialize<componentID_t>(buffer, pos);

        if (component >= this->componentSerializers.size() or this->componentSerializers[component] == nullptr)
        {
            throw std::invalid_argument("no componentSerializer for componentID " + std::to_string(component));
        }

        this->componentSerializers[component]->deserializeForEntity(buffer, pos, entity, this->entityManager, this->handleManager);
    }
}

void NetworkComponentSyncronizationClient::deserializeSingleComponent(size_t& pos)
{
    decltype(this->deserializedComponentBuffer)& buffer = this->deserializedComponentBuffer;

    const componentID_t component = Serialization::deserialize<componentID_t>(buffer, pos);
    debugOutLevel(Debug::DebugLevels::updateLoop + 1, "deserialized componentID", component);

    if (component >= this->componentSerializers.size() or this->componentSerializers[component] == nullptr)
    {
        throw std::invalid_argument("no componentSerializer for componentID " + std::to_string(component));
    }

    // save to use entityID_t as number of entities because there can at most be max(entityID_t)
    // entities for which a component was received
    const entityID_t numEntitiesToDeserialize = Serialization::deserialize<entityID_t>(buffer, pos);
    debugOutLevel(Debug::DebugLevels::updateLoop + 1, "deserialized numEntitiesToDeserialize", numEntitiesToDeserialize);

    for (entityID_t i = 0; i < numEntitiesToDeserialize; ++i)
    {
        const auto entity = Serialization::deserialize<entityID_t>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::updateLoop + 2, "deserialized entitID", entity);
        this->createOrMoveEntityIfNeeded(entity);
        this->componentSerializers[component]->deserializeForEntity(buffer, pos, entity, this->entityManager, this->handleManager);
    } // for (entityID_t i = 0; i < numEntitiesToDeserialize; ++i)
}

void NetworkComponentSyncronizationClient::createOrMoveEntityIfNeeded(const entityID_t entity)
{
    constexpr bool createdByServer = true;
    if (entityManager.entityExists(entity))
    {
        if (not entityManager.createdByServer(entity))
        {
            debugOutLevel(Debug::DebugLevels::updateLoop + 4, "moving entity", entity, "because it already exists!");
            entityManager.copyEntity(entity);
            entityManager.deleteEntity(entity);
            entityManager.addEntity(entity, createdByServer);
        }
    }
    else
    {
        entityManager.addEntity(entity, createdByServer);
        debugOutLevel(Debug::DebugLevels::updateLoop + 3, "entity", entity, "already exists");
    }
}
