/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "entityBlueprint.h"

#include <algorithm> // std::sort

#include "entityManager.h"
#include "../utils/HandleManager.h"
#include "../utils/static/error.h"

void EntityBlueprint::addComponent(const std::string component, const Handles::Handle handle)
{
    this->addComponent(this->entityManager.getComponentIDFromName(component), handle);
}

void EntityBlueprint::addComponent(const componentID_t component, const Handles::Handle handle)
{
    this->componentValues.push_back(std::make_pair(component, handle));

    // when creating the entity and adding the components it is pretty important to keep the
    // components sorted
    // because later components might need earlyer ones (e.g. the Movement Component needs the
    // position)
    // -> sort the vector after adding a component
    std::sort(this->componentValues.begin(),
              this->componentValues.end(),
              [](const auto& left, const auto& right) { return left.first < right.first; });
}

Handles::Handle EntityBlueprint::getComponent(const componentID_t component) const
{
    for (auto pair : this->componentValues)
    {
        if (pair.first == component)
        {
            return pair.second;
        }
    }
    return Handles::InvalidHandle;
}

entityID_t EntityBlueprint::createEntity(const bool createdByServer) const
{
    auto const entityID = this->entityManager.addEntity(createdByServer);
    for (const auto& cmp : this->componentValues)
    {
        // TODO: maybe to all the nullptr checking in the handleManager? + check for InvalidHandle?
        // -> no if/else here
        if (cmp.second != Handles::InvalidHandle)
        {
            this->entityManager.addInitializedComponent(entityID, cmp.first, cmp.second);
        }
        else
        {
            this->entityManager.addComponent(entityID, cmp.first);
        }
    }
    return entityID;
}


entityID_t EntityBlueprint::createEntity(const componentID_t component,
                                         const void* const initialValue,
                                         const bool createdByServer) const
{
    bool foundOverwrite = false;
    auto const entityID = this->entityManager.addEntity(createdByServer);
    for (const auto& cmp : this->componentValues)
    {
        // TODO: maybe to all the nullptr checking in the handleManager? + check for InvalidHandle?
        // -> no if/else here
        if (cmp.first == component)
        {
            this->entityManager.addInitializedComponent(entityID, cmp.first, initialValue);
            foundOverwrite = true;
        }
        else if (cmp.second != Handles::InvalidHandle)
        {
            this->entityManager.addInitializedComponent(entityID, cmp.first, cmp.second);
        }
        else
        {
            this->entityManager.addComponent(entityID, cmp.first);
        }
    }

    if (not foundOverwrite)
    {
        Error::errTerminate("Blueprint",
                            name,
                            "doesn't contain component",
                            Components::NameFromComponendID[component],
                            "which should have been overwritten");
    }

    return entityID;
}

entityID_t EntityBlueprint::createEntity(const std::unordered_map<componentID_t, const void* const>& initialValues,
                                         const bool createdByServer) const
{
    std::vector<componentID_t> found; // TODO: this is errorchecking and might not be needed in the
                                      // game. Maybe create a way to disable/enable error checking
                                      // via preprocessor commands?
    auto const entityID = this->entityManager.addEntity(createdByServer);
    for (const auto& cmp : this->componentValues)
    {
        // TODO: maybe to all the nullptr checking in the handleManager? + check for InvalidHandle?
        // -> no if/else here
        const auto it = initialValues.find(cmp.first);
        if (it != initialValues.end())
        {
            this->entityManager.addInitializedComponent(entityID, cmp.first, it->second);
            found.push_back(cmp.first);
        }
        else if (cmp.second != Handles::InvalidHandle)
        {
            this->entityManager.addInitializedComponent(entityID, cmp.first, cmp.second);
        }
        else
        {
            // using default value
            this->entityManager.addComponent(entityID, cmp.first);
        }
    }

    if (found.size() != initialValues.size())
    {
        for (const auto initial : initialValues)
        {
            if (std::find(found.begin(), found.end(), initial.first) == found.end())
            {
                Error::errTerminate("Blueprint",
                                    name,
                                    "doesn't contain component",
                                    Components::NameFromComponendID[initial.first],
                                    "which should have been overwritten");
            }
        }
        Error::errTerminate("Blueprint", name, "didn't contain some components which should have been overwritten");
    }

    return entityID;
}
