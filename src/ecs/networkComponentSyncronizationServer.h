/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef NETWORKCOMPONENTSYNCRONIZATIONSERVER
#define NETWORKCOMPONENTSYNCRONIZATIONSERVER 1

#include <unordered_map>
#include "INetworkComponentSyncronization.h"
#include "componentSerialization.h"
#include "components.h"
#include "systems/systemBase.h"

class ReplicaManager3BT;

class NetworkComponentSyncronizationServer : public INetworkComponentSyncronization, public SystemBase
{
   public:
    NetworkComponentSyncronizationServer(ReplicaManager3BT* const replicaManager,
                                         EntityManager& entityManager_,
                                         Handles::HandleManager& handleManager_,
                                         const std::vector<IComponentSerializerBase*> componentSerializers_,
                                         ThreadPool& threadPool_,
                                         EventManager& eventManager_,
                                         const size_t numberOfPlayers);

    NetworkComponentSyncronizationServer(const NetworkComponentSyncronizationServer&) = delete;
    NetworkComponentSyncronizationServer& operator=(const NetworkComponentSyncronizationServer&) = delete;

    virtual ~NetworkComponentSyncronizationServer() final;

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle) final;

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID) final;

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime) final;

    // ----------------------------------------------------------------------------
    // Replica functions
    // ----------------------------------------------------------------------------

    virtual RakNet::RM3QuerySerializationResult
    QuerySerialization(RakNet::Connection_RM3* destinationConnection) final;

    virtual RakNet::RM3SerializationResult
    Serialize(RakNet::SerializeParameters* /* serializeParameters */) final;

    virtual void Deserialize(RakNet::DeserializeParameters* deserializeParameters) final;

   private:
    ReplicaManager3BT* const replicaManager3BT;

    std::vector<Serialization::Buffer_t> deserializedComponentBuffer;

    std::vector<IComponentSerializerBase*> componentSerializers;

    std::array<std::unordered_map<entityID_t, Handles::Handle>, static_cast<size_t>(Components::NetworkPriority::BasePriority::NumPriorities)> trackedEntities;

    /// @brief which level currently gets serialized at. Starts at Low so every component of every
    /// entity gets serialized on the first tick.
    Components::NetworkPriority::BasePriority currentSerializationLevel =
        Components::NetworkPriority::BasePriority::Low;

    /// @brief which level the different players will be serialized at next and wether they have
    /// been serialized already or not.
    std::vector<std::pair<Components::NetworkPriority::BasePriority, bool>> serializationLevels;

    /// @brief serializes all components for all tracked entities at the specified priority.
    /// @param playerID the player who will receive the data. Will be used to determine the priority
    /// of specific entities (e.g. because they are visible on his screen)
    /// @param priority priority of the serialization. Only components for entities which priority
    /// matches (after modifiers, see 'playerID') EXACTLY will be written into the stream.
    /// @param outputBitstream
    void serializeComponents(const size_t playerID,
                             const Components::NetworkPriority::BasePriority priority,
                             RakNet::BitStream* const outputBitstream);
};

#endif /* ifndef NETWORKCOMPONENTSYNCRONIZATIONSERVER */
