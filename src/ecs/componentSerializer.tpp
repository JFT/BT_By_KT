/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

template <typename ComponentType>
componentID_t IComponentSerializer<ComponentType>::getComponentID() const
{
    return Components::ComponentID<ComponentType>::CID;
}

template <typename ComponentType>
Serialization::SerializationVersion_t IComponentSerializer<ComponentType>::getSerializationVersion() const
{
    return Serialization::ComponentSerialization::SerializationVersion<ComponentType>::value;
}

template <typename ComponentType>
void TrivialComponentSerializer<ComponentType>::serializeForEntity(std::vector<Serialization::Buffer_t>& buffer,
                                                                   const entityID_t entity,
                                                                   const EntityManager& etm,
                                                                   const Handles::HandleManager& hm) const
{
    POW2_ASSERT(etm.hasComponent<ComponentType>(entity));
    const auto handle = etm.getComponent<ComponentType>(entity);
    const auto& value = hm.getAsRef<ComponentType>(handle);
    Serialization::ComponentSerialization::serialize<ComponentType>(buffer, value);
}

template <typename ComponentType>
void TrivialComponentSerializer<ComponentType>::deserializeForEntity(const std::vector<Serialization::Buffer_t>& buffer,
                                                                     size_t& pos,
                                                                     const entityID_t entity,
                                                                     EntityManager& etm,
                                                                     Handles::HandleManager& hm) const
{
    const auto value = Serialization::ComponentSerialization::deserialize<ComponentType>(buffer, pos);
    if (etm.hasComponent<ComponentType>(entity))
    {
        const auto handle = etm.getComponent<ComponentType>(entity);
        hm.getAsRef<ComponentType>(handle) = value;
    }
    else
    {
        etm.addInitializedComponent<ComponentType>(entity, &value);
    }
}
