/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "entityManager.h"

#include "componentID.h"
#include "storageSystems/componentStorage.h"
#include "systems/systemBase.h"
#include "../utils/Handle.h"
#include "../utils/debug.h"
#include "../utils/static/error.h"

EntityManager::EntityManager()
    : components(std::vector<SingleComponent>(Components::NumComponents))
{
}

EntityManager::~EntityManager()
{
    std::vector<entityID_t> toDelete;
    for (entityID_t entity = 0; entity < this->entities.size(); ++entity)
    {
        if (this->entities[entity].isActive)
        {
            toDelete.emplace_back(entity);
        }
    }

    for (const auto& entity : toDelete)
    {
        this->deleteEntity(entity);
    }

    for (auto& storage : this->components)
    {
        if (storage.valueStore != nullptr)
        {
            delete storage.valueStore;
        }
    }

    for (const auto& system : this->systemsToUpdate)
    {
        delete system;
    }
}

componentID_t EntityManager::defineComponent(std::unique_ptr<IComponentValueStorage> valueStore)
{
    componentID_t newComponentID = valueStore->getComponentID();

    POW2_ASSERT(newComponentID < this->components.size());

    if (this->components[newComponentID].valueStore != nullptr)
    {
        Error::errContinue("Component with ID",
                           newComponentID,
                           "already defined with name",
                           this->components[newComponentID].valueStore->getComponentName());
        return ECS::INVALID_COMPONENT;
    }

    if (newComponentID > this->components.size())
    {
        this->components.resize(newComponentID);
    }

    this->components[newComponentID].valueStore = valueStore.release();
    POW2_ASSERT(this->components[newComponentID].valueStore->getComponentName() ==
                Components::NameFromComponendID[newComponentID]);

    debugOutLevel(Debug::DebugLevels::stateInit,
                  "defined component",
                  this->components[newComponentID].valueStore->getComponentName(),
                  "with id",
                  newComponentID);

    return newComponentID;
}

std::vector<componentID_t> EntityManager::getDefinedComponentIDs() const
{
    std::vector<componentID_t> definedComponents;
    for (const auto& component : this->components)
    {
        if (component.valueStore)
        {
            definedComponents.push_back(component.valueStore->getComponentID());
        }
    }
    return definedComponents;
}

void EntityManager::bindSystem(std::unique_ptr<SystemBase>& system)
{
    if (not system)
    {
        return;
    }

    for (const auto boundComponentID : system->boundComponents)
    {
        POW2_ASSERT(boundComponentID < this->components.size());
        if (this->components[boundComponentID].valueStore != nullptr)
        {
            this->components[boundComponentID].boundSystems.push_back(system.get());
            debugOutLevel(Debug::DebugLevels::stateInit,
                          "bound system at",
                          &system,
                          "to component",
                          Components::NameFromComponendID[boundComponentID],
                          "with id",
                          boundComponentID);
        }
        else
        {
            Error::errContinue("tried to bind to component",
                               Components::NameFromComponendID[boundComponentID],
                               "which doesn't have any accociated value storage");
        }
    }

    this->systemsToUpdate.emplace_back(system.get());
    system.release();
}

void EntityManager::updateSystems(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    for (auto* const system : this->systemsToUpdate)
    {
        system->update(tickNumber, scriptEngine, deltaTime);
    }
}

std::vector<SystemBase*> EntityManager::getBoundSystems() const
{
    std::vector<SystemBase*> systems;
    std::copy(this->systemsToUpdate.begin(), this->systemsToUpdate.end(), std::back_inserter(systems));
    return systems;
}

componentID_t EntityManager::getComponentIDFromName(const std::string& componentName) const
{
    for (size_t cid = 0; cid < this->components.size(); cid++)
    {
        if (this->components[cid].valueStore != nullptr &&
            this->components[cid].valueStore->getComponentName().compare(componentName) == 0)
        {
            return cid;
        }
    }
    Error::errContinue("No component", componentName, "found!");
    return ECS::INVALID_COMPONENT;
}

std::string EntityManager::getComponentNameFromID(const componentID_t componentID) const
{
    if (componentID >= this->components.size())
    {
        Error::errContinue("Searching for name of invalid componentID", componentID);
        return std::string("INVALID_COMPONENT");
    }
    return Components::NameFromComponendID[componentID];
}

entityID_t EntityManager::getMaximumEntityID() const
{
    for (int maxActiveEntityID = static_cast<int>(this->entities.size()) - 1; maxActiveEntityID >= 0; --maxActiveEntityID)
    {
        if (this->entities[static_cast<size_t>(maxActiveEntityID)].isActive)
        {
            return static_cast<entityID_t>(maxActiveEntityID);
        }
    }
    return ECS::INVALID_ENTITY;
}

bool EntityManager::entityExists(const entityID_t entityID) const
{
    return entityID < this->entities.size() && this->entities[entityID].isActive;
}

bool EntityManager::createdByServer(const entityID_t entityID) const
{
    POW2_ASSERT(entityID < this->entities.size());
    return this->entities[entityID].createdByServer;
}

entityID_t EntityManager::addEntity(const bool createdByServer)
{
    if (not this->nextEntityIDToUse.empty())
    {
        entityID_t reuseID;
        // The server defaults to re-using the earliest freed entityID.
        // The client defaults to re-using the latest freed entityID.
        // This raises the odds that, if data about an entity which doesn't exist on the client but
        // on the server is received by the client
        // it didn't re-use that entityID already (which would mean copyEntity() has to be called to
        // free that entityID).
        if (createdByServer)
        {
            reuseID = nextEntityIDToUse.front();
            nextEntityIDToUse.pop_front();
        }
        else
        {
            reuseID = nextEntityIDToUse.back();
            nextEntityIDToUse.pop_back();
        }
        POW2_ASSERT(this->entities[reuseID].components == Components::ComponentFlag_t()); // reused entity has no components
        POW2_ASSERT(!this->entities[reuseID].isActive);
        this->entities[reuseID].isActive = true;
        this->entities[reuseID].createdByServer = createdByServer;
        return reuseID;
    }
    else
    {
        const entityID_t newEntityID = this->entities.size();
        Entity newEntity;
        newEntity.components = Components::ComponentFlag_t();
        newEntity.isActive = true;
        newEntity.createdByServer = createdByServer;
        this->entities.emplace_back(std::move(newEntity));

        return newEntityID;
    }
}

void EntityManager::addEntity(const entityID_t toUse, const bool createdByServer)
{
    if (toUse >= this->entities.size())
    {
        this->entities.resize(toUse + 1);
    }
    POW2_ASSERT(this->entities[toUse].components == Components::ComponentFlag_t()); // reused entity has no components
    this->entities[toUse].isActive = true;
    this->entities[toUse].createdByServer = createdByServer;
}

std::vector<entityID_t> EntityManager::entitiesWithComponent(const componentID_t cid) const
{
    std::vector<entityID_t> ret;
    for (entityID_t id = 0; id < this->entities.size(); ++id)
    {
        if (!this->entities[id].isActive) continue;
        if (Components::hasComponent(this->entities[id].components, cid))
        {
            ret.push_back(id);
        }
    }
    return ret;
}

void EntityManager::deleteEntity(const entityID_t entityID)
{
    POW2_ASSERT(this->entities[entityID].isActive);
    // first notify all systems and only then remove all components.
    // this is better because
    // 1) systems can use other components to speed up the removal process
    // 2) componentFlags value for the entity has to be changed only once (instead of after every
    // component)
    for (const auto componentID : Components::getComponentIDArrayFromFlag(this->entities[entityID].components))
    {
        for (auto* bs : this->components[componentID].boundSystems)
        {
            bs->untrackEntity(entityID, componentID);
        }
    }
    for (const auto componentID : Components::getComponentIDArrayFromFlag(this->entities[entityID].components))
    {
        POW2_ASSERT(this->components[componentID].valueStore != nullptr);
        this->components[componentID].valueStore->removeComponent(entityID);
    }
    this->entities[entityID].components = Components::ComponentFlag_t();
    this->entities[entityID].isActive = false;
    this->nextEntityIDToUse.push_back(entityID);
}

entityID_t EntityManager::copyEntity(const entityID_t entityID)
{
    const entityID_t newEntity = this->addEntity();
    POW2_ASSERT(this->entityExists(entityID));
    for (const auto cid : Components::getComponentIDArrayFromFlag(this->entities[entityID].components))
    {
        const auto oldValueHandle = this->getComponent(entityID, cid);
        this->addInitializedComponent(newEntity, cid, oldValueHandle);
    }
    return newEntity;
}

Handles::Handle EntityManager::addComponent(const entityID_t entityID, const componentID_t componentID)
{
    return this->addInitializedComponent(entityID, componentID, nullptr);
}

Handles::Handle EntityManager::addInitializedComponent(const entityID_t entityID,
                                                       const componentID_t componentID,
                                                       const Handles::Handle copyFromHandle)
{
    if (entityID >= this->entities.size())
    {
        Error::errTerminate("Tried to add component", componentID, "to INVALID entity id", entityID);
    }
    if (componentID >= this->components.size())
    {
        Error::errTerminate("Tried to add INVALID component", componentID, "to entity id", entityID);
    }

    const Handles::Handle newComponentHandle =
        this->components[componentID].valueStore->addComponent(entityID, copyFromHandle);

    const Components::ComponentFlag_t oldFlags = this->entities[entityID].components;
    this->entities[entityID].components = Components::addComponent(oldFlags, componentID);

    debugOutLevel(Debug::DebugLevels::updateLoop, "added component", componentID, "to entity", entityID, "and got component handle", newComponentHandle);

    for (SystemBase* bs : this->components[componentID].boundSystems)
    {
        // debugOutLevel(Debug::DebugLevels::updateLoop, "added component", componentID, "to
        // entity", entityID, "and got component handle", newComponentHandle);
        bs->trackEntity(entityID, componentID, newComponentHandle);
    }

    return newComponentHandle;
}

Handles::Handle EntityManager::addInitializedComponent(const entityID_t entityID,
                                                       const componentID_t componentID,
                                                       const void* const initialData)
{
    if (entityID >= this->entities.size())
    {
        Error::errTerminate("Tried to add component", componentID, "to INVALID entity id", entityID);
    }

    POW2_ASSERT(componentID < this->components.size() && this->components[componentID].valueStore != nullptr);

    const Handles::Handle newComponentHandle =
        this->components[componentID].valueStore->addComponent(entityID, initialData);

    const Components::ComponentFlag_t oldFlags = this->entities[entityID].components;
    this->entities[entityID].components = Components::addComponent(oldFlags, componentID);

    debugOutLevel(Debug::DebugLevels::updateLoop, "added component", componentID, "to entity", entityID, "and got component handle", newComponentHandle);

    for (SystemBase* bs : this->components[componentID].boundSystems)
    {
        // debugOutLevel(Debug::DebugLevels::updateLoop, "added component", componentID, "to
        // entity", entityID, "and got component handle", newComponentHandle);
        bs->trackEntity(entityID, 0, newComponentHandle);
    }

    return newComponentHandle;
}

void EntityManager::copyComponent(const Handles::Handle handleBase,
                                  const Handles::Handle handleToCopy,
                                  const componentID_t componentID)
{
    this->components[componentID].valueStore->copyComponent(handleBase, handleToCopy);
}

void EntityManager::removeComponent(const entityID_t entityID, const componentID_t componentID)
{
    if (entityID >= this->entities.size())
    {
        Error::errTerminate("Tried to remove component", componentID, "of INVALID entity id", entityID);
    }
    if (componentID >= this->components.size())
    {
        Error::errTerminate("Tried to remove INVALID component", componentID, "of entity id", entityID);
    }

    for (SystemBase* b : this->components[componentID].boundSystems)
    {
        b->untrackEntity(entityID, componentID);
    }

    // the handle will be marked as invalid by the componentValueStore.
    this->components[componentID].valueStore->removeComponent(entityID);
    const Components::ComponentFlag_t oldFlags = this->entities[entityID].components;
    this->entities[entityID].components = Components::removeComponent(oldFlags, componentID);
}


Handles::Handle EntityManager::createFreeValue(const componentID_t componentID)
{
    if (componentID >= this->components.size() || this->components[componentID].valueStore == nullptr)
    {
        Error::errTerminate("Tried to create INVALID free component", componentID);
    }

    return this->components[componentID].valueStore->createFreeValue();
}

void EntityManager::destroyFreeValue(const componentID_t componentID, const Handles::Handle handle)
{
    if (componentID >= this->components.size() || this->components[componentID].valueStore == nullptr)
    {
        Error::errTerminate("Tried to remove INVALID free component", componentID);
    }
    if (handle == Handles::InvalidHandle)
    {
        Error::errTerminate("Tried to remove a free component value for component", componentID, "but supplied an invalid Handle!");
    }

    return this->components[componentID].valueStore->destroyFreeValue(handle);
}


Handles::Handle EntityManager::getComponent(const entityID_t entityID, const componentID_t componentID) const
{
    if (componentID >= this->components.size())
    {
        Error::errContinue("Requested invalid component id", componentID, "of entity", entityID);
        return Handles::InvalidHandle;
    }
    if (entityID >= this->entities.size())
    {
        Error::errContinue("Requested components of invalid entity id", entityID);
        return Handles::InvalidHandle;
    }

    POW2_ASSERT(Components::hasComponent(this->entities[entityID].components, componentID));

    // note: the handle might still be invalid. But this must be found out by looking the handle up
    // in the handle-array and testing for validity there
    return this->components[componentID].valueStore->getComponent(entityID);
}

bool EntityManager::hasComponent(const entityID_t entityID, const componentID_t component) const
{
    return Components::hasComponent(this->entities[entityID].components, component);
}

void EntityManager::printEntityComponents(const entityID_t entityID)
{
    std::string out = "\n";

    for (const auto& component : Components::getComponentIDArrayFromFlag(this->entities[entityID].components))
    {
        out += "\t";
        out += Components::NameFromComponendID.at(component);
        out += "\n";
    }
    debugOut("Components of Entity", entityID, out);
}
