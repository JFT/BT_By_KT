/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "testSystem.h"

#include "../components.h"
#include "../entityManager.h"
#include "../events/eventManager.h"
#include "../../map/gamemap.h" // Randam Walk
#include "../../utils/HandleManager.h"

TestSystem::TestSystem(ThreadPool& threadPool_,
                       EventManager& eventManager_,
                       EntityManager& entityManager_,
                       Handles::HandleManager& handleManager_,
                       GameMap* const gameMap_)
    : SystemBase(Systems::SystemIdentifiers::TestSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , dist()
{
    // Randam Walk
    this->gameMap = gameMap_;
    this->dist = std::uniform_real_distribution<float>(
        0, 0.9999f * gameMap->getMapSize().X); // using 0.9999 * size because hitting the exact
                                               // border of the map leads to invalid accesses

    // Randam Walk
    m_subscriptions.push_back(this->eventManager.subscribe<Events::FinalWaypointReached>(
        [this](const Events::FinalWaypointReached event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::FinalWaypointReached, event);
        }));
}

void TestSystem::trackEntity(const entityID_t entityID, const componentID_t internalCompomentID, const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);
    POW2_ASSERT(this->handleManager.isValid(handle));

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    /*
    if (!this->entityManager.hasComponent< ? >(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all
    needed Components available"); return;
    }

    TrackedEntityHandleCollection newEntry;
    newEntry.? = this->entityManager.getComponent<Components::?>(entityID);

    this->trackedEntities[entityID] = newEntry;
    */
}

void TestSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void TestSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    (void) deltaTime;

    if (!this->systemIsActive)
    {
        this->receivedEvents.clear();
        return;
    }

    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void TestSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::FinalWaypointReached: // Randam Walk
            {
                const auto event = this->receivedEvents.readNextEvent<Events::FinalWaypointReached>(ap);
                // maybe check trackedEntities.containsEntity
                if (not this->entityManager.hasComponent<Components::Position, Components::Movement>(
                        event.entityID))
                {
                    break;
                }
                const Handles::Handle positionHandle =
                    this->entityManager.getComponent<Components::Position>(event.entityID);
                auto& pos = this->handleManager.getAsRef<Components::Position>(positionHandle).position;
                const Handles::Handle movementHandle =
                    this->entityManager.getComponent<Components::Movement>(event.entityID);
                auto& movement = this->handleManager.getAsRef<Components::Movement>(movementHandle);

                // generate a list of random movement targets
                irr::core::vector2df target(dist(twister), dist(twister));
                movement.waypoints = gameMap->getPath(pos, target);
            }
            break;
            default:
            {
                Error::errTerminate("In TestSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}
