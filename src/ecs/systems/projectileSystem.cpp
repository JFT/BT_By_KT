/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "projectileSystem.h"

#include <iostream> //TODO: remove if unnececary
#include <string>

#include "../components.h"
#include "../entityManager.h"
#include "../events/eventManager.h"
#include "../../core/game.h" //TODO: maybe make this code only build if debugging pathing?
#include "../../map/gamemap.h"
#include "../../map/pathGrid.h"
#include "../../utils/HandleManager.h"
#include "../../utils/timeMeasurement.h"

ProjectileSystem::ProjectileSystem(ThreadPool& threadPool_,
                                   EventManager& eventManager_,
                                   EntityManager& entityManager_,
                                   Handles::HandleManager& handleManager_,
                                   GameMap* const gameMap_)
    : SystemBase(Systems::SystemIdentifiers::ProjectileSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , gameMap(gameMap_)
{
    this->boundComponents.push_back(Components::ComponentID<Components::Position>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::ProjectileMovement>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Damage>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::Teleport>([this](const auto& event) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::Teleport, event);
    }));
}

void ProjectileSystem::trackEntity(const entityID_t entityID,
                                   const componentID_t internalCompomentID,
                                   const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    if (!this->entityManager.hasComponent<Components::Position, Components::ProjectileMovement, Components::Damage>(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all needed Components available");
        return;
    }

    TrackedEntityHandleCollection newEntry;
    newEntry.positionHandle = this->entityManager.getComponent<Components::Position>(entityID);
    newEntry.movementHandle = this->entityManager.getComponent<Components::ProjectileMovement>(entityID);
    newEntry.damageHandle = this->entityManager.getComponent<Components::Damage>(entityID);
    const auto projectileMovement =
        this->handleManager.getAsValue<Components::ProjectileMovement>(newEntry.movementHandle);

    newEntry.targetEntityID = projectileMovement.targetEntity;
    if (!this->entityManager.hasComponent<Components::Position>(newEntry.targetEntityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "Can't tracking entity",
                      entityID,
                      "target entity",
                      newEntry.targetEntityID,
                      "has no position Component");
        return;
    }
    newEntry.targetPositionHandle =
        this->entityManager.getComponent<Components::Position>(newEntry.targetEntityID);

    this->trackedEntities[entityID] = newEntry;
}

void ProjectileSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void ProjectileSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    TimeMeasurement timing;

    // First need to check if any projectiles need to be detached
    // (Teleport will remove all unit tracking projectiles)
    this->handleReceivedTeleportationEvents();

    constexpr irr::f32 targetHitDistanceSQ = 10.0f;
    for (auto& entry : this->trackedEntities)
    {
        auto& handleCollection = entry.second;

        const auto& movement =
            this->handleManager.getAsRef<Components::ProjectileMovement>(handleCollection.movementHandle);
        auto& position =
            this->handleManager.getAsRef<Components::Position>(handleCollection.positionHandle).position;

        const auto targetPosition = this->handleManager
                                        .getAsValue<Components::Position>(handleCollection.targetPositionHandle)
                                        .position;

        irr::f32 distanceSQ = (position - targetPosition).getLengthSQ();

        if (distanceSQ < movement.speed * deltaTime * movement.speed * deltaTime)
        {
            // will reach the target in this tick
            position = targetPosition;
        }
        else
        {
            const auto direction = (targetPosition - position).normalize();
            position += direction * movement.speed * deltaTime;
        }

        distanceSQ = (position - targetPosition).getLengthSQ();
        if (distanceSQ < targetHitDistanceSQ)
        {
            const auto damage =
                this->handleManager.getAsValue<Components::Damage>(handleCollection.damageHandle).damage;
            eventManager.emit(Events::TakeDamage(handleCollection.targetEntityID, damage));
            // Dont remove elements from trackedEntities here,
            // because we are currently iterating over trackedEntities
            this->entityIDsToDelete.push_back(entry.first);
        }
    }

    this->deletePreviouslyMarkedProjectiles();

    const auto durationMS = timing.durationMS();
    this->updateLoopTiming.addMeasurement(durationMS);
    if (outputEvery <= 0)
    {
        std::cout << "update loop timing " << this->updateLoopTiming.getPerformanceResult()
                  << " last: " << durationMS << std::endl;
        outputEvery = 31;
    }
    outputEvery--;
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void ProjectileSystem::handleReceivedTeleportationEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            // Don't add another Event to the Mailbox. It is important to ensure that
            // no other Event will treated for the TeleportationEvents
            case InternalEventTypes::Teleport:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::Teleport>(ap);
                // remove all unit tracking projectiles
                for (const auto& entry : this->trackedEntities)
                {
                    if (event.entityID == entry.second.targetEntityID)
                    {
                        // Dont remove elements from trackedEntities here,
                        // because we are currently iterating over trackedEntities
                        this->entityManager.deleteEntity(entry.first);
                    }
                }
            }
            break;
            default:
            {
                Error::errTerminate("In ProjectileSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }

    this->deletePreviouslyMarkedProjectiles();

    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

void ProjectileSystem::deletePreviouslyMarkedProjectiles()
{
    for (const auto entityID : this->entityIDsToDelete)
    {
        this->entityManager.deleteEntity(entityID);
    }
    this->entityIDsToDelete.clear();
}
