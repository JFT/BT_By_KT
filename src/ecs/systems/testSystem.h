/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TEST_SYSTEM_H_
#define TEST_SYSTEM_H_

#include <ctime>  // Randam Walk
#include <random> // Randam Walk
#include <unordered_map>

#include "systemBase.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}

class GameMap; // Randam Walk

class TestSystem : public SystemBase
{
   public:
    TestSystem(ThreadPool& threadPool_,
               EventManager& eventManager_,
               EntityManager& entityManager_,
               Handles::HandleManager& handleManager_,
               GameMap* const gameMap_);

    TestSystem(const TestSystem&) = delete;
    TestSystem operator=(const TestSystem&) = delete;

    virtual ~TestSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

    void activateSystem() { this->systemIsActive = true; }
    void deactivateSystem() { this->systemIsActive = false; }

   private:
    bool systemIsActive = true;
    struct TrackedEntityHandleCollection
    {
    };
    std::unordered_map<entityID_t, TrackedEntityHandleCollection> trackedEntities = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        FinalWaypointReached = 0, // Randam Walk
        NumInternalEvents,
    };

    void handleReceivedEvents();

    // Randam Walk BEGIN //
    GameMap* gameMap = nullptr;

    std::mt19937 twister = std::mt19937(12);
    std::uniform_real_distribution<float> dist;
    // Randam Walk END //
};

#endif /* TEST_SYSTEM_H_ */
