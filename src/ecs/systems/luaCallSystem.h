/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "systemBase.h"

class ScriptingModule;
namespace sol
{
    class state;
}

/// @brief this class makes it possible to define systems
/// which call lua code in their update() functions (even multithreaded)
/// and which can actually be defined completely inside lua, including constructor
/// and destructor calls.
class LuaCallSystem : public SystemBase
{
   public:
    /// @brief create system, calls the lua function
    /// [luaBaseName]constructor(boundComponents&) IN ALL LUA INSTANCES!
    /// make sure to write bound components only once (use 'if(inSingleInstance) then').
    /// Calling it multiple times makes sure additional initialization data
    /// can be written in all instances.
    /// [luaBaseName]constructor() must exist in ALL LUA INSTANCES,
    /// otherwise BT terminates throwing an error.
    /// @param identifier_
    /// @param threadPool_
    /// @param eventManager_
    /// @param luaBaseName this name defines which lua functions are called.
    /// They are:
    ///
    /// [luaBaseName]constructor(std::vector& boundComponents)
    /// - (see above), MUST EXIST IN ALL LUA INSTANCES
    /// [luaBaseName]destructor()
    /// - called only if it exists from inside the C++ destructor.
    ///   existance is checked in all lua instances independendly
    /// [luaBaseName]updateMT(const uint32_t tickNumber, const float deltaTime)
    /// - if it exists in ScriptEngine:singeInstance()
    ///   this function is called in parallel in multiple lua instances
    /// - if it exists it must exist in ALL LUA INSTANCES
    /// [luaBaseName]update(const uint32_t tickNumber, const float deltaTime)
    /// - default update function, called if updateMT doesn't exist.
    /// - can only exist in ScriptEngine:singleInstance(),
    ///   as it is only called in that instance.
    ///
    /// The existence of the functions is checked before calling,
    /// which allows them to be (un)defined at runtime,
    /// as long as the calling rules are obeyed.
    /// @param scriptingModule
    LuaCallSystem(const Systems::SystemIdentifiers identifier_,
                  ThreadPool& threadPool_,
                  EventManager& eventManager_,
                  const std::string& luaBaseName,
                  ScriptingModule& scriptingModule);

    virtual ~LuaCallSystem();

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

    /// @brief multithreaded update version of update(),
    /// enqueued by update() with multiple sol::states if the function {luaBaseName}updateMT exists.
    /// @param tickNumber
    /// @param state {luaBaseName}updateMT is called in this state
    /// @param deltaTime
    void updateMT(const uint32_t tickNumber, sol::state& state, const float deltaTime);

   private:
    const std::string luaBaseName;
    ScriptEngine& scriptEngine;
};
