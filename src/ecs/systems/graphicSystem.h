/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include <unordered_map>

#include "systemBase.h"

#include "../../utils/Pow2Assert.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}
namespace grid
{
    class Grid;
}
class Game;
class GameMap;


class GraphicSystem : public SystemBase
{
   public:
    GraphicSystem(ThreadPool& threadPool_,
                  EventManager& eventManager_,
                  EntityManager& entityManager_,
                  Handles::HandleManager& handleManager_,
                  GameMap* const gameMap_,
                  const grid::Grid& grid_,
                  Game& game_);

    GraphicSystem(const GraphicSystem&) = delete;
    GraphicSystem operator=(const GraphicSystem&) = delete;

    virtual ~GraphicSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);
    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

   private:
    struct TrackedEntityHandleCollection
    {
        Handles::Handle graphicH = Handles::InvalidHandle;
        Handles::Handle positionH = Handles::InvalidHandle;
        Handles::Handle rotationH =
            Handles::InvalidHandle; // TODO: check if this is actually needed. Currently the
                                    // rotation handle is fetched in update() whenever it is needed.
    };
    std::unordered_map<entityID_t, TrackedEntityHandleCollection> trackedEntities = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        ChangeGraphicScale = 0,
        NumInternalEvents,
    };

    void handleReceivedEvents();

    GameMap* gameMap = nullptr;
    const grid::Grid& grid;

    Game& game;

    ScriptEngine* se = nullptr;
};
