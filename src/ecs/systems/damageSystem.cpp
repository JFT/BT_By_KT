/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "damageSystem.h"

#include "../componentID.h"
#include "../components.h"
#include "../events/eventManager.h"
#include "../../utils/HandleManager.h"
#include "../../utils/static/error.h"


DamageSystem::DamageSystem(ThreadPool& threadPool_, EventManager& eventManager_, Handles::HandleManager& handleManager_)
    : SystemBase(Systems::SystemIdentifiers::DamageSystem, threadPool_, eventManager_)
    , handleManager(handleManager_)
{
    this->boundComponents.push_back(Components::ComponentID<Components::Physical>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::TakeDamage>([this](const auto& event) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::TakeDamage, event);
    }));
    m_subscriptions.push_back(this->eventManager.subscribe<Events::TakeHeal>([this](const auto& e) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::TakeHeal, e);
    }));
}

void DamageSystem::trackEntity(const entityID_t entityID,
                               const componentID_t internalCompomentID,
                               const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    POW2_ASSERT(handle != Handles::InvalidHandle);

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "already tracking entity", entityID);
        return;
    }

    this->trackedEntities[entityID] = handle;
}

void DamageSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void DamageSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    (void) deltaTime;

    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void DamageSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::TakeDamage:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::TakeDamage>(ap);
                this->handleTakeDamageEvent(event.targetEntityID, event.amount);
            }
            break;
            case InternalEventTypes::TakeHeal:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::TakeHeal>(ap);
                this->handleGetHealEvent(event.targetEntityID, event.amount);
            }
            break;
            default:
            {
                Error::errTerminate("In DamageSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

void DamageSystem::handleTakeDamageEvent(const entityID_t targetEntityID, const float amount)
{
    if (!this->trackedEntities.count(targetEntityID))
    {
        Error::errContinue("Damage Event on untracked entity", targetEntityID);
        return;
    }
    const auto handle = this->trackedEntities[targetEntityID];
    auto& physical = this->handleManager.getAsRef<Components::Physical>(handle);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "Damage", amount, "physical.health", physical.health);
    physical.health -= amount;
    this->eventManager.emit(Events::HealthChanged{targetEntityID});
    if (physical.health <= 0)
    {
        this->eventManager.emit(Events::HealthBelowZero{targetEntityID});
    }
}

void DamageSystem::handleGetHealEvent(const entityID_t targetEntityID, const float amount)
{
    if (!this->trackedEntities.count(targetEntityID))
    {
        Error::errContinue("Heal Event on untracked entity", targetEntityID);
        return;
    }
    const auto handle = this->trackedEntities[targetEntityID];
    auto& physical = this->handleManager.getAsRef<Components::Physical>(handle);
    physical.health += amount;
    this->eventManager.emit(Events::HealthChanged{targetEntityID});
    if (physical.health > physical.maxHealth)
    {
        physical.health = physical.maxHealth;
    }
}
