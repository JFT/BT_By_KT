/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "pathingSystem.h"

#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/ISceneCollisionManager.h>
#include <irrlicht/ISceneManager.h>

#include "../components.h"
#include "../entityManager.h"
#include "../events/eventManager.h"
#include "../../core/game.h"
#include "../../grid/grid.h"
#include "../../map/gamemap.h"
#include "../../utils/HandleManager.h"
#include "../../utils/aabbox2d.h" // allows using screen coordinate boxes like 3d aabboxes

PathingSystem::PathingSystem(ThreadPool& threadPool_,
                             EventManager& eventManager_,
                             EntityManager& entityManager_,
                             Handles::HandleManager& handleManager_,
                             GameMap* const gameMap_,
                             grid::Grid& grid_,
                             Game& game_)
    : SystemBase(Systems::SystemIdentifiers::PathingSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , gameMap(gameMap_)
    , grid(grid_)
    , game(game_)
{
    m_subscriptions.push_back(this->eventManager.subscribe<Events::FindPathTo>(
        [this](const auto& event) { this->receivedEvents.receiveThreadedEvent(0, event); }));
}

void PathingSystem::trackEntity(const entityID_t entityID,
                                const componentID_t internalCompomentID,
                                const Handles::Handle handle)
{
    assert(false);
}

void PathingSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    assert(false);
}

void PathingSystem::update(const uint32_t tickNumber, ScriptEngine&, const float)
{
    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void PathingSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "trying to read event with internal type",
                      static_cast<size_t>(type));
        assert(type == 0);
        {
            const auto event = this->receivedEvents.readNextEvent<Events::FindPathTo>(ap);
            const auto entity = event.entityID;

            Handles::Handle posHandle = this->entityManager.getComponent<Components::Position>(entity);
            Handles::Handle moveHandle = this->entityManager.getComponent<Components::Movement>(entity);

            auto path = gameMap->getPath(handleManager.getAsRef<Components::Position>(posHandle).position,
                                         event.target);
            Components::Movement& mvcomp = handleManager.getAsRef<Components::Movement>(moveHandle);
            mvcomp.waypoints = std::move(path);
            eventManager.emit(Events::MovementTargetChanged{entity, event.target});
        }
    }
    this->receivedEvents.clear();
}
