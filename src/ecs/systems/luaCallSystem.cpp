/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "luaCallSystem.h"
#include "../../scripting/scriptingModule.h"
#include "../../utils/threadPool.hpp"

LuaCallSystem::LuaCallSystem(const Systems::SystemIdentifiers identifier_,
                             ThreadPool& threadPool_,
                             EventManager& eventManager_,
                             const std::string& luaBaseName_,
                             ScriptingModule& scriptingModule)
    : SystemBase(identifier_, threadPool_, eventManager_)
    , luaBaseName(luaBaseName_)
    , scriptEngine(scriptingModule.getScriptEngine())

{
    debugOutLevel(Debug::DebugLevels::stateInit + 1, ":C++ctor of LuaCallSystem", static_cast<void*>(this));
    // even if currenlty looping over something in lua
    // we need to force the calling of ctors in all instances
    // because the system ctor will only ever be called once
    scriptingModule.getScriptEngine().loopOverInstances(
        [this](sol::state& instance) {
            sol::function ctor = instance[this->luaBaseName + "constructor"];
            if (not ctor.valid())
            {
                Error::errTerminate("constructor ",
                                    "'" + this->luaBaseName + "constructor' doesn't exist in lua instance",
                                    static_cast<void*>(&instance),
                                    "did you forget to define it beforehand?");
            }
            ctor.call<void>(std::ref(this->boundComponents));
        },
        ScriptEngine::ForceLoop::True);
    debugOutLevel(Debug::DebugLevels::stateInit + 1, "end C++ctor of LuaCallSystem");
}

LuaCallSystem::~LuaCallSystem()
{
    this->scriptEngine.loopOverInstances(
        [this](sol::state& instance) {
            sol::function dtor = instance[this->luaBaseName + "destructor"];
            if (dtor.valid())
            {
                dtor.call<void>(std::ref(this->boundComponents));
            }
        },
        ScriptEngine::ForceLoop::True);
}

void LuaCallSystem::trackEntity(const entityID_t entityID,
                                const componentID_t internalCompomentID,
                                const Handles::Handle handle)
{
}

void LuaCallSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
}

void LuaCallSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    // if multithreaded update is possible use that
    if (static_cast<sol::protected_function>(scriptEngine.singleInstance()[luaBaseName + "updateMT"])
            .valid())
    {
        std::vector<std::future<void>> results;

        scriptEngine.loopOverInstances([&results, this, tickNumber, deltaTime](sol::state& instance) {
            results.push_back(this->threadPool.enqueue(
                static_cast<void (LuaCallSystem::*)(const uint32_t, sol::state&, const float)>(&LuaCallSystem::updateMT),
                this,
                tickNumber,
                std::ref(instance),
                deltaTime));
        });

        for (auto& result : results)
        {
            result.wait();
        }
    }
    else
    {
        sol::protected_function update = scriptEngine.singleInstance()[luaBaseName + "update"];
        if (not update.valid())
        {
            Error::errContinue("Could neither find a lua function '" + this->luaBaseName +
                               "update or '" + this->luaBaseName + "MT_update'!");
        }
        sol::protected_function_result result = update(tickNumber, deltaTime);
        {
            if (not result.valid())
            {
                sol::error err = result;
                Error::errTerminate("Error:", err.what());
            }
        }
    }
}

void LuaCallSystem::updateMT(const uint32_t tickNumber, sol::state& instance, const float deltaTime)
{
    sol::protected_function update = instance[this->luaBaseName + "updateMT"];
    sol::protected_function_result result = update(tickNumber, deltaTime);
    if (not result.valid())
    {
        sol::error err = result;
        Error::errTerminate("Error:", err.what());
    }
}
