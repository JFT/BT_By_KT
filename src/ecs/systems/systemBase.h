/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef BASE_SYSTEM_H
#define BASE_SYSTEM_H

#include <memory>
#include <string>
#include <vector>

#include "systemIdentifier.h"
#include "../entity.h"
#include "../events/eventID.h"
#include "../events/eventMailbox.h"
#include "../events/eventManager.h"
#include "../events/events.h"
#include "../../utils/Handle.h"

class ThreadPool;
class ScriptEngine;

class SystemBase
{
   public:
    SystemBase(const Systems::SystemIdentifiers identifier_, ThreadPool& threadPool_, EventManager& eventManager_)
        : identifier(identifier_)
        , threadPool(threadPool_)
        , eventManager(eventManager_)
        , receivedEvents(*(new EventMailbox(threadPool_))){};

    virtual ~SystemBase() { delete &this->receivedEvents; };
    /// @brief called by the entityManager if an entity gets any of the 'boundComponents' as a new
    /// component
    /// @param entityID
    /// @param internalCompomentID index into the 'boundComponents' array which component was added
    /// to the entity
    /// @param handle handle to the value (via the HandleManager) of the new component
    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle) = 0;

    /// @brief called by the entityManager if ANY of the 'boundComponents' get removed from the
    /// entity.
    /// The system must decide if it stops tracking the entity or continues to track the entity
    /// (maybe if it uses more than one component)
    /// @param entityID
    /// @param internalCompomentID index into the 'boundComponents' array which component was
    /// removed from the entity
    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID) = 0;

    /// @brief do some operation on tracked entities / handle messages, etc...
    /// update() will be called once per game-tick
    /// @param tickNumber the current tick
    /// @param deltaTime how much realtime is elapsed since the last tick
    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime) = 0;

    /// @brief components bound to this System. If any of the components are added to an entity this
    /// system will start tracking that entity
    /// If the component is removed again the system will be asked to untrack that entity
    std::vector<componentID_t> boundComponents = {};

    /// @brief used during loading/saving to select the correct system for events to be loaded
    const Systems::SystemIdentifiers identifier;

   protected:
    friend class TrivialSystemSaver;

    /// @brief ThreadPool allows tasks to be pushed to a taskqueue that is handled by the threads in
    /// the pool.
    ThreadPool& threadPool;

    /// @brief The EventManager allows to subscribe for events and emit them
    EventManager& eventManager;
    std::vector<std::unique_ptr<EventManager::Subscription>> m_subscriptions;

    /// @brief The EventMailbox can be used by systems to easily receive multiple threaded events
    /// and read them out linearly. Abstracts the buffer resizing and addressing away.
    EventMailbox& receivedEvents;
};

#endif // BASE_SYSTEM_H
