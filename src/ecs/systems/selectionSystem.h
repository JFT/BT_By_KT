/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <irrlicht/aabbox3d.h>
#include <irrlicht/position2d.h> // can't be forward declared because it is a typedef (even though only references/pointers are used in this header)
#include <unordered_map>
#include <vector>

#include "systemBase.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}
namespace grid
{
    class Grid;
}
class InputContextManager;
class Game;
class GameMap;
namespace irr
{
    struct SEvent;
} // namespace irr

class SelectionSystem : public SystemBase
{
   public:
    SelectionSystem(ThreadPool& threadPool_,
                    EventManager& eventManager_,
                    EntityManager& entityManager_,
                    Handles::HandleManager& handleManager_,
                    InputContextManager& inputContextManager_,
                    GameMap* const gameMap_,
                    grid::Grid& grid_,
                    Game& game_);

    SelectionSystem(const SelectionSystem&) = delete;
    SelectionSystem operator=(const SelectionSystem&) = delete;

    virtual ~SelectionSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

    template <typename T> // definition after class definiton
    void receiveEvent(const size_t internalEventID, const T& event);

   private:
    std::unordered_map<entityID_t, Handles::Handle> trackedEntities = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        PartialEntitySelection = 0,
        EntityDeselected,
        NumInternalEvents,
    };

    void handleReceivedEvents();
    bool handleRightClick(const irr::SEvent& event);
    void handleSelectionDone(const aabbox2di& area);
    irr::core::vector3df getGraphicCoord3d(const irr::core::position2di& vec, const irr::f32 zValue) const;
    bool get2dBoxFromWorldBox(const irr::core::aabbox3df& worldBox, aabbox2di& out) const;
    irr::core::aabbox3df graphicCoordsBoxFromWorldBox(const irr::core::aabbox3df& box) const;

    /// @brief using a selection box to highlight or actually select entities inside the box
    /// @param emitSelectedEvent actually select entities (emit a 'PartialEntitySelection' event) or
    /// only visually highlight them
    /// @param box box inside which entities might need selecting
    void highlightOrSelectionSquare(const bool emitSelectedEvent, const aabbox2di& box) const;

    InputContextManager& inputContextManager;

    GameMap* gameMap = nullptr;
    grid::Grid& grid;

    Game& game;


    float minimalSelectionPercentage = 0.3f;

    std::vector<entityID_t> selectedEntities = {};
};
