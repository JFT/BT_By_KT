/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include <irrlicht/aabbox3d.h>
#include <irrlicht/position2d.h> // can't be forward declared because it is a typedef (even though only references/pointers are used in this header)
#include <unordered_map>

#include "systemBase.h"

#include "../../utils/Pow2Assert.h"
#include "../../utils/aabbox2d.h" // allows using screen coordinate boxes like 3d aabboxes

// forward declarations
namespace Handles
{
    class HandleManager;
}

class CooldownSystem : public SystemBase
{
   public:
    CooldownSystem(ThreadPool& threadPool_, EventManager& eventManager_, Handles::HandleManager& handleManager_);

    CooldownSystem(const CooldownSystem&) = delete;
    CooldownSystem operator=(const CooldownSystem&) = delete;

    virtual ~CooldownSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

   private:
    std::unordered_map<entityID_t, Handles::Handle> trackedEntities = {};

    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        StartCooldown = 0,
        NumInternalEvents,
    };

    void handleReceivedEvents();
    void receiveStartCooldown(const entityID_t entityID);

    std::vector<std::vector<entityID_t>> startEvents;
};
