// linearProgram.h is adopted from multiple files of the ORCA RVO v2.0 Library
// http://gamma.cs.unc.edu/RVO2
// https://github.com/snape/RVO2
// RVO2 is released under the Apache License Version 2.0 (see licenses/apache)

/*
 * RVO2 Library
 *
 * Copyright 2008 University of North Carolina at Chapel Hill
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please send all bug reports to <geom@cs.unc.edu>.
 *
 * The authors may be contacted via:
 *
 * Jur van den Berg, Stephen J. Guy, Jamie Snape, Ming C. Lin, Dinesh Manocha
 * Dept. of Computer Science
 * 201 S. Columbia St.
 * Frederick P. Brooks, Jr. Computer Science Bldg.
 * Chapel Hill, N.C. 27599-3175
 * United States of America
 *
 * <http://gamma.cs.unc.edu/RVO2/>
 */


#pragma once

#include <vector>

#include <irrlicht/vector2d.h>


namespace LinearPrograms
{
    /// @brief Defines a directed line.
    struct Line
    {
        Line()
            : point()
            , direction()
        {
        }

        /// @brief A point on the directed line.
        irr::core::vector2df point;
        irr::core::vector2df direction;
    };

    /// @brief A sufficiently small positive number.
    static constexpr float RVO_EPSILON = 0.00001f;

    /// @relates    irr::core::vector2df
    /// @brief      Computes the determinant of a two-dimensional square matrix with rows consisting
    /// of the specified two-dimensional vectors.
    /// @param      vector1         The top row of the two-dimensional square matrix.
    /// @param      vector2         The bottom row of the two-dimensional square matrix.
    /// @return     The determinant of the two-dimensional square matrix.
    inline float getDeterminant(const irr::core::vector2df& vector1, const irr::core::vector2df& vector2)
    {
        return vector1.X * vector2.Y - vector1.Y * vector2.X;
    }

    static bool linearProgram1(const std::vector<Line>& lines,
                               size_t lineNo,
                               float radius,
                               const irr::core::vector2df& optVelocity,
                               bool directionOpt,
                               irr::core::vector2df& result);
    static size_t linearProgram2(const std::vector<Line>& lines,
                                 const size_t numLines,
                                 float radius,
                                 const irr::core::vector2df& optVelocity,
                                 bool directionOpt,
                                 irr::core::vector2df& result);
    static void linearProgram3(const std::vector<Line>& lines,
                               const size_t numLines,
                               size_t numObstLines,
                               size_t beginLine,
                               float radius,
                               irr::core::vector2df& result);
} // namespace LinearPrograms

#include "linearProgram.tpp"
