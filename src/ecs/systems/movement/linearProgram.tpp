// linearProgram.tpp is adopted from multiple files of the ORCA RVO v2.0 Library
// http://gamma.cs.unc.edu/RVO2
// https://github.com/snape/RVO2
// RVO2 is released under the Apache License Version 2.0 (see licenses/apache)

/*
 * Agent.cpp
 * RVO2 Library
 *
 * Copyright 2008 University of North Carolina at Chapel Hill
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please send all bug reports to <geom@cs.unc.edu>.
 *
 * The authors may be contacted via:
 *
 * Jur van den Berg, Stephen J. Guy, Jamie Snape, Ming C. Lin, Dinesh Manocha
 * Dept. of Computer Science
 * 201 S. Columbia St.
 * Frederick P. Brooks, Jr. Computer Science Bldg.
 * Chapel Hill, N.C. 27599-3175
 * United States of America
 *
 * <http://gamma.cs.unc.edu/RVO2/>
 */

#include <algorithm>
#include <cstddef> // defines ptrdiff_t

bool LinearPrograms::linearProgram1(const std::vector<Line>& lines,
                                    size_t lineNo,
                                    float radius,
                                    const irr::core::vector2df& optVelocity,
                                    bool directionOpt,
                                    irr::core::vector2df& result)
{
    const float dotProduct = lines[lineNo].point.dotProduct(lines[lineNo].direction);
    const float discriminant = dotProduct * dotProduct + radius * radius - lines[lineNo].point.getLengthSQ();

    if (discriminant < 0.0f)
    {
        /* Max speed circle fully invalidates line lineNo. */
        return false;
    }

    const float sqrtDiscriminant = std::sqrt(discriminant);
    float tLeft = -dotProduct - sqrtDiscriminant;
    float tRight = -dotProduct + sqrtDiscriminant;

    for (size_t i = 0; i < lineNo; ++i)
    {
        const float denominator = getDeterminant(lines[lineNo].direction, lines[i].direction);
        const float numerator = getDeterminant(lines[i].direction, lines[lineNo].point - lines[i].point);

        if (std::fabs(denominator) <= RVO_EPSILON)
        {
            /* Lines lineNo and i are (almost) parallel. */
            if (numerator < 0.0f)
            {
                return false;
            }
            else
            {
                continue;
            }
        }

        const float t = numerator / denominator;

        if (denominator >= 0.0f)
        {
            /* Line i bounds line lineNo on the right. */
            tRight = std::min(tRight, t);
        }
        else
        {
            /* Line i bounds line lineNo on the left. */
            tLeft = std::max(tLeft, t);
        }

        if (tLeft > tRight)
        {
            return false;
        }
    }

    if (directionOpt)
    {
        /* Optimize direction. */
        if (optVelocity.dotProduct(lines[lineNo].direction) > 0.0f)
        {
            /* Take right extreme. */
            result = lines[lineNo].point + tRight * lines[lineNo].direction;
        }
        else
        {
            /* Take left extreme. */
            result = lines[lineNo].point + tLeft * lines[lineNo].direction;
        }
    }
    else
    {
        /* Optimize closest point. */
        const float t = lines[lineNo].direction.dotProduct(optVelocity - lines[lineNo].point);

        if (t < tLeft)
        {
            result = lines[lineNo].point + tLeft * lines[lineNo].direction;
        }
        else if (t > tRight)
        {
            result = lines[lineNo].point + tRight * lines[lineNo].direction;
        }
        else
        {
            result = lines[lineNo].point + t * lines[lineNo].direction;
        }
    }

    return true;
}

size_t LinearPrograms::linearProgram2(const std::vector<Line>& lines,
                                      const size_t numLines,
                                      float radius,
                                      const irr::core::vector2df& optVelocity,
                                      bool directionOpt,
                                      irr::core::vector2df& result)
{
    if (directionOpt)
    {
        /*
         * Optimize direction. Note that the optimization velocity is of unit
         * length in this case.
         */
        result = optVelocity * radius;
    }
    else if (optVelocity.getLengthSQ() > radius * radius)
    {
        /* Optimize closest point and outside circle. */
        result = optVelocity / optVelocity.getLength() * radius;
    }
    else
    {
        /* Optimize closest point and inside circle. */
        result = optVelocity;
    }

    for (size_t i = 0; i < numLines; ++i)
    {
        if (getDeterminant(lines[i].direction, lines[i].point - result) > 0.0f)
        {
            /* Result does not satisfy constraint i. Compute new optimal result. */
            const irr::core::vector2df tempResult = result;

            if (!linearProgram1(lines, i, radius, optVelocity, directionOpt, result))
            {
                result = tempResult;
                return i;
            }
        }
    }

    return numLines;
}

void LinearPrograms::linearProgram3(const std::vector<Line>& lines,
                                    size_t numLines,
                                    size_t numObstLines,
                                    size_t beginLine,
                                    float radius,
                                    irr::core::vector2df& result)
{
    float distance = 0.0f;

    std::vector<Line> projLines(lines.begin(), lines.begin() + static_cast<ptrdiff_t>(numObstLines));
    projLines.resize(numLines);

    for (size_t i = beginLine; i < numLines; ++i)
    {
        if (getDeterminant(lines[i].direction, lines[i].point - result) > distance)
        {
            /* Result does not satisfy constraint of line i. */
            // std::vector<Line> projLines(lines.begin(), lines.begin() + static_cast<ptrdiff_t>(numObstLines));

            for (size_t j = numObstLines; j < i; ++j)
            {
                Line line;

                float determinant = getDeterminant(lines[i].direction, lines[j].direction);

                if (std::fabs(determinant) <= RVO_EPSILON)
                {
                    /* Line i and line j are parallel. */
                    if (lines[i].direction.dotProduct(lines[j].direction) > 0.0f)
                    {
                        /* Line i and line j point in the same direction. */
                        continue;
                    }
                    else
                    {
                        /* Line i and line j point in opposite direction. */
                        line.point = 0.5f * (lines[i].point + lines[j].point);
                    }
                }
                else
                {
                    line.point = lines[i].point +
                        (getDeterminant(lines[j].direction, lines[i].point - lines[j].point) / determinant) *
                            lines[i].direction;
                }

                line.direction = (lines[j].direction - lines[i].direction).normalize();
                projLines[j] = line;
            }

            const size_t numProjLines = numObstLines + i; // the above loop add i lines

            const irr::core::vector2df tempResult = result;

            if (linearProgram2(projLines,
                               numProjLines,
                               radius,
                               irr::core::vector2df(-lines[i].direction.Y, lines[i].direction.X),
                               true,
                               result) < numProjLines)
            {
                /* This should in principle not happen.  The result is by definition
                 * already in the feasible region of this linear program. If it fails,
                 * it is due to small floating point error, and the current result is
                 * kept.
                 */
                result = tempResult;
            }

            distance = getDeterminant(lines[i].direction, lines[i].point - result);
        }
    }
}
