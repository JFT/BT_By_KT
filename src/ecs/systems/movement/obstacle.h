// obstacle.h is adopted from multiple files of the ORCA RVO v2.0 Library
// http://gamma.cs.unc.edu/RVO2
// https://github.com/snape/RVO2
// RVO2 is released under the Apache License Version 2.0 (see licenses/apache)

/*
 * RVO2 Library
 *
 * Copyright 2008 University of North Carolina at Chapel Hill
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please send all bug reports to <geom@cs.unc.edu>.
 *
 * The authors may be contacted via:
 *
 * Jur van den Berg, Stephen J. Guy, Jamie Snape, Ming C. Lin, Dinesh Manocha
 * Dept. of Computer Science
 * 201 S. Columbia St.
 * Frederick P. Brooks, Jr. Computer Science Bldg.
 * Chapel Hill, N.C. 27599-3175
 * United States of America
 *
 * <http://gamma.cs.unc.edu/RVO2/>
 */

#pragma once

#include <irrlicht/vector2d.h>

class Obstacle
{
   public:
    size_t nextObstacle = static_cast<size_t>(-1);
    size_t prevObstacle = static_cast<size_t>(-1);
    bool isConvex = false;
    irr::core::vector2df point = irr::core::vector2df(0.0f, 0.0f);
    irr::core::vector2df unitDir = irr::core::vector2df(0.0f, 0.0f);
};
