// rvo2.h is adopted from multiple files of the ORCA RVO v2.0 Library
// http://gamma.cs.unc.edu/RVO2
// https://github.com/snape/RVO2
// RVO2 is released under the Apache License Version 2.0 (see licenses/apache)

/*
 * RVO2 Library
 *
 * Copyright 2008 University of North Carolina at Chapel Hill
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please send all bug reports to <geom@cs.unc.edu>.
 *
 * The authors may be contacted via:
 *
 * Jur van den Berg, Stephen J. Guy, Jamie Snape, Ming C. Lin, Dinesh Manocha
 * Dept. of Computer Science
 * 201 S. Columbia St.
 * Frederick P. Brooks, Jr. Computer Science Bldg.
 * Chapel Hill, N.C. 27599-3175
 * United States of America
 *
 * <http://gamma.cs.unc.edu/RVO2/>
 */

#include <vector>
#include "../../../grid/grid.h"
#include "../../../utils/optimization.h"

void MovementSystem::computeNewVelocityForEntity(const TrackedEntity& entity,
                                                 const grid::gridCellID_t gridCellID,
                                                 const float deltaTime)
{
    POW2_ASSERT(!m_inGridCells[gridCellID].empty());

    const size_t threadIndex = threadPool.getMappedIndex(std::this_thread::get_id());
    POW2_ASSERT(threadIndex < this->selectedEntities.size());

    if (this->threadCaches[threadIndex].orcaLines == nullptr)
    {
        this->threadCaches[threadIndex].orcaLines = std::make_unique<std::vector<LinearPrograms::Line>>();
    }

    std::vector<LinearPrograms::Line>& orcaLines = *this->threadCaches[threadIndex].orcaLines;
    orcaLines.clear();

// orcaLines.clear();
#if defined(MOVEMENT_SYSTEM_ORCA_STATIC_OBSTACLES)
    /* Create obstacle ORCA lines. */
    numObstLines = 0;
    for (auto cellID : this->grid.get3x3Around(gridCellID))
    {
        if (cellID != grid::InvalidCellID)
        {
            // get3x3Around() contains InvalidCellID entries if the cell didn't have a neighboring
            // cell in a particular direction (because it was at the border of the map)
            numObstLines += this->calculateStaticObstacleORCALines(entity, cellID, deltaTime, orcaLines);
        }
    }
#endif

    const size_t numObstLines = 0; // orcaLines.size();

    // reserve space for all entity ORCA lines
    size_t numEntityLines = 0;
    for (auto cellID : this->grid.get3x3Around(gridCellID))
    {
        if (cellID != grid::InvalidCellID)
        {
            // get3x3Around() contains InvalidCellID entries if the cell didn't have a cellIDing
            // cell in a particular direction (because it was at the border of the map)
            numEntityLines += m_inGridCells[cellID].size();
        }
    }
    POW2_ASSERT(numEntityLines >= 1);
    numEntityLines -= 1; // -1 because the entity doesn't generate an ORCA line for itself [but is
                         // counted in m_inGridCells[cellID].size()]

    if (numObstLines + numEntityLines > orcaLines.size())
    {
        orcaLines.resize(numObstLines + numEntityLines);
    }

    size_t nextOrcaLineIndex = numObstLines;
    /* Create other entities ORCA lines. */
    for (auto cellID : this->grid.get3x3Around(gridCellID))
    {
        if (cellID != grid::InvalidCellID)
        {
            // get3x3Around() contains InvalidCellID entries if the cell didn't have a neighboring
            // cell in a particular direction (because it was at the border of the map)
            nextOrcaLineIndex =
                this->computeNewVelocityForEntityAndList(entity, m_inGridCells[cellID], deltaTime, orcaLines, nextOrcaLineIndex);
        }
    }

    const size_t numOrcaLines = nextOrcaLineIndex;

    Components::Movement& movement = this->handleManager.getAsRef<Components::Movement>(entity.mov);
    // calculate the preferred velocity for the agent
    irr::core::vector2df preferredVelocity = irr::core::vector2df(0.0f, 0.0f);
    if (!movement.waypoints.empty())
    {
        // prefer to move directly to the target
        const auto position = this->handleManager.getAsRef<Components::Position>(entity.pos).position;
        preferredVelocity = movement.waypoints.front() - position;
        preferredVelocity.normalize();
        preferredVelocity *= movement.speed;
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "entity",
                      entity.id,
                      "wants to move from",
                      position,
                      "to",
                      movement.waypoints.front(),
                      "with preferred velocity",
                      preferredVelocity);
        if (movement.movementStatus != Components::Movement::MovementStatus::PathAroundObstacle)
        {
            movement.movementStatus = Components::Movement::MovementStatus::DirectMovementToTarget;
        }
    }

    size_t lineFail = LinearPrograms::linearProgram2(
        orcaLines, numOrcaLines, movement.speed, preferredVelocity, false, movement.newVelocity);

    if (lineFail < numOrcaLines)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 2,
                      "lineFail =",
                      lineFail,
                      "which is smaller than the number of ORCA lines",
                      numOrcaLines,
                      "-> using LinearPrograms::linearProgram3");
        LinearPrograms::linearProgram3(
            orcaLines, numOrcaLines, numObstLines, lineFail, movement.speed, movement.newVelocity);
    }
}

#if defined(MOVEMENT_SYSTEM_ORCA_STATIC_OBSTACLES)
void MovementSystem::calculateStaticObstacleORCALines(const MovementSystem::TrackedEntity& entity,
                                                      const grid::gridCellID_t gridCellID,
                                                      const float deltaTime,
                                                      const LinearPrograms::std::vector<LinearPrograms::Line>& orcaLines)
{
    const Components::Movement& movement = this->handleManager.getAsRef<Components::Movement>(entity.mov);
    POW2_ASSERT(movement.radius > 0.0f); // make sure the components are correctly set
    POW2_ASSERT(movement.speed >= 0.0f);

    size_t numObstLines = 0;

    // const float invTimeHorizonObst = 1.0f / movement.timeHorizonObstacle;
    // using anything bigger than the deltaTime for timeHorizonObstacle seems to cause entities to
    // move into static obstacles for no apparent reason.
    // thus we always use the deltaTime to compute invTimeHorizonObst
    // TODO: maybe remove timeHorizonObstacle completely?
    const float invTimeHorizonObst = 1.0f / deltaTime;

    for (size_t i = 0; i < this->staticORCAObstacles[gridCellID].size(); ++i)
    {
        const Obstacle* obstacle1 = &this->staticORCAObstacles[gridCellID][i];
        const Obstacle* obstacle2 = &this->staticORCAObstacles[gridCellID][obstacle1->nextObstacle];

        const irr::core::vector2df relativePosition1 =
            obstacle1->point - this->handleManager.getAsRef<Components::Position>(entity.pos).position;
        const irr::core::vector2df relativePosition2 =
            obstacle2->point - this->handleManager.getAsRef<Components::Position>(entity.pos).position;

        /*
         * Check if velocity obstacle of obstacle is already taken care of by
         * previously constructed obstacle ORCA lines.
         */
        bool alreadyCovered = false;

        for (size_t j = 0; j < orcaLines.size(); ++j)
        {
            if (LinearPrograms::getDeterminant(invTimeHorizonObst * relativePosition1 - orcaLines[j].point,
                                               orcaLines[j].direction) -
                        invTimeHorizonObst * movement.radius >=
                    -LinearPrograms::RVO_EPSILON &&
                LinearPrograms::getDeterminant(invTimeHorizonObst * relativePosition2 - orcaLines[j].point,
                                               orcaLines[j].direction) -
                        invTimeHorizonObst * movement.radius >=
                    -LinearPrograms::RVO_EPSILON)
            {
                alreadyCovered = true;
                break;
            }
        }

        if (alreadyCovered)
        {
            continue;
        }

        /* Not yet covered. Check for collisions. */

        const float distSq1 = relativePosition1.getLengthSQ();
        const float distSq2 = relativePosition2.getLengthSQ();

        const float radiusSq = movement.radius * movement.radius;

        const irr::core::vector2df obstacleVector = obstacle2->point - obstacle1->point;
        const float s = (-relativePosition1).dotProduct(obstacleVector) / obstacleVector.getLengthSQ();
        const float distSqLine = (-relativePosition1 - s * obstacleVector).getLengthSQ();

        LinearPrograms::Line line;

        if (s < 0.0f && distSq1 <= radiusSq)
        {
            /* Collision with left vertex. Ignore if non-convex. */
            if (obstacle1->isConvex)
            {
                line.point = irr::core::vector2df(0.0f, 0.0f);
                line.direction =
                    (irr::core::vector2df(-relativePosition1.Y, relativePosition1.X)).normalize();
                orcaLines.push_back(line);
            }

            continue;
        }
        else if (s > 1.0f && distSq2 <= radiusSq)
        {
            /* Collision with right vertex. Ignore if non-convex
             * or if it will be taken care of by neighoring obstace */
            if (obstacle2->isConvex && LinearPrograms::getDeterminant(relativePosition2, obstacle2->unitDir) >= 0.0f)
            {
                line.point = irr::core::vector2df(0.0f, 0.0f);
                line.direction =
                    (irr::core::vector2df(-relativePosition2.Y, relativePosition2.X)).normalize();
                orcaLines.push_back(line);
            }

            continue;
        }
        else if (s >= 0.0f && s < 1.0f && distSqLine <= radiusSq)
        {
            /* Collision with obstacle segment. */
            line.point = irr::core::vector2df(0.0f, 0.0f);
            line.direction = -obstacle1->unitDir;
            orcaLines.push_back(line);
            continue;
        }

        /*
         * No collision.
         * Compute legs. When obliquely viewed, both legs can come from a single
         * vertex. Legs extend cut-off line when nonconvex vertex.
         */

        irr::core::vector2df leftLegDirection, rightLegDirection;

        if (s < 0.0f && distSqLine <= radiusSq)
        {
            /*
             * Obstacle viewed obliquely so that left vertex
             * defines velocity obstacle.
             */
            if (!obstacle1->isConvex)
            {
                /* Ignore obstacle. */
                continue;
            }

            obstacle2 = obstacle1;

            const float leg1 = std::sqrt(distSq1 - radiusSq);
            leftLegDirection =
                irr::core::vector2df(relativePosition1.X * leg1 - relativePosition1.Y * movement.radius,
                                     relativePosition1.X * movement.radius + relativePosition1.Y * leg1) /
                distSq1;
            rightLegDirection =
                irr::core::vector2df(relativePosition1.X * leg1 + relativePosition1.Y * movement.radius,
                                     -relativePosition1.X * movement.radius + relativePosition1.Y * leg1) /
                distSq1;
        }
        else if (s > 1.0f && distSqLine <= radiusSq)
        {
            /*
             * Obstacle viewed obliquely so that
             * right vertex defines velocity obstacle.
             */
            if (!obstacle2->isConvex)
            {
                /* Ignore obstacle. */
                continue;
            }

            obstacle1 = obstacle2;

            const float leg2 = std::sqrt(distSq2 - radiusSq);
            leftLegDirection =
                irr::core::vector2df(relativePosition2.X * leg2 - relativePosition2.Y * movement.radius,
                                     relativePosition2.X * movement.radius + relativePosition2.Y * leg2) /
                distSq2;
            rightLegDirection =
                irr::core::vector2df(relativePosition2.X * leg2 + relativePosition2.Y * movement.radius,
                                     -relativePosition2.X * movement.radius + relativePosition2.Y * leg2) /
                distSq2;
        }
        else
        {
            /* Usual situation. */
            if (obstacle1->isConvex)
            {
                const float leg1 = std::sqrt(distSq1 - radiusSq);
                leftLegDirection =
                    irr::core::vector2df(relativePosition1.X * leg1 - relativePosition1.Y * movement.radius,
                                         relativePosition1.X * movement.radius + relativePosition1.Y * leg1) /
                    distSq1;
            }
            else
            {
                /* Left vertex non-convex; left leg extends cut-off line. */
                leftLegDirection = -obstacle1->unitDir;
            }

            if (obstacle2->isConvex)
            {
                const float leg2 = std::sqrt(distSq2 - radiusSq);
                rightLegDirection =
                    irr::core::vector2df(relativePosition2.X * leg2 + relativePosition2.Y * movement.radius,
                                         -relativePosition2.X * movement.radius + relativePosition2.Y * leg2) /
                    distSq2;
            }
            else
            {
                /* Right vertex non-convex; right leg extends cut-off line. */
                rightLegDirection = obstacle1->unitDir;
            }
        }

        /*
         * Legs can never point into neighboring edge when convex vertex,
         * take cutoff-line of neighboring edge instead. If velocity projected on
         * "foreign" leg, no constraint is added.
         */

        const Obstacle* const leftNeighbor = &this->staticORCAObstacles[gridCellID][obstacle1->prevObstacle];

        bool isLeftLegForeign = false;
        bool isRightLegForeign = false;

        if (obstacle1->isConvex && LinearPrograms::getDeterminant(leftLegDirection, -leftNeighbor->unitDir) >= 0.0f)
        {
            /* Left leg points into obstacle. */
            leftLegDirection = -leftNeighbor->unitDir;
            isLeftLegForeign = true;
        }

        if (obstacle2->isConvex && LinearPrograms::getDeterminant(rightLegDirection, obstacle2->unitDir) <= 0.0f)
        {
            /* Right leg points into obstacle. */
            rightLegDirection = obstacle2->unitDir;
            isRightLegForeign = true;
        }

        /* Compute cut-off centers. */
        const irr::core::vector2df leftCutoff = invTimeHorizonObst *
            (obstacle1->point - this->handleManager.getAsRef<Components::Position>(entity.pos).position);
        const irr::core::vector2df rightCutoff = invTimeHorizonObst *
            (obstacle2->point - this->handleManager.getAsRef<Components::Position>(entity.pos).position);
        const irr::core::vector2df cutoffVec = rightCutoff - leftCutoff;

        /* Project current velocity on velocity obstacle. */

        /* Check if current velocity is projected on cutoff circles. */
        const float t =
            (obstacle1 == obstacle2 ?
                 0.5f :
                 ((movement.velocity - leftCutoff).dotProduct(cutoffVec)) / cutoffVec.getLengthSQ());
        const float tLeft = (movement.velocity - leftCutoff).dotProduct(leftLegDirection);
        const float tRight = (movement.velocity - rightCutoff).dotProduct(rightLegDirection);

        if ((t < 0.0f && tLeft < 0.0f) || (obstacle1 == obstacle2 && tLeft < 0.0f && tRight < 0.0f))
        {
            /* Project on left cut-off circle. */
            const irr::core::vector2df unitW = (movement.velocity - leftCutoff).normalize();

            line.direction = irr::core::vector2df(unitW.Y, -unitW.X);
            line.point = leftCutoff + movement.radius * invTimeHorizonObst * unitW;
            orcaLines.push_back(line);
            continue;
        }
        else if (t > 1.0f && tRight < 0.0f)
        {
            /* Project on right cut-off circle. */
            const irr::core::vector2df unitW = (movement.velocity - rightCutoff).normalize();

            line.direction = irr::core::vector2df(unitW.Y, -unitW.X);
            line.point = rightCutoff + movement.radius * invTimeHorizonObst * unitW;
            orcaLines.push_back(line);
            continue;
        }

        /*
         * Project on left leg, right leg, or cut-off line, whichever is closest
         * to velocity.
         */
        const float distSqCutoff = ((t < 0.0f || t > 1.0f || obstacle1 == obstacle2) ?
                                        std::numeric_limits<float>::infinity() :
                                        (movement.velocity - (leftCutoff + t * cutoffVec)).getLengthSQ());
        const float distSqLeft =
            ((tLeft < 0.0f) ? std::numeric_limits<float>::infinity() :
                              (movement.velocity - (leftCutoff + tLeft * leftLegDirection)).getLengthSQ());
        const float distSqRight =
            ((tRight < 0.0f) ?
                 std::numeric_limits<float>::infinity() :
                 (movement.velocity - (rightCutoff + tRight * rightLegDirection)).getLengthSQ());

        if (distSqCutoff <= distSqLeft && distSqCutoff <= distSqRight)
        {
            /* Project on cut-off line. */
            line.direction = -obstacle1->unitDir;
            line.point = leftCutoff +
                movement.radius * invTimeHorizonObst *
                    irr::core::vector2df(-line.direction.Y, line.direction.X);
            orcaLines.push_back(line);
            continue;
        }
        else if (distSqLeft <= distSqRight)
        {
            /* Project on left leg. */
            if (isLeftLegForeign)
            {
                continue;
            }

            line.direction = leftLegDirection;
            line.point = leftCutoff +
                movement.radius * invTimeHorizonObst *
                    irr::core::vector2df(-line.direction.Y, line.direction.X);
            orcaLines.push_back(line);
            continue;
        }
        else
        {
            /* Project on right leg. */
            if (isRightLegForeign)
            {
                continue;
            }

            line.direction = -rightLegDirection;
            line.point = rightCutoff +
                movement.radius * invTimeHorizonObst *
                    irr::core::vector2df(-line.direction.Y, line.direction.X);
            orcaLines.push_back(line);
            continue;
        }
    } // for (size_t i = 0; i < this->staticORCAObstacles[gridCellID].size(); ++i)
}
#endif

size_t MovementSystem::computeNewVelocityForEntityAndList(const MovementSystem::TrackedEntity& entity,
                                                          const std::vector<MovementSystem::TrackedEntity>& inCell,
                                                          const float deltaTime,
                                                          std::vector<LinearPrograms::Line>& orcaLines,
                                                          size_t nextOrcaLineIndex)
{
    const irr::core::vector2df position =
        this->handleManager.getAsRef<Components::Position>(entity.pos).position;
    const Components::Movement& movement = this->handleManager.getAsRef<Components::Movement>(entity.mov);
    POW2_ASSERT(movement.radius > 0.0f); // make sure the components are correctly set
    POW2_ASSERT(movement.timeHorizon > 0.0f);
    POW2_ASSERT(movement.speed >= 0.0f);

    const float invTimeHorizon = 1.0f / movement.timeHorizon;

    for (const auto& other : inCell)
    {
        if (MACRO_UNLIKELY(other.id == entity.id))
        {
            // avoid self-collision
            continue;
        }

        const irr::core::vector2df relativePosition =
            this->handleManager.getAsRef<Components::Position>(other.pos).position - position;
        const Components::Movement& otherMovement =
            this->handleManager.getAsRef<Components::Movement>(other.mov);
        const irr::core::vector2df relativeVelocity = movement.velocity - otherMovement.velocity;
        const float distSq = relativePosition.getLengthSQ();
        const float combinedRadius = movement.radius + otherMovement.radius;
        const float combinedRadiusSq = combinedRadius * combinedRadius;

        LinearPrograms::Line line;
        irr::core::vector2df u;

        if (distSq > combinedRadiusSq)
        {
            /* No collision. */
            const irr::core::vector2df w = relativeVelocity - invTimeHorizon * relativePosition;
            /* Vector from cutoff center to relative velocity. */
            const float wLengthSq = w.getLengthSQ();

            const float dotProduct1 = w.dotProduct(relativePosition);

            if (dotProduct1 < 0.0f && dotProduct1 * dotProduct1 > combinedRadiusSq * wLengthSq)
            {
                /* Project on cut-off circle. */
                const float wLength = w.getLength(); // std::sqrt(wLengthSq);
                const irr::core::vector2df unitW = w / wLength;

                line.direction = irr::core::vector2df(unitW.Y, -unitW.X);
                u = (combinedRadius * invTimeHorizon - wLength) * unitW;
                // u = combinedRadius * invTimeHorizon * unitW - w;
            }
            else
            {
                /* Project on legs. */
                const float leg = std::sqrt(distSq - combinedRadiusSq);

                if (LinearPrograms::getDeterminant(relativePosition, w) > 0.0f)
                {
                    /* Project on left leg. */
                    line.direction =
                        irr::core::vector2df(relativePosition.X * leg - relativePosition.Y * combinedRadius,
                                             relativePosition.X * combinedRadius + relativePosition.Y * leg) /
                        distSq;
                }
                else
                {
                    /* Project on right leg. */
                    line.direction =
                        -irr::core::vector2df(relativePosition.X * leg + relativePosition.Y * combinedRadius,
                                              -relativePosition.X * combinedRadius +
                                                  relativePosition.Y * leg) /
                        distSq;
                }

                const float dotProduct2 = relativeVelocity.dotProduct(line.direction);

                u = dotProduct2 * line.direction - relativeVelocity;
            }
        } // if (distSq > combinedRadiusSq)
        else
        {
            /* Collision. Project on cut-off circle of time timeStep. */
            const float invTimeStep = 1.0f / deltaTime;

            /* Vector from cutoff center to relative velocity. */
            const irr::core::vector2df w = relativeVelocity - invTimeStep * relativePosition;

            const float wLength = w.getLength();
            const irr::core::vector2df unitW = w / wLength;

            line.direction = irr::core::vector2df(unitW.Y, -unitW.X);
            u = (combinedRadius * invTimeStep - wLength) * unitW;
        } // if (distSq > combinedRadiusSq) {...} else

        line.point = movement.velocity + 0.5f * u;

        //orcaLines.push_back(std::move(line)); // even though std::move is possible using push_back is faster when profiling.
        orcaLines[nextOrcaLineIndex] =
            std::move(line); // even though std::move is possible using push_back is faster when profiling.
        nextOrcaLineIndex++;
    } // for (const auto& other : inCell)

    return nextOrcaLineIndex;
}
