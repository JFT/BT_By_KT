/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MOVEMENT_SYSTEM_H
#define MOVEMENT_SYSTEM_H

// define MOVEMENT_SYSTEM_STATIC_OBSTACLE_CLAMPING to clamp to static obstacles when doing
// movement-calculations
// this system seems very fast but might lead to non-optimal movement of entities in narrow channels
// or around corners
// TODO: test which system is actually faster
#define MOVEMENT_SYSTEM_STATIC_OBSTACLE_CLAMPING
#ifndef MOVEMENT_SYSTEM_STATIC_OBSTACLE_CLAMPING
// otherwise the RVO2 ORCA system is used to also include static obstacles.
// calls to this system are very slow but might lead to better movement into narrow channels or
// around corners
#define MOVEMENT_SYSTEM_ORCA_STATIC_OBSTACLES
#endif

#include <string>

// TODO check still
#include <memory> // std::unique_ptr
#include <random> //TODO: remove: only included for testing to set random movement targetsneeded

#include <irrlicht/SColor.h> // for pathing debug node coloring
#include <irrlicht/matrix4.h>
#include <irrlicht/vector3d.h>

#include "../../systems/systemBase.h"

#include "linearProgram.h"
#include "obstacle.h"
#include "../../../utils/Pow2Assert.h"
#include "../../../utils/optimization.h" // non-initializing allocator + vector
#include "../../../utils/performanceMeasurement.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}
class Game;
class GameMap;
namespace grid
{
    class Grid;
}
struct PathGrid;
namespace Components
{
    struct Movement;
}


class MovementSystem : public SystemBase
{
   public:
    MovementSystem(ThreadPool& threadPool_,
                   EventManager& eventManager_,
                   EntityManager& entityManager_,
                   Handles::HandleManager& handleManager_,
                   GameMap* const gameMap_,
                   const grid::Grid& grid_,
                   Game& game_);

    MovementSystem(const MovementSystem&) = delete;
    MovementSystem operator=(const MovementSystem&) = delete;

    virtual ~MovementSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);


    template <size_t internalEventID, typename T>
    void receiveEvent(const T& event); // definition after class definition

    void receiveSelectionEvent(const entityID_t entityID);

    PerformanceMeasurement<float> updateLoopTiming = PerformanceMeasurement<float>();
    int outputEvery = 30;

    struct TrackedEntity
    {
        entityID_t id;
        Handles::Handle pos;
        Handles::Handle mov;
    };

   private:
    std::vector<TrackedEntity> trackedEntities;

    std::vector<std::vector<TrackedEntity>> m_inGridCells;

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t // TODO make pretty
    {
        WaypointsChanged = 0,
        FinalWaypointReached,
        EntityDeselected,
        RepathRequest,

        NumInternalEvents,

        OwnedEntitySelected,    // selection events have a special event storage
        NotOwnedEntitySelected, // selection events have a special event storage
    };

    void handleReceivedEvents();

    GameMap* gameMap = nullptr;
    const grid::Grid& grid;
    PathGrid& pathGrid;

    // TODO: maybe make this code only build if debugging pathing?
    Game& game;

    /// @brief For entities which have reached their target emit IntermediateWaypointReached
    ///         or FinalWaypointReached events  and update the movement component accordingly.
    void handleTargetAlreadyReached();
    void handleTargetAlreadyReachedIn(const grid::gridCellID_t gridCellID);

    void computeNewVelocities(const float deltaTime);
    void computeNewVelocities(const grid::gridCellID_t gridCellID, const float deltaTime);
    void computeNewVelocityForEntity(const TrackedEntity& entity,
                                     const grid::gridCellID_t gridCellID,
                                     const float deltaTime);

    size_t computeNewVelocityForEntityAndList(const TrackedEntity& entity,
                                              const std::vector<TrackedEntity>& inCell,
                                              const float deltaTime,
                                              std::vector<LinearPrograms::Line>& orcaLines,
                                              size_t nextOrcaLineIndex);

    void applyMovement(const float deltaTime);
    void applyMovement(const grid::gridCellID_t gridCellID, const float deltaTime);

    irr::core::matrix4 updateAngle(irr::core::matrix4 oldMat, irr::core::matrix4 newMat, irr::f32 maxRotDistance);

    std::vector<std::vector<entityID_t>> selectedEntities;
    void showPathForSelectedEntities() const;

    ScriptEngine* se = nullptr;

#if defined(MOVEMENT_SYSTEM_STATIC_OBSTACLE_CLAMPING)
    irr::core::vector2df moveOutsideStaticObstacles(const irr::core::vector2df oldPosition,
                                                    const irr::core::vector2df newPosition,
                                                    const float radius) const;
#endif

#if defined(MOVEMENT_SYSTEM_ORCA_STATIC_OBSTACLES)
    std::vector<std::vector<Obstacle>> staticORCAObstacles;

    void generateStaticORCAObstacles();

    void showStaticORCAObstacles();

    void calculateStaticObstacleORCALines(const entityID_t storageIndex,
                                          const grid::gridCellID_t getCellID,
                                          const float deltaTime,
                                          const std::vector<LinearPrograms::Line>& orcaLines);
#endif

    // cache arrays for the movement update of the threads
    struct ThreadCache // TODO check still needed
    {
        ThreadCache()
            : twister()
            , dist()
        {
        }

        std::unique_ptr<std::vector<LinearPrograms::Line>> orcaLines = nullptr;
        std::mt19937 twister; // each thread gets it's own random number-generation suite so threads
                              // don't interfere with oneanother
        std::uniform_real_distribution<float> dist;
    };
    std::vector<ThreadCache> threadCaches; // TODO check still needed

    /// @brief to be called if an entity get to the target in its Movement.waypoints.front().
    /// Pops the next target from the queue and throws events (depending on if there are more
    /// targets to move to or if the entity reached its destination)
    /// @param entityID
    /// @param movementComponent the movement component of the entity which reached the waypoint
    void waypointReached(const entityID_t entityID, Components::Movement& movementComponent);

    /// @brief tests all entities vs. all other entities and checks if they collide
    /// VERY SLOW!!! USE ONLY FOR ERROR-FINDING PURPOSES
    /// @return true if at least two entities collide
    bool entitiesColliding();
}; // class MovementSystem : public SystemBase

#endif // MOVEMENT_SYSTEM_H
