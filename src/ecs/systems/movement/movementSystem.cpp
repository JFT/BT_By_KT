/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// operator<< for irr::core::vector must be defined before
// including debug.h therefore do it first to be save.
#include "../../../utils/printfunctions.h"

#include "movementSystem.h"

#include <chrono>
#include <irrlicht/irrMath.h>
#include <random>
#include <string>

#include "../../components.h" // rvo2 uses Components::Movement which is only forward declared in movementSystem.h -> include components.h before rvo2.h
#include "../../entityManager.h"
#include "../../events/eventManager.h"
#include "../../../core/game.h" //TODO: maybe make this code only build if debugging pathing?
#include "../../../map/gamemap.h"
#include "../../../map/pathGrid.h"
#include "../../../utils/HandleManager.h"
#include "../../../utils/algos.h"
#include "../../../utils/threadPool.hpp"

#include "rvo2.h" // need previous includes

namespace
{
    bool closeEnough(const irr::core::vector2df& a, const irr::core::vector2df& b)
    {
        // TODO: fix magic number
        return a.getDistanceFromSQ(b) <= 5;
    }
} // namespace

MovementSystem::MovementSystem(ThreadPool& threadPool_,
                               EventManager& eventManager_,
                               EntityManager& entityManager_,
                               Handles::HandleManager& handleManager_,
                               GameMap* const gameMap_,
                               const grid::Grid& grid_,
                               Game& game_)
    : SystemBase(Systems::SystemIdentifiers::MovementSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , gameMap(gameMap_)
    , grid(grid_)
    , pathGrid(gameMap_->getPathGrid())
    , game(game_) // TODO: maybe only do game/the dists if debugging pathing?
    , selectedEntities(std::vector<std::vector<entityID_t>>(threadPool_.getNumberOfWorkerThreads() + 1))
    // numWorkers + 1 because the main thread may call update() too and needs to be able to cache
    // data
    , threadCaches(std::vector<ThreadCache>(threadPool_.getNumberOfWorkerThreads() + 1))
{
    for (auto& tcache : threadCaches)
    {
        tcache.dist = std::uniform_real_distribution<float>(0, 0.9999f * this->gameMap->getMapSize().X); // using 0.9999 * size because hitting the exact border of the map leads to invalid accesses
    }

    if (this->grid.sideLength < this->pathGrid.unitSize)
    {
        Error::errTerminate("Grid::sideLength (",
                            this->grid.sideLength,
                            ") must be >= PathGrid::unitSize (",
                            this->pathGrid.unitSize);
    }
#pragma GCC diagnostic ignored "-Wfloat-equal"
    if (std::fmod(this->grid.sideLength, this->pathGrid.unitSize) != 0.0f)
    {
        Error::errTerminate("Grid::sideLength (",
                            this->grid.sideLength,
                            ") must be a multiple of PathGrid::unitSize (",
                            this->pathGrid.unitSize);
    }
#pragma GCC diagnostic pop

#if defined(MOVEMENT_SYSTEM_ORCA_STATIC_OBSTACLES)
    this->generateStaticORCAObstacles();
#endif

    this->boundComponents.push_back(Components::ComponentID<Components::Position>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Movement>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::WaypointsChanged>(
        [this](const Events::WaypointsChanged event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::WaypointsChanged, event);
        }));
    m_subscriptions.push_back(this->eventManager.subscribe<Events::EntityDeselected>(
        [this](const Events::EntityDeselected event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::EntityDeselected, event);
        }));
    m_subscriptions.push_back(
        this->eventManager.subscribe<Events::RepathRequest>([this](const Events::RepathRequest event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::RepathRequest, event);
        }));

    // TODO: maybe make this code only build if debugging pathing?
    m_subscriptions.push_back(this->eventManager.subscribe<Events::OwnedEntitySelected>(
        [this](const auto& e) { this->receiveSelectionEvent(e.entityID); }));
    m_subscriptions.push_back(this->eventManager.subscribe<Events::NotOwnedEntitySelected>(
        [this](const auto& e) { this->receiveSelectionEvent(e.entityID); }));
}

void MovementSystem::trackEntity(const entityID_t entityID,
                                 const componentID_t internalCompomentID,
                                 const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");
    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);

    if (!this->entityManager.hasComponent<Components::Position, Components::Movement>(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all needed Components available");
        return;
    }

    this->trackedEntities.push_back({entityID,
                                     this->entityManager.getComponent<Components::Position>(entityID),
                                     this->entityManager.getComponent<Components::Movement>(entityID)});
}

void MovementSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    auto at = std::find_if(trackedEntities.begin(), trackedEntities.end(), [entityID](const auto& e) {
        return e.id == entityID;
    });
    if (at == trackedEntities.end())
    {
        // not tracking this entity
        return;
    }

    if (trackedEntities.size() == 1)
    {
        // special case: only 1 entity remaining
        trackedEntities.clear();
    }
    else
    {
        // as the order of the tracked entities isn't important replace the entry to remove with the
        // last element in the vector and shorten the vector by 1.
        *at = trackedEntities.back();
        trackedEntities.resize(trackedEntities.size() - 1);
    }
}

std::vector<std::vector<MovementSystem::TrackedEntity>>
sortIntoGridCells(const std::vector<MovementSystem::TrackedEntity>& trackedEntities,
                  const grid::Grid& grid,
                  Handles::HandleManager& hm)
{
    std::vector<std::vector<MovementSystem::TrackedEntity>> sorted(
        static_cast<size_t>(grid.xNumberOfCells) * static_cast<size_t>(grid.zNumberOfCells));

    for (const auto& entity : trackedEntities)
    {
        const auto pos = hm.getAsRef<Components::Position>(entity.pos).position;
        const auto cellID = grid.getCellID(pos);
        sorted[cellID].push_back(entity);
    }

    return sorted;
}


void MovementSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    const auto start = std::chrono::high_resolution_clock::now();

    this->handleReceivedEvents();

    // TODO: put somewhere sensible
    this->se = &scriptEngine;

    m_inGridCells = sortIntoGridCells(trackedEntities, grid, handleManager);

    // the case of a freshly added movement component which has the current entity position as its
    // waypoint must be handled now, otherwise \ref computeNewVelocities() will generate 'NaN's due
    // to position->target distance normalization.
    handleTargetAlreadyReached();

    this->computeNewVelocities(deltaTime);

    this->applyMovement(deltaTime);

    this->showPathForSelectedEntities();

    /*if (this->entitiesColliding())
    {
        Error::errTerminate("entities collided!");
    }*/

    debugOutLevel(Debug::DebugLevels::updateLoop + 1, "resetting dynamic Walkability of the PathGrid");
    this->pathGrid.resetDynamicWalkability();

    debugOutLevel(Debug::DebugLevels::updateLoop, "complete grid updated!");

#if defined(MOVEMENT_SYSTEM_ORCA_STATIC_OBSTACLES)
    this->showStaticORCAObstacles();
#endif

    const auto end = std::chrono::high_resolution_clock::now();
    const auto duration = end - start;
    using ms = std::chrono::duration<float, std::milli>;
    const auto durationMS = std::chrono::duration_cast<ms>(duration).count();
    this->updateLoopTiming.addMeasurement(durationMS);
    if (outputEvery <= 0)
    {
        std::cout << "update loop timing " << this->updateLoopTiming.getPerformanceResult()
                  << " last: " << durationMS << std::endl;
        outputEvery = 31;
    }
    outputEvery--;
}

void MovementSystem::receiveSelectionEvent(const entityID_t entityID)
{
    const size_t threadIndex = threadPool.getMappedIndex(std::this_thread::get_id());
    POW2_ASSERT(threadIndex < this->selectedEntities.size());
    // TODO: maybe check against double-insertion? Or use a set?
    this->selectedEntities[threadIndex].push_back(entityID);
}

void MovementSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::EntityDeselected:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::EntityDeselected>(ap);
                (void) event;
                for (auto& selections : this->selectedEntities)
                {
                    selections.clear();
                }
            }
            break;
            case InternalEventTypes::RepathRequest:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::RepathRequest>(ap);

                if (not this->entityManager.hasComponent<Components::Position>(event.entityID))
                {
                    break;
                }

                auto at = std::find_if(trackedEntities.cbegin(),
                                       trackedEntities.cend(),
                                       [id = event.entityID](const auto& e) { return e.id == id; });
                if (at != trackedEntities.end())
                {
                    // entity wasn't tracked
                    break;
                }
                const auto& position = this->handleManager.getAsRef<Components::Position>(at->pos).position;
                auto& movement = this->handleManager.getAsRef<Components::Movement>(at->mov);
                // some other event might have lead to the deletion of the waypoints
                if (movement.waypoints.empty())
                {
                    break;
                }
                const auto target = movement.waypoints.back();
                movement.waypoints = this->gameMap->getPath(position, target);
                if (!movement.waypoints.empty())
                {
                    // only set the expectedDistanceSQToWaypoint and movementStatus if the
                    // repathing actually generated waypoints
                    movement.expectedDistanceSQToWaypoint =
                        position.getDistanceFromSQ(movement.waypoints.front());
                    movement.movementStatus = Components::Movement::MovementStatus::PathAroundObstacle;
                }
            }
            break;
            default:
                Error::errTerminate("In MovementSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

#if defined(MOVEMENT_SYSTEM_STATIC_OBSTACLE_CLAMPING)
irr::core::vector2df MovementSystem::moveOutsideStaticObstacles(const irr::core::vector2df oldPosition,
                                                                const irr::core::vector2df newPosition,
                                                                const float radius) const
{
    // assert disabled because in the applyMovement function setting the position of the entity
    // directly by the waypoint (if the entity is very close)
    // the waypoint (and thus oldPosition) might overlap with a static object
    // TODO: find way to enable again
    // POW2_ASSERT(!this->pathGrid.collidesWithStaticObject(oldPosition, radius,
    // PathGrid::staticObstacleSize));

    auto correctedPosition = newPosition;

    // get all pathCells this entity could collide with at its new position
    int pathCellXMin = std::max(this->pathGrid.getPathCellX(newPosition.X - radius), 0);
    int pathCellXMax = std::min(this->pathGrid.getPathCellX(newPosition.X + radius),
                                static_cast<int>(this->pathGrid.width - 1));
    // y because vector2df only has X/Y-coordinates, but all functions call the coordinate Z
    // (because that is it's 3d coordinate)
    int pathCellZMin = std::max(this->pathGrid.getPathCellZ(newPosition.Y - radius), 0);
    int pathCellZMax = std::min(this->pathGrid.getPathCellZ(newPosition.Y + radius),
                                static_cast<int>(this->pathGrid.height - 1));


    const auto oldMovement = (newPosition - oldPosition);

    for (int x = pathCellXMin; x <= pathCellXMax; x++)
    {
        for (int z = pathCellZMin; z <= pathCellZMax; z++)
        {
            if (this->pathGrid.get(x, z) != PathGrid::Walkability::StaticNonWalkable)
            {
                continue;
            }
            const auto pathCellCenter = this->pathGrid.getCenterPosition(x, z);
            // scenario:
            // Ox  = oldPosition
            // N   = newPosition
            // |'- = the pathCell
            // ()  = the radius of the entity
            //
            //                             O1
            //
            //     .------------.
            //     |            | .--.   O2
            //     |            |( XX )
            //     |            | '--'
            //     |        N   |
            //     |            |
            //     '------------'
            //
            // find the closest new position (XX) which keeps the entity outside of the obstacle but
            // moves it along it's actual path
            // (otherwise the entity will 'snap' around if the barrier just pushes it out in one
            // direction, e.g)

            constexpr float staticObstacleHalfSize = PathGrid::staticObstacleSize / 2.0f;

            const bool xIntersection =
                std::abs(pathCellCenter.X - correctedPosition.X) - staticObstacleHalfSize < radius;
            const bool zIntersection = std::abs(pathCellCenter.Y - correctedPosition.Y) - staticObstacleHalfSize <
                radius; // z = y because all positions are vector2df (X,Y) but 3d coordinates are (X,Z) (and thus called X,Z everywhere)

            // is the entity colliding with the pathCell?
            if (xIntersection && zIntersection)
            {
                // where to reset the entity to? One of xIntersection or yIntersection must be
                // changed to false.

                // was the entity already inside the pathCell in one dimension but outside the other
                // (e.g. oldPosition = O2 in the example above)
                const bool oldXIntersection =
                    std::abs(pathCellCenter.X - oldPosition.X) - staticObstacleHalfSize < radius;
                const bool oldZIntersection =
                    std::abs(pathCellCenter.Y - oldPosition.Y) - staticObstacleHalfSize < radius;

                const auto clampX = [pathCellCenter, this, radius, oldPosition, staticObstacleHalfSize]() {
                    // which side of the pathCell was the entity on last update?
                    // (because newPosition might be more than halfway through the pathCell
                    // the test needs to be done against the old position)
                    float clampedX = pathCellCenter.X + staticObstacleHalfSize +
                        radius; // default assumption: oldPosition.X > pathCellCenter.X
                    if (oldPosition.X < pathCellCenter.X)
                    {
                        clampedX = pathCellCenter.X - staticObstacleHalfSize - radius;
                    }
                    return clampedX;
                };
                const auto clampZ = [pathCellCenter, this, radius, oldPosition, staticObstacleHalfSize]() {
                    float clampedZ = pathCellCenter.Y + staticObstacleHalfSize +
                        radius; // default assumption: oldPosition.Y > pathCellCenter.Y
                    if (oldPosition.Y < pathCellCenter.Y)
                    {
                        clampedZ = pathCellCenter.Y - staticObstacleHalfSize - radius;
                    }
                    return clampedZ;
                };

                // clamp the coordinate which wasn't intersecting before and allow full movement in
                // the other direction
                if (oldXIntersection)
                {
                    correctedPosition = irr::core::vector2df(correctedPosition.X, clampZ());
                }
                else if (oldZIntersection)
                {
                    correctedPosition = irr::core::vector2df(clampX(), correctedPosition.Y);
                }
                else
                {
                    // entity came e.g. from oldPosition O2 into the static obstacle
                    // clamp depending on which component of the movement is bigger to allow the
                    // entity to continue to move into it's most desired direction
                    //
                    // but also make sure the entity doesn't 'jump' around when clamping (because it
                    // moves faster than it tried to)
                    // -> never allow the length of the clamped movement vector to be larger than
                    // the length of the non-clamped one)
                    if (std::abs(oldMovement.X) > std::abs(oldMovement.Y))
                    {
                        correctedPosition = irr::core::vector2df(correctedPosition.X, clampZ());
                    }
                    else
                    {
                        correctedPosition = irr::core::vector2df(clampX(), correctedPosition.Y);
                    }
                    auto newMovement = correctedPosition - oldPosition;
                    if (newMovement.getLengthSQ() > oldMovement.getLengthSQ())
                    {
                        newMovement = oldMovement.getLength() * newMovement.normalize();
                    }
                    correctedPosition = oldPosition + newMovement;
                }
            }
        } // for (z = pathCellZMin; z < pathCellZMax; z++)
    }     // for (int x = pathCellXMin; x < pathCellXMax; x++)

    // POW2_ASSERT(!this->pathGrid.collidesWithStaticObject(correctedPosition, radius,
    // PathGrid::staticObstacleSize));

    return correctedPosition;
}
#endif


#if defined(MOVEMENT_SYSTEM_ORCA_STATIC_OBSTACLES)
void MovementSystem::generateStaticORCAObstacles()
{
    if (this->gameMap == nullptr)
    {
        this->staticORCAObstacles.resize(0);
        return;
    }

    POW2_ASSERT(std::fmod(this->grid.sideLength, this->pathGrid.unitSize) == 0.0f); // grid + pathGrid must align
    const PathGrid::PathGridDimension_t pathCellsPerGridCellSide =
        this->grid.sideLength / this->pathGrid.unitSize;

    this->staticORCAObstacles.resize(this->grid.xNumberOfCells * this->grid.zNumberOfCells);

    struct ObstacleWrapper
    {
        Obstacle obstacle;
        ObstacleWrapper* next = nullptr;
        ObstacleWrapper* prev = nullptr;
    };

    // each pathCell gets 4 obstacles
    /*
    std::vector<std::vector<std::array<ObstacleWrapper*, 4>>> pathGridObstacles;
    std::vector<std::vector<std::array<Obstacle, 4>>> pathGridObstacles;
    for (size_t x = 0; x < this->pathGrid.width; x++) {
        pathGridObstacles.resize(this->pathGrid.width);
        for (size_t z = 0; z < this->pathGrid.height; z++) {
            pathGridObstacles[x].resize(this->pathGrid.height);
            Obstacle& tr = pathGridObstacles[x][z][0]; // top right
            Obstacle& tl = pathGridObstacles[x][z][1]; // top left
            Obstacle& bl = pathGridObstacles[x][z][2]; // bottom left
            Obstacle& br = pathGridObstacles[x][z][3]; // bottom right

            auto pathCellCenter = irr::core::vector2df(this->pathGrid.unitSize * x,
                    this->pathGrid.unitSize * z);

            tr.point = pathCellCenter + irr::core::vector2df(- this->pathGrid.unitSize * 0.5, -
    this->pathGrid.unitSize * 0.5); tl.point = pathCellCenter + irr::core::vector2df(+
    this->pathGrid.unitSize * 0.5, - this->pathGrid.unitSize * 0.5); bl.point = pathCellCenter +
    irr::core::vector2df(+ this->pathGrid.unitSize * 0.5, + this->pathGrid.unitSize * 0.5); br.point
    = pathCellCenter + irr::core::vector2df(- this->pathGrid.unitSize * 0.5, +
    this->pathGrid.unitSize * 0.5);
        }
    }*/

    // set their position to the center of the pathGrid tile they are in
    for (size_t x = 0; x < this->pathGrid.width; x++)
    {
        for (size_t z = 0; z < this->pathGrid.height; z++)
        {
            if (this->pathGrid.get(x, z) == PathGrid::Walkability::Free)
            {
                continue;
            }

            auto pathCellCenter = this->pathGrid.getCenterPosition(x, z);
            grid::gridCellID_t gridCellID = this->grid.getCellID(pathCellCenter);

            const size_t startIndex = this->staticORCAObstacles[gridCellID].size();
            this->staticORCAObstacles[gridCellID].push_back(Obstacle());
            this->staticORCAObstacles[gridCellID].push_back(Obstacle());
            this->staticORCAObstacles[gridCellID].push_back(Obstacle());
            this->staticORCAObstacles[gridCellID].push_back(Obstacle());

            Obstacle& tr = this->staticORCAObstacles[gridCellID][startIndex];     // top right
            Obstacle& tl = this->staticORCAObstacles[gridCellID][startIndex + 1]; // top left
            Obstacle& bl = this->staticORCAObstacles[gridCellID][startIndex + 2]; // bottom left
            Obstacle& br = this->staticORCAObstacles[gridCellID][startIndex + 3]; // bottom right

            tr.point = pathCellCenter +
                irr::core::vector2df(-this->pathGrid.unitSize * 0.45, -this->pathGrid.unitSize * 0.45);
            tl.point = pathCellCenter +
                irr::core::vector2df(+this->pathGrid.unitSize * 0.45, -this->pathGrid.unitSize * 0.45);
            bl.point = pathCellCenter +
                irr::core::vector2df(+this->pathGrid.unitSize * 0.45, +this->pathGrid.unitSize * 0.45);
            br.point = pathCellCenter +
                irr::core::vector2df(-this->pathGrid.unitSize * 0.45, +this->pathGrid.unitSize * 0.45);

            tr.isConvex = true;
            tl.isConvex = true;
            bl.isConvex = true;
            br.isConvex = true;

            auto connect = [this, gridCellID, startIndex](const size_t o1, const size_t o2) {
                this->staticORCAObstacles[gridCellID][startIndex + o1].nextObstacle = startIndex + o2;
                this->staticORCAObstacles[gridCellID][startIndex + o2].prevObstacle = startIndex + o1;
                this->staticORCAObstacles[gridCellID][startIndex + o1].unitDir =
                    (this->staticORCAObstacles[gridCellID][startIndex + o2].point -
                     this->staticORCAObstacles[gridCellID][startIndex + o1].point)
                        .normalize();
            };

            connect(0, 1);
            connect(1, 2);
            connect(2, 3);
            connect(3, 0);
        }
    }

    // make all the little squares convex
    for (auto& obstacles : this->staticORCAObstacles)
    {
        for (auto& obstacle : obstacles)
        {
            obstacle.isConvex = true;
        }
    }

    return;

    /*

    typedef std::vector<Obstacle> simplex;
    std::vector<simplex> simplexes;

    // fill the whole pathGrid with obstacles
    std::vector<std::vector<Obstacle>> obstacles;
    obstacles.resize(this->pathGrid.width);
    for (auto& part : obstacles) {
        part.resize(this->pathGrid.height, static_cast<size_t>(-1));
    }

    // basic idea: walk through the pathGrid scanning a 2x2 area.
    // There are 6 possible states this area can be in (X: StaticNonWalkable, O: Free)
    // 1)   2)   3)   4)   5)
    // OO   OX   OX   OX   XX
    // OO   OO   OX   XX   XX
    //
    // cases 1) and 5) are trival: nothing is to be done for each.
    // in cases 2)-4) (and their rotations) obstacles must be connected.

    for (size_t x = 0; x < this->pathGrid.width - 1; x++) {
        for (size_t z = 0; z < this->pathGrid.height - 1; z++) {
          auto counter = [this](const size_t x, const size_t z) {
            return (this->pathGrid.get(x, z) ==
                    PathGrid::Walkability::StaticNonWalkable)
                       ? 1
                       : 0;
          };
          int numNonWalkable = counter(x, z) + counter(x + 1, z) + counter(x, z + 1) + counter(x +
    1, z + 1);
        }
    }

    // walk through the whole pathGrid.
    for (grid::gridCellID_t x = 0; x < this->grid.xNumberOfCells; x++) {
        for (grid::gridCellID_t z = 0; z < this->grid.zNumberOfCells; z++) {
            const grid::gridCellID_t gridCellID = this->grid.getCellID(x, z);

            const PathGrid::PathCellCoordinate_t topRight = this->pathGrid.getPathCellCoordinate(x *
    this->grid.sideLength, z * this->grid.sideLength);

            for (int x = 0; x < pathCellsPerGridCellSide; x++) {
                for (int z = 0; z < pathCellsPerGridCellSide; z++) {
                    // find all
                }
            }

            this->staticORCAObstacles[gridCellID].push_back(Obstacle());
            this->staticORCAObstacles[gridCellID].push_back(Obstacle());
            this->staticORCAObstacles[gridCellID].push_back(Obstacle());
            this->staticORCAObstacles[gridCellID].push_back(Obstacle());

            //TODO: remove this test code
            //just for testing: create a non-walkable part of the gridCell in the center of the cell.
            Obstacle& obstacleTopRight = this->staticORCAObstacles[gridCellID][0];
            Obstacle& obstacleTopLeft = this->staticORCAObstacles[gridCellID][1];
            Obstacle& obstacleBottomLeft = this->staticORCAObstacles[gridCellID][2];
            Obstacle& obstacleBottomRight = this->staticORCAObstacles[gridCellID][3];

            obstacleTopRight.point = irr::core::vector2df(x * this->grid.sideLength +
    this->grid.sideLength * 0.45, z * this->grid.sideLength + this->grid.sideLength * 0.45);
            obstacleTopLeft.point = obstacleTopRight.point + irr::core::vector2df(0.45f *
    this->grid.sideLength, 0.0f); obstacleBottomLeft.point = obstacleTopLeft.point +
    irr::core::vector2df(0.0f, 0.45f * this->grid.sideLength); obstacleBottomRight.point =
    obstacleTopRight.point + irr::core::vector2df(0.0f, 0.45f * this->grid.sideLength);

            const auto topRightPath = this->pathGrid.getPathCellCoordinate(obstacleTopRight.point);
            const auto bottomLeftPath =
    this->pathGrid.getPathCellCoordinate(obstacleBottomLeft.point);

            for (auto x = topRightPath.X; x < bottomLeftPath.X; x++) {
                for (auto z = topRightPath.Z; z < bottomLeftPath.Z; z++) {
    //                this->pathGrid.set(x, z, PathGrid::Walkability::StaticNonWalkable);
                }
            }

            obstacleTopRight.isConvex = true;
            obstacleTopLeft.isConvex = true;
            obstacleBottomLeft.isConvex = true;
            obstacleBottomRight.isConvex = true;

            auto connect = [this, gridCellID](const size_t o1, const size_t o2) {
              this->staticORCAObstacles[gridCellID][o1].nextObstacle = o2;
              this->staticORCAObstacles[gridCellID][o2].prevObstacle = o1;
              this->staticORCAObstacles[gridCellID][o1].unitDir =
                  (this->staticORCAObstacles[gridCellID][o2].point -
                   this->staticORCAObstacles[gridCellID][o1].point)
                      .normalize();
            };

            connect(0, 1);
            connect(1, 2);
            connect(2, 3);
            connect(3, 0);
        }
    }

    this->gameMap->generateDebugCubes();

    */
}

void MovementSystem::showStaticORCAObstacles()
{
    for (auto& obstacles : this->staticORCAObstacles)
    {
        for (auto& obstacle : obstacles)
        {
            auto& nextObstacle = obstacles[obstacle.nextObstacle];
            auto pos1 = this->gameMap->get3dPosition(obstacle.point);
            auto pos2 = this->gameMap->get3dPosition(nextObstacle.point);
            if (pos1.Y == -FLT_MAX)
            {
                pos1.Y = pos2.Y;
            }
            if (pos2.Y == -FLT_MAX)
            {
                pos2.Y = pos1.Y;
            }

            if (pos1.Y != -FLT_MAX && pos2.Y != -FLT_MAX)
            {
                this->game.line2Draw.push_back(line2Draw_s(pos1, pos2, irr::video::SColor(255, 255, 0, 0)));
            }

            auto ud = this->gameMap->get3dPosition(obstacle.point + 20.0 * obstacle.unitDir);
            if (ud.Y == -FLT_MAX)
            {
                ud.Y = pos1.Y;
            }

            if (pos1.Y != -FLT_MAX)
            {
                this->game.line2Draw.push_back(line2Draw_s(pos1, ud, irr::video::SColor(255, 0, 255, 0)));
            }
        } // for (auto& obstacle : obstacles)
    }     // for (auto& obstacles: this->staticORCAObstacles)
}
#endif

void MovementSystem::handleTargetAlreadyReached()
{
    std::vector<std::future<void>> results;

    for (grid::gridCellID_t cellID = 0; cellID < grid.maxCellID; ++cellID)
    {
        if (m_inGridCells[cellID].empty())
        {
            // optimization: don't do any thread syncronization stuff (enqueueing needs
            // syncronization) if no updates to do
            continue;
        }
        results.push_back(threadPool.enqueue(static_cast<void (MovementSystem::*)(const grid::gridCellID_t)>(
                                                 &MovementSystem::handleTargetAlreadyReachedIn),
                                             this,
                                             cellID));
    }

    for (auto& result : results)
    {
        result.wait();
    }
}

void MovementSystem::handleTargetAlreadyReachedIn(const grid::gridCellID_t gridCellID)
{
    for (const auto& entity : m_inGridCells[gridCellID])
    {
        Components::Movement& movement = this->handleManager.getAsRef<Components::Movement>(entity.mov);

        if (!movement.waypoints.empty())
        {
            const irr::core::vector2df& position =
                this->handleManager.getAsRef<Components::Position>(entity.pos).position;
            // if the entity moved it must have a movement target
            POW2_ASSERT(!movement.waypoints.empty());
            const irr::core::vector2df& waypoint = movement.waypoints.front();
            if (closeEnough(position, waypoint))
            {
                this->waypointReached(entity.id, movement);
            }
        }
    }
}

void MovementSystem::computeNewVelocities(const float deltaTime)
{
    std::vector<std::future<void>> results;

    for (grid::gridCellID_t x = 0; x < this->grid.xNumberOfCells; x++)
    {
        for (grid::gridCellID_t z = 0; z < this->grid.zNumberOfCells; z++)
        {
            const grid::gridCellID_t cellID = this->grid.getCellID(x, z);
            if (m_inGridCells[cellID].empty())
            {
                // optimization: don't do any thread syncronization stuff (enqueueing needs
                // syncronization) if no updates to do
                continue;
            }
            results.push_back(
                this->threadPool.enqueue(static_cast<void (MovementSystem::*)(grid::gridCellID_t, const float)>(
                                             &MovementSystem::computeNewVelocities),
                                         this,
                                         cellID,
                                         deltaTime));
        }
    }

    for (auto& result : results)
    {
        result.wait();
    }
}

void MovementSystem::computeNewVelocities(const grid::gridCellID_t gridCellID, const float deltaTime)
{
    POW2_ASSERT(!m_inGridCells[gridCellID].empty());
    for (const auto& entity : m_inGridCells[gridCellID])
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "computeNewVelocityForEntity loop for entity",
                      entity.id,
                      "in gridCellID",
                      gridCellID);

        this->computeNewVelocityForEntity(entity, gridCellID, deltaTime);
    }
}

void MovementSystem::applyMovement(const float deltaTime)
{
    std::vector<std::future<void>> results;

    // calculation of new positions can be done fully parallel (but is paralleled gridCell-based
    // because the access for tracked entities is best done gridCell based
    for (grid::gridCellID_t x = 0; x < this->grid.xNumberOfCells; x++)
    {
        for (grid::gridCellID_t z = 0; z < this->grid.zNumberOfCells; z++)
        {
            const grid::gridCellID_t cellID = this->grid.getCellID(x, z);
            if (m_inGridCells[cellID].empty())
            {
                // optimization: don't do any thread syncronization stuff (enqueueing needs
                // syncronization) if no updates to do
                continue;
            }
            // debugOutLevel(Debug::DebugLevels::updateLoop, "adding threaded update call for
            // celllID:", cellID);
            // this->applyMovement(cellID, deltaTime);
            results.push_back(this->threadPool.enqueue(
                static_cast<void (MovementSystem::*)(const grid::gridCellID_t, const float)>(&MovementSystem::applyMovement),
                this,
                cellID,
                deltaTime));
        }
    }
    // must wait until all threads are finished before continuing because otherwise multiple threads
    // might try to access the same cell
    for (size_t i = 0; i < results.size(); i++)
    {
        // debugOutLevel(Debug::DebugLevels::updateLoop, "waiting for result", i);
        results[i].wait();
        // debugOutLevel(Debug::DebugLevels::updateLoop, "result", i, "finished");
    }
    results.clear();
}

irr::core::matrix4
MovementSystem::updateAngle(irr::core::matrix4 oldMat, irr::core::matrix4 newMat, irr::f32 maxRotDistance)
{
    irr::f32 mat[16];
    irr::core::matrix4 ret;
    irr::f32 time = 0.1f;

    for (irr::u32 i = 0; i < 16; i += 4)
    {
        mat[i + 0] =
            (oldMat[i + 0] +
             (irr::core::clamp(newMat[i + 0] - oldMat[i + 0], -1 * maxRotDistance, maxRotDistance)) * time);
        mat[i + 1] =
            (oldMat[i + 1] +
             (irr::core::clamp(newMat[i + 1] - oldMat[i + 1], -1 * maxRotDistance, maxRotDistance)) * time);
        mat[i + 2] =
            (oldMat[i + 2] +
             (irr::core::clamp(newMat[i + 2] - oldMat[i + 2], -1 * maxRotDistance, maxRotDistance)) * time);
        mat[i + 3] =
            (oldMat[i + 3] +
             (irr::core::clamp(newMat[i + 3] - oldMat[i + 3], -1 * maxRotDistance, maxRotDistance)) * time);
    }
    ret.setM(mat);
    return ret;
}

void MovementSystem::applyMovement(const grid::gridCellID_t gridCellID, const float deltaTime)
{
    const size_t threadIndex = threadPool.getMappedIndex(std::this_thread::get_id());
    POW2_ASSERT(threadIndex < this->threadCaches.size());

    irr::video::SColor debugColor = irr::video::SColor(255, 0, 0, 255);
    for (const auto& entity : m_inGridCells[gridCellID])
    {
        debugColor = irr::video::SColor(255, 0, 0, 255);
        if (this->entityManager.hasComponent<Components::DebugColor>(entity.id))
        {
            debugColor = this->handleManager
                             .getAsValue<Components::DebugColor>(
                                 this->entityManager.getComponent<Components::DebugColor>(entity.id))
                             .color;
        }
        irr::core::vector2df& position =
            this->handleManager.getAsRef<Components::Position>(entity.pos).position;
        Components::Movement& movement = this->handleManager.getAsRef<Components::Movement>(entity.mov);
#if defined(MOVEMENT_SYSTEM_STATIC_OBSTACLE_CLAMPING)
// assert disabled because in the applyMovement function setting the position of the entity directly
// by the waypoint (if the entity is very close)
// the waypoint (and thus oldPosition) might overlap with a static object
// TODO: find way to enable again
// POW2_ASSERT(!this->pathGrid.collidesWithStaticObject(position, movement.radius,
// PathGrid::staticObstacleSize));
#endif

        if (!movement.waypoints.empty())
        {
            this->eventManager.emit(Events::PositionChanged(entity.id));

            // check if the gridCell changed
            const grid::gridCellID_t oldGridCellID = this->grid.getCellID(position);

            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "entity",
                          entity.id,
                          "with positionHandle",
                          entity.pos,
                          "and MovementHandle",
                          entity.mov,
                          "was at (",
                          position.X,
                          position.Y,
                          ")");

// move the entity out of any static obstacles it moved into
#if defined(MOVEMENT_SYSTEM_STATIC_OBSTACLE_CLAMPING)
            const auto testNewPosition = position + movement.newVelocity * deltaTime; // simple euler integration
            auto newPosition = this->moveOutsideStaticObstacles(position, testNewPosition, movement.radius);
            const irr::core::vector2df positionOffset = newPosition - position;
            // calculate velocity from actual movement
            movement.velocity = positionOffset / deltaTime;
#else
            const irr::core::vector2df positionOffset = movement.newVelocity * deltaTime; // simple euler integration
            const auto newPosition = position + positionOffset;
            movement.velocity = movement.newVelocity;
#endif

            if (this->entityManager.hasComponent<Components::Rotation>(entity.id))
            {
                auto normVector =
                    this->gameMap->getTerrainNode()->getNormalVector(newPosition.X, newPosition.Y);
                const auto rotHandle = this->entityManager.getComponent<Components::Rotation>(entity.id);
                auto& rot = this->handleManager.getAsRef<Components::Rotation>(rotHandle).rotation;
                irr::core::quaternion old_rot(rot * irr::core::DEGTORAD);
                irr::core::vector3df new_rot_euler;
                // TODO replace with getSphericalCoordinates if possible
                const irr::f32 x_rot =
                    -1.f * static_cast<irr::f32>(atan2(normVector.Y, normVector.Z)) + irr::core::HALF_PI;
                const irr::f32 y_rot = static_cast<irr::f32>(movement.velocity.getAngle()) * irr::core::DEGTORAD;
                const irr::f32 z_rot =
                    static_cast<irr::f32>(atan2(normVector.Y, normVector.X)) - irr::core::HALF_PI;
                irr::core::quaternion new_rot(irr::core::vector3df(x_rot, 0, 0.f));
                irr::core::quaternion y_mat(irr::core::vector3df(0.f, y_rot, 0.f));
                irr::core::quaternion z_mat(irr::core::vector3df(0.f, 0.f, z_rot));
                new_rot *= z_mat;
                new_rot *= y_mat;

                new_rot.slerp(new_rot, old_rot, 0.9f).toEuler(new_rot_euler);
                rot = new_rot_euler * irr::core::RADTODEG;
            }
            const irr::core::vector2df& waypoint = movement.waypoints.front();

            // to prevent rubber-banding around the waypoint move the entity directly ontop of
            // it if the distance to the waypoint is less than the step the entity makes
            const irr::f32 distanceToWaypointSQ = position.getDistanceFromSQ(waypoint);

            // calculate how close we should be to the waypoint. If that
            // is larger than the actual distance (by a margin of
            // several movement steps at the entities speed)
            // request re-pathing to the final target
            // (because that means the entity might be stuck somewhere and
            // can't move towards the target)
            constexpr float maxMovementStepsDelta = 5.0f;
            movement.expectedDistanceSQToWaypoint -= movement.speed * movement.speed * deltaTime * deltaTime;

            // actually move the entity
            if (distanceToWaypointSQ < positionOffset.getLengthSQ())
            {
                position = movement.waypoints.front();
#if defined(MOVEMENT_SYSTEM_STATIC_OBSTACLE_CLAMPING)
// assert disabled because in the applyMovement function setting the position of
// the entity directly by the waypoint (if the entity is very close)
// the waypoint (and thus oldPosition) might overlap with a static object
// TODO: find way to enable again
// POW2_ASSERT(!this->pathGrid.collidesWithStaticObject(newPosition, movement.radius,
// PathGrid::staticObstacleSize));
#endif
            }
            else
            {
#if defined(MOVEMENT_SYSTEM_STATIC_OBSTACLE_CLAMPING)
// POW2_ASSERT(!this->pathGrid.collidesWithStaticObject(newPosition,
//                                                     movement.radius,
//                                                     PathGrid::staticObstacleSize));
#endif
                position = newPosition;
            }

            // this->gameMap->setDebugCubeColor(cacheEntry.oldPathGridID, debugColor);
            const grid::gridCellID_t newGridCellID = this->grid.getCellID(position);
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "entity",
                          entity.id,
                          "moved with velocity (",
                          movement.velocity.X,
                          movement.velocity.Y,
                          ") and is now at (",
                          position.X,
                          position.Y,
                          ") grid:",
                          oldGridCellID,
                          "->",
                          newGridCellID,
                          "movementStatus",
                          static_cast<int>(movement.movementStatus));

            if (closeEnough(position, waypoint))
            {
                this->waypointReached(entity.id, movement);
            }
            else if (distanceToWaypointSQ - movement.expectedDistanceSQToWaypoint >
                     movement.speed * deltaTime * maxMovementStepsDelta)
            {
                // if the entity reaches its final target a RepathRequest would lead to an
                // invalid access when handling the event (because the waypoints-queue will be
                // empty)
                // -> only emit a RepathRequest if the entity didn't get to its target
                debugOutLevel(Debug::DebugLevels::updateLoop,
                              "entity",
                              entity.id,
                              "expected distance",
                              movement.expectedDistanceSQToWaypoint,
                              "but was at",
                              distanceToWaypointSQ,
                              "bigger than",
                              movement.speed * deltaTime * maxMovementStepsDelta,
                              "-> repathing requested");
                this->eventManager.emit(Events::RepathRequest(entity.id));
            }
        }
    } // for (const auto& entity : m_inGridCells[gridCellID])
}

void MovementSystem::showPathForSelectedEntities() const
{
#ifndef MAKE_SERVER_
    // TODO: maybe make this code only build if debugging pathing?
    for (auto& selection : this->selectedEntities)
    {
        for (entityID_t entityID : selection)
        {
            if (!this->entityManager.hasComponent<Components::Position>(entityID))
            {
                continue;
            }
            const Handles::Handle positionHandle =
                this->entityManager.getComponent<Components::Position>(entityID);
            if (positionHandle == Handles::InvalidHandle)
            { // that entity didn't have a position
                continue;
            }
            const auto at = std::find_if(trackedEntities.cbegin(),
                                         trackedEntities.cend(),
                                         [entityID](const auto& e) { return e.id == entityID; });
            if (at == trackedEntities.end())
            {
                continue;
            }

            POW2_ASSERT(at->pos == positionHandle);
            auto debugColor = irr::video::SColor(255, 0, 0, 255);
            if (this->entityManager.hasComponent<Components::DebugColor>(at->id))
            {
                debugColor = this->handleManager
                                 .getAsValue<Components::DebugColor>(
                                     this->entityManager.getComponent<Components::DebugColor>(at->id))
                                 .color;
            }
            const Components::Movement& movement =
                this->handleManager.getAsRef<Components::Movement>(at->mov);
            irr::core::vector3df lastTarget3d = irr::core::vector3df(-100.0f, -100.0f, -100.0f);
            for (const auto& target : movement.waypoints)
            {
                const irr::core::vector3df target3d = this->gameMap->get3dPosition(target);
                if (lastTarget3d == irr::core::vector3df(-100.0f, -100.0f, -100.0f))
                {
                    lastTarget3d = this->gameMap->get3dPosition(
                        this->handleManager.getAsValue<Components::Position>(at->pos).position);
                    debugOutLevel(Debug::DebugLevels::updateLoop + 2,
                                  "pos: (",
                                  lastTarget3d.X,
                                  ",",
                                  lastTarget3d.Z,
                                  ")",
                                  "tar: (",
                                  target.X,
                                  ",",
                                  target.Y,
                                  ")",
                                  "distSQ:",
                                  irr::core::vector2df(lastTarget3d.X, lastTarget3d.Z).getDistanceFromSQ(target),
                                  "expectedSQ",
                                  movement.expectedDistanceSQToWaypoint);
                }
                game.line2Draw.push_back(line2Draw_s(lastTarget3d, target3d, debugColor));
                lastTarget3d = target3d;
            }
        } // for (entityID_t entityID : selection)
    }     // for (auto& selection: this->selectedEntities)
#endif
}

void MovementSystem::waypointReached(const entityID_t entityID, Components::Movement& movement)
{
    // remove the current target
    POW2_ASSERT(!movement.waypoints.empty());
    // get current position to calculate new expected distance
    const irr::core::vector2df currentPosition = movement.waypoints.front();
    movement.waypoints.pop_front();

    if (movement.waypoints.empty())
    {
        movement.expectedDistanceSQToWaypoint = 0.0f; // TODO: this might not need to be set because
                                                      // if the entity gets new waypoints it should
                                                      // set the expected distance by itself
        // this is just a failsafe to make sure the entity moves if someone forgets to set the
        // expectedDistanceSQToWaypoint when setting new waypoints
        debugOutLevel(Debug::DebugLevels::updateLoop + 2, "entity", entityID, "reached targed and has an empty target queue -> emitting 'FinalWaypointReached'");
        this->eventManager.emit(Events::FinalWaypointReached(entityID));
    }
    else
    {
        movement.expectedDistanceSQToWaypoint =
            currentPosition.getDistanceFromSQ(movement.waypoints.front());
        debugOutLevel(Debug::DebugLevels::updateLoop + 3, "entity", entityID, "reached target and still has more waypoints. emitting 'IntermediateWaypointReached'");
        this->eventManager.emit(Events::IntermediateWaypointReached(entityID));
    }
}

bool MovementSystem::entitiesColliding()
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "running entitiesColliding()");

    for (grid::gridCellID_t x = 0; x < this->grid.xNumberOfCells; x++)
    {
        for (grid::gridCellID_t z = 0; z < this->grid.zNumberOfCells; z++)
        {
            const grid::gridCellID_t cellID1 = this->grid.getCellID(x, z);
            for (const auto& entity1 : m_inGridCells[cellID1])
            {
                for (const auto cellID2 : grid.get3x3Around(cellID1))
                {
                    if (cellID2 == grid::InvalidCellID)
                    {
                        continue;
                    }

                    for (const auto& entity2 : m_inGridCells[cellID2])
                    {
                        if (entity1.id == entity2.id)
                        {
                            continue;
                        }

                        const irr::core::vector2df& position1 =
                            this->handleManager.getAsRef<Components::Position>(entity1.pos).position;
                        const irr::core::vector2df& position2 =
                            this->handleManager.getAsRef<Components::Position>(entity2.pos).position;

                        const Components::Movement& movement1 =
                            this->handleManager.getAsRef<Components::Movement>(entity1.mov);
                        const Components::Movement& movement2 =
                            this->handleManager.getAsRef<Components::Movement>(entity2.mov);

                        if (position1.getDistanceFromSQ(position2) >
                            movement1.radius * movement1.radius + movement2.radius * movement2.radius)
                        {
                            POW2_ASSERT(false);
                            Error::errContinue("entities",
                                               entity1.id,
                                               "and",
                                               entity2.id,
                                               "collided with distance",
                                               position1.getDistanceFromSQ(position2));
                            return true;
                        }
                    }
                }
            }
        }
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "entitiesColliding() finished without finding any collision(s)");
    return false;
}
