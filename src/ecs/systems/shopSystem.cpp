/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "shopSystem.h"

#include <irrlicht/IAnimatedMeshSceneNode.h>
#include "../entityManager.h"
#include "../events/eventManager.h"
#include "../../utils/HandleManager.h"

ShopSystem::ShopSystem(ThreadPool& threadPool_,
                       EventManager& eventManager_,
                       EntityManager& entityManager_,
                       Handles::HandleManager& handleManager_,
                       std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection_)
    : SystemBase(Systems::SystemIdentifiers::ShopSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , blueprintCollection(blueprintCollection_)
{
    this->boundComponents.push_back(Components::ComponentID<Components::PurchasableItems>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::SelectionID>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Position>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Range>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::TryToBuyAShoptItem>(
        [this](const Events::TryToBuyAShoptItem event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::TryToBuyAShoptItem, event);
        }));
}

void ShopSystem::trackEntity(const entityID_t entityID, const componentID_t internalCompomentID, const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);
    POW2_ASSERT(this->handleManager.isValid(handle));

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    if (!this->entityManager.hasComponent<Components::PurchasableItems, Components::SelectionID, Components::Position, Components::Range>(
            entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all needed Components available");
        return;
    }

    const auto selectionH = this->entityManager.getComponent<Components::SelectionID>(entityID);
    const auto selectionType = this->handleManager.getAsValue<Components::SelectionID>(selectionH).selectionType;
    if (selectionType != Components::SelectionID::SelectionType::ItemShop and
        selectionType != Components::SelectionID::SelectionType::TankShop)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't track entity '", entityID, "' Components::SelectionID::selectionType unequal Shop ");
        return;
    }

    TrackedEntityHandleCollection newEntry;
    newEntry.selectionIDH = this->entityManager.getComponent<Components::SelectionID>(entityID);
    newEntry.positionH = this->entityManager.getComponent<Components::Position>(entityID);
    newEntry.rangeH = this->entityManager.getComponent<Components::Range>(entityID);

    this->trackedEntities[entityID] = newEntry;
}

void ShopSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void ShopSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    (void) deltaTime;

    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void ShopSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::TryToBuyAShoptItem:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::TryToBuyAShoptItem>(ap);
                this->handleTryToBuyAShoptItem(event.buyerEntityID, event.shopEntityID, event.blueprintId);
            }
            break;
            default:
            {
                Error::errTerminate("In ShopSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

void ShopSystem::handleTryToBuyAShoptItem(const entityID_t buyerEntityID,
                                          const entityID_t shopEntityID,
                                          const blueprintID_t blueprintId)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleTryToBuyAShoptItem", buyerEntityID, shopEntityID, blueprintId);

    if (buyerEntityID == ECS::INVALID_ENTITY or shopEntityID == ECS::INVALID_ENTITY or
        blueprintId == ECS::INVALID_BLUEPRINT)
    {
        return;
    }

    if (!this->trackedEntities.count(shopEntityID))
    {
        return;
    }

    const auto shopEntityInfos = this->trackedEntities[shopEntityID];
    const auto playserBaseH = this->entityManager.getComponent<Components::PlayerBase>(buyerEntityID);
    auto& playerBase = this->handleManager.getAsRef<Components::PlayerBase>(playserBaseH);

    const auto currentTankEntityID = playerBase.tankEntityID;

    // check on the same team
    const auto tankSelectionIDH = this->entityManager.getComponent<Components::SelectionID>(currentTankEntityID);
    const auto tankSelectionID = this->handleManager.getAsValue<Components::SelectionID>(tankSelectionIDH);

    auto shopSelectionID = this->handleManager.getAsValue<Components::SelectionID>(shopEntityInfos.selectionIDH);

    if (tankSelectionID.team != shopSelectionID.team)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "Shop and buyer not on the same team. Unable to buy selected Item");
        return;
    }

    // check buyer in range
    const auto tankPositionHandle = this->entityManager.getComponent<Components::Position>(currentTankEntityID);
    const auto tankPosition = this->handleManager.getAsValue<Components::Position>(tankPositionHandle);

    const auto shopPosition = this->handleManager.getAsRef<Components::Position>(shopEntityInfos.positionH);
    const auto shopRange = this->handleManager.getAsValue<Components::Range>(shopEntityInfos.rangeH);

    if (std::pow(tankPosition.position.X - shopPosition.position.X, 2) +
            std::pow(tankPosition.position.Y - shopPosition.position.Y, 2) >
        shopRange.squaredRange)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Out of range. Unable to buy a new Item");
        return;
    }

    // check price
    const auto blueprint = this->blueprintCollection[int(blueprintId)].first;
    const auto itemPriceH = blueprint.getComponent(Components::ComponentID<Components::Price>::CID);
    const auto itemPrice = this->handleManager.getAsValue<Components::Price>(itemPriceH).price;

    if (itemPrice > playerBase.money)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Not enough money. Unable to buy selected Item");
        return;
    }

    const auto blueprintType = blueprintCollection[blueprintId].second;
    if (blueprintType == EntityBlueprint::BlueprintType::Tank)
    {
        // check requirement
        const auto tankRequirementH =
            blueprint.getComponent(Components::ComponentID<Components::Requirement>::CID);
        const auto tankRequirement =
            this->handleManager.getAsValue<Components::Requirement>(tankRequirementH).requirement;

        const auto currentRequirement = 100; // TODO get real value

        if (tankRequirement > currentRequirement)
        {
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "Requirement to high (",
                          tankRequirement,
                          " > ",
                          currentRequirement,
                          "). Unable to buy selected Tank");
            return;
        }

        // buy and create/update the tank //
        const auto priceH = this->entityManager.getComponent<Components::Price>(currentTankEntityID);
        const auto oldTankPrice = this->handleManager.getAsValue<Components::Price>(priceH).price;

        const auto blueprintComponents = this->blueprintCollection[int(blueprintId)].first.getAllComponents();

        const std::vector<componentID_t> componentsToMaintain = {
            Components::ComponentID<Components::Position>::CID,
            Components::ComponentID<Components::SelectionID>::CID};
        irr::scene::IAnimatedMeshSceneNode* oldNode = nullptr;
        for (auto pair : blueprintComponents)
        {
            if (std::find(componentsToMaintain.begin(), componentsToMaintain.end(), pair.first) ==
                componentsToMaintain.end())
            {
                auto tankComponentHandle = this->entityManager.getComponent(currentTankEntityID, pair.first);
                if (pair.first == Components::ComponentID<Components::Graphic>::CID)
                {
                    oldNode =
                        this->handleManager.getAsValue<Components::Graphic>(tankComponentHandle).node;
                }
                this->entityManager.copyComponent(tankComponentHandle, pair.second, pair.first);
                if (pair.first == Components::ComponentID<Components::Graphic>::CID)
                {
                    auto& graphic = this->handleManager.getAsRef<Components::Graphic>(tankComponentHandle);
                    graphic.node = oldNode;

                    POW2_ASSERT(graphic.mesh != nullptr);
                    POW2_ASSERT(graphic.texture != nullptr);

                    constexpr irr::u32 layer = 0;
                    graphic.node->setMesh(graphic.mesh);
                    graphic.node->setMaterialTexture(layer, graphic.texture);
                    graphic.node->setScale(graphic.scale);
                }
            }
        }

        this->eventManager.emit(Events::TransferMoney(buyerEntityID, static_cast<int16_t>(-1 * itemPrice)));
        this->eventManager.emit(Events::TransferMoney(buyerEntityID, oldTankPrice / 2));
    }
    else
    {
        const auto inventoryHandle = this->entityManager.getComponent<Components::Inventory>(buyerEntityID);
        const auto inventory = this->handleManager.getAsRef<Components::Inventory>(inventoryHandle).inventory;

        // Check still slots free
        unsigned int emptySlots = 0;
        for (unsigned int i = 0; i < 6; i++)
        {
            if (inventory[i] == ECS::INVALID_ENTITY)
            {
                emptySlots++;
            }
        }
        if (emptySlots < 1)
        {
            debugOutLevel(Debug::DebugLevels::updateLoop, "No free Slot for new Item left");
            return;
        }

        // buy and create the item
        Components::OwnerEntityID ownerEntityID;
        ownerEntityID.ownerEntityID = buyerEntityID;
        const auto itemEntityID = this->blueprintCollection[int(blueprintId)].first.createEntity(
            Components::ComponentID<Components::OwnerEntityID>::CID, &ownerEntityID);

        this->eventManager.emit(Events::TransferMoney(buyerEntityID, static_cast<int16_t>(-1 * itemPrice)));
        this->eventManager.emit(Events::AddItemToInventory(buyerEntityID, itemEntityID));
    }
}
