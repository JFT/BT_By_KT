/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DAMAGE_SYSTEM_H
#define DAMAGE_SYSTEM_H

#include <unordered_map>

#include "systemBase.h"

class DamageSystem : public SystemBase
{
   public:
    DamageSystem(ThreadPool& threadPool_, EventManager& eventManager_, Handles::HandleManager& handleManager_);

    DamageSystem(const DamageSystem&) = delete;
    DamageSystem operator=(const DamageSystem&) = delete;

    virtual ~DamageSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

   private:
    std::unordered_map<entityID_t, Handles::Handle> trackedEntities = {};

    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        TakeDamage = 0,
        TakeHeal,
        NumInternalEvents,
    };

    void handleReceivedEvents();
    void handleTakeDamageEvent(const entityID_t entityID, const float amount);
    void handleGetHealEvent(const entityID_t entityID, const float amount);
};

#endif // DAMAGE_SYSTEM_H
