/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "graphicSystem.h"

#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/ISceneCollisionManager.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/position2d.h>

#include "../components.h"
#include "../entityManager.h"
#include "../events/eventManager.h"
#include "../../core/game.h"
#include "../../graphics/graphicsPipeline.h"
#include "../../grid/grid.h"
#include "../../map/gamemap.h"
#include "../../utils/HandleManager.h"

GraphicSystem::GraphicSystem(ThreadPool& threadPool_,
                             EventManager& eventManager_,
                             EntityManager& entityManager_,
                             Handles::HandleManager& handleManager_,
                             GameMap* const gameMap_,
                             const grid::Grid& grid_,
                             Game& game_)
    : SystemBase(Systems::SystemIdentifiers::GraphicSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , gameMap(gameMap_)
    , grid(grid_)
    , game(game_)
{
    this->boundComponents.push_back(Components::ComponentID<Components::Graphic>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Position>::CID);
    // this->boundComponents.push_back(Components::ComponentID<Components::Rotation>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::ChangeGraphicScale>([this](const auto& event) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::ChangeGraphicScale, event);
    }));
}

void GraphicSystem::trackEntity(const entityID_t entityID,
                                const componentID_t internalCompomentID,
                                const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    if (!this->entityManager.hasComponent<Components::Graphic, Components::Position>(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all needed Components available");
        return;
    }

    TrackedEntityHandleCollection newEntry;
    newEntry.positionH = this->entityManager.getComponent<Components::Position>(entityID);
    POW2_ASSERT(newEntry.positionH != Handles::InvalidHandle);
    POW2_ASSERT(this->handleManager.isValid(newEntry.positionH));
    newEntry.graphicH = this->entityManager.getComponent<Components::Graphic>(entityID);
    POW2_ASSERT(newEntry.graphicH != Handles::InvalidHandle);
    POW2_ASSERT(this->handleManager.isValid(newEntry.graphicH));
    // newEntry.rotationH = this->entityManager.getComponent<Components::Rotation>(entityID);
    // POW2_ASSERT(newEntry.rotationH != Handles::InvalidHandle);
    // POW2_ASSERT(this->handleManager.isValid(newEntry.rotationH));

    Components::Graphic& graphic = this->handleManager.getAsRef<Components::Graphic>(newEntry.graphicH);
    POW2_ASSERT(graphic.mesh != nullptr);

    graphic.node = this->game.getSceneManager()->addAnimatedMeshSceneNode(graphic.mesh, nullptr);
    if (graphic.texture != nullptr)
    {
        // TODO: maybe allow setting different textures for different layers?
        constexpr irr::u32 layer = 0;
        graphic.node->setMaterialTexture(layer, graphic.texture);
    }
    const auto position = this->handleManager.getAsValue<Components::Position>(newEntry.positionH).position;
    // const auto rotation =
    // this->handleManager.getAsValue<Components::Rotation>(newEntry.rotationH).rotation;

    auto position3D = this->gameMap->get3dPosition(position);
    position3D.Y = position3D.Y + graphic.offset * graphic.scale.Y;
    graphic.node->setPosition(position3D);
    graphic.node->setScale(graphic.scale);
    graphic.node->setAnimationSpeed(0);

    game.getGraphics()->addNodeToStage(graphic.node, RenderStageType::ANIMATED);
    game.getGraphics()->addNodeToStage(graphic.node, RenderStageType::LIGHTING);
    this->trackedEntities[entityID] = newEntry;
}

void GraphicSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    auto& gC = this->handleManager.getAsRef<Components::Graphic>(this->trackedEntities[entityID].graphicH);
    game.getGraphics()->removeNodeFromStage(gC.node, RenderStageType::LIGHTING);
    game.getGraphics()->removeNodeFromStage(gC.node, RenderStageType::ANIMATED);
    gC.node->remove();
    gC.node = nullptr;
    this->trackedEntities.erase(entityID);
}

namespace
{
    void easeToActualPositionRotation(irr::scene::IAnimatedMeshSceneNode* node,
                                      irr::core::vector3df pos,
                                      std::pair<bool, irr::core::vector3df> rotation)
    {
        constexpr double movementPerStep = 1.0f / 4.0f;

        // easing via power law
        const auto halfWay = node->getPosition() + (pos - node->getPosition()) * movementPerStep;

        node->setPosition(halfWay);
        if (rotation.first)
        {
            node->setRotation(node->getRotation().getInterpolated(rotation.second, 1.0f - movementPerStep));
        }
    }
} // namespace

void GraphicSystem::update(const uint32_t tickNumber, ScriptEngine&, const float)
{
    this->handleReceivedEvents();

    for (auto& entry : this->trackedEntities)
    {
        const auto graphic = this->handleManager.getAsValue<Components::Graphic>(entry.second.graphicH);
        const auto position =
            this->handleManager.getAsValue<Components::Position>(entry.second.positionH).position;

        auto position3D = this->gameMap->get3dPosition(position);
        position3D.Y = position3D.Y + graphic.offset * graphic.scale.Y;

        debugOutLevel(99, "3d position:", position3D.X, position3D.Y, position3D.Z);

        if (graphic.node != nullptr) // TODO check graphic needed (allready checked by trackEntity)
        {
            std::pair<bool, irr::core::vector3df> rotation = {false, {}};
            if (this->entityManager.hasComponent<Components::Rotation>(entry.first))
            {
                rotation.second =
                    this->handleManager
                        .getAsValue<Components::Rotation>(
                            this->entityManager.getComponent<Components::Rotation>(entry.first))
                        .rotation;
                rotation.first = true;
            }
            easeToActualPositionRotation(graphic.node, position3D, rotation);
        }
    }

    //return; // <-- might have to enable this if drawing too much and bgfx flips out

    // draw bounding boxes of the grid
    for (grid::gridCellID_t x = 0; x < this->grid.xNumberOfCells; x++)
    {
        for (grid::gridCellID_t z = 0; z < this->grid.zNumberOfCells; z++)
        {
            // draw lines top + right (the next gridCell will draw the left + bottom line (exept at
            // the very left/bottom)
            //     <----------+
            //      x         |
            //                |
            //                |
            //                |
            //                |
            //                v z
            //
            irr::core::vector3df topRight(x * this->grid.sideLength, 0, z * this->grid.sideLength);
            topRight.X = irr::core::clamp(topRight.X, 0.0f, 0.9999f * this->gameMap->getMapSize().X); // 0.9999f because getHeight() returns -INF if the point is outside of the map (which it might be if it is directly on the border of the map)
            topRight.Z = irr::core::clamp(topRight.Z, 0.0f, 0.9999f * this->gameMap->getMapSize().Y);
            topRight.Y = this->gameMap->getTerrainNode()->getHeight(topRight.X, topRight.Z);
            irr::core::vector3df topLeft(static_cast<irr::f32>(x + 1) * this->grid.sideLength,
                                         0.0f,
                                         topRight.Z);
            topLeft.X = irr::core::clamp(topLeft.X, 0.0f, 0.9999f * this->gameMap->getMapSize().X);
            topLeft.Z = irr::core::clamp(topLeft.Z, 0.0f, 0.9999f * this->gameMap->getMapSize().Y);
            topLeft.Y = this->gameMap->getTerrainNode()->getHeight(topLeft.X, topLeft.Z);
            this->game.line2Draw.push_back(
                line2Draw_s(topRight, topLeft, irr::video::SColor(255, 255, 255, 255)));
            irr::core::vector3df bottomRight(topRight.X,
                                             0.0f,
                                             static_cast<irr::f32>(z + 1) * this->grid.sideLength);
            bottomRight.X =
                irr::core::clamp(bottomRight.X, 0.0f, 0.9999f * this->gameMap->getMapSize().X);
            bottomRight.Z =
                irr::core::clamp(bottomRight.Z, 0.0f, 0.9999f * this->gameMap->getMapSize().Y);
            bottomRight.Y = this->gameMap->getTerrainNode()->getHeight(bottomRight.X, bottomRight.Z);
            this->game.line2Draw.push_back(
                line2Draw_s(topRight, bottomRight, irr::video::SColor(255, 255, 255, 255)));

            irr::core::vector3df bottomLeft(topLeft.X,
                                            this->gameMap->getTerrainNode()->getHeight(topLeft.X,
                                                                                       bottomRight.Z),
                                            bottomRight.Z);
            bottomLeft.X =
                irr::core::clamp(bottomLeft.X, 0.0f, 0.9999f * this->gameMap->getMapSize().X);
            bottomLeft.Z =
                irr::core::clamp(bottomLeft.Z, 0.0f, 0.9999f * this->gameMap->getMapSize().Y);
            if (z == this->grid.zNumberOfCells - 1)
            {
                this->game.line2Draw.push_back(
                    line2Draw_s(bottomLeft, bottomRight, irr::video::SColor(255, 255, 255, 255)));
            }
            if (x == this->grid.xNumberOfCells - 1)
            {
                this->game.line2Draw.push_back(
                    line2Draw_s(topLeft, bottomLeft, irr::video::SColor(255, 255, 255, 255)));
            }
        }
    }
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void GraphicSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::ChangeGraphicScale:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::ChangeGraphicScale>(ap);
                auto& graphic = this->handleManager.getAsRef<Components::Graphic>(
                    this->trackedEntities[event.entityID].graphicH);
                graphic.scale = event.newScale;
                graphic.node->setScale(event.newScale);
            }
            break;
            default:
            {
                Error::errTerminate("In GraphicSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}
