/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inventorySystem.h"

#include "../components.h"
#include "../entityManager.h"
#include "../events/eventManager.h"
#include "../../utils/HandleManager.h"

InventorySystem::InventorySystem(ThreadPool& threadPool_,
                                 EventManager& eventManager_,
                                 EntityManager& entityManager_,
                                 Handles::HandleManager& handleManager_,
                                 std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection_)
    : SystemBase(Systems::SystemIdentifiers::InventorySystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , blueprintCollection(blueprintCollection_)
{
    this->boundComponents.push_back(Components::ComponentID<Components::Inventory>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::AddItemToInventory>([this](const auto& event) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::AddItemToInventory, event);
    }));
    m_subscriptions.push_back(this->eventManager.subscribe<Events::RemoveItemFromInventory>([this](const auto& event) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::RemoveItemFromInventory, event);
    }));
    m_subscriptions.push_back(
        this->eventManager.subscribe<Events::DropItemFromInventoryToTheMap>([this](const auto& event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::DropItemFromInventoryToTheMap, event);
        }));
    m_subscriptions.push_back(this->eventManager.subscribe<Events::PickUpDroppedItem>([this](const auto& event) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::PickUpDroppedItem, event);
    }));
}

void InventorySystem::trackEntity(const entityID_t entityID,
                                  const componentID_t internalCompomentID,
                                  const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);
    POW2_ASSERT(this->handleManager.isValid(handle));

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    if (!this->entityManager.hasComponent<Components::Inventory>(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all needed Components available");
        return;
    }

    TrackedEntityHandleCollection newEntry;
    newEntry.inventoryH = this->entityManager.getComponent<Components::Inventory>(entityID);

    this->trackedEntities[entityID] = newEntry;
}

void InventorySystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void InventorySystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    (void) deltaTime;

    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void InventorySystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::AddItemToInventory:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::AddItemToInventory>(ap);
                this->handleAddItemToInventory(event.itemOwnerEntityID, event.itemEntityID);
            }
            break;
            case InternalEventTypes::RemoveItemFromInventory:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::RemoveItemFromInventory>(ap);
                this->handleRemoveItemFromInventory(event.itemOwnerEntityID, event.itemEntityID);
            }
            break;
            case InternalEventTypes::DropItemFromInventoryToTheMap:
            {
                const auto event =
                    this->receivedEvents.readNextEvent<Events::DropItemFromInventoryToTheMap>(ap);
                this->handleDropItemFromInventoryToTheMap(event.itemOwnerEntityID,
                                                          event.itemEntityID,
                                                          event.position);
            }
            break;
            case InternalEventTypes::PickUpDroppedItem:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::PickUpDroppedItem>(ap);
                this->handlePickUpDroppedItem(event.droppedItemChestEntityID);
            }
            break;
            default:
            {
                Error::errTerminate("In InventorySystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

void InventorySystem::handleAddItemToInventory(const entityID_t itemOwnerEntityID, const entityID_t itemEntityID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleAddItemToInventory");
    if (this->trackedEntities.count(itemOwnerEntityID))
    {
        auto& inventory =
            this->handleManager
                .getAsRef<Components::Inventory>(this->trackedEntities[itemOwnerEntityID].inventoryH)
                .inventory;
        for (unsigned int i = 0; i < 6; i++)
        {
            if (inventory[i] == ECS::INVALID_ENTITY)
            {
                inventory[i] = itemEntityID;
                break;
            }
        }
        this->eventManager.emit(Events::UpdateInventoryGui(itemOwnerEntityID));
        return;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "itemOwnerEntityID", itemOwnerEntityID, "is not tracked");
    return;
}

void InventorySystem::handleRemoveItemFromInventory(const entityID_t itemOwnerEntityID, const entityID_t itemEntityID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleRemoveItemFromInventory");
    if (this->trackedEntities.count(itemOwnerEntityID))
    {
        auto& inventory =
            this->handleManager
                .getAsRef<Components::Inventory>(this->trackedEntities[itemOwnerEntityID].inventoryH)
                .inventory;
        for (unsigned int i = 0; i < 6; i++)
        {
            if (inventory[i] == itemEntityID)
            {
                inventory[i] = ECS::INVALID_ENTITY;
                this->eventManager.emit(Events::UpdateInventoryGui(itemOwnerEntityID));
                return;
            }
        }
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't remove item ", itemEntityID, "no element of the Inventory");
    }
}

void InventorySystem::handleDropItemFromInventoryToTheMap(const entityID_t itemOwnerEntityID,
                                                          const entityID_t itemEntityID,
                                                          const irr::core::vector2df chestPosition)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleDropItemFromInventoryToTheMap");

    // TODO do in init (get droppedItemChestBlueprint)
    auto compare = [](const std::pair<EntityBlueprint, EntityBlueprint::BlueprintType> other) {
        return other.second == EntityBlueprint::BlueprintType::DroppedItemChest;
    };
    auto it = std::find_if(this->blueprintCollection.begin(), this->blueprintCollection.end(), compare);

    if (it == this->blueprintCollection.end())
    {
        Error::errContinue("Can't find DroppedItemChest blueprint");
        return;
    }

    EntityBlueprint droppedItemChestBlueprint = it->first;

    const auto PlayerBaseH = this->entityManager.getComponent<Components::PlayerBase>(itemOwnerEntityID);
    const auto PlayerBase = this->handleManager.getAsValue<Components::PlayerBase>(PlayerBaseH);


    const auto selectionIDH = this->entityManager.getComponent<Components::SelectionID>(PlayerBase.tankEntityID);
    const auto selectionID = this->handleManager.getAsValue<Components::SelectionID>(selectionIDH);

    Components::OwnerEntityID ownerEntityID;
    ownerEntityID.ownerEntityID = itemOwnerEntityID;

    Components::DroppedItemEntityID itemID;
    itemID.droppedItemEntityID = itemEntityID;

    Components::Position position;
    position.position = chestPosition;

    std::unordered_map<componentID_t, const void* const> initialValues;
    initialValues.insert(
        std::pair<componentID_t, const void* const>(Components::ComponentID<Components::SelectionID>::CID,
                                                    &selectionID));
    initialValues.insert(
        std::pair<componentID_t, const void* const>(Components::ComponentID<Components::OwnerEntityID>::CID,
                                                    &ownerEntityID));
    initialValues.insert(std::pair<componentID_t, const void* const>(
        Components::ComponentID<Components::DroppedItemEntityID>::CID, &itemID));
    initialValues.insert(
        std::pair<componentID_t, const void* const>(Components::ComponentID<Components::Position>::CID, &position));

    const auto droppedItemChestEntityID = droppedItemChestBlueprint.createEntity(initialValues);

    this->trackedDroppedItemChest.push_back(droppedItemChestEntityID);

    this->eventManager.emit(Events::RemoveItemFromInventory(itemOwnerEntityID, itemEntityID));
    this->eventManager.emit(Events::DeactivateItem(itemEntityID));
}

void InventorySystem::handlePickUpDroppedItem(const entityID_t droppedItemChestEntityID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handlePickUpDroppedItem");

    const auto ownerEntityIDH =
        this->entityManager.getComponent<Components::OwnerEntityID>(droppedItemChestEntityID);
    const auto ownerEntityID =
        this->handleManager.getAsValue<Components::OwnerEntityID>(ownerEntityIDH).ownerEntityID;

    const auto droppedItemEntityIDH =
        this->entityManager.getComponent<Components::DroppedItemEntityID>(droppedItemChestEntityID);
    const auto droppedItemEntityID =
        this->handleManager.getAsValue<Components::DroppedItemEntityID>(droppedItemEntityIDH).droppedItemEntityID;

    this->eventManager.emit(Events::AddItemToInventory(ownerEntityID, droppedItemEntityID));
    this->eventManager.emit(Events::ReactivateItem(droppedItemEntityID));

    this->entityManager.deleteEntity(droppedItemChestEntityID);
}

void InventorySystem::handleSellItem(const entityID_t selledItemEntityID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleSellItem");

    const auto ownerEntityIDH = this->entityManager.getComponent<Components::OwnerEntityID>(selledItemEntityID);
    const auto itemOwnerEntityID =
        this->handleManager.getAsValue<Components::OwnerEntityID>(ownerEntityIDH).ownerEntityID;

    const auto priceH = this->entityManager.getComponent<Components::Price>(selledItemEntityID);
    const auto price = this->handleManager.getAsValue<Components::Price>(priceH).price;

    this->eventManager.emit(Events::TransferMoney(itemOwnerEntityID, price / 2));
    this->eventManager.emit(Events::RemoveItemFromInventory(itemOwnerEntityID, selledItemEntityID));

    this->entityManager.deleteEntity(selledItemEntityID);
}
