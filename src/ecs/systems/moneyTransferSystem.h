/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MONEYTRANSFER_SYSTEM_H_
#define MONEYTRANSFER_SYSTEM_H_

#include <unordered_map>

#include "systemBase.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}

class MoneyTransferSystem : public SystemBase
{
   public:
    MoneyTransferSystem(ThreadPool& threadPool_,
                        EventManager& eventManager_,
                        EntityManager& entityManager_,
                        Handles::HandleManager& handleManager_);

    MoneyTransferSystem(const MoneyTransferSystem&) = delete;
    MoneyTransferSystem operator=(const MoneyTransferSystem&) = delete;

    virtual ~MoneyTransferSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

   private:
    struct TrackedEntityHandleCollection
    {
        Handles::Handle playerBaseH = Handles::InvalidHandle;
    };
    std::unordered_map<entityID_t, TrackedEntityHandleCollection> trackedEntities = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        TransferMoney = 0,
        NumInternalEvents,
    };

    void handleReceivedEvents();
    void handelMoneyTransfer(entityID_t entityID, int16_t amount);

    uint16_t incomePerSecond = 4;
    uint16_t incomePerFiveMinuts = 1000; // TODO get Real value
};

#endif /* MONEYTRANSFER_SYSTEM_H_ */
