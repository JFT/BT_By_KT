/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROJECTILE_SYSTEM_H
#define PROJECTILE_SYSTEM_H

#include <mutex>  // locking when receiving a teleport event
#include <random> //TODO: remove: only included for testing to set random movement targets
#include <string>
#include <unordered_map>

#include <irrlicht/SColor.h> // for pathing debug node coloring
#include <irrlicht/vector3d.h>

#include "systemBase.h"

#include "../../utils/Pow2Assert.h"
#include "../../utils/optimization.h" // non-initializing allocator + vector
#include "../../utils/performanceMeasurement.h"
#include "../../utils/variableUnorderedQuickBuffer.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}
class Game;
class GameMap;
namespace grid
{
    class Grid;
}
struct PathGrid;
namespace Components
{
    struct ProjectileComponent;
}


class ProjectileSystem : public SystemBase
{
   public:
    ProjectileSystem(ThreadPool& threadPool_,
                     EventManager& eventManager_,
                     EntityManager& entityManager_,
                     Handles::HandleManager& handleManager_,
                     GameMap* const gameMap_);

    ProjectileSystem(const ProjectileSystem&) = delete;
    ProjectileSystem operator=(const ProjectileSystem&) = delete;

    virtual ~ProjectileSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);


    void receiveTeleportationEvent(const Events::Teleport event);

    PerformanceMeasurement<float> updateLoopTiming = {};
    int outputEvery = 30;

   private:
    struct TrackedEntityHandleCollection
    {
        Handles::Handle positionHandle = Handles::InvalidHandle;
        Handles::Handle movementHandle = Handles::InvalidHandle;
        Handles::Handle damageHandle = Handles::InvalidHandle;
        entityID_t targetEntityID = ECS::INVALID_ENTITY;
        Handles::Handle targetPositionHandle = Handles::InvalidHandle;
    };
    std::unordered_map<entityID_t, TrackedEntityHandleCollection> trackedEntities = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        Teleport = 0,
        NumInternalEvents,
    };

    // Use the standard Mailbox just for handle received TeleportationEvents
    // handleReceivedEvents->handleReceivedTeleportationEvents
    // Don't add another Event to the Mailbox. It is important to ensure that
    // no other Event will treated for the TeleportationEvents
    void handleReceivedTeleportationEvents();

    std::vector<entityID_t> entityIDsToDelete = {};
    void deletePreviouslyMarkedProjectiles();

    GameMap* gameMap = nullptr;

}; // class ProjectileSystem : public SystemBase

#endif // PROJECTILE_SYSTEM_H
