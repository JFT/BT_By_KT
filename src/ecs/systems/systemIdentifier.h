/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef BT_ECS_SYSTEMS_SYSTEMIDENTIFIER_H
#define BT_ECS_SYSTEMS_SYSTEMIDENTIFIER_H

namespace Systems
{
    /// @brief this enum is used during saving/loading to
    /// load the correct saved events for each system.
    /// If a new system is added make sure NOT to change the numbers for the old
    /// systems.
    enum class SystemIdentifiers
    {
        AutomaticWeaponSystem = 0,
        CooldownSystem = 1,
        DamageSystem = 2,
        GraphicSystem = 3,
        HealthBarSystem = 4,
        InventoryGuiSystem = 5,
        ShopGuiSystem = 6,
        InventorySystem = 7,
        MoneyTransferSystem = 8,
        MovementSystem = 9,
        ProjectileSystem = 10,
        SelectionSystem = 11,
        ShopSystem = 12,
        TestSystem = 13,
        UserEventSystem = 14,
        NetworkComponentSyncronizationServer = 15,
        PathingSystem = 16,
    };
} // namespace Systems

#endif /* ifndef BT_ECS_SYSTEMS_SYSTEMIDENTIFIER_H */
