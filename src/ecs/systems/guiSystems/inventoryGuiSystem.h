/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INVENTORY_GUI_SYSTEM_H_
#define INVENTORY_GUI_SYSTEM_H_

#include <unordered_map>

#include "../systemBase.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}
class GameMap;
class CEGUIEnvironment;

namespace CEGUI
{
    class Window;
    class EventArgs;
    class String;
} // namespace CEGUI

class InventoryGuiSystem : public SystemBase
{
   public:
    InventoryGuiSystem(ThreadPool& threadPool_,
                       EventManager& eventManager_,
                       EntityManager& entityManager_,
                       Handles::HandleManager& handleManager_,
                       entityID_t myPlayerProfile_,
                       GameMap* const gameMap_,
                       CEGUIEnvironment& guiEnvironment_);

    InventoryGuiSystem(const InventoryGuiSystem&) = delete;
    InventoryGuiSystem operator=(const InventoryGuiSystem&) = delete;

    virtual ~InventoryGuiSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

   private:
    struct TrackedEntityHandleCollection
    {
        Handles::Handle inventoryH = Handles::InvalidHandle;
    };
    std::unordered_map<entityID_t, TrackedEntityHandleCollection> trackedEntities = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        InventorySelection = 0,
        EntityDeselected,
        UpdateInventoryGui,
        DroppedItemChestSelection,
        NumInternalEvents,
    };

    entityID_t myPlayerProfileEntityID = ECS::INVALID_ENTITY;
    Components::SelectionID::Team playerTeam = Components::SelectionID::Team::Invalid;

    GameMap* gameMap = nullptr;

    CEGUIEnvironment& guiEnvironment;

    entityID_t selectedInventoryEntityID = ECS::INVALID_ENTITY;


    ///< Hold a Pointer to the Root of the the Inventrory-Gui
    CEGUI::Window* const guiRoot;

    void updateInventoryGui();

    void handleReceivedEvents();
    void handleInventorySelected(const entityID_t inventoryEntityID);
    void handleInventoryDeselected();

    void handleUpdateInventoryGui(const entityID_t ownerEntityID);

    void handleDroppedItemChestSelection(const entityID_t droppedItemChestEntityID);

    bool handleItemDroppedOntheMap(const CEGUI::EventArgs& args);
    bool handleItemSwapedSlot(const CEGUI::EventArgs& args);
};

#endif /* INVENTORY_GUI_SYSTEM_H_ */
