/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef HEALTHBAR_SYSTEM_H
#define HEALTHBAR_SYSTEM_H

#include <unordered_map>

#include "../systemBase.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}

class Game;
namespace CEGUI
{
    class ProgressBar;
}

class HealthBarSystem : public SystemBase
{
   public:
    HealthBarSystem(ThreadPool& threadPool_,
                    EventManager& eventManager_,
                    EntityManager& entityManager_,
                    Handles::HandleManager& handleManager_,
                    Game& game_);

    HealthBarSystem(const HealthBarSystem&) = delete;
    HealthBarSystem operator=(const HealthBarSystem&) = delete;

    virtual ~HealthBarSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

   private:
    struct TrackedEntityHandleCollection
    {
        Handles::Handle physicalH = Handles::InvalidHandle;
        Handles::Handle positionH = Handles::InvalidHandle;
        Handles::Handle graphicH = Handles::InvalidHandle;
        CEGUI::ProgressBar* healthBar = nullptr;
    };
    std::unordered_map<entityID_t, TrackedEntityHandleCollection> trackedEntities = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        HealthChanged = 0,
        NumInternalEvents,
    };

    void handleReceivedEvents();
    void handleHealthChangedEvent(const entityID_t targetEntityID);

    Game& game;

    float screenWidth = 0.0f;

    // magic numbers to make its look pretty
    float healthBarHeight = 0.0125f;
    float healthBarWidth = 0.04f;
    float highOffset = 15.0f;

    void updateHealthBar();
};

#endif // HEALTHBAR_SYSTEM_H
