/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "healthBarSystem.h"

#include <CEGUI/WindowManager.h>
#include <CEGUI/widgets/ProgressBar.h>
#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/ISceneCollisionManager.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/IrrlichtDevice.h>

#include "../../components.h"
#include "../../entityManager.h"
#include "../../events/eventManager.h"
#include "../../../core/game.h"
#include "../../../utils/HandleManager.h"

HealthBarSystem::HealthBarSystem(ThreadPool& threadPool_,
                                 EventManager& eventManager_,
                                 EntityManager& entityManager_,
                                 Handles::HandleManager& handleManager_,
                                 Game& game_)
    : SystemBase(Systems::SystemIdentifiers::HealthBarSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , game(game_)
    , screenWidth(
          static_cast<float>(this->game.getDevice()->getVideoModeList()->getDesktopResolution().Width))
{
    this->boundComponents.push_back(Components::ComponentID<Components::Physical>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Position>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Graphic>::CID);

    m_subscriptions.push_back(
        this->eventManager.subscribe<Events::HealthChanged>([this](const Events::HealthChanged event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::HealthChanged, event);
        }));
}

void HealthBarSystem::trackEntity(const entityID_t entityID,
                                  const componentID_t internalCompomentID,
                                  const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    if (!this->entityManager.hasComponent<Components::Physical, Components::Position, Components::Graphic>(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all needed Components available");
        return;
    }

    TrackedEntityHandleCollection newEntry;
    newEntry.physicalH = this->entityManager.getComponent<Components::Physical>(entityID);
    newEntry.positionH = this->entityManager.getComponent<Components::Position>(entityID);
    newEntry.graphicH = this->entityManager.getComponent<Components::Graphic>(entityID);

    // Create and calibrate new healthBar
    CEGUI::String name = CEGUI::PropertyHelper<entityID_t>::toString(int(entityID));
    newEntry.healthBar = static_cast<CEGUI::ProgressBar*>(
        CEGUI::WindowManager::getSingleton().createWindow("BT/ProgressBar", name));
    const auto healthBarSize =
        CEGUI::USize(CEGUI::UDim(this->healthBarWidth, 0), CEGUI::UDim(this->healthBarHeight, 0));
    newEntry.healthBar->setSize(healthBarSize);
    newEntry.healthBar->setProgress(1.0f); // Initial progress of 100%
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(newEntry.healthBar);

    this->trackedEntities[entityID] = newEntry;
}

void HealthBarSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    CEGUI::WindowManager::getSingleton().destroyWindow(this->trackedEntities[entityID].healthBar);
    this->trackedEntities.erase(entityID);
}

void HealthBarSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    (void) deltaTime;

    this->handleReceivedEvents();

    // TODO maybe use position change events
    // TODO make pretty (use cameraPosition)
    const float x_offset = (this->healthBarWidth * this->screenWidth) / float(8.0); // magic number
    for (const auto& entry : this->trackedEntities)
    {
        const auto position =
            this->handleManager.getAsValue<Components::Position>(entry.second.positionH).position;
        const auto node = this->handleManager.getAsValue<Components::Graphic>(entry.second.graphicH).node;

        const float high = node->getTransformedBoundingBox().MaxEdge.Y + this->highOffset;
        const auto pos3d = irr::core::vector3df(position.X, high, position.Y);

        const auto screenPos =
            game.getSceneManager()->getSceneCollisionManager()->getScreenCoordinatesFrom3DPosition(pos3d);

        entry.second.healthBar->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, float(screenPos.X) + x_offset),
                                                            CEGUI::UDim(0.0f, float(screenPos.Y))));
    }
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void HealthBarSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::HealthChanged:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::HealthChanged>(ap);
                this->handleHealthChangedEvent(event.targetEntityID);
            }
            break;
            default:
            {
                Error::errTerminate("In HealthBarSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

void HealthBarSystem::handleHealthChangedEvent(const entityID_t targetEntityID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleHealthChangedEvent");
    if (!this->trackedEntities.count(targetEntityID))
    {
        Error::errContinue("Damage Event on untracked entity", targetEntityID);
        return;
    }
    const auto physical =
        this->handleManager.getAsValue<Components::Physical>(this->trackedEntities[targetEntityID].physicalH);

    this->trackedEntities[targetEntityID].healthBar->setProgress(float(physical.health / physical.maxHealth));
}
