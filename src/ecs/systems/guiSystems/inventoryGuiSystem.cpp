/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inventoryGuiSystem.h"

#include <CEGUI/BasicImage.h>
#include <CEGUI/String.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/widgets/Combobox.h>
#include <CEGUI/widgets/DragContainer.h>
#include <CEGUI/widgets/ListboxTextItem.h>
#include <irrlicht/ITexture.h>

#include "../../components.h"
#include "../../entityBlueprint.h"
#include "../../entityManager.h"
#include "../../events/eventManager.h"
#include "../../../gui/ceGuiEnvironment.h"
#include "../../../map/gamemap.h"
#include "../../../utils/HandleManager.h"

InventoryGuiSystem::InventoryGuiSystem(ThreadPool& threadPool_,
                                       EventManager& eventManager_,
                                       EntityManager& entityManager_,
                                       Handles::HandleManager& handleManager_,
                                       entityID_t myPlayerProfile_,
                                       GameMap* const gameMap_,
                                       CEGUIEnvironment& guiEnvironment_)
    : SystemBase(Systems::SystemIdentifiers::InventoryGuiSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , myPlayerProfileEntityID(myPlayerProfile_)
    , gameMap(gameMap_)
    , guiEnvironment(guiEnvironment_)
    , guiRoot(CEGUI::WindowManager::getSingleton().loadLayoutFromFile("inventoryGui.layout"))
{
    this->boundComponents.push_back(Components::ComponentID<Components::Inventory>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::InventorySelection>(
        [this](const Events::InventorySelection event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::InventorySelection, event);
        }));
    m_subscriptions.push_back(this->eventManager.subscribe<Events::EntityDeselected>(
        [this](const Events::EntityDeselected event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::EntityDeselected, event);
        }));
    m_subscriptions.push_back(this->eventManager.subscribe<Events::UpdateInventoryGui>(
        [this](const Events::UpdateInventoryGui event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::UpdateInventoryGui, event);
        }));
    m_subscriptions.push_back(this->eventManager.subscribe<Events::DroppedItemChestSelection>(
        [this](const Events::DroppedItemChestSelection event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::DroppedItemChestSelection, event);
        }));

    CEGUI::System::getSingleton()
        .getDefaultGUIContext()
        .getRootWindow()
        ->getChild("WindowRoot_Game")
        ->addChild(this->guiRoot);


    // TODO find a way to do this in the layout-file
    CEGUI::String nr;
    CEGUI::DragContainer* newItem = nullptr;
    CEGUI::Window* newIconImage = nullptr;
    CEGUI::String icon = "media/ecs/transparent.png";
    for (int i = 1; 6 >= i; i++)
    {
        nr = CEGUI::PropertyHelper<int>::toString(i);
        CEGUI::Window* slot = this->guiRoot->getChild("Window_Bag/Slot" + nr);

        newItem = new CEGUI::DragContainer("DragContainer", "DragContainer");
        newItem->setArea(CEGUI::UDim(0.f, 0.f), CEGUI::UDim(0.f, 0.f), CEGUI::UDim(1.f, 0.f), CEGUI::UDim(1.f, 0.f));
        newItem->setProperty("StickyMode", "false");
        newItem->setID(i - 1);
        newItem->setAlpha(0.f);
        newItem->setDraggingEnabled(false);
        newIconImage = CEGUI::WindowManager::getSingleton().createWindow("BT/StaticImage", "ItemIcon");
        newIconImage->setArea(CEGUI::UDim(0.05f, 0.f),
                              CEGUI::UDim(0.10f, 0.f),
                              CEGUI::UDim(0.90f, 0.f),
                              CEGUI::UDim(0.80f, 0.f));
        newIconImage->setProperty("Image", icon);
        newIconImage->setProperty("MousePassThroughEnabled", "true");
        newItem->addChild(newIconImage);
        slot->addChild(newItem);
        newItem->subscribeEvent(CEGUI::Window::EventDragDropItemDropped,
                                CEGUI::Event::Subscriber(&InventoryGuiSystem::handleItemSwapedSlot, this));
        newItem->subscribeEvent(CEGUI::DragContainer::EventDragEnded,
                                CEGUI::Event::Subscriber(&InventoryGuiSystem::handleItemDroppedOntheMap, this));
    }
}

void InventoryGuiSystem::trackEntity(const entityID_t entityID,
                                     const componentID_t internalCompomentID,
                                     const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    if (!this->entityManager.hasComponent<Components::Inventory>(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all needed Components available");
        return;
    }

    TrackedEntityHandleCollection newEntry;
    newEntry.inventoryH = this->entityManager.getComponent<Components::Inventory>(entityID);

    this->trackedEntities[entityID] = newEntry;
}

void InventoryGuiSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void InventoryGuiSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    (void) deltaTime;

    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void InventoryGuiSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::InventorySelection:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::InventorySelection>(ap);
                this->handleInventorySelected(event.entityID);
            }
            break;
            case InternalEventTypes::EntityDeselected:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::EntityDeselected>(ap);
                (void) event;
                this->handleInventoryDeselected();
            }
            break;
            case InternalEventTypes::UpdateInventoryGui:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::UpdateInventoryGui>(ap);
                this->handleUpdateInventoryGui(event.ownerEntityID);
            }
            break;
            case InternalEventTypes::DroppedItemChestSelection:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::DroppedItemChestSelection>(ap);
                this->handleDroppedItemChestSelection(event.droppedItemChestEntityID);
            }
            break;
            default:
            {
                Error::errTerminate("In InventoryGuiSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

void InventoryGuiSystem::updateInventoryGui()
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "updateInventoryGui");

    const auto selectedEntityID = this->selectedInventoryEntityID;
    if (selectedEntityID == ECS::INVALID_ENTITY)
    {
        Error::errContinue("Can't update InventoryGui of ECS::INVALID_ENTITY");
        return;
    }

    auto const inventory =
        this->handleManager
            .getAsRef<Components::Inventory>(this->trackedEntities[selectedEntityID].inventoryH)
            .inventory;

    Handles::Handle iconHandle = Handles::InvalidHandle;
    irr::video::ITexture* iconTexture = nullptr;
    CEGUI::String nr = "";
    CEGUI::Window* slot = nullptr;
    CEGUI::DragContainer* container = nullptr;
    for (unsigned int i = 0; i < 6; i++)
    {
        nr = CEGUI::PropertyHelper<int>::toString(i + 1);

        container = static_cast<CEGUI::DragContainer*>(
            this->guiRoot->getChild("Window_Bag/Slot" + nr + "/DragContainer"));
        if (selectedEntityID == this->myPlayerProfileEntityID)
        {
            container->setDraggingEnabled(true);
        }
        else
        {
            container->setDraggingEnabled(false);
        }

        if (inventory[i] != ECS::INVALID_ENTITY)
        {
            debugOutLevel(Debug::DebugLevels::updateLoop, "Update Slot", nr, "with Item ", inventory[i]);

            container->setAlpha(1.0);
            container->setDraggingEnabled(true);

            iconHandle = this->entityManager.getComponent<Components::Icon>(inventory[i]);
            iconTexture = this->handleManager.getAsValue<Components::Icon>(iconHandle).texture;

            slot = container->getChild("ItemIcon");

            this->guiEnvironment.setImageToWindow(slot, iconTexture, "Image");
            // debugOut("test");
            // CEGUI::String name = "media/ecs/weapons/w1_icon.png";
            // slot->setProperty("Image", name);
            // static_cast<CEGUI::BasicImage*>(slot)->setTexture(iconTexture);
        }
        else
        {
            container->setAlpha(0.0);
            container->setDraggingEnabled(false);
        }
    }
}

void InventoryGuiSystem::handleInventorySelected(const entityID_t entityID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleInventorySelected ", entityID);
    if (!this->trackedEntities.count(entityID))
    {
        Error::errContinue("Event on untracked entity", entityID);
        return;
    }
    this->selectedInventoryEntityID = entityID;
    this->guiRoot->setVisible(true);

    this->updateInventoryGui();
}

void InventoryGuiSystem::handleInventoryDeselected()
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleInventoryDeselected ");
    // TESTING BEGIN
    //    auto nameHandle =
    //    this->entityManager.getComponent<Components::EntityName>(this->selectedInventoryEntityID);
    //    auto name = this->handleManager.getAsValue<Components::EntityName>(nameHandle).entityName;
    //    debugOut("Inventory", irr::core::stringc(name).c_str());
    // TESTING END

    this->selectedInventoryEntityID = ECS::INVALID_ENTITY;
    this->guiRoot->setVisible(false);
}

void InventoryGuiSystem::handleUpdateInventoryGui(const entityID_t ownerEntityID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleUpdateInventoryGui ", ownerEntityID);
    if (ownerEntityID == this->selectedInventoryEntityID)
    {
        this->updateInventoryGui();
    }
}

void InventoryGuiSystem::handleDroppedItemChestSelection(const entityID_t droppedItemChestEntityID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleDroppedItemChestSelection ", droppedItemChestEntityID);
    this->eventManager.emit(Events::PickUpDroppedItem(droppedItemChestEntityID));
}

bool InventoryGuiSystem::handleItemDroppedOntheMap(const CEGUI::EventArgs& args)
{
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "handleItemDroppedOntheMap");

    const auto cursorPositionCEGUI =
        CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();
    if (this->guiRoot->getChild("Window_Bag")->isHit(cursorPositionCEGUI)) // TODO find a better way
                                                                           // or improve check
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "Dropped the item one the inventory gui");
        return true;
    }

    irr::core::vector2d<int> cursorPosition =
        irr::core::vector2d<int>(int(cursorPositionCEGUI.d_x), int(cursorPositionCEGUI.d_y));
    irr::core::vector2df mapPosition;
    if (!this->gameMap->calculateCursorTo2DMapPosition(cursorPosition, mapPosition))
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "Dropped the item outside the map");
        return true;
    }

    const CEGUI::WindowEventArgs& w_args = static_cast<const CEGUI::WindowEventArgs&>(args);
    const auto dropedItemSlot = w_args.window->getID();
    debugOut(dropedItemSlot);

    auto const inventoryHandle =
        this->entityManager.getComponent<Components::Inventory>(this->myPlayerProfileEntityID); // TODO check is clever
    auto const inventory = this->handleManager.getAsRef<Components::Inventory>(inventoryHandle).inventory;

    const auto dropedEntityID = inventory[dropedItemSlot];
    debugOut(dropedEntityID);

    this->eventManager.emit(
        Events::DropItemFromInventoryToTheMap(this->myPlayerProfileEntityID, dropedEntityID, mapPosition));
    return true;
}

bool InventoryGuiSystem::handleItemSwapedSlot(const CEGUI::EventArgs& args)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleItemSwapedSlot");
    entityID_t entityID = this->myPlayerProfileEntityID; // TODO check is clever

    // cast the args to the 'real' type so we can get access to extra fields
    const CEGUI::DragDropEventArgs& dd_args = static_cast<const CEGUI::DragDropEventArgs&>(args);
    auto dragContainer = dd_args.dragDropItem;
    auto dropContainer = dd_args.window;

    auto const inventoryHandle = this->entityManager.getComponent<Components::Inventory>(entityID);
    auto const inventory = this->handleManager.getAsRef<Components::Inventory>(inventoryHandle).inventory;

    debugOut(dragContainer->getID(), dropContainer->getID());
    const auto dragItem = inventory[dragContainer->getID()];
    inventory[dragContainer->getID()] = inventory[dropContainer->getID()];
    debugOut(dropContainer->getParent()->getName(), dropContainer->getID());
    inventory[dropContainer->getID()] = dragItem;

    this->updateInventoryGui();
    return true;
}
