/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "shopGuiSystem.h"

#include <CEGUI/BasicImage.h>
#include <CEGUI/String.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/widgets/Combobox.h>
#include <CEGUI/widgets/DragContainer.h>
#include <CEGUI/widgets/ListboxTextItem.h>
#include <irrlicht/ITexture.h>

#include "../../components.h"
#include "../../entityManager.h"
#include "../../events/eventManager.h"
#include "../../../gui/ceGuiEnvironment.h"
#include "../../../utils/HandleManager.h"

ShopGuiSystem::ShopGuiSystem(ThreadPool& threadPool_,
                             EventManager& eventManager_,
                             EntityManager& entityManager_,
                             Handles::HandleManager& handleManager_,
                             std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection_,
                             entityID_t myPlayerProfile_)
    : SystemBase(Systems::SystemIdentifiers::ShopGuiSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , blueprintCollection(blueprintCollection_)
    , myPlayerProfileEntityID(myPlayerProfile_)
    , guiRoot(CEGUI::WindowManager::getSingleton().loadLayoutFromFile("shopGui.layout"))
{
    this->boundComponents.push_back(Components::ComponentID<Components::PurchasableItems>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::SelectionID>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Position>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Range>::CID);

    m_subscriptions.push_back(
        this->eventManager.subscribe<Events::ShopSelection>([this](const Events::ShopSelection event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::ShopSelection, event);
        }));
    m_subscriptions.push_back(this->eventManager.subscribe<Events::EntityDeselected>(
        [this](const Events::EntityDeselected event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::EntityDeselected, event);
        }));

    CEGUI::System::getSingleton()
        .getDefaultGUIContext()
        .getRootWindow()
        ->getChild("WindowRoot_Game")
        ->addChild(this->guiRoot);

    this->shopItemsCombobox =
        static_cast<CEGUI::Combobox*>(this->guiRoot->getChild("Combobox_ShopItems"));

    this->guiRoot->getChild("Combobox_ShopItems")
        ->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted,
                         CEGUI::Event::Subscriber(&ShopGuiSystem::handleShopItemSelection, this));
}

void ShopGuiSystem::trackEntity(const entityID_t entityID,
                                const componentID_t internalCompomentID,
                                const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    if (!this->entityManager.hasComponent<Components::PurchasableItems, Components::SelectionID, Components::Position, Components::Range>(
            entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all needed Components available");
        return;
    }

    TrackedEntityHandleCollection newEntry;
    newEntry.purchasableItemsH = this->entityManager.getComponent<Components::PurchasableItems>(entityID);
    newEntry.selectionIDH = this->entityManager.getComponent<Components::SelectionID>(entityID);
    newEntry.positionH = this->entityManager.getComponent<Components::Position>(entityID);
    newEntry.rangeH = this->entityManager.getComponent<Components::Range>(entityID);

    this->trackedEntities[entityID] = newEntry;
}

void ShopGuiSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void ShopGuiSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    (void) deltaTime;

    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void ShopGuiSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::ShopSelection:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::ShopSelection>(ap);
                this->handleShopSelected(event.entityID);
            }
            break;
            case InternalEventTypes::EntityDeselected:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::EntityDeselected>(ap);
                (void) event;
                this->handleShopDeselected();
                ;
            }
            break;
            default:
            {
                Error::errTerminate("In ShopGuiSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

void ShopGuiSystem::handleShopSelected(entityID_t entityID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleShopSelection ", entityID);
    if (!this->trackedEntities.count(entityID))
    {
        Error::errContinue("Event on untracked ShopEntity", entityID);
        return;
    }

    this->selectedShopEntityID = entityID;

    // TESTING BEGIN
    if (this->entityManager.hasComponent<Components::EntityName>(this->selectedShopEntityID))
    {
        auto nameHandle = this->entityManager.getComponent<Components::EntityName>(this->selectedShopEntityID);
        auto name = this->handleManager.getAsValue<Components::EntityName>(nameHandle).entityName;
        debugOutLevel(Debug::DebugLevels::updateLoop, "Shop", irr::core::stringc(name).c_str());
    }
    // TESTING END

    auto purchasableItems =
        this->handleManager
            .getAsValue<Components::PurchasableItems>(this->trackedEntities[entityID].purchasableItemsH)
            .purchasableItems;

    this->shopItemsCombobox->resetList();
    if (purchasableItems.size() > 0)
    {
        this->shopItemsCombobox->setVisible(true);
    }

    std::string itemName = "";
    CEGUI::ListboxTextItem* textItem = nullptr;
    for (const auto& item : purchasableItems)
    {
        itemName = std::string(
            irr::core::stringc(
                this->handleManager
                    .getAsValue<Components::EntityName>(this->blueprintCollection[item].first.getComponent(
                        Components::ComponentID<Components::EntityName>::CID))
                    .entityName)
                .c_str());

        textItem = new CEGUI::ListboxTextItem(itemName);
        textItem->setID(CEGUI::uint(item));
        this->shopItemsCombobox->addItem(textItem);
    }
}

void ShopGuiSystem::handleShopDeselected()
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleShopDeselected");
    // TESTING BEGIN
    if (this->entityManager.hasComponent<Components::EntityName>(this->selectedShopEntityID))
    {
        auto nameHandle = this->entityManager.getComponent<Components::EntityName>(this->selectedShopEntityID);
        auto name = this->handleManager.getAsValue<Components::EntityName>(nameHandle).entityName;
        debugOutLevel(Debug::DebugLevels::updateLoop, "Shop", irr::core::stringc(name).c_str());
    }
    // TESTING END

    this->selectedShopEntityID = ECS::INVALID_ENTITY;
    this->shopItemsCombobox->setVisible(false);
}

bool ShopGuiSystem::handleShopItemSelection(const CEGUI::EventArgs& args)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "handleShopItemSelection");
    (void) args;
    // const CEGUI::WindowEventArgs& winArgs = static_cast<const CEGUI::WindowEventArgs&>(args);
    // debugOutLevel(Debug::DebugLevels::updateLoop, winArgs.window->getText(),
    // winArgs.window->getID());

    if (this->shopItemsCombobox->getSelectedItem())
    {
        blueprintID_t blueprintId = this->shopItemsCombobox->getSelectedItem()->getID();
        const auto blueprintType = blueprintCollection[blueprintId].second;
        if (blueprintType == EntityBlueprint::BlueprintType::Invalid)
        {
            debugOutLevel(Debug::DebugLevels::updateLoop, "Can't buy item of Invalid type");
            return true;
        }
        else if (this->isAllowedToBuy(this->selectedShopEntityID, blueprintId))
        {
            this->eventManager.emit(
                Events::TryToBuyAShoptItem(this->myPlayerProfileEntityID, this->selectedShopEntityID, blueprintId));
        }
    }

    return true;
}

bool ShopGuiSystem::isAllowedToBuy(const entityID_t shopEntityID, const blueprintID_t blueprintId)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "check isAllowedToBuy");
    const auto playserBaseH =
        this->entityManager.getComponent<Components::PlayerBase>(this->myPlayerProfileEntityID);
    auto& playerBase = this->handleManager.getAsRef<Components::PlayerBase>(playserBaseH);

    const auto currentTankEntityID = playerBase.tankEntityID;

    const auto shopEntityInfos = this->trackedEntities[shopEntityID];

    // check on the same team
    const auto tankSelectionIDH = this->entityManager.getComponent<Components::SelectionID>(currentTankEntityID);
    const auto tankSelectionID = this->handleManager.getAsValue<Components::SelectionID>(tankSelectionIDH);

    auto shopSelectionID = this->handleManager.getAsValue<Components::SelectionID>(shopEntityInfos.selectionIDH);

    if (tankSelectionID.team != shopSelectionID.team)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "Shop and buyer not on the same team. Unable to buy selected Item");
        return false;
    }

    // check buyer in range
    const auto tankPositionHandle = this->entityManager.getComponent<Components::Position>(currentTankEntityID);
    const auto tankPosition = this->handleManager.getAsValue<Components::Position>(tankPositionHandle);

    const auto shopPosition = this->handleManager.getAsRef<Components::Position>(shopEntityInfos.positionH);
    const auto shopRange = this->handleManager.getAsValue<Components::Range>(shopEntityInfos.rangeH);

    if (std::sqrt(std::pow(tankPosition.position.X - shopPosition.position.X, 2) +
                  std::pow(tankPosition.position.Y - shopPosition.position.Y, 2)) > shopRange.squaredRange)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Out of range. Unable to buy a new Item");
        return false;
    }

    // check price
    const auto blueprint = this->blueprintCollection[int(blueprintId)].first;
    const auto itemPriceH = blueprint.getComponent(Components::ComponentID<Components::Price>::CID);
    const auto itemPrice = this->handleManager.getAsValue<Components::Price>(itemPriceH).price;

    if (itemPrice > playerBase.money)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Not enough money. Unable to buy selected Item");
        return false;
    }

    const auto blueprintType = blueprintCollection[blueprintId].second;
    if (blueprintType == EntityBlueprint::BlueprintType::Tank)
    {
        // check requirement
        const auto tankRequirementH =
            blueprint.getComponent(Components::ComponentID<Components::Requirement>::CID);
        const auto tankRequirement =
            this->handleManager.getAsValue<Components::Requirement>(tankRequirementH).requirement;

        const auto currentRequirement = 100; // TODO get real value

        if (tankRequirement > currentRequirement)
        {
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "Requirement to high (",
                          tankRequirement,
                          " > ",
                          currentRequirement,
                          "). Unable to buy selected Tank");
            return false;
        }
    }
    else
    {
        auto const inventoryHandle =
            this->entityManager.getComponent<Components::Inventory>(this->myPlayerProfileEntityID);
        auto const inventory = this->handleManager.getAsRef<Components::Inventory>(inventoryHandle).inventory;

        // Check still slots free
        unsigned int emptySlots = 0;
        for (unsigned int i = 0; i < 6; i++)
        {
            if (inventory[i] == ECS::INVALID_ENTITY)
            {
                emptySlots++;
            }
        }
        if (emptySlots < 1)
        {
            debugOutLevel(Debug::DebugLevels::updateLoop, "No free Slot for new Item left");
            return false;
        }
    }

    return true;
}
