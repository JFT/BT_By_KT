/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SHOP_GUI_SYSTEM_H_
#define SHOP_GUI_SYSTEM_H_

#include <unordered_map>

#include "../systemBase.h"

#include "../../entityBlueprint.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}
class CEGUIEnvironment;
namespace CEGUI
{
    class Window;
    class EventArgs;
    class String;
    class Combobox;
} // namespace CEGUI

class ShopGuiSystem : public SystemBase
{
   public:
    ShopGuiSystem(ThreadPool& threadPool_,
                  EventManager& eventManager_,
                  EntityManager& entityManager_,
                  Handles::HandleManager& handleManager_,
                  std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection_,
                  entityID_t myPlayerProfile_);

    ShopGuiSystem(const ShopGuiSystem&) = delete;
    ShopGuiSystem operator=(const ShopGuiSystem&) = delete;

    virtual ~ShopGuiSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

   private:
    struct TrackedEntityHandleCollection
    {
        Handles::Handle purchasableItemsH = Handles::InvalidHandle;
        Handles::Handle selectionIDH = Handles::InvalidHandle;
        Handles::Handle positionH = Handles::InvalidHandle;
        Handles::Handle rangeH = Handles::InvalidHandle;
    };
    std::unordered_map<entityID_t, TrackedEntityHandleCollection> trackedEntities = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        ShopSelection = 0,
        EntityDeselected,
        NumInternalEvents,
    };

    std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection;

    entityID_t myPlayerProfileEntityID = ECS::INVALID_ENTITY;
    Components::SelectionID::Team playerTeam = Components::SelectionID::Team::Invalid;


    entityID_t selectedShopEntityID = ECS::INVALID_ENTITY;

    ///< Hold a Pointer to the Root of the the shop-Gui
    CEGUI::Window* const guiRoot;

    CEGUI::Combobox* shopItemsCombobox = nullptr;

    void handleReceivedEvents();
    void handleShopSelected(entityID_t shopEntityID);
    void handleShopDeselected();

    bool handleShopItemSelection(const CEGUI::EventArgs& args);

    bool isAllowedToBuy(const entityID_t shopEntityID, const blueprintID_t blueprintId);
};

#endif /* SHOP_GUI_SYSTEM_H_ */
