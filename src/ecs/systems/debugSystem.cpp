/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "debugSystem.h"

#include <irrlicht/IAnimatedMeshSceneNode.h>
#include "../entityManager.h"
#include "../events/eventManager.h"
#include "../../utils/HandleManager.h"

DebugSystem::DebugSystem(ThreadPool& threadPool_,
                         EventManager& eventManager_,
                         EntityManager& entityManager_,
                         Handles::HandleManager& handleManager_)
    : SystemBase(threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , selectedEntities(std::vector<std::vector<entityID_t>>(threadPool_.getNumberOfWorkerThreads() + 1))
{
    this->eventManager.subscribe<Events::OwnedEntitySelected>(
        [this](const Events::OwnedEntitySelected& e) { this->receiveSelectionEvent(e.entityID); });
    this->eventManager.subscribe<Events::NotOwnedEntitySelected>(
        [this](const Events::NotOwnedEntitySelected& e) { this->receiveSelectionEvent(e.entityID); });
}

void DebugSystem::trackEntity(const entityID_t entityID,
                              const componentID_t internalCompomentID,
                              const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);
    POW2_ASSERT(this->handleManager.isValid(handle));

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    if (!this->entityManager.hasComponent<Components::PurchasableItems, Components::SelectionID, Components::Position, Components::Range>(
            entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all needed Components available");
        return;
    }

    const auto selectionH = this->entityManager.getComponent<Components::SelectionID>(entityID);
    const auto selectionType = this->handleManager.getAsValue<Components::SelectionID>(selectionH).selectionType;
    if (selectionType != Components::SelectionID::SelectionType::ItemShop and
        selectionType != Components::SelectionID::SelectionType::TankShop)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't track entity '", entityID, "' Components::SelectionID::selectionType unequal Shop ");
        return;
    }

    TrackedEntityHandleCollection newEntry;
    newEntry.selectionIDH = this->entityManager.getComponent<Components::SelectionID>(entityID);
    newEntry.positionH = this->entityManager.getComponent<Components::Position>(entityID);
    newEntry.rangeH = this->entityManager.getComponent<Components::Range>(entityID);

    this->trackedEntities[entityID] = newEntry;
}

void DebugSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void DebugSystem::update(ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    (void) deltaTime;

    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void DebugSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::TryToBuyAShoptItem:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::TryToBuyAShoptItem>(ap);
                this->handleTryToBuyAShoptItem(event.buyerEntityID, event.shopEntityID, event.blueprintId);
            }
            break;
            default:
            {
                Error::errTerminate("In DebugSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}


void DebugSystem::receiveSelectionEvent(const entityID_t entityID)
{
    const size_t threadIndex = threadPool.getMappedIndex(std::this_thread::get_id());
    POW2_ASSERT(threadIndex < this->selectedEntities.size());
    // TODO: maybe check against double-insertion? Or use a set?
    this->selectedEntities[threadIndex].push_back(entityID);
}

void DebugSystem::showPathForSelectedEntities() const
{
#ifndef MAKE_SERVER_
    for (auto& selection : this->selectedEntities)
    {
        for (entityID_t entityID : selection)
        {
            if (!this->entityManager.hasComponent<Components::Position>(entityID))
            {
                continue;
            }
            const Handles::Handle positionHandle =
                this->entityManager.getComponent<Components::Position>(entityID);
            if (positionHandle == Handles::InvalidHandle)
            { // that entity didn't have a position
                continue;
            }
            const auto& tr = this->trackedEntities.getEntry(entityID, positionHandle);
            if (tr.entityID != ECS::INVALID_ENTITY)
            {
                POW2_ASSERT(tr.entityID == entityID);
                POW2_ASSERT(tr.positionHandle == positionHandle);
                auto debugColor = irr::video::SColor(255, 0, 0, 255);
                if (this->entityManager.hasComponent<Components::DebugColor>(tr.entityID))
                {
                    debugColor = this->handleManager
                                     .getAsValue<Components::DebugColor>(
                                         this->entityManager.getComponent<Components::DebugColor>(tr.entityID))
                                     .color;
                }
                const Components::Movement& movement =
                    this->handleManager.getAsRef<Components::Movement>(tr.data);
                irr::core::vector3df lastTarget3d = irr::core::vector3df(-100.0f, -100.0f, -100.0f);
                for (const auto& target : movement.waypoints)
                {
                    const irr::core::vector3df target3d = this->gameMap->get3dPosition(target);
                    if (lastTarget3d == irr::core::vector3df(-100.0f, -100.0f, -100.0f))
                    {
                        lastTarget3d = this->gameMap->get3dPosition(
                            this->handleManager.getAsValue<Components::Position>(tr.positionHandle).position);
                        debugOutLevel(Debug::DebugLevels::updateLoop + 2,
                                      "pos: (",
                                      lastTarget3d.X,
                                      ",",
                                      lastTarget3d.Z,
                                      ")",
                                      "tar: (",
                                      target.X,
                                      ",",
                                      target.Y,
                                      ")",
                                      "distSQ:",
                                      irr::core::vector2df(lastTarget3d.X, lastTarget3d.Z).getDistanceFromSQ(target),
                                      "expectedSQ",
                                      movement.expectedDistanceSQToWaypoint);
                    }
                    game.line2Draw.push_back(line2Draw_s(lastTarget3d, target3d, debugColor));
                    lastTarget3d = target3d;
                }
            } // if (tr.entityID != ECS::INVALID_ENTITY)
        }     // for (entityID_t entityID : selection)
    }         // for (auto& selection: this->selectedEntities)
#endif
}
