/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUTOMATICWEAPON_SYSTEM_H
#define AUTOMATICWEAPON_SYSTEM_H

#include <ctime>
#include <random>
#include <unordered_map>

#include "systemBase.h"

#include "../entityBlueprint.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}

class AutomaticWeaponSystem : public SystemBase
{
   public:
    AutomaticWeaponSystem(ThreadPool& threadPool_,
                          EventManager& eventManager_,
                          EntityManager& entityManager_,
                          Handles::HandleManager& handleManager_,
                          std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection_);

    AutomaticWeaponSystem(const AutomaticWeaponSystem&) = delete;
    AutomaticWeaponSystem operator=(const AutomaticWeaponSystem&) = delete;

    virtual ~AutomaticWeaponSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

   private:
    struct TrackedWeaponEntityHandleCollection
    {
        Handles::Handle weaponOwnerSelectionIDH = Handles::InvalidHandle;
        Handles::Handle weaponOwnerPositionH = Handles::InvalidHandle;
        Handles::Handle damageH = Handles::InvalidHandle;
        Handles::Handle rangeH = Handles::InvalidHandle;
        Handles::Handle ammoH = Handles::InvalidHandle;
    };
    std::unordered_map<entityID_t, TrackedWeaponEntityHandleCollection> trackedWeaponEntities = {};
    std::unordered_map<entityID_t, TrackedWeaponEntityHandleCollection> deactivatedWeaponEntities = {};
    struct TrackedTargetEntityHandleCollection
    {
        Handles::Handle physicalH = Handles::InvalidHandle;
        Handles::Handle positionH = Handles::InvalidHandle;
    };
    std::unordered_map<entityID_t, TrackedTargetEntityHandleCollection> trackedEntitiesOfTeam1AsTarget = {};
    std::unordered_map<entityID_t, TrackedTargetEntityHandleCollection> trackedEntitiesOfTeam2AsTarget = {};

    std::vector<entityID_t> possibleTargets = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        CooldownFinished = 0,
        DeactivateItem,
        ReactivateItem,
        NumInternalEvents,
    };

    std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection;

    // random number generator for random target selection
    std::mt19937 targetTwister = std::mt19937(std::time(nullptr)); // TODO find a better seed

    std::vector<entityID_t> weaponsReadyToFire_stack = {};
    std::vector<entityID_t> weaponsReadyToFire_store = {};

    void handleReceivedEvents();
    void prepareProjectileShooting(const entityID_t weaponID);
    void deactivateAutomaticWeapon(const entityID_t weaponID);
    void reactivateAutomaticWeapon(const entityID_t weaponID);
};

#endif // AUTOMATICWEAPON_SYSTEM_H
