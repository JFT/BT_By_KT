/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <irrlicht/aabbox3d.h>
#include <irrlicht/position2d.h> // can't be forward declared because it is a typedef (even though only references/pointers are used in this header)
#include <unordered_map>
#include <vector>

#include "systemBase.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}
namespace grid
{
    class Grid;
}
class Game;
class GameMap;
template <typename T>
class aabbox2d;
typedef class aabbox2d<int> aabbox2di;
namespace irr
{
    struct SEvent;
}

class PathingSystem : public SystemBase
{
   public:
    PathingSystem(ThreadPool& threadPool_,
                  EventManager& eventManager_,
                  EntityManager& entityManager_,
                  Handles::HandleManager& handleManager_,
                  GameMap* const gameMap_,
                  grid::Grid& grid_,
                  Game& game_);

    PathingSystem(const PathingSystem&) = delete;
    PathingSystem operator=(const PathingSystem&) = delete;

    virtual ~PathingSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

    void handleReceivedEvents();

    template <typename T> // definition after class definiton
    void receiveEvent(const size_t internalEventID, const T& event);

   private:
    std::unordered_map<entityID_t, Handles::Handle> trackedEntities = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    GameMap* gameMap = nullptr;
    grid::Grid& grid;

    Game& game;

    std::vector<entityID_t> selectedEntities = {};
};
