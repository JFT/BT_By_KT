/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "automaticWeaponSystem.h"

#include "../components.h"
#include "../entityManager.h"
#include "../events/eventManager.h"
#include "../../utils/HandleManager.h"
#include "../../utils/static/error.h"

AutomaticWeaponSystem::AutomaticWeaponSystem(ThreadPool& threadPool_,
                                             EventManager& eventManager_,
                                             EntityManager& entityManager_,
                                             Handles::HandleManager& handleManager_,
                                             std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection_)
    : SystemBase(Systems::SystemIdentifiers::AutomaticWeaponSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , blueprintCollection(blueprintCollection_)
{
    // this system tracks weapons and targets seperated from each other
    this->boundComponents.push_back(Components::ComponentID<Components::OwnerEntityID>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Ammo>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Range>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Damage>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Cooldown>::CID);

    this->boundComponents.push_back(Components::ComponentID<Components::Position>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::SelectionID>::CID);
    this->boundComponents.push_back(Components::ComponentID<Components::Physical>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::CooldownFinished>(
        [this](const Events::CooldownFinished event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::CooldownFinished, event);
        }));
    m_subscriptions.push_back(
        this->eventManager.subscribe<Events::DeactivateItem>([this](const Events::DeactivateItem event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::DeactivateItem, event);
        }));
    m_subscriptions.push_back(
        this->eventManager.subscribe<Events::ReactivateItem>([this](const Events::ReactivateItem event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::ReactivateItem, event);
        }));
}

void AutomaticWeaponSystem::trackEntity(const entityID_t entityID,
                                        const componentID_t internalCompomentID,
                                        const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);

    if (this->entityManager.hasComponent<Components::OwnerEntityID, Components::Ammo, Components::Range, Components::Damage, Components::Cooldown>(
            entityID))
    {
        if (this->trackedWeaponEntities.count(entityID))
        {
            debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking WeaponEntity", entityID);
            return;
        }

        debugOutLevel(Debug::DebugLevels::updateLoop, "Tracking '", entityID, "' as Weapon");

        const auto ownerEntityIDHandle = this->entityManager.getComponent<Components::OwnerEntityID>(entityID);
        const auto ownerEntityID =
            this->handleManager.getAsValue<Components::OwnerEntityID>(ownerEntityIDHandle).ownerEntityID;

        // debugOut("entityID", entityID, "ownerEntityID", ownerEntityID);
        TrackedWeaponEntityHandleCollection newEntry;

        // Check owner is a player so we need the position of his tank or the owner is a other
        // entity like a tower
        if (this->entityManager.hasComponent<Components::Position, Components::SelectionID>(ownerEntityID))
        {
            newEntry.weaponOwnerPositionH =
                this->entityManager.getComponent<Components::Position>(ownerEntityID);
            newEntry.weaponOwnerSelectionIDH =
                this->entityManager.getComponent<Components::SelectionID>(ownerEntityID);
        }
        else if (this->entityManager.hasComponent<Components::PlayerBase>(ownerEntityID))
        {
            const auto playerBaseH = this->entityManager.getComponent<Components::PlayerBase>(ownerEntityID);
            const auto playerBase = this->handleManager.getAsValue<Components::PlayerBase>(playerBaseH);

            if (this->entityManager.hasComponent<Components::Position, Components::SelectionID>(
                    playerBase.tankEntityID))
            {
                newEntry.weaponOwnerPositionH =
                    this->entityManager.getComponent<Components::Position>(playerBase.tankEntityID);
                newEntry.weaponOwnerSelectionIDH =
                    this->entityManager.getComponent<Components::SelectionID>(playerBase.tankEntityID);
            }
            else
            {
                debugOutLevel(Debug::DebugLevels::updateLoop,
                              "Can't track the entity",
                              entityID,
                              "the Tank has no Position or SelectionID Component");
                return;
            }
        }
        else
        {
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "Can't track the entity'",
                          entityID,
                          "' the ownerEntity has no Position or SelectionID Component");
            return;
        }
        newEntry.damageH = this->entityManager.getComponent<Components::Damage>(entityID);
        newEntry.rangeH = this->entityManager.getComponent<Components::Range>(entityID);
        newEntry.ammoH = this->entityManager.getComponent<Components::Ammo>(entityID);

        this->trackedWeaponEntities[entityID] = newEntry;
        debugOutLevel(Debug::DebugLevels::updateLoop, "Finally tracked '", entityID, "' as Weapon");
    }
    else if (this->entityManager.hasComponent<Components::Position, Components::SelectionID, Components::Physical>(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tracking '", entityID, "' as Target");

        TrackedTargetEntityHandleCollection newEntry;
        newEntry.physicalH = this->entityManager.getComponent<Components::Physical>(entityID);
        newEntry.positionH = this->entityManager.getComponent<Components::Position>(entityID);

        const auto selectioIDH = this->entityManager.getComponent<Components::SelectionID>(entityID);
        const auto& selectioID = this->handleManager.getAsValue<Components::SelectionID>(selectioIDH);

        // debugOut("target team", int(selectioID.team), int(selectioID.selectionType));
        if (selectioID.team == Components::SelectionID::Team::Team1)
        {
            if (this->trackedEntitiesOfTeam1AsTarget.find(entityID) !=
                this->trackedEntitiesOfTeam1AsTarget.end())
            {
                debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
                return;
            }
            this->trackedEntitiesOfTeam1AsTarget[entityID] = newEntry;
            debugOutLevel(Debug::DebugLevels::updateLoop, "Finally tracked entity", entityID, "of Team 1 as Target");
        }
        else if (selectioID.team == Components::SelectionID::Team::Team2)
        {
            if (this->trackedEntitiesOfTeam2AsTarget.find(entityID) !=
                this->trackedEntitiesOfTeam2AsTarget.end())
            {
                debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
                return;
            }
            this->trackedEntitiesOfTeam2AsTarget[entityID] = newEntry;
            debugOutLevel(Debug::DebugLevels::updateLoop, "Finally tracked entity", entityID, "of Team 2 as Target");
        }
        else
        {
            debugOutLevel(Debug::DebugLevels::updateLoop, "Can't track entity", entityID, "team is invalid");
            return;
        }
    }
    else
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't track entity", entityID, "not all needed Components available");
        return;
    }
}

void AutomaticWeaponSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (this->entityManager.hasComponent<Components::SelectionID>(entityID))
    {
        const auto selectioIDH = this->entityManager.getComponent<Components::SelectionID>(entityID);
        const auto team = this->handleManager.getAsValue<Components::SelectionID>(selectioIDH).team;

        if (team == Components::SelectionID::Team::Team1)
        {
            if (this->trackedEntitiesOfTeam1AsTarget.find(entityID) ==
                this->trackedEntitiesOfTeam1AsTarget.end())
            {
                debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
                return;
            }
            this->trackedEntitiesOfTeam1AsTarget.erase(entityID);
        }
        else if (team == Components::SelectionID::Team::Team2)
        {
            if (this->trackedEntitiesOfTeam2AsTarget.find(entityID) ==
                this->trackedEntitiesOfTeam2AsTarget.end())
            {
                debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
                return;
            }
            this->trackedEntitiesOfTeam2AsTarget.erase(entityID);
        }
    }
    else
    {
        if (!this->trackedWeaponEntities.count(entityID))
        {
            debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
            return;
        }
        this->trackedWeaponEntities.erase(entityID);
    }
}

void AutomaticWeaponSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    (void) deltaTime;

    this->handleReceivedEvents();

    // handle weapons already ready to fire (last time no target in range)
    // prepareProjectileShooting will push the weaponEntityID again to weaponsReadyToFire if unable
    // to fire
    std::swap(weaponsReadyToFire_stack, weaponsReadyToFire_store);
    for (const auto weaponEntityID : weaponsReadyToFire_store)
    {
        this->prepareProjectileShooting(weaponEntityID);
    }
    weaponsReadyToFire_store.clear();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void AutomaticWeaponSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::CooldownFinished:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::CooldownFinished>(ap);
                this->prepareProjectileShooting(event.entityID);
            }
            break;
            case InternalEventTypes::DeactivateItem:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::DeactivateItem>(ap);
                this->deactivateAutomaticWeapon(event.itemEntityID);
            }
            break;
            case InternalEventTypes::ReactivateItem:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::ReactivateItem>(ap);
                this->reactivateAutomaticWeapon(event.itemEntityID);
            }
            break;
            default:
            {
                Error::errTerminate("In AutomaticWeaponSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

void AutomaticWeaponSystem::prepareProjectileShooting(const entityID_t weaponID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "prepareProjectileShooting from Weapon", weaponID);
    if (!this->trackedWeaponEntities.count(weaponID))
    {
        return;
    }

    const auto weapon = this->trackedWeaponEntities[weaponID];
    const auto weaponSquaredRange = this->handleManager.getAsValue<Components::Range>(weapon.rangeH).squaredRange;
    const auto startPosition = this->handleManager.getAsValue<Components::Position>(weapon.weaponOwnerPositionH);
    const auto tankPosition = startPosition.position;
    const auto team =
        this->handleManager.getAsValue<Components::SelectionID>(weapon.weaponOwnerSelectionIDH).team;
    const auto attacksUnitType =
        this->handleManager.getAsValue<Components::Damage>(weapon.damageH).attacksUnitTypeFlag;

    std::unordered_map<entityID_t, TrackedTargetEntityHandleCollection>* targetPool = nullptr;
    if (team == Components::SelectionID::Team::Team1)
    {
        targetPool = &this->trackedEntitiesOfTeam2AsTarget;
    }
    else if (team == Components::SelectionID::Team::Team2)
    {
        targetPool = &this->trackedEntitiesOfTeam1AsTarget;
    }

    this->possibleTargets.clear();
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "Searching targets in range", targetPool->size());
    for (const auto& target : *targetPool)
    {
        const auto unitType =
            this->handleManager.getAsValue<Components::Physical>(target.second.physicalH).unitTypeFlag;
        const auto targetPos =
            this->handleManager.getAsValue<Components::Position>(target.second.positionH).position;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                      "attacksUnitType",
                      attacksUnitType,
                      "unitType",
                      unitType,
                      (attacksUnitType & unitType));
        if (((int(attacksUnitType) == -1) or
             ((attacksUnitType != flag_t(UnitTypeE::Invalid) and (attacksUnitType & unitType) == attacksUnitType))) and
            (std::pow(tankPosition.X - targetPos.X, 2) + std::pow(tankPosition.Y - targetPos.Y, 2) <= weaponSquaredRange))
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop, "Add Entity", target.first, "to the potential target list");
            this->possibleTargets.push_back(target.first);
        }
    }

    int nrOfTargetsInRange = int(possibleTargets.size());
    if (nrOfTargetsInRange == 0)
    {
        // will be handled into the update loop
        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "No targets in range");
        this->weaponsReadyToFire_stack.push_back(weaponID);
        return;
    }

    auto weaponDamage = this->handleManager.getAsValue<Components::Damage>(weapon.damageH);

    auto ammo = this->handleManager.getAsValue<Components::Ammo>(weapon.ammoH);
    const auto projectileBlueprint = this->blueprintCollection[ammo.projectilID].first;

    const auto projMovHandle =
        projectileBlueprint.getComponent(Components::ComponentID<Components::ProjectileMovement>::CID);
    // TODO check projMovHandle invalid
    auto projectileMovement = this->handleManager.getAsValue<Components::ProjectileMovement>(projMovHandle);

    std::uniform_int_distribution<irr::u32> targetEntityDist(0, nrOfTargetsInRange - 1);

    entityID_t targetID = ECS::INVALID_ENTITY;
    for (int proj = 1; proj <= ammo.nrProjectiles; proj++)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "shoot projectile");
        // random number generator for random target selection
        const auto targetIndex = int(targetEntityDist(this->targetTwister));
        targetID = this->possibleTargets.at(targetIndex);
        projectileMovement.targetEntity = targetID;

        std::unordered_map<componentID_t, const void* const> initialValues;
        initialValues.insert(std::pair<componentID_t, const void* const>(
            Components::ComponentID<Components::ProjectileMovement>::CID, &projectileMovement));
        initialValues.insert(
            std::pair<componentID_t, const void* const>(Components::ComponentID<Components::Damage>::CID,
                                                        &weaponDamage));
        initialValues.insert(
            std::pair<componentID_t, const void* const>(Components::ComponentID<Components::Position>::CID,
                                                        &startPosition));

        projectileBlueprint.createEntity(initialValues);

        // debug
        const auto ownerEntityIDHandle = this->entityManager.getComponent<Components::OwnerEntityID>(weaponID);
        const auto ownerEntityID =
            this->handleManager.getAsValue<Components::OwnerEntityID>(ownerEntityIDHandle).ownerEntityID;
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "Shooting projectile from Source with ID",
                      ownerEntityID,
                      " with the weapon with ID",
                      weaponID,
                      " to the target with ID",
                      targetID);
    }
    this->eventManager.emit(Events::StartCooldown{weaponID});
}

void AutomaticWeaponSystem::deactivateAutomaticWeapon(const entityID_t weaponID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "deactivateAutomaticWeapon");
    if (this->trackedWeaponEntities.count(weaponID))
    {
        this->deactivatedWeaponEntities[weaponID] = this->trackedWeaponEntities[weaponID];
        this->trackedWeaponEntities.erase(weaponID);
    }
}

void AutomaticWeaponSystem::reactivateAutomaticWeapon(const entityID_t weaponID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "reactivateAutomaticWeapon");
    if (this->deactivatedWeaponEntities.find(weaponID) != this->deactivatedWeaponEntities.end())
    {
        this->trackedWeaponEntities[weaponID] = this->deactivatedWeaponEntities[weaponID];
        this->deactivatedWeaponEntities.erase(weaponID);

        const auto cooldownH = this->entityManager.getComponent<Components::Cooldown>(weaponID);
        const auto cooldown = this->handleManager.getAsValue<Components::Cooldown>(cooldownH);

        if (!cooldown.active)
        {
            this->prepareProjectileShooting(weaponID);
        }
    }
}
