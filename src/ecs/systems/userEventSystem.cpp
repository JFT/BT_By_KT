/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz, Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "userEventSystem.h"

#include "../userinput/inputContextManager.h"
#include "../../utils/Pow2Assert.h"
#include "../../utils/debug.h"
#include "../../utils/static/error.h"

UserEventSystem::UserEventSystem(ThreadPool& threadPool_, EventManager& eventManager_, InputContextManager& inputContextManager_)
    : SystemBase(Systems::SystemIdentifiers::UserEventSystem, threadPool_, eventManager_)
    , inputContextManager(inputContextManager_)
{
}

void UserEventSystem::update(const uint32_t tickNumber, ScriptEngine& /* scriptEngine */, const float /* deltaTime */)
{
    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void UserEventSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            default:
            {
                Error::errTerminate("In UserEventSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

bool UserEventSystem::processInputEvents(const irr::SEvent& event)
{
    return inputContextManager.onEvent(event);
}
