/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "moneyTransferSystem.h"

#include "../components.h"
#include "../entityManager.h"
#include "../events/eventManager.h"
#include "../../utils/HandleManager.h"

MoneyTransferSystem::MoneyTransferSystem(ThreadPool& threadPool_,
                                         EventManager& eventManager_,
                                         EntityManager& entityManager_,
                                         Handles::HandleManager& handleManager_)
    : SystemBase(Systems::SystemIdentifiers::MoneyTransferSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
{
    this->boundComponents.push_back(Components::ComponentID<Components::PlayerBase>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::TransferMoney>([this](const auto& event) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::TransferMoney, event);
    }));
}

void MoneyTransferSystem::trackEntity(const entityID_t entityID,
                                      const componentID_t internalCompomentID,
                                      const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    (void) handle;
    POW2_ASSERT(handle != Handles::InvalidHandle);
    POW2_ASSERT(this->handleManager.isValid(handle));

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    if (!this->entityManager.hasComponent<Components::PlayerBase>(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Can't tracking entity", entityID, "not all needed Components available");
        return;
    }

    TrackedEntityHandleCollection newEntry;
    newEntry.playerBaseH = this->entityManager.getComponent<Components::PlayerBase>(entityID);

    this->trackedEntities[entityID] = newEntry;
}

void MoneyTransferSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void MoneyTransferSystem::update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime)
{
    (void) scriptEngine;
    (void) deltaTime;

    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void MoneyTransferSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::TransferMoney:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::TransferMoney>(ap);
                this->handelMoneyTransfer(event.entityID, event.amount);
            }
            break;
            default:
            {
                Error::errTerminate("In MoneyTransferSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

void MoneyTransferSystem::handelMoneyTransfer(entityID_t entityID, int16_t amount)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "handelMoneyTransfer",
                  entityID,
                  this->trackedEntities.size());

    auto& credit = this->handleManager
                       .getAsRef<Components::PlayerBase>(this->trackedEntities[entityID].playerBaseH)
                       .money;

    debugOutLevel(Debug::DebugLevels::updateLoop, "old credit: ", credit, ", amount: ", amount);
    POW2_ASSERT(credit > (-1 * amount)); // unsigned int can't check credit < 0
    credit = static_cast<uint16_t>(credit + amount);
    debugOutLevel(Debug::DebugLevels::updateLoop, "new credit: ", credit);
}
