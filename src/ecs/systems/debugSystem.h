/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SHOP_SYSTEM_H
#define SHOP_SYSTEM_H

#include <unordered_map>

#include "systemBase.h"
#include "../entityBlueprint.h"

// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}
class EntityBlueprint;

class ShopSystem : public SystemBase // TODO clean code
{
   public:
    ShopSystem(ThreadPool& threadPool_,
               EventManager& eventManager_,
               EntityManager& entityManager_,
               Handles::HandleManager& handleManager_);

    ShopSystem(const ShopSystem&) = delete;
    ShopSystem operator=(const ShopSystem&) = delete;

    virtual ~ShopSystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(ScriptEngine& scriptEngine, const float deltaTime);

   private:
    struct TrackedEntityHandleCollection
    {
        Handles::Handle selectionIDH = Handles::InvalidHandle;
        Handles::Handle positionH = Handles::InvalidHandle;
        Handles::Handle rangeH = Handles::InvalidHandle;
    };
    std::unordered_map<entityID_t, TrackedEntityHandleCollection> trackedEntities = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        TryToBuyAShoptItem = 0,
        NumInternalEvents,
    };

    void handleReceivedEvents();

    std::vector<std::vector<entityID_t>> selectedEntities;
    void showPathForSelectedEntities() const;
};

#endif /* SHOP_SYSTEM_H */
