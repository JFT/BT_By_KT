/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "cooldownSystem.h"

#include "../componentID.h"
#include "../components.h"
#include "../events/eventManager.h"
#include "../../utils/HandleManager.h"
#include "../../utils/threadPool.hpp"

CooldownSystem::CooldownSystem(ThreadPool& threadPool_, EventManager& eventManager_, Handles::HandleManager& handleManager_)
    : SystemBase(Systems::SystemIdentifiers::CooldownSystem, threadPool_, eventManager_)
    , handleManager(handleManager_)
    , startEvents(std::vector<std::vector<entityID_t>>(threadPool_.getNumberOfWorkerThreads() + 1)) // + 1 because there are X worker threads and the main thread occupies an extra threadID
{
    this->boundComponents.push_back(Components::ComponentID<Components::Cooldown>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::StartCooldown>([this](const auto& event) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::StartCooldown, event);
    }));
}


void CooldownSystem::trackEntity(const entityID_t entityID,
                                 const componentID_t internalCompomentID,
                                 const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    POW2_ASSERT(handle != Handles::InvalidHandle);

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Already tracking entity", entityID);
        return;
    }

    this->trackedEntities[entityID] = handle;
}

void CooldownSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void CooldownSystem::update(const uint32_t tickNumber, ScriptEngine&, const irr::f32 deltaTime)
{
    for (auto& threadEvents : this->startEvents)
    {
        for (const auto entityID : threadEvents)
        {
            POW2_ASSERT(this->trackedEntities.find(entityID) != this->trackedEntities.end());
            auto& cd = this->handleManager.getAsRef<Components::Cooldown>(this->trackedEntities[entityID]);
            cd.left = cd.initial;
            cd.active = true;
        }
        threadEvents.clear();
    }

    for (auto entry : this->trackedEntities)
    {
        auto& cd = this->handleManager.getAsRef<Components::Cooldown>(entry.second);
        if (cd.active)
        {
            cd.left -= deltaTime;
            if (cd.left <= 0)
            {
                this->eventManager.emit(Events::CooldownFinished{entry.first});
                cd.active = false;
            }
        }
    }
    this->handleReceivedEvents();
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void CooldownSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "Trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::StartCooldown:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::StartCooldown>(ap);
                this->receiveStartCooldown(event.entityID);
            }
            break;
            default:
            {
                Error::errTerminate("In CooldownSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}

void CooldownSystem::receiveStartCooldown(const entityID_t entityID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "receiveStartCooldown");
    const size_t threadIndex = this->threadPool.getMappedIndex(std::this_thread::get_id());
    this->startEvents[threadIndex].push_back(entityID);
}
