/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz, Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef USEREVENT_SYSTEM_H
#define USEREVENT_SYSTEM_H

#include <random>
#include <string>

#include "systemBase.h"

// forward declarations
namespace irr
{
    namespace scene
    {
        class ITerrainSceneNode;
    }
} // namespace irr
namespace Components
{
    struct UserEventComponent;
}
class InputContextManager;
namespace irr
{
    struct SEvent;
}

// create the defaultInputcontext


class UserEventSystem : public SystemBase
{
   public:
    UserEventSystem(ThreadPool& threadPool_, EventManager& eventManager_, InputContextManager& inputContextManager_);

    UserEventSystem(const UserEventSystem&) = delete;
    UserEventSystem operator=(const UserEventSystem&) = delete;

    virtual ~UserEventSystem(){};

    virtual void trackEntity(const entityID_t /* entityID */,
                             const componentID_t /* internalCompomentID */,
                             const Handles::Handle /* handle */){};

    virtual void
    untrackEntity(const entityID_t /* entityID */, const componentID_t /* internalCompomentID */){};

    virtual void update(const uint32_t tickNumber, ScriptEngine& /* scriptEngine */, const float /* deltaTime */);


    bool processInputEvents(const irr::SEvent& event);

   private:
    InputContextManager& inputContextManager;

    enum InternalEventTypes : Events::EventID_t
    {
        NumInternalEvents = 0,
    };

    void handleReceivedEvents();
};

#endif // USEREVENT_SYSTEM_H
