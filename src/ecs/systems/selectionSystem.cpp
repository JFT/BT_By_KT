/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "selectionSystem.h"

#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/ISceneCollisionManager.h>
#include <irrlicht/ISceneManager.h>

#include "../components.h"
#include "../entityManager.h"
#include "../events/eventManager.h"
#include "../userinput/defaultInputContext.h"
#include "../userinput/inputContext.hpp"
#include "../userinput/inputContextManager.h"
#include "../userinput/selectionBoxContext.h"
#include "../../core/game.h"
#include "../../grid/grid.h"
#include "../../map/gamemap.h"
#include "../../utils/HandleManager.h"
#include "../../utils/aabbox2d.h" // allows using screen coordinate boxes like 3d aabboxes

namespace
{
    std::tuple<grid::gridCellID_t, grid::gridCellID_t, grid::gridCellID_t, grid::gridCellID_t>
    coveredGridCells(const irr::scene::TerrainSceneNode* const terrain, const grid::Grid& grid)
    {
        // TODO: reseve this memory one time instead of every event in this call
        // the click might have happened into any visible patch (attention: patch (visibility) !=
        // gridCell (movement, shooting-range))
        irr::core::array<irr::s32> patchesLOD;
        terrain->getCurrentLODOfPatches(patchesLOD);

        // entities in all visible cellIDs should be checked
        // and a single patch might span multiple gridCells -> find out the x/z cell coordinates we
        // need to iterate over
        POW2_ASSERT(grid.xNumberOfCells >= 1);
        grid::gridCellID_t minXCellID = static_cast<grid::gridCellID_t>(grid.xNumberOfCells - 1);
        grid::gridCellID_t maxXCellID = 0;
        POW2_ASSERT(grid.zNumberOfCells >= 1);
        grid::gridCellID_t minZCellID = static_cast<grid::gridCellID_t>(grid.zNumberOfCells - 1);
        grid::gridCellID_t maxZCellID = 0;
        for (irr::u32 patchIndex = 0; patchIndex < patchesLOD.size(); patchIndex++)
        {
            if (patchesLOD[patchIndex] < 0)
            { // LOD < 0 -> patch is invisible
                continue;
            }
            const irr::core::aabbox3df BBox = terrain->getBoundingBox(patchIndex);
            // which gridCell does the bounding box touch?
            // the bounding box has a total of 8 corners but they are axis-aligned -> use only the
            // bottom 4 (X-Z-plane) to determine gridCellIDs
            const grid::gridCellID_t patchCellXmin =
                std::min(grid.getXIndex(BBox.MinEdge.X), grid.getXIndex(BBox.MaxEdge.X));
            minXCellID = std::min(minXCellID, patchCellXmin);
            POW2_ASSERT(minXCellID >= 0);
            const grid::gridCellID_t patchCellXmax =
                std::max(grid.getXIndex(BBox.MinEdge.X), grid.getXIndex(BBox.MaxEdge.X));
            maxXCellID = std::max(maxXCellID, patchCellXmax);
            POW2_ASSERT(maxXCellID < grid.xNumberOfCells);

            const grid::gridCellID_t patchCellZmin =
                std::min(grid.getZIndex(BBox.MinEdge.Z), grid.getZIndex(BBox.MaxEdge.Z));
            minZCellID = std::min(minZCellID, patchCellZmin);
            POW2_ASSERT(minZCellID >= 0);
            const grid::gridCellID_t patchCellZmax =
                std::max(grid.getZIndex(BBox.MinEdge.Z), grid.getZIndex(BBox.MaxEdge.Z));
            maxZCellID = std::max(maxZCellID, patchCellZmax);
            POW2_ASSERT(maxZCellID < grid.zNumberOfCells);
        }

        return {minXCellID, maxXCellID, minZCellID, maxZCellID};
    }
} // namespace

SelectionSystem::SelectionSystem(ThreadPool& threadPool_,
                                 EventManager& eventManager_,
                                 EntityManager& entityManager_,
                                 Handles::HandleManager& handleManager_,
                                 InputContextManager& inputContextManager_,
                                 GameMap* const gameMap_,
                                 grid::Grid& grid_,
                                 Game& game_)
    : SystemBase(Systems::SystemIdentifiers::SelectionSystem, threadPool_, eventManager_)
    , entityManager(entityManager_)
    , handleManager(handleManager_)
    , inputContextManager(inputContextManager_)
    , gameMap(gameMap_)
    , grid(grid_)
    , game(game_)
{
    this->boundComponents.push_back(Components::ComponentID<Components::SelectionID>::CID);

    m_subscriptions.push_back(this->eventManager.subscribe<Events::PartialEntitySelection>([this](const auto& event) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::PartialEntitySelection, event);
    }));
    m_subscriptions.push_back(this->eventManager.subscribe<Events::EntityDeselected>([this](const auto& event) {
        this->receivedEvents.receiveThreadedEvent(InternalEventTypes::EntityDeselected, event);
    }));

    // create a selection box input state and enter it when pressing the mouse down inside the default state
    auto selectionContext = static_cast<std::unique_ptr<InputContext>>(std::make_unique<SelectionBoxContext>(
        [this](const aabbox2di& area) { this->handleSelectionDone(area); }));

    inputContextManager.setInputContext(InputContextTypes::ENTITY_SELECTION_BOX, std::move(selectionContext));

    const auto mouseDown = InputContext::generateUniqueEventValue(irr::EEVENT_TYPE::EET_MOUSE_INPUT_EVENT,
                                                                  irr::EMIE_LMOUSE_PRESSED_DOWN);
    inputContextManager.addTransition(InputContextTypes::DEFAULT_CONTEXT,
                                      mouseDown,
                                      InputContextTypes::ENTITY_SELECTION_BOX,
                                      true);
}

void SelectionSystem::trackEntity(const entityID_t entityID,
                                  const componentID_t internalCompomentID,
                                  const Handles::Handle handle)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "started tracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was added!");

    POW2_ASSERT(handle != Handles::InvalidHandle);

    if (this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "already tracking entity", entityID);
        return;
    }

    this->trackedEntities[entityID] = handle;
}

void SelectionSystem::untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "Untracking entity",
                  entityID,
                  " because internal component id ",
                  internalCompomentID,
                  " was removed!");

    if (!this->trackedEntities.count(entityID))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Tried to remove untracked entity", entityID);
        return;
    }

    this->trackedEntities.erase(entityID);
}

void SelectionSystem::update(const uint32_t tickNumber, ScriptEngine&, const float)
{
    this->handleReceivedEvents();

    const auto selectionHandling =
        static_cast<SelectionBoxContext*>(inputContextManager.getInputContext(InputContextTypes::ENTITY_SELECTION_BOX))
            ->selectionHandling;

    if (selectionHandling.inputState == SelectionBoxContext::SelectionHandling::InputStates::E_SelectionSquare)
    {
        this->highlightOrSelectionSquare(false, selectionHandling.currentSelection);
    }
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void SelectionSystem::handleReceivedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::PartialEntitySelection:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::PartialEntitySelection>(ap);
                entityID_t entityID = event.entityID;
                const Handles::Handle entry = this->trackedEntities[entityID];
                POW2_ASSERT(entry != Handles::InvalidHandle);
                Components::SelectionID const* pcomp =
                    this->handleManager.getAsPtr<Components::SelectionID>(entry);
                if (event.selectionPercent >= minimalSelectionPercentage)
                {
                    if (pcomp->selectionType != Components::SelectionID::SelectionType::Invalid)
                    {
                        /*
                        if (ownedUnitSelectionEvents == 0)
                        {
                            this->selectedEntities.clear();
                            ownedUnitSelectionEvents++;
                        }
                        */

                        debugOutLevel(Debug::DebugLevels::updateLoop + 2, "Select Entity: ", entityID, " owned by player");
                        this->selectedEntities.push_back(entityID);
                        this->eventManager.emit(Events::OwnedEntitySelected(entityID));

                        // Check Inventory selection
                        if (this->entityManager.hasComponent<Components::Inventory>(entityID))
                        {
                            this->eventManager.emit(Events::InventorySelection(entityID));
                        }
                        else if (this->entityManager.hasComponent<Components::OwnerEntityID>(entityID))
                        {
                            const auto ownerEntityIDH =
                                this->entityManager.getComponent<Components::OwnerEntityID>(entityID);
                            const auto ownerEntityID = this->handleManager
                                                           .getAsValue<Components::OwnerEntityID>(ownerEntityIDH)
                                                           .ownerEntityID;
                            if (this->entityManager.hasComponent<Components::Inventory>(ownerEntityID))
                            {
                                this->eventManager.emit(Events::InventorySelection(ownerEntityID));
                            }
                        }
                        // Check DroppedItemEntityID selection
                        if (this->entityManager.hasComponent<Components::DroppedItemEntityID>(entityID))
                        {
                            this->eventManager.emit(Events::DroppedItemChestSelection(entityID));
                        }
                        // Check Shop selection
                        if (this->entityManager.hasComponent<Components::PurchasableItems>(entityID))
                        {
                            this->eventManager.emit(Events::ShopSelection(entityID));
                        }
                        this->inputContextManager.switchContext(InputContextTypes::ENTITY_SELECTED_CONTEXT);
                    }
                    else
                    {
                        // We track all selected units -> if they are enemys you can only select
                        // one unit explicitly
                        //--> so check if unitSelectionEvents == 1 to emit an
                        // NotOwnedEntitySelected

                        // notOwnedUnitSelectionEvents++;
                        // lastSelectionID = entityID;
                    }
                }
            }
            break;
            case InternalEventTypes::EntityDeselected:
            {
                const auto event = this->receivedEvents.readNextEvent<Events::EntityDeselected>(ap);
                (void) event;
                // There is no Information contained in this Event
                // Todo: handle deselection events
                debugOutLevel(Debug::DebugLevels::updateLoop + 2, "Deselect Entities: ");
                selectedEntities.clear();
                inputContextManager.switchContext(InputContextTypes::DEFAULT_CONTEXT);
            }
            break;
            default:
            {
                Error::errTerminate("In SelectionSystem::handleReceivedEvents(): unknown "
                                    "internalEventID",
                                    static_cast<size_t>(type));
            }
        }
        numEvents++;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop, "Update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();

    /*
    if (notOwnedUnitSelectionEvents == 1 && selectedEntities.empty())
    {
       debugOutLevel(Debug::DebugLevels::updateLoop + 2,
                      "Select Entity: ",
                      lastSelectionID,
                      " not owned by player");
        selectedEntities.clear();
        eventManager.emit(Events::NotOwnedEntitySelected(lastSelectionID));
    }
    */
}

irr::core::vector3df SelectionSystem::getGraphicCoord3d(const irr::core::position2di& vec, const irr::f32 zValue) const
{
    debugOutLevel(Debug::DebugLevels::updateLoop + 3,
                  "calculating 3d graphic coords for screen coords (",
                  vec.X,
                  vec.Y,
                  ")");
    return irr::core::vector3df(
        static_cast<irr::f32>(vec.X) * 2.0f /
                static_cast<irr::f32>(this->game.getVideoDriver()->getViewPort().getWidth()) -
            1.0f,
        static_cast<irr::f32>(-vec.Y) * 2.0f /
                static_cast<irr::f32>(this->game.getVideoDriver()->getViewPort().getHeight()) +
            1.0f,
        zValue);
}

bool SelectionSystem::handleRightClick(const irr::SEvent& event)
{
    irr::core::line3df ray = game.getSceneManager()->getSceneCollisionManager()->getRayFromScreenCoordinates(
        irr::core::vector2di(event.MouseInput.X, event.MouseInput.Y), game.getSceneManager()->getActiveCamera());
    irr::core::vector3df colpoint;
    irr::core::triangle3df tri3d;
    irr::scene::ISceneNode* node;
    if (!game.getSceneManager()->getSceneCollisionManager()->getCollisionPoint(
            ray, gameMap->getTriangleSelector(), colpoint, tri3d, node))
    {
        return false;
    }
    if (node)
    {
        // node->setMaterialFlag(irr::video::EMF_WIREFRAME, true);

        // FOR DEBUGGING PATH FINDING
        debugOutLevel(Debug::DebugLevels::updateLoop + 2,
                      selectedEntities.size(),
                      " Entities sent to: ",
                      colpoint.X,
                      " ",
                      colpoint.Y,
                      " ",
                      colpoint.Z);
        for (auto entity : selectedEntities)
        {
            if (this->entityManager.hasComponent<Components::Movement>(entity))
            {
                Handles::Handle posHandle = this->entityManager.getComponent<Components::Position>(entity);
                Handles::Handle moveHandle = this->entityManager.getComponent<Components::Movement>(entity);

                const irr::core::vector2df target(colpoint.X, colpoint.Z);
                eventManager.emit(Events::FindPathTo{entity, target});
            }
        }
    }

    return true;
}

void SelectionSystem::handleSelectionDone(const aabbox2di& area)
{
    debugOutLevel(Debug::DebugLevels::onEvent, "in handle selection done");

    this->highlightOrSelectionSquare(true, area);

#if 0
    for (const auto& entry : this->trackedEntities)
    {
        POW2_ASSERT(this->entityManager.hasComponent<Components::Position>(entry.first));
        const auto posH = this->entityManager.getComponent<Components::Position>(entry.first);
        const auto posC = this->handleManager.getAsValue<Components::Position>(posH);
        const auto selectionC = this->handleManager.getAsValue<Components::SelectionID>(entry.second);

        /*
         *      .--------------------------------------------------------.
         *      |                                                        |
         *      |           .--.                                         |
         *      |          ( X  ) <- entity + radius                     |  <- selection square
         *      |           '--'                                         |
         *      |                                                        |
         *      '--------------------------------------------------------'
         */

        if (posC.X + selectionC.radius < defaultInputContext->selectionHandling
                                             .currentSelection.MinEdge.X ||
            posC.X - selectionC.radius > defaultInputContext->selectionHandling
                                             .currentSelection.MaxEdge.X ||
            posC.Y + selectionC.radius < defaultInputContext->selectionHandling
                                             .currentSelection.MinEdge.Y ||
            posC.Y - selectionC.radius > defaultInputContext->selectionHandling
                                             .currentSelection.MaxEdge.Y) {
            // the entity definetely is outside of the box
            continue;
        }
        else
        {
            //TODO://FIXME: dirty hack: any entity which is selected is 100% selected.
            this->eventManager.emit(Events::PartialEntitySelection(entry.first, 1.0f));
        }
    } // for (const auto& entry : this->trackedEntities)
#endif
}

void SelectionSystem::highlightOrSelectionSquare(const bool emitSelectedEvent, const aabbox2di& box) const
{
    irr::video::SColor selectionSquareColor = irr::video::SColor(255, 0, 255, 0);
    if (emitSelectedEvent)
    {
        selectionSquareColor = irr::video::SColor(255, 255, 0, 0);
    }

    // draw the selection square
    this->game.box2Draw_2d.push_back(
        box2Draw_s(irr::core::aabbox3df(this->getGraphicCoord3d(box.MinEdge, 0.0f),
                                        this->getGraphicCoord3d(box.MaxEdge, 1.0f)),
                   selectionSquareColor));

    auto [minXCellID, maxXCellID, minZCellID, maxZCellID] = coveredGridCells(gameMap->getTerrainNode(), grid);

    // If at this point the min cell id is bigger than the max cell id they didn't change from
    // their initial values. This can happen if the whole selection box is outside of the map.
    if (minXCellID > maxXCellID || minZCellID > maxZCellID)
    {
        return;
    }

    // calculate BBoxes in screen coordinates for all entities inside the visible gridCells and
    // check if these BBoxes are inside the selection area
    bool atleastOneEntityPartiallySelected = false;

    for (const auto entry : this->trackedEntities)
    {
        if (!this->entityManager.hasComponent<Components::Position>(entry.first) ||
            !this->entityManager.hasComponent<Components::Graphic>(entry.first))
        {
            Error::errContinue("entity",
                               entry.first,
                               "has a selection component but doesn't have "
                               "a position or graphics component!");
            continue;
        }

        const auto posH = this->entityManager.getComponent<Components::Position>(entry.first);
        const auto entityCellID =
            this->grid.getCellID(this->handleManager.getAsValue<Components::Position>(posH).position);
        const auto gridCell = this->grid.getXZCoordinate(entityCellID);
        if (gridCell.first < minXCellID || gridCell.first > maxXCellID ||
            gridCell.second < minZCellID || gridCell.second > maxZCellID)
        {
            continue;
        }
        const auto graphicH = this->entityManager.getComponent<Components::Graphic>(entry.first);
        // get srceen coordinates of the bbox of that entity
        const auto& g = this->handleManager.getAsRef<Components::Graphic>(graphicH);

        POW2_ASSERT(g.node != nullptr);

        // using only the center of the BBox
        const irr::core::aabbox3df BBox = g.node->getTransformedBoundingBox();
        // draw the bounding box
        auto debugColor = irr::video::SColor(255, 0, 0, 255);
        if (this->entityManager.hasComponent<Components::DebugColor>(entry.first))
        {
            debugColor = this->handleManager
                             .getAsValue<Components::DebugColor>(
                                 this->entityManager.getComponent<Components::DebugColor>(entry.first))
                             .color;
        }
        this->game.box2Draw.push_back(box2Draw_s(BBox, debugColor));

        // const irr::core::vector3df bBoxCenter = BBox.getCenter();
        // const irr::core::vector2di
        // bBoxCenterScreenCoords(game.getSceneManager()->getSceneCollisionManager()->getScreenCoordinatesFrom3DPosition(bBoxCenter,
        // this->game.getCamera()));
        // debugOutLevel(Debug::DebugLevels::updateLoop + 2, "bounding box center of entity",
        // tr.entityID, "(", bBoxCenterScreenCoords.X, bBoxCenterScreenCoords.Y, ")");

        // translate world bounding box to screen coordinates bounding box
        aabbox2di screenCoords(irr::core::vector2di(0, 0)); // the initial value will be overwritten
                                                            // by 'get2dBoxFromWorldBox'

        if (this->get2dBoxFromWorldBox(BBox, screenCoords))
        { // only use the box if at least one point was found which is actually displayed on screen
            // there are 2 ways the boxes could overlap 100%:
            // 1) the selection box is larger than the entity box and the entity box is fully inside
            // the selection box
            // 2) 1) but the entity box is the larger one
            const irr::s32 maxOverlapArea = std::min(screenCoords.getArea(), box.getArea());
            const irr::s32 overlapArea = box.getIntersection(screenCoords).getArea();

            if (emitSelectedEvent && overlapArea > 0)
            {
                atleastOneEntityPartiallySelected = true;
                this->eventManager.emit(Events::PartialEntitySelection(
                    entry.first, static_cast<irr::f32>(overlapArea) / static_cast<irr::f32>(maxOverlapArea)));
            }

            if (static_cast<irr::f32>(overlapArea) / static_cast<irr::f32>(maxOverlapArea) >= 0.25)
            {
                debugOutLevel(Debug::DebugLevels::updateLoop + 2,
                              "selection area was",
                              overlapArea,
                              "/",
                              maxOverlapArea,
                              "highlighting screencord-bbox of entity",
                              entry.first,
                              "world BBox at (",
                              BBox.MinEdge.X,
                              BBox.MinEdge.Y,
                              BBox.MinEdge.Z,
                              "), (",
                              BBox.MaxEdge.X,
                              BBox.MaxEdge.Y,
                              BBox.MaxEdge.Z,
                              ")");

                // world coordinates to 2d graphics coordinates
                const irr::core::aabbox3df graphicCoords = this->graphicCoordsBoxFromWorldBox(BBox);

                debugOutLevel(Debug::DebugLevels::updateLoop + 2,
                              "graphic coords of the graphicCoords at (",
                              graphicCoords.MinEdge.X,
                              graphicCoords.MinEdge.Y,
                              graphicCoords.MinEdge.Z,
                              "), (",
                              graphicCoords.MaxEdge.X,
                              graphicCoords.MaxEdge.Y,
                              graphicCoords.MaxEdge.Z,
                              ")");

                // display the selection box
                // this->game.box2Draw_2d.push_back(box2Draw_s(graphicCoords,
                // irr::video::SColor(255, 0, 255, 0)));
            }
        } // if (this->get2dBoxFromWorldBox(BBox, screenCoords))
    }     // for (const auto entry : this->trackedEntities)

    if (!atleastOneEntityPartiallySelected && emitSelectedEvent)
    {
        this->eventManager.emit(Events::EntityDeselected());
    }
}


bool SelectionSystem::get2dBoxFromWorldBox(const irr::core::aabbox3df& worldBox, aabbox2di& out) const
{
    irr::core::array<irr::core::vector3df> edges;
    edges.set_used(8); // reserve space for the 8 edges

    worldBox.getEdges(edges.pointer());

    bool singleCorrectPoint = false;

    // add all other edges as screen coords
    for (irr::u32 i = 0; i < 8; i++)
    {
        const irr::core::position2di screenCoordPoint =
            game.getSceneManager()->getSceneCollisionManager()->getScreenCoordinatesFrom3DPosition(
                edges[i], this->game.getCamera());
        // if the z coordinate of the transformed point is < 0 (behind the camera) irrlicht will
        // return (-10000, -10000)
        // ignore those points
        if (screenCoordPoint != irr::core::vector2di(-10000, -10000))
        {
            if (not singleCorrectPoint)
            {
                debugOutLevel(Debug::DebugLevels::updateLoop + 2,
                              "initializing screenCoords box at screen coords: (",
                              screenCoordPoint.X,
                              screenCoordPoint.Y,
                              ") from 3d coords (",
                              edges[i].X,
                              edges[i].Y,
                              edges[i].Z,
                              ")");
                // make sure the box is at least 1 pixel big (because we might divide by it's area)
                out.reset(screenCoordPoint, screenCoordPoint + irr::core::vector2di(1, 1));
                singleCorrectPoint = true;
            }
            else
            {
                debugOutLevel(Debug::DebugLevels::updateLoop + 2,
                              "adding to screenCoords box at screen coords: (",
                              screenCoordPoint.X,
                              screenCoordPoint.Y,
                              ") from 3d coords (",
                              edges[i].X,
                              edges[i].Y,
                              edges[i].Z,
                              ")");
                out.addInternalPoint(screenCoordPoint);
            }
        }
    }


    return singleCorrectPoint;
}


irr::core::aabbox3df SelectionSystem::graphicCoordsBoxFromWorldBox(const irr::core::aabbox3df& box) const
{
    irr::core::array<irr::core::vector3df> edges;
    edges.set_used(8); // reserve space for the 8 edges

    box.getEdges(edges.pointer());

    // graphic coordinates: (0,0) in the middle, +- 1.0 in X and +- 1.0 in Y, Z totally unimportant

    // return value initialized with first edge at depth 1.0f to make sure the box is displayed (it
    // won't if all points are on the z = 0 plane)
    irr::core::aabbox3df graphicCoords(
        this->getGraphicCoord3d(game.getSceneManager()->getSceneCollisionManager()->getScreenCoordinatesFrom3DPosition(
                                    edges[0], this->game.getCamera()),
                                1.0f));

    // add all other edges as graphic coords
    for (irr::u32 i = 1; i < edges.size(); i++)
    {
        const irr::core::position2di screenCoordPoint =
            game.getSceneManager()->getSceneCollisionManager()->getScreenCoordinatesFrom3DPosition(
                edges[i], this->game.getCamera());
        const irr::core::vector3df graphicCoord = this->getGraphicCoord3d(screenCoordPoint, 0.0f);
        debugOutLevel(Debug::DebugLevels::updateLoop + 2,
                      "adding edge at: (",
                      graphicCoord.X,
                      graphicCoord.Y,
                      graphicCoord.Z,
                      ")");
        graphicCoords.addInternalPoint(graphicCoord);
    }

    return graphicCoords;
}
