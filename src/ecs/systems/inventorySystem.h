/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INVENTORY_SYSTEM_H
#define INVENTORY_SYSTEM_H

#include <unordered_map>

#include "systemBase.h"

#include "../entityBlueprint.h"


// forward declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}

class InventorySystem : public SystemBase
{
   public:
    InventorySystem(ThreadPool& threadPool_,
                    EventManager& eventManager_,
                    EntityManager& entityManager_,
                    Handles::HandleManager& handleManager_,
                    std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection_);

    InventorySystem(const InventorySystem&) = delete;
    InventorySystem operator=(const InventorySystem&) = delete;

    virtual ~InventorySystem(){};

    virtual void trackEntity(const entityID_t entityID,
                             const componentID_t internalCompomentID,
                             const Handles::Handle handle);

    virtual void untrackEntity(const entityID_t entityID, const componentID_t internalCompomentID);

    virtual void update(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

   private:
    struct TrackedEntityHandleCollection
    {
        Handles::Handle inventoryH = Handles::InvalidHandle;
    };
    std::unordered_map<entityID_t, TrackedEntityHandleCollection> trackedEntities = {};
    std::vector<entityID_t> trackedDroppedItemChest = {};

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;

    enum InternalEventTypes : Events::EventID_t
    {
        AddItemToInventory = 0,
        RemoveItemFromInventory,
        DropItemFromInventoryToTheMap,
        PickUpDroppedItem,
        NumInternalEvents,
    };

    void handleReceivedEvents();
    void handleAddItemToInventory(const entityID_t itemOwnerEntityID, const entityID_t itemEntityID);
    void handleRemoveItemFromInventory(const entityID_t itemOwnerEntityID, const entityID_t itemEntityID);
    void handleDropItemFromInventoryToTheMap(const entityID_t itemOwnerEntityID,
                                             const entityID_t itemEntityID,
                                             const irr::core::vector2df chestPosition);
    void handlePickUpDroppedItem(const entityID_t droppedItemChestEntityID);
    void handleSellItem(const entityID_t selledItemEntityID);

    std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection;
};

#endif // INVENTORY_SYSTEM_H
