/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENTITY_MANAGER_H
#define ENTITY_MANAGER_H

#include <deque>
#include <memory> // unique_ptr
#include <string>
#include <vector>

#include "componentFlags.h"
#include "entity.h" // entityID_t, componentID_t
#include "../network/globaldefine.h"
#include "../utils/Handle.h"

// forward declarations
class SystemBase;
class IComponentValueStorage;
class ScriptEngine;

class EntityManager
{
   public:
    struct SingleComponent
    {
        SingleComponent()
            : boundSystems()
            , valueStore(nullptr){};

        /// @brief list of all systems bound to this component
        std::vector<SystemBase*> boundSystems;
        /// @brief storage of the values of the components and the mapping between entityIDs <->
        /// componentValues <-> Handles
        IComponentValueStorage* valueStore;

        SingleComponent(const SingleComponent& other) = default;
        SingleComponent& operator=(const SingleComponent& other) = default;
    };

    EntityManager();

    ~EntityManager();

    /// @brief define a new component and returns it's id
    /// @param valueStore the storage location for the values of this component. The storage
    /// location is used when trying to get Handles for component Values of specific entities.
    /// Ownership of the pointer will be transferred to the entityManager!
    /// @return id of the newly defined component
    componentID_t defineComponent(std::unique_ptr<IComponentValueStorage> valueStore);

    /// @brief get all IDs for components which where previously defined (meaning all IDs for which
    /// values of specific components can be stored)
    /// @return
    std::vector<componentID_t> getDefinedComponentIDs() const;

    /// @brief bind a
    /// @param system
    /// Ownership of the pointer will be transferred to the entityManager!
    /// The order of 'bindSystem' calls determines the order
    /// in which 'update' will be called for the system.
    /// The first system to get bound will also be the first system to get updated.
    void bindSystem(std::unique_ptr<SystemBase>& system);

    /// @brief calls update() for all bound systems.
    /// @param tickNumber the current tick
    /// @param scriptEngine
    /// @param deltaTime
    /// The order of 'bindSystem' calls determines the order
    /// in which 'update' will be called for the system.
    /// The first system to get bound will also be the first system to get updated.
    void updateSystems(const uint32_t tickNumber, ScriptEngine& scriptEngine, const float deltaTime);

    /// @return return all systems which where bound by 'bindSystem'
    std::vector<SystemBase*> getBoundSystems() const;

    componentID_t getComponentIDFromName(const std::string& componentName) const;
    std::string getComponentNameFromID(const componentID_t componentID) const;

    /// @brief get the highest used entityID.
    /// ATTENTION: some entities with lower id's might not be used!
    /// @return the highest active entityID. INVALID_ENTITY if no entity is active.
    entityID_t getMaximumEntityID() const;

    /// @brief check if an entity exists (it might not have any componnets!)
    /// @param entityID
    bool entityExists(const entityID_t entityID) const;

    /// @brief read weather this entity was created on the server or the client. (as set when
    /// calling addEntity).
    /// @param entityID
    /// @return argument 'createdByServer' of 'addEntity' when this entity was created.
    bool createdByServer(const entityID_t entityID) const;

    /// @brief create a new entity (without any components)
    /// @param createdByServer signals if this entity was created on the server or the client.
    /// Defaults to true on server, false on client.
    /// @return entityID of the new entity
    entityID_t addEntity(const bool createdByServer = GlobalBools::systemIsServer);

    /// @brief like addEntity() but forces which entityID to use
    /// @param toUse entityID of the newly added entity
    /// @param createdByServer signals if this entity was created on the server or the client.
    /// Defaults to true on server, false on client.
    void addEntity(const entityID_t toUse, const bool createdByServer = GlobalBools::systemIsServer);

    std::vector<entityID_t> entitiesWithComponent(const componentID_t id) const;

    void deleteEntity(const entityID_t entityID);

    /// @brief copy all components and their values from 'entity' to a newly created entity.
    /// All systems are notified as if the components were added to the entity manually or via a
    /// blueprint.
    /// @param entityID the entity to copy components from. MUST be an existing entity. If the old
    /// entity doesn't have any components the new one won't either.
    /// @return entityID of the newly created copy of the entity
    entityID_t copyEntity(const entityID_t entityID);

    /// @brief adds the component 'componentID' to entity 'entityID'
    /// and informs any systems bound to that component
    /// to start tracking the entity
    /// @param entityID
    /// @param componentID
    /// @return a handle pointing to the memory address of the component value
    Handles::Handle addComponent(const entityID_t entityID, const componentID_t componentID);

    /// @brief like addComponent() but infers the componentID based on 'ComponentType'.
    /// @tparam ComponentType
    template <typename ComponentType>
    Handles::Handle addComponent(const entityID_t entityID)
    {
        constexpr auto cid = Components::ComponentID<ComponentType>::CID;
        return this->addComponent(entityID, cid);
    }

    /// @brief adds the component 'componentID' to entity 'entityID' and
    /// (before any bound system is informed of the entity)
    /// initializes the component by copying the data from 'copyFromHandle'
    /// @param entityID
    /// @param componentID
    /// @param copyFromHandle Handle to data the component should be initialized with.
    /// MUST be a handle to the data of the same componentID.
    /// @return a handle to the newly created component for the entity
    Handles::Handle addInitializedComponent(const entityID_t entityID,
                                            const componentID_t componentID,
                                            const Handles::Handle copyFromHandle);

    /// @brief like addInitializedComponent() but infers the componentID based on 'ComponentType'
    /// @tparam ComponentType
    template <typename ComponentType>
    Handles::Handle addInitializedComponent(const entityID_t entityID, const Handles::Handle copyFromHandle)
    {
        constexpr auto cid = Components::ComponentID<ComponentType>::CID;
        return this->addInitializedComponent(entityID, cid, copyFromHandle);
    }

    /// @brief: adds the component by copying data from 'initialData'. See addInitializedComponent()
    /// with a handle for reference.
    /// @param initialData MUST be either nullptr or be a valid pointer to the component type with
    /// id 'componentID'
    Handles::Handle addInitializedComponent(const entityID_t entityID,
                                            const componentID_t componentID,
                                            const void* const initialData = nullptr);

    /// @brief like addInitializedComponent() but infers the componentID based on 'ComponentType'
    /// @tparam ComponentType
    template <typename ComponentType>
    Handles::Handle addInitializedComponent(const entityID_t entityID, const void* const initialData = nullptr)
    {
        constexpr auto cid = Components::ComponentID<ComponentType>::CID;
        return this->addInitializedComponent(entityID, cid, initialData);
    }

    void copyComponent(const Handles::Handle handleBase, const Handles::Handle handleToCopy, const componentID_t componentID);

    void removeComponent(const entityID_t entityID, const componentID_t componentID);

    /// @brief like removeComponent() but infers the componentID based on 'ComponentType'
    /// @tparam ComponentType
    template <typename ComponentType>
    void removeComponent(const entityID_t entityID)
    {
        constexpr auto cid = Components::ComponentID<ComponentType>::CID;
        this->removeComponent(entityID, cid);
    }

    /// @brief creates storage for this component which isn't connected to any entity.
    /// WARNING: if this storage isn't needed any longer destroyFreeValue() should be called!
    /// Otherwise the memory will stay alloated until the destruction of the value storage.
    /// @param componentID id of the component.
    /// @return a handle to the free value.
    Handles::Handle createFreeValue(const componentID_t componentID);

    /// @brief destroy a value of the component which was created by 'createFreeValue()'
    /// and which isn't connected to a particular entity.
    /// @param handle
    void destroyFreeValue(const componentID_t componentID, const Handles::Handle handle);

    /// @brief (slow lookup) to get a handle to the specific component. best to store this handle
    /// inside the system using the component for faster access
    /// @param entityID
    /// @param componentID
    /// @return handle which can be translated into memory pointer via the HandleManager.
    /// The handle itself might be Handles::InvalidHandle
    /// Or the HandleManager might determine that the handle is invalid. Handle with care ;-)
    Handles::Handle getComponent(const entityID_t entityID, const componentID_t componentID) const;

    /// @brief like getComponent() but infers the componentID based on 'ComponentType'
    /// @tparam ComponentType
    template <typename ComponentType>
    Handles::Handle getComponent(const entityID_t entityID) const
    {
        constexpr auto cid = Components::ComponentID<ComponentType>::CID;
        return this->getComponent(entityID, cid);
    }

    bool hasComponent(const entityID_t entityID, const componentID_t component) const;

    template <class... ComponentTypes>
    bool hasComponent(const entityID_t entityID) const
    {
        return Components::hasComponent<ComponentTypes...>(this->entities[entityID].components);
    }

    void printEntityComponents(const entityID_t entityID);

   private:
    std::vector<SingleComponent> components;
    std::vector<SystemBase*> systemsToUpdate = {};

    struct Entity
    {
        Components::ComponentFlag_t components = Components::ComponentFlag_t();
        bool isActive = false;
        bool createdByServer = GlobalBools::systemIsServer;
    };
    // TODO: make sure this is bitsquased to make the most
    // out of the storage

    std::vector<Entity> entities = {};

    std::deque<entityID_t> nextEntityIDToUse = {};
};

#endif // ENTITY_MANAGER_H
