/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz, Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/// This file define all components used in the EntityComponentSystem
/// After adding a new component make sure to also add it to the
/// ecs/componentID.h!


/// REMEMBER! WHEN CHANGING A COMPONENT MAKE SURE TO UPDATE SAVING/LOADING BY CHANGING THE VERSION
/// IN componentSerialization.h AND PROVIDE A WAY TO UPDATE OLD SAVED DATA TO THE NEW VERSION!!!


#pragma once

#ifndef COMPONENTS_H
#define COMPONENTS_H value

#include <cstdint> // (u)intX_t
#include <queue>
#include <vector>

#include <irrlicht/SColor.h>
#include <irrlicht/irrString.h>
#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>
#include "entity.h" // defines componentID_t, blueprintID_t, entityID_t

// forward declarations
namespace irr
{
    namespace scene
    {
        class IAnimatedMeshSceneNode;
        class IAnimatedMesh;
    } // namespace scene
    namespace video
    {
        class ITexture;
    }
} // namespace irr
namespace Handles
{
    class HandleManager;
}

typedef int flag_t;
enum struct UnitTypeE
{
    Invalid = 0,

    Ground = 1,
    Air = 2,

    Tank = 4,
    Creep = 8,
    Building = 16,
};

/// REMEMBER! WHEN CHANGING A COMPONENT MAKE SURE TO UPDATE SAVING/LOADING BY CHANGING THE VERSION
/// IN componentSerialization.h AND PROVIDE A WAY TO UPDATE OLD SAVED DATA TO THE NEW VERSION!!!
namespace Components
{
    struct DebugColor
    {
        irr::video::SColor color = irr::video::SColor(0, 0, 0, 0);
    };

    struct Position
    {
        irr::core::vector2df position = irr::core::vector2df(0.0f, 0.0f);
    };

    struct Movement
    {
        enum MovementStatus
        {
            DirectMovementToTarget,
            StandingStill,
            PathAroundObstacle
        };

        irr::f32 speed = 0.0f;
        std::deque<irr::core::vector2df> waypoints = {};
        irr::f32 expectedDistanceSQToWaypoint = 0.0f;
        MovementStatus movementStatus = MovementStatus::StandingStill;

        irr::core::vector2df velocity = {0.0f, 0.0f};
        irr::core::vector2df newVelocity = {0.0f, 0.0f};

        /// @brief The minimal amount of time for which this
        ///        agent's velocities that are computed by the
        ///        simulation are safe with respect to other
        ///        agents. The larger this number, the sooner
        ///        this agent will respond to the presence of
        ///        other agents, but the less freedom this
        ///        agent has in choosing its velocities.
        ///        MUST be positive.
        irr::f32 timeHorizon = 0.5f;

        /// @brief Radius of the agent. MUST be non-negative
        irr::f32 radius = 32.0f;

        /// @brief The maximum speed of this agent.
        ///        Must be non-negative.
        irr::f32 maxSpeed = 500.0f;
        /// @brief The maximum rotation speed of this agent.
        ///        Must be non-negative. In degrees/second
        irr::f32 maxRotationSpeed = 90.0f;
    };

    struct Graphic
    {
        irr::scene::IAnimatedMeshSceneNode* node = nullptr;
        irr::scene::IAnimatedMesh* mesh = nullptr;
        irr::video::ITexture* texture = nullptr;
        irr::core::vector3df scale = irr::core::vector3df(1.0f, 1.0f, 1.0f);
        irr::f32 offset = 0.0f;
    };

    struct ProjectileMovement
    {
        entityID_t targetEntity = ECS::INVALID_ENTITY;
        irr::f32 speed = 0.0f;
    };

    struct Rotation
    {
        irr::core::vector3df rotation = irr::core::vector3df(0.0f, 0.0f, 0.0f);
    };

    struct EntityName
    {
        irr::core::stringw entityName = "unknown";
    };

    struct Price
    {
        uint16_t price = 0;
    };

    struct Damage
    {
        irr::f32 damage = 0.0f;
        flag_t attacksUnitTypeFlag = int(UnitTypeE::Invalid);
    };

    struct Range
    {
        // save value as square spare take the square root of comparing values
        uint32_t squaredRange = 0;
    };

    struct Ammo
    {
        uint16_t nrProjectiles = 0;
        blueprintID_t projectilID = 0;
    };

    struct Icon
    {
        irr::video::ITexture* texture = nullptr;
    };

    struct Inventory
    {
        entityID_t inventory[6] = {ECS::INVALID_ENTITY,
                                   ECS::INVALID_ENTITY,
                                   ECS::INVALID_ENTITY,
                                   ECS::INVALID_ENTITY,
                                   ECS::INVALID_ENTITY,
                                   ECS::INVALID_ENTITY};
    };

    struct PurchasableItems
    {
        std::vector<blueprintID_t> purchasableItems = {};
    };

    struct Requirement
    {
        uint16_t requirement = 0;
    };

    struct SelectionID
    {
        // in the gamestate used by the number -> dont move values
        enum struct Team
        {
            Invalid, // replace with Invalid
            Team1,
            Team2
        };

        // in the gamestate used by the number -> dont move values
        enum struct SelectionType
        {
            Invalid,
            Tank1,
            Tank2,
            Tank3,
            Tank4,
            Tank5,
            Tank6,
            Tank7,
            Tank8,
            Tank9,
            Tank10,
            ItemShop,
            TankShop
        };

        Team team = Team::Invalid;
        SelectionType selectionType = SelectionType::Invalid;
    };

    struct Cooldown
    {
        bool active = true;
        irr::f32 left = 0.0f;
        irr::f32 initial = 0.0f;
    };

    struct Physical
    {
        enum ArmorType
        {
            Normal = 0,
            Count
        };

        float maxHealth = 0; // maybe as int
        float health = 0;    // maybe as int
        float armor = 0;     // maybe as int
        ArmorType armorType = ArmorType::Normal;

        flag_t unitTypeFlag = int(UnitTypeE::Invalid);
    };

    struct OwnerEntityID
    {
        entityID_t ownerEntityID = ECS::INVALID_ENTITY;
    };

    struct WeaponEntityID
    {
        entityID_t weaponEntityID = ECS::INVALID_ENTITY;
    };

    struct DroppedItemEntityID
    {
        entityID_t droppedItemEntityID = ECS::INVALID_ENTITY;
    };

    struct PlayerStats
    {
        uint16_t earnedMoney = 0; // Money earned
        uint16_t damageTaken = 0;
        uint16_t damageDealt = 0;
        uint16_t healingTaken = 0;
        uint16_t kills = 0; // Takedowns
        uint16_t deaths = 0;
        uint16_t assists = 0;
        uint16_t largestKillingSpree = 0;
        uint16_t largestMultiKill = 0;
    };

    struct PlayerBase
    {
        uint16_t money = 3000; // start money should be 3000
        uint16_t experience = 0;

        entityID_t tankEntityID = ECS::INVALID_ENTITY;
    };

    /// @brief determines how often the components for an entity get send over the network
    struct NetworkPriority
    {
        /// @brief priorities modeled after the RakNet PacketPriority enum
        enum class BasePriority
        {
            Low = 0,
            Medium,
            High,
            NumPriorities
        } priority = BasePriority::Medium;
    };

} // namespace Components

#endif /* ifndef COMPONENTS_H */
