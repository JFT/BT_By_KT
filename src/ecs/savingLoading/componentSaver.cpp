/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "componentSaver.h"
#include <irrlicht/IMeshCache.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/ITexture.h>
#include <irrlicht/IVideoDriver.h>
#include "../../utils/platform_independent/pathOperations.h"

GraphicComponentSaver::GraphicComponentSaver(irr::scene::ISceneManager* const sceneManager_)
    : IComponentSaver(*new GraphicComponentSerializer())
    , sceneManager(sceneManager_)
{
}


void GraphicComponentSaver::save(std::vector<Buffer_t>& buffer,
                                 const EntityManager& etm,
                                 const Handles::HandleManager& hm) const
{
    using GC = Components::Graphic;

    Serialization::serialize<Serialization::SerializationVersion_t>(buffer, this->serializer.getSerializationVersion());

    const entityID_t maxActiveEntityID = etm.getMaximumEntityID();
    if (maxActiveEntityID == ECS::INVALID_ENTITY)
    {
        Serialization::serialize<entityID_t>(buffer, 0);
        return;
    }

    std::vector<entityID_t> withComponent;
    // count number of entities which have the component first
    for (entityID_t entityID = 0; entityID <= maxActiveEntityID; ++entityID)
    {
        if (etm.hasComponent<GC>(entityID))
        {
            POW2_ASSERT(etm.entityExists(entityID));
            withComponent.push_back(entityID);
        }
    }

    // abusing the entityID writing routine to write the number of entities with this component
    // works because std::numeric_limits<entityID_t>::max() is ECS::INVALID_ENTITY which can't
    // be used as a specific entityID but can be used as a number of entities
    Serialization::serialize<entityID_t>(buffer, withComponent.size());

    // generate mapping between saved ids and model/texture strings
    // TODO: maybe implement generic method to serialize std::unordered_map?
    std::unordered_map<irr::video::ITexture*, int32_t> textureIDs;
    std::unordered_map<irr::scene::IAnimatedMesh*, int32_t> meshIDs;
    for (const auto& entity : withComponent)
    {
        const auto& value = hm.getAsRef<GC>(etm.getComponent<GC>(entity));

        // using the irrlicht mesh indices
        const auto meshIndex = sceneManager->getMeshCache()->getMeshIndex(value.mesh);
        POW2_ASSERT(meshIndex != -1);
        meshIDs.insert(std::make_pair(value.mesh, meshIndex));

        // but our own texture indices
        if (value.texture)
        {
            const int32_t newIndex = static_cast<int32_t>(textureIDs.size());
            textureIDs.insert(std::make_pair(value.texture, newIndex));
        }
    }

    Serialization::serialize<uint32_t>(buffer, static_cast<uint32_t>(meshIDs.size()));
    for (const auto& entry : meshIDs)
    {
        static_cast<GraphicComponentSerializer&>(this->serializer)
            .addMeshToIDMapping(entry.first, entry.second);
        Serialization::serialize<int32_t>(buffer, static_cast<int32_t>(entry.second));
        const auto name = sceneManager->getMeshCache()->getMeshName(static_cast<irr::u32>(entry.second));
        const auto relative =
            PlatformIndependent::getRelativePathWithRoot(std::string(name.getPath().c_str()), "media/");
        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                      "serializing mesh path",
                      relative,
                      "with id",
                      entry.second);
        Serialization::serialize<std::string>(buffer, relative);
    }

    Serialization::serialize<uint32_t>(buffer, static_cast<uint32_t>(textureIDs.size()));
    for (const auto& entry : textureIDs)
    {
        static_cast<GraphicComponentSerializer&>(this->serializer)
            .addTextureToIDMapping(entry.first, entry.second);
        Serialization::serialize<int32_t>(buffer, entry.second);
        const auto relative = PlatformIndependent::getRelativePathWithRoot(
            std::string(entry.first->getName().getPath().c_str()), "/media/");
        debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                      "serializing texture path",
                      relative,
                      "with id",
                      entry.second);
        Serialization::serialize<std::string>(buffer, relative);
    }

    for (const auto& entity : withComponent)
    {
        Serialization::serialize<entityID_t>(buffer, entity);
        static_cast<GraphicComponentSerializer&>(this->serializer).serializeForEntity(buffer, entity, etm, hm);
    }
}

void GraphicComponentSaver::load(std::vector<Buffer_t>& buffer, size_t& pos, EntityManager& etm, Handles::HandleManager& hm) const
{
    const auto savedVersion = Serialization::deserialize<Serialization::SerializationVersion_t>(buffer, pos);

    if (savedVersion != this->serializer.getSerializationVersion())
    {
        throw std::invalid_argument(
            "GraphicComponentSaver: no known way to deserialize saved version " + std::to_string(savedVersion) +
            " with serializer version " + std::to_string(this->serializer.getSerializationVersion()) +
            " did someone forget to write an update routine?");
    }

    const entityID_t numEntities = Serialization::deserialize<entityID_t>(buffer, pos);
    debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                  "deserialized numEntities",
                  numEntities,
                  "from pos",
                  pos,
                  "out of buffer size",
                  buffer.size());

    const auto numMeshMapEntries = Serialization::deserialize<uint32_t>(buffer, pos);
    for (uint32_t i = 0; i < numMeshMapEntries; ++i)
    {
        const auto index = Serialization::deserialize<int32_t>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "deseralized mesh index", index);
        const auto name = Serialization::deserialize<std::string>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "deseralized mesh name", name, "... getting mesh...");
        const auto mesh = sceneManager->getMesh(name.c_str());
        if (mesh == nullptr)
        {
            Error::errContinue("savegame: couldn't load mesh", name);
        }
        static_cast<GraphicComponentSerializer&>(this->serializer).addMeshToIDMapping(mesh, index);
    }

    const auto numTextureIDEntries = Serialization::deserialize<uint32_t>(buffer, pos);
    for (uint32_t i = 0; i < numTextureIDEntries; ++i)
    {
        const auto ID = Serialization::deserialize<int32_t>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "deseralized texture ID", ID);
        const auto name = Serialization::deserialize<std::string>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "deseralized texture name", name, "... getting texture...");
        const auto texture = sceneManager->getVideoDriver()->getTexture(name.c_str());
        if (texture == nullptr)
        {
            Error::errContinue("savegame: couldn't load texture", name);
        }
        static_cast<GraphicComponentSerializer&>(this->serializer).addTextureToIDMapping(texture, ID);
    }

    for (entityID_t i = 0; i < numEntities; ++i)
    {
        const entityID_t entityID = Serialization::deserialize<entityID_t>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::updateLoop + 2, "deserialized entityID", entityID, "from pos", pos);
        if (entityID == ECS::INVALID_ENTITY)
        {
            throw std::invalid_argument(
                "tried to read the " + std::to_string(i) + "-th entity of " + std::to_string(numEntities) +
                " entities from index " + std::to_string(pos) + " inside a buffer size " +
                std::to_string(buffer.size()) + " but read ECS::INVALID_ENTITY or pos is after buffer!");
        }

        this->serializer.deserializeForEntity(buffer, pos, entityID, etm, hm);

    } // for (entityID_t i = 0; i < numEntities; ++i)
}


IconComponentSaver::IconComponentSaver(irr::scene::ISceneManager* const sceneManager_)
    : IComponentSaver(*new IconComponentSerializer())
    , sceneManager(sceneManager_)
{
}


void IconComponentSaver::save(std::vector<Buffer_t>& buffer,
                              const EntityManager& etm,
                              const Handles::HandleManager& hm) const
{
    using IC = Components::Icon;

    Serialization::serialize<Serialization::SerializationVersion_t>(buffer, this->serializer.getSerializationVersion());

    const entityID_t maxActiveEntityID = etm.getMaximumEntityID();
    if (maxActiveEntityID == ECS::INVALID_ENTITY)
    {
        Serialization::serialize<entityID_t>(buffer, 0);
        return;
    }

    std::vector<entityID_t> withComponent;
    // count number of entities which have the component first
    for (entityID_t entityID = 0; entityID <= maxActiveEntityID; ++entityID)
    {
        if (etm.hasComponent<IC>(entityID))
        {
            POW2_ASSERT(etm.entityExists(entityID));
            withComponent.push_back(entityID);
        }
    }

    // abusing the entityID writing routine to write the number of entities with this component
    // works because std::numeric_limits<entityID_t>::max() is ECS::INVALID_ENTITY which can't
    // be used as a specific entityID but can be used as a number of entities
    Serialization::serialize<entityID_t>(buffer, withComponent.size());

    // generate mapping between saved ids and model/texture strings
    // TODO: maybe implement generic method to serialize std::unordered_map?
    std::unordered_map<irr::video::ITexture*, int32_t> textureIDs;
    for (const auto& entity : withComponent)
    {
        const auto& value = hm.getAsRef<IC>(etm.getComponent<IC>(entity));
        // using our our own texture indices
        if (value.texture)
        {
            const int32_t newIndex = static_cast<int32_t>(textureIDs.size());
            textureIDs.insert(std::make_pair(value.texture, newIndex));
        }
    }

    Serialization::serialize<uint32_t>(buffer, static_cast<uint32_t>(textureIDs.size()));
    for (const auto& entry : textureIDs)
    {
        static_cast<IconComponentSerializer&>(this->serializer)
            .addTextureToIDMapping(entry.first, entry.second);
        Serialization::serialize<int32_t>(buffer, entry.second);
        const auto relative = PlatformIndependent::getRelativePathWithRoot(
            std::string(entry.first->getName().getPath().c_str()), "media/");
        Serialization::serialize<std::string>(buffer, relative);
    }

    for (const auto& entity : withComponent)
    {
        Serialization::serialize<entityID_t>(buffer, entity);
        static_cast<IconComponentSerializer&>(this->serializer).serializeForEntity(buffer, entity, etm, hm);
    }
}

void IconComponentSaver::load(std::vector<Buffer_t>& buffer, size_t& pos, EntityManager& etm, Handles::HandleManager& hm) const
{
    const auto savedVersion = Serialization::deserialize<Serialization::SerializationVersion_t>(buffer, pos);

    if (savedVersion != this->serializer.getSerializationVersion())
    {
        throw std::invalid_argument(
            "IconComponentSaver: no known way to deserialize saved version " + std::to_string(savedVersion) +
            " with serializer version " + std::to_string(this->serializer.getSerializationVersion()) +
            " did someone forget to write an update routine?");
    }

    const entityID_t numEntities = Serialization::deserialize<entityID_t>(buffer, pos);
    debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                  "deserialized numEntities",
                  numEntities,
                  "from pos",
                  pos,
                  "out of buffer size",
                  buffer.size());

    const auto numTextureIDEntries = Serialization::deserialize<uint32_t>(buffer, pos);
    for (uint32_t i = 0; i < numTextureIDEntries; ++i)
    {
        const auto ID = Serialization::deserialize<int32_t>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "deseralized texture ID", ID);
        const auto name = Serialization::deserialize<std::string>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "deseralized texture name", name, "... getting texture...");
        const auto texture = sceneManager->getVideoDriver()->getTexture(name.c_str());
        std::cerr << "TEX=" << static_cast<void*>(texture) << std::endl;
        static_cast<IconComponentSerializer&>(this->serializer).addTextureToIDMapping(texture, ID);
    }

    for (entityID_t i = 0; i < numEntities; ++i)
    {
        const entityID_t entityID = Serialization::deserialize<entityID_t>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::updateLoop + 2, "deserialized entityID", entityID, "from pos", pos);
        if (entityID == ECS::INVALID_ENTITY)
        {
            throw std::invalid_argument(
                "tried to read the " + std::to_string(i) + "-th entity of " + std::to_string(numEntities) +
                " entities from index " + std::to_string(pos) + " inside a buffer size " +
                std::to_string(buffer.size()) + " but read ECS::INVALID_ENTITY or pos is after buffer!");
        }

        this->serializer.deserializeForEntity(buffer, pos, entityID, etm, hm);

    } // for (entityID_t i = 0; i < numEntities; ++i)
}
