/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "systemSaver.h"
#include <numeric> // std::accumulate
#include "../events/eventMailbox.h"
#include "../systems/systemBase.h"
#include "../../utils/debug.h"
#include "../../utils/serialization.h"


constexpr Serialization::SerializationVersion_t TrivialSystemSaver::version;

void TrivialSystemSaver::save(std::vector<Buffer_t>& buffer) const
{
    static_assert(sizeof(Buffer_t) == sizeof(decltype(this->system.receivedEvents.storedEvents)::value_type::value_type),
                  "type events are stored inside the mailBox isn't the same as the buffer trying "
                  "to serialize into!");
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "serializing version",
                  static_cast<size_t>(this->version),
                  "to buffer pos",
                  buffer.size());
    Serialization::serialize<Serialization::SerializationVersion_t>(buffer, this->version);

    auto& mailBox = this->system.receivedEvents;
    const uint32_t eventDataSize =
        std::accumulate(mailBox.storedEvents.begin(),
                        mailBox.storedEvents.end(),
                        static_cast<uint32_t>(0),
                        [](const uint32_t init, const decltype(mailBox.storedEvents)::value_type& a) {
                            return init + a.size();
                        });
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "serializing eventDataSize",
                  eventDataSize,
                  "to buffer pos",
                  buffer.size());
    Serialization::serialize<uint32_t>(buffer, eventDataSize);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "serializing event data at buffer size",
                  buffer.size());
    for (const auto& vec : mailBox.storedEvents)
    {
        std::copy(vec.begin(), vec.end(), std::back_inserter(buffer));
    }
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "ended event data serialization at buffer size",
                  buffer.size());
}

void TrivialSystemSaver::load(const std::vector<Buffer_t>& buffer, size_t& pos)
{
    static_assert(sizeof(Buffer_t) == sizeof(decltype(this->system.receivedEvents.storedEvents)::value_type::value_type),
                  "type events are stored inside the mailBox isn't the same as the buffer trying "
                  "to serialize into!");

    for (const auto& vec : this->system.receivedEvents.storedEvents)
    {
        if (vec.size() != 0)
        {
            throw std::invalid_argument("supplied system already has stored some events!");
        }
    }

    const auto savedVersion = Serialization::deserialize<Serialization::SerializationVersion_t>(buffer, pos);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "deserialized version",
                  static_cast<size_t>(savedVersion),
                  "now at position",
                  pos);
    if (savedVersion != this->version)
    {
        throw Serialization::deserialization_version_mismatch(this->version, savedVersion);
    }

    const auto eventDataSize = Serialization::deserialize<uint32_t>(buffer, pos);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "deserialized eventDataSize", eventDataSize, "now at position", pos);
    if (pos + eventDataSize > buffer.size())
    {
        throw Serialization::serialization_buffer_overrun(eventDataSize, pos, buffer.size(), "saved event data");
    }

    using dt = std::vector<Buffer_t>::difference_type;
    std::copy(buffer.begin() + static_cast<dt>(pos),
              buffer.begin() + static_cast<dt>(pos) + static_cast<dt>(eventDataSize),
              std::back_inserter(this->system.receivedEvents.storedEvents.at(0)));
    pos += eventDataSize;
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "deserialized events, now at position", pos);
}
