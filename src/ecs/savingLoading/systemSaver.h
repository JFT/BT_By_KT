/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef BT_ECS_SYSTEMSAVER_H
#define BT_ECS_SYSTEMSAVER_H

#include <cstddef> // size_t
#include <vector>
#include "../../utils/serializationTypes.h"

class SystemBase;

class ISystemSaver
{
   public:
    using Buffer_t = Serialization::Buffer_t;

    ISystemSaver(SystemBase& system_)
        : system(system_)
    {
    }

    virtual ~ISystemSaver() {}

    /// @brief saves unhandled events inside the system.
    /// @param buffer the serialized events will be appended to this buffer.
    /// @param system the system to be saved.
    /// Make sure to use the corrrect systemSaver for the system because otherwise
    /// not all or unwanted events will be saved, too.
    virtual void save(std::vector<Buffer_t>& buffer) const = 0;

    /// @brief load unhandled events which were saved using 'save'
    /// @param buffer buffer containing the serialized events
    /// @param pos byte offset into 'buffer' the serialized events for the system begin.
    /// 'pos' will be advanced by 'sizeOfEventDat' after succesfull deserialization.
    /// @param system the system to be saved.
    /// Make sure to use the corrrect systemSaver for the system because otherwise
    /// not all or unwanted events will be saved, too.
    /// This function throws Serialization::serialization_exceptions if the serialization
    /// fails for some reason.
    virtual void load(const std::vector<Buffer_t>& buffer, size_t& pos) = 0;

   protected:
    SystemBase& system;
};

/// @brief Use this class to save events for systems which only use the
/// EventMailbox inside SystemBase and which need to save + load all unhandled events.
class TrivialSystemSaver : public ISystemSaver
{
   public:
    TrivialSystemSaver(SystemBase& system_)
        : ISystemSaver(system_)
    {
    }

    virtual ~TrivialSystemSaver() {}

    virtual void save(std::vector<Buffer_t>& buffer) const;

    virtual void load(const std::vector<Buffer_t>& buffer, size_t& pos);

   private:
    static constexpr Serialization::SerializationVersion_t version = 0;
};

#endif /* ifndef BT_ECS_SYSTEMSAVER_H */
