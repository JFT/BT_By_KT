/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../componentSerializer.h"

namespace irr
{
    namespace scene
    {
        class ISceneManager;
    }
} // namespace irr

/// @brief abstract base class to save/load a single component.
class IComponentSaver
{
   public:
    using Buffer_t = Serialization::Buffer_t;

    explicit IComponentSaver(IComponentSerializerBase& serializer_)
        : serializer(serializer_){};

    virtual ~IComponentSaver(){};

    /// @brief get the componentID of the component which can be saved/loaded using this instance of
    /// the IComponentSaver. See ecs/componentID.h
    /// @return
    virtual componentID_t getComponentID() const { return this->serializer.getComponentID(); }

    /// @brief save all component values of the component for this instance of the IComponentSaver
    /// into 'buffer'
    /// @param buffer component values and extra data will be appended to this buffer.
    /// @param etm
    /// @param hm
    virtual void
    save(std::vector<Buffer_t>& buffer, const EntityManager& etm, const Handles::HandleManager& hm) const = 0;

    /// @brief load previously saved components from 'buffer' beginning at byte offset 'pos'.
    /// @param buffer
    /// @param pos byte offset from which loading the component values should begin.
    /// 'pos' will be advanced by amount of bytes read after succesfull deserialization.
    /// @param etm
    /// @param hm
    virtual void
    load(std::vector<Buffer_t>& buffer, size_t& pos, EntityManager& etm, Handles::HandleManager& hm) const = 0;

   protected:
    IComponentSerializerBase& serializer;
};

template <typename ComponentType>
class TrivialComponentSaver : public IComponentSaver
{
   public:
    TrivialComponentSaver()
        : IComponentSaver(*new TrivialComponentSerializer<ComponentType>())
    {
    }

    virtual ~TrivialComponentSaver() { delete &this->serializer; };

    virtual void save(std::vector<IComponentSaver::Buffer_t>& buffer,
                      const EntityManager& etm,
                      const Handles::HandleManager& hm) const;

    virtual void
    load(std::vector<Buffer_t>& buffer, size_t& pos, EntityManager& etm, Handles::HandleManager& hm) const;
};

#include "componentSaver.tpp"

class GraphicComponentSaver : public IComponentSaver
{
   public:
    explicit GraphicComponentSaver(irr::scene::ISceneManager* const sceneManager_);

    GraphicComponentSaver(const GraphicComponentSaver&) = delete;
    GraphicComponentSaver& operator=(const GraphicComponentSaver&) = delete;

    virtual ~GraphicComponentSaver() { delete &this->serializer; }

    virtual void
    save(std::vector<Buffer_t>& buffer, const EntityManager& etm, const Handles::HandleManager& hm) const;

    virtual void
    load(std::vector<Buffer_t>& buffer, size_t& pos, EntityManager& etm, Handles::HandleManager& hm) const;

   private:
    irr::scene::ISceneManager* const sceneManager;
};

class IconComponentSaver : public IComponentSaver
{
   public:
    explicit IconComponentSaver(irr::scene::ISceneManager* const sceneManager_);

    IconComponentSaver(const IconComponentSaver&) = delete;
    IconComponentSaver& operator=(const IconComponentSaver&) = delete;

    virtual ~IconComponentSaver() { delete &this->serializer; }

    virtual void
    save(std::vector<Buffer_t>& buffer, const EntityManager& etm, const Handles::HandleManager& hm) const;

    virtual void
    load(std::vector<Buffer_t>& buffer, size_t& pos, EntityManager& etm, Handles::HandleManager& hm) const;

   private:
    irr::scene::ISceneManager* const sceneManager;
};
