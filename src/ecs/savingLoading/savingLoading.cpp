/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "savingLoading.h"
#include <fstream>
#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/ISceneManager.h>
#include "componentSaver.h"
#include "systemSaver.h"
#include "../dynamicallySizedComponentSerialization.h"
#include "../entityManager.h"
#include "../systems/automaticWeaponSystem.h"
#include "../systems/cooldownSystem.h"
#include "../systems/damageSystem.h"
#include "../systems/graphicSystem.h"
#include "../systems/guiSystems/healthBarSystem.h"
#include "../systems/inventorySystem.h"
#include "../systems/moneyTransferSystem.h"
#include "../systems/movement/movementSystem.h"
#include "../systems/projectileSystem.h"
#include "../systems/selectionSystem.h"
#include "../systems/shopSystem.h"
#include "../systems/testSystem.h"
#include "../../utils/debug.h"
#include "../../utils/serialization.h"

ErrCode SavingLoading::saveEntitySystem(const std::string& filename,
                                        const EntityManager& entityManager,
                                        const Handles::HandleManager& handleManager,
                                        const SaveLoadDependencies& dependencies)
{
    std::fstream outFile;
    outFile.open(filename, std::ios::out | std::ios::binary);
    if (not outFile.good())
    {
        Error::errContinue("couldn't open file", filename);
        return ErrCodes::GENERIC_ERR;
    }

    // 1st step save all active entities and their 'createdByServer' state because when loading
    // again before deserialization the 'createdByServer' state of all entities should be known.
    entityID_t numActiveEntities = 0;
    if (entityManager.getMaximumEntityID() != ECS::INVALID_ENTITY)
    {
        for (entityID_t entity = 0; entity <= entityManager.getMaximumEntityID(); ++entity)
        {
            if (entityManager.entityExists(entity))
            {
                numActiveEntities++;
            }
        }
    }

    std::vector<Serialization::Buffer_t> buffer;

    Serialization::serialize<entityID_t>(buffer, numActiveEntities);
    debugOutLevel(Debug::DebugLevels::stateInit, "serialized numActiveEntities =", numActiveEntities);

    if (entityManager.getMaximumEntityID() != ECS::INVALID_ENTITY)
    {
        for (entityID_t entity = 0; entity <= entityManager.getMaximumEntityID(); ++entity)
        {
            if (entityManager.entityExists(entity))
            {
                Serialization::serialize<entityID_t>(buffer, entity);
                const bool createdByServer = entityManager.createdByServer(entity);
                buffer.push_back(static_cast<Serialization::Buffer_t>(createdByServer));
                debugOutLevel(Debug::DebugLevels::stateInit, "serialized entity", entity, createdByServer ? "true" : "false");
            }
        }
    }

    const auto componentIDs = entityManager.getDefinedComponentIDs();
    Serialization::serialize<uint32_t>(buffer, static_cast<uint32_t>(componentIDs.size()));
    for (const auto componentID : componentIDs)
    {
        Serialization::serialize<componentID_t>(buffer, componentID);

        const auto sizeAtIndex = buffer.size();
        Serialization::serialize<uint32_t>(buffer, 0);
        const auto sizeBefore = buffer.size();
        debugOutLevel(Debug::DebugLevels::firstOrderLoop, "begin writing at buffersize", buffer.size(), "bytes");

        const auto saver = getComponentSaverFor(componentID, dependencies);
        if (saver)
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                          "saving values for component:",
                          Components::NameFromComponendID[componentID]);
            POW2_ASSERT(componentID == saver->getComponentID());
            saver->save(buffer, entityManager, handleManager);
        }
        else
        {
            Error::errContinue("No saver for component with componentID", static_cast<size_t>(componentID));
        }

        const auto sizeWritten = buffer.size() - sizeBefore;
        Serialization::serializeAt<uint32_t>(buffer, sizeAtIndex, static_cast<uint32_t>(sizeWritten));
        debugOutLevel(
            Debug::DebugLevels::firstOrderLoop, "wrote", sizeWritten, "bytes, new size is", buffer.size(), "bytes");
    }

    Serialization::serialize<uint32_t>(buffer, static_cast<uint32_t>(entityManager.getBoundSystems().size()));
    for (auto* const system : entityManager.getBoundSystems())
    {
        Serialization::serialize<uint32_t>(buffer, static_cast<uint32_t>(system->identifier));

        // store the index at which the size is serialized and write the size of the serialized data
        // there after the system has written its data.
        const auto sizeAtIndex = buffer.size();
        Serialization::serialize<uint32_t>(buffer, 0);
        const auto sizeBefore = buffer.size();

        auto saver = getSystemSaverFor(*system);
        if (saver)
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                          "saving data for system with identifier",
                          static_cast<size_t>(system->identifier));
            saver->save(buffer);
        }
        else
        {
            Error::errContinue("No saver for system with identifier", static_cast<size_t>(system->identifier), "supplied!");
        }

        const auto sizeWritten = buffer.size() - sizeBefore;
        Serialization::serializeAt<uint32_t>(buffer, sizeAtIndex, static_cast<uint32_t>(sizeWritten));
    }

    if (dependencies.sceneManager != nullptr)
    {
        Serialization::serialize<bool>(buffer, true);
        Serialization::serialize(buffer, dependencies.sceneManager->getActiveCamera()->getPosition());
        Serialization::serialize(buffer, dependencies.sceneManager->getActiveCamera()->getRotation());
        Serialization::serialize(buffer, dependencies.sceneManager->getActiveCamera()->getTarget());
    }
    else
    {
        Serialization::serialize<bool>(buffer, false);
    }

    static_assert(sizeof(char) == sizeof(buffer[0]), "Serialization::Buffer_t isn't a byte-sized type!");
    const char* const out = reinterpret_cast<const char* const>(&buffer[0]);
    outFile.write(out, buffer.size());
    outFile.close();

    return ErrCodes::NO_ERR;
}

ErrCode SavingLoading::loadEntitySystem(const std::string& filename,
                                        EntityManager& entityManager,
                                        Handles::HandleManager& handleManager,
                                        const SaveLoadDependencies& dependencies)
{
#ifndef MAKE_DEBUG_
    // try catch block only in release mode
    // (easier to get a backtrace in debug mode this way)
    try
#endif // ifndef MAKE_DEBUG_
    {
        if (entityManager.getMaximumEntityID() != ECS::INVALID_ENTITY)
        {
            Error::errContinue("there are already existing entities! "
                               "(entityManager.getMaximumEntityID() == ",
                               entityManager.getMaximumEntityID(),
                               ")");
            return ErrCodes::GENERIC_ERR;
        }

        std::fstream inFile;
        inFile.open(filename, std::ios::in | std::ios::binary);
        if (not inFile.good())
        {
            Error::errContinue("couldn't open file", filename);
            return ErrCodes::GENERIC_ERR;
        }

        inFile.seekg(0, std::ios_base::end);
        const size_t fileSize = inFile.tellp();
        // MUST return to the beginning of the file before reading!
        inFile.seekg(0, std::ios_base::beg);

        // TODO: make sure file isn't too big
        std::vector<Serialization::Buffer_t> buffer;
        buffer.resize(fileSize);
        char* const in = reinterpret_cast<char* const>(&buffer[0]);
        inFile.read(in, fileSize);

        size_t pos = 0;
        const entityID_t numActiveEntities = Serialization::deserialize<entityID_t>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::stateInit, "deserialized numActiveEntities =", numActiveEntities);

        for (entityID_t i = 0; i < numActiveEntities; ++i)
        {
            const entityID_t entity = Serialization::deserialize<entityID_t>(buffer, pos);
            const bool createdByServer = static_cast<const bool>(buffer[pos]);
            pos++;
            debugOutLevel(Debug::DebugLevels::stateInit, "deserialized entity", entity, createdByServer ? "true" : "false");
            entityManager.addEntity(entity, createdByServer);
        }

        const auto numSavedComponents = Serialization::deserialize<uint32_t>(buffer, pos);

        for (uint32_t i = 0; i < numSavedComponents; ++i)
        {
            const componentID_t component = Serialization::deserialize<componentID_t>(buffer, pos);
            debugOutLevel(Debug::DebugLevels::stateInit + 4, "deserialized componentID", component);

            const auto sizeOfComponentData = Serialization::deserialize<uint32_t>(buffer, pos);
            const auto startPos = pos;
            debugOutLevel(Debug::DebugLevels::stateInit + 4,
                          "deserialized sizeOfComponentData",
                          sizeOfComponentData,
                          "beginning at byte",
                          startPos);

            const auto saver = getComponentSaverFor(component, dependencies);

            if (not saver)
            {
                Error::errContinue("don't know how to load data for component",
                                   component < Components::NumComponents ?
                                       Components::NameFromComponendID[component] :
                                       "ERROR: INVALID COMPONENTID " +
                                           std::to_string(static_cast<size_t>(component)));
                pos += sizeOfComponentData;
                continue;
            }
            debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                          "loading values for",
                          Components::NameFromComponendID[component]);

            saver->load(buffer, pos, entityManager, handleManager);
            if (pos != startPos + sizeOfComponentData)
            {
                throw Serialization::serialization_exception(
                    "deserialization of values for component '" +
                    std::string(Components::NameFromComponendID[component]) + "' ended at byte " +
                    std::to_string(pos) + " but should have been at " +
                    std::to_string(startPos + sizeOfComponentData) + " (" +
                    std::to_string(sizeOfComponentData) + " bytes after " + std::to_string(startPos) + ")!");
            }
        }

        const auto numSavedSystemEvents = Serialization::deserialize<uint32_t>(buffer, pos);

        for (uint32_t i = 0; i < numSavedSystemEvents; ++i)
        {
            const auto systemIdentifier =
                static_cast<Systems::SystemIdentifiers>(Serialization::deserialize<uint32_t>(buffer, pos));
            const auto systems = entityManager.getBoundSystems();
            const auto system =
                std::find_if(systems.begin(), systems.end(), [systemIdentifier](const SystemBase* const a) {
                    return a->identifier == systemIdentifier;
                });
            // SystemBase* system = *system_it;
            const auto sizeOfEventData = Serialization::deserialize<uint32_t>(buffer, pos);
            if (system == systems.end())
            {
                Error::errContinue("didn't find a system with identifier", static_cast<size_t>(systemIdentifier));
                pos += sizeOfEventData;
                continue;
            }
            auto saver = getSystemSaverFor(*(*system));
            if (saver == nullptr)
            {
                Error::errContinue("didn't find a system saver for identifier",
                                   static_cast<size_t>(systemIdentifier));
                pos += sizeOfEventData;
                continue;
            }
            saver->load(buffer, pos);
        }

        const bool savedCameraData = Serialization::deserialize<bool>(buffer, pos);
        if (savedCameraData && dependencies.sceneManager != nullptr)
        {
            dependencies.sceneManager->getActiveCamera()->setPosition(
                Serialization::deserialize<irr::core::vector3df>(buffer, pos));
            dependencies.sceneManager->getActiveCamera()->setRotation(
                Serialization::deserialize<irr::core::vector3df>(buffer, pos));
            dependencies.sceneManager->getActiveCamera()->setTarget(
                Serialization::deserialize<irr::core::vector3df>(buffer, pos));
        }

        POW2_ASSERT(pos == fileSize); // no data left over
        inFile.close();
        return ErrCodes::NO_ERR;
    }
#ifndef MAKE_DEBUG_
    catch (const std::exception& e)
    {
        Error::errContinue("Error:", e.what());
        return ErrCodes::GENERIC_ERR;
    }
#endif // ifndef MAKE_DEBUG_
}

std::unique_ptr<IComponentSaver>
SavingLoading::getComponentSaverFor(const componentID_t component, const SaveLoadDependencies& dependencies)
{
    using namespace Components;

    switch (component)
    {
        case ComponentID<DebugColor>::CID:
            return std::make_unique<TrivialComponentSaver<DebugColor>>();
        case ComponentID<Position>::CID:
            return std::make_unique<TrivialComponentSaver<Position>>();
        case ComponentID<Movement>::CID:
            return std::make_unique<TrivialComponentSaver<Movement>>();
        case ComponentID<Graphic>::CID:
            if (not dependencies.sceneManager)
            {
                return nullptr;
            }
            return std::make_unique<GraphicComponentSaver>(dependencies.sceneManager);
        case ComponentID<ProjectileMovement>::CID:
            return std::make_unique<TrivialComponentSaver<ProjectileMovement>>();
        case ComponentID<Rotation>::CID:
            return std::make_unique<TrivialComponentSaver<Rotation>>();
        case ComponentID<EntityName>::CID:
            return std::make_unique<TrivialComponentSaver<EntityName>>();
        case ComponentID<Price>::CID:
            return std::make_unique<TrivialComponentSaver<Price>>();
        case ComponentID<Damage>::CID:
            return std::make_unique<TrivialComponentSaver<Damage>>();
        case ComponentID<Range>::CID:
            return std::make_unique<TrivialComponentSaver<Range>>();
        case ComponentID<Ammo>::CID:
            return std::make_unique<TrivialComponentSaver<Ammo>>();
        case ComponentID<Icon>::CID:
            return std::make_unique<IconComponentSaver>(dependencies.sceneManager);
        case ComponentID<Inventory>::CID:
            return std::make_unique<TrivialComponentSaver<Inventory>>();
        case ComponentID<PurchasableItems>::CID:
            return std::make_unique<TrivialComponentSaver<PurchasableItems>>();
        case ComponentID<Requirement>::CID:
            return std::make_unique<TrivialComponentSaver<Requirement>>();
        case ComponentID<SelectionID>::CID:
            return std::make_unique<TrivialComponentSaver<SelectionID>>();
        case ComponentID<Cooldown>::CID:
            return std::make_unique<TrivialComponentSaver<Cooldown>>();
        case ComponentID<Physical>::CID:
            return std::make_unique<TrivialComponentSaver<Physical>>();
        case ComponentID<OwnerEntityID>::CID:
            return std::make_unique<TrivialComponentSaver<OwnerEntityID>>();
        case ComponentID<WeaponEntityID>::CID:
            return std::make_unique<TrivialComponentSaver<WeaponEntityID>>();
        case ComponentID<DroppedItemEntityID>::CID:
            return std::make_unique<TrivialComponentSaver<DroppedItemEntityID>>();
        case ComponentID<PlayerStats>::CID:
            return std::make_unique<TrivialComponentSaver<PlayerStats>>();
        case ComponentID<PlayerBase>::CID:
            return std::make_unique<TrivialComponentSaver<PlayerBase>>();
        case ComponentID<NetworkPriority>::CID:
            return std::make_unique<TrivialComponentSaver<NetworkPriority>>();
        default:
            Error::errContinue("don't know any saver for componentID", static_cast<size_t>(component));
    }

    return nullptr;
}

std::unique_ptr<ISystemSaver> SavingLoading::getSystemSaverFor(SystemBase& system)
{
    using ID = Systems::SystemIdentifiers;

    switch (system.identifier)
    {
        case ID::AutomaticWeaponSystem:
        case ID::CooldownSystem:
        case ID::DamageSystem:
        case ID::GraphicSystem:
        case ID::HealthBarSystem:
        case ID::InventoryGuiSystem:
        case ID::ShopGuiSystem:
        case ID::InventorySystem:
        case ID::MoneyTransferSystem:
        case ID::MovementSystem:
        case ID::ProjectileSystem:
        case ID::SelectionSystem:
        case ID::ShopSystem:
        case ID::TestSystem:
            return std::make_unique<TrivialSystemSaver>(system);
        case ID::NetworkComponentSyncronizationServer:
            // not receiving any events to save/load
            return std::unique_ptr<ISystemSaver>(nullptr);
    }

    return std::unique_ptr<ISystemSaver>(nullptr);
}
