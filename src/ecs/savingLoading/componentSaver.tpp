/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

template <typename ComponentType>
void TrivialComponentSaver<ComponentType>::save(std::vector<IComponentSaver::Buffer_t>& buffer,
                                                const EntityManager& etm,
                                                const Handles::HandleManager& hm) const
{
    Serialization::serialize<Serialization::SerializationVersion_t>(buffer, this->serializer.getSerializationVersion());

    const entityID_t maxActiveEntityID = etm.getMaximumEntityID();
    if (maxActiveEntityID == ECS::INVALID_ENTITY)
    {
        Serialization::serialize<entityID_t>(buffer, 0);
        return;
    }

    entityID_t withComponent = 0;
    // count number of entities which have the component first
    for (entityID_t entityID = 0; entityID <= maxActiveEntityID; ++entityID)
    {
        if (etm.hasComponent<ComponentType>(entityID))
        {
            POW2_ASSERT(etm.entityExists(entityID));
            ++withComponent;
        }
    }

    // abusing the entityID writing routine to write the number of entities with this component
    // works because std::numeric_limits<entityID_t>::max() is ECS::INVALID_ENTITY which can't
    // be used as a specific entityID but can be used as a number of entities
    Serialization::serialize<entityID_t>(buffer, withComponent);
    debugOutLevel(Debug::DebugLevels::firstOrderLoop, "serialized numEntities =", withComponent);

    for (entityID_t entityID = 0; entityID <= maxActiveEntityID; ++entityID)
    {
        if (etm.hasComponent<ComponentType>(entityID))
        {
            Serialization::serialize<entityID_t>(buffer, entityID);
            this->serializer.serializeForEntity(buffer, entityID, etm, hm);
        }
    }
}

template <typename ComponentType>
void TrivialComponentSaver<ComponentType>::load(std::vector<Serialization::Buffer_t>& buffer,
                                                size_t& pos,
                                                EntityManager& etm,
                                                Handles::HandleManager& hm) const
{
    const auto savedVersion = Serialization::deserialize<Serialization::SerializationVersion_t>(buffer, pos);

    if (savedVersion != this->serializer.getSerializationVersion())
    {
        throw std::invalid_argument(
            "(Default) saver for " + std::string(Components::ComponentID<ComponentType>::name) +
            ": no known way to deserialize saved version " + std::to_string(savedVersion) +
            " with serializer version " + std::to_string(this->serializer.getSerializationVersion()) +
            " did someone forget to write an update routine?");
    }

    const entityID_t numEntities = Serialization::deserialize<entityID_t>(buffer, pos);
    debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                  "deserialized numEntities",
                  numEntities,
                  "from pos",
                  pos,
                  "out of buffer size",
                  buffer.size());
    for (entityID_t i = 0; i < numEntities; ++i)
    {
        const entityID_t entityID = Serialization::deserialize<entityID_t>(buffer, pos);
        debugOutLevel(Debug::DebugLevels::updateLoop + 2, "deserialized entityID", entityID, "from pos", pos);
        if (entityID == ECS::INVALID_ENTITY)
        {
            throw std::invalid_argument(
                "tried to read the " + std::to_string(i) + "-th entity of " + std::to_string(numEntities) +
                " entities from index " + std::to_string(pos) + " inside a buffer size " +
                std::to_string(buffer.size()) + " but read ECS::INVALID_ENTITY or pos is after buffer!");
        }

        this->serializer.deserializeForEntity(buffer, pos, entityID, etm, hm);

    } // for (entityID_t i = 0; i < numEntities; ++i)
}
