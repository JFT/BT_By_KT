/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SAVINGLOADING_H
#define SAVINGLOADING_H

#include <map>
#include <memory> // std::unique_ptr
#include <string>
#include <unordered_map>
#include <vector>
#include "../entity.h" // componentID_t
#include "../systems/systemIdentifier.h"
#include "../../utils/static/error.h"

class IComponentSaver;
class ISystemSaver;
class SystemBase;
class EntityManager;
namespace Handles
{
    class HandleManager;
}
namespace irr
{
    namespace scene
    {
        class ISceneManager;
    }
} // namespace irr

class SavingLoading
{
   public:
    struct SaveLoadDependencies
    {
        std::map<std::string, std::size_t> blueprintIDtoNameMap = {};
        /// @brief sceneManager needed by:
        /// GraphicComponent
        /// Icon
        /// component saver/loader.
        /// If this is nullptr (de)serialization these components
        /// won't be saved/loaded correctly.
        /// Also needed by:
        /// Camera position saving.
        /// If you don't supply the sceneManager the camera position can't be saved/restored.
        irr::scene::ISceneManager* sceneManager = nullptr;
    };


    /// @brief save the full state of the ECS
    /// @param filename
    /// @param entityManager
    /// @param handleManager
    /// @param dependencies dependencies for specific saver/loaders. If a dependency for a
    /// saver/loader isn't supplied the component values can't be saved and will be ignored (all
    /// other values will be saved normally)
    /// @return
    static ErrCode saveEntitySystem(const std::string& filename,
                                    const EntityManager& entityManager,
                                    const Handles::HandleManager& handleManager,
                                    const SaveLoadDependencies& dependencies);

    /// @brief load a state of the ECS saved with 'saveEntitySystem'
    /// @param filename
    /// @param entityManager
    /// @param handleManager
    /// @param dependencies dependencies for specific saver/loaders. If a dependency for a
    /// saver/loader isn't supplied the component values can't be loaded and will be ignored (all
    /// other values will be loaded normally)
    /// @return
    static ErrCode loadEntitySystem(const std::string& filename,
                                    EntityManager& entityManager,
                                    Handles::HandleManager& handleManager,
                                    const SaveLoadDependencies& dependencies);

   private:
    /// @brief return the correct saver/loader for a specific component.
    /// @param component
    /// @param dependencies SaveLoadDependencies to be used for loading. If any dependency for a
    /// component saver/loader isn't supplied nullptr is returned
    /// @return might be nullptr if no saver was found (e.g. none implemented or missing dependency)
    static std::unique_ptr<IComponentSaver>
    getComponentSaverFor(const componentID_t component, const SaveLoadDependencies& dependencies);

    /// @brief get a saver/loader for a specific system.
    /// @param system which system to get the saver/loader for
    /// @return might be nullptr if no saver/loader was found (e.g. none implemented)
    static std::unique_ptr<ISystemSaver> getSystemSaverFor(SystemBase& system);
};


#endif /* ifndef SAVINGLOADING_H */
