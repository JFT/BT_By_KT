
#include "componentFlags.h"

namespace Components
{
    std::vector<componentID_t> getComponentIDArrayFromFlag(const ComponentFlag_t flag)
    {
        std::vector<componentID_t> components;
        components.reserve(NumComponents);
        for (componentID_t i = 0; i < NumComponents; i++)
        {
            if (hasComponent(flag, i))
            {
                components.push_back(i);
            }
        }
        return components;
    }
} // namespace Components
