/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Joachim Beerwerth, Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#ifndef BT_BY_KT_SRC_ECS_COMPONENTSERIALIZER_H_
#define BT_BY_KT_SRC_ECS_COMPONENTSERIALIZER_H_

#include <unordered_map>
#include <vector>

#include "componentID.h"
#include "componentSerialization.h"
#include "components.h"
#include "entity.h"
#include "entityManager.h"
#include "../utils/Handle.h"
#include "../utils/HandleManager.h"
#include "../utils/serialization.h"
#include "../utils/static/error.h"

namespace irr
{
    namespace scene
    {
        class IAnimatedMesh;
        class ISceneManager;
    } // namespace scene
} // namespace irr


class IComponentSerializerBase
{
   public:
    IComponentSerializerBase() {}

    /// @return componentID of the component this serializer (de)serializes.
    /// See ComponentID<ComponentType>::CID in ecs/componentID.h
    virtual componentID_t getComponentID() const = 0;

    virtual Serialization::SerializationVersion_t getSerializationVersion() const = 0;

    /// @brief serialize one component value for 'entity' and append the serialized bytes to
    /// 'buffer', resizing if neccecary
    /// @param buffer
    /// @param entity the entity MUST have the component!
    /// @param etm
    /// @param hm
    virtual void serializeForEntity(std::vector<Serialization::Buffer_t>& buffer,
                                    const entityID_t entity,
                                    const EntityManager& etm,
                                    const Handles::HandleManager& hm) const = 0;

    /// @brief read one component value out of 'buffer' beginning at offset byte 'pos'
    /// and set the component for 'entity' to that value.
    /// If the entity already has the component it's value will be changed to the new value.
    /// Otherwise EntityManager::addInitializedComponent() will be called.
    /// @param buffer
    /// @param pos byte offset from the beginning of 'buffer' at which the component value to
    /// deserialize begins. 'pos' will be advanced by amount of bytes read after succesfull
    /// deserialization.
    /// @param entity component value will be set for this entity. This entity MUST already exist!
    /// @param etm
    /// @param hm
    virtual void deserializeForEntity(const std::vector<Serialization::Buffer_t>& buffer,
                                      size_t& pos,
                                      const entityID_t entity,
                                      EntityManager& etm,
                                      Handles::HandleManager& hm) const = 0;

    virtual ~IComponentSerializerBase(){};

   private:
};

template <typename ComponentType>
class IComponentSerializer : public IComponentSerializerBase
{
   public:
    virtual componentID_t getComponentID() const;

    virtual Serialization::SerializationVersion_t getSerializationVersion() const;

    virtual ~IComponentSerializer(){};

    virtual void serializeForEntity(std::vector<Serialization::Buffer_t>& buffer,
                                    const entityID_t entity,
                                    const EntityManager& etm,
                                    const Handles::HandleManager& hm) const = 0;

    virtual void deserializeForEntity(const std::vector<Serialization::Buffer_t>& buffer,
                                      size_t& pos,
                                      const entityID_t entity,
                                      EntityManager& etm,
                                      Handles::HandleManager& hm) const = 0;
};

/// @brief Use an instance of this class to serialize a component which
/// can be serialized using only the component value an can be deserialized
/// using only the serialized bytes for that component (e.g. Position).
/// For this Serialization::ComponentSerialization::(de)serialize functions
/// must be implemented.
/// Components which have external dependencies which need to be resolved
/// when (de)serializing need their own class which inherits from IComponentSerializer
/// (e.g. Graphic, which needs a way to deal with irr::scene::IAnimatedMesh*)
/// @tparam ComponentType Component type which shold be (de)serialized.
template <typename ComponentType>
class TrivialComponentSerializer : public IComponentSerializer<ComponentType>
{
    static_assert(not std::is_same<ComponentType, Components::Graphic>::value,
                  "Graphic components MUST be serialized using the GraphicComponentSerializer!");
    static_assert(not std::is_same<ComponentType, Components::Icon>::value,
                  "Graphic components MUST be serialized using the IconComponentSerializer!");

   public:
    virtual void serializeForEntity(std::vector<Serialization::Buffer_t>& buffer,
                                    const entityID_t entity,
                                    const EntityManager& etm,
                                    const Handles::HandleManager& hm) const;

    virtual void deserializeForEntity(const std::vector<Serialization::Buffer_t>& buffer,
                                      size_t& pos,
                                      const entityID_t entity,
                                      EntityManager& etm,
                                      Handles::HandleManager& hm) const;
};

class GraphicComponentSerializer : public IComponentSerializerBase
{
   public:
    GraphicComponentSerializer();

    GraphicComponentSerializer(const GraphicComponentSerializer&) = delete;
    GraphicComponentSerializer& operator=(const GraphicComponentSerializer&) = delete;

    virtual componentID_t getComponentID() const
    {
        return Components::ComponentID<Components::Graphic>::CID;
    }

    virtual Serialization::SerializationVersion_t getSerializationVersion() const
    {
        return Serialization::ComponentSerialization::SerializationVersion<Components::Graphic>::value;
    }

    /// @brief behaviour: see serializeForEntity of IComponentSerializerBase
    /// Serializes the Graphic component for 'entity'.
    /// Can only succesfully serialize the component if the mesh
    /// (and/or texture) are assigned an ID.
    /// See addMeshToIDMapping() and addTextureToIDMapping().
    /// If this mapping doesn't exist when serializing mesh (and/or texture)
    /// will be serialized as nullptr.
    virtual void serializeForEntity(std::vector<Serialization::Buffer_t>& buffer,
                                    const entityID_t entity,
                                    const EntityManager& etm,
                                    const Handles::HandleManager& hm) const;

    /// @brief behaviour: see deserializeForEntity of IComponentSerializerBase
    /// Deserializes the Graphic component for 'entity'.
    /// Can only succesfully deserialize the component if
    /// the deserialized mesh and/or texture IDs are assigned a pointer
    /// value. Otherwise Graphic::mesh and/or Graphic::texture will be nullptr.
    /// See addMeshToIDMapping() and addTextureToIDMapping().
    /// This mapping should be the same as used during serialization.
    /// If no such mapping exists the deserialized graphic component will
    /// have mesh and/or texture set to nullptr.
    virtual void deserializeForEntity(const std::vector<Serialization::Buffer_t>& buffer,
                                      size_t& pos,
                                      const entityID_t entity,
                                      EntityManager& etm,
                                      Handles::HandleManager& hm) const;

    /// @brief create a mapping between serialized IDs and used meshes.
    /// Mesh and texture information of the Graphic component.
    /// Mapping during serialization and deserialization should be the same,
    /// otherwise different meshes/textures will be deserialized than originally
    /// serialized.
    /// @param mesh
    /// @param ID
    void addMeshToIDMapping(irr::scene::IAnimatedMesh* const mesh, const irr::s32 ID);

    /// @brief create a mapping between serialized IDs and used meshes.
    /// Mesh and texture information of the Graphic component.
    /// Mapping during serialization and deserialization should be the same,
    /// otherwise different meshes/textures will be deserialized than originally
    /// serialized.
    /// @param mesh
    /// @param ID
    void addTextureToIDMapping(irr::video::ITexture* const texture, const irr::s32 ID);

   private:
    // mappings for serialization/deserialization
    std::unordered_map<irr::scene::IAnimatedMesh*, irr::s32> meshToID = {};
    std::unordered_map<irr::video::ITexture*, irr::s32> textureToID = {};
};


/// @brief see GraphicComponentSerializer for references to the functions
/// Only difference is that Components::Icon doesn't have a mesh
/// and the serializer therefore doesn't need the meshToID map.
class IconComponentSerializer : public IComponentSerializerBase
{
   public:
    IconComponentSerializer();

    IconComponentSerializer(const IconComponentSerializer&) = delete;
    IconComponentSerializer& operator=(const IconComponentSerializer&) = delete;

    virtual componentID_t getComponentID() const
    {
        return Components::ComponentID<Components::Icon>::CID;
    }

    virtual Serialization::SerializationVersion_t getSerializationVersion() const
    {
        return Serialization::ComponentSerialization::SerializationVersion<Components::Icon>::value;
    }

    virtual void serializeForEntity(std::vector<Serialization::Buffer_t>& buffer,
                                    const entityID_t entity,
                                    const EntityManager& etm,
                                    const Handles::HandleManager& hm) const;

    virtual void deserializeForEntity(const std::vector<Serialization::Buffer_t>& buffer,
                                      size_t& pos,
                                      const entityID_t entity,
                                      EntityManager& etm,
                                      Handles::HandleManager& hm) const;

    void addTextureToIDMapping(irr::video::ITexture* const texture, const irr::s32 ID);

   private:
    // mappings for serialization/deserialization
    std::unordered_map<irr::video::ITexture*, irr::s32> textureToID = {};
};

#include "componentSerializer.tpp"

#endif /* BT_BY_KT_SRC_ECS_COMPONENTSERIALIZER_H_ */
