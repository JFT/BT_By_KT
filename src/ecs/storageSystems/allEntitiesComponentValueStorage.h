/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef ALLENTITIESCOMPONENTVALUESTORAGE_H
#define ALLENTITIESCOMPONENTVALUESTORAGE_H

#include "componentStorage.h"

#include "../componentID.h"
#include "../components.h"
#include "../../grid/grid.h"    // defines grid::gridCellID_t
#include "../../map/pathGrid.h" // defines PathGrid::pathGridID_t
#include "../../utils/HandleManager.h"
#include "../../utils/Pow2Assert.h"
#include "../../utils/debug.h"
#include "../../utils/static/error.h"

/// @brief stores the component type 'T' in a single std::vector using the entityID as an index into
/// the vector
/// the vector grows in size for 10000 ticks (=333 seconds at 30 Hz) and then resizes down to the
/// number of entities when defragmenting
/// @tparam T component value type to store inside the vector
template <typename T>
class AllEntitiesComponentValueStorage : public IComponentValueStorage
{
   public:
    explicit AllEntitiesComponentValueStorage(Handles::HandleManager& handleManager_,
                                              const T& defaultValue_ = T())
        : IComponentValueStorage(handleManager_, 10000)
        , defaultValue(defaultValue_)
        , componentValues()
        , handles()
        , freeValues()
        , freeValueHandles(){};

    virtual ~AllEntitiesComponentValueStorage() {}
    virtual componentID_t getComponentID() const { return Components::ComponentID<T>::CID; }
    virtual std::string getComponentName() const { return Components::ComponentID<T>::name; }
    virtual Handles::Handle addComponent(const entityID_t entityID, const Handles::Handle copyFromHandle)
    {
        void* const initialData = this->handleManager.get(copyFromHandle);
        return this->addComponent(entityID, initialData);
    }

    virtual Handles::Handle addComponent(const entityID_t entityID, const void* const initialData = nullptr)
    {
        if (entityID >= this->componentValues.size())
        {
            this->resize(entityID + 1);
            debugOutLevel(Debug::DebugLevels::updateLoop - 1,
                          "resized component storage for",
                          this->getComponentName(),
                          "to new size",
                          entityID + 1,
                          "to make room for entityID",
                          entityID,
                          "new array resides at memory location",
                          &this->componentValues[0],
                          "updating all handles...");
        }

        if (initialData != nullptr)
        {
            this->componentValues[entityID] = *reinterpret_cast<const T* const>(initialData);
        }

        const Handles::Handle handle = this->handleManager.add(&this->componentValues[entityID]);
        this->handles[entityID] = handle;
        return handle;
    }

    virtual Handles::Handle getComponent(const entityID_t entityID) const
    {
        if (entityID >= this->componentValues.size())
        {
            Error::errContinue("tried to get position component of invalid entityID", entityID);
            return Handles::InvalidHandle;
        }
        return this->handles[entityID];
    }

    virtual void copyComponent(const Handles::Handle handleBase, const Handles::Handle handleToCopy)
    {
        void* baseVoid = nullptr;
        void* toCopyVoid = nullptr;
        this->handleManager.get(handleBase, baseVoid);
        this->handleManager.get(handleToCopy, toCopyVoid);
        POW2_ASSERT(baseVoid != nullptr);
        POW2_ASSERT(toCopyVoid != nullptr);
        auto& baseData = *reinterpret_cast<T*>(baseVoid);
        const auto dataToCopy = *reinterpret_cast<T*>(toCopyVoid);
        baseData = dataToCopy;
    }

    virtual void removeComponent(const entityID_t entityID)
    {
        this->handleManager.remove(this->handles[entityID]);
        if (this->handleManager.isValid(this->handles[entityID]))
        {
            Error::errContinue("removed entityID", entityID, "from storage but the HandleManager says it's still valid!");
        }
        // doing nothing else to e.g. the array or the handles for performance reasons
        // TODO: maybe add debug code to actually invalidate the handle -> makes it easier to check
        // if everything okay from inside the systems
        // TODO: remove this as it probably isn't neccecary and only slows us down
        // makes it easyer to debug though
        this->handles[entityID] = Handles::InvalidHandle;
        this->componentValues[entityID] = this->defaultValue;
    }

    virtual void defragment(const entityID_t numEntities) { this->resize(numEntities); }
    virtual Handles::Handle createFreeValue()
    {
        POW2_ASSERT(this->freeValues.size() == this->freeValueHandles.size());
        // try reusing already deleted free values
        size_t i = 0;
        for (i = 0; i < this->freeValueHandles.size(); i++)
        {
            if (this->freeValueHandles[i] == Handles::InvalidHandle)
            {
                // reset to default value
                this->freeValues[i] = this->defaultValue;
                break;
            }
        }
        if (i >= this->freeValues.size())
        {
            this->resizeFreeValues(this->freeValues.size() + 1);
        }

        const auto handle = this->handleManager.add(&this->freeValues[i]);
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "created free",
                      this->getComponentName(),
                      "value at",
                      &this->freeValues[i],
                      "with handle",
                      handle);
        this->freeValueHandles[i] = handle;
        return handle;
    }

    virtual void destroyFreeValue(const Handles::Handle handle)
    {
        POW2_ASSERT(this->freeValues.size() == this->freeValueHandles.size());
        for (size_t i = 0; i < this->freeValueHandles.size(); i++)
        {
            if (this->freeValueHandles[i] == handle)
            {
                std::cerr << "freeing handle" << handle << std::endl;
                this->handleManager.remove(handle);
                this->freeValueHandles[i] = Handles::InvalidHandle;
                if (i + 1 == this->freeValueHandles.size())
                {
                    this->resizeFreeValues(this->freeValueHandles.size() - 1);
                }
                return;
            }
        }
        Error::errTerminate("No free value with handle", handle, "in this component storage!");
    }

   private:
    T defaultValue;
    std::vector<T> componentValues;
    std::vector<Handles::Handle> handles;

    std::vector<T> freeValues;
    std::vector<Handles::Handle> freeValueHandles;

    void resize(const entityID_t newSize)
    {
        this->componentValues.resize(newSize, this->defaultValue);
        // TODO: if downsizing: actually invalidate these handles and maybe check with the
        // HandleManager if they are still valid
        // better yet (as they should invalidate on their own if everything is correct) check if any
        // of them are still valid by iterating over the array
        this->handles.resize(newSize, Handles::InvalidHandle);

        debugOutLevel(Debug::DebugLevels::updateLoop - 1,
                      "resized component storage",
                      this->getComponentName(),
                      "to new size",
                      newSize,
                      "new values array resides at memory location",
                      &this->componentValues[0],
                      "updating all handles...");

        // because the handles point directly to memory that pointer needs to be updated after
        // resizing
        for (entityID_t e = 0; e < this->handles.size() - 1; e++)
        {
            // std::cerr << "testing handle for entity id " << e << " h = " << this->handles[e] << "
            // valid: " << this->handleManager.isValid(e) << " points to " <<
            // this->handleManager.get(this->handles[e]) << std::endl;
            if (this->handleManager.isValid(this->handles[e]))
            {
                this->handleManager.update(this->handles[e], &this->componentValues[e]);
            }
        }
    }

    void resizeFreeValues(const entityID_t newSize)
    {
        this->freeValues.resize(newSize, this->defaultValue);
        // TODO: if downsizing: actually invalidate these handles and maybe check with the
        // HandleManager if they are still valid
        // better yet (as they should invalidate on their own if everything is correct) check if any
        // of them are still valid by iterating over the array
        this->freeValueHandles.resize(newSize, Handles::InvalidHandle);
        debugOutLevel(Debug::DebugLevels::updateLoop - 1,
                      "resized free component",
                      this->getComponentName(),
                      "storage to new size",
                      newSize,
                      "new values array resides at memory location",
                      &this->freeValues[0],
                      "updating all handles...");

        // because the handles point directly to memory that pointer needs to be updated after
        // resizing
        for (entityID_t e = 0; e < this->freeValueHandles.size() - 1; e++)
        {
            if (this->freeValueHandles[e] != Handles::InvalidHandle)
            { // InvalidHandles set if the free component was destroyed via destroyFreeValue()
                POW2_ASSERT(this->handleManager.isValid(this->freeValueHandles[e]));
                this->handleManager.update(this->freeValueHandles[e], &this->freeValues[e]);
            }
        }
    }
};

#endif /* ifndef ALLENTITIESCOMPONENTVALUESTORAGE_H */
