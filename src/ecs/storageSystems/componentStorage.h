/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#ifndef COMPONENT_COLLECTION_H
#define COMPONENT_COLLECTION_H

#include <string>

#include "../entity.h"
#include "../../utils/Handle.h"

// forward declarations
namespace Handles
{
    class HandleManager;
}
class EntityManager;


class IComponentValueStorage
{
   public:
    IComponentValueStorage(Handles::HandleManager& handleManager_, const uint32_t defragmentationInterval_)
        : handleManager(handleManager_)
        , defragmentationInterval(defragmentationInterval_){};

    IComponentValueStorage(const IComponentValueStorage& other) = delete;
    IComponentValueStorage& operator=(const IComponentValueStorage& other) = delete;

    virtual componentID_t getComponentID() const = 0;

    virtual std::string getComponentName() const = 0;

    /// @brief add this component to a specific entity
    /// @param entityID
    /// @param copyFromHandle handle of data with which this component should be initialized
    /// @return Handle to the value of the added component
    virtual Handles::Handle addComponent(const entityID_t entityID, const Handles::Handle copyFromHandle) = 0;

    /// @brief add this component to a specific entity
    /// @param entityID
    /// @param initialData ptr to data with which this component should be initialized
    /// @return Handle to the value of the added component
    virtual Handles::Handle addComponent(const entityID_t entityID, const void* const initialData = nullptr) = 0;

    virtual Handles::Handle getComponent(const entityID_t entityID) const = 0;

    virtual void copyComponent(const Handles::Handle handleBase, const Handles::Handle handleToCopy) = 0;

    /// @brief remove this component from a specific entity
    /// @param entityID
    virtual void removeComponent(const entityID_t entityID) = 0;

    /// @brief creates storage for this component which isn't connected to any entity.
    /// WARNING: this storage only gets cleared if this object is destructed.
    /// To destroy free values while this object still exists call 'destroyFreeValue()'
    /// with the handle that was returned by 'createFreeValue()'.
    /// @return a handle to the free value (initialized with default values).
    virtual Handles::Handle createFreeValue() = 0;

    /// @brief destroy a value of the component which was created by 'createFreeValue()'
    /// and which isn't connected to a particular entity.
    /// @param handle
    virtual void destroyFreeValue(const Handles::Handle handle) = 0;

    /// @brief called every 'IComponentValueStorage::defragmentationInterval' ticks (+- maybe a few
    /// ticks to balance out resorting of multiple componentValueStorages)
    /// asking the storage to (if neccecary) defragment it's contents and update the handles
    /// accordingly.
    /// Actual implementation of this defragmentation is left to the individual storages.
    virtual void defragment(const entityID_t numEntities) = 0;

    virtual ~IComponentValueStorage(){};

   protected:
    Handles::HandleManager& handleManager;
    /// @brief every 'defragmentationInterval' ticks the 'defragment()' method should be called by
    /// the states updateloops.
    uint32_t defragmentationInterval;
};

#endif // COMPONENT_COLLECTION_H
