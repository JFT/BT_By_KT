/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <unordered_map>
#include <utility> // std::pair
#include <vector>
#include "entity.h" // defines componentID_t
#include "../network/globaldefine.h"
#include "../utils/Handle.h"

// forcard declarations
class EntityManager;
namespace Handles
{
    class HandleManager;
}


/// @brief class to create a 'Blueprint' of an entity
/// with which an entity can easily be created multiple times
/// without having to set all components individually
class EntityBlueprint
{
   public:
    const std::string name;

    EntityBlueprint(const std::string name_, EntityManager& entityManager_)
        : name(name_)
        , entityManager(entityManager_)
    {
    }

    void addComponent(const std::string component, const Handles::Handle handle);

    void addComponent(const componentID_t component, const Handles::Handle handle);

    Handles::Handle getComponent(const componentID_t component) const;

    std::vector<std::pair<componentID_t, Handles::Handle>> getAllComponents() const
    {
        return this->componentValues;
    }

    entityID_t createEntity(const bool createdByServer = GlobalBools::systemIsServer) const;

    entityID_t createEntity(const componentID_t component,
                            const void* const initialValue,
                            const bool createdByServer = GlobalBools::systemIsServer) const;

    entityID_t createEntity(const std::unordered_map<componentID_t, const void* const>& initialValues,
                            const bool createdByServer = GlobalBools::systemIsServer) const;

    // todo ?? WE also have to sync with EntityFactory::getBlueprintType

    enum class BlueprintType
    {
        Invalid,
        Tank,
        Creep,
        AutomaticWeapon,
        Projectile,
        Shop,
        DroppedItemChest,
        Count

    };

    const std::string BlueprintTypeNames[size_t(BlueprintType::Count)] = {
        "Invalid", "Tank", "Creep", "AutomaticWeapon", "Projectile", "Shop", "DroppedItemChest"};

   private:
    std::vector<std::pair<componentID_t, Handles::Handle>> componentValues = {};
    EntityManager& entityManager;
};
