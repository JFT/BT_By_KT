/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef INETWORKCOMPONENTSYNCRONIZATION
#define INETWORKCOMPONENTSYNCRONIZATION

#include <raknet/ReplicaManager3.h>
#include "componentSerialization.h"
#include "../network/globaldefine.h"

// forward declarations
class IComponentSerializerBase;
class EntityManager;
namespace Handles
{
    class HandleManager;
}


class INetworkComponentSyncronization : public RakNet::Replica3
{
   public:
    INetworkComponentSyncronization(EntityManager& entityManager_, Handles::HandleManager& handleManager_);

    INetworkComponentSyncronization(const INetworkComponentSyncronization&) = delete;

    void applyReceivedComponents();

    virtual ~INetworkComponentSyncronization();

    // ----------------------------------------------------------------------------
    // Replica functions
    // ----------------------------------------------------------------------------

    void WriteAllocationID(RakNet::Connection_RM3* /* destinationConnection */,
                           RakNet::BitStream* allocationIdBitstream) const;
    void DeallocReplica(RakNet::Connection_RM3* /* sourceConnection */) { delete this; }

    virtual RakNet::RM3ConstructionState QueryConstruction(RakNet::Connection_RM3* destinationConnection,
                                                           RakNet::ReplicaManager3* /* replicaManager3 */);

    virtual bool QueryRemoteConstruction(RakNet::Connection_RM3* sourceConnection);

    virtual void SerializeConstruction(RakNet::BitStream* constructionBitstream,
                                       RakNet::Connection_RM3* destinationConnection);
    virtual bool DeserializeConstruction(RakNet::BitStream* constructionBitstream,
                                         RakNet::Connection_RM3* sourceConnection);
    virtual void SerializeConstructionExisting(RakNet::BitStream* /* constructionBitstream */,
                                               RakNet::Connection_RM3* /* destinationConnection */);
    virtual void DeserializeConstructionExisting(RakNet::BitStream* /* constructionBitstream */,
                                                 RakNet::Connection_RM3* /* sourceConnection */);

    // Destruction
    virtual void SerializeDestruction(RakNet::BitStream* /* destructionBitstream */,
                                      RakNet::Connection_RM3* /* destinationConnection */)
    {
        // TODO: find out what to do here...
    }
    virtual bool DeserializeDestruction(RakNet::BitStream* /* destructionBitstream */,
                                        RakNet::Connection_RM3* /* sourceConnection */)
    {
        // TODO: find out what to do here...
        return false;
    }

    virtual RakNet::RM3QuerySerializationResult
    QuerySerialization(RakNet::Connection_RM3* destinationConnection) = 0;

    virtual RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* serializeParameters) = 0;

    virtual void Deserialize(RakNet::DeserializeParameters* deserializeParameters) = 0;

    virtual RakNet::RM3ActionOnPopConnection
    QueryActionOnPopConnection(RakNet::Connection_RM3* /* droppedConnection */) const
    {
        return RakNet::RM3AOPC_DO_NOTHING;
    }

    INetworkComponentSyncronization& operator=(const INetworkComponentSyncronization&) = delete;

   protected:
    enum class SerializationVariant_e : uint8_t
    {
        SingleEntity = 0,
        SingleComponent
    };

    EntityManager& entityManager;
    Handles::HandleManager& handleManager;
};

#endif /* ifndef NETWORKCOMPONENTSYNCRONIZATION */
