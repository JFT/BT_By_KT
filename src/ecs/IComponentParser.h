/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>

#include "entity.h"
#include "../utils/Handle.h"

// forward declarations
namespace Json
{
    class Value;
}
namespace Handles
{
    class HandleManager;
}

namespace ComponentParsing
{
    class IComponentParser
    {
       public:
        explicit IComponentParser(Handles::HandleManager& handleManager_)
            : handleManager(handleManager_){};

        virtual ~IComponentParser(){};

        // virtual componentID_t getComponentID() const = 0;
        // virtual std::string getComponentName() const = 0;

        virtual bool parseFromJson(Json::Value& v, Handles::Handle handle) const = 0;

       protected:
        Handles::HandleManager& handleManager;
    };

} // namespace ComponentParsing
