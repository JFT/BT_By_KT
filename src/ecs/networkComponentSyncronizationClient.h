/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef NETWORKCOMPONENTSYNCRONIZATIONCLIENT
#define NETWORKCOMPONENTSYNCRONIZATIONCLIENT 1

#include "INetworkComponentSyncronization.h"
#include "componentSerialization.h"


class NetworkComponentSyncronizationClient : public INetworkComponentSyncronization
{
   public:
    NetworkComponentSyncronizationClient(EntityManager& entityManager_,
                                         Handles::HandleManager& handleManager_,
                                         const std::vector<IComponentSerializerBase*>& componentSerializers_);

    NetworkComponentSyncronizationClient(const NetworkComponentSyncronizationClient&) = delete;
    NetworkComponentSyncronizationClient& operator=(const NetworkComponentSyncronizationClient&) = delete;

    virtual ~NetworkComponentSyncronizationClient();

    void applyReceivedComponents();

    // ----------------------------------------------------------------------------
    // Replica functions
    // ----------------------------------------------------------------------------

    virtual RakNet::RM3QuerySerializationResult
    QuerySerialization(RakNet::Connection_RM3* /* destinationConnection */) final
    {
        return RakNet::RM3QuerySerializationResult::RM3QSR_NEVER_CALL_SERIALIZE;
    }

    virtual RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* /* serializeParameters */)
    {
        // the client should only receive component updates, never send them to the server
        return RakNet::RM3SerializationResult::RM3SR_NEVER_SERIALIZE_FOR_THIS_CONNECTION;
    }

    virtual void Deserialize(RakNet::DeserializeParameters* deserializeParameters);

   private:
    std::vector<Serialization::Buffer_t> deserializedComponentBuffer;

    std::vector<IComponentSerializerBase*> componentSerializers;

    void deserializeEntity(size_t& pos);

    void deserializeSingleComponent(size_t& pos);

    /// @brief Makes sure that 'entity' was created by the server.
    /// Creates a new entity if the entity didn't exist and moves an
    /// existing entity created by the client if neccecary.
    /// @param entity the entityID of the entity to be used.
    /// Will have 'createdByServer' set to true after this function.
    void createOrMoveEntityIfNeeded(const entityID_t entity);
};

#endif /* ifndef NETWORKCOMPONENTSYNCRONIZATIONCLIENT */
