/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Joachim Beerwerth, Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "componentSerializer.h"
#include <algorithm>

GraphicComponentSerializer::GraphicComponentSerializer()
{
    // default mapping so nullptr doesn' have to be added explicitly
    this->meshToID[nullptr] = -1;
    this->textureToID[nullptr] = -1;
}

void GraphicComponentSerializer::addMeshToIDMapping(irr::scene::IAnimatedMesh* const mesh, const irr::s32 ID)
{
    this->meshToID[mesh] = ID;
}

void GraphicComponentSerializer::addTextureToIDMapping(irr::video::ITexture* const texture, const irr::s32 ID)
{
    this->textureToID[texture] = ID;
}

void GraphicComponentSerializer::serializeForEntity(std::vector<Serialization::Buffer_t>& buffer,
                                                    const entityID_t entity,
                                                    const EntityManager& etm,
                                                    const Handles::HandleManager& hm) const
{
    using GC = Components::Graphic;
    POW2_ASSERT(etm.hasComponent<GC>(entity));
    const auto handle = etm.getComponent<GC>(entity);
    const auto& value = hm.getAsRef<GC>(handle);

    const auto meshIt = this->meshToID.find(value.mesh);
    if (meshIt != this->meshToID.end())
    {
        Serialization::serialize<int32_t>(buffer, meshIt->second);
    }
    else
    {
        Error::errContinue("serialized mesh = nullptr for entity",
                           entity,
                           "because there is no entry inside the meshToID map! Did you forget to "
                           "call addMeshToIDMapping()?");
        Serialization::serialize<int32_t>(buffer, -1);
    }

    const auto textureIt = this->textureToID.find(value.texture);
    if (textureIt != this->textureToID.end())
    {
        Serialization::serialize<int32_t>(buffer, textureIt->second);
    }
    else
    {
        Error::errContinue("serialized texture = nullptr for entity",
                           entity,
                           "because there is no entry inside the textureToID map! Did you forget "
                           "to call addTextureToIDMapping()?");
        Serialization::serialize<int32_t>(buffer, -1);
    }
    Serialization::serialize<irr::core::vector3df>(buffer, value.scale);
}

void GraphicComponentSerializer::deserializeForEntity(const std::vector<Serialization::Buffer_t>& buffer,
                                                      size_t& pos,
                                                      const entityID_t entity,
                                                      EntityManager& etm,
                                                      Handles::HandleManager&) const
{
    using GC = Components::Graphic;
    const auto meshID = Serialization::deserialize<int32_t>(buffer, pos);

    const auto meshIt = std::find_if(this->meshToID.begin(),
                                     this->meshToID.end(),
                                     [meshID](const decltype(this->meshToID)::value_type& a) {
                                         return a.second == meshID;
                                     });
    if (meshIt == this->meshToID.end())
    {
        throw Serialization::serialization_exception(
            "deserialized meshID " +
            std::to_string(meshID) + " but this wasn't found in the meshToID map. Was the mesh added with 'addMeshToIDMapping'?");
    }

    const auto textureID = Serialization::deserialize<int32_t>(buffer, pos);
    const auto textureIt = std::find_if(this->textureToID.begin(),
                                        this->textureToID.end(),
                                        [textureID](const decltype(this->textureToID)::value_type& a) {
                                            return a.second == textureID;
                                        });
    if (textureIt == this->textureToID.end())
    {
        throw Serialization::serialization_exception(
            "deserialized textureID " + std::to_string(textureID) +
            " but this wasn't found in the textureToID map. Was the texture added with "
            "'addtextureToIDMapping'?");
    }

    const auto scale = Serialization::deserialize<irr::core::vector3df>(buffer, pos);

    if (etm.hasComponent<GC>(entity))
    {
        // don't know what to do here??
        Error::errTerminate(
            "deserialized graphic component which the entity already had!, what to do?");
    }
    else
    {
        GC newComponent;
        newComponent.mesh = meshIt->first;
        newComponent.texture = textureIt->first;
        newComponent.scale = scale;
        etm.addInitializedComponent<GC>(entity, &newComponent);
    }
}


IconComponentSerializer::IconComponentSerializer()
{
    // default mapping so nullptr doesn' have to be added explicitly
    this->textureToID[nullptr] = -1;
}

void IconComponentSerializer::addTextureToIDMapping(irr::video::ITexture* const texture, const irr::s32 ID)
{
    this->textureToID[texture] = ID;
}

void IconComponentSerializer::serializeForEntity(std::vector<Serialization::Buffer_t>& buffer,
                                                 const entityID_t entity,
                                                 const EntityManager& etm,
                                                 const Handles::HandleManager& hm) const
{
    using IC = Components::Icon;
    POW2_ASSERT(etm.hasComponent<IC>(entity));
    const auto handle = etm.getComponent<IC>(entity);
    const auto& value = hm.getAsRef<IC>(handle);

    const auto textureIt = this->textureToID.find(value.texture);
    if (textureIt != this->textureToID.end())
    {
        Serialization::serialize<int32_t>(buffer, textureIt->second);
    }
    else
    {
        Error::errContinue("serialized texture = nullptr for entity",
                           entity,
                           "because there is no entry inside the textureToID map! Did you forget "
                           "to call addTextureToIDMapping()?");
        Serialization::serialize<int32_t>(buffer, -1);
    }
    Serialization::serialize<std::vector<uint32_t>>(buffer, std::vector<uint32_t>());
}

void IconComponentSerializer::deserializeForEntity(const std::vector<Serialization::Buffer_t>& buffer,
                                                   size_t& pos,
                                                   const entityID_t entity,
                                                   EntityManager& etm,
                                                   Handles::HandleManager&) const
{
    using IC = Components::Icon;

    const auto textureID = Serialization::deserialize<int32_t>(buffer, pos);
    const auto vec = Serialization::deserialize<std::vector<uint32_t>>(buffer, pos);
    std::cerr << "VECTOR: " << vec.size() << std::endl;
    const auto textureIt = std::find_if(this->textureToID.begin(),
                                        this->textureToID.end(),
                                        [textureID](const decltype(this->textureToID)::value_type& a) {
                                            return a.second == textureID;
                                        });
    if (textureIt == this->textureToID.end())
    {
        throw Serialization::serialization_exception(
            "deserialized textureID " + std::to_string(textureID) +
            " but this wasn't found in the textureToID map. Was the texture added with "
            "'addtextureToIDMapping'?");
    }

    if (etm.hasComponent<IC>(entity))
    {
        // don't know what to do here??
        Error::errTerminate(
            "deserialized graphic component which the entity already had!, what to do?");
    }
    else
    {
        IC newComponent;
        newComponent.texture = textureIt->first;
        etm.addInitializedComponent<IC>(entity, &newComponent);
    }
}
