/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENTITY_H
#define ENTITY_H

#include <cstdint>  // (u)intX_t
#include "stddef.h" // defines size_t

// TODO: choose smaller componentID type. Maybe even bitfiddle to fit both component id and index
// into 4 bytes
typedef size_t blueprintID_t;
typedef size_t componentID_t;
typedef size_t entityID_t;

namespace ECS
{
    constexpr blueprintID_t INVALID_BLUEPRINT = static_cast<blueprintID_t>(-1);
    constexpr componentID_t INVALID_COMPONENT = static_cast<componentID_t>(-1);
    constexpr entityID_t INVALID_ENTITY = static_cast<entityID_t>(-1);
} // namespace ECS

#endif // ENTITY_H
