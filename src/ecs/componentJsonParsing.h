/*
BattleTanks standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <map>

#include "components.h"

#include <string>

// forward declarations
namespace Json
{
    class Value;
}
namespace irr
{
    namespace scene
    {
        class ISceneManager;
    }
} // namespace irr


namespace ComponentParsing
{
    template <typename T>
    bool parseFromJson(const Json::Value& v, T& out);

    template <typename T>
    bool parseFromJson(const Json::Value& v, T& out, irr::scene::ISceneManager* const sceneManager);

    template <typename T>
    bool parseFromJson(const Json::Value& v, T& out, std::map<std::string, blueprintID_t>& blueprintNameToIDMap);
} // namespace ComponentParsing
