#include "componentSerialization.h"
#include "components.h"

namespace Serialization
{
    namespace ComponentSerialization
    {
        template <>
        void serialize(std::vector<Buffer_t>& buffer, const Components::Movement& value)
        {
            Serialization::serialize<irr::f32>(buffer, value.speed);
            Serialization::serialize<uint32_t>(buffer, static_cast<uint32_t>(value.waypoints.size()));
            for (const auto& waypoint : value.waypoints)
            {
                Serialization::serialize<irr::core::vector2df>(buffer, waypoint);
            }
            Serialization::serialize<irr::f32>(buffer, value.expectedDistanceSQToWaypoint);
            Serialization::serialize<uint8_t>(buffer, static_cast<uint8_t>(value.movementStatus));
            Serialization::serialize<irr::core::vector2df>(buffer, value.velocity);
            Serialization::serialize<irr::core::vector2df>(buffer, value.newVelocity);
            Serialization::serialize<irr::f32>(buffer, value.timeHorizon);
            Serialization::serialize<irr::f32>(buffer, value.radius);
            Serialization::serialize<irr::f32>(buffer, value.maxSpeed);
            Serialization::serialize<irr::f32>(buffer, value.maxRotationSpeed);
        }

        template <>
        Components::Movement deserialize(const std::vector<Buffer_t>& buffer, std::size_t& pos)
        {
            Components::Movement value;
            value.speed = Serialization::deserialize<irr::f32>(buffer, pos);
            value.waypoints.resize(Serialization::deserialize<uint32_t>(buffer, pos));
            for (auto& waypoint : value.waypoints)
            {
                waypoint = Serialization::deserialize<irr::core::vector2df>(buffer, pos);
            }
            value.expectedDistanceSQToWaypoint = Serialization::deserialize<irr::f32>(buffer, pos);
            value.movementStatus = static_cast<Components::Movement::MovementStatus>(
                Serialization::deserialize<uint8_t>(buffer, pos));
            value.velocity = Serialization::deserialize<irr::core::vector2df>(buffer, pos);
            value.newVelocity = Serialization::deserialize<irr::core::vector2df>(buffer, pos);
            value.timeHorizon = Serialization::deserialize<irr::f32>(buffer, pos);
            value.radius = Serialization::deserialize<irr::f32>(buffer, pos);
            value.maxSpeed = Serialization::deserialize<irr::f32>(buffer, pos);
            value.maxRotationSpeed = Serialization::deserialize<irr::f32>(buffer, pos);
            return value;
        }

        template <>
        void serialize(std::vector<Buffer_t>& buffer, const Components::EntityName& value)
        {
            Serialization::serialize<irr::core::stringw>(buffer, value.entityName);
        }

        template <>
        Components::EntityName deserialize(const std::vector<Buffer_t>& buffer, std::size_t& pos)
        {
            Components::EntityName value;
            value.entityName = Serialization::deserialize<irr::core::stringw>(buffer, pos);
            return value;
        }

        template <>
        void serialize(std::vector<Buffer_t>& buffer, const Components::PurchasableItems& value)
        {
            Serialization::serialize<decltype(value.purchasableItems)>(buffer, value.purchasableItems);
        }

        template <>
        Components::PurchasableItems deserialize(const std::vector<Buffer_t>& buffer, std::size_t& pos)
        {
            Components::PurchasableItems items;
            items.purchasableItems =
                Serialization::deserialize<decltype(items.purchasableItems)>(buffer, pos);
            return items;
        }

        template <>
        void serialize(std::vector<Buffer_t>& buffer, const Components::Physical& value)
        {
            Serialization::serialize<float>(buffer, value.maxHealth);
            Serialization::serialize<float>(buffer, value.health);
            Serialization::serialize<float>(buffer, value.armor);
            Serialization::serialize<int32_t>(buffer, static_cast<int32_t>(value.armorType));
            Serialization::serialize<flag_t>(buffer, value.unitTypeFlag);
        }

        template <>
        Components::Physical deserialize(const std::vector<Buffer_t>& buffer, std::size_t& pos)
        {
            Components::Physical value;
            value.maxHealth = Serialization::deserialize<float>(buffer, pos);
            value.health = Serialization::deserialize<float>(buffer, pos);
            value.armor = Serialization::deserialize<float>(buffer, pos);
            value.armorType = static_cast<Components::Physical::ArmorType>(
                Serialization::deserialize<int32_t>(buffer, pos));
            if (value.armorType >= Components::Physical::ArmorType::Count)
            {
                throw Serialization::serialization_exception(
                    "Deserialized invalid Components::Physical::ArmorType " + std::to_string(value.armorType));
            }
            value.unitTypeFlag = Serialization::deserialize<flag_t>(buffer, pos);
            return value;
        }
    } // namespace ComponentSerialization
} // namespace Serialization
