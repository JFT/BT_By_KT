/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef COMPONENTPARSERS_H
#define COMPONENTPARSERS_H

#include "IComponentParser.h"

#include "componentJsonParsing.h"
#include "components.h"

#include "../utils/HandleManager.h"
#include "../utils/Pow2Assert.h"

namespace irr
{
    namespace scene
    {
        class ISceneManager;
    }
} // namespace irr


namespace ComponentParsing
{
    template <typename T>
    class GenericComponentParser : public IComponentParser
    {
       public:
        explicit GenericComponentParser(Handles::HandleManager& handleManager_)
            : IComponentParser(handleManager_)
        {
        }

        GenericComponentParser(const GenericComponentParser&) = delete;
        GenericComponentParser operator=(const GenericComponentParser&) = delete;

        virtual ~GenericComponentParser(){};

        /*virtual componentID_t getComponentID() const
        {
            return Components::ComponentID<T>::CID;
        }*/

        /*virtual std::string getComponentName() const
        {
            return Components::ComponentID<T>::name;
        }*/

        virtual bool parseFromJson(Json::Value& v, const Handles::Handle handle) const
        {
            POW2_ASSERT(this->handleManager.isValid(handle));
            T& component = this->handleManager.template getAsRef<T>(handle);
            ComponentParsing::parseFromJson(v, component);
            return true;
        }
    };

    template <typename T>
    class BlueprintIDComponentParser : public IComponentParser
    {
       public:
        BlueprintIDComponentParser(Handles::HandleManager& handleManager_,
                                   std::map<std::string, blueprintID_t>& blueprintNameToIDMap_)
            : IComponentParser(handleManager_)
            , blueprintNameToIDMap(blueprintNameToIDMap_)
        {
        }

        BlueprintIDComponentParser(const BlueprintIDComponentParser&) = delete;
        BlueprintIDComponentParser operator=(const BlueprintIDComponentParser&) = delete;

        virtual ~BlueprintIDComponentParser(){};

        /*virtual componentID_t getComponentID() const
        {
            return Components::ComponentID<T>::CID;
        }*/

        /*virtual std::string getComponentName() const
        {
            return Components::ComponentID<T>::name;
        }*/

        virtual bool parseFromJson(Json::Value& v, const Handles::Handle handle) const
        {
            POW2_ASSERT(this->handleManager.isValid(handle));
            T& component = this->handleManager.template getAsRef<T>(handle);
            ComponentParsing::parseFromJson(v, component, this->blueprintNameToIDMap);
            return true;
        }

        std::map<std::string, blueprintID_t>& blueprintNameToIDMap;
    };

    template <typename T>
    class GraphicComponentParser : public IComponentParser
    {
       public:
        GraphicComponentParser(Handles::HandleManager& handleManager_, irr::scene::ISceneManager* const sceneManager_)
            : IComponentParser(handleManager_)
            , sceneManager(sceneManager_)
        {
        }

        GraphicComponentParser(const GraphicComponentParser&) = delete;
        GraphicComponentParser operator=(const GraphicComponentParser&) = delete;

        virtual ~GraphicComponentParser() {}
        /*virtual componentID_t getComponentID() const
        {
            return Components::ComponentID<Components::Graphic>::CID;
        }

        virtual std::string getComponentName() const
        {
            return Components::ComponentID<Components::Graphic>::name;
        }*/

        virtual bool parseFromJson(Json::Value& v, const Handles::Handle handle) const
        {
            POW2_ASSERT(this->handleManager.isValid(handle));
            T& component = this->handleManager.template getAsRef<T>(handle);
            ComponentParsing::parseFromJson(v, component, this->sceneManager);
            return true;
        }

       private:
        irr::scene::ISceneManager* const sceneManager;
    };

} // namespace ComponentParsing

#endif /* ifndef COMPONENTPARSERS_H */
