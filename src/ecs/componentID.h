/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz, Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMPONENTID_H
#define COMPONENTID_H

#include <array>

#include "entity.h"

namespace Components
{
    // forward declare all components
    struct DebugColor;
    struct Position;
    struct Movement;
    struct Graphic;
    struct ProjectileMovement;
    struct Rotation;
    struct EntityName;
    struct Price;
    struct Damage;
    struct Range;
    struct Ammo;
    struct Icon;
    struct Inventory;
    struct PurchasableItems;
    struct Requirement;
    struct SelectionID;
    struct Cooldown;
    struct Physical;
    struct OwnerEntityID;
    struct WeaponEntityID;
    struct DroppedItemEntityID;
    struct PlayerStats;
    struct PlayerBase;
    struct NetworkPriority;

    template <typename T>
    struct ComponentID
    {
    };

    template <>
    struct ComponentID<DebugColor>
    {
        enum
        {
            CID = static_cast<componentID_t>(0)
        };
        static constexpr const char* const name = "DebugColor";
    };
    template <>
    struct ComponentID<Position>
    {
        enum
        {
            CID = static_cast<componentID_t>(1)
        };
        static constexpr const char* const name = "Position";
    };
    template <>
    struct ComponentID<Movement>
    {
        enum
        {
            CID = static_cast<componentID_t>(2)
        };
        static constexpr const char* const name = "Movement";
    };
    template <>
    struct ComponentID<Graphic>
    {
        enum
        {
            CID = static_cast<componentID_t>(3)
        };
        static constexpr const char* const name = "Graphic";
    };
    template <>
    struct ComponentID<ProjectileMovement>
    {
        enum
        {
            CID = static_cast<componentID_t>(4)
        };
        static constexpr const char* const name = "ProjectileMovement";
    };
    template <>
    struct ComponentID<Rotation>
    {
        enum
        {
            CID = static_cast<componentID_t>(5)
        };
        static constexpr const char* const name = "Rotation";
    };
    template <>
    struct ComponentID<EntityName>
    {
        enum
        {
            CID = static_cast<componentID_t>(6)
        };
        static constexpr const char* const name = "EntityName";
    };
    template <>
    struct ComponentID<Price>
    {
        enum
        {
            CID = static_cast<componentID_t>(7)
        };
        static constexpr const char* const name = "Price";
    };
    template <>
    struct ComponentID<Damage>
    {
        enum
        {
            CID = static_cast<componentID_t>(8)
        };
        static constexpr const char* const name = "Damage";
    };
    template <>
    struct ComponentID<Range>
    {
        enum
        {
            CID = static_cast<componentID_t>(9)
        };
        static constexpr const char* const name = "Range";
    };
    template <>
    struct ComponentID<Ammo>
    {
        enum
        {
            CID = static_cast<componentID_t>(10)
        };
        static constexpr const char* const name = "Ammo";
    };
    template <>
    struct ComponentID<Icon>
    {
        enum
        {
            CID = static_cast<componentID_t>(11)
        };
        static constexpr const char* const name = "Icon";
    };
    template <>
    struct ComponentID<Inventory>
    {
        enum
        {
            CID = static_cast<componentID_t>(12)
        };
        static constexpr const char* const name = "Inventory";
    };
    template <>
    struct ComponentID<PurchasableItems>
    {
        enum
        {
            CID = static_cast<componentID_t>(13)
        };
        static constexpr const char* const name = "PurchasableItems";
    };
    template <>
    struct ComponentID<Requirement>
    {
        enum
        {
            CID = static_cast<componentID_t>(14)
        };
        static constexpr const char* const name = "Requirement";
    };
    template <>
    struct ComponentID<SelectionID>
    {
        enum
        {
            CID = static_cast<componentID_t>(15)
        };
        static constexpr const char* const name = "SelectionID";
    };
    template <>
    struct ComponentID<Cooldown>
    {
        enum
        {
            CID = static_cast<componentID_t>(16)
        };
        static constexpr const char* const name = "Cooldown";
    };
    template <>
    struct ComponentID<Physical>
    {
        enum
        {
            CID = static_cast<componentID_t>(17)
        };
        static constexpr const char* const name = "Physical";
    };
    template <>
    struct ComponentID<OwnerEntityID>
    {
        enum
        {
            CID = static_cast<componentID_t>(18)
        };
        static constexpr const char* const name = "OwnerEntityID";
    };
    template <>
    struct ComponentID<WeaponEntityID>
    {
        enum
        {
            CID = static_cast<componentID_t>(19)
        };
        static constexpr const char* const name = "WeaponEntityID";
    };
    template <>
    struct ComponentID<DroppedItemEntityID>
    {
        enum
        {
            CID = static_cast<componentID_t>(20)
        };
        static constexpr const char* const name = "DroppedItemEntityID";
    };
    template <>
    struct ComponentID<PlayerStats>
    {
        enum
        {
            CID = static_cast<componentID_t>(21)
        };
        static constexpr const char* const name = "PlayerStats";
    };
    template <>
    struct ComponentID<PlayerBase>
    {
        enum
        {
            CID = static_cast<componentID_t>(22)
        };
        static constexpr const char* const name = "PlayerBase";
    };
    template <>
    struct ComponentID<NetworkPriority>
    {
        enum
        {
            CID = static_cast<componentID_t>(23)
        };
        static constexpr const char* const name = "NetworkPriority";
    };
    constexpr componentID_t NumComponents = 24;

    constexpr std::array<const char* const, NumComponents> NameFromComponendID = {{
        ComponentID<DebugColor>::name,
        ComponentID<Position>::name,
        ComponentID<Movement>::name,
        ComponentID<Graphic>::name,
        ComponentID<ProjectileMovement>::name,
        ComponentID<Rotation>::name,
        ComponentID<EntityName>::name,
        ComponentID<Price>::name,
        ComponentID<Damage>::name,
        ComponentID<Range>::name,
        ComponentID<Ammo>::name,
        ComponentID<Icon>::name,
        ComponentID<Inventory>::name,
        ComponentID<PurchasableItems>::name,
        ComponentID<Requirement>::name,
        ComponentID<SelectionID>::name,
        ComponentID<Cooldown>::name,
        ComponentID<Physical>::name,
        ComponentID<OwnerEntityID>::name,
        ComponentID<WeaponEntityID>::name,
        ComponentID<DroppedItemEntityID>::name,
        ComponentID<PlayerStats>::name,
        ComponentID<PlayerBase>::name,
        ComponentID<NetworkPriority>::name,
    }};


} // namespace Components
#endif /* ifndef COMPONENTID_H */
