/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "INetworkComponentSyncronization.h"
#include "componentSerializer.h"
#include "../network/allocationIDs.h"
#include "../utils/debug.h"
#include "../utils/static/error.h"


INetworkComponentSyncronization::INetworkComponentSyncronization(EntityManager& entityManager_,
                                                                 Handles::HandleManager& handleManager_)
    : entityManager(entityManager_)
    , handleManager(handleManager_)
{
}

INetworkComponentSyncronization::~INetworkComponentSyncronization() {}

void INetworkComponentSyncronization::WriteAllocationID(RakNet::Connection_RM3* /* destinationConnection */,
                                                        RakNet::BitStream* allocationIdBitstream) const
{
    debugOut("INetworkComponentSyncronization: writing allocation ID",
             Networking::AllocationID::NetworkComponentSyncronization);
    allocationIdBitstream->Write(static_cast<Networking::NetworkedAllocationIDType>(
        Networking::AllocationID::NetworkComponentSyncronization));
}


RakNet::RM3ConstructionState
INetworkComponentSyncronization::QueryConstruction(RakNet::Connection_RM3* destinationConnection,
                                                   RakNet::ReplicaManager3* /* replicaManager3 */)
{
    return QueryConstruction_ServerConstruction(destinationConnection, GlobalBools::systemIsServer);
}

bool INetworkComponentSyncronization::QueryRemoteConstruction(RakNet::Connection_RM3* sourceConnection)
{
    return QueryRemoteConstruction_ServerConstruction(sourceConnection, GlobalBools::systemIsServer);
}

void INetworkComponentSyncronization::SerializeConstruction(RakNet::BitStream* /* constructionBitstream */,
                                                            RakNet::Connection_RM3* /* destinationConnection */)
{
}

bool INetworkComponentSyncronization::DeserializeConstruction(RakNet::BitStream* /* constructionBitstream */,
                                                              RakNet::Connection_RM3* /* sourceConnection */)
{
    return not GlobalBools::systemIsServer; // construct only on client
}


void INetworkComponentSyncronization::SerializeConstructionExisting(RakNet::BitStream* /* constructionBitstream */,
                                                                    RakNet::Connection_RM3* /* destinationConnection */)
{
    POW2_ASSERT_FAIL("this object shouldn't be calling SerializeConstructionExisting because "
                     "'QueryConstruction' souldn't return 'RM3CS_ALREADY_EXISTS_REMOTELY'",
                     true);
}
void INetworkComponentSyncronization::DeserializeConstructionExisting(
    RakNet::BitStream* /* constructionBitstream */, RakNet::Connection_RM3* /* sourceConnection */)
{
    POW2_ASSERT_FAIL("this object shouldn't be calling DeserializeConstructionExisting because "
                     "'QueryConstruction' souldn't return 'RM3CS_ALREADY_EXISTS_REMOTELY'",
                     true);
}
