/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <irrlicht/IEventReceiver.h>

#include <functional>
#include <unordered_map>
#include <variant>


typedef irr::u32 uniqueEventValue_t;

enum class InputContextTypes
{
    DEFAULT_CONTEXT,
    ENTITY_SELECTION_BOX,
    ENTITY_SELECTED_CONTEXT,
    CONTEXT_COUNT
};

class InputContext
{
   public:
    /// @brief Result of event handling by a context.
    struct EventState_t
    {
        /// @brief true if the event was handled and will not be passed to any other receiver
        bool handled;
        /// @brief true if this context is still active, false if it wants to deactivate itself. Note:
        /// not handling the event but still leaving the context is a legitimate use case. This can happen when e.g. pressing 'enter' to activate the chat while a selection box is being drawn.
        bool stayInContext;
    };
    using Action_t = std::function<EventState_t(const irr::SEvent&)>;

    InputContext(const InputContextTypes type);

    virtual ~InputContext() = default;

    /// @brief
    /// @param event If set, the event which caused the transition to this context.
    virtual void onSelect() = 0;

    /// @brief
    /// @param event If set, the event which caused the transition out of this context.
    virtual void onDeselect() = 0;

    virtual EventState_t handleInputEvent(const irr::SEvent& event);


    virtual void setInputAction(uniqueEventValue_t eventValue, Action_t action);

    /// create unique value for a specific event with bitmagic for now only process mouse and
    /// Keyevents correctly
    /// From front to back of bitchain: first 4 bits contain the eventtype
    /// next 8 bits cotain the mouse or keyvalue
    static uniqueEventValue_t generateUniqueEventValue(const irr::SEvent& event);

    /// Input eventType and corresponding eventEnum Descriptor
    /// e.g. EET_MOUSE_INPUT_EVENT and EMIE_LMOUSE_LEFT_UP
    static uniqueEventValue_t
    generateUniqueEventValue(const irr::EEVENT_TYPE eventType, const irr::c8 eventDescriptor);


   protected:
    /// @brief Type of this context
    const InputContextTypes m_type;
    // use an unordered map that puts the values in buckets according to a hash generated from the
    // key
    // the find function has a high probability to be faster that way
    std::unordered_map<uniqueEventValue_t, Action_t> actionMap;
};
