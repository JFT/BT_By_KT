/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "inputContextManager.h"

#include "defaultInputContext.h"
#include "unitSelectedContext.h"

#include "../../utils/Pow2Assert.h"

InputContextManager::InputContextManager(std::unique_ptr<InputContext> defaultContext_)
    : m_current(InputContextTypes::DEFAULT_CONTEXT)
{
    static_assert(static_cast<size_t>(InputContextTypes::DEFAULT_CONTEXT) == 0);
    m_contexts[0] = std::move(defaultContext_);
    for (size_t i = 1; i < m_contexts.size(); ++i)
    {
        m_contexts[i] = static_cast<std::unique_ptr<InputContext>>(std::make_unique<DefaultInputContext>());
    }
}

InputContextManager::InputContextManager()
    : InputContextManager(std::make_unique<DefaultInputContext>())
{
    m_contexts[static_cast<size_t>(InputContextTypes::ENTITY_SELECTED_CONTEXT)] =
        static_cast<std::unique_ptr<InputContext>>(std::make_unique<UnitSelectedContext>());
}

void InputContextManager::setInputContext(InputContextTypes type, std::unique_ptr<InputContext> context)
{
    m_contexts[static_cast<size_t>(type)] = std::move(context);
}

InputContext* InputContextManager::getInputContext(InputContextTypes type)
{
    return getRaw(type);
}

bool InputContextManager::onActiveContextEvent(const irr::SEvent& event)
{
    if (m_current == InputContextTypes::DEFAULT_CONTEXT) return false;

    // Is this a transition event _out_ of the context?
    if (handleTransitions(m_current, event))
    {
        return true;
    }

    const auto status = getRaw(m_current)->handleInputEvent(event);
    if (!status.stayInContext)
    {
        switchContext(InputContextTypes::DEFAULT_CONTEXT);
    }

    return status.handled;
}

bool InputContextManager::onDefaultContextEvent(const irr::SEvent& event)
{
    constexpr auto type = InputContextTypes::DEFAULT_CONTEXT;
    // Is this a transition event _out_ of the context?
    if (handleTransitions(type, event))
    {
        return true;
    }

    // The default context cannot be left, only returning handled.
    return getRaw(type)->handleInputEvent(event).handled;
}

void InputContextManager::addTransition(const InputContextTypes from,
                                        const uniqueEventValue_t event,
                                        const InputContextTypes to,
                                        bool passEvent)
{
    m_transitions[from][event] = {to, passEvent};
}

void InputContextManager::removeTransition(const InputContextTypes from, const uniqueEventValue_t event)
{
    const auto eventDict = m_transitions.find(from);
    POW2_ASSERT(eventDict != m_transitions.end());

    const auto it = m_transitions[from].find(event);
    POW2_ASSERT(it != m_transitions[from].end());

    eventDict->second.erase(it);

    if (eventDict->second.size() == 0)
    {
        m_transitions.erase(eventDict);
    }
}

void InputContextManager::switchContext(const InputContextTypes type)
{
    getRaw(m_current)->onDeselect();
    m_current = type;
    getRaw(m_current)->onSelect();
}

InputContext* InputContextManager::getRaw(InputContextTypes type) const noexcept
{
    return m_contexts[static_cast<size_t>(type)].get();
}

bool InputContextManager::handleTransitions(const InputContextTypes from, const irr::SEvent& event)
{
    if (const auto at = m_transitions.find(from); at != m_transitions.end())
    {
        const auto id = InputContext::generateUniqueEventValue(event);
        if (const auto to = at->second.find(id); to != at->second.end())
        {
            switchContext(to->second.first);
            if (to->second.second)
            {
                getRaw(to->second.first)->handleInputEvent(event);
            }
            return true;
        }
    }

    return false;
}
