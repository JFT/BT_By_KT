/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include "inputContext.hpp"

#include <array>
#include <cstddef> // defines size_t
#include <memory>

// forward declarations
namespace irr
{
    struct SEvent;
}

class InputContextManager
{
   public:
    InputContextManager();
    explicit InputContextManager(std::unique_ptr<InputContext> defaultContext);

    InputContextManager(const InputContextManager&) = delete;
    InputContextManager operator=(const InputContextManager&) = delete;

    virtual ~InputContextManager() = default;

    void switchContext(const InputContextTypes type);
    void setInputContext(const InputContextTypes type, std::unique_ptr<InputContext> context);

    InputContext* getInputContext(const InputContextTypes type);

    /// @brief If a context is active either handle a transition from the context or pass the event to it.
    /// @param event
    /// @return true if a context was active and the event handled or the context transitioned, false otherwise.
    bool onActiveContextEvent(const irr::SEvent& event);

    /// @brief Transition to a different context or pass the event to the default context.
    /// @param event
    /// @return true if the default context handled the event or transitioned to another context, false otherwise.
    bool onDefaultContextEvent(const irr::SEvent& event);

    /// @brief Add a transition from a certain input type to another input type on a certain event.
    /// @param from The context to transition from
    /// @param event The event to transition on
    /// @param to The context to transition to
    /// @param passEvent If true the event which caused the transition is also passed to the context after the transition.
    void addTransition(const InputContextTypes from,
                       const uniqueEventValue_t event,
                       const InputContextTypes to,
                       bool passEvent = false);

    /// @brief Inverse operation of \ref addTransition()
    void removeTransition(const InputContextTypes from, const uniqueEventValue_t event);

   private:
    InputContextTypes m_current;

    std::array<std::unique_ptr<InputContext>, static_cast<size_t>(InputContextTypes::CONTEXT_COUNT)> m_contexts;

    std::unordered_map<InputContextTypes, std::unordered_map<uniqueEventValue_t, std::pair<InputContextTypes, bool>>> m_transitions;

    InputContext* getRaw(const InputContextTypes type) const noexcept;

    bool handleTransitions(const InputContextTypes from, const irr::SEvent& event);
};
