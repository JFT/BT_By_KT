/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "inputContext.hpp"

#include "../../utils/debug.h"

class DefaultInputContext : public InputContext
{
   public:
    DefaultInputContext()
        : InputContext(InputContextTypes::DEFAULT_CONTEXT)
    {
    }

    ~DefaultInputContext() override = default;

    void onSelect() override
    {
        debugOutLevel(Debug::DebugLevels::onEvent, "entering default input context");
    }

    void onDeselect() override
    {
        debugOutLevel(Debug::DebugLevels::onEvent, "leaving default input context");
    }
};
