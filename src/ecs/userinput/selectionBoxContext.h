/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "inputContext.hpp"

#include "../../utils/Pow2Assert.h"
#include "../../utils/aabbox2d.h"
#include "../../utils/debug.h"

class SelectionBoxContext : public InputContext
{
   public:
    SelectionBoxContext(std::function<void(const aabbox2di&)> onSelectionDone)
        : InputContext(InputContextTypes::ENTITY_SELECTION_BOX)
        , m_onSelectionDone(onSelectionDone)
    {
        actionMap[generateUniqueEventValue(irr::EET_MOUSE_INPUT_EVENT, irr::EMIE_MOUSE_MOVED)] =
            [this](const irr::SEvent& event) { return this->handleMouseMoved(event); };
        actionMap[generateUniqueEventValue(irr::EET_MOUSE_INPUT_EVENT, irr::EMIE_LMOUSE_LEFT_UP)] =
            [this](const irr::SEvent& event) { return this->handleMouseReleased(event); };
        actionMap[generateUniqueEventValue(irr::EET_MOUSE_INPUT_EVENT, irr::EMIE_LMOUSE_PRESSED_DOWN)] =
            [this](const irr::SEvent& event) { return this->handleMousePressedDown(event); };
    }

    virtual ~SelectionBoxContext() = default;

    void onSelect() override
    {
        this->selectionHandling.inputState = SelectionHandling::InputStates::E_Default;
    }

    void onDeselect() override
    {
        this->selectionHandling.inputState = SelectionHandling::InputStates::E_Default;
    }

    EventState_t handleMousePressedDown(const irr::SEvent& event)
    {
        // Normally the selection box context should be left once the mouse is depressed and noone
        // else should handle that event. In case this doesn't happen and the state is active when the mouse is pressed leave it (but don't handle the event so another state can handle it).
        if (this->selectionHandling.inputState != SelectionHandling::InputStates::E_Default)
        {
            POW2_ASSERT(false);
            return {false, false};
        }

        debugOutLevel(Debug::DebugLevels::onEvent,
                      "LMouse pressed down, in default input state "
                      "and not handled by cegui -> starting selection "
                      "square");
        this->selectionHandling.selectionStart =
            irr::core::vector2di(event.MouseInput.X, event.MouseInput.Y);
        this->selectionHandling.inputState = SelectionHandling::InputStates::E_SelectionSquare;
        this->updateSelectionBox(event.MouseInput.X, event.MouseInput.Y);

        return {true, true};
    }

    EventState_t handleMouseMoved(const irr::SEvent& event)
    {
        debugOutLevel(Debug::DebugLevels::onEvent,
                      "Mouse moved in default input state "
                      "and not handled by cegui -> selection state: ",
                      static_cast<int>(this->selectionHandling.inputState));

        if (this->selectionHandling.inputState == SelectionHandling::InputStates::E_SelectionSquare)
        {
            debugOutLevel(Debug::DebugLevels::onEvent, "selection square updated");
            this->updateSelectionBox(event.MouseInput.X, event.MouseInput.Y);
            return {true, true};
        }
        return {false, false};
    }

    EventState_t handleMouseReleased(const irr::SEvent& event)
    {
        debugOutLevel(Debug::DebugLevels::onEvent, "Mouse left up in selection box input state");

        POW2_ASSERT(this->selectionHandling.inputState == SelectionHandling::InputStates::E_SelectionSquare);

        debugOutLevel(Debug::DebugLevels::onEvent, "selection square updated");
        this->updateSelectionBox(event.MouseInput.X, event.MouseInput.Y);
        m_onSelectionDone(this->selectionHandling.currentSelection);

        return {true, false};
    }

    inline void updateSelectionBox(const irr::s32 X, const irr::s32 Y)
    {
        POW2_ASSERT(this->selectionHandling.inputState == SelectionHandling::InputStates::E_SelectionSquare);
        debugOutLevel(Debug::DebugLevels::onEvent, "selection square updated");

        this->selectionHandling.currentSelection = aabbox2di(this->selectionHandling.selectionStart);
        this->selectionHandling.currentSelection.addInternalPoint(
            irr::core::vector2di(X, Y)); // adding the point instead of directly creating the bbox
                                         // because the end of the selection box might make either
                                         // the new MinEdge or the new MaxEdge
        // if the mouse was released on the same pixel it was pressed the selectiosSquare would have
        // area 0 and selection would fail -> fix that
        if (this->selectionHandling.currentSelection.getArea() == 0)
        {
            this->selectionHandling.currentSelection.addInternalPoint(
                this->selectionHandling.currentSelection.MinEdge + irr::core::vector2di(1, 1));
        }
    }

    struct SelectionHandling
    {
        enum InputStates
        {
            E_SelectionSquare = 0,
            E_Default
        };
        irr::core::vector2di selectionStart = irr::core::vector2di(0, 0);
        aabbox2di currentSelection =
            aabbox2di(irr::core::vector2di(0, 0)); // while the mouse is not moving but the button
                                                   // is still pressed the square should still be
                                                   // drawn -> need to cache the last drawn square
        InputStates inputState = InputStates::E_Default;
    } selectionHandling;

   private:
    std::function<void(const aabbox2di&)> m_onSelectionDone;
};
