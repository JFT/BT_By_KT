/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inputContext.hpp"

InputContext::InputContext(const InputContextTypes type)
    : m_type(type)
{
}

InputContext::EventState_t InputContext::handleInputEvent(const irr::SEvent& event)
{
    auto it = actionMap.find(generateUniqueEventValue(event));
    if (it != actionMap.end())
    {
        return it->second(event);
    }
    return {false, true};
}

void InputContext::setInputAction(uniqueEventValue_t eventValue, InputContext::Action_t action)
{
    actionMap[eventValue] = action;
}

uniqueEventValue_t InputContext::generateUniqueEventValue(const irr::SEvent& event)
{
    uniqueEventValue_t retVal = event.EventType << 10;

#pragma GCC diagnostic ignored "-Wswitch"
#pragma GCC diagnostic ignored "-Wswitch-enum"
    switch (event.EventType)
    {
        case irr::EET_MOUSE_INPUT_EVENT:
        {
            retVal |= event.MouseInput.Event;
        }
        break;
        case irr::EET_KEY_INPUT_EVENT:
        {
            // 256 KEYS!! --> 8 bits
            retVal |= event.KeyInput.Key;
        }
        break;
        /// TODO: add more cases if we need to handle other events
        default:
            retVal |= event.EventType; // just add eventtype at the end again ;)
            break;
    }
#pragma GCC diagnostic pop
#pragma GCC diagnostic pop

    return retVal;
}

uniqueEventValue_t InputContext::generateUniqueEventValue(const irr::EEVENT_TYPE eventType, const irr::c8 eventDescriptor)
{
    return (eventType << 10) | eventDescriptor;
}
