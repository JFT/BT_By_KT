/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "networkComponentSyncronizationServer.h"
#include "componentSerializer.h"
#include "../network/allocationIDs.h"
#include "../network/connectionbt.h"
#include "../network/replicamanager3bt.h"
#include "../utils/Pow2Assert.h"
#include "../utils/debug.h"
#include "../utils/static/error.h"


NetworkComponentSyncronizationServer::NetworkComponentSyncronizationServer(
    ReplicaManager3BT* const replicaManager_,
    EntityManager& entityManager_,
    Handles::HandleManager& handleManager_,
    const std::vector<IComponentSerializerBase*> componentSerializers_,
    ThreadPool& threadPool_,
    EventManager& eventManager_,
    const size_t numberOfPlayers)
    : INetworkComponentSyncronization(entityManager_, handleManager_)
    , SystemBase(Systems::SystemIdentifiers::NetworkComponentSyncronizationServer, threadPool_, eventManager_)
    , replicaManager3BT(replicaManager_)
    , deserializedComponentBuffer(std::vector<Serialization::Buffer_t>(numberOfPlayers))
    , componentSerializers(componentSerializers_)
    , trackedEntities()
    , serializationLevels(std::vector<std::pair<Components::NetworkPriority::BasePriority, bool>>(
          numberOfPlayers, std::make_pair(Components::NetworkPriority::BasePriority::High, false)))
{
    this->replicaManager3BT->Reference(this);
    this->boundComponents.push_back(Components::ComponentID<Components::NetworkPriority>::CID);
}

NetworkComponentSyncronizationServer::~NetworkComponentSyncronizationServer()
{
    // this->replicaManager3BT->Dereference(this); dereference is called in replica3 destructor
}

void NetworkComponentSyncronizationServer::trackEntity(const entityID_t entityID,
                                                       const componentID_t /* internalCompomentID */,
                                                       const Handles::Handle handle)
{
    const size_t priority =
        static_cast<size_t>(this->handleManager.getAsValue<Components::NetworkPriority>(handle).priority);
    this->trackedEntities[priority][entityID] = handle;
}

void NetworkComponentSyncronizationServer::untrackEntity(const entityID_t entityID, const componentID_t /* internalCompomentID */)
{
    POW2_ASSERT(this->entityManager.hasComponent<Components::NetworkPriority>(entityID));
    for (auto& trackedPriorities : this->trackedEntities)
    {
        trackedPriorities.erase(entityID);
    }
}

void NetworkComponentSyncronizationServer::update(const uint32_t /* tickNumber */,
                                                  ScriptEngine& /* scriptEngine */,
                                                  const float /* deltaTime */)
{
    using PR = Components::NetworkPriority::BasePriority;
    switch (this->currentSerializationLevel)
    {
        case PR::Low:
            this->currentSerializationLevel = PR::Medium;
            break;
        case PR::Medium:
            this->currentSerializationLevel = PR::High;
            break;
        case PR::High:
            this->currentSerializationLevel = PR::Low;
            break;
        case PR::NumPriorities:
            this->currentSerializationLevel = PR::Low;
            POW2_ASSERT(false);
    }

    for (auto& serializationLevel : this->serializationLevels)
    {
        serializationLevel.second = false;
    }
}

RakNet::RM3QuerySerializationResult
NetworkComponentSyncronizationServer::QuerySerialization(RakNet::Connection_RM3* destinationConnection)
{
    const size_t playerID = reinterpret_cast<ConnectionBT*>(destinationConnection)->getPlayerID();

    const bool levelsMatch = this->currentSerializationLevel == this->serializationLevels[playerID].first;
    const bool serializedThisLevel = this->serializationLevels[playerID].second;

    if (levelsMatch && serializedThisLevel)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 4,
                      "not serializing components for player",
                      playerID,
                      "because they have already been synced");
        return RakNet::RM3QuerySerializationResult::RM3QSR_DO_NOT_CALL_SERIALIZE;
    }
    debugOutLevel(Debug::DebugLevels::updateLoop + 4, "must serialize components for player", playerID);
    return RakNet::RM3QuerySerializationResult::RM3QSR_CALL_SERIALIZE;
}

RakNet::RM3SerializationResult
NetworkComponentSyncronizationServer::Serialize(RakNet::SerializeParameters* serializeParameters)
{
    const size_t playerID =
        reinterpret_cast<ConnectionBT*>(serializeParameters->destinationConnection)->getPlayerID();

    using prio = Components::NetworkPriority::BasePriority;
    debugOutLevel(Debug::DebugLevels::updateLoop + 3,
                  "trying to serialize components for player",
                  playerID,
                  "at priority",
                  static_cast<size_t>(this->currentSerializationLevel));
    switch (this->currentSerializationLevel)
    {
        // using fall-through in switch statement to serialize high: always, medium: only every 2nd
        // tick, low: only every 3rd tick
        case prio::Low:
            this->serializeComponents(playerID, prio::Low, serializeParameters->outputBitstream);
        case prio::Medium:
            this->serializeComponents(playerID, prio::Medium, serializeParameters->outputBitstream);
        case prio::High:
            this->serializeComponents(playerID, prio::High, serializeParameters->outputBitstream);
            break;
        case prio::NumPriorities:
            Error::errTerminate("called currentSerializationLevel should never be NumPriorities");
    }

    this->serializationLevels[playerID].first = this->currentSerializationLevel;
    this->serializationLevels[playerID].second = true;

    if (serializeParameters->outputBitstream->GetNumberOfBytesUsed() > 0)
    {
        return RakNet::RM3SerializationResult::RM3SR_SERIALIZED_UNIQUELY;
    }
    else
    {
        return RakNet::RM3SerializationResult::RM3SR_DO_NOT_SERIALIZE;
    }
}

void NetworkComponentSyncronizationServer::Deserialize(RakNet::DeserializeParameters* deserializeParameters)
{
    DEBUG_ONLY_(deserializeParameters->serializationBitstream->AssertStreamEmpty());
}


void NetworkComponentSyncronizationServer::serializeComponents(const size_t playerID,
                                                               const Components::NetworkPriority::BasePriority priority,
                                                               RakNet::BitStream* const outputBitstream)
{
    std::vector<Serialization::Buffer_t> tmpBuffer;
    std::vector<entityID_t> entitiesToSerialize;
    for (auto& serializer : this->componentSerializers)
    {
        for (const auto& entity : this->trackedEntities[static_cast<size_t>(priority)])
        {
            // TODO: apply priority modifiers (e.g. position of the players screen)
            const auto prio =
                this->handleManager.getAsValue<Components::NetworkPriority>(entity.second).priority;

            if (prio == priority and this->entityManager.hasComponent(entity.first, serializer->getComponentID()))
            {
                entitiesToSerialize.push_back(entity.first);
            }
        }

        if (not entitiesToSerialize.empty())
        {
            outputBitstream->Write(static_cast<Serialization::Buffer_t>(SerializationVariant_e::SingleComponent));
            Serialization::serialize<componentID_t>(tmpBuffer, serializer->getComponentID());
            Serialization::serialize<entityID_t>(tmpBuffer, entitiesToSerialize.size());
            for (const auto& entity : entitiesToSerialize)
            {
                Serialization::serialize<entityID_t>(tmpBuffer, entity);
                serializer->serializeForEntity(tmpBuffer, entity, this->entityManager, this->handleManager);
            }
            outputBitstream->WriteAlignedBytes(Serialization::ComponentSerialization::Networking::toNetworkDataType(
                                                   &tmpBuffer[0]),
                                               static_cast<unsigned int>(tmpBuffer.size()));
        }
        debugOutLevel(Debug::DebugLevels::updateLoop + 3,
                      "serialized",
                      tmpBuffer.size(),
                      "bytes for component",
                      static_cast<size_t>(serializer->getComponentID()));
        tmpBuffer.clear();
        entitiesToSerialize.clear();
    }
}
