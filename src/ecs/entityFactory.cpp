/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "entityFactory.h"

#include <fstream>
#include <limits> // std::numeric_limits<T>
#include "IComponentParser.h"
#include "componentJsonParsing.h"
#include "entityManager.h"
#include "../utils/debug.h"
#include "../utils/json.h"
#include "../utils/static/error.h"
#include "../utils/static/repr.h"

void EntityFactory::createBlueprintsFromJson(
    const std::string filename,
    EntityManager& entityManager,
    const std::map<componentID_t, ComponentParsing::IComponentParser*>& parsers,
    std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection)
{
    std::ifstream file;
    file.open(filename.c_str());
    if (!file.is_open())
    {
        throw std::ios_base::failure("Couldn't open file '" + filename + "'");
    }

    Json::Value p;
    file >> p;

    for (const auto blueprintTypeName : p.getMemberNames())
    {
        const auto blueprintType = this->getBlueprintType(blueprintTypeName);
        for (const auto blueprintName : p[blueprintTypeName].getMemberNames())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop, "blueprint name", blueprintName);
            EntityBlueprint blueprint(blueprintName, entityManager);

            const auto componentNames = p[blueprintTypeName][blueprintName].getMemberNames();
            for (const auto componentName : componentNames)
            {
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 1,
                              "found component",
                              repr<std::string>(componentName));
                const auto CID = entityManager.getComponentIDFromName(componentName);
                if (CID == ECS::INVALID_COMPONENT)
                {
                    Error::errContinue("unknown component", repr<std::string>(componentName));
                    continue;
                }
                auto parser = parsers.find(CID);
                if (parser == parsers.end())
                {
                    Error::errContinue("no json parser for component", componentName, "ID =", CID, "supplied!");
                    continue;
                }

                const auto handle = entityManager.createFreeValue(CID);
                try
                {
                    // if parseFromJson return false, we can free the handle again
                    if (parser->second->parseFromJson(p[blueprintTypeName][blueprintName][componentName], handle))
                    {
                        blueprint.addComponent(CID, handle);
                    }
                    else
                    {
                        entityManager.destroyFreeValue(CID, handle);
                        blueprint.addComponent(CID, Handles::InvalidHandle);
                    }
                }
                catch (const std::exception& ex)
                {
                    Error::errContinue("error parsing component",
                                       repr<std::string>(componentName),
                                       "(CID =",
                                       CID,
                                       ") from input",
                                       repr<std::string>(
                                           p[blueprintTypeName][blueprintName][componentName].toStyledString()),
                                       "message:",
                                       ex.what());
                    entityManager.destroyFreeValue(CID, handle);
                    continue;
                }
            }

            blueprintCollection.push_back(std::make_pair(blueprint, blueprintType));
        } // for (const auto blueprintName : p[blueprintTypeName].getMemberNames())
    }     // for (const auto blueprintTypeName : p.getMemberNames())
    file.close();
}

void EntityFactory::registerBlueprintsFromJson(const std::string filename,
                                               std::map<std::string, blueprintID_t>& blueprintNameToIDMap)
{
    std::ifstream file;
    file.open(filename.c_str());
    if (!file.is_open())
    {
        throw std::ios_base::failure("Couldn't open file '" + filename + "'");
    }

    Json::Value p;
    file >> p;
    POW2_ASSERT(blueprintNameToIDMap.size() <= std::numeric_limits<uint16_t>::max());
    uint16_t blueprintID = static_cast<uint16_t>(blueprintNameToIDMap.size());
    for (const auto blueprintTypeName : p.getMemberNames())
    {
        for (const auto blueprintName : p[blueprintTypeName].getMemberNames())
        {
            if (blueprintNameToIDMap.find(blueprintName) == blueprintNameToIDMap.end())
            {
                POW2_ASSERT(blueprintNameToIDMap.size() <= std::numeric_limits<uint16_t>::max());
                blueprintID = static_cast<uint16_t>(blueprintNameToIDMap.size());
                blueprintNameToIDMap[blueprintName] = blueprintID;
                debugOutLevel(Debug::DebugLevels::firstOrderLoop, "Register new blueprint ", blueprintName, "with ID ", blueprintID);
            }
        }
    }
}

EntityBlueprint::BlueprintType EntityFactory::getBlueprintType(std::string blueprintTypeName)
{
    debugOutLevel(Debug::DebugLevels::firstOrderLoop + 1, "Loading blueprints from type", blueprintTypeName);
    auto blueprintType = EntityBlueprint::BlueprintType::Invalid;
    if (blueprintTypeName == "Tanks")
    {
        blueprintType = EntityBlueprint::BlueprintType::Tank;
    }
    else if (blueprintTypeName == "Creep")
    {
        blueprintType = EntityBlueprint::BlueprintType::Creep;
    }
    else if (blueprintTypeName == "AutomaticWeapons")
    {
        blueprintType = EntityBlueprint::BlueprintType::AutomaticWeapon;
    }
    else if (blueprintTypeName == "Projectiles")
    {
        blueprintType = EntityBlueprint::BlueprintType::Projectile;
    }
    else if (blueprintTypeName == "Shops")
    {
        blueprintType = EntityBlueprint::BlueprintType::Shop;
    }
    else if (blueprintTypeName == "DroppedItemChest")
    {
        blueprintType = EntityBlueprint::BlueprintType::DroppedItemChest;
    }
    else
    {
        Error::errContinue("Unknown blueprintTypeName '", blueprintTypeName, "' set blueprint type to invalid");
    }
    return blueprintType;
}
