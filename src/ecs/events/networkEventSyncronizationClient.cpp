/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "networkEventSyncronizationClient.h"
#include "dynamicallySizedEventSerialization.h"
#include "eventManager.h"
#include "events.h"
#include "../../network/allocationIDs.h"
#include "../../utils/debug.h"
#include "../../utils/static/error.h"


NetworkEventSyncronizationClient::NetworkEventSyncronizationClient(ReplicaManager3BT* const replicaManager_,
                                                                   EventManager& eventManager_,
                                                                   ThreadPool& threadPool_)
    : replicaManager(replicaManager_)
    , eventManager(eventManager_)
    , threadPool(threadPool_)
    // getNumberOfWorkerThreads() + 1 because the main thread also occipies a threadID
    , receivedEventsToBeSerialized(
          decltype(receivedEventsToBeSerialized)(threadPool_.getNumberOfWorkerThreads() + 1))
    , deserializedEventBuffer()
{
    constexpr bool subscribedToUnACKed = true;

    m_subscriptions.push_back(this->eventManager.subscribe<Events::TESTEVENT>(
        [this](const Events::TESTEVENT event) { this->receiveLocalEvent(event); }, subscribedToUnACKed));

    m_subscriptions.push_back(this->eventManager.subscribe<Events::TESTEVENT_DYNAMICSIZE>(
        [this](const Events::TESTEVENT_DYNAMICSIZE event) { this->receiveLocalEvent(event); }, subscribedToUnACKed));

    m_subscriptions.push_back(this->eventManager.subscribe<Events::MovementTargetChanged>(
        [this](const Events::MovementTargetChanged event) { this->receiveLocalEvent(event); }, subscribedToUnACKed));

    m_subscriptions.push_back(this->eventManager.subscribe<Events::FindPathTo>(
        [this](const Events::FindPathTo event) { this->receiveLocalEvent(event); }, subscribedToUnACKed));

    m_subscriptions.push_back(this->eventManager.subscribe<Events::TESTEVENT_DYNAMICSIZE>(
        [this](const Events::TESTEVENT_DYNAMICSIZE event) {
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "RECEIVED DYNAMIC SIZE ACKED FROM SERVER!!! with",
                          event.vecs.size(),
                          "entries:");
            for (const auto& vec : event.vecs)
            {
                debugOutLevel(Debug::DebugLevels::updateLoop, "\t", vec.X, vec.Y, vec.Z);
            }
        }));
}

NetworkEventSyncronizationClient::~NetworkEventSyncronizationClient()
{
    // this->replicaManager->Dereference(this); dereference is called in replica3 destructor
}

void NetworkEventSyncronizationClient::throwNetworkReceivedEvents(const uint32_t tickNumber)
{
    size_t numEvents = 0;
    size_t index = 0;
    while (index < this->deserializedEventBuffer.size())
    {
        // TODO: replace those asserts with runtime-checks and (maybe) gracefully disconnect from
        // the server if something goes wrong

        // make sure at least the event data header exists
        POW2_ASSERT(this->deserializedEventBuffer.size() >=
                    index + Events::Networking::NetworkEventMetaData::SizeInBuffer);

        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "trying to read event at index",
                      index,
                      "from buffer size",
                      this->deserializedEventBuffer.size(),
                      "with Events::Serialization::NetworkEventMetaData::Size =",
                      Events::Networking::NetworkEventMetaData::SizeInBuffer);
        const auto metaData = Events::Networking::deserializeMetaData(&this->deserializedEventBuffer[index]);
        // read the metadata -> advance the index the size of the metadata inside the buffer to put
        // it where serialized event begins
        index += static_cast<size_t>(Events::Networking::NetworkEventMetaData::SizeInBuffer);

        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "metadata for this event",
                      static_cast<size_t>(metaData.type),
                      static_cast<size_t>(metaData.eventDataSize));

        POW2_ASSERT(this->deserializedEventBuffer.size() >= index + metaData.eventDataSize);

        constexpr bool eventIsServerACKed = true;
        using namespace Events::Networking;
        switch (metaData.type)
        {
            case NetworkEventType<Events::TESTEVENT>::type:
            {
                Events::TESTEVENT event = Events::Serialization::deserialize<Events::TESTEVENT>(
                    &this->deserializedEventBuffer[index]);
                // make the receiving of this event visible by changing the scale of an entity
                Events::ChangeGraphicScale e;
                e.entityID = static_cast<entityID_t>(event.vec.X);
                e.newScale = event.vec;
                this->eventManager.emit(e);
            }
            break;
            case NetworkEventType<Events::TESTEVENT_DYNAMICSIZE>::type:
            {
                Events::TESTEVENT_DYNAMICSIZE e =
                    Events::Serialization::deserialize<Events::TESTEVENT_DYNAMICSIZE>(
                        &this->deserializedEventBuffer[index]);
                this->eventManager.emit(e, eventIsServerACKed);
            }
            break;
            case NetworkEventType<Events::MovementTargetChanged>::type:
            {
                auto e = Events::Serialization::deserialize<Events::MovementTargetChanged>(
                    &this->deserializedEventBuffer[index]);
                this->eventManager.emit(e, eventIsServerACKed);
            }
            break;
            case NetworkEventType<Events::FindPathTo>::type:
            {
                auto e = Events::Serialization::deserialize<Events::FindPathTo>(
                    &this->deserializedEventBuffer[index]);
                this->eventManager.emit(e, eventIsServerACKed);
            }
            break;

            default:
                Error::errTerminate("deserialized unknown network event type",
                                    static_cast<size_t>(metaData.type));
        }
        numEvents++;
        index += static_cast<size_t>(metaData.eventDataSize);
    } // while (index < this->deserializedEventBuffer.size())

    debugOutLevel(Debug::DebugLevels::updateLoop, "update: read", numEvents, "from deserialization buffer");
    this->deserializedEventBuffer.clear();
}

void NetworkEventSyncronizationClient::WriteAllocationID(RakNet::Connection_RM3* /* destinationConnection */,
                                                         RakNet::BitStream* allocationIdBitstream) const
{
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "NetworkEventSyncronizationClient: writing allocation ID",
                  Networking::AllocationID::NetworkEventSyncronization);
    allocationIdBitstream->Write(static_cast<Networking::NetworkedAllocationIDType>(
        Networking::AllocationID::NetworkEventSyncronization));
}

void NetworkEventSyncronizationClient::SerializeConstruction(RakNet::BitStream* /* constructionBitstream */,
                                                             RakNet::Connection_RM3* /* destinationConnection */)
{
}

bool NetworkEventSyncronizationClient::DeserializeConstruction(RakNet::BitStream* /* constructionBitstream */,
                                                               RakNet::Connection_RM3* /* sourceConnection */)
{
    // construct only on client
    return not GlobalBools::systemIsServer;
}


void NetworkEventSyncronizationClient::SerializeConstructionExisting(
    RakNet::BitStream* /* constructionBitstream */, RakNet::Connection_RM3* /* destinationConnection */)
{
    POW2_ASSERT_FAIL("this object shouldn't be calling SerializeConstructionExisting because "
                     "'QueryConstruction' souldn't return 'RM3CS_ALREADY_EXISTS_REMOTELY'",
                     true);
}
void NetworkEventSyncronizationClient::DeserializeConstructionExisting(
    RakNet::BitStream* /* constructionBitstream */, RakNet::Connection_RM3* /* sourceConnection */)
{
    POW2_ASSERT_FAIL("this object shouldn't be calling DeserializeConstructionExisting because "
                     "'QueryConstruction' souldn't return 'RM3CS_ALREADY_EXISTS_REMOTELY'",
                     true);
}

RakNet::RM3SerializationResult NetworkEventSyncronizationClient::Serialize(RakNet::SerializeParameters* serializeParameters)
{
    uint32_t totalDataSize = 0;
    for (auto& threadData : this->receivedEventsToBeSerialized)
    {
        totalDataSize += static_cast<uint32_t>(threadData.size());
    }

    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "totalDataSize =",
                  totalDataSize,
                  "if 0 won't "
                  "serialize, if "
                  "!= 0 will");
    if (totalDataSize == 0)
    {
        return RakNet::RM3SerializationResult::RM3SR_DO_NOT_SERIALIZE;
    }

    serializeParameters->outputBitstream->Write(totalDataSize);
    for (auto& threadData : this->receivedEventsToBeSerialized)
    {
        if (threadData.size() > 0)
        {
            static_assert(sizeof(const unsigned char) == sizeof(threadData[0]), "threadData isn't a byte-sized-type");
            serializeParameters->outputBitstream->WriteAlignedBytes(
                Events::Networking::toNetworkDataType(&threadData[0]),
                static_cast<const unsigned int>(threadData.size()));
            threadData.clear();
        }
    }

    return RakNet::RM3SerializationResult::RM3SR_BROADCAST_IDENTICALLY_FORCE_SERIALIZATION;
}

void NetworkEventSyncronizationClient::Deserialize(RakNet::DeserializeParameters* deserializeParameters)
{
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "before deserialization for connection",
                  deserializeParameters->sourceConnection,
                  ": deserializedEventBuffer is",
                  this->deserializedEventBuffer.size(),
                  "bytes");
    // TODO: check if we are expecting events from this connection
    uint32_t totalDataSize = 0;
    if (not deserializeParameters->serializationBitstream->Read(totalDataSize))
    {
        Error::errContinue("couldn't read 'totalDataSize' from deserialization bitstream");
        return;
    }
    size_t oldSize = this->deserializedEventBuffer.size();
    // TODO: set acceptable limits which make sure the client doesn't reserve GBs of memory because
    // we fucked something up when serializing
    this->deserializedEventBuffer.resize(oldSize + totalDataSize);

    if (sizeof(uint32_t) + totalDataSize > deserializeParameters->serializationBitstream->GetNumberOfBytesUsed())
    {
        Error::errContinue("the remote said there will be",
                           totalDataSize,
                           "bytes of events but the received data is only",
                           deserializeParameters->serializationBitstream->GetNumberOfBytesUsed(),
                           "bytes in the stream (",
                           sizeof(uint32_t),
                           "where used for the length of the data)");
        // new space contains invalid data
        this->deserializedEventBuffer.resize(oldSize);
        return;
    }

    // writing new data starts at 'oldSize' which is the first index of the now created free space
    static_assert(sizeof(const unsigned char) == sizeof(this->deserializedEventBuffer[0]),
                  "threadData isn't a byte-sized-type");
    if (not deserializeParameters->serializationBitstream->ReadAlignedBytes(
            Events::Networking::toNetworkDataType(&this->deserializedEventBuffer[oldSize]), totalDataSize))
    {
        Error::errContinue("couldn't read event bytes out of the deserialization bitstream");
        // new space contains invalid data
        this->deserializedEventBuffer.resize(oldSize);
        return;
    }

    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "deserialized",
                  totalDataSize,
                  "bytes. New deserializedEventBuffer is",
                  this->deserializedEventBuffer.size(),
                  "bytes");

    DEBUG_ONLY_(deserializeParameters->serializationBitstream->AssertStreamEmpty());
}


RakNet::RM3QuerySerializationResult
NetworkEventSyncronizationClient::QuerySerialization(RakNet::Connection_RM3* /* destinationConnection */)
{
    for (auto& threatData : this->receivedEventsToBeSerialized)
    {
        if (threatData.size() > 0)
        {
            return RakNet::RM3QuerySerializationResult::RM3QSR_CALL_SERIALIZE;
        }
    }
    return RakNet::RM3QuerySerializationResult::RM3QSR_DO_NOT_CALL_SERIALIZE;
}
