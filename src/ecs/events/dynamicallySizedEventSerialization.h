/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DYNAMICALLYSIZEDEVENTSERIALIZATION
#define DYNAMICALLYSIZEDEVENTSERIALIZATION

#include "eventSerialization.h"
#include "events.h"

/// Serialization template specializations for dynamically sized events
/// Most events (if they are a POD-type) can be serialized without any special handling.
/// But events which contain dynamically-sized data (like vectors, lists) need special care.
///
/// Different event types are often serialized into the same continious buffer.
/// For POD-type events the size of the serialized event doesn't change
/// and thus doesn't need to be copied around (only the type is stored before the actual event data)
/// or waste space inside buffers.
/// But the deserialization routine for a dynamically-sized event must know when to stop reading
/// the buffer because the event is fully read.
/// This means the dynamic size must be transmitted with the event.
///
/// This file provides template specializations for dynamically sized events.
///
/// For an event with dynamically-sized data to be handled correctly 4 function templates must be
/// specialized:
/// 1) SerializedEventSize(const EventType&)
///     Size this event will have if it is serialized (in bytes). Must include storing the size
///     itself!
/// 2) serialize(const EventType& event, SerializedEventBuffer_t* targetBuffer)
///     The actual function which serializes the event.
///     This MUST also serialize the 'SerializedEventSize' to a position where it can be read by
///     'deserializeEventSize()'
/// 3) deserializeEventSize(const SerializedEventBuffer_t* const buffer)
///     Deserializes the size (in bytes) of the dynamically-sized event.
///     Even if the event isn't deserialized this can be used to skip the event inside a continious
///     buffer.
/// 4) deserialize(const SerializedEventBuffer_t* const buffer)
///     The actual deserialization routine for the event.
///     Note that 'buffer' is the same as supplied to deserializeEventSize()
///     and can be used to get the size of the event inside deserialize()

namespace Events
{
    using ES = Serialization;

    // -----------------------------------------------------------------
    // TESTEVENT_DYNAMICSIZE
    // -----------------------------------------------------------------

    template <>
    ES::SerializedEventSize_t ES::SerializedEventSize(const TESTEVENT_DYNAMICSIZE& event)
    {
        return static_cast<SerializedEventSize_t>(
            sizeof(SerializedEventSize_t) +
            event.vecs.size() * sizeof(decltype(TESTEVENT_DYNAMICSIZE::vecs)::value_type));
    }

    template <>
    void ES::serialize<TESTEVENT_DYNAMICSIZE>(const TESTEVENT_DYNAMICSIZE& event, SerializedEventBuffer_t* targetBuffer)
    {
        *reinterpret_cast<SerializedEventSize_t*>(targetBuffer) = SerializedEventSize(event);
        targetBuffer += sizeof(SerializedEventSize_t);
        for (const auto& vec : event.vecs)
        {
            *reinterpret_cast<decltype(TESTEVENT_DYNAMICSIZE::vecs)::value_type*>(targetBuffer) = vec;
            targetBuffer += sizeof(decltype(TESTEVENT_DYNAMICSIZE::vecs)::value_type);
        }
    }

    template <>
    std::size_t ES::deserializeEventSize<TESTEVENT_DYNAMICSIZE>(const SerializedEventBuffer_t* const buffer)
    {
        return *reinterpret_cast<const SerializedEventSize_t* const>(buffer);
    }

    template <>
    TESTEVENT_DYNAMICSIZE ES::deserialize(const Serialization::SerializedEventBuffer_t* buffer)
    {
        // the serialized event size contains
        // 1) sizeof(SerializedEventBuffer_t) for the size itself
        // 2) sizeof(value_type) * #elements
        const std::size_t serializedSize = ES::deserializeEventSize<TESTEVENT_DYNAMICSIZE>(buffer);
        const std::size_t numberOfElements = (serializedSize - sizeof(SerializedEventBuffer_t)) /
            sizeof(decltype(TESTEVENT_DYNAMICSIZE::vecs)::value_type);
        buffer += sizeof(SerializedEventSize_t);
        TESTEVENT_DYNAMICSIZE event;
        for (size_t i = 0; i < numberOfElements; i++)
        {
            event.vecs.emplace_back(
                *reinterpret_cast<const decltype(TESTEVENT_DYNAMICSIZE::vecs)::value_type* const>(buffer));
            buffer += sizeof(decltype(TESTEVENT_DYNAMICSIZE::vecs)::value_type);
        }
        return event;
    }
} // namespace Events

#endif /* ifndef DYNAMICALLYSIZEDEVENTSERIALIZATION */
