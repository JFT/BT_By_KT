/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef EVENTMAILBOX_H
#define EVENTMAILBOX_H

#include <vector>
#include "eventSerialization.h"
#include "../../utils/static/error.h"

// forward declarations
class ThreadPool;

/// @brief class which can be used by systems to easily receive multiple threaded events and read
/// them out linearly.
/// Abstracts the buffer resizing and addressing away.
class EventMailbox
{
   public:
    /// @brief represents the location of a single event inside the internal received event buffer
    /// single events should be accessed using AccessPoints because they are automatically modified
    /// to point towards the next event
    struct AccessPoint
    {
        std::vector<size_t> pos = {};
    };

    explicit EventMailbox(ThreadPool& threadPool);

    /// @brief receive an event (can be called from multiple threads in parallel) and stores it
    /// inside an internal buffer for later retrival
    /// @tparam EventType
    /// @param type an arbitrary type indicator for the event (must fit into Events::EventID_t).
    /// Will later be returned when getNextEventType() is called
    /// @param event
    template <typename EventType>
    void receiveThreadedEvent(const Events::EventID_t type, const EventType& event)
    {
        auto* const buffer = this->reserveBuffer(Events::Serialization::NeededLocalBufferSize(event));
        Events::Serialization::serializeEventAndType(type, event, buffer);
    }

    /// @brief return an AccessPoint which points to the first event inside the internal buffer
    /// @return
    AccessPoint getAccessPoint() const;

    /// @brief read the next event type
    /// getNextEventType() and readNextEvent() are designed to be called after one-another
    /// @param accessPoint where inside the internal buffer to read the event type from.
    /// MUST be either
    /// 1) an AccessPoint returned by getAccessPoint() or
    /// 2) an AccessPoint with which readNextEvent() was called last.
    /// Otherwise OOB-reads will occur!
    /// @return the type of the next event at accessPoint (as saved by receiveThreadedEvent()).
    /// Events::InvalidEventType if there are no more events in the buffer.
    Events::EventID_t getNextEventType(AccessPoint& accessPoint);

    /// @brief read the internal buffer at accessPoint as an event of type EventType
    /// getNextEventType() and readNextEvent() are designed to be called after one-another
    /// @tparam EventType event will contain garbage data if EventType isn't the same as the one
    /// used in receiveThreadedEvent. Use the type returned by getNextEventType() to determine which
    /// EventType to use.
    /// @param accessPoint where inside the internal buffer to read the event type from.
    /// MUST be an AccessPoint modified by getNextEventType()
    /// @return the deserialized event ouf of the buffer.
    template <typename EventType>
    EventType readNextEvent(AccessPoint& accessPoint)
    {
#if defined(MAKE_DEBUG_)
        if (this->calledGetNextEventType == false)
        {
            Error::errTerminate(
                "getNextEventType() and readNextEvent() must be called alternating!");
        }

        this->calledGetNextEventType = false;
#endif

        POW2_ASSERT(accessPoint.pos.size() == this->storedEvents.size());
        for (size_t threadIndex = 0; threadIndex < this->storedEvents.size(); ++threadIndex)
        {
            if (accessPoint.pos[threadIndex] < this->storedEvents[threadIndex].size())
            {
                const std::size_t serializedEventSize = Events::Serialization::deserializeEventSize<EventType>(
                    &this->storedEvents[threadIndex][accessPoint.pos[threadIndex]]);
                POW2_ASSERT(accessPoint.pos[threadIndex] + serializedEventSize <=
                            this->storedEvents[threadIndex].size());
                const EventType event = Events::Serialization::deserialize<EventType>(
                    &this->storedEvents[threadIndex][accessPoint.pos[threadIndex]]);
                accessPoint.pos[threadIndex] += serializedEventSize;
                return event;
            }
        }
        Error::errTerminate(
            "readNextEvent called but the supplied accessPoint points past all buffers!");
    }

    /// @brief clear all internal buffers. Use if all received events have been handled.
    void clear();

   private:
    friend class TrivialSystemSaver;

    ThreadPool& threadPool;

    /// @brief byte array where received events are stored
    /// one std::vector for each possible event sending thread
    /// each of the std::vectors in storedEvents stores events in this layout
    /// [event_1_ID,
    ///   event_1_data,   ^
    ///   event_1_data,   |
    ///       .           |
    ///       .           |} size of serialized event(event_1) bytes of data
    ///       .           |
    ///       .           |
    ///   evert_1_data,   v
    ///   event_2_ID,
    ///   event_2_data,
    ///        .
    ///        .
    ///        .
    ///    event_N_ID,
    ///       .
    ///       .
    ///  event_N_data]
    std::vector<std::vector<Events::Serialization::SerializedEventBuffer_t>> storedEvents;

    /// @brief reserve space of the internal buffer for the thread calling this function
    /// resizes the buffer if neccecary.
    /// @param size how many 'Events::Serialization::SerializedEventBuffer_t' to reserver
    /// @return pointer to the beginning of the newly reserved space.
    Events::Serialization::SerializedEventBuffer_t* reserveBuffer(const std::size_t size);

#if defined(MAKE_DEBUG_)
    /// @brief debug bool used to test if getNextEventType() and readNextEvent() are called
    /// alternatingly
    bool calledGetNextEventType = false;
#endif
};

#endif /* ifndef EVENTMAILBOX_H */
