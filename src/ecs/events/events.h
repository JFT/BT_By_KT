/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>

#include "../components.h"
#include "../entity.h"            // entityID_t
#include "../../grid/gridTypes.h" // grid::gridCellID_t
#include "../../utils/aabbox2d.h" // needed for the selection square

namespace Events
{
    struct TESTEVENT
    {
        irr::core::vector3df vec;
    };

    struct TESTEVENT_DYNAMICSIZE
    {
        std::vector<irr::core::vector3df> vecs;
    };

    struct FindPathTo
    {
        entityID_t entityID;
        irr::core::vector2df target;
    };

    struct MovementTargetChanged
    {
        entityID_t entityID;
        irr::core::vector2df vec;
    };

    struct TESTEVENT_OTHER
    {
        float targetQueue;
    };

    struct bladiblablustr
    {
        int targetQueue;
    };

    struct SelectionSquare
    {
        explicit SelectionSquare(const aabbox2di box_)
            : box(box_)
        {
        }

        SelectionSquare(const irr::core::vector2di minEdge, const irr::core::vector2di maxEdge)
            : box(aabbox2di(minEdge, maxEdge))
        {
        }

        aabbox2di box;
    };

    struct HighlightSquare
    {
        // a copy of 'SelectionSquare' with a different name to differentiate highlighting
        // (selection box without letting go of the mouse button) vs. actual selection
        explicit HighlightSquare(const aabbox2di box_)
            : box(box_)
        {
        }

        HighlightSquare(const irr::core::vector2di minEdge, const irr::core::vector2di maxEdge)
            : box(aabbox2di(minEdge, maxEdge))
        {
        }

        aabbox2di box;
    };

    struct PartialEntitySelection
    {
        PartialEntitySelection(const entityID_t entityID_, const irr::f32 selectionPercent_)
            : entityID(entityID_)
            , selectionPercent(selectionPercent_)
        {
        }

        entityID_t entityID;
        irr::f32 selectionPercent;
    };

    struct OwnedEntitySelected
    {
        explicit OwnedEntitySelected(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }
        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct NotOwnedEntitySelected
    {
        explicit NotOwnedEntitySelected(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }
        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct EntityDeselected
    {
        // TODO check eventSerialization
        // static_assert(sizeof(EventType) > 1,
        // "event size must be at least 1 byte (because that "
        // "is assumed by various users of this)!");

        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct MouseClick
    {
        MouseClick(const float x_, const float y_)
            : x(x_)
            , y(y_)
        {
        }

        float x;
        float y;
    };

    struct ClickOnEntity
    {
        explicit ClickOnEntity(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }

        entityID_t entityID;
    };

    struct Teleport
    {
        Teleport() = default;
        Teleport(const entityID_t entityID_, const Components::Position teleportTarget_)
            : entityID(std::move(entityID_))
            , teleportTarget(std::move(teleportTarget_))
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
        Components::Position teleportTarget = Components::Position();
    };

    struct WaypointsChanged
    {
        WaypointsChanged() = default;
        explicit WaypointsChanged(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct FinalWaypointReached
    {
        FinalWaypointReached() = default;
        explicit FinalWaypointReached(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct IntermediateWaypointReached
    {
        IntermediateWaypointReached() = default;
        explicit IntermediateWaypointReached(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct RepathRequest
    {
        explicit RepathRequest(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct PositionChanged
    {
        PositionChanged() = default;
        explicit PositionChanged(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct ChangeGraphicScale
    {
        ChangeGraphicScale() = default;
        ChangeGraphicScale(const entityID_t entityID_, irr::core::vector3df scale_)
            : entityID(entityID_)
            , newScale(scale_)
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
        irr::core::vector3df newScale = irr::core::vector3df(1.0f, 1.0f, 1.0f);
    };

    struct StartCooldown
    {
        StartCooldown() = default;
        explicit StartCooldown(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct CooldownFinished
    {
        CooldownFinished() = default;
        explicit CooldownFinished(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct TakeDamage
    {
        enum DamageType
        {
            Normal
        };

        TakeDamage() = default;
        TakeDamage(const entityID_t target_, const float value_)
            : targetEntityID(std::move(target_))
            , amount(std::move(value_))
        {
        }

        entityID_t targetEntityID = ECS::INVALID_ENTITY;
        float amount = 0.0f;
        DamageType damageType = DamageType::Normal;
    };

    struct HealthBelowZero
    {
        HealthBelowZero() = default;
        explicit HealthBelowZero(const entityID_t entityID_)
            : targetEntityID(entityID_)
        {
        }

        entityID_t targetEntityID = ECS::INVALID_ENTITY;
    };

    struct TakeHeal
    {
        TakeHeal() = default;
        explicit TakeHeal(const entityID_t target_, const float value_)
            : targetEntityID(std::move(target_))
            , amount(std::move(value_))
        {
        }

        entityID_t targetEntityID = ECS::INVALID_ENTITY;
        float amount = 0.0f;
    };

    struct HealthChanged
    {
        HealthChanged() = default;
        explicit HealthChanged(const entityID_t entityID_)
            : targetEntityID(entityID_)
        {
        }

        entityID_t targetEntityID = ECS::INVALID_ENTITY;
    };

    struct ShopSelection
    {
        ShopSelection() = default;
        explicit ShopSelection(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct TryToBuyAShoptItem
    {
        TryToBuyAShoptItem() = default;
        explicit TryToBuyAShoptItem(const entityID_t buyerEntityID_, const entityID_t shopEntityID_, const blueprintID_t blueprintId_)
            : buyerEntityID(buyerEntityID_)
            , shopEntityID(shopEntityID_)
            , blueprintId(blueprintId_)
        {
        }

        entityID_t buyerEntityID = ECS::INVALID_ENTITY;
        entityID_t shopEntityID = ECS::INVALID_ENTITY;
        blueprintID_t blueprintId = ECS::INVALID_BLUEPRINT;
    };

    struct DeactivateItem
    {
        DeactivateItem() = default;
        explicit DeactivateItem(const entityID_t itemEntityID_)
            : itemEntityID(itemEntityID_)


        {
        }

        entityID_t itemEntityID = ECS::INVALID_ENTITY;
    };

    struct ReactivateItem
    {
        ReactivateItem() = default;
        explicit ReactivateItem(const entityID_t itemEntityID_)
            : itemEntityID(itemEntityID_)


        {
        }

        entityID_t itemEntityID = ECS::INVALID_ENTITY;
    };

    struct TransferMoney
    {
        TransferMoney() = default;
        TransferMoney(const entityID_t entityID_, const int16_t amount_)
            : entityID(entityID_)
            , amount(amount_)

        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
        int16_t amount = 0;
    };

    struct InventorySelection
    {
        InventorySelection() = default;
        explicit InventorySelection(const entityID_t entityID_)
            : entityID(entityID_)
        {
        }

        entityID_t entityID = ECS::INVALID_ENTITY;
    };

    struct UpdateInventoryGui
    {
        UpdateInventoryGui() = default;
        explicit UpdateInventoryGui(const entityID_t ownerEntityID_)
            : ownerEntityID(ownerEntityID_)

        {
        }

        entityID_t ownerEntityID = ECS::INVALID_ENTITY;
    };

    struct AddItemToInventory
    {
        AddItemToInventory() = default;
        AddItemToInventory(const entityID_t itemOwnerEntityID_, const entityID_t itemEntityID_)
            : itemOwnerEntityID(itemOwnerEntityID_)
            , itemEntityID(itemEntityID_)

        {
        }

        entityID_t itemOwnerEntityID = ECS::INVALID_ENTITY;
        entityID_t itemEntityID = ECS::INVALID_ENTITY;
    };

    struct RemoveItemFromInventory
    {
        RemoveItemFromInventory() = default;
        RemoveItemFromInventory(const entityID_t itemOwnerEntityID_, const entityID_t itemEntityID_)
            : itemOwnerEntityID(itemOwnerEntityID_)
            , itemEntityID(itemEntityID_)

        {
        }

        entityID_t itemOwnerEntityID = ECS::INVALID_ENTITY;
        entityID_t itemEntityID = ECS::INVALID_ENTITY;
    };

    struct DroppedItemChestSelection
    {
        DroppedItemChestSelection() = default;
        explicit DroppedItemChestSelection(const entityID_t droppedItemChestEntityID_)
            : droppedItemChestEntityID(droppedItemChestEntityID_)
        {
        }

        entityID_t droppedItemChestEntityID = ECS::INVALID_ENTITY;
    };

    struct DropItemFromInventoryToTheMap
    {
        DropItemFromInventoryToTheMap() = default;
        DropItemFromInventoryToTheMap(const entityID_t itemOwnerEntityID_,
                                      const entityID_t itemEntityID_,
                                      const irr::core::vector2df position_)
            : itemOwnerEntityID(itemOwnerEntityID_)
            , itemEntityID(itemEntityID_)
            , position(position_)
        {
        }

        entityID_t itemOwnerEntityID = ECS::INVALID_ENTITY;
        entityID_t itemEntityID = ECS::INVALID_ENTITY;
        irr::core::vector2df position = irr::core::vector2df(0.0f, 0.0f);
    };

    struct PickUpDroppedItem
    {
        PickUpDroppedItem() = default;
        explicit PickUpDroppedItem(const entityID_t droppedItemChestEntityID_)
            : droppedItemChestEntityID(droppedItemChestEntityID_)
        {
        }

        entityID_t droppedItemChestEntityID = ECS::INVALID_ENTITY;
    };
} // namespace Events
