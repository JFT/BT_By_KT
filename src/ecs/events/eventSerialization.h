/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef EVENTSERILAIZATION_H
#define EVENTSERILAIZATION_H

#include <cstdint> // (u)int8/32_t
#include <limits>
#include <type_traits> // std::is_trivially_copyable

#include "eventID.h"
#include "../../utils/Pow2Assert.h"

namespace Events
{
    struct TESTEVENT;
    struct MovementTargetChanged;
    struct SelectionSquare;
    struct HighlightSquare;
    struct PartialEntitySelection;
    struct Teleport;
    struct ChangeGraphicScale;
    struct DropItemFromInventoryToTheMap;
    struct FindPathTo;

    /// @brief some events fail std::is_trivially_copyable<EventType> but can be
    /// reinterpret_cast into a buffer anyways (e.g. if they contain irr::vector2df).
    /// This bool makes sure they can use the generic way to get serialized.
    /// For any new event which is is_trivially_copyable
    /// but for which std::is_trivially_copyable<EventType>::value == false
    /// A is_trivially_copyable_override template specialization must be created in order
    /// to use the default serialization method.
    /// @tparam EventType
    template <typename EventType>
    struct is_trivially_copyable_override
    {
        static constexpr bool value = false;
    };
    template <>
    struct is_trivially_copyable_override<TESTEVENT>
    {
        static constexpr bool value = true;
    };
    template <>
    struct is_trivially_copyable_override<MovementTargetChanged>
    {
        static constexpr bool value = true;
    };
    template <>
    struct is_trivially_copyable_override<SelectionSquare>
    {
        static constexpr bool value = false; // TODO check
    };
    template <>
    struct is_trivially_copyable_override<HighlightSquare>
    {
        static constexpr bool value = false; // TODO check
    };
    template <>
    struct is_trivially_copyable_override<Teleport>
    {
        static constexpr bool value = true;
    };
    template <>
    struct is_trivially_copyable_override<ChangeGraphicScale>
    {
        static constexpr bool value = true;
    };
    template <>
    struct is_trivially_copyable_override<DropItemFromInventoryToTheMap>
    {
        static constexpr bool value = true;
    };
    template <>
    struct is_trivially_copyable_override<FindPathTo>
    {
        static constexpr bool value = true;
    };

    /// @brief this class is used to serialize events.
    /// Either to store different events into a single buffer (e.g. done by the EventMailBox inside
    /// systems)
    /// or to transmit the over the network (see networkedEvents.h and eventNetworking.h)
    /// The functions defined here should only be used to (de)serialize trivially copyable-types.
    /// For dynamically-sized events (e.g. events containing std::vectors) see
    /// 'dynamicallySizedEventSerialization.h'
    class Serialization
    {
       public:
        typedef int8_t SerializedEventBuffer_t;
        typedef uint32_t SerializedEventSize_t;

        template <typename EventType>
        static SerializedEventSize_t SerializedEventSize(const EventType&)
        {
            static_assert(std::is_trivially_copyable<EventType>::value or
                              is_trivially_copyable_override<EventType>::value,
                          "Supplied EventType isn't trivially copyable. Inclide "
                          "ecs/events/dynamicallySizedEventSerialization.h!");
            return sizeof(EventType);
        }

        template <typename EventType>
        static void serialize(const EventType& event, SerializedEventBuffer_t* targetBuffer)
        {
            static_assert(std::is_trivially_copyable<EventType>::value or
                              is_trivially_copyable_override<EventType>::value,
                          "Supplied EventType isn't trivially copyable. Inclide "
                          "ecs/events/dynamicallySizedEventSerialization.h!");
            *reinterpret_cast<EventType* const>(targetBuffer) = event;
        }

        template <typename EventType>
        static std::size_t deserializeEventSize(const SerializedEventBuffer_t*)
        {
            static_assert(std::is_trivially_copyable<EventType>::value or
                              is_trivially_copyable_override<EventType>::value,
                          "Supplied EventType isn't trivially copyable. Inclide "
                          "ecs/events/dynamicallySizedEventSerialization.h!");
            static_assert(sizeof(EventType) > 1,
                          "event size must be at least 1 byte (because that "
                          "is assumed by various users of this)!");
            return sizeof(EventType);
        }

        template <typename EventType>
        static EventType deserialize(const SerializedEventBuffer_t* buffer)
        {
            static_assert(std::is_trivially_copyable<EventType>::value or
                              is_trivially_copyable_override<EventType>::value,
                          "Supplied EventType isn't trivially copyable. Inclide "
                          "ecs/events/dynamicallySizedEventSerialization.h!");
            return *reinterpret_cast<const EventType*>(buffer);
        }

        // ---------------------------------------------------------------------------
        // here begin functions used by all events, trivially copyable-types and dynamically sized.
        // ---------------------------------------------------------------------------

        // 1 byte: local event type, the rest for the actual event
        template <typename EventType>
        static std::size_t NeededLocalBufferSize(const EventType& event)
        {
            static_assert(std::is_trivially_copyable<EventType>::value or
                              is_trivially_copyable_override<EventType>::value,
                          "Supplied EventType isn't trivially copyable. Inclide "
                          "ecs/events/dynamicallySizedEventSerialization.h!");
            return 1 + SerializedEventSize<EventType>(event);
        }

        static Events::EventID_t deserializeType(const SerializedEventBuffer_t* buffer)
        {
            return static_cast<Events::EventID_t>(*buffer);
        }

        template <typename EventType>
        static void
        serializeEventAndType(const EventID_t type, const EventType& event, SerializedEventBuffer_t* targetBuffer)
        {
            static_assert(std::is_trivially_copyable<EventType>::value or
                              is_trivially_copyable_override<EventType>::value,
                          "Supplied EventType isn't trivially copyable. Inclide "
                          "ecs/events/dynamicallySizedEventSerialization.h!");
            POW2_ASSERT(type <= std::numeric_limits<SerializedEventBuffer_t>::max());
            // write the type into the buffer
            *targetBuffer = static_cast<SerializedEventBuffer_t>(type);
            Serialization::serialize(event, targetBuffer + 1);
        }
    };

} // namespace Events

#endif /* ifndef EVENTSERILAIZATION_H */
