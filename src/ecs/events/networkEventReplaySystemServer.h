
/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2018 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef NETWORKEVENTREPLAYSERVER
#define NETWORKEVENTREPLAYSERVER

#include "networkEventSyncronizationServer.h"
#include "../../utils/serializationTypes.h"
#include "../../utils/threadedIO.h"

class NetworkEventReplayRecorderServer : public NetworkEventSyncronizationServer
{
   public:
    NetworkEventReplayRecorderServer(std::unique_ptr<std::ofstream>& file,
                                     const uint32_t numberOfPlayers,
                                     ReplicaManager3BT* const replicaManager_,
                                     EventManager& eventManager_,
                                     ThreadPool& threadPool_)
        : NetworkEventSyncronizationServer(numberOfPlayers, replicaManager_, eventManager_, threadPool_)
    {
        this->writer = std::make_unique<ThreadedIoWriter<Serialization::Buffer_t>>(file);
    }

    virtual void throwNetworkReceivedEvents(const uint32_t tickNumber);

   private:
    std::unique_ptr<ThreadedIoWriter<Serialization::Buffer_t>> writer = nullptr;
};

class NetworkEventReplayPlayerServer : public NetworkEventSyncronizationServer
{
   public:
    NetworkEventReplayPlayerServer(std::unique_ptr<std::ifstream>& file,
                                   const uint32_t numberOfPlayers,
                                   ReplicaManager3BT* const replicaManager_,
                                   EventManager& eventManager_,
                                   ThreadPool& threadPool_)
        : NetworkEventSyncronizationServer(numberOfPlayers, replicaManager_, eventManager_, threadPool_)
    {
        this->reader = std::make_unique<ThreadedIoReader<Serialization::Buffer_t>>(file);
    }

    virtual void throwNetworkReceivedEvents(const uint32_t tickNumber);

    virtual void Deserialize(RakNet::DeserializeParameters* deserializeParameters) {}

   private:
    std::unique_ptr<ThreadedIoReader<Serialization::Buffer_t>> reader = nullptr;
};

#endif // NETWORKEVENTREPLAYSERVER
