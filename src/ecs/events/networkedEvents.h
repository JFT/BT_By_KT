/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef NETWORKEDEVENTS_H
#define NETWORKEDEVENTS_H

// this file provides a stable id for events so they can be syncronized over the network
// to add a new networked event the following things must be done
// 1) Forward declaration of the event
// 2) Adding a new template specialization of NetworkEventType<EventType> which defines the
// networkID of the event and whether the event must first be acknowledged by the server before the
// client can emit it.
// 3) adding a definition of the NetworkEventType to networkedEvents.cpp. (TODO:this isn't neccecray
// for c++17. If we ever update make sure to remove this comment and the networkedEvents.cpp file)
// Also make sure the event is serialized/deserialized correctly (see eventSerialization.h and
// dynamicallySizedEventSerialization.h)

#include "events.h"

namespace Events
{
    // forward declaration of all networked events
    // struct TESTEVENT;
    // struct TESTEVENT_DYNAMICSIZE;

    namespace Networking
    {
        /// @brief Type which defines a stable id for different events so they can be syncronized
        /// between client <-> server
        /// and which also sets if an event needs to be greenlit by the server or if the client can
        /// send the event to itself.
        /// @tparam EventType
        template <typename EventType>
        struct NetworkEventType
        {
            /// @brief The event types are generated at runtime and depend on the order an event is
            /// used with the eventHandler (e.g. subscribed to or emitted).
            /// Thus they can't be send over the network because they
            /// - can differ between server and client (if the server subscribes to events in a
            /// different order)
            /// - can differ between versions (if e.g. a patch makes a system subscribe to events in
            /// a different order or adds a subscrption)
            /// The following provides a stable interface to map between event types as seen by the
            /// EventHandler (which change) and as seen by the network (which shouldn't change)
            /// This type MUST be usique between all explicitly declared NetworkEventTypes!
            static constexpr std::size_t type = static_cast<std::size_t>(-1);
            /// @brief bool which tells the EventManager if an event needs to be greenlit be the
            /// server or if it can be send to any subscribing system on the client itself without
            /// asking the server. Defaults to false and needs to be overwritten explicitly for
            /// certain events.
            static constexpr bool needsServerACK = false;
        };

        template <typename EventType>
        constexpr bool NetworkEventType<EventType>::needsServerACK;

        template <>
        struct NetworkEventType<TESTEVENT>
        {
            static constexpr std::size_t type = static_cast<std::size_t>(0);
            static constexpr bool needsServerACK = true;
        };

        template <>
        struct NetworkEventType<TESTEVENT_DYNAMICSIZE>
        {
            static constexpr std::size_t type = NetworkEventType<TESTEVENT>::type + 1;
            static constexpr bool needsServerACK = true;
        };

        template <>
        struct NetworkEventType<FindPathTo>
        {
            static constexpr std::size_t type = NetworkEventType<TESTEVENT_DYNAMICSIZE>::type + 1;
            static constexpr bool needsServerACK = true;
        };

        template <>
        struct NetworkEventType<MovementTargetChanged>
        {
            static constexpr std::size_t type = NetworkEventType<FindPathTo>::type + 1;
            static constexpr bool needsServerACK = true;
        };

    } // namespace Networking
} // namespace Events

#endif /* ifndef NETWORKEDEVENTS_H */
