#include "networkEventReplaySystemServer.h"

#include <numeric>
#include "../../utils/serialization.h"

void NetworkEventReplayRecorderServer::throwNetworkReceivedEvents(const uint32_t tickNumber)
{
    // simple intercept -> save data and use base class for everything else
    std::vector<Serialization::Buffer_t> data;
    Serialization::serialize(data, tickNumber);

    const auto sizeAt = data.size();
    Serialization::serialize(data, uint32_t(0));
    Serialization::serialize(data, this->deserializedEventBuffers);
    POW2_ASSERT(data.size() > sizeAt + sizeof(uint32_t));
    Serialization::serializeAt<uint32_t>(data, sizeAt, static_cast<uint32_t>(data.size() - sizeAt - sizeof(uint32_t)));

    this->writer->write(data);
    NetworkEventSyncronizationServer::throwNetworkReceivedEvents(tickNumber);
}

void NetworkEventReplayPlayerServer::throwNetworkReceivedEvents(const uint32_t tickNumber)
{
    size_t readingFrom = 0;
    const auto savedTick =
        Serialization::deserialize<decltype(tickNumber)>(this->reader->read(sizeof(uint32_t)), readingFrom);
    readingFrom = 0;

    POW2_ASSERT(savedTick == tickNumber);

    const auto eventBufferSize =
        Serialization::deserialize<uint32_t>(this->reader->read(sizeof(uint32_t)), readingFrom);

    const auto data = this->reader->read(eventBufferSize);

    // restore buffers and use the normal NetworkEventSyncronizationServer to do all the
    // evaluation/even throwing
    readingFrom = 0;
    this->deserializedEventBuffers =
        Serialization::deserialize<decltype(this->deserializedEventBuffers)>(data, readingFrom);

    NetworkEventSyncronizationServer::throwNetworkReceivedEvents(tickNumber);
}
