/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "networkEventSyncronizationClientStub.h"
#include "eventMailbox.h"
#include "eventManager.h"
#include "events.h"
#include "../../utils/debug.h"


NetworkEventSyncronizationClientStub::NetworkEventSyncronizationClientStub(EventManager& eventManager_,
                                                                           ThreadPool& threadPool_)
    : eventManager(eventManager_)
    , receivedEvents(*(new EventMailbox(threadPool_)))
{
    m_subscriptions.push_back(this->eventManager.subscribe<Events::TESTEVENT>(
        [this](const Events::TESTEVENT event) {
            this->receivedEvents.receiveThreadedEvent(InternalEventTypes::TESTEVENT, event);
        },
        true));
}


NetworkEventSyncronizationClientStub::~NetworkEventSyncronizationClientStub()
{
    delete &this->receivedEvents;
}

void NetworkEventSyncronizationClientStub::throwServerAckedEvents()
{
    EventMailbox::AccessPoint ap = this->receivedEvents.getAccessPoint();

    size_t numEvents = 0;

    for (Events::EventID_t type = this->receivedEvents.getNextEventType(ap); type != Events::InvalidEventType;
         type = this->receivedEvents.getNextEventType(ap))
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "trying to read event with internal type",
                      static_cast<size_t>(type));
        switch (type)
        {
            case InternalEventTypes::TESTEVENT:
                const Events::TESTEVENT event = this->receivedEvents.readNextEvent<Events::TESTEVENT>(ap);
                this->eventManager.emit(event, true);
                break;
        }
        numEvents++;
    }

    debugOutLevel(Debug::DebugLevels::updateLoop, "update: read", numEvents, "events from buffer");
    this->receivedEvents.clear();
}
