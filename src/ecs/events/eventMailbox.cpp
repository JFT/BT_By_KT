/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke, Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "eventMailbox.h"
#include "../../utils/debug.h"
#include "../../utils/threadPool.hpp"

EventMailbox::EventMailbox(ThreadPool& threadPool_)
    : threadPool(threadPool_)
    ,
    // getNumberOfWorkerThreads() + 1 because the main thread also occipies a threadID
    storedEvents(decltype(storedEvents)(threadPool.getNumberOfWorkerThreads() + 1))
{
}

EventMailbox::AccessPoint EventMailbox::getAccessPoint() const
{
    AccessPoint ap;
    ap.pos.resize(this->storedEvents.size(), static_cast<size_t>(0));
    return ap;
}

Events::EventID_t EventMailbox::getNextEventType(AccessPoint& accessPoint)
{
    DEBUG_ONLY_(this->calledGetNextEventType = true;)
    POW2_ASSERT(accessPoint.pos.size() == this->storedEvents.size());
    for (size_t threadIndex = 0; threadIndex < this->storedEvents.size(); ++threadIndex)
    {
        if (accessPoint.pos[threadIndex] < this->storedEvents[threadIndex].size())
        {
            POW2_ASSERT(accessPoint.pos[threadIndex] + 1 < this->storedEvents[threadIndex].size());
            const Events::EventID_t type = Events::Serialization::deserializeType(
                &this->storedEvents[threadIndex][accessPoint.pos[threadIndex]]);
            accessPoint.pos[threadIndex] += 1;
            return type;
        }
    }
    // ap points past all supplied buffers -> no more events to read anywhere
    return Events::InvalidEventType;
}

void EventMailbox::clear()
{
    for (auto& threadBuffers : this->storedEvents)
    {
        threadBuffers.clear();
    }
}

Events::Serialization::SerializedEventBuffer_t* EventMailbox::reserveBuffer(const std::size_t size)
{
    const size_t threadIndex = threadPool.getMappedIndex(std::this_thread::get_id());
    POW2_ASSERT(threadIndex < this->storedEvents.size());

    const size_t startIndex = this->storedEvents[threadIndex].size();
    debugOutLevel(Debug::DebugLevels::updateLoop, "preparing to reserve buffersize", size, "for event data for thread", threadIndex);
    if (startIndex + size >= this->storedEvents[threadIndex].capacity())
    {
        // make room for this + 7 other events of this type (this assumes events are roughly equally
        // large)
        // TODO: the magic number 8 * sizeof(T) needs some tests
        this->storedEvents[threadIndex].reserve(this->storedEvents[threadIndex].size() + 8 * size);
    }
    this->storedEvents[threadIndex].resize(storedEvents[threadIndex].size() + size);
    return &this->storedEvents[threadIndex][startIndex];
}
