/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef NETWOREVENTSYNCRONIZATIONCLIENT
#define NETWOREVENTSYNCRONIZATIONCLIENT

#include <raknet/ReplicaManager3.h>
#include "eventManager.h"
#include "eventNetworking.h"
#include "eventSerialization.h"
#include "../../network/globaldefine.h"
#include "../../utils/threadPool.hpp"

// forward declarations
class ReplicaManager3BT;


class NetworkEventSyncronizationClient : public RakNet::Replica3
{
   public:
    NetworkEventSyncronizationClient(ReplicaManager3BT* const replicaManager,
                                     EventManager& eventManager_,
                                     ThreadPool& threadPool_);

    NetworkEventSyncronizationClient(const NetworkEventSyncronizationClient&) = delete;

    virtual ~NetworkEventSyncronizationClient();

    void throwNetworkReceivedEvents(const uint32_t tickNumber);

    template <typename T>
    void receiveLocalEvent(const T& event); // definition after class definiton

    // ----------------------------------------------------------------------------
    // Replica functions
    // ----------------------------------------------------------------------------

    void WriteAllocationID(RakNet::Connection_RM3* /* destinationConnection */,
                           RakNet::BitStream* allocationIdBitstream) const;
    void DeallocReplica(RakNet::Connection_RM3* /* sourceConnection */) { delete this; }
    virtual void SerializeConstruction(RakNet::BitStream* constructionBitstream,
                                       RakNet::Connection_RM3* destinationConnection);
    virtual bool DeserializeConstruction(RakNet::BitStream* constructionBitstream,
                                         RakNet::Connection_RM3* sourceConnection);
    virtual void SerializeConstructionExisting(RakNet::BitStream* /* constructionBitstream */,
                                               RakNet::Connection_RM3* /* destinationConnection */);
    virtual void DeserializeConstructionExisting(RakNet::BitStream* /* constructionBitstream */,
                                                 RakNet::Connection_RM3* /* sourceConnection */);

    // Destruction
    virtual void SerializeDestruction(RakNet::BitStream* /* destructionBitstream */,
                                      RakNet::Connection_RM3* /* destinationConnection */)
    {
        // TODO: find out what to do here... shouldn't need to return the unused entityIDs because
        // the server should keep track of them himself
    }
    virtual bool DeserializeDestruction(RakNet::BitStream* /* destructionBitstream */,
                                        RakNet::Connection_RM3* /* sourceConnection */)
    {
        // TODO: find out what to do here... shouldn't need to return the unused entityIDs because
        // the server should keep track of them himself
        return false;
    }

    // Serialization
    virtual RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* serializeParameters);
    virtual void Deserialize(RakNet::DeserializeParameters* deserializeParameters);

    // Query Construction, Serialization, PopConnection
    virtual RakNet::RM3ConstructionState QueryConstruction(RakNet::Connection_RM3* destinationConnection,
                                                           RakNet::ReplicaManager3* /* replicaManager3 */)
    {
        return QueryConstruction_ServerConstruction(destinationConnection, GlobalBools::systemIsServer);
    }

    virtual bool QueryRemoteConstruction(RakNet::Connection_RM3* sourceConnection)
    {
        return QueryRemoteConstruction_ServerConstruction(sourceConnection, GlobalBools::systemIsServer);
    } // if someone else created a new obj and send it to us don't accept it
    virtual RakNet::RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3* destinationConnection);
    virtual RakNet::RM3ActionOnPopConnection
    QueryActionOnPopConnection(RakNet::Connection_RM3* /* droppedConnection */) const
    {
        return RakNet::RM3AOPC_DO_NOTHING;
    }

    NetworkEventSyncronizationClient& operator=(const NetworkEventSyncronizationClient&) = delete;

   private:
    ReplicaManager3BT* replicaManager;
    EventManager& eventManager;
    ThreadPool& threadPool;

    std::vector<std::vector<Events::Serialization::SerializedEventBuffer_t>> receivedEventsToBeSerialized;

    std::vector<Events::Serialization::SerializedEventBuffer_t> deserializedEventBuffer;

    std::vector<std::unique_ptr<EventManager::Subscription>> m_subscriptions;
};


/// @brief the network event syncronization has it's own receive function because it writes the
/// events into the buffer in a form that allows them to be written into the RakNet bitstream
/// directly when serializing speeding up that process.
/// @tparam T type of the event the system received locally
/// @param event the locally received event
template <typename EventType>
void NetworkEventSyncronizationClient::receiveLocalEvent(const EventType& event)
{
    const size_t threadIndex = threadPool.getMappedIndex(std::this_thread::get_id());
    POW2_ASSERT(threadIndex < this->receivedEventsToBeSerialized.size());
    Events::Networking::serializeNetworkEvent(event, this->receivedEventsToBeSerialized[threadIndex]);
}

#endif /* ifndef NETWOREVENTSYNCRONIZATIONCLIENT */
