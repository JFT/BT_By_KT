/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef NETWORKEVENTSYNCRONIZATIONSERVER
#define NETWORKEVENTSYNCRONIZATIONSERVER

#include <raknet/ReplicaManager3.h>
#include "eventManager.h"
#include "eventNetworking.h"
#include "../../network/globaldefine.h"
#include "../../utils/threadPool.hpp"

// forward declarations
namespace grid
{
    class Grid;
}
namespace Handles
{
    class HandleManager;
}
class ReplicaManager3BT;


class NetworkEventSyncronizationServer : public RakNet::Replica3
{
   public:
    NetworkEventSyncronizationServer(const uint32_t numberOfPlayers,
                                     ReplicaManager3BT* const replicaManager_,
                                     EventManager& eventManager_,
                                     ThreadPool& threadPool_);

    NetworkEventSyncronizationServer(const NetworkEventSyncronizationServer&) = delete;
    NetworkEventSyncronizationServer& operator=(const NetworkEventSyncronizationServer&) = delete;

    virtual ~NetworkEventSyncronizationServer();

    virtual void throwNetworkReceivedEvents(const uint32_t tickNumber);

    template <typename T>
    void receiveLocalEvent(const T& event); // definition after class definiton

    // ----------------------------------------------------------------------------
    // Replica functions
    // ----------------------------------------------------------------------------

    virtual void WriteAllocationID(RakNet::Connection_RM3* /* destinationConnection */,
                                   RakNet::BitStream* allocationIdBitstream) const;
    virtual void DeallocReplica(RakNet::Connection_RM3* /* sourceConnection */) { delete this; }
    virtual void SerializeConstruction(RakNet::BitStream* constructionBitstream,
                                       RakNet::Connection_RM3* destinationConnection);
    virtual bool DeserializeConstruction(RakNet::BitStream* constructionBitstream,
                                         RakNet::Connection_RM3* sourceConnection);
    virtual void SerializeConstructionExisting(RakNet::BitStream* /* constructionBitstream */,
                                               RakNet::Connection_RM3* /* destinationConnection */);
    virtual void DeserializeConstructionExisting(RakNet::BitStream* /* constructionBitstream */,
                                                 RakNet::Connection_RM3* /* sourceConnection */);

    // Destruction
    virtual void SerializeDestruction(RakNet::BitStream* /* destructionBitstream */,
                                      RakNet::Connection_RM3* /* destinationConnection */)
    {
        // TODO: find out what to do here... shouldn't need to return the unused entityIDs because
        // the server should keep track of them himself
    }
    virtual bool DeserializeDestruction(RakNet::BitStream* /* destructionBitstream */,
                                        RakNet::Connection_RM3* /* sourceConnection */)
    {
        // TODO: find out what to do here... shouldn't need to return the unused entityIDs because
        // the server should keep track of them himself
        return false;
    }

    // Serialization
    virtual RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* serializeParameters);
    virtual void Deserialize(RakNet::DeserializeParameters* deserializeParameters);

    // Query Construction, Serialization, PopConnection
    virtual RakNet::RM3ConstructionState QueryConstruction(RakNet::Connection_RM3* destinationConnection,
                                                           RakNet::ReplicaManager3* /* replicaManager3 */)
    {
        return QueryConstruction_ServerConstruction(destinationConnection, GlobalBools::systemIsServer);
    }

    virtual bool QueryRemoteConstruction(RakNet::Connection_RM3* sourceConnection)
    {
        return QueryRemoteConstruction_ServerConstruction(sourceConnection, GlobalBools::systemIsServer);
    } // if someone else created a new obj and send it to us don't accept it
    virtual RakNet::RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3* destinationConnection);

    virtual RakNet::RM3ActionOnPopConnection
    QueryActionOnPopConnection(RakNet::Connection_RM3* /* droppedConnection */) const
    {
        return RakNet::RM3AOPC_DO_NOTHING;
    }


   protected:
    ReplicaManager3BT* replicaManager;
    EventManager& eventManager;
    ThreadPool& threadPool;

    /// @brief one 'deserializedEventBuffer' for each player
    std::vector<std::vector<Events::Serialization::SerializedEventBuffer_t>> deserializedEventBuffers;
    /// @brief stores events by other players which will be serialized to the specific player
    std::vector<std::vector<Events::Serialization::SerializedEventBuffer_t>> serializeToPlayers;

    std::vector<std::vector<Events::Serialization::SerializedEventBuffer_t>> receivedLocalEvents;

    std::vector<std::unique_ptr<EventManager::Subscription>> m_subscriptions;

    /// @brief this system is subscribed to events it throws itself.
    //  If this system throws an event it is subscribed too (which will surely happen with the
    //  networkSystem) it would receive that event again and try to retransmit it.
    //  Thus when throwing events 'ignoreEvents' is used to stop them from being saved again
    bool ignoreEvents = false;

    /// @brief check all deserialized events from 'playerID' and emit those this player is actually
    /// allowed to send.
    /// Also copies allowed events into serializeToPlayers so they can be send to the other clients.
    /// @param playerID
    void filterAndEmitEvents(const size_t playerID);

    /// @brief check if the client is allowed to actually send the event we received (and
    /// deserialized) from it
    /// @param clientID
    /// @param event the deserialized event received by the client
    /// @return
    template <typename EventType>
    bool isAllowedToSend(const size_t clientID, const EventType event) const
    {
        // TODO: actually implement a policy which client is allowed to send which events.
        return true;
    }
};


template <typename EventType>
void NetworkEventSyncronizationServer::receiveLocalEvent(const EventType& event)
{
    if (this->ignoreEvents)
    {
        return;
    }
    const size_t threadIndex = this->threadPool.getMappedIndex(std::this_thread::get_id());
    POW2_ASSERT(threadIndex < this->receivedLocalEvents.size());
    Events::Networking::serializeNetworkEvent(event, this->receivedLocalEvents[threadIndex]);
}

#endif /* ifndef NETWORKEVENTSYNCRONIZATIONSERVER */
