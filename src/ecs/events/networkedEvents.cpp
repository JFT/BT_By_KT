#include "networkedEvents.h"

// definitions for constexpr static variables
// see: http://en.cppreference.com/w/cpp/language/static#Constant_static_members
// TODO: if we ever switch to c++17 remove this file
constexpr bool Events::Networking::NetworkEventType<Events::TESTEVENT>::needsServerACK;
constexpr bool Events::Networking::NetworkEventType<Events::TESTEVENT_DYNAMICSIZE>::needsServerACK;
constexpr bool Events::Networking::NetworkEventType<Events::FindPathTo>::needsServerACK;
constexpr bool Events::Networking::NetworkEventType<Events::MovementTargetChanged>::needsServerACK;
