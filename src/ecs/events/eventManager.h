// EventHandling for ECS
//based on a Forumpost at: https://codereview.stackexchange.com/questions/79211/ecs-event-messaging-implementation

#pragma once

#include <functional>
#include <memory>
#include <unordered_map>
#include <vector>
#include "eventID.h"
#include "networkedEvents.h"
#include "../../network/globaldefine.h"
#include "../../utils/debug.h"

struct BaseEvent
{
    virtual ~BaseEvent() {}

   protected:
    static size_t getNextType()
    {
        static size_t type_count(0);
        return type_count++;
    }
};

template <typename EventType>
struct Event : BaseEvent
{
    static size_t type()
    {
        static size_t t_type = BaseEvent::getNextType();
        return t_type;
    }
    explicit Event(const EventType& event)
        : event_(event)
    {
    }
    const EventType& event_;
};

/* Usage Example:

Events are normal structs containing the needed information to process the event
e.g:
struct PLAYER_LVL_UP
{ int new_level; };

struct PLAYER_HIT
{ int damage; };

struct COLLISION
{ Entity entity1; Entity entity2; };

now you can just use the following inside the systems to subscribe to an event

Eventmanager.subscribe<PLAYER_HIT>([this](){this->handle_hit();}); //-->this event will call the handle_hit() function inside the system for example

to emit an event simply use:
Eventmanager.emit<PLAYER_HIT>(PLayer_HIT{damage});

*/

class EventManager
{
   public:
    template <class EventType>
    using call_type = std::function<void(const EventType&)>;

    struct Subscription
    {
        const size_t id;
        EventManager& manager;

        ~Subscription() { manager.unsubscribe(id); }
    };

    EventManager()
        : m_subscribers()
    {
    }

    template <typename EventType>
    std::unique_ptr<Subscription> subscribe(call_type<EventType> callable, const bool subscribedToUnACKed = false)
    {
        const size_t type = Event<EventType>::type();
        if (type >= m_subscribers.size())
        {
            m_subscribers.resize(type + 1);
        }
        m_subscribers[type][m_nextSubscriberId] = {CallbackWrapper<EventType>(callable), !subscribedToUnACKed};

        Subscription* sub = new Subscription{m_nextSubscriberId, std::ref(*this)};

        m_nextSubscriberId++;
        return std::unique_ptr<Subscription>(sub);
    }

    void unsubscribe(const size_t id)
    {
        for (auto& eventHandlers : m_subscribers)
        {
            const auto at = eventHandlers.find(id);
            if (at != eventHandlers.end())
            {
                eventHandlers.erase(at);
                break;
            }
        }
    }

    /// @brief
    /// @tparam EventType
    /// @param event
    /// @param serverACKed indicates if the event was greenlit by the server. Determines which
    /// subscribed systems get the event (non-ACKed events only go to systems subscribed to
    /// non-ACKed events and vice-versa). Defaults to false on the client and to true on the server.
    /// If the event doesn't need ACK
    /// (Events::Networking::NetworkEventType<EventType>::needsServerACK is false) serverACKed
    /// defaults to true.
    template <typename EventType>
    void emit(const EventType& event, bool serverACKed = false) const
    {
        size_t type = Event<EventType>::type();
        // if an event gets emited that has no subscribers... we do not need to handle it!
        if (type >= m_subscribers.size()) return;
        // if the event doesn't need to be ACKed act as if it is an ACKed event (makes the testing
        // before emitting easier)
        if (not Events::Networking::NetworkEventType<EventType>::needsServerACK)
        {
            serverACKed = true;
        }
        Event<EventType> eventWrapper(event);
        for (auto& handler : m_subscribers[type])
        {
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "testing event with type",
                          type,
                          "subscriber id",
                          handler.first,
                          "callForUnACKed =",
                          handler.second.onlyAcked,
                          "serverACKed =",
                          serverACKed);
            // two possibilities to send the event
            // 1) system wants ACKed and event is ACKed
            // 2) system wants non-ACKed and event isn't ACKed
            if ((handler.second.onlyAcked and serverACKed) or
                ((not handler.second.onlyAcked) and (not serverACKed)))
            {
                handler.second.callback(eventWrapper);
            }
        }
    }

    template <typename EventType>
    struct CallbackWrapper
    {
        explicit CallbackWrapper(call_type<EventType> callable)
            : m_callable(callable)
        {
        }

        void operator()(const BaseEvent& event)
        {
            m_callable(static_cast<const Event<EventType>&>(event).event_);
        }

        call_type<EventType> m_callable;
    };

   private:
    template <class EventType>
    struct CallbackEntry
    {
        call_type<BaseEvent> callback;
        bool onlyAcked = true;
    };

    std::vector<std::unordered_map<size_t, CallbackEntry<BaseEvent>>> m_subscribers;
    size_t m_nextSubscriberId = 0;
};
