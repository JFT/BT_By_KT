/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef EVENTNETWORKING_H
#define EVENTNETWORKING_H

#include <cstddef> // size_t
#include <vector>

#include "eventSerialization.h"
#include "networkedEvents.h"

namespace Events
{
    namespace Networking
    {
        inline unsigned char* toNetworkDataType(Serialization::SerializedEventBuffer_t* const buffer)
        {
            return reinterpret_cast<unsigned char* const>(buffer);
        }

        struct NetworkEventMetaData
        {
            static constexpr size_t SizeInBuffer = 1 + sizeof(Serialization::SerializedEventSize_t);
            EventID_t type = InvalidEventType;
            Serialization::SerializedEventSize_t eventDataSize = 0;
        };

        template <typename EventType>
        size_t NeededNetworkBufferSize(const EventType& event)
        {
            // networked meta data (event type, size of the event), the rest for the actual event
            return NetworkEventMetaData::SizeInBuffer + Serialization::SerializedEventSize<EventType>(event);
        }

        template <typename EventType>
        void serializeNetworkEvent(const EventType& event, Serialization::SerializedEventBuffer_t* targetBuffer)
        {
            using NET = NetworkEventType<EventType>;

            constexpr size_t type = NET::type;
            static_assert(type != static_cast<std::size_t>(-1),
                          "This event isn't a networked event but it must be in order to guarantee "
                          "correct event type. See events/networkedEvents.h");
            static_assert(type <= static_cast<size_t>(
                                      std::numeric_limits<Serialization::SerializedEventBuffer_t>::max()),
                          "the networked type of the received event doesn't fit into the "
                          "designated size for the serialized buffer. See "
                          "events/eventSerialization and "
                          "events/dynamicallySizedEventSerialization.h");

            *targetBuffer = static_cast<Serialization::SerializedEventBuffer_t>(type);
            *reinterpret_cast<Serialization::SerializedEventSize_t*>(targetBuffer + 1) =
                Serialization::SerializedEventSize<EventType>(event);
            Serialization::serialize(event, targetBuffer + NetworkEventMetaData::SizeInBuffer);
        }

        template <typename EventType>
        static void serializeNetworkEvent(const EventType& event,
                                          std::vector<Serialization::SerializedEventBuffer_t>& targetBuffer)
        {
            const auto neededMemorySizeInBytes =
                Events::Networking::NeededNetworkBufferSize<EventType>(event);

            const size_t oldSize = targetBuffer.size();

            if (oldSize + neededMemorySizeInBytes >= targetBuffer.capacity())
            {
                // make room for this + 7 other events of this type (this assumes events are roughly
                // equally
                // large)
                // TODO: the magic number 8 * sizeof(T) needs some tests
                targetBuffer.reserve(oldSize + 8 * neededMemorySizeInBytes);
            }
            targetBuffer.resize(oldSize + neededMemorySizeInBytes);
            Events::Networking::serializeNetworkEvent(event, &targetBuffer[oldSize]);
        }


        static NetworkEventMetaData deserializeMetaData(const Serialization::SerializedEventBuffer_t* const buffer)
        {
            NetworkEventMetaData metaData;
            metaData.type = static_cast<EventID_t>(*buffer);
            metaData.eventDataSize =
                *reinterpret_cast<const Serialization::SerializedEventSize_t* const>(buffer + 1);
            return metaData;
        }
    } // namespace Networking
} // namespace Events

#endif /* ifndef EVENTNETWORKING_H */
