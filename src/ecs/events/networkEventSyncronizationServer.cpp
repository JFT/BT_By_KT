/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "networkEventSyncronizationServer.h"
#include "dynamicallySizedEventSerialization.h"
#include "eventManager.h"
#include "events.h"
#include "../../network/allocationIDs.h"
#include "../../network/connectionbt.h"
#include "../../network/replicamanager3bt.h"
#include "../../utils/debug.h"
#include "../../utils/static/error.h"

#include <numeric>

NetworkEventSyncronizationServer::NetworkEventSyncronizationServer(const uint32_t numberOfPlayers,
                                                                   ReplicaManager3BT* const replicaManager_,
                                                                   EventManager& eventManager_,
                                                                   ThreadPool& threadPool_)
    : replicaManager(replicaManager_)
    , eventManager(eventManager_)
    , threadPool(threadPool_)
    , deserializedEventBuffers(decltype(deserializedEventBuffers)(numberOfPlayers))
    , serializeToPlayers(decltype(serializeToPlayers)(numberOfPlayers))
    // getNumberOfWorkerThreads() + 1 because the main thread also occipies a threadID
    , receivedLocalEvents(decltype(receivedLocalEvents)(threadPool.getNumberOfWorkerThreads() + 1))
{
    this->replicaManager->Reference(this);

    this->m_subscriptions.push_back(this->eventManager.subscribe<Events::TESTEVENT>(
        [this](const Events::TESTEVENT event) { this->receiveLocalEvent(event); }));
}

NetworkEventSyncronizationServer::~NetworkEventSyncronizationServer()
{
    // this->replicaManager->Dereference(this); dereference is called in replica3 destructor
}

void NetworkEventSyncronizationServer::throwNetworkReceivedEvents(const uint32_t tickNumber)
{
    for (size_t playerID = 0; playerID < this->deserializedEventBuffers.size(); playerID++)
    {
        this->filterAndEmitEvents(playerID);
    }
}

void NetworkEventSyncronizationServer::WriteAllocationID(RakNet::Connection_RM3* /* destinationConnection */,
                                                         RakNet::BitStream* allocationIdBitstream) const
{
    debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                  "NetworkEventSyncronizationServer: writing allocation ID",
                  Networking::AllocationID::NetworkEventSyncronization);
    allocationIdBitstream->Write(static_cast<Networking::NetworkedAllocationIDType>(
        Networking::AllocationID::NetworkEventSyncronization));
}

void NetworkEventSyncronizationServer::SerializeConstruction(RakNet::BitStream* DEBUG_ONLY_(constructionBitstream),
                                                             RakNet::Connection_RM3* /* destinationConnection */)
{
}

bool NetworkEventSyncronizationServer::DeserializeConstruction(RakNet::BitStream* DEBUG_ONLY_(constructionBitstream),
                                                               RakNet::Connection_RM3* /* sourceConnection */)
{
    return not GlobalBools::systemIsServer; // construct only on client
}


void NetworkEventSyncronizationServer::SerializeConstructionExisting(
    RakNet::BitStream* /* constructionBitstream */, RakNet::Connection_RM3* /* destinationConnection */)
{
    POW2_ASSERT_FAIL("this object shouldn't be calling SerializeConstructionExisting because "
                     "'QueryConstruction' souldn't return 'RM3CS_ALREADY_EXISTS_REMOTELY'",
                     true);
}
void NetworkEventSyncronizationServer::DeserializeConstructionExisting(
    RakNet::BitStream* /* constructionBitstream */, RakNet::Connection_RM3* /* sourceConnection */)
{
    POW2_ASSERT_FAIL("this object shouldn't be calling DeserializeConstructionExisting because "
                     "'QueryConstruction' souldn't return 'RM3CS_ALREADY_EXISTS_REMOTELY'",
                     true);
}

RakNet::RM3SerializationResult NetworkEventSyncronizationServer::Serialize(RakNet::SerializeParameters* serializeParameters)
{
    const auto playerID =
        static_cast<ConnectionBT*>(serializeParameters->destinationConnection)->getPlayerID();
    // events other players send to this player
    const auto totalDataSize = static_cast<uint32_t>(
        std::accumulate(receivedLocalEvents.begin(),
                        receivedLocalEvents.end(),
                        serializeToPlayers[playerID].size(),
                        [](const auto& sum, const auto& data) { return sum + data.size(); }));

    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "serialize() for player",
                  playerID,
                  "totalDataSize =",
                  totalDataSize,
                  "if 0 won't "
                  "serialize, if "
                  "!= 0 will");
    if (totalDataSize == 0)
    {
        return RakNet::RM3SerializationResult::RM3SR_DO_NOT_SERIALIZE;
    }

    serializeParameters->outputBitstream->Write(totalDataSize);

    serializeParameters->outputBitstream->WriteAlignedBytes(
        Events::Networking::toNetworkDataType(&this->serializeToPlayers[playerID][0]),
        static_cast<const unsigned int>(this->serializeToPlayers[playerID].size()));
    this->serializeToPlayers[playerID].clear();

    for (auto& threadData : this->receivedLocalEvents)
    {
        if (threadData.size() > 0)
        {
            serializeParameters->outputBitstream->WriteAlignedBytes(
                Events::Networking::toNetworkDataType(&threadData[0]),
                static_cast<const unsigned int>(threadData.size()));
            threadData.clear();
        }
    }

    return RakNet::RM3SerializationResult::RM3SR_BROADCAST_IDENTICALLY_FORCE_SERIALIZATION;
}

void NetworkEventSyncronizationServer::Deserialize(RakNet::DeserializeParameters* deserializeParameters)
{
    const auto playerID = static_cast<ConnectionBT*>(deserializeParameters->sourceConnection)->getPlayerID();
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "before deserialization for playerID",
                  playerID,
                  ": deserializedEventBuffer is",
                  this->deserializedEventBuffers[playerID].size(),
                  "bytes");
    // TODO: check if we are expecting events from this connection
    uint32_t totalDataSize = 0;
    if (not deserializeParameters->serializationBitstream->Read(totalDataSize))
    {
        Error::errContinue("couldn't read 'totalDataSize' from deserialization bitstream");
        return;
    }
    size_t oldSize = this->deserializedEventBuffers[playerID].size();
    // TODO: set acceptable limits which make sure the client doesn't reserve GBs of memory because
    // we fucked something up when serializing
    this->deserializedEventBuffers[playerID].resize(oldSize + totalDataSize);

    if (sizeof(uint32_t) + totalDataSize > deserializeParameters->serializationBitstream->GetNumberOfBytesUsed())
    {
        Error::errContinue("the remote said there will be",
                           totalDataSize,
                           "bytes of events but the received data is only",
                           deserializeParameters->serializationBitstream->GetNumberOfBytesUsed(),
                           "bytes in the stream (",
                           sizeof(uint32_t),
                           "where used for the length of the data)");
        // new space contains invalid data
        this->deserializedEventBuffers[playerID].resize(oldSize);
        return;
    }

    // writing new data starts at 'oldSize' which is the first index of the now created free space
    if (not deserializeParameters->serializationBitstream->ReadAlignedBytes(
            Events::Networking::toNetworkDataType(&this->deserializedEventBuffers[playerID][oldSize]), totalDataSize))
    {
        Error::errContinue("couldn't read event bytes out of the deserialization bitstream");
        // new space contains invalid data
        this->deserializedEventBuffers[playerID].resize(oldSize);
        return;
    }

    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "deserialized",
                  totalDataSize,
                  "bytes. New deserializedEventBuffer is",
                  this->deserializedEventBuffers[playerID].size(),
                  "bytes");

    DEBUG_ONLY_(deserializeParameters->serializationBitstream->AssertStreamEmpty());
}


RakNet::RM3QuerySerializationResult
NetworkEventSyncronizationServer::QuerySerialization(RakNet::Connection_RM3* destinationConnection)
{
    // data to be send to all clients (events generated on the server)
    for (auto& threatData : this->receivedLocalEvents)
    {
        if (threatData.size() > 0)
        {
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "query serialization for player",
                          reinterpret_cast<ConnectionBT*>(destinationConnection)->getPlayerID(),
                          "returned call_serialize! (due to local events!)");
            return RakNet::RM3QuerySerializationResult::RM3QSR_CALL_SERIALIZE;
        }
    }

    // data to be forwarded to this client generated by other clients
    const auto playerID = static_cast<ConnectionBT*>(destinationConnection)->getPlayerID();
    if (this->serializeToPlayers[playerID].size() > 0)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "query serialization for player",
                      reinterpret_cast<ConnectionBT*>(destinationConnection)->getPlayerID(),
                      "returned call_serialize!");
        return RakNet::RM3QuerySerializationResult::RM3QSR_CALL_SERIALIZE;
    }

    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "query serialization for player",
                  reinterpret_cast<ConnectionBT*>(destinationConnection)->getPlayerID(),
                  "returned DO_NOT_CALL_SERIALIZE!");
    return RakNet::RM3QuerySerializationResult::RM3QSR_DO_NOT_CALL_SERIALIZE;
}

void NetworkEventSyncronizationServer::filterAndEmitEvents(const size_t playerID)
{
    this->ignoreEvents = true;
    size_t numEvents = 0;
    size_t index = 0;

    while (index < this->deserializedEventBuffers[playerID].size())
    {
        const size_t metaDataStartIndex = index;
        const auto metaData =
            Events::Networking::deserializeMetaData(&this->deserializedEventBuffers[playerID][index]);
        // read the metadata -> advance the index the size of the metadata inside the buffer to put
        // it where serialized event begins
        index += static_cast<size_t>(Events::Networking::NetworkEventMetaData::SizeInBuffer);

        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "metadata for this event",
                      static_cast<size_t>(metaData.type),
                      static_cast<size_t>(metaData.eventDataSize));

        bool isAllowedToSendThisEvent = false;
        switch (metaData.type)
        {
            case Events::Networking::NetworkEventType<Events::TESTEVENT>::type:
            {
                POW2_ASSERT(this->deserializedEventBuffers[playerID].size() >= index + metaData.eventDataSize);
                POW2_ASSERT(metaData.eventDataSize ==
                            Events::Serialization::deserializeEventSize<Events::TESTEVENT>(
                                &this->deserializedEventBuffers[playerID][index]));

                const auto event = Events::Serialization::deserialize<Events::TESTEVENT>(
                    &this->deserializedEventBuffers[playerID][index]);
                // only player 0 is allowed to send this event
                isAllowedToSendThisEvent = playerID == 0;
                this->eventManager.emit(event);
            }
            break;
            case Events::Networking::NetworkEventType<Events::TESTEVENT_DYNAMICSIZE>::type:
            {
                POW2_ASSERT(this->deserializedEventBuffers[playerID].size() >= index + metaData.eventDataSize);
                POW2_ASSERT(metaData.eventDataSize ==
                            Events::Serialization::deserializeEventSize<Events::TESTEVENT_DYNAMICSIZE>(
                                &this->deserializedEventBuffers[playerID][index]));

                auto event = Events::Serialization::deserialize<Events::TESTEVENT_DYNAMICSIZE>(
                    &this->deserializedEventBuffers[playerID][index]);
                // when sending back this event change the first vector entry to that of the player
                // who send this event
                if (not event.vecs.empty())
                {
                    event.vecs[0].X = playerID;
                }
                // overwrite the event in the buffer with the new event (only works because the
                // events have the same serialized size)
                Events::Serialization::serialize(event, &this->deserializedEventBuffers[playerID][index]);
                isAllowedToSendThisEvent = true;
                this->eventManager.emit(event);
            }
            break;
            case Events::Networking::NetworkEventType<Events::MovementTargetChanged>::type:
            {
                POW2_ASSERT(this->deserializedEventBuffers[playerID].size() >= index + metaData.eventDataSize);
                POW2_ASSERT(metaData.eventDataSize ==
                            Events::Serialization::deserializeEventSize<Events::MovementTargetChanged>(
                                &this->deserializedEventBuffers[playerID][index]));

                auto event = Events::Serialization::deserialize<Events::MovementTargetChanged>(
                    &this->deserializedEventBuffers[playerID][index]);
                // TODO: actually check what is allowed
                isAllowedToSendThisEvent = true;
                this->eventManager.emit(event);
            }
            break;
            case Events::Networking::NetworkEventType<Events::FindPathTo>::type:
            {
                POW2_ASSERT(this->deserializedEventBuffers[playerID].size() >= index + metaData.eventDataSize);
                POW2_ASSERT(metaData.eventDataSize ==
                            Events::Serialization::deserializeEventSize<Events::FindPathTo>(
                                &this->deserializedEventBuffers[playerID][index]));

                auto event = Events::Serialization::deserialize<Events::FindPathTo>(
                    &this->deserializedEventBuffers[playerID][index]);
                // TODO: actually check what is allowed
                isAllowedToSendThisEvent = true;
                this->eventManager.emit(event);
            }
            break;
            default:
                Error::errTerminate("unhandled network event type", static_cast<size_t>(metaData.type));
        } // switch (metaData.type)

        // TODO: handle players sending events they are not allowed to send. Kick them?
        if (isAllowedToSendThisEvent)
        {
            // also send the event back to the player who send it because he will now accept it when
            // it comes from the server
            for (auto& player : this->serializeToPlayers)
            {
                const size_t oldSize = player.size();
                player.reserve(oldSize + Events::Networking::NetworkEventMetaData::SizeInBuffer +
                               metaData.eventDataSize);
                for (size_t i = 0;
                     i < Events::Networking::NetworkEventMetaData::SizeInBuffer + metaData.eventDataSize;
                     i++)
                {
                    player.push_back(this->deserializedEventBuffers[playerID][i + metaDataStartIndex]);
                }
            }
        }
        // TODO: what to do with rejected events?

        numEvents++;
        index += metaData.eventDataSize;
    } // while (index < this->deserializedEventBuffers[playerID].size())

    this->deserializedEventBuffers[playerID].clear();
    this->ignoreEvents = false;
}
