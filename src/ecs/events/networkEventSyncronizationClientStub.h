/*
BattleTanks standalone client. a C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef NETWORKEVENTSYNCRONIZATIONCLIENTSTUB
#define NETWORKEVENTSYNCRONIZATIONCLIENTSTUB

class ThreadPool;
class EventMailbox;

#include "eventID.h"
#include "../events/eventManager.h"

/// @brief Stub of the networkEventSyncronization for the client. All events received are re-throw
/// marked as server-ACKed.
class NetworkEventSyncronizationClientStub
{
   public:
    enum InternalEventTypes : Events::EventID_t
    {
        TESTEVENT = 0
    };

    NetworkEventSyncronizationClientStub(EventManager& eventManager_, ThreadPool& threadPool_);
    ~NetworkEventSyncronizationClientStub();

    void throwServerAckedEvents();

   private:
    EventManager& eventManager;
    std::vector<std::unique_ptr<EventManager::Subscription>> m_subscriptions;
    EventMailbox& receivedEvents;
};

#endif /* ifndef NETWORKEVENTSYNCRONIZATIONCLIENTSTUB */
