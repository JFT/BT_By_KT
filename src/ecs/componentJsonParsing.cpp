/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "componentJsonParsing.h"

#include <irrlicht/ISceneManager.h>
#include <irrlicht/IVideoDriver.h>

#include "../utils/debug.h"
#include "../utils/json.h"
#include "../utils/static/error.h"

#include <cmath>

namespace ComponentParsing // TODO: make proper lowercase comparison
{
    template <>
    bool parseFromJson(const Json::Value& v, Components::DebugColor&)
    {
        (void) v;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "No parser for DebugColor defined");
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Set DebugColor to default value");
        return false;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Position& p)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Position component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Input value is Null. Set Positon to default value");
            return false;
        }
        else if (v.size() == 2)
        {
            p.position.X = v[0u].asFloat();
            p.position.Y = v[1u].asFloat();
            debugOutLevel(
                Debug::DebugLevels::firstOrderLoop + 3, "Parsed position:", p.position.X, p.position.Y);
            return true;
        }
        throw std::invalid_argument("Position component must have exactly 2 elements");
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Movement& m)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Movement component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input value is Null. Set Movement to default value");
            return false;
        }
        if (not v.isObject() || v.getMemberNames().empty())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input dict is Empty. Set Movement to default value");
            return false;
        }
        for (const auto member : v.getMemberNames())
        {
            if (member == "speed" or member == "Speed")
            {
                m.speed = v[member].asFloat();
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed speed:", m.speed);
            }
            else if (member == "radius" or member == "Radius")
            {
                m.radius = v[member].asFloat();
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed radius:", m.radius);
            }
            else
            {
                throw std::invalid_argument("Unknown value '" + member + "' in movement component!");
            }
        }
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Graphic& g, irr::scene::ISceneManager* const sceneManager)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Graphic component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Input value is Null. Set Graphic to default value");
            return false;
        }
        if (not v.isObject() || v.getMemberNames().empty())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Input dict is Empty. Set Graphic to default value");
            return false;
        }
        for (const auto member : v.getMemberNames())
        {
            if (member == "Mesh" or member == "mesh")
            {
                const auto meshFile = v[member].asString();
                irr::scene::IAnimatedMesh* mesh = sceneManager->getMesh(meshFile.c_str());
                if (mesh == nullptr)
                {
                    throw std::invalid_argument("Couldn't load mesh '" + meshFile + "'");
                }
                g.mesh = mesh;
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed meshFile:", meshFile, "mesh=", g.mesh);
            }
            else if (member == "Texture" or member == "texture")
            {
                const auto textureFile = v[member].asString();
                irr::video::ITexture* const texture =
                    sceneManager->getVideoDriver()->getTexture(textureFile.c_str());
                if (texture == nullptr)
                {
                    throw std::invalid_argument("Couldn't load texture '" + std::string(textureFile) + "'");
                }
                g.texture = texture;
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed textureFile:", textureFile, "texture=", g.texture);
            }
            else if (member == "Scale" or member == "scale")
            {
                if (v[member].size() != 3)
                {
                    throw std::invalid_argument("Scale component must have exactly 3 elements");
                }
                g.scale.X = v[member][0u].asFloat();
                g.scale.Y = v[member][1u].asFloat();
                g.scale.Z = v[member][2u].asFloat();
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                              "Parsed scale:",
                              g.scale.X,
                              g.scale.Y,
                              g.scale.Z);
            }
            else if (member == "Offset" or member == "offset")
            {
                g.offset = v[member].asFloat();
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed offset:", g.offset);
            }
            else
            {
                throw std::invalid_argument("Unknown value '" + member + "' in Graphic component!");
            }
        }
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::ProjectileMovement& p)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing ProjectileMovement component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input value is Null. Doesn't "
                          "parsed ProjectileMovement (used "
                          "default)!!!");
            return false;
        }
        if (not v.isObject() || v.getMemberNames().empty())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input dict is Empty. Doesn't "
                          "parsed ProjectileMovement (used "
                          "default)!!!");
            return false;
        }
        for (const auto member : v.getMemberNames())
        {
            if (member == "Speed" or member == "speed")
            {
                const auto speed = v[member].asFloat();
                p.speed = speed;
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed speed:", p.speed);
            }
            else
            {
                throw std::invalid_argument("Unknown value '" + member + "' in ProjectileMovement component!");
            }
        }
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Rotation& r)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Rotation component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input value is Null. Set Rotation to default value");
            return false;
        }
        else if (v.size() != 3)
        {
            throw std::invalid_argument("Rotation component must have exactly 3 elements");
        }
        r.rotation.X = v[0u].asFloat();
        r.rotation.Y = v[1u].asFloat();
        r.rotation.Z = v[2u].asFloat();
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                      "Parsed rotation:",
                      r.rotation.X,
                      r.rotation.Y,
                      r.rotation.Z);

        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::EntityName& e)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing EntityName component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input value is Null. Set EntityName to default value");
            return false;
        }
        const irr::core::stringw name = irr::core::stringw(v.asCString());
        if (name == "")
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Can't parsed EntityName '' use default value");
            return false;
        }
        e.entityName = name;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                      "Parsed EntityName:",
                      irr::core::stringc(e.entityName).c_str());
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Price& p)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Price component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Input value is Null. Set Price to default value");
            return false;
        }
        if (v.isDouble())
        {
            const auto d = std::round(v.asDouble());
            p.price = static_cast<uint16_t>(std::round(v.asDouble()));
        }
        else
        {
            p.price = static_cast<uint16_t>(v.asUInt());
        }
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed Price:", p.price);
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Damage& d)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Damage component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Input value is Null. Set Damage to default value");
            return false;
        }
        if (not v.isObject() || v.getMemberNames().empty())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Input dict is Empty. Set Damage to default value");
            return false;
        }
        for (const auto member : v.getMemberNames())
        {
            if (member == "Damage" or member == "damage")
            {
                d.damage = v[member].asFloat();
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed Damage:", d.damage);
            }
            else if (member == "AttacksUnitType" or member == "attacksUnitType")
            {
                auto completeAttacksUnitTypesString = std::string(v[member].asCString());

                std::stringstream ss(completeAttacksUnitTypesString);
                std::string attacksUnitTypeStringElement;
                std::vector<std::string> attacksUnitTypeStrings;
                while (ss >> attacksUnitTypeStringElement)
                    attacksUnitTypeStrings.push_back(attacksUnitTypeStringElement);

                bool attackNotFlag = false;
                for (auto attacksUnitTypeStr : attacksUnitTypeStrings)
                {
                    debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, attacksUnitTypeStr.c_str());
                    if (attacksUnitTypeStr == "Normal" or attacksUnitTypeStr == "normal") // means all
                    {
                        d.attacksUnitTypeFlag = ~int(UnitTypeE::Invalid);
                    }
                    else if (attacksUnitTypeStr == "No" or attacksUnitTypeStr == "no")
                    {
                        attackNotFlag = true;
                    }
                    else if (attacksUnitTypeStr == "Ground" or attacksUnitTypeStr == "ground")
                    {
                        d.attacksUnitTypeFlag = d.attacksUnitTypeFlag | int(UnitTypeE::Ground);
                    }
                    else if (attacksUnitTypeStr == "Air" or attacksUnitTypeStr == "air")
                    {
                        d.attacksUnitTypeFlag = d.attacksUnitTypeFlag | int(UnitTypeE::Air);
                    }
                    else if (attacksUnitTypeStr == "Tanks" or attacksUnitTypeStr == "tanks")
                    {
                        d.attacksUnitTypeFlag = d.attacksUnitTypeFlag | int(UnitTypeE::Tank);
                    }
                    else if (attacksUnitTypeStr == "Creeps" or attacksUnitTypeStr == "creeps")
                    {
                        d.attacksUnitTypeFlag = d.attacksUnitTypeFlag | int(UnitTypeE::Creep);
                    }
                    else if (attacksUnitTypeStr == "Buildings" or attacksUnitTypeStr == "buildings")
                    {
                        d.attacksUnitTypeFlag = d.attacksUnitTypeFlag | int(UnitTypeE::Building);
                    }
                    else
                    {
                        throw std::invalid_argument("Unknown AttacksUnitType" +
                                                    std::string(attacksUnitTypeStr.c_str()));
                    }
                }

                if (attackNotFlag)
                {
                    d.attacksUnitTypeFlag = ~(d.attacksUnitTypeFlag);
                }

                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                              "Parsed AttacksUnitType '",
                              completeAttacksUnitTypesString,
                              "' to:",
                              d.attacksUnitTypeFlag);
            }
            else
            {
                throw std::invalid_argument("Unknown value '" + member + "' in Damage component!");
            }
        }
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Range& r)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Range component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Input value is Null. Set Range to default value");
            return false;
        }
        r.squaredRange = static_cast<uint32_t>(v.asUInt() * v.asUInt());
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed Range:", r.squaredRange);
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Ammo& a, std::map<std::string, blueprintID_t>& blueprintNameToIDMap)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Ammo component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Input value is Null. Set Ammo to default value");
            return false;
        }
        if (not v.isObject() || v.getMemberNames().empty())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Input dict is Empty. Set Ammo to default value");
            return false;
        }
        for (const auto member : v.getMemberNames())
        {
            if (member == "NrProjectiles" or member == "nrProjectiles")
            {
                a.nrProjectiles = static_cast<uint16_t>(v[member].asUInt());
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed nrProjectiles:", a.nrProjectiles);
            }
            else if (member == "ProjectilID" or member == "projectilID")
            {
                if (v[member].isNull())
                {
                    a.projectilID = blueprintNameToIDMap["testProjectile"];
                    debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                                  "Null value. Doesn't parsed ProjectilID, but in this case "
                                  "overwrite default (used testProjectile ",
                                  a.projectilID,
                                  "as default)!!!");
                    //                    debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                    //                                  "Null value. Set ProjectilID to default
                    //                                  value");
                    continue;
                }
                const auto projectileEntityName = v[member].asString();
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed ProjectileEntityName:", projectileEntityName);
                if (blueprintNameToIDMap.find(projectileEntityName) != blueprintNameToIDMap.end())
                {
                    a.projectilID = blueprintNameToIDMap[projectileEntityName];
                    debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed projectilID:", a.projectilID);
                }
                else
                {
                    throw std::invalid_argument("Unknown EntityName '" + projectileEntityName + "'");
                }
            }
            else
            {
                throw std::invalid_argument("Unknown value '" + member + "' in Ammo component!");
            }
        }
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Icon& i, irr::scene::ISceneManager* const sceneManager)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Icon component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Input value is Null. Set Icon to default value");
            return false;
        }
        const auto textureFile = v.asString();
        irr::video::ITexture* const texture =
            sceneManager->getVideoDriver()->getTexture(textureFile.c_str());
        if (texture == nullptr)
        {
            throw std::invalid_argument("Couldn't load texture '" + textureFile + "'");
        }
        i.texture = texture;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed Icon textureFile:", textureFile, "texture=", i.texture);
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Inventory& i)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Inventory component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input value is Null. Set Inventory to default value");
            return false;
        }
        else if (v.size() > 6) // TODO make pretty
        {
            throw std::invalid_argument("Inventory component must have less than 6 elements");
        }
        else
        {
            for (unsigned int itemNr = 0; itemNr < v.size(); itemNr++)
            {
                i.inventory[itemNr] = v[itemNr].asUInt();
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed ItemID:", i.inventory[itemNr]);
            }
        }

        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v,
                       Components::PurchasableItems& p,
                       std::map<std::string, blueprintID_t>& blueprintNameToIDMap)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing PurchasableItems component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input value is Null. Set PurchasableItems to default value");
            return false;
        }
        for (unsigned int itemNr = 0; itemNr < v.size(); itemNr++)
        {
            auto itemName = v[itemNr].asString();
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed ItemName:", itemName);
            if (blueprintNameToIDMap.find(itemName) != blueprintNameToIDMap.end())
            {
                p.purchasableItems.push_back(blueprintNameToIDMap[itemName]);
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed ItemID:", int(p.purchasableItems[itemNr]));
            }
            else
            {
                throw std::invalid_argument("Unknown EntityName '" + itemName + "'");
            }
        }
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Requirement& r)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Requirement component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input value is Null. Set Requirement to default value");
            return false;
        }
        r.requirement = static_cast<uint16_t>(v.asUInt());
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed Requirement:", r.requirement);
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::SelectionID&)
    {
        (void) v;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "No parser for SelectionID defined");
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Set SelectionID to default value");
        return false;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Cooldown& c)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Cooldown component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input value is Null. Set Cooldown to default value");
            return false;
        }
        c.initial = v.asFloat();
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed Cooldown initial:", c.initial);
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::Physical& p)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing Physical component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input value is Null. Set Physical to default value");
            return false;
        }
        if (not v.isObject() || v.getMemberNames().empty())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input dict is Empty. Set Physical to default value");
            return false;
        }
        for (const auto member : v.getMemberNames())
        {
            if (member == "HP" or member == "hp")
            {
                p.maxHealth = float(v[member].asInt());
                p.health = p.maxHealth;
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed HP:", p.maxHealth);
            }
            else if (member == "Armor" or member == "armor")
            {
                p.armor = float(v[member].asInt());
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed Armor:", p.armor);
            }
            else if (member == "UnitType" or member == "unitType")
            {
                auto completeUnitTypesString = std::string(v[member].asCString());

                std::stringstream ss(completeUnitTypesString);
                std::string unitTypeStringElement;
                std::vector<std::string> unitTypeStrings;
                while (ss >> unitTypeStringElement)
                    unitTypeStrings.push_back(unitTypeStringElement);

                for (auto unitTypeStr : unitTypeStrings)
                {
                    debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, unitTypeStr.c_str());
                    if (unitTypeStr == "Ground" or unitTypeStr == "ground")
                    {
                        p.unitTypeFlag = p.unitTypeFlag | int(UnitTypeE::Ground);
                    }
                    else if (unitTypeStr == "Air" or unitTypeStr == "air")
                    {
                        p.unitTypeFlag = p.unitTypeFlag | int(UnitTypeE::Air);
                    }
                    else if (unitTypeStr == "Tank" or unitTypeStr == "tank")
                    {
                        p.unitTypeFlag = p.unitTypeFlag | int(UnitTypeE::Tank);
                    }
                    else if (unitTypeStr == "Creep" or unitTypeStr == "creep")
                    {
                        p.unitTypeFlag = p.unitTypeFlag | int(UnitTypeE::Creep);
                    }
                    else if (unitTypeStr == "Building" or unitTypeStr == "building")
                    {
                        p.unitTypeFlag = p.unitTypeFlag | int(UnitTypeE::Building);
                    }
                    else
                    {
                        throw std::invalid_argument("Unknown UnitType" + std::string(unitTypeStr.c_str()));
                    }
                }
                debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                              "Parsed UnitType '",
                              completeUnitTypesString.c_str(),
                              "' to:",
                              p.unitTypeFlag);
            }
            else
            {
                throw std::invalid_argument("Unknown value '" + member + "' in Physical component!");
            }
        }
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::OwnerEntityID&)
    {
        (void) v;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "No parser for OwnerEntityID defined");
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Set OwnerEntityID to default value");
        return false;
    }

    template <>
    bool parseFromJson(const Json::Value& v,
                       Components::WeaponEntityID& w,
                       std::map<std::string, blueprintID_t>& blueprintNameToIDMap)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Start parsing WeaponEntityID component.");
        if (v.isNull())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3,
                          "Input value is Null. Set WeaponEntityID to default value");
            return false;
        }

        const auto weaponEntityName = v.asString();
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed WeaponEntityName:", weaponEntityName);
        if (blueprintNameToIDMap.find(weaponEntityName) != blueprintNameToIDMap.end())
        {
            w.weaponEntityID = blueprintNameToIDMap[weaponEntityName];
            debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Parsed projectilID:", w.weaponEntityID);
        }
        else
        {
            throw std::invalid_argument("Unknown EntityName '" + weaponEntityName + "'");
        }
        return true;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::DroppedItemEntityID&)
    {
        (void) v;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "No parser for DroppedItemEntityID defined");
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Set DroppedItemEntityID to default value");
        return false;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::PlayerStats&)
    {
        (void) v;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "No parser for PlayerStats defined");
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Set PlayerStats to default value");
        return false;
    }

    template <>
    bool parseFromJson(const Json::Value& v, Components::PlayerBase&)
    {
        (void) v;
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "No parser for PlayerBase defined");
        debugOutLevel(Debug::DebugLevels::firstOrderLoop + 3, "Set PlayerBase to default value");
        return false;
    }

} // namespace ComponentParsing
