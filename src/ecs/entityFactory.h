/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <map>
#include <vector>

#include "entityBlueprint.h"

// forward declarations
class EntityManager;
namespace ComponentParsing
{
    class IComponentParser;
}

class EntityFactory
{
   public:
    void createBlueprintsFromJson(const std::string filename,
                                  EntityManager& entityManager,
                                  const std::map<componentID_t, ComponentParsing::IComponentParser*>& parsers,
                                  std::vector<std::pair<EntityBlueprint, EntityBlueprint::BlueprintType>>& blueprintCollection);
    void registerBlueprintsFromJson(const std::string filename,
                                    std::map<std::string, blueprintID_t>& blueprintNameToIDMap);
    EntityBlueprint::BlueprintType getBlueprintType(std::string blueprintTypeName);
};
