/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMPONENT_FLAGS_H
#define COMPONENT_FLAGS_H

#include <climits> // bytes -> bits via CHAR_BIT
#include <vector>

#include "componentID.h"
#include "../utils/Pow2Assert.h"

namespace Components
{
    using ComponentFlag_t = int32_t;
    constexpr ComponentFlag_t NoComponents = 0;

    template <typename T>
    inline bool hasComponent(const ComponentFlag_t flag)
    {
        constexpr auto cid = static_cast<componentID_t>(ComponentID<T>::CID);
        static_assert(cid < CHAR_BIT * sizeof(ComponentFlag_t),
                      "Too many componentiIDs to fit int32_t -> use a bigger ComponentFlag type!");
        static_assert(cid < NumComponents, "Forgot to update NumComponents!");
        return flag & (static_cast<ComponentFlag_t>(1) << cid);
    }

    template <typename T, typename T2, typename... Args> // hasComponent varidic expansion must have
    // 2 explicit template args (T and T2)
    // otherwise compiler function overload
    // resolution will not work correctly
    // (getting stuck because it chooses
    // hasComponent(T, Args...) with Args... =
    // {})
    inline bool hasComponent(const ComponentFlag_t flag)
    {
        return hasComponent<T>(flag) && hasComponent<T2, Args...>(flag);
    }

    inline bool hasComponent(const ComponentFlag_t flag, const componentID_t componentID)
    {
        auto cid = static_cast<ComponentFlag_t>(componentID);
        POW2_ASSERT(static_cast<size_t>(cid) < CHAR_BIT * sizeof(ComponentFlag_t));
        POW2_ASSERT(static_cast<size_t>(cid) < NumComponents);
        return flag & (static_cast<ComponentFlag_t>(1) << cid);
    }

    template <typename T>
    inline ComponentFlag_t addComponent(const ComponentFlag_t to)
    {
        constexpr auto cid = static_cast<ComponentFlag_t>(ComponentID<T>::CID);
        static_assert(cid < CHAR_BIT * sizeof(ComponentFlag_t),
                      "Too many componentiIDs to fit int32_t -> use a bigger ComponentFlag type!");
        static_assert(cid < NumComponents, "Forgot to update NumComponents!");
        POW2_ASSERT(!hasComponent<T>(to));
        return to | (static_cast<ComponentFlag_t>(1) << cid);
    }

    inline ComponentFlag_t addComponent(const ComponentFlag_t to, const componentID_t componentID)
    {
        POW2_ASSERT(!hasComponent(to, componentID));
        return to | (static_cast<ComponentFlag_t>(1) << static_cast<ComponentFlag_t>(componentID));
    }

    template <typename T>
    inline ComponentFlag_t removeComponent(const ComponentFlag_t from)
    {
        constexpr auto cid = static_cast<ComponentFlag_t>(ComponentID<T>::CID);
        static_assert(cid < CHAR_BIT * sizeof(ComponentFlag_t),
                      "Too many componentiIDs to fit int32_t -> use a bigger ComponentFlag type!");
        static_assert(cid < NumComponents, "Forgot to update NumComponents!");
        POW2_ASSERT(hasComponent<T>(from));
        return from ^ (static_cast<ComponentFlag_t>(1) << cid);
    }

    inline ComponentFlag_t removeComponent(const ComponentFlag_t from, const componentID_t componentID)
    {
        POW2_ASSERT(hasComponent(from, componentID));
        return from ^ (static_cast<ComponentFlag_t>(1) << static_cast<ComponentFlag_t>(componentID));
    }

    std::vector<componentID_t> getComponentIDArrayFromFlag(const ComponentFlag_t flag);

} // namespace Components

#endif /* ifndef COMPONENT_FLAGS_H */
