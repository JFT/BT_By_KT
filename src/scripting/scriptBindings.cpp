/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <functional>
#include <queue>

#include "scriptBindings.hpp"

#include <sol.hpp>

// Add the headers to bind here
#include <irrlicht/IVideoDriver.h>
#include <irrlicht/IVideoModeList.h>
#include <irrlicht/IrrlichtDevice.h>
#include <irrlicht/dimension2d.h>
#include <irrlicht/irrString.h>
#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>

#include "../core/game.h"

#include "../core/config.h"

#include "../ecs/components.h"

#include "../ecs/entityManager.h"

#include "../ecs/events/eventManager.h"
#include "../ecs/events/events.h"
#include "../map/terrainscenenode.h"
#include "../utils/HandleManager.h"

#include <CEGUI/ScriptModule.h>
#include <CEGUI/System.h>
#include "../ecs/systems/luaCallSystem.h"
#include "../ecs/systems/testSystem.h"
#include "../grid/grid.h"
#include "../utils/threadPool.hpp"

#include "scriptBindingHelper.hpp"
#include "scriptingModule.h"

#include <raknet/RakNetTypes.h>
#include "../audio/audioScriptBindings.hpp"
#include "../graphics/graphicsScriptBindings.hpp"

bool createScriptBindings(sol::state* const luastate)
{
    luastate->set("ApplyToAllInstances", false);
    sol::table btTable = luastate->create_named_table("BT");

    // TODO: namespace irrlicht types in lua ?
    luastate->new_usertype<irr::core::vector2df>(
        "vector2df",
        sol::constructors<sol::types<>, sol::types<float, float>, sol::types<const irr::core::vector2df&>>(),
        "X",
        &irr::core::vector2df::X,
        "Y",
        &irr::core::vector2df::Y);


    luastate->new_usertype<irr::core::dimension2du>(
        "dimension2du",
        sol::constructors<sol::types<>, sol::types<irr::u32, irr::u32>, sol::types<const irr::core::vector2d<irr::u32>&>>(),
        "Height",
        &irr::core::dimension2du::Height,
        "Width",
        &irr::core::dimension2du::Width);


    luastate->new_usertype<irr::core::vector3df>(
        "vector3df",
        sol::constructors<sol::types<>, sol::types<float, float, float>, sol::types<const irr::core::vector3df&>>(),
        "getDistanceFromSQ",
        &irr::core::vector3df::getDistanceFromSQ,
        "normalize",
        &irr::core::vector3df::normalize,
        "X",
        &irr::core::vector3df::X,
        "Y",
        &irr::core::vector3df::Y,
        "Z",
        &irr::core::vector3df::Z,
        "setBy",
        &irr::core::vector3df::operator=,
        // operators
        sol::meta_function::addition,
        sol::overload(static_cast<irr::core::vector3df (irr::core::vector3df::*)(const irr::core::vector3df&) const>(
            &irr::core::vector3df::operator+)),
        sol::meta_function::subtraction,
        sol::overload(static_cast<irr::core::vector3df (irr::core::vector3df::*)(const irr::core::vector3df&) const>(
            &irr::core::vector3df::operator-)),
        sol::meta_function::multiplication,
        sol::overload(static_cast<irr::core::vector3df (irr::core::vector3df::*)(const float) const>(
            &irr::core::vector3df::operator*)));

    // Core Modules
    btTable.new_usertype<Game>("Game",
                               "new",
                               sol::no_constructor,
                               SOL_FUN(Game, getDeltaTime),
                               SOL_FUN(Game, getRuntimeMS),
                               SOL_FUN(Game, getTickrate),
                               SOL_FUN(Game, setTickrate),
                               SOL_FUN(Game, getTickNumber),
                               SOL_FUN(Game, findState),
                               SOL_FUN(Game, changeState),
                               SOL_FUN(Game, getGameStarted),
#ifndef MAKE_SERVER_
                               SOL_FUN(Game, getSoundEngine),
#endif
                               SOL_FUN(Game, quit));


    luastate->new_usertype<Config>("Config",
                                   "new",
                                   sol::no_constructor,
                                   SOL_FUN(Config, saveConfig),
                                   SOL_FUN(Config, getSettings),
                                   SOL_FUN(Config, getModifiableSettings),
                                   "defaultSettings",
                                   &Config::defaultSettings);

    luastate->new_usertype<irr::SIrrlichtCreationParameters>("irrlichtParams",
                                                             "new",
                                                             sol::no_constructor,
                                                             "WindowSize",
                                                             &irr::SIrrlichtCreationParameters::WindowSize,
                                                             "Bits",
                                                             &irr::SIrrlichtCreationParameters::Bits,
                                                             "Fullscreen",
                                                             &irr::SIrrlichtCreationParameters::Fullscreen);


    luastate->new_usertype<Config::Settings>("Settings",
                                             "new",
                                             sol::no_constructor,
                                             "irrlichtParams",
                                             &Config::Settings::irrlichtParams,
                                             "soundEnabled",
                                             &Config::Settings::soundEnabled,
                                             "soundVolume",
                                             &Config::Settings::soundVolume,
                                             "playerName",
                                             &Config::Settings::playerName,
                                             "serverName",
                                             &Config::Settings::serverName,
                                             "serverPort",
                                             &Config::Settings::serverPort,
                                             "serverPassword",
                                             &Config::Settings::serverPassword);

    luastate->new_usertype<irr::IrrlichtDevice>("IrrlichtDevice",
                                                "new",
                                                sol::no_constructor,
                                                "getVideoModeList",
                                                &irr::IrrlichtDevice::getVideoModeList);

    luastate->new_usertype<irr::video::IVideoDriver>("IVideoDriver",
                                                     "new",
                                                     sol::no_constructor,
                                                     "getScreenSize",
                                                     &irr::video::IVideoDriver::getScreenSize);


    luastate->new_usertype<irr::video::IVideoModeList>(
        "IVideoModeList",
        "new",
        sol::no_constructor,
        "getVideoModeCount",
        &irr::video::IVideoModeList::getVideoModeCount,
        "getVideoModeDepth",
        &irr::video::IVideoModeList::getVideoModeDepth,
        "getDesktopDepth",
        &irr::video::IVideoModeList::getDesktopDepth,
        "getVideoModeResolution",
        sol::overload(static_cast<irr::core::dimension2du (irr::video::IVideoModeList::*)(irr::s32) const>(
                          &irr::video::IVideoModeList::getVideoModeResolution),
                      static_cast<irr::core::dimension2du (irr::video::IVideoModeList::*)(
                          const irr::core::dimension2d<irr::u32>&, const irr::core::dimension2d<irr::u32>&) const>(
                          &irr::video::IVideoModeList::getVideoModeResolution)));


    btTable.new_usertype<irr::scene::ITerrainSceneNode>("TerrainSceneNode",
                                                        "new",
                                                        sol::no_constructor,
                                                        "getHeight",
                                                        &irr::scene::ITerrainSceneNode::getHeight);

    btTable.new_usertype<grid::Grid>(
        "Grid",
        "new",
        sol::no_constructor,
        "getCellID",
        sol::overload(static_cast<grid::gridCellID_t (grid::Grid::*)(const irr::core::vector3df&) const>(
            &grid::Grid::getCellID)));

    btTable.new_usertype<Components::Movement>("Movement",
                                               "new",
                                               sol::no_constructor,
                                               "waypoints",
                                               &Components::Movement::waypoints,
                                               "speed",
                                               &Components::Movement::speed);

    btTable.new_usertype<Handles::HandleManager>(
        "HandleManager",
        "new",
        sol::no_constructor,
        "isValid",
        &Handles::HandleManager::isValid,
        "getAsFloatPtr",
        &Handles::HandleManager::getAsPtr<irr::f32>,
        "getAsFloatValue",
        &Handles::HandleManager::getAsValue<irr::f32>,
        "setAsFloat",
        &Handles::HandleManager::setAs<irr::f32>,
        "getAsVector3dfPtr",
        &Handles::HandleManager::getAsPtr<irr::core::vector3df>,
        "getAsVector3dfValue",
        &Handles::HandleManager::getAsValue<irr::core::vector3df>,
        "setAsVector3df",
        &Handles::HandleManager::setAs<irr::core::vector3df>,
        "getAsQueueVector3dfPtr",
        &Handles::HandleManager::getAsPtr<std::queue<irr::core::vector3df>>,
        "getAsQueueVector3dfValue",
        &Handles::HandleManager::getAsValue<std::queue<irr::core::vector3df>>,
        "setAsQueueVector3df",
        &Handles::HandleManager::setAs<std::queue<irr::core::vector3df>>,
        "getAsMovementPtr",
        &Handles::HandleManager::getAsPtr<Components::Movement>,
        "getAsMovementValue",
        &Handles::HandleManager::getAsValue<Components::Movement>);

    btTable.new_usertype<EventManager>("EventManager",
                                       "new",
                                       sol::no_constructor,
                                       "emit_PositionChanged",
                                       &EventManager::emit<Events::PositionChanged>,
                                       "emit_IntermediateWaypointReached",
                                       &EventManager::emit<Events::IntermediateWaypointReached>,
                                       "emit_FinalWaypointReached",
                                       &EventManager::emit<Events::FinalWaypointReached>);

    btTable.new_usertype<EntityManager>(
        "EntityManager",
        "new",
        sol::no_constructor,
        "getComponent",
        sol::overload(static_cast<Handles::Handle (EntityManager::*)(const entityID_t, const componentID_t) const>(
            &EntityManager::getComponent)),
        "getComponentIDFromName",
        &EntityManager::getComponentIDFromName,
        "getComponentNameFromID",
        &EntityManager::getComponentNameFromID,
        "addComponent",
        static_cast<Handles::Handle (EntityManager::*)(const entityID_t, const componentID_t)>(&EntityManager::addComponent),
        "bindSystem",
        [](EntityManager& etm, SystemBase* system) {
            std::unique_ptr<SystemBase> sys_ptr(system);
            etm.bindSystem(sys_ptr);
        });
    //"removeComponent",
    //&EntityManager::removeComponent);

    btTable.new_usertype<TestSystem>("TestSystem",
                                     "new",
                                     sol::no_constructor,
                                     SOL_FUN(TestSystem, activateSystem),
                                     SOL_FUN(TestSystem, deactivateSystem));

    btTable.new_usertype<ThreadPool>("ThreadPool", "new", sol::no_constructor, SOL_FUN(ThreadPool, getNumberOfWorkerThreads));

    btTable.new_usertype<ScriptingModule>("ScriptingModule",
                                          "new",
                                          sol::no_constructor,
                                          SOL_FUN(ScriptingModule, loadScriptFile),
                                          SOL_FUN(ScriptingModule, reloadScript),
                                          SOL_FUN(ScriptingModule, reloadScripts));


    btTable.new_usertype<LuaCallSystem>(
        "LuaCallSystem",
        "new",
        sol::no_constructor,
        "create", // using 'create' to indicate that this memory won't be managed by lua
        [](const Systems::SystemIdentifiers identifier,
           ThreadPool& threadPool,
           EventManager& eventManager,
           const std::string& luaBaseName,
           ScriptingModule& scriptingModule) {
            return new LuaCallSystem(identifier, threadPool, eventManager, luaBaseName, scriptingModule);
        });

#ifndef MAKE_SERVER_
    // here create Audio and Graphics bindings
    createAudioBindings(btTable);
    createGraphicsBindings(btTable);
#endif

    return true;
}
