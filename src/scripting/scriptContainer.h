/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCRIPTCONTAINER_H_
#define SCRIPTCONTAINER_H_


#include <map>
#include <vector>
#include "../utils/static/error.h" // defines ErrCode

class ScriptingModule;

/// TODO: Check for usage of the scriptContainer - possibilities for function names in json file??

class ScriptContainer
{
   public:
    ScriptContainer();
    ~ScriptContainer();

    ErrCode initialize(ScriptingModule* const module);

    inline std::string getScriptfromID(size_t id) { return scripts[id].second; }
    inline std::string getScriptNameFromID(size_t id) { return scripts[id].first; }
    inline size_t getScriptCount() { return scripts.size(); }
    /// Overrides the script at id with a new one
    ///
    /// @param script the new scriptFile name
    /// @param id ID of the Object to override
    /// @return NO_ERR for success and GENERIC_ERR for failure
    ErrCode setScriptAtID(const std::string scriptFile, size_t id);

    /// Obtain the ID from the ScriptName
    ///
    /// @param fname name/path of the script
    /// @return ID of the Script in the container or -1 if it doesn't exist
    size_t getIDFromScriptName(std::string fname);


   private:
    // vector containing scripts - every Object has a unique ID -> get Script via vect[ID]
    // it contains first: scriptname ,second name
    std::vector<std::pair<std::string, std::string>> scripts;
    // map only used for a more convenient reload
    std::map<std::string, size_t> scriptIDMap;
};
#endif // SCRIPTCONTAINER_H_
