/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2018 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#define EXPAND(x) x
// Make a FOREACH macro
#define FE_1(WHAT, X1, X2) EXPAND(WHAT(X1, X2))
#define FE_2(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_1(WHAT, X1, __VA_ARGS__))
#define FE_3(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_2(WHAT, X1, __VA_ARGS__))
#define FE_4(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_3(WHAT, X1, __VA_ARGS__))
#define FE_5(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_4(WHAT, X1, __VA_ARGS__))
#define FE_6(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_5(WHAT, X1, __VA_ARGS__))
#define FE_7(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_6(WHAT, X1, __VA_ARGS__))
#define FE_8(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_7(WHAT, X1, __VA_ARGS__))
#define FE_9(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_8(WHAT, X1, __VA_ARGS__))
#define FE_10(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_9(WHAT, X1, __VA_ARGS__))
#define FE_11(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_10(WHAT, X1, __VA_ARGS__))
#define FE_12(WHAT, X1, X2, ...) WHAT(X1, X2) EXPAND(FE_11(WHAT, X1, __VA_ARGS__))

#define GET_MACRO(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, NAME, ...) NAME
#define FOR_EACH(action, arg, ...)                                                                            \
    EXPAND(GET_MACRO(__VA_ARGS__, FE_12, FE_11, FE_10, FE_9, FE_8, FE_7, FE_6, FE_5, FE_4, FE_3, FE_2, FE_1)( \
        action, arg, __VA_ARGS__))


#define SOL_FUN(Class, Name) #Name, &Class::Name

#define SOL_FUN_C(Class, Name) , #Name, &Class::Name

#define SOL_ENUM_MEMBER(Enum, Name) , #Name, Enum::Name


#define SOL_ENUM(Table, Enum, ...) \
    Table.new_enum(#Enum FOR_EACH(SOL_ENUM_MEMBER, Enum, __VA_ARGS__))

#define SOL_FUNS(Class, ...) FOR_EACH(SOL_FUN_C, Class, __VA_ARGS__)