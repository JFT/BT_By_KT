/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>

#include "scriptBindings.hpp"
#include "scriptContainer.h"
#include "scriptingModule.h"

// headers containing only static functions can be included in .cpp files
#include <fstream>
#include "../utils/static/conversions.h"
#include "../utils/threadPool.hpp"


ScriptEngine::ScriptEngine(ThreadPool* tp)
{
    luaInstances.reserve(tp->getNumberOfWorkerThreads());
    for (size_t i = 0; i < tp->getNumberOfWorkerThreads(); ++i)
    {
        luaInstances.emplace_back(sol::state());
        luaInstances[i].open_libraries(sol::lib::base,
                                       sol::lib::package,
                                       sol::lib::io,
                                       sol::lib::os,
                                       sol::lib::string,
                                       sol::lib::table,
                                       sol::lib::math,
                                       sol::lib::coroutine,
                                       sol::lib::debug);
        luaInstances[i]["inSingleInstance"] = false;
        luaInstances[i]["thisInstanceID"] = static_cast<int>(i);
        threadIDLuaInstanceMap[tp->getThreadIDOfWorker(i)] = static_cast<int>(i);
        createScriptBindings(&luaInstances[i]);
    }
    // must do the mapping AFTER pushing onto the instance
    // in case the vector gets resized and moves the instances to different
    // addresses
    for (size_t i = 0; i < tp->getNumberOfWorkerThreads(); ++i)
    {
        this->instanceToIDMapping[static_cast<void*>(&this->luaInstances[i])] = static_cast<int>(i);
    }

    // inSingleInstance can be used to execute code
    // inside a script in only one lua instance (e.g.
    // code which changes the gamestate).
    // By defining marking the LAST instance
    // with 'inSingleInstance' we make sure that
    // executing a script file like this:

    // function test_constructor(boundComponents)
    // end
    //
    // if (inSingleInstance) then
    //     LuaCallSystem.create(16, threadPool, eventManager, "test_", scriptingModule)
    // end

    // executes correctly
    // Because the ctor of LuaCallSystem forces a loop over all instances,
    // calling test_constructor() (this allows a lua-defined
    // ECS system to e.g. initialize local variables in all
    // luaInstances)
    // If the system was created in the first instance instead,
    // test_constructor would be undefined in all other instances
    // and the code would fail.

    (*luaInstances.rbegin())["inSingleInstance"] = true;
}

// todo: if an error occurs output more information
ErrCode ScriptEngine::loadFile(const std::string fname)
{
    bool error = false;
    this->loopOverInstances([this, fname, &error](sol::state& instance) {
        sol::load_result s = instance.load_file(fname);
        if (!s.valid())
        {
            error = true;
            const sol::error err = s;
            Error::errContinue("instance:",
                               this->instanceToIDMapping[&instance],
                               "error compiling lua script",
                               fname,
                               "message:",
                               err.what());
            // using singleInstance() to print this message makes it appear on the ScriptConsole
            // (because print() gets captured there) printing one message per instance
            // TODO: find out if one message per instance really neccecary or if syntax always
            // instance-independent
            this->singleInstance().script(
                "print(\"Instance: " + std::to_string(this->instanceToIDMapping[&instance]) +
                ": error compiling lua script '" + fname + "'\\n message: '" + err.what() + "'\")");
        }
        else
        {
            s();
        }
    });

    return (error ? ErrCodes::GENERIC_ERR : ErrCodes::NO_ERR);
}

ErrCode ScriptEngine::loadScript(const std::string script)
{
    bool error = false;
    this->loopOverInstances([this, script, &error](sol::state& instance) {
        sol::load_result s = instance.load(script);
        if (!s.valid())
        {
            error = true;
            const sol::error err = s;
            Error::errContinue("instance:",
                               this->instanceToIDMapping[&instance],
                               "error compiling lua script",
                               script,
                               "message:",
                               err.what());
            // using singleInstance() to print this message makes it appear on the ScriptConsole
            // (because print() gets captured there) printing one message per instance
            // TODO: find out if one message per instance really neccecary or if syntax always
            // instance-independent
            this->singleInstance().script(
                "print(\"Instance: " + std::to_string(this->instanceToIDMapping[&instance]) +
                ": error compiling lua script '" + script + "'\\n message: '" + err.what() + "'\")");
        }
        else
        {
            s();
        }
    });
    return (error ? ErrCodes::GENERIC_ERR : ErrCodes::NO_ERR);
}


ScriptingModule::ScriptingModule(ThreadPool* tp)
    : scriptContainer(new ScriptContainer())
    , scriptEngine(tp)
{
    scriptContainer->initialize(this);
}

ScriptingModule::~ScriptingModule() {}

ErrCode ScriptingModule::loadScriptFile(const std::string fname)
{
    return scriptEngine.loadFile(fname);
}

ErrCode ScriptingModule::reloadScripts(bool allScripts, size_t ID)
{
    // Todo: insert errorchecking in getScriptNameFromID/getIDFromScriptName etc.
    // TODO: scriptContainer setScriptAtID - is obsolete
    bool error = false;
    if (allScripts)
    {
        debugOutLevel(Debug::DebugLevels::firstOrderLoop - 5, "Reloading all Scripts");
        for (size_t i = 0; i < scriptContainer->getScriptCount(); ++i)
        {
            if (loadScriptFile(scriptContainer->getScriptNameFromID(i)) != NO_ERR)
            {
                Error::errContinue("Reloading File: ", scriptContainer->getScriptNameFromID(i), " failed!");
                error = true;
            }
        }

        if (error)
        {
            return GENERIC_ERR;
        }
    }
    else
    {
        if (ID != static_cast<size_t>(-1) && ID < scriptContainer->getScriptCount())
        {
            debugOutLevel(Debug::DebugLevels::firstOrderLoop - 5,
                          "Reloading Script:",
                          scriptContainer->getScriptNameFromID(ID));
            return loadScriptFile(scriptContainer->getScriptNameFromID(ID));
        }
        else
        {
            Error::errContinue("can't find the file/ID to recompile - did you compile it before?");
            return GENERIC_ERR;
        }
    }
    debugOutLevel(Debug::DebugLevels::firstOrderLoop - 5, "Reloading Script(s) successful");
    return NO_ERR;
}

ErrCode ScriptingModule::reloadScript(std::string fname)
{
    size_t scriptID = scriptContainer->getIDFromScriptName(fname);
    if (scriptID == static_cast<size_t>(-1))
    {
        Error::errContinue("can't find the file: ", fname, "to recompile - did you compile it before?");
        return GENERIC_ERR;
    }

    return this->reloadScripts(false, scriptID);
}
