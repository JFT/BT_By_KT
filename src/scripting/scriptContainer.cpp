/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scriptContainer.h"
#include <fstream>
#include "scriptingModule.h"
#include "../utils/debug.h"
#include "../utils/json.h"
#include "../utils/static/error.h"

ScriptContainer::ScriptContainer()
    : scripts(std::vector<std::pair<std::string, std::string>>())
    , scriptIDMap(std::map<std::string, size_t>())
{
}


ScriptContainer::~ScriptContainer() {}


ErrCode ScriptContainer::initialize(ScriptingModule* module)
{
    std::ifstream file;
    file.open("scripts/objectScripts.json");
    if (!file.is_open())
    {
        Error::errTerminate("could not load \"script/objectScripts.json\"");
        return GENERIC_ERR;
    }

    Json::Value jVal;
    file >> jVal;

    scripts.resize(jVal["ScriptObject"].size());
    for (auto it = jVal["ScriptObject"].begin(); it != jVal["ScriptObject"].end(); ++it)
    {
        if (module->loadScriptFile((*it)["script"].asString()) == NO_ERR)
        {
            scripts[(*it)["id"].asUInt()] =
                std::pair<std::string, std::string>((*it)["script"].asString(), (*it)["name"].asString());
            scriptIDMap[(*it)["script"].asString()] = (*it)["id"].asUInt();
        }
    }
    file.close();
    return NO_ERR;
}

ErrCode ScriptContainer::setScriptAtID(const std::string scriptFile, size_t id)
{
    if (id > scripts.size())
    {
        return GENERIC_ERR;
    }
    scripts[id].first = std::move(scriptFile);

    return NO_ERR;
}


size_t ScriptContainer::getIDFromScriptName(std::string fname)
{
    auto it = scriptIDMap.find(fname);
    if (it == scriptIDMap.end())
    {
        Error::errContinue("File: ", fname, " could not be found in scriptIDMap. Returning -1");
        return -1;
    }
    return it->second;
}
