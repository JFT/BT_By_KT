/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SRC_SCRIPTING_SCRIPTINGMODULE_H_
#define SRC_SCRIPTING_SCRIPTINGMODULE_H_

#include <functional>
#include <map>
#include <sol.hpp>
#include <string>
#include <thread>
#include <vector>
#include "../utils/debug.h"
#include "../utils/static/error.h" // defines ErrCode

class ThreadPool;
class ScriptContainer;

// even though it is named sol::state we will call it
// 'instance' internally to differentiate between sol::states
// and gamestates (see. core/statemanager.h or  folder states/)

class ScriptEngine
{
   public:
    enum class ForceLoop
    {
        True,
        False
    };

    explicit ScriptEngine(ThreadPool* tp);
    ScriptEngine(const ScriptEngine&) = delete;

    ScriptEngine& operator=(const ScriptEngine&) = delete;

    sol::state& getSolState(uint16_t id)
    {
        assert(id < this->luaInstances.size());
        return this->luaInstances[id];
    }

    sol::state& operator()(const std::thread::id& id)
    {
        return luaInstances[threadIDLuaInstanceMap[id]];
    }
    ErrCode loadScript(const std::string script);
    ErrCode loadFile(const std::string fname);

    sol::state& singleInstance() { return this->luaInstances.front(); }

    /// @brief loop over all lua instances and executes 'function' with the instance as its argument
    /// defaults to allowing only one loop, calling loopOverInstances() while still inside
    /// loopOverInstances() will not loop but execute 'function' with the lua instance of the outer
    /// loop. This makes it possible to expose functions to lua which might start recursive loops
    /// because these loops are stopped and only the outermost loop stays.
    /// @param function function to call with each instance as argument
    /// WARNING: make sure you know what you are doing if 'function' internally
    /// discriminates between different instances,
    /// otherwise the recursion stopping mechanism might break your logic
    /// in unexpected ways.
    /// @param force set to ForceLoop::True if 'function' should always be called for all instances,
    /// even if an outer loop is active. Use e.g. in object constructors or gamestate onEnter()
    /// functions which are ensured by other means to be called only once. Usage example is
    /// executing a script file (-> this generates an outer loop over all instances) which changes
    /// the gamestate, which in tern must expose e.g. the HandeManager to all instances. The
    /// exposing-loop will be called with ForceLoop::True because even if the gamestate is changed
    /// multiple times (due to the outer loop) the HandleManager pointer will change each time,
    /// meaning the inner loop can't just write it once in each instances.
    void loopOverInstances(std::function<void(sol::state&)> function, const ForceLoop force = ForceLoop::False)
    {
        if (this->currentLoopingInstance != nullptr && force == ForceLoop::False)
        {
            // if we are already looping over all instances we will perform
            // the action we probably will call loopOverInstances
            // with the same 'function' in all instances -> no
            // need to loop over the instances again.
            // Simply call the function for the current outermost looping instance and be done.
            debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                          "only looping for outer instance:",
                          static_cast<void*>(this->currentLoopingInstance));
            function(*this->currentLoopingInstance);
            return;
        }
        // either we are currently not looping or a loop was forced:
        // if not looping: currentLoopingInstance needs to be changed inside the loop.
        // if forced: leave currentLoopingInstance as it is.
        const bool inForcedLoop = this->currentLoopingInstance != nullptr && force == ForceLoop::True;
        // must catch exceptions here because if an exception is thrown
        // while inside this loop
        // currentLoopingInstance will never be reset and
        // subsequent calls to loopOverInstances will only
        // execute in the luaInstance which threw the exception.
        try
        {
            for (auto& instance : this->luaInstances)
            {
                if (not inForcedLoop)
                {
                    this->currentLoopingInstance = &instance;
                }
                debugOutLevel(Debug::DebugLevels::secondOrderLoop,
                              "looping for INNER instance:",
                              static_cast<void*>(&instance),
                              "forced:",
                              inForcedLoop);
                function(instance);
            }
        }
        catch (const std::exception&) // unused variables don't need a name
        {
            if (not inForcedLoop)
            {
                // we have changed the currentLoopingInstance away from nullptr inside the loop,
                // reset it back again.
                debugOutLevel(Debug::DebugLevels::secondOrderLoop, "resetting currentLoopingInstance to nullptr");
                this->currentLoopingInstance = nullptr;
            }
            // re-throw the exception and let the caller handle it
            throw;
        }
        if (not inForcedLoop)
        {
            // we have changed the currentLoopingInstance away from nullptr inside the loop,
            // reset it back again.
            debugOutLevel(Debug::DebugLevels::secondOrderLoop, "resetting currentLoopingInstance to nullptr");
            this->currentLoopingInstance = nullptr;
        }
    }

    void runInSingleInstance(uint16_t id, std::function<void(sol::state&)> function)
    {
        // TODO: do we need an assert for id checking?
        function(this->getSolState(id));
    }

   private:
    sol::state* currentLoopingInstance = nullptr;

    std::vector<sol::state> luaInstances = {};
    std::map<std::thread::id, int> threadIDLuaInstanceMap = {};

    std::unordered_map<void*, int> instanceToIDMapping = {};
};


class ScriptingModule
{
   public:
    explicit ScriptingModule(ThreadPool* tp);
    virtual ~ScriptingModule();
    ScriptingModule(const ScriptingModule&) = delete;
    ScriptingModule operator=(const ScriptingModule&) = delete;

    /// Loads a Scriptfile
    /// Automatically adds the script to the database
    /// @param fname ("path/filename")
    /// @return true if success/ false if failure
    ErrCode loadScriptFile(const std::string fname);

    /// Reloads a Scriptfile
    ///
    /// @param fname filename of the file you want to reload
    /// @return NO_ERR if successful and GENERIC_ERR if it fails
    ErrCode reloadScript(std::string fname);

    /// Reloads the Scriptfile(s)
    /// If you reload all scripts - it can take a some time to do so
    ///
    /// @param allScripts if false you need to pass an ID, if true it will reload all scripts it
    /// loaded until now
    /// @param ID ObjectID of the Object if you only want to reload one file
    /// @return NO_ERR if successful and GENERIC_ERR if it fails
    ErrCode reloadScripts(bool allScripts, size_t ID = -1);

    // Getter ---
    inline ScriptContainer* getScriptContainer() { return scriptContainer; }
    inline ScriptEngine& getScriptEngine() { return scriptEngine; }

   protected:
   private:
    ScriptContainer* scriptContainer;

    ScriptEngine scriptEngine;
};

#endif /* SRC_SCRIPTING_SCRIPTINGMODULE_H_ */
