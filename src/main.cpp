/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if defined MAKE_SERVER_
#include "core/server/app_server.h"
#else
#include "core/client/app.h"
#endif

#include "utils/debug.h"

#if defined(MAKE_SERVER_)
int main()
{
    loadDebugLevels(); // initialize debug Code (this call will be optimized away if compiled for
                       // release)

    printf("Starting Server");

    AppServer app;
    ErrCode err = app.init();
    app.run();
    return err;
#else
int main(int argumentCount, char const** arguments)
{
    loadDebugLevels(); // initialize debug Code (this call will be optimized away if compiled for
                       // release)

    bool stdinToConsole = false;
    if (argumentCount > 1)
    {
        if (std::string(arguments[1]).compare("--console") == 0)
        {
            stdinToConsole = true;
        }
    }
    App app;
    ErrCode err = app.init();
    app.run(stdinToConsole);
    return err;
#endif
    return 0;
}
