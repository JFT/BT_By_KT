/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ceGuiEnvironment.h"

#include <algorithm> // std::find
#include <cctype>    // toupper


#include <CEGUI/AnimationManager.h>
#include <CEGUI/DefaultResourceProvider.h>
#include <CEGUI/FontManager.h>
#include <CEGUI/ImageManager.h>
#include <CEGUI/SchemeManager.h>
#include <CEGUI/ScriptModule.h>
#include <CEGUI/ScriptModules/Lua/ScriptModule.h>
#include <CEGUI/Window.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/XMLParser.h>
#include <CEGUI/falagard/WidgetLookManager.h>
#include <CEGUI/widgets/Tooltip.h>
#include <sol.hpp>

#include <CEGUI/BasicImage.h>
#include <CEGUI/RendererModules/Irrlicht/Texture.h>
#include <irrlicht/ITexture.h>

#include <sol/state.hpp>

#include "../core/game.h"
#include "../scripting/scriptingModule.h"
#include "../utils/algos.h"

// clang-format off
#include "mapeditorstate/mapEditorGui.h"  //TODO remove
// clang-format on

CEGUIEnvironment::CEGUIEnvironment(irr::IrrlichtDevice* device_)
    : myRenderer(&CEGUI::IrrlichtRenderer::bootstrapSystem(*(device_)))
    , guiRoot(CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "RootWindow"))
{
    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(guiRoot);
    this->guiRoot->setMousePassThroughEnabled(true);
}

// ---------------------------------------------------------------------------
CEGUIEnvironment::~CEGUIEnvironment()
{
    // CEGUI::WindowManager::getSingleton().destroyAllWindows();
    this->myRenderer->destroySystem();
}

// ---------------------------------------------------------------------------
void CEGUIEnvironment::initGui(irr::IrrlichtDevice* device_, sol::state* luaState)
{
    CEGUI::DefaultResourceProvider* const rp =
        static_cast<CEGUI::DefaultResourceProvider*>(CEGUI::System::getSingleton().getResourceProvider());

    // clang-format off
    rp->setResourceGroupDirectory("fonts",       "media/cegui/fonts/");
    rp->setResourceGroupDirectory("imagesets",   "media/cegui/imagesets/");
    rp->setResourceGroupDirectory("schemes",     "data/cegui/schemes/");
    rp->setResourceGroupDirectory("looknfeels",  "data/cegui/looknfeel/");
    rp->setResourceGroupDirectory("layouts",     "data/cegui/layouts/");
    rp->setResourceGroupDirectory("lua_scripts", "data/cegui/lua_scripts/");
    rp->setResourceGroupDirectory("animations",  "data/cegui/animations/");
    // clang-format on

    // set the default resource groups to be used
    CEGUI::Font::setDefaultResourceGroup("fonts");
    CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
    CEGUI::Scheme::setDefaultResourceGroup("schemes");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
    CEGUI::WindowManager::setDefaultResourceGroup("layouts");
    CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");
    CEGUI::AnimationManager::setDefaultResourceGroup("animations");

    // setup default group for validation schemas
    CEGUI::XMLParser* parser = CEGUI::System::getSingleton().getXMLParser();
    if (parser->isPropertyPresent("SchemaDefaultResourceGroup"))
    {
        parser->setProperty("SchemaDefaultResourceGroup", "schemas");
    }

    // create (load) the BT scheme file
    // (this auto-loads the BT looknfeel, imageset and font files)
    CEGUI::SchemeManager::getSingleton().createFromFile("BT.scheme");
    CEGUI::SchemeManager::getSingleton().createFromFile("GWEN.scheme");


    // The first font loaded automatically becomes the default font, but note
    // that the scheme might have already loaded a font, so there may already
    // be a default set - if we want the "Protoculture Bold-14" font to definitely
    // be the default, we should set the default explicitly afterwards.
    CEGUI::FontManager::getSingleton().createFromFile("Protoculture Bold-14.font");

    CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultTooltipType("BT/Tooltip");

    if (luaState)
    {
        CEGUI::LuaScriptModule& module = CEGUI::LuaScriptModule::create(luaState->lua_state());
        CEGUI::System::getSingleton().setScriptingModule(&module);
        auto btTable = luaState->get<sol::table>("BT");
        btTable.set_function("gui", [](const std::string s) {
            if (!CEGUI::System::getSingleton().getScriptingModule())
            {
                std::cerr << "NO SCRIPTINGMODULE!" << std::endl;
                return;
            }
            CEGUI::System::getSingleton().getScriptingModule()->executeString(s);
        });
    }

    (void) device_;
    // Set new CursorImage
    /*device_->getCursorControl()->setVisible(false); //Hide default Cursor
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("BT/MouseArrow");*/
}


// ---------------------------------------------------------------------------
void CEGUIEnvironment::drawAll(float timeElapsed_)
{
    // we have to inject time pulses into both guicontext and system
    CEGUI::System::getSingleton().injectTimePulse(timeElapsed_);
    CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(timeElapsed_);
    CEGUI::System::getSingleton().renderAllGUIContexts();
}

void CEGUIEnvironment::notifyDisplaySizeChanged(const int windowWidth, const int windowHeight)
{
    const CEGUI::Size<int> currentWindowSize(windowWidth, windowHeight);
    if (currentWindowSize != this->oldWindowSize)
    {
        CEGUI::System::getSingleton().notifyDisplaySizeChanged(
            CEGUI::Size<float>(static_cast<float>(windowWidth), static_cast<float>(windowHeight)));
        this->oldWindowSize = currentWindowSize;
    }
}

// TODO clean code
std::string CEGUIEnvironment::getShortTexturePath(irr::video::ITexture* const texture, const std::string pathRoot) const
{
    std::string fullTexturePath = irr::core::stringc(texture->getName()).c_str();

    auto formatLength = 0; // dont remove .png or .jpg
    auto pathToRootLength = fullTexturePath.find(pathRoot.c_str());
    if (pathRoot == "name") // TODO find a better way to get just the name maybe with a bool
    {
        pathToRootLength = fullTexturePath.rfind('/') + 1;
        formatLength = 4; // remove .png or .jpg
    }

    std::string shortTexturePath =
        fullTexturePath.substr(pathToRootLength, fullTexturePath.size() - pathToRootLength - formatLength);
    return shortTexturePath;
}

// TODO clean code
// ---------------------------------------------------------------------------
void CEGUIEnvironment::setImageToWindow(CEGUI::Window* const myImageWindow,
                                        irr::video::ITexture* const myIrrlichtTexture,
                                        CEGUI::String const imageType) // testing
{
    debugOutLevel(20, "setImageToWindow");
    // CEGUI::Editbox* imagepath =
    // static_cast<CEGUI::Editbox*>(this->toolBoxPageTexturing->getChild("Label_ItemName"));
    std::string textureName = this->getShortTexturePath(myIrrlichtTexture, "media");
    // imagepath->setText(textureName);

    CEGUI::IrrlichtTexture* myCeGuiIrrTexture = nullptr;
    CEGUI::BasicImage* image = nullptr;
    if (this->myRenderer->isTextureDefined(textureName))
    {
        debugOutLevel(25, textureName, "is defined");
        myCeGuiIrrTexture =
            static_cast<CEGUI::IrrlichtTexture*>(&(this->myRenderer->getTexture(textureName)));
        image = static_cast<CEGUI::BasicImage*>(&CEGUI::ImageManager::getSingleton().get(textureName));
    }
    else
    {
        debugOutLevel(25, "define", textureName);
        myCeGuiIrrTexture =
            static_cast<CEGUI::IrrlichtTexture*>(&(this->myRenderer->createTexture(textureName)));
        image = static_cast<CEGUI::BasicImage*>(
            &CEGUI::ImageManager::getSingleton().create("BasicImage", textureName));
    }
    myCeGuiIrrTexture->setIrrlichtTexture(myIrrlichtTexture);

    bool isTextureTargetVerticallyFlipped = this->myRenderer->isTexCoordSystemFlipped();
    CEGUI::Rectf imageArea;

    float textureWidth = float(fabs(myImageWindow->getWidth().d_scale));
    float textureHeight = float(fabs(myImageWindow->getHeight().d_scale));

    if (isTextureTargetVerticallyFlipped)
    {
        imageArea = CEGUI::Rectf(textureHeight, 0.0f, 0.0f, textureWidth);
    }
    else
    {
        imageArea = CEGUI::Rectf(0.0f, 0.0f, 1000.0f, 1000.0f);
    }
    // image.set setPosition();
    // debugOut(imageArea.getWidth(), imageArea.getHeight());
    // debugOut(myImageWindow->getHitTestRect().getWidth(),
    // myImageWindow->getHitTestRect().getHeight());
    // This is necessary to define the image's size. Regardless of flipping or not you need to set
    // the size. Multiple images can be associated with a single texture.
    image->setArea(imageArea);
    //image->setArea(myImageWindow->getRenderedSize());//myImageWindow->getHitTestRect()
    // You most likely don't want autoscaling for RTT images. If you display it in stretched-mode
    // inside a button or Generic/Image widget, then this setting does not play a role anyways.
    image->setAutoScaled(CEGUI::ASM_Horizontal);
    image->setTexture(myCeGuiIrrTexture);
    myImageWindow->setProperty(imageType, textureName);
}

// ---------------------------------------------------------------------------
void CEGUIEnvironment::initGuiElement(const std::string name)
{
    if (not contains(this->initializedGuiElements, name))
    {
        std::string file = name;
        // all script filenames begin with a lowercase letter
        file[0] = std::tolower(file[0]);
        file += ".lua";
        debugOutLevel(20, "load " + name + " luaScript'", file, "'");
        CEGUI::System::getSingleton().executeScriptFile(file);
        CEGUI::System::getSingleton().executeScriptString(name + ".create()");
        this->initializedGuiElements.push_back(name);
    }
    else
    {
        debugOutLevel(20, "'", name, "' already loaded, so just set visible");
        CEGUI::System::getSingleton().getScriptingModule()->executeString(name + ".root:setVisible(true)");
    }
}

void CEGUIEnvironment::destructGuiElement(const std::string name)
{
    auto elem = find(this->initializedGuiElements, name);
    if (elem != this->initializedGuiElements.end())
    {
        debugOutLevel(20, "destruct GuiElement '", name, "'");
        CEGUI::System::getSingleton().getScriptingModule()->executeString(name + ".delete()");
        this->initializedGuiElements.erase(elem);
    }
    else
    {
        Error::errTerminate("Can't destruct GuiElement '", name, "'");
    }
}

// ---------------------------------------------------------------------------
MapEditorGui* CEGUIEnvironment::createMapEditorGui(const std::string file, Game* const game)
{
    if (this->mapEditorGuiObj == nullptr)
    {
        debugOutLevel(20, "create new mapEditorGuiObj");
        this->mapEditorGuiObj = new MapEditorGui(file, game);
        return mapEditorGuiObj;
    }
    else
    {
        debugOutLevel(20, "'mapEditorGuiObj' already existing, so just set visible");
        this->mapEditorGuiObj->getRoot()->setVisible(true);
        return this->mapEditorGuiObj;
    }
}
// ---------------------------------------------------------------------------
void CEGUIEnvironment::deleteMapEditorGui()
{
    debugOutLevel(20, "delete MapEditorGuiObj");
    if (this->mapEditorGuiObj != nullptr)
    {
        delete this->mapEditorGuiObj;
        this->mapEditorGuiObj = nullptr;
    }
}
