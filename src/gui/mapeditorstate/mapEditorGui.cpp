/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mapEditorGui.h"
// ---------------------------------------------------------------------------

#include <CEGUI/BasicImage.h>
#include <CEGUI/ImageManager.h>
#include <CEGUI/RendererModules/Irrlicht/Texture.h>
#include <CEGUI/ScriptModules/Lua/ScriptModule.h>
#include <CEGUI/widgets/Editbox.h>
#include <CEGUI/widgets/MenuItem.h>
#include <CEGUI/widgets/PushButton.h>
#include <CEGUI/widgets/Spinner.h>
#include <CEGUI/widgets/TabControl.h>
#include <CEGUI/widgets/ToggleButton.h>
#include <CEGUI/widgets/Tree.h>
#include <CEGUI/widgets/TreeItem.h>
#include <irrlicht/IFileSystem.h>
#include <irrlicht/IrrlichtDevice.h>
#include <sol.hpp>
#include "../../utils/stringwstream.h" // stringwstream.h needs to be included before debug.h because debug.h uses operator<< on a stringw

#include "../ceGuiEnvironment.h"
#include "../multiplestates/messageBoxGui.h"
#include "../../core/game.h"
#include "../../ecs/components.h"
#include "../../ecs/entity.h"
#include "../../ecs/entityBlueprint.h"
#include "../../mapeditor/modifiableParameters.h"
#include "../../scripting/scriptingModule.h"
#include "../../states/mapeditorstate.h"
#include "../../utils/debug.h"


// ---------------------------------------------------------------------------
MapEditorGui::MapEditorGui(const std::string file, Game* const game_)
    : Map_FILE_SELECTED(false)
    , Texture_FILE_SELECTED(false)
    , game(game_)
    // FileIOException, thrown if something goes wrong while processing the file
    // filename and InvalidRequestException, thrown if filename appears to be invalid
    , guiRoot(CEGUI::WindowManager::getSingleton().loadLayoutFromFile(file))
    //, toolBoxPageTexturing(CEGUI::WindowManager::getSingleton().loadLayoutFromFile(
    //      "mapEditorToolBoxPageTexturingGui.layout"))
    //, toolBoxPageEntity(CEGUI::WindowManager::getSingleton().loadLayoutFromFile(
    //      "mapEditorToolBoxPageEntityGui.layout"))
    //, entityTreeRoot(static_cast<CEGUI::Tree*>(this->toolBoxPageEntity->getChild("Tree_Entity")))
    //, messageBoxObj(nullptr)
    , fileDialog(SFileDialog(game_->getDevice()->getFileSystem()->getWorkingDirectory()))
{
    debugOut("MapEditorRoot: ", this->guiRoot);
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(this->guiRoot);
    auto rootWindow = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
    for (size_t i = 0;
         i < CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChildCount();
         ++i)
    {
        debugOut(static_cast<CEGUI::NamedElement*>(rootWindow->getChildElementAtIdx(i))->getName());
    }
    // CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild()
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->setMousePassThroughEnabled(true);


    sol::state& solState = game->getScriptingModule()->getScriptEngine().getSolState(0);
    CEGUI::LuaScriptModule& scriptMod(CEGUI::LuaScriptModule::create(solState.lua_state()));
    CEGUI::System::getSingleton().setScriptingModule(&scriptMod);
    CEGUI::System::getSingleton().executeScriptFile("mapEditorGui.lua");


    // this->messageBoxObj = new MessageBoxGui("messageBoxGui.layout",
    //                                        this->guiRoot); // Important: cant create settingMenuObj
    // before added SettingMenuWindow to Gui


    // CEGUI::TabControl* tc =
    //    static_cast<CEGUI::TabControl*>(this->guiRoot->getChild("Window_ToolBox/TabControl"));

    // Add some pages to tab control
    // tc->addTab(this->toolBoxPageEntity);
    // tc->addTab(this->toolBoxPageTexturing);

    // this->entityTreeRoot->setMultiselectEnabled(false);
    // this->subscribeEvents();

    // addSelectableToolBoxItemTypes();
    /*
       std::string textureName = std::to_string(layerNr);
       CEGUI::IrrlichtTexture& ceTex =
       static_cast<CEGUI::IrrlichtTexture&>(game->getGuiEnvironment()->getRenderer()->createTexture(textureName));
       ceTex.setIrrlichtTexture(texDrawer->getLayer(layerNr)->red);
       textureName.append(std::to_string(layerNr));
       CEGUI::BasicImage* image =
       static_cast<CEGUI::BasicImage*>(&CEGUI::ImageManager::getSingleton().create("BasicImage",
       textureName));

       std::string textureName_ = "BasicImage/";
       textureName_.append(textureName);
       irr::video::ITexture* irrtext = game->getVideoDriver()->getTexture("media/map/Rock.jpg");
       std::string textureName = "Brush";
       CEGUI::IrrlichtTexture& ceTex =
       static_cast<CEGUI::IrrlichtTexture&>(game->getGuiEnvironment()->getRenderer()->createTexture(textureName));
       ceTex.setIrrlichtTexture(irrtext);
       CEGUI::BasicImage* image =
       static_cast<CEGUI::BasicImage*>(&CEGUI::ImageManager::getSingleton().create("BasicImage",
       textureName));*/
    // debugOut(image->getSchemaName(), image->getName());
    // myImageWindow->setProperty("Image", image->getSchemaName() );

    // CEGUI::ButtonBase* btn1 =
    // static_cast<CEGUI::ButtonBase*>(this->guiRoot->getChild("ToolBoxFrame/FrameWindow/Button"));
    // btn1->set ( &image );
    // CEGUI::FalagardStaticImage* treeRoot;// = static_cast<CEGUI::FalagardStaticImage*>(this->guiRoot->getChild("StatusWindow/StaticImage"));


    // CEGUI::Imageset* imageSet =
    // static_cast<CEGUI::Imageset*>(&CEGUI::ImageManager::getSingleton().create("BasicImage",
    // textureName));
    //*/
    //newLayer->setIcon(*image);//ImageManager::getSingleton().get("DriveIcons/Artic"));
    //newLayer->setIcon(texDrawer->getLayer(layerNr)->red);//ImageManager::getSingleton().get("DriveIcons/Artic"));
    // newLayer->setIcon(CEGUI::ImageManager::getSingleton().get("DriveIcons/Artic"));
    // CEGUI::ImageManager::getSingleton().loadImagesetFromString("vanilla.png");
    // newLayer->setIcon(CEGUI::ImageManager::getSingleton().get("vanilla.png"));
    // newLayer->setSelectionBrushImage(image);
}

// ---------------------------------------------------------------------------
MapEditorGui::~MapEditorGui()
{
    CEGUI::WindowManager::getSingleton().destroyWindow(this->guiRoot);
}

// ---------------------------------------------------------------------------
void MapEditorGui::subscribeEvents()
{
    /*
    // this->guiRoot
    //     ->getChild("Menubar/MenuItem_SettingMenu/PopupMenu_SettingMenu/MenuItem_SettingMenu_Load")
    //     ->subscribeEvent(CEGUI::MenuItem::EventClicked,
    //                      CEGUI::Event::Subscriber(&MapEditorGui::handleLoadMap, this));
    //this->guiRoot
    //    ->getChild("Menubar/MenuItem_SettingMenu/PopupMenu_SettingMenu/MenuItem_SettingMenu_Save")
    //    ->subscribeEvent(CEGUI::MenuItem::EventClicked,
    //                     CEGUI::Event::Subscriber(&MapEditorGui::handleSaveMap, this));
    this->guiRoot
        ->getChild("Menubar/MenuItem_SettingMenu/PopupMenu_SettingMenu/MenuItem_SettingMenu_Quit")
        ->subscribeEvent(CEGUI::MenuItem::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleQuitButton, this));

    this->guiRoot->getChild("Menubar/MenuItem_ToolBox")
        ->subscribeEvent(CEGUI::MenuItem::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleShowToolBoxButton, this));

    this->guiRoot->getChild("Window_FileDialog/Tree_FileList")
        ->subscribeEvent(CEGUI::Tree::EventSelectionChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleFileDialogTreeItemSelectionChanged, this));
    this->guiRoot->getChild("Window_FileDialog/Button_Ok")
        ->subscribeEvent(CEGUI::PushButton::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleFileDialogOkButton, this));
    this->guiRoot->getChild("Window_FileDialog/Button_Cancel")
        ->subscribeEvent(CEGUI::PushButton::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleFileDialogCancelButton, this));
    this->guiRoot->getChild("Window_FileDialog/Button_FileUp")
        ->subscribeEvent(CEGUI::PushButton::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleFileDialogFileUpButton, this));

    this->messageBoxObj->getButtonYes()->subscribeEvent(CEGUI::PushButton::EventClicked,
                                                        CEGUI::Event::Subscriber(&MapEditorGui::handleMessageBoxButton_Yes,
                                                                                 this));
    this->messageBoxObj->getButtonNo()->subscribeEvent(CEGUI::PushButton::EventClicked,
                                                       CEGUI::Event::Subscriber(&MapEditorGui::handleMessageBoxButton_No,
                                                                                this));
    this->messageBoxObj->getButtonOk()->subscribeEvent(CEGUI::PushButton::EventClicked,
                                                       CEGUI::Event::Subscriber(&MapEditorGui::handleMessageBoxButton_Ok,
                                                                                this));


    this->guiRoot->getChild("Window_ToolBox/TabControl")
        ->subscribeEvent(CEGUI::TabControl::EventSelectionChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleTabSelectionChanged, this));

    this->toolBoxPageTexturing->getChild("Button_AddTexture")
        ->subscribeEvent(CEGUI::PushButton::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleAddTextureButton, this));
    this->toolBoxPageTexturing->getChild("Button_DeleteTexture")
        ->subscribeEvent(CEGUI::PushButton::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleDeleteTextureButton, this));

    for (int i = 0; i < 8; i++)
    {
        this->toolBoxPageTexturing
            ->getChild("Button_Texture" + CEGUI::String(irr::core::stringc(i).c_str()))
            ->subscribeEvent(CEGUI::PushButton::EventClicked,
                             CEGUI::Event::Subscriber(&MapEditorGui::handleTextureButtenSelected, this));
    }
    this->toolBoxPageTexturing->getChild("Scrollbar_TextureButton")
        ->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleScrollbarTextureButton, this));

    this->toolBoxPageTexturing->getChild("Checkbox_Smooth")
        ->subscribeEvent(CEGUI::ToggleButton::EventSelectStateChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSmoothStateChanged, this));
    static_cast<CEGUI::ToggleButton*>(this->toolBoxPageTexturing->getChild("Checkbox_Smooth"))
        ->setSelected(defaultParameters.smoothDrawing);

    this->toolBoxPageTexturing->getChild("Checkbox_OverwriteLowest")
        ->subscribeEvent(CEGUI::ToggleButton::EventSelectStateChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleOverwriteLowestStateChanged, this));
    static_cast<CEGUI::ToggleButton*>(
        this->toolBoxPageTexturing->getChild("Checkbox_OverwriteLowest"))
        ->setSelected(defaultParameters.overwriteLowestIntensity);

    this->toolBoxPageTexturing->getChild("Checkbox_InstantDraw")
        ->subscribeEvent(CEGUI::ToggleButton::EventSelectStateChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleInstantDrawStateChanged, this));
    static_cast<CEGUI::ToggleButton*>(this->toolBoxPageTexturing->getChild("Checkbox_InstantDraw"))
        ->setSelected(not defaultParameters.incrementalIntensity);

    this->toolBoxPageTexturing->getChild("Checkbox_IntensityDecrease")
        ->subscribeEvent(CEGUI::ToggleButton::EventSelectStateChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleIntensityDecreaseStateChanged, this));
    static_cast<CEGUI::ToggleButton*>(
        this->toolBoxPageTexturing->getChild("Checkbox_IntensityDecrease"))
        ->setSelected(defaultParameters.allowIntensityDecrease);

    this->toolBoxPageTexturing->getChild("Spinner_BrushSize")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleBrushSizeValueChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageTexturing->getChild("Spinner_BrushSize"))
        ->setCurrentValue(defaultParameters.brushSize);

    this->toolBoxPageTexturing->getChild("Spinner_Speed")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSpeedValueChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageTexturing->getChild("Spinner_Speed"))
        ->setCurrentValue(defaultParameters.drawsPerSecond);

    this->toolBoxPageTexturing->getChild("Spinner_Intensity")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleIntensityValueChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageTexturing->getChild("Spinner_Intensity"))
        ->setCurrentValue(defaultParameters.brushIntensity);


    this->toolBoxPageEntity->getChild("Button_AddNewObject")
        ->subscribeEvent(CEGUI::PushButton::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleAddNewObjButten, this));
    this->guiRoot->getChild("Window_AddNewObj/Button_Cancel")
        ->subscribeEvent(CEGUI::PushButton::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleNewObjWindowButten_Cancel, this));
    this->guiRoot->getChild("Window_AddNewObj/Button_Create")
        ->subscribeEvent(CEGUI::PushButton::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleNewObjWindowButten_Create, this));

    this->guiRoot->getChild("Window_AddNewObj/Button_LoadMeshFile")
        ->subscribeEvent(CEGUI::PushButton::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleNewObjWindowButton_LoadMeshFile, this));
    this->guiRoot->getChild("Window_AddNewObj/Button_LoadTextureFile")
        ->subscribeEvent(CEGUI::PushButton::EventClicked,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleNewObjWindowButton_LoadTextureFile, this));


    this->entityTreeRoot->subscribeEvent(CEGUI::Tree::EventSelectionChanged,
                                         CEGUI::Event::Subscriber(&MapEditorGui::handleEntityTreeItemChanged, this));

    this->toolBoxPageEntity->getChild("Spinner_Position_X")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSelectedObjectPositionChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Position_X"))->setCurrentValue(0);

    this->toolBoxPageEntity->getChild("Spinner_Position_Y")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSelectedObjectPositionChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Position_Y"))->setCurrentValue(0);

    this->toolBoxPageEntity->getChild("Spinner_Offset")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSelectedObjectOffsetChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Offset"))->setCurrentValue(0);

    this->toolBoxPageEntity->getChild("Spinner_Rotation_X")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSelectedObjectRotationChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Rotation_X"))->setCurrentValue(0);

    this->toolBoxPageEntity->getChild("Spinner_Rotation_Y")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSelectedObjectRotationChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Rotation_Y"))->setCurrentValue(0);

    this->toolBoxPageEntity->getChild("Spinner_Rotation_Z")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSelectedObjectRotationChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Rotation_Z"))->setCurrentValue(0);

    this->toolBoxPageEntity->getChild("Spinner_Scale_X")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSelectedObjectScaleChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Scale_X"))->setCurrentValue(1);

    this->toolBoxPageEntity->getChild("Spinner_Scale_Y")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSelectedObjectScaleChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Scale_Y"))->setCurrentValue(1);

    this->toolBoxPageEntity->getChild("Spinner_Scale_Z")
        ->subscribeEvent(CEGUI::Spinner::EventValueChanged,
                         CEGUI::Event::Subscriber(&MapEditorGui::handleSelectedObjectScaleChanged, this));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Scale_Z"))->setCurrentValue(1);
    */
}

// ---------------------------------------------------------------------------
void MapEditorGui::initFileDialog(std::string proposalFile_,
                                  bool* const fileSelectedFlag_,
                                  CEGUI::Editbox* const editboxWaitingForFile_)
{
    this->guiRoot->getChild("Window_FileDialog")->setVisible(true);

    this->fileDialog.selectedFile = "NONE";
    CEGUI::Editbox* editbox =
        static_cast<CEGUI::Editbox*>(this->guiRoot->getChild("Window_FileDialog/Editbox"));
    editbox->setText("");

    this->fileDialog.fileSelectedFlag = fileSelectedFlag_;
    this->fileDialog.editboxWaitingForFile = editboxWaitingForFile_;

    auto const pos = proposalFile_.find_last_of('/');
    std::string file = proposalFile_.substr(pos + 1);
    std::string path = "/" + proposalFile_.substr(0, pos);

    debugOut(path, ": ", file);


    this->updateFileDialog(game->getDevice()->getFileSystem()->getWorkingDirectory() +
                           irr::io::path(path.c_str()));

    CEGUI::Tree* treeRoot =
        static_cast<CEGUI::Tree*>(this->guiRoot->getChild("Window_FileDialog/Tree_FileList"));

    std::string label = "-";
    for (unsigned int i = 1; i < this->fileDialog.fileList->getFileCount(); i++)
    {
        label = std::string(irr::core::stringc(this->fileDialog.fileList->getFileName(i)).c_str());
        if (irr::core::stringw(label.c_str()).equals_ignore_case(file.c_str())) // TODO Fix Case-problem --- is there a case problem?
        {
            if (this->fileDialog.fileList->isDirectory(i))
            {
                debugOut("error file: '", file, "' is Folder");
            }
            else
            {
                treeRoot->setItemSelectState(treeRoot->findFirstItemWithText(label), true);
            }
            return;
        }
    }
}

// ---------------------------------------------------------------------------
void MapEditorGui::closeFileDialog()
{
    debugOut("closeFileDialog");
    this->guiRoot->getChild("Window_FileDialog")->setVisible(false);
    this->game->getDevice()->getFileSystem()->changeWorkingDirectoryTo(
        this->fileDialog.dafaultWorkingDirectory); // reset WorkingDirectory changes
}

// ---------------------------------------------------------------------------
void MapEditorGui::addEntityBlueprintToTree(const std::string type, const std::string name, const entityID_t blueprintIndex)
{
    /*debugOut("addEntityBlueprintToTree(blueprintIndex)");
    CEGUI::String label = CEGUI::String(name);
    CEGUI::String t_type = CEGUI::String(type);


    //process Type first
    CEGUI::TreeItem* typeItem = this->entityTreeRoot->findFirstItemWithText(t_type);
    if(!typeItem)
    {
        typeItem = new CEGUI::TreeItem(t_type,ListIDs::Types);
        this->entityTreeRoot->addItem(typeItem);
    }
    CEGUI::TreeItem* newEntityBlueprint = this->entityTreeRoot->findFirstItemWithText(label);
    if (newEntityBlueprint == nullptr)
    {
        newEntityBlueprint = new CEGUI::TreeItem(label, ListIDs::Blueprint, reinterpret_cast<void*>(blueprintIndex));
        newEntityBlueprint->setSelectionBrushImage("BT/GenericBrush");
        typeItem->addItem(newEntityBlueprint);
    }
    else
    {
        debugOut("EntityTemplate with name '", label, "' already exist");
    }*/
}


// ---------------------------------------------------------------------------
void MapEditorGui::addEntityFromBlueprintToTree(const entityID_t entityID, const std::string blueprintName)
{
    /*//TODO: redesign Mapeditor with more tabs, trees etc.
    debugOut("addEntityFromBlueprintToTree(entityID)");
    CEGUI::String label = CEGUI::String(blueprintName + " entityID(" + std::to_string(entityID) + ")");
    CEGUI::TreeItem* blueprintItem = this->entityTreeRoot->findFirstItemWithText(blueprintName.c_str());
    CEGUI::TreeItem* newEntityItem =
        new CEGUI::TreeItem(label, ListIDs::ExistingObject, reinterpret_cast<void*>(entityID));

    newEntityItem->setSelectionBrushImage("BT/GenericBrush");

    if (blueprintItem == nullptr)
    {
        debugOut("No blueprint with name '", blueprintName, "' found");
        blueprintItem = this->entityTreeRoot->findFirstItemWithText("Unowned Objects");
        if (blueprintItem == nullptr)
        {
            blueprintItem = new CEGUI::TreeItem("Unowned Objects", ListIDs::Blueprint, nullptr); // TODO: this nullptr what does it mean???
            blueprintItem->setSelectionBrushImage("BT/GenericBrush");
            this->entityTreeRoot->addItem(blueprintItem);
        }
    }
    blueprintItem->addItem(newEntityItem);
    */
}

// ---------------------------------------------------------------------------
void MapEditorGui::updateTextureButtons()
{
    /*debugOut("fillTextureButtons");

    std::vector<irr::core::stringw> textureArrayFilenames = this->modifiableParameters->textureArrayFilenames;

    CEGUI::ButtonBase* textureButton = nullptr;
    irr::video::ITexture* myIrrlichtTexture = nullptr;
    unsigned int nrOfTextures = textureArrayFilenames.size();

    CEGUI::Scrollbar* sb = static_cast<CEGUI::Scrollbar*>(
        this->toolBoxPageTexturing->getChild("Scrollbar_TextureButton"));
    int offset = 2 * static_cast<int>(sb->getScrollPosition()); // TODO: maybe add +0.5 to
                                                                // scrollPosition to round the
                                                                // scrolling (especially if
                                                                // completely at top/bottom)?

    unsigned int indice;
    for (unsigned int buttonNr = 0; buttonNr < 8; buttonNr++)
    {
        CEGUI::Window* textureButtonWindow = this->toolBoxPageTexturing->getChild(
            "Button_Texture" + CEGUI::String(irr::core::stringc(buttonNr).c_str()));
        textureButton = static_cast<CEGUI::ButtonBase*>(textureButtonWindow);

        indice = buttonNr + offset;
        if (indice < nrOfTextures)
        {
            textureButton->setVisible(true);
            textureButton->setID(indice);
            textureButton->setTooltipText("TextureID: "); //+CEGUI::String(irr::core::stringc(i).c_str())+"/n"+CEGUI::String(irr::core::stringc(textureArrayFilenames[i]).c_str()));
            myIrrlichtTexture =
                this->game->getVideoDriver()->getTexture(textureArrayFilenames[indice].c_str());
            if (myIrrlichtTexture != nullptr)
            {
                debugOutLevel(20,
                              "fill TextureButtons: ",
                              buttonNr,
                              "with Texture: ",
                              myIrrlichtTexture->getName().getPath().c_str());
                this->setImageToWindow(textureButtonWindow, myIrrlichtTexture, "NormalImage");
            }
        }
        else
        {
            textureButton->setVisible(false);
        }
    }
    if (nrOfTextures > 8)
    {
        sb->setVisible(true);
        int diff = nrOfTextures - 8;
        sb->setDocumentSize(int(diff / 2) + int(diff % 2));
        // sb->getThumb()->setwWidth(1/sb->getDocumentSize());
    }*/
}

// ---------------------------------------------------------------------------
void MapEditorGui::setPositionDisplay(const irr::core::vector2df position)
{
    /*static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Position_X"))
        ->setText(std::to_string(position.X));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Position_Y"))
        ->setText(std::to_string(position.Y));*/
}

// ---------------------------------------------------------------------------
void MapEditorGui::setOffsetDisplay(const irr::f32 offset)
{
    //   static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Offset"))->setText(std::to_string(offset));
}

// ---------------------------------------------------------------------------
void MapEditorGui::setRotationDisplay(const irr::core::vector3df rotation)
{
    /*static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Rotation_X"))
        ->setText(std::to_string(rotation.X));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Rotation_Y"))
        ->setText(std::to_string(rotation.Y));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Rotation_Z"))
        ->setText(std::to_string(rotation.Z));*/
}

// ---------------------------------------------------------------------------
void MapEditorGui::setScaleDisplay(const irr::core::vector3df scale)
{
    /*static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Scale_X"))
        ->setText(std::to_string(scale.X));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Scale_Y"))
        ->setText(std::to_string(scale.Y));
    static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Scale_Z"))
        ->setText(std::to_string(scale.Z));*/
}
/**************************************************************
 * private memberfunctions
 **************************************************************/


// ---------------------------------------------------------------------------
void MapEditorGui::updateFileDialog(irr::io::path basisPath_)
{
    debugOut("updateFileDialog:", basisPath_.c_str());
    this->fileDialog.currentWorkingDirectory = basisPath_;
    this->game->getDevice()->getFileSystem()->changeWorkingDirectoryTo(this->fileDialog.currentWorkingDirectory);
    debugOut(this->fileDialog.currentWorkingDirectory.c_str(),
             ": ",
             game->getDevice()->getFileSystem()->getWorkingDirectory().c_str());
    this->fileDialog.fileList = this->game->getDevice()->getFileSystem()->createFileList();

    CEGUI::Tree* treeRoot =
        static_cast<CEGUI::Tree*>(this->guiRoot->getChild("Window_FileDialog/Tree_FileList"));
    treeRoot->resetList();
    CEGUI::TreeItem* newItem = nullptr;
    std::string label = "";
    for (unsigned int i = 1; i < this->fileDialog.fileList->getFileCount(); i++)
    {
        // debugOut(this->fileDialog.fileList->getFileName(i));
        label = std::string(irr::core::stringc(this->fileDialog.fileList->getFileName(i)).c_str());
        newItem = new CEGUI::TreeItem(label.c_str(), i);
        newItem->setSelectionBrushImage("BT/GenericBrush");
        if (this->fileDialog.fileList->isDirectory(i))
        {
            newItem->setIcon(CEGUI::ImageManager::getSingleton().get("BT/Folder"));
        }
        else
        {
            newItem->setIcon(CEGUI::ImageManager::getSingleton().get("BT/File"));
        }
        treeRoot->addItem(newItem);
        // debugOut();
    }
}


// ---------------------------------------------------------------------------
bool MapEditorGui::handleFileDialogTreeItemSelectionChanged(const CEGUI::EventArgs&)
{
    debugOut("handleTreeItemSelectionChanged");

    CEGUI::Tree* treeRoot =
        static_cast<CEGUI::Tree*>(this->guiRoot->getChild("Window_FileDialog/Tree_FileList"));
    CEGUI::Editbox* editbox =
        static_cast<CEGUI::Editbox*>(this->guiRoot->getChild("Window_FileDialog/Editbox"));
    editbox->setText(treeRoot->getFirstSelectedItem()->getText().c_str());
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleFileDialogOkButton(const CEGUI::EventArgs&)
{
    debugOut("handleOkButton");
    CEGUI::Tree* treeRoot =
        static_cast<CEGUI::Tree*>(this->guiRoot->getChild("Window_FileDialog/Tree_FileList"));

    auto selectedItem = treeRoot->getFirstSelectedItem();
    if (selectedItem)
    {
        uint32_t i = selectedItem->getID();
        if (this->fileDialog.fileList->isDirectory(i))
        {
            this->updateFileDialog(this->fileDialog.currentWorkingDirectory + irr::io::path("/") +
                                   irr::io::path(treeRoot->getFirstSelectedItem()->getText().c_str()));
            CEGUI::Editbox* editbox =
                static_cast<CEGUI::Editbox*>(this->guiRoot->getChild("Window_FileDialog/Editbox"));
            editbox->setText("");
            return true;
        }
        else
        {
            std::string selectedFile = std::string(
                irr::core::stringc(this->fileDialog.currentWorkingDirectory + irr::io::path("/") +
                                   irr::io::path(treeRoot->getFirstSelectedItem()->getText().c_str()))
                    .c_str());
            auto const pos = std::string(this->fileDialog.dafaultWorkingDirectory.c_str()).length();
            this->fileDialog.selectedFile = irr::io::path(selectedFile.substr(pos + 1).c_str());
            debugOut("selectedFile",
                     selectedFile,
                     " : ",
                     this->fileDialog.dafaultWorkingDirectory.c_str(),
                     ", ",
                     pos,
                     ", ",
                     this->fileDialog.selectedFile);

            this->closeFileDialog();

            if (this->fileDialog.editboxWaitingForFile != nullptr)
            {
                debugOut("set Editbox-Text");
                this->fileDialog.editboxWaitingForFile->setText(this->fileDialog.selectedFile.c_str());
            }
            else
            {
                if (this->fileDialog.fileSelectedFlag != nullptr)
                {
                    *this->fileDialog.fileSelectedFlag = true;
                    irr::SEvent event;
                    event.EventType = irr::EET_GUI_EVENT;
                    event.GUIEvent.EventType = irr::gui::EGET_FILE_SELECTED;
                    game->OnEvent(event);
                }
            }
            return true;
        }
    }
    debugOut("something wrong here");
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleFileDialogCancelButton(const CEGUI::EventArgs&)
{
    debugOut("handleCancelButton");
    this->closeFileDialog();
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleFileDialogFileUpButton(const CEGUI::EventArgs&)
{
    debugOut("handleFileUpButton");
    this->updateFileDialog(this->fileDialog.currentWorkingDirectory + irr::io::path("/.."));
    return true;
}

// ---------------------------------------------------------------------------
std::string MapEditorGui::getShortTexturePath(irr::video::ITexture* const texture, const std::string pathRoot) const
{
    std::string fullTexturePath = irr::core::stringc(texture->getName()).c_str();

    unsigned int formatLength = 0; // dont remove .png or .jpg
    unsigned int pathToRootLength = fullTexturePath.find(pathRoot.c_str());
    if (pathRoot == "name") // TODO find a better way to get just the name maybe with a bool
    {
        pathToRootLength = fullTexturePath.rfind('/') + 1;
        formatLength = 4; // remove .png or .jpg
    }

    std::string shortTexturePath =
        fullTexturePath.substr(pathToRootLength, fullTexturePath.size() - pathToRootLength - formatLength);
    return shortTexturePath;
}

// ---------------------------------------------------------------------------
void MapEditorGui::setSelectedBlueprint(const entityID_t blueprintIndex)
{
    /*debugOut("setSelectedBlueprint(blueprintIndex)");

    // unselect old item
    CEGUI::TreeItem* const selectedItem = this->entityTreeRoot->getFirstSelectedItem();
    if (selectedItem != nullptr)
    {
        if (selectedItem->getID() != ListIDs::Blueprint)
        {
            selectedItem->setSelected(false);
        }
        else
        {
            const entityID_t selectedBlueprintIndex =
                reinterpret_cast<entityID_t>(selectedItem->getUserData());
            // StaticObject* const staticObj = static_cast<StaticObject*
            // const>(selectedItem->getUserData());
            if (selectedBlueprintIndex != blueprintIndex)
            {
                selectedItem->setSelected(false);
                // this->entityTreeRoot->setItemSelectState(selectedItem, false);
            }
        }
    }

    if (blueprintIndex != ECS::INVALID_ENTITY)
    { // invalid blueprintIndex means select nothing
        CEGUI::TreeItem* entityItem = getItemFromBlueprintIndex(blueprintIndex);
        //POW2_ASSERT(entityItem != nullptr); // the item should always be inside the tree
        if(!entityItem)
        {
            std::cerr<<"TreeItem could not be found from blueprintIndex:
    "<<blueprintIndex<<std::endl; return;
        }


        entityItem->setSelected(true);
        // this->entityTreeRoot->setItemSelectState(entityItem, true);
    }

    this->entityTreeRoot->handleUpdatedItemData(); // important, otherwise gui doesn't update
    */
}

// ---------------------------------------------------------------------------
void MapEditorGui::setSelectedEntity(const entityID_t entityID)
{
    /* debugOut("setSelectedEntity(entityID)");

     // unselect old item
     CEGUI::TreeItem* const selectedItem = this->entityTreeRoot->getFirstSelectedItem();
     if (selectedItem != nullptr)
     {
         if (selectedItem->getID() != ListIDs::ExistingObject)
         {
             selectedItem->setSelected(false);
         }
         else
         {
             const entityID_t selectedEntityID = reinterpret_cast<entityID_t>(selectedItem->getUserData());
             // StaticObject* const staticObj = static_cast<StaticObject*
             // const>(selectedItem->getUserData());
             if (selectedEntityID != entityID)
             {
                 selectedItem->setSelected(false);
                 // this->entityTreeRoot->setItemSelectState(selectedItem, false);
             }
         }
     }

     if (entityID != ECS::INVALID_ENTITY)
     { // invalid entity means select nothing
         CEGUI::TreeItem* entityItem = getItemFromEntityID(entityID);
         //POW2_ASSERT(entityItem != nullptr); // the item should always be inside the tree
         if(!entityItem)
         {
             std::cerr<<"TreeItem could not be found from entityID: "<<entityID<<std::endl;
             return;
         }
         entityItem->setSelected(true);
         // this->entityTreeRoot->setItemSelectState(entityItem, true);
     }

     this->entityTreeRoot->handleUpdatedItemData(); // important
     */
}

// ---------------------------------------------------------------------------
void MapEditorGui::deleteEntityFromTree(const entityID_t entityID)
{
    /*
    debugOut("deleteEntityFromTree");

    CEGUI::TreeItem* const itemToDelete = this->getItemFromEntityID(entityID);
    if (itemToDelete != nullptr)
    {
        debugOut("remove item");
        this->entityTreeRoot->setItemSelectState(itemToDelete, false);

        // can't call this->entityTreeRoot->removeItem(itemToDelete); because that doesn't recurse
        // down into subtrees
        // fix-> iterate over all blueprints and see if the itemToDelete is part of that subtree
        bool found = false;
        CEGUI::TreeItem* blueprintItem = this->entityTreeRoot->findFirstItemWithID(ListIDs::Blueprint);
        while (blueprintItem != nullptr)
        {
            for (size_t i = 0; i < blueprintItem->getItemCount(); i++)
            {
                if (itemToDelete == blueprintItem->getTreeItemFromIndex(i))
                {
                    const bool autoDelete = itemToDelete->isAutoDeleted();
                    blueprintItem->removeItem(itemToDelete);
                    if (!autoDelete)
                    {
                        delete itemToDelete;
                    }
                    found = true;
                    break;
                }
            }
            if (found)
            {
                break;
            }
            blueprintItem = this->entityTreeRoot->findNextItemWithID(ListIDs::Blueprint, blueprintItem);
        }

        this->entityTreeRoot->handleUpdatedItemData(); // important
    }*/
}

// ---------------------------------------------------------------------------
CEGUI::TreeItem* MapEditorGui::getItemFromBlueprintIndex(const entityID_t blueprintIndex)
{
    /*POW2_ASSERT(blueprintIndex != ECS::INVALID_ENTITY);

    auto* treeItem = this->entityTreeRoot->findFirstItemWithID(ListIDs::Blueprint);

    while (treeItem != nullptr)
    {
        if (reinterpret_cast<const entityID_t>(treeItem->getUserData()) == blueprintIndex)
        {
            return treeItem;
        }
        treeItem = this->entityTreeRoot->findNextItemWithID(ListIDs::Blueprint, treeItem);
    }*/
    return nullptr;
}

// ---------------------------------------------------------------------------
CEGUI::TreeItem* MapEditorGui::getItemFromEntityID(const entityID_t entityID)
{
    /*
    POW2_ASSERT(entityID != ECS::INVALID_ENTITY);
    auto* blueprintItem = this->entityTreeRoot->findFirstItemWithID(ListIDs::Blueprint);
    while (blueprintItem != nullptr)
    {
        // debugOut(blueprintItem->getText());
        auto* entityItem = this->entityTreeRoot->findNextItemWithID(ListIDs::ExistingObject, blueprintItem);
        while (entityItem != nullptr)
        {
            // debugOut(entityItem->getText());
            if (reinterpret_cast<const entityID_t>(entityItem->getUserData()) == entityID)
            {
                return entityItem;
            }
            entityItem = this->entityTreeRoot->findNextItemWithID(ListIDs::ExistingObject, entityItem);
        }
        blueprintItem = this->entityTreeRoot->findNextItemWithID(ListIDs::Blueprint, blueprintItem);
    }*/
    return nullptr;
}

// ---------------------------------------------------------------------------
void MapEditorGui::setImageToWindow(CEGUI::Window* const myImageWindow,
                                    irr::video::ITexture* const myIrrlichtTexture,
                                    CEGUI::String const imageType) // testing
{
    /*
    debugOutLevel(20, "setImageToWindow");
    // CEGUI::Editbox* imagepath =
    // static_cast<CEGUI::Editbox*>(this->toolBoxPageTexturing->getChild("Label_ItemName"));
    std::string textureName = this->getShortTexturePath(myIrrlichtTexture, "media");
    // imagepath->setText(textureName);

    CEGUI::IrrlichtTexture* myCeGuiIrrTexture = nullptr;
    CEGUI::BasicImage* image = nullptr;
    if (game->getGuiEnvironment()->getRenderer()->isTextureDefined(textureName))
    {
        debugOutLevel(25, textureName, "is defined");
        myCeGuiIrrTexture = static_cast<CEGUI::IrrlichtTexture*>(
            &(game->getGuiEnvironment()->getRenderer()->getTexture(textureName)));
        image = static_cast<CEGUI::BasicImage*>(&CEGUI::ImageManager::getSingleton().get(textureName));
    }
    else
    {
        debugOutLevel(25, "define", textureName);
        myCeGuiIrrTexture = static_cast<CEGUI::IrrlichtTexture*>(
            &(game->getGuiEnvironment()->getRenderer()->createTexture(textureName)));
        image = static_cast<CEGUI::BasicImage*>(
            &CEGUI::ImageManager::getSingleton().create("BasicImage", textureName));
    }
    myCeGuiIrrTexture->setIrrlichtTexture(myIrrlichtTexture);

    bool isTextureTargetVerticallyFlipped =
        game->getGuiEnvironment()->getRenderer()->isTexCoordSystemFlipped();
    CEGUI::Rectf imageArea;

    float textureWidth = float(fabs(myImageWindow->getWidth().d_scale));
    float textureHeight = float(fabs(myImageWindow->getHeight().d_scale));

    if (isTextureTargetVerticallyFlipped)
    {
        imageArea = CEGUI::Rectf(textureHeight, 0.0f, 0.0f, textureWidth);
    }
    else
    {
        imageArea = CEGUI::Rectf(0.0f, 0.0f, 1000.0f, 1000.0f);
    }
    // image.set setPosition();
    // debugOut(imageArea.getWidth(), imageArea.getHeight());
    // debugOut(myImageWindow->getHitTestRect().getWidth(),
    // myImageWindow->getHitTestRect().getHeight());
    // This is necessary to define the image's size. Regardless of flipping or not you need to set
    // the size. Multiple images can be associated with a single texture.
    image->setArea(imageArea);
    //image->setArea(myImageWindow->getRenderedSize());//myImageWindow->getHitTestRect()
    // You most likely don't want autoscaling for RTT images. If you display it in stretched-mode
    // inside a button or Generic/Image widget, then this setting does not play a role anyways.
    image->setAutoScaled(CEGUI::ASM_Disabled);
    image->setTexture(myCeGuiIrrTexture);
    myImageWindow->setProperty(imageType, textureName);*/
}
//*/

// ---------------------------------------------------------------------------
void MapEditorGui::unlockObjectInfoSpinner()
{
    /*
    this->toolBoxPageEntity->getChild("Spinner_Position_X")->enable();
    this->toolBoxPageEntity->getChild("Spinner_Position_Y")->enable();

    this->toolBoxPageEntity->getChild("Spinner_Offset")->enable();

    this->toolBoxPageEntity->getChild("Spinner_Rotation_X")->enable();
    this->toolBoxPageEntity->getChild("Spinner_Rotation_Y")->enable();
    this->toolBoxPageEntity->getChild("Spinner_Rotation_Z")->enable();

    this->toolBoxPageEntity->getChild("Spinner_Scale_X")->enable();
    this->toolBoxPageEntity->getChild("Spinner_Scale_Y")->enable();
    this->toolBoxPageEntity->getChild("Spinner_Scale_Z")->enable();*/
}

// ---------------------------------------------------------------------------
void MapEditorGui::lockObjectInfoSpinner()
{
    /*
    this->toolBoxPageEntity->getChild("Spinner_Position_X")->disable();
    this->toolBoxPageEntity->getChild("Spinner_Position_Y")->disable();

    this->toolBoxPageEntity->getChild("Spinner_Offset")->disable();

    this->toolBoxPageEntity->getChild("Spinner_Rotation_X")->disable();
    this->toolBoxPageEntity->getChild("Spinner_Rotation_Y")->disable();
    this->toolBoxPageEntity->getChild("Spinner_Rotation_Z")->disable();

    this->toolBoxPageEntity->getChild("Spinner_Scale_X")->disable();
    this->toolBoxPageEntity->getChild("Spinner_Scale_Y")->disable();
    this->toolBoxPageEntity->getChild("Spinner_Scale_Z")->disable();
    */
}


// ---------------------------------------------------------------------------
void MapEditorGui::setPosSync(GuiSyncedValue<irr::core::vector2df>* posSync_)
{
    /*this->posSync = posSync_;

    if (this->posSync != nullptr)
    {
        this->posSync->addUser(this, [this](const irr::core::vector2df newValue) {
            this->setPositionDisplay(newValue);
        });
    }*/
}

// ---------------------------------------------------------------------------
void MapEditorGui::setOffsetSync(GuiSyncedValue<irr::f32>* offsetSync_)
{
    /*this->offsetSync = offsetSync_;

    if (this->offsetSync != nullptr)
    {
        this->offsetSync->addUser(this, [this](const irr::f32 newValue) {
            this->setOffsetDisplay(newValue);
        });
    }*/
}

// ---------------------------------------------------------------------------
void MapEditorGui::setRotationSync(GuiSyncedValue<irr::core::vector3df>* rotationSync_)
{
    /*this->rotationSync = rotationSync_;

    if (this->rotationSync != nullptr)
    {
        this->rotationSync->addUser(this, [this](const irr::core::vector3df newValue) {
            this->setRotationDisplay(newValue);
        });
    }*/
}

// ---------------------------------------------------------------------------
void MapEditorGui::setScaleSync(GuiSyncedValue<irr::core::vector3df>* scaleSync_)
{
    /*this->scaleSync = scaleSync_;

    if (this->scaleSync != nullptr)
    {
        this->scaleSync->addUser(this, [this](const irr::core::vector3df newValue) {
            this->setScaleDisplay(newValue);
        });
    }*/
}
// ---------------------------------------------------------------------------
bool MapEditorGui::handleLoadMap(const CEGUI::EventArgs&)
{
    debugOut("handleLoadMap");
    this->initFileDialog("data/map/mapEditorMap.xml", &this->Map_FILE_SELECTED);
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleSaveMap(const CEGUI::EventArgs&)
{
    debugOut("handleSaveMap");
    MapEditorState* mapEditorState = reinterpret_cast<MapEditorState*>(this->game->getCurrentState());
    mapEditorState->saveMap();
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleQuitButton(const CEGUI::EventArgs&)
{
    debugOut("handleQuitButton");
    this->messageBoxObj->ask("Attention!",
                             "Done modification will not be stored! Are you sure you "
                             "want to quit the current Map without saving?");
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleShowToolBoxButton(const CEGUI::EventArgs& arg_)
{
    debugOut("handleShowToolBoxButton");

    MapEditorState* mapEditorState = reinterpret_cast<MapEditorState*>(this->game->getCurrentState());

    if (this->guiRoot->getChild("Window_ToolBox")->isVisible())
    {
        this->guiRoot->getChild("Window_ToolBox")->setVisible(false);
        mapEditorState->setEditorStateTo(MAPEDIT_STATE::MES_DONOTHING);
    }
    else
    {
        this->guiRoot->getChild("Window_ToolBox")->setVisible(true);

        this->handleTabSelectionChanged(arg_);
    }

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleMessageBoxButton_Yes(const CEGUI::EventArgs& args)
{
    debugOut("handleMessageBoxButton_Yes");
    (void) args;

    this->Map_FILE_SELECTED = true;

    this->game->changeState(Game::States_e::Menu);

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleMessageBoxButton_No(const CEGUI::EventArgs& args)
{
    (void) args;

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleMessageBoxButton_Ok(const CEGUI::EventArgs& args)
{
    (void) args;

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleTabSelectionChanged(const CEGUI::EventArgs&)
{
    /*debugOutLevel(15, "handleTabSelectionChanged");
    MapEditorState* mapEditorState = reinterpret_cast<MapEditorState*>(this->game->getCurrentState());

    CEGUI::TabControl* tc =
        static_cast<CEGUI::TabControl*>(this->guiRoot->getChild("Window_ToolBox/TabControl"));

    if (tc->isTabContentsSelected(this->toolBoxPageTexturing))
    {
        mapEditorState->setEditorStateTo(MAPEDIT_STATE::MES_GROUND_NOTEXTURESELECTED);
    }
    else if (tc->isTabContentsSelected(this->toolBoxPageEntity))
    {
        mapEditorState->setEditorStateTo(MAPEDIT_STATE::MES_OBJECT_NOTHINGSELECTED);
        this->entityTreeRoot->handleUpdatedItemData();
    }*/

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleTextureButtenSelected(const CEGUI::EventArgs& args)
{
    /* debugOutLevel(15, "handleTextureButtenSelected");
     const CEGUI::WindowEventArgs& window = static_cast<const CEGUI::WindowEventArgs&>(args);
     CEGUI::ButtonBase* selectedButton = static_cast<CEGUI::ButtonBase*>(window.window);

     unsigned int textureId = selectedButton->getID();

     MapEditorState* mapEditorState =
     reinterpret_cast<MapEditorState*>(this->game->getCurrentState()); MAPEDIT_STATE state =
     mapEditorState->getEditorState(); if (state == MAPEDIT_STATE::MES_GROUND_NOTEXTURESELECTED)
     {
         debugOut("set textureIndexToDraw to", textureId);
         this->modifiableParameters->textureIndexToDraw = textureId;
         mapEditorState->setEditorStateTo(MAPEDIT_STATE::MES_GROUND_TEXTURING);
         selectedButton->setText("X");
     }
     if (state == MAPEDIT_STATE::MES_GROUND_TEXTURING)
     {
         int previousSelectedId = this->modifiableParameters->textureIndexToDraw;
         debugOut("textureId", textureId, "previousSelectedId", previousSelectedId);
         if (previousSelectedId == textureId)
         {
             mapEditorState->setEditorStateTo(MAPEDIT_STATE::MES_GROUND_NOTEXTURESELECTED);
             selectedButton->setText("");
         }
         else
         {
             CEGUI::Scrollbar* sb = static_cast<CEGUI::Scrollbar*>(
                 this->toolBoxPageTexturing->getChild("Scrollbar_TextureButton"));
             int offset = int(2 * sb->getScrollPosition());

             int buttonNr = previousSelectedId - offset;
             debugOut("buttonNr", buttonNr, "previousSelectedId", previousSelectedId);
             CEGUI::Window* windowButton = this->toolBoxPageTexturing->getChild(
                 "Button_Texture" + CEGUI::String(irr::core::stringc(buttonNr).c_str()));
             CEGUI::ButtonBase* previousButton = static_cast<CEGUI::ButtonBase*>(windowButton);
             previousButton->setText("");

             debugOut("set textureIndexToDraw to", textureId);
             this->modifiableParameters->textureIndexToDraw = textureId;
             selectedButton->setText("X");
         }
     }*/

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleAddTextureButton(const CEGUI::EventArgs& args)
{
    debugOutLevel(15, "handleAddTextureButton");
    (void) args;
    this->initFileDialog("media/map/texarray/", &this->Texture_FILE_SELECTED);

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleDeleteTextureButton(const CEGUI::EventArgs& args)
{
    /* debugOutLevel(15, "handleDeleteTextureButton");
     (void) args;

     MapEditorState* mapEditorState = reinterpret_cast<MapEditorState*>(this->game->getCurrentState());
     MAPEDIT_STATE state = mapEditorState->getEditorState();
     if (state == MAPEDIT_STATE::MES_GROUND_TEXTURING)
     {
         int textIndex = this->modifiableParameters->textureIndexToDraw;
         std::vector<irr::core::stringw> tempTextureArray = this->modifiableParameters->textureArrayFilenames;
         tempTextureArray.erase(tempTextureArray.begin() + textIndex);

         if (mapEditorState->updateTextureArray(tempTextureArray))
         {
             CEGUI::Scrollbar* sb = static_cast<CEGUI::Scrollbar*>(
                 this->toolBoxPageTexturing->getChild("Scrollbar_TextureButton"));
             int offset = int(2 * sb->getScrollPosition());

             int buttonNr = textIndex - offset;
             CEGUI::Window* textureButtonWindow = this->toolBoxPageTexturing->getChild(
                 "Button_Texture" + CEGUI::String(irr::core::stringc(buttonNr).c_str()));
             CEGUI::ButtonBase* selectedButton = static_cast<CEGUI::ButtonBase*>(textureButtonWindow);

             selectedButton->setText("");

             mapEditorState->setEditorStateTo(MAPEDIT_STATE::MES_GROUND_NOTEXTURESELECTED);
             this->updateTextureButtons();
         }
     }*/
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleScrollbarTextureButton(const CEGUI::EventArgs& args)
{
    debugOutLevel(15, "handleScrollbarTextureButton");
    (void) args;

    //    const CEGUI::WindowEventArgs& window = static_cast<const CEGUI::WindowEventArgs&>(args);
    //    CEGUI::Scrollbar* sb = static_cast<CEGUI::Scrollbar*>(window.window);
    this->updateTextureButtons();
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleSmoothStateChanged(const CEGUI::EventArgs&)
{
    /*if (this->modifiableParameters != nullptr)
    {
        this->modifiableParameters->smoothDrawing =
            static_cast<CEGUI::ToggleButton*>(
                this->toolBoxPageTexturing->getChild("Checkbox_Smooth"))
                ->isSelected();
    }
    debugOutLevel(15, "set smoothDrawing to:", this->modifiableParameters->smoothDrawing ? "True" : "False");*/
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleOverwriteLowestStateChanged(const CEGUI::EventArgs&)
{
    /* if (this->modifiableParameters != nullptr)
     {
         this->modifiableParameters->overwriteLowestIntensity =
             static_cast<CEGUI::ToggleButton*>(
                 this->toolBoxPageTexturing->getChild("Checkbox_OverwriteLowest"))
                 ->isSelected();
         debugOutLevel(15,
                       "set overwriteLowestIntensity to:",
                       this->modifiableParameters->overwriteLowestIntensity ? "True" : "False");
     }*/
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleInstantDrawStateChanged(const CEGUI::EventArgs&)
{
    /*if (this->modifiableParameters != nullptr)
    {
        this->modifiableParameters->incrementalIntensity =
            not static_cast<CEGUI::ToggleButton*>(
                    this->toolBoxPageTexturing->getChild("Checkbox_InstantDraw"))
                    ->isSelected();
        debugOutLevel(15, "set incrementalIntensity to:", this->modifiableParameters->incrementalIntensity ? "True" : "False");
    }*/
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleIntensityDecreaseStateChanged(const CEGUI::EventArgs& args)
{
    (void) args;

    /* if (this->modifiableParameters != nullptr)
     {
         this->modifiableParameters->allowIntensityDecrease =
             static_cast<CEGUI::ToggleButton*>(
                 this->toolBoxPageTexturing->getChild("Checkbox_IntensityDecrease"))
                 ->isSelected();
         debugOutLevel(15,
                       "set allowIntensityDecrease to:",
                       this->modifiableParameters->allowIntensityDecrease ? "True" : "False");
     }*/
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleBrushSizeValueChanged(const CEGUI::EventArgs& args)
{
    (void) args;

    /* if (this->modifiableParameters != nullptr)
     {
         this->modifiableParameters->brushSize = static_cast<irr::f32>(
             static_cast<CEGUI::Spinner*>(this->toolBoxPageTexturing->getChild("Spinner_BrushSize"))->getCurrentValue());
         debugOutLevel(15, "set brushSize to:", this->modifiableParameters->brushSize);
     }*/
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleSpeedValueChanged(const CEGUI::EventArgs&)
{
    /*if (this->modifiableParameters != nullptr)
    {
        this->modifiableParameters->drawsPerSecond = static_cast<irr::f32>(
            static_cast<CEGUI::Spinner*>(this->toolBoxPageTexturing->getChild("Spinner_Speed"))->getCurrentValue());
        debugOutLevel(15, "set drawsPerSecond to:", this->modifiableParameters->drawsPerSecond);
    }*/
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleIntensityValueChanged(const CEGUI::EventArgs&)
{
    /* if (this->modifiableParameters != nullptr)
     {
         this->modifiableParameters->brushIntensity = static_cast<irr::f32>(
             static_cast<CEGUI::Spinner*>(this->toolBoxPageTexturing->getChild("Spinner_Intensity"))->getCurrentValue());
         debugOutLevel(15, "set brushIntensity to:", this->modifiableParameters->brushIntensity);
     }*/
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleAddNewObjButten(const CEGUI::EventArgs& args)
{
    debugOut("handleAddNewObjButten");
    (void) args;

    /*this->guiRoot->getChild("Window_AddNewObj")->setVisible(true);

    CEGUI::Editbox* edit =
        static_cast<CEGUI::Editbox*>(this->guiRoot->getChild("Window_AddNewObj/Editbox_Name"));
    edit->setText(""); // ---------------------------------------------------------------------------

    edit = static_cast<CEGUI::Editbox*>(this->guiRoot->getChild("Window_AddNewObj/Editbox_Mesh"));
    edit->setText("");
    edit =
        static_cast<CEGUI::Editbox*>(this->guiRoot->getChild("Window_AddNewObj/Editbox_Texture"));
    edit->setText("");*/
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleNewObjWindowButten_Create(const CEGUI::EventArgs& args)
{
    debugOut("handleNewObjWindowButten_Create");
    (void) args;

    CEGUI::Editbox* edit =
        static_cast<CEGUI::Editbox*>(this->guiRoot->getChild("Window_AddNewObj/Editbox_Name"));
    irr::core::stringw name = edit->getText().c_str();
    edit = static_cast<CEGUI::Editbox*>(this->guiRoot->getChild("Window_AddNewObj/Editbox_Mesh"));
    irr::core::stringw meshPath = edit->getText().c_str();
    edit =
        static_cast<CEGUI::Editbox*>(this->guiRoot->getChild("Window_AddNewObj/Editbox_Texture"));
    irr::core::stringw texturePath = edit->getText().c_str();

    debugOut("meshPath", meshPath);
    if (name == "")
    {
        this->messageBoxObj->inform("Cant create Entity", "Please enter a Name for the new Object");
        return true;
    }

    if (meshPath == "")
    {
        this->messageBoxObj->inform("Cant create Entity", "Please enter a Mesh for the new Object");
        return true;
    }

    if (this->game->getDevice()->getFileSystem()->existFile(irr::io::path(meshPath.c_str())) == true)
    {
        debugOut("sfv");
        irr::io::path extension = "";
        irr::core::getFileNameExtension(extension, irr::io::path(meshPath.c_str()));
        extension.make_lower();

        if ((extension == ".md2" || extension == ".b3d" || extension == ".x" || extension == ".dae") == false)
        {
            this->messageBoxObj->inform("Cant create Entity",
                                        "MeshFile-extension '" + std::string(extension.c_str()) + "' is unsupported");
            Error::errContinue("MeshFile-extension '", extension.c_str(), "' is unsupported");
            return true;
        }
    }
    else
    {
        this->messageBoxObj->inform("Cant create Entity",
                                    "MeshPath '" + std::string(irr::core::stringc(meshPath).c_str()) + "' does not exist");
        Error::errContinue("MeshPath '", meshPath.c_str(), "' does not exist");
        return true;
    }

    if ((texturePath != "" and
         this->game->getDevice()->getFileSystem()->existFile(irr::io::path(texturePath.c_str()))) == true)
    {
        irr::io::path extension = "";
        irr::core::getFileNameExtension(extension, irr::io::path(texturePath.c_str()));
        extension.make_lower();

        if ((extension == ".jpg" || extension == ".pcx" || extension == ".png" || extension == ".ppm" ||
             extension == ".pgm" || extension == ".pbm" || extension == ".psd" || extension == ".tga" ||
             extension == ".bmp" || extension == ".wal" || extension == ".rgb" || extension == ".rgba") == false)
        {
            this->messageBoxObj->inform("Cant create Entity",
                                        "TextureFile-extension '" + std::string(extension.c_str()) + "'is unsupported");
            Error::errContinue("TextureFile-extension '", extension.c_str(), "'is unsupported");
            return true;
        }
    }
    else if (texturePath != "")
    {
        this->messageBoxObj->inform("Cant create Entity",
                                    "TexturePath '" + std::string(irr::core::stringc(texturePath).c_str()) +
                                        "' does not exist");
        Error::errContinue("TexturePath '", texturePath.c_str(), "' does not exist");
        return true;
    }


    /*StaticObject* so = new StaticObject( this->game,
                                         0, //object id //TODO set real id
                                         meshPath,
                                         texturePath,
                                         ObjectOwner::OO_STATIC, //TODO rework
                                         0, //layer for GameObject::loadModel
                                         irr::core::vector3df(0, -1000, 0),  //position
                                         irr::core::vector3df(0, -0, 0),     //rot
                                         irr::core::vector3df(1, 1, 1));
    so->setName(name);
    this->addStaticObjectTemplateToTheTreeStructure(so);*/

    MapEditorState* mapEditorState = reinterpret_cast<MapEditorState*>(this->game->getCurrentState());
    // mapEditorState->unselectObject();
    mapEditorState->setEditorStateTo(MAPEDIT_STATE::MES_OBJECT_PLACING);
    // mapEditorState->updateSelectedObject(so);
    // this->selectStaticObjectItem(so);

    this->guiRoot->getChild("Window_AddNewObj")->setVisible(false);
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleNewObjWindowButten_Cancel(const CEGUI::EventArgs& args)
{
    debugOut("handleNewObjWindowButten_Cancel");
    (void) args;

    this->guiRoot->getChild("Window_AddNewObj")->setVisible(false);
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleNewObjWindowButton_LoadMeshFile(const CEGUI::EventArgs& args)
{
    debugOut("handleNewObjWindowButton_LoadMeshFile");
    (void) args;

    CEGUI::Editbox* const meshEdit = static_cast<CEGUI::Editbox* const>(
        this->guiRoot->getChild("Window_AddNewObj/Editbox_Mesh"));

    this->initFileDialog("media/models/", nullptr, meshEdit);
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleNewObjWindowButton_LoadTextureFile(const CEGUI::EventArgs& args)
{
    debugOut("handleNewObjWindowButton_LoadTextureFile");
    (void) args;

    CEGUI::Editbox* const textureEdit = static_cast<CEGUI::Editbox* const>(
        this->guiRoot->getChild("Window_AddNewObj/Editbox_Texture"));

    this->initFileDialog("media/models/", nullptr, textureEdit);

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleEntityTreeItemChanged(const CEGUI::EventArgs& args_)
{
    /*debugOut("handleEntityTreeItemChanged");
    (void) args_;

    MapEditorState* mapEditorState =
    reinterpret_cast<MapEditorState*>(this->game->getCurrentState()); const CEGUI::TreeItem* const
    selectedItem = this->entityTreeRoot->getFirstSelectedItem(); if (selectedItem == nullptr)
    {
        mapEditorState->setEditorStateTo(MAPEDIT_STATE::MES_OBJECT_NOTHINGSELECTED);
        return true;
    }

    static_assert(sizeof(entityID_t) <= sizeof(decltype(selectedItem->getUserData())),
                  "can't fit blueprint index into userData!");
    entityID_t selectionIndex = reinterpret_cast<entityID_t>(selectedItem->getUserData());
    // mapEditorState->unselectObject(false);
    if (selectedItem->getID() == ListIDs::Blueprint)
    {
        // selectionIndex is a blueprint
        mapEditorState->setEditorStateTo(MAPEDIT_STATE::MES_OBJECT_PLACING);
        const auto blueprintIndex = selectionIndex;
        mapEditorState->setSelectedBlueprint(blueprintIndex);
    }
    else if (selectedItem->getID() == ListIDs::ExistingObject)
    {
        mapEditorState->switchCameraPosition(selectionIndex);
        mapEditorState->setEditorStateTo(MAPEDIT_STATE::MES_OBJECT_MANIPULATION);
        mapEditorState->setSelectedEntity(selectionIndex);
    }*/

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleSelectedObjectPositionChanged(const CEGUI::EventArgs&)
{
    /*  debugOutLevel(15, "handleSelectedObjectPositionChanged");
      if (this->posSync != nullptr)
      {
          const irr::core::vector2df newPos(
              irr::f32(static_cast<CEGUI::Spinner*>(
                           this->toolBoxPageEntity->getChild("Spinner_Position_X"))
                           ->getCurrentValue()),
              irr::f32(static_cast<CEGUI::Spinner*>(
                           this->toolBoxPageEntity->getChild("Spinner_Position_Y"))
                           ->getCurrentValue()));

          this->posSync->setFrom(this, newPos);
      }*/

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleSelectedObjectOffsetChanged(const CEGUI::EventArgs&)
{
    /*debugOutLevel(15, "handleSelectedObjectOffsetChanged");
    if (this->offsetSync != nullptr)
    {
        const irr::f32 newOffset = irr::f32(
            static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Offset"))->getCurrentValue());

        this->offsetSync->setFrom(this, newOffset);
    }*/

    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleSelectedObjectRotationChanged(const CEGUI::EventArgs&)
{
    /*debugOutLevel(15, "handleSelectedObjectRotationChanged");
    if (this->rotationSync != nullptr)
    {
        const irr::core::vector3df newRotation(
            irr::f32(static_cast<CEGUI::Spinner*>(
                         this->toolBoxPageEntity->getChild("Spinner_Rotation_X"))
                         ->getCurrentValue()),
            irr::f32(static_cast<CEGUI::Spinner*>(
                         this->toolBoxPageEntity->getChild("Spinner_Rotation_Y"))
                         ->getCurrentValue()),
            irr::f32(static_cast<CEGUI::Spinner*>(
                         this->toolBoxPageEntity->getChild("Spinner_Rotation_Z"))
                         ->getCurrentValue()));

        this->rotationSync->setFrom(this, newRotation);
    }*/
    return true;
}

// ---------------------------------------------------------------------------
bool MapEditorGui::handleSelectedObjectScaleChanged(const CEGUI::EventArgs&)
{
    /*debugOutLevel(15, "handleSelectedObjectScaleChanged");
    if (this->scaleSync != nullptr)
    {
        debugOut("handleSelectedObjectScaleChanged do");
        const irr::core::vector3df newScale(
            irr::f32(
                static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Scale_X"))->getCurrentValue()),
            irr::f32(
                static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Scale_Y"))->getCurrentValue()),
            irr::f32(
                static_cast<CEGUI::Spinner*>(this->toolBoxPageEntity->getChild("Spinner_Scale_Z"))->getCurrentValue()));


        this->scaleSync->setFrom(this, newScale);
    }*/
    return true;
}
