/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAPEDITORGUI_H
#define MAPEDITORGUI_H

//#include <irrlicht/IFileSystem.h>
#include <CEGUI/RendererModules/Irrlicht/Texture.h>
#include <irrlicht/irrString.h> // needed by modifiableParameters
#include <irrlicht/path.h>
#include <irrlicht/vector3d.h>

#include "../../ecs/components.h"
#include "../../ecs/entity.h"
#include "../../mapeditor/modifiableParameters.h"

// forward declarations
class Game;
class MapEditorState;
class MessageBoxGui;
class GameObjectContainer;
template <typename T>
class GuiSyncedValue;
namespace CEGUI
{
    class Window;
    class EventArgs;
} // namespace CEGUI
namespace irr
{
    namespace io
    {
        class IFileList;
    }
} // namespace irr


class MapEditorGui // TODO add a flag which store whether unsaved map-modifications was done
{
   public:
    MapEditorGui(const std::string file, Game* const gameptr);
    MapEditorGui(const MapEditorGui&) = delete;
    MapEditorGui operator=(const MapEditorGui&) = delete;
    virtual ~MapEditorGui();

    void subscribeEvents();

    bool Map_FILE_SELECTED;
    bool Texture_FILE_SELECTED;

    void updateTextureButtons();

    // entity placement
    void addEntityBlueprintToTree(const std::string type, const std::string name, const entityID_t blueprintIndex);

    void addEntityFromBlueprintToTree(const entityID_t entityID, const std::string blueprintName);
    void deleteEntityFromTree(const entityID_t entityID);

    void setSelectedBlueprint(const entityID_t blueprintIndex);
    void setSelectedEntity(const entityID_t entityID);

    void unlockObjectInfoSpinner();
    void lockObjectInfoSpinner();


    void transferModifiableParameters(ModifiableParameters* modifiableParameters_)
    {
        this->modifiableParameters = modifiableParameters_;
    };

    void initFileDialog(std::string proposalFile_ = "",
                        bool* const fileSelectedFlag = nullptr,
                        CEGUI::Editbox* const editboxWaitingForFile_ = nullptr);
    irr::io::path getSelectedPath() { return this->fileDialog.selectedFile; };
    CEGUI::Window* getRoot() const { return this->guiRoot; };
    // CEGUI::Window* getToolBoxPageTexturing() const { return this->toolBoxPageTexturing; };
    // CEGUI::Window* getToolBoxPageEntity() const { return this->toolBoxPageEntity; };
    MessageBoxGui* getMessageBox() const { return this->messageBoxObj; };
    void setPosSync(GuiSyncedValue<irr::core::vector2df>* posSync_);
    void setOffsetSync(GuiSyncedValue<irr::f32>* offsetSync_);
    void setRotationSync(GuiSyncedValue<irr::core::vector3df>* rotationSync_);
    void setScaleSync(GuiSyncedValue<irr::core::vector3df>* scaleSync_);


   private:
    enum ListIDs
    {
        Types,
        Blueprint,
        ExistingObject
    };

    /// @brief used to syncronize changes of an entitys position between MapEditorState and the gui
    // GuiSyncedValue<irr::core::vector2df>* posSync = nullptr;
    /// @brief used to syncronize changes of an entitys offset between MapEditorState and the gui
    // GuiSyncedValue<irr::f32>* offsetSync = nullptr;
    /// @brief used to syncronize the rotation of a selected entity between the MapEditorState and
    /// the MapEditorGui
    // GuiSyncedValue<irr::core::vector3df>* rotationSync = nullptr;
    /// @brief used to syncronize the scale of a selected entity between the MapEditorState and the
    /// MapEditorGui
    // GuiSyncedValue<irr::core::vector3df>* scaleSync = nullptr;

    ///< Hold a Pointer to game, so no method need it as Attribute
    Game* const game;

    ///< Hold a Pointer to the Root of the Setting-Menu-Gui
    CEGUI::Window* const guiRoot;

    ///< Holds Pointer to the Pages of the Setting-Menu-Gui
    // CEGUI::Window* const toolBoxPageTexturing;
    // CEGUI::Window* const toolBoxPageEntity;

    // CEGUI::Tree* const entityTreeRoot;

    ///< Hold a Pointer to a Messagebox Object
    MessageBoxGui* messageBoxObj; // Cant be a const ptr, because of creating order

    struct SFileDialog
    {
        explicit SFileDialog(irr::io::path dafaultWorkingDirectory_)
            : dafaultWorkingDirectory(dafaultWorkingDirectory_)
        {
        }

        SFileDialog(const SFileDialog&) = default;
        SFileDialog& operator=(const SFileDialog& other) = default;

        irr::io::path const dafaultWorkingDirectory;

        irr::io::path currentWorkingDirectory = "";
        irr::io::IFileList* fileList = nullptr;

        irr::io::path selectedFile = "NONE";
        bool* fileSelectedFlag = nullptr;
        CEGUI::Editbox* editboxWaitingForFile = nullptr;
    };

    SFileDialog fileDialog;

    void updateFileDialog(irr::io::path basisPath);
    void closeFileDialog();

    /// @brief parameters used to set the inital states of the buttons
    const ModifiableParameters defaultParameters = ModifiableParameters();

    ModifiableParameters* modifiableParameters = nullptr;

    CEGUI::TreeItem* getItemFromEntityID(const entityID_t entityID);
    CEGUI::TreeItem* getItemFromBlueprintIndex(const entityID_t blueprintIndex);

    /// @brief set the content of the position display for an entity (called from this->posSync if
    /// changed in the MapEditorState)
    /// @param position
    void setPositionDisplay(const irr::core::vector2df position);
    void setOffsetDisplay(const irr::f32 offset);
    void setRotationDisplay(const irr::core::vector3df rotation);
    void setScaleDisplay(const irr::core::vector3df scale);
    // void addSelectableToolBoxItemTypes();
    // void showTerrainGuiOverface();
    // void showEntitysGuiOverface();

    std::string getShortTexturePath(irr::video::ITexture* const texture, const std::string pathRoot = "name") const; // TODO check std::string const pathRoot = "name" is a good idea

    void setImageToWindow(CEGUI::Window* const myImageWindow,
                          irr::video::ITexture* const myIrrlichtTexture,
                          CEGUI::String const imageType);

    // GUI-Checkbox:
    // ALL handleX() functions return true if the supplied event was handled and false otherwise.

    bool handleLoadMap(const CEGUI::EventArgs&);
    bool handleSaveMap(const CEGUI::EventArgs&);

    bool handleQuitButton(const CEGUI::EventArgs&);
    bool handleShowToolBoxButton(const CEGUI::EventArgs&);

    bool handleMessageBoxButton_Yes(const CEGUI::EventArgs&);
    bool handleMessageBoxButton_No(const CEGUI::EventArgs&);
    bool handleMessageBoxButton_Ok(const CEGUI::EventArgs&);

    bool handleFileDialogOkButton(const CEGUI::EventArgs&);
    bool handleFileDialogCancelButton(const CEGUI::EventArgs&);
    bool handleFileDialogTreeItemSelectionChanged(const CEGUI::EventArgs&);
    bool handleFileDialogFileUpButton(const CEGUI::EventArgs&);

    bool handleTabSelectionChanged(const CEGUI::EventArgs&);

    bool handleTextureButtenSelected(const CEGUI::EventArgs&);
    bool handleAddTextureButton(const CEGUI::EventArgs&);
    bool handleDeleteTextureButton(const CEGUI::EventArgs&);
    bool handleScrollbarTextureButton(const CEGUI::EventArgs&);

    // texture drawing brush parameters
    bool handleSmoothStateChanged(const CEGUI::EventArgs&);
    bool handleOverwriteLowestStateChanged(const CEGUI::EventArgs&);
    bool handleInstantDrawStateChanged(const CEGUI::EventArgs&);
    bool handleIntensityDecreaseStateChanged(const CEGUI::EventArgs&);
    bool handleBrushSizeValueChanged(const CEGUI::EventArgs&);
    bool handleSpeedValueChanged(const CEGUI::EventArgs&);
    bool handleIntensityValueChanged(const CEGUI::EventArgs&);

    // entity modification
    bool handleAddNewObjButten(const CEGUI::EventArgs&);
    bool handleNewObjWindowButten_Create(const CEGUI::EventArgs&);
    bool handleNewObjWindowButten_Cancel(const CEGUI::EventArgs&);
    bool handleNewObjWindowButton_LoadMeshFile(const CEGUI::EventArgs&);
    bool handleNewObjWindowButton_LoadTextureFile(const CEGUI::EventArgs&);
    bool handleEntityTreeItemChanged(const CEGUI::EventArgs&);
    bool handleSelectedObjectPositionChanged(const CEGUI::EventArgs&);
    bool handleSelectedObjectOffsetChanged(const CEGUI::EventArgs&);
    bool handleSelectedObjectRotationChanged(const CEGUI::EventArgs&);
    bool handleSelectedObjectScaleChanged(const CEGUI::EventArgs&);
};

#endif // MAPEDITORGUI_H
