/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "messageBoxGui.h"

#include <CEGUI/WindowManager.h>
#include <CEGUI/widgets/PushButton.h>

#include "../../core/game.h"

MessageBoxGui::MessageBoxGui(const std::string file, CEGUI::Window* const guiRoot_)
    // FileIOException, thrown if something goes wrong while processing the file
    // filename and InvalidRequestException, thrown if filename appears to be invalid
    : guiRoot(CEGUI::WindowManager::getSingleton().loadLayoutFromFile(file))
{
    guiRoot_->addChild(this->guiRoot);
    this->subscribeEvents();
}

// ---------------------------------------------------------------------------
MessageBoxGui::~MessageBoxGui()
{
    CEGUI::WindowManager::getSingleton().destroyWindow(this->guiRoot);
}

// ---------------------------------------------------------------------------
void MessageBoxGui::subscribeEvents()
{
    this->getButtonYes()->subscribeEvent(CEGUI::PushButton::EventClicked,
                                         CEGUI::Event::Subscriber(&MessageBoxGui::handleButton, this));
    this->getButtonNo()->subscribeEvent(CEGUI::PushButton::EventClicked,
                                        CEGUI::Event::Subscriber(&MessageBoxGui::handleButton, this));
    this->getButtonOk()->subscribeEvent(CEGUI::PushButton::EventClicked,
                                        CEGUI::Event::Subscriber(&MessageBoxGui::handleButton, this));
}

// ---------------------------------------------------------------------------
void MessageBoxGui::inform(const std::string titlebarText_, const std::string mainText_, CEGUI::Window* const source_)
{
    debugOutLevel(20, "inform");
    this->addText(titlebarText_, mainText_);
    this->setButtonState(buttonState::OK);
    this->source = source_;

    this->guiRoot->getChild("FrameWindow_MessageBox")->moveToFront();
    this->guiRoot->getChild("FrameWindow_MessageBox")->setVisible(true);
    this->guiRoot->getChild("FrameWindow_MessageBox")->setModalState(true);
}

// ---------------------------------------------------------------------------
void MessageBoxGui::ask(const std::string titlebarText_, const std::string mainText_, CEGUI::Window* const source_)
{
    debugOutLevel(20, "ask");
    this->addText(titlebarText_, mainText_);
    this->setButtonState(buttonState::YESNO);
    this->source = source_;

    this->guiRoot->getChild("FrameWindow_MessageBox")->moveToFront();
    this->guiRoot->getChild("FrameWindow_MessageBox")->setVisible(true);
    this->guiRoot->getChild("FrameWindow_MessageBox")->setModalState(true);
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void MessageBoxGui::addText(const std::string titlebarText_, const std::string mainText_)
{
    this->guiRoot->getChild("FrameWindow_MessageBox")->setText(titlebarText_);
    this->guiRoot->getChild("FrameWindow_MessageBox/StaticText_MainText")->setText(mainText_);
}

// ---------------------------------------------------------------------------
void MessageBoxGui::setButtonState(const enum buttonState buttonState_)
{
    if (buttonState_ == buttonState::OK)
    {
        this->guiRoot->getChild("FrameWindow_MessageBox/Button_Yes")->setVisible(false);
        this->guiRoot->getChild("FrameWindow_MessageBox/Button_No")->setVisible(false);
        this->guiRoot->getChild("FrameWindow_MessageBox/Button_Ok")->setVisible(true);
    }
    else if (buttonState_ == buttonState::YESNO)
    {
        this->guiRoot->getChild("FrameWindow_MessageBox/Button_Yes")->setVisible(true);
        this->guiRoot->getChild("FrameWindow_MessageBox/Button_No")->setVisible(true);
        this->guiRoot->getChild("FrameWindow_MessageBox/Button_Ok")->setVisible(false);
    }
    else
    {
        Error::errContinue("Unknown Button-state");
    }
}

// ---------------------------------------------------------------------------
bool MessageBoxGui::handleButton(const CEGUI::EventArgs&)
{
    this->guiRoot->getChild("FrameWindow_MessageBox")->setModalState(false);
    this->guiRoot->getChild("FrameWindow_MessageBox")->setVisible(false);
    return true;
}
