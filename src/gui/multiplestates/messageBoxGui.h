/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MESSAGEBOXGUI_H
#define MESSAGEBOXGUI_H

#include <string>

#include <CEGUI/Window.h> // forward declaration of window is unable because of inline use

// forward declarations
class Game;
namespace CEGUI
{
    class EventArgs;
}

///< Hold informations which Button on the MessageBox-Gui should be shown
enum buttonState
{
    OK = 0,
    YESNO,

    COUNT
};


class MessageBoxGui
{
   public:
    MessageBoxGui(const std::string file, CEGUI::Window* const guiRoot_);
    MessageBoxGui(const MessageBoxGui&) = delete;
    MessageBoxGui operator=(const MessageBoxGui&) = delete;
    virtual ~MessageBoxGui();

    void subscribeEvents();

    ///< Return a Pointer to the Root of the Setting-Menu-Gui
    CEGUI::Window* getRoot() const { return this->guiRoot; };
    ///< Create a information MessageBox with just a "OK" Button
    void inform(const std::string titelbarText_, const std::string mainText_, CEGUI::Window* const source = nullptr);

    ///< Create a question MessageBox with a "Yes" and a "No" Button
    void ask(const std::string titelbarText_, const std::string mainText_, CEGUI::Window* const source = nullptr);

    ///< Return a Pointer to the Yes-Button-Window, needed to inject events for the Parent Gui
    CEGUI::Window* getButtonYes() const
    {
        return this->guiRoot->getChild("FrameWindow_MessageBox/Button_Yes");
    };
    ///< Return a Pointer to the No-Button-Window, needed to inject events for the Parent Gui
    CEGUI::Window* getButtonNo() const
    {
        return this->guiRoot->getChild("FrameWindow_MessageBox/Button_No");
    };
    ///< Return a Pointer to the Ok-Button-Window, needed to inject events for the Parent Gui
    CEGUI::Window* getButtonOk() const
    {
        return this->guiRoot->getChild("FrameWindow_MessageBox/Button_Ok");
    };

   private:
    ///< Hold a Pointer to the Root of the the MessageBox-Gui
    CEGUI::Window* const guiRoot;

    ///< Hold a Pointer to the Source of the MessageBox popup
    CEGUI::Window* source = nullptr;

    ///< Add Titlebar and main Text to the MessageBox-FrameWindow
    void addText(const std::string titlebarText_, const std::string mainText_);

    ///< Handle Button Visibility
    ///< Attribute buttonState: informations which Button on the MessageBox-Gui should be shown
    void setButtonState(const enum buttonState);

    ///< Handle Gui-Events
    ///< Just set the MessageBox-Gui invisible, the rest should be handled in the Parent Gui
    bool handleButton(const CEGUI::EventArgs& args);
};

#endif // MESSAGEBOXGUI_H
