/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2018 Florian Schulz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CEGUIENVIRONMENT_H
#define CEGUIENVIRONMENT_H

#include <CEGUI/RendererModules/Irrlicht/Texture.h>
#include <list>
#include <string>

// forward declarations
class Game;
class LobbyGui;     // TODO remove
class MapEditorGui; // TODO remove
namespace CEGUI
{
    class IrrlichtRenderer;
    class Window;
} // namespace CEGUI
namespace irr
{
    class IrrlichtDevice;
}
namespace sol
{
    class state;
}


class CEGUIEnvironment
{
   public:
    explicit CEGUIEnvironment(irr::IrrlichtDevice* device);
    CEGUIEnvironment(const CEGUIEnvironment&) = delete;
    CEGUIEnvironment operator=(const CEGUIEnvironment&) = delete;
    virtual ~CEGUIEnvironment();

    ///< Initialization of the Gui: setResourceGroupDirectory and DefauldValues for "schemes",
    ///"imagesets", "fonts", "layouts", "looknfeels" and "lua_scripts"
    void initGui(irr::IrrlichtDevice* device, sol::state* luaState);

    ///< Return a Pointer to the CEGUI IrrlichtRenderer
    inline CEGUI::IrrlichtRenderer* getRenderer() const { return this->myRenderer; }
    ///< Return a Pointer to the Root of all Gui-Windows
    CEGUI::Window* getRoot() const { return this->guiRoot; };

    std::list<std::string> initializedGuiElements = {};

    void initGuiElement(const std::string name);
    void destructGuiElement(const std::string name);

    MapEditorGui* createMapEditorGui(const std::string file, Game* const game);
    void deleteMapEditorGui();


    ///< Render the whole GUI and inject a TimePulse (the amount of time passed, in seconds, since
    /// the last time this method was called)
    void drawAll(float timeElapsed_);

    /// @param windowWidth width of the window
    /// @param windowHeight height of the window
    void notifyDisplaySizeChanged(const int windowWidth, const int windowHeight);

    // TODO make pretty
    void setImageToWindow(CEGUI::Window* const myImageWindow,
                          irr::video::ITexture* const myIrrlichtTexture,
                          CEGUI::String const imageType);

    // TODO make pretty
    std::string getShortTexturePath(irr::video::ITexture* const texture, const std::string pathRoot = "name") const;

   private:
    ///< Hold a Pointer to the CEGUI IrrlichtRenderer
    CEGUI::IrrlichtRenderer* const myRenderer;
    ///< Hold a Pointer to the Root of all Gui-Windows
    CEGUI::Window* const guiRoot;

    MapEditorGui* mapEditorGuiObj = nullptr; // TODO remove

    ///< Storing window size to check if it changed, because then we need to notify CEGUI about this.
    CEGUI::Size<int> oldWindowSize = CEGUI::Size<int>(-1, -1);

    // CEGUI::LuaScriptModule* scriptingModule;
};

#endif // CEGUIENVIRONMENT_H
