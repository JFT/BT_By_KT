/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <irrlicht/ISceneManager.h>
#include <irrlicht/IVideoDriver.h>
#include <irrlicht/IrrlichtDevice.h>
#include <irrlicht/dimension2d.h>

#include "binaryShaderLoader.h"
#include "nodeContainer.h"

// forward declarations
namespace irr
{
    class IrrlichtDevice;
    namespace scene
    {
        class ISceneManager;
        class ISceneNode;
    } // namespace scene

    namespace video
    {
        class IVideoDriver;
        class IRenderTarget;
    } // namespace video
} // namespace irr

class IRenderStage
{
   public:
    virtual ~IRenderStage()
    {
        clearNodes();
        if (this->device) this->device->drop();
    }

    virtual void initialize(irr::IrrlichtDevice* const t_device, irr::scene::ISceneManager* const t_smgr)
    {
        // Init necessary Irrlicht pointer
        this->device = t_device;
        this->device->grab();

        this->driver = t_device->getVideoDriver();

        this->smgr = t_smgr;

        setScreenResolution(driver->getScreenSize());
    }

    /// Renders the stage. This must be done between IVideoDriver::beginScene and
    /// IVideoDriver::endScene.
    /// The input render target can be passed to use existing rendered information for the Stage.
    /// A render target may be passed as the output target, else rendering will done on the
    /// backbuffer. Note: The rendertargets should normally be Screensize

    virtual void render(irr::video::IRenderTarget* input = nullptr,
                        irr::video::IRenderTarget* outputTarget = nullptr) = 0;

    // TODO: figure out a consitent handling of time input
    virtual void update(size_t timeInUs = 0) {}

    /// Important: - If you use this function to render/animate
    /// make sure you call removeNode when you drop/remove the node!!
    // TODO: figure out a safe way to treat the nodes
    void addNode(irr::scene::ISceneNode* node)
    {
        node->grab();
        nodeContainer.insert(node);
    }

    void removeNode(irr::scene::ISceneNode* node)
    {
        node->drop();
        nodeContainer.remove(node);
    }

    void setClearFlagForOutput(short flag) { clearFlag = flag; }

    virtual void setScreenResolution(const irr::core::dimension2du& res) { resolution = res; }

    void clearNodes()
    {
        for (size_t i = 0; i < nodeContainer.size(); ++i)
        {
            if (nodeContainer[i])
            {
                this->removeNode(nodeContainer[i]);
            }
        }
        nodeContainer.clear();
    }

   protected:
    void setRenderTarget(irr::video::IRenderTarget* rt,
                         irr::u16 clearFlag,
                         irr::video::SColor clearColor = irr::video::SColor(255, 0, 0, 0),
                         irr::f32 clearDepth = 1.f,
                         irr::u8 clearStencil = 0)
    {
        bool retVal = this->driver->setRenderTargetEx(rt, clearFlag, clearColor, clearDepth, clearStencil);
    }

    static constexpr size_t INITIAL_STORAGE_SIZE = 200;

    irr::IrrlichtDevice* device = nullptr;
    irr::video::IVideoDriver* driver = nullptr;
    irr::scene::ISceneManager* smgr = nullptr;

    BinaryShaderLoader binShaderLoader = BinaryShaderLoader();

    NodeContainer<irr::scene::ISceneNode*> nodeContainer =
        NodeContainer<irr::scene::ISceneNode*>(INITIAL_STORAGE_SIZE);

    short clearFlag = 0;

    // ScreenResolution
    irr::core::dimension2du resolution = irr::core::dimension2du();
};
