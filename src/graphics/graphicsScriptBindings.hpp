#pragma once

#ifndef MAKE_SERVER_
#include "graphicsPipeline.h"
#include "particles/particleSystemGPU.h"

#include "../scripting/scriptBindingHelper.hpp"

#include <sol.hpp>

bool createGraphicsBindings(sol::table& btTable)
{
    auto graphicsTable = btTable.create_named("graphics");
    SOL_ENUM(graphicsTable, RenderStageType, STATIC, ANIMATED, LIGHTING, PARTICLE, DECAL, DEBUG_STAGE);

    graphicsTable.new_usertype<DirectionalLight>(
        "DirectionalLighting",
        "new",
        sol::no_constructor SOL_FUNS(DirectionalLight, setLightDirection, getDirection, getFarValue, getNearValue)
            SOL_FUNS(DirectionalLight, setNearValue, setFarValue, setLightColor, getLightColor));

    graphicsTable.new_usertype<VectorField>("VectorField", "new", sol::no_constructor);
    //SOL_FUNS(VectorField,)); //TODO: add FgaVectorField etc...

    graphicsTable.new_usertype<ParticleSystemGPU>(
        "ParticleSystemGPU",
        "new",
        sol::no_constructor SOL_FUNS(ParticleSystemGPU, createEmitter, clearEmitters, getVectorField));


    graphicsTable.new_usertype<GraphicsPipeline>(
        "GraphicsPipeline",
        "new",
        sol::no_constructor SOL_FUNS(GraphicsPipeline, clear, addNodeToStage, setScreenSize, enableRenderStage, disableRenderStage)
            SOL_FUNS(GraphicsPipeline, getLight, getParticleSystemGPU));

    return true;
}
#endif