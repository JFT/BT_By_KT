#include "preRenderStage.h"

#include <irrlicht/IAnimatedMeshSceneNode.h>
#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/ISceneNode.h>
#include <irrlicht/ITimer.h>
#include <irrlicht/IrrlichtDevice.h>

#include "../utils/debug.h"
#include "../utils/static/error.h"

using namespace irr;


PreRenderStage::PreRenderStage() {}


PreRenderStage::~PreRenderStage() {}

void PreRenderStage::initialize(irr::IrrlichtDevice* const t_device, irr::scene::ISceneManager* const t_smgr)
{
    IRenderStage::initialize(t_device, t_smgr);
}

void PreRenderStage::render(irr::video::IRenderTarget* input, irr::video::IRenderTarget* outputTarget)
{
    if (!driver) return;


    // reset all transforms
    driver->setMaterial(video::SMaterial());
    driver->setTransform(video::ETS_PROJECTION, core::IdentityMatrix);
    driver->setTransform(video::ETS_VIEW, core::IdentityMatrix);
    driver->setTransform(video::ETS_WORLD, core::IdentityMatrix);
    for (irr::u32 i = video::ETS_COUNT - 1; i >= video::ETS_TEXTURE_0; --i)
        driver->setTransform((video::E_TRANSFORMATION_STATE) i, core::IdentityMatrix);
    // TODO: check if zwrite needs to be used
    driver->setAllowZWriteOnTransparent(false);


    auto ActiveCamera = smgr->getActiveCamera();
    ActiveCamera->OnAnimate(device->getTimer()->getTime());
    for (size_t i = 0; i < nodeContainer.size(); ++i)
    {
        if (!nodeContainer[i]) continue;

        nodeContainer[i]->OnAnimate(device->getTimer()->getTime());
    }

    /*!
    First Scene Node for prerendering should be the active camera
    consistent Camera is needed for culling
    */
    if (ActiveCamera)
    {
        ActiveCamera->render();
    }

    // let all nodes register themselves
    ActiveCamera->OnRegisterSceneNode();
    for (size_t i = 0; i < nodeContainer.size(); ++i)
    {
        if (!nodeContainer[i]) continue;

        nodeContainer[i]->OnRegisterSceneNode();
    }

    // render camera scenes
    {
        auto CurrentRenderPass = irr::scene::ESNRP_CAMERA;
        driver->getOverrideMaterial().Enabled =
            ((driver->getOverrideMaterial().EnablePasses & CurrentRenderPass) != 0);

        ActiveCamera->render();
    }
}
