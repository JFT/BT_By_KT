/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once
#include <irrlicht/IBgfxManipulator.h>
#include <string>
#include <unordered_map>
#include <vector>

#include "particleEmitterGPU.h"

class Game;
struct ParticleShaders;
class FgaFileLoader;

class ParticleSystemGPU
{
   public:
    explicit ParticleSystemGPU(irr::IrrlichtDevice* const t_device, irr::scene::ISceneManager* const t_smgr);

    ParticleSystemGPU(const ParticleSystemGPU& other) = delete;

    ParticleSystemGPU& operator=(const ParticleSystemGPU& other) = delete;

    virtual ~ParticleSystemGPU();

    ParticleEmitterGPU* createEmitter(irr::core::vector3df position = irr::core::vector3df(0, 0, 0),
                                      float spawnProbability = 0.1f,
                                      size_t particleCount = 1 << 14);

    void updateEmitters();

    void renderEmitters();

    void clearEmitters();

    VectorField* getVectorField(const irr::io::path& fname);

   private:
    irr::IrrlichtDevice* const device;
    irr::video::IVideoDriver* const driver;
    irr::scene::ISceneManager* const smgr;

    void loadShaders(){};

    ParticleShaders* particleShaders;

    std::vector<ParticleEmitterGPU*> emitters;

    // Loader for VectorField Data
    FgaFileLoader* fgaFileLoader;

    std::unordered_map<std::string, VectorField*> vectorFields;
};
