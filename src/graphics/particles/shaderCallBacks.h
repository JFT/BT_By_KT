/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <irrlicht/irrlicht.h>

#include "particle.h"

class ParticleInitCallBack : public irr::video::IShaderConstantSetCallBack
{
   public:
    ParticleInitCallBack() {}

    ParticleInitCallBack(const ParticleInitCallBack& particleInitCB) = default;

    ParticleInitCallBack& operator=(const ParticleInitCallBack& other) = delete;

    virtual ~ParticleInitCallBack() {}

    irr::core::array<irr::core::stringc> getShaderConstantNames()
    {
        irr::core::array<irr::core::stringc> constantNames;
        constantNames.push_back("parameters");
        return constantNames;
    }

    irr::core::array<irr::u32> getShaderConstantSizes()
    {
        irr::core::array<irr::u32> constantSizes;
        constantSizes.push_back(12); // Check Particleinformation
        return constantSizes;
    }

    virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32)
    {
        // irr::video::IVideoDriver* driver = services->getVideoDriver();
        services->setPixelShaderConstant(services->getPixelShaderConstantID("parameters"),
                                         &(particleInfo->globalTime),
                                         12);
    }

    Particle* particleInfo = nullptr;
};

class ParticleUpdateCallBack : public irr::video::IShaderConstantSetCallBack
{
   public:
    ParticleUpdateCallBack(){};

    ParticleUpdateCallBack(const ParticleUpdateCallBack& particleInitCB) = default;

    ParticleUpdateCallBack& operator=(const ParticleUpdateCallBack& other) = delete;

    virtual ~ParticleUpdateCallBack() {}

    irr::core::array<irr::core::stringc> getShaderConstantNames()
    {
        irr::core::array<irr::core::stringc> constantNames;
        constantNames.push_back("parameters");
        constantNames.push_back("dimension");
        constantNames.push_back("spawnArea");
        return constantNames;
    }

    irr::core::array<irr::u32> getShaderConstantSizes()
    {
        irr::core::array<irr::u32> constantSizes;
        constantSizes.push_back(12); // Check Particleinformation
        constantSizes.push_back(12);
        constantSizes.push_back(8);
        return constantSizes;
    }

    virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32)
    {
        // irr::video::IVideoDriver* driver = services->getVideoDriver();
        services->setPixelShaderConstant(services->getPixelShaderConstantID("parameters"),
                                         reinterpret_cast<float*>(particleInfo),
                                         12);
        services->setPixelShaderConstant(services->getPixelShaderConstantID("dimension"),
                                         reinterpret_cast<float*>(texBufferDim),
                                         12);
        services->setPixelShaderConstant(services->getPixelShaderConstantID("spawnArea"),
                                         reinterpret_cast<float*>(spawnArea),
                                         8);
    }

    Particle* particleInfo = nullptr;
    TextureBufferDimension* texBufferDim = nullptr;
    SpawnArea* spawnArea = nullptr;
};

class ParticleRenderCallBack : public irr::video::IShaderConstantSetCallBack
{
   public:
    ParticleRenderCallBack(){};

    ParticleRenderCallBack(const ParticleRenderCallBack& particleInitCB) = default;

    ParticleRenderCallBack& operator=(const ParticleRenderCallBack& other) = delete;

    virtual ~ParticleRenderCallBack() {}

    irr::core::array<irr::core::stringc> getShaderConstantNames()
    {
        irr::core::array<irr::core::stringc> constantNames;
        constantNames.push_back("parameters");
        constantNames.push_back("dimension");
        return constantNames;
    }

    irr::core::array<irr::u32> getShaderConstantSizes()
    {
        irr::core::array<irr::u32> constantSizes;
        constantSizes.push_back(12); // Check Particleinformation
        constantSizes.push_back(8);
        return constantSizes;
    }

    virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32)
    {
        // irr::video::IVideoDriver* driver = services->getVideoDriver();
        services->setPixelShaderConstant(services->getPixelShaderConstantID("parameters"),
                                         reinterpret_cast<float*>(particleInfo),
                                         12);
        services->setPixelShaderConstant(services->getPixelShaderConstantID("dimension"),
                                         reinterpret_cast<float*>(texBufferDim),
                                         8);
    }

    Particle* particleInfo = nullptr;
    TextureBufferDimension* texBufferDim = nullptr;
};
