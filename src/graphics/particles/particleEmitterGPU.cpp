/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "particleEmitterGPU.h"

#include <math.h>
#include "particleShaders.h"
#include "shaderCallBacks.h"
#include "../../core/game.h"
#include "../../utils/fgaFileLoader.h"


ParticleEmitterGPU::ParticleEmitterGPU(irr::scene::ISceneNode* parent,
                                       irr::IrrlichtDevice* const t_device,
                                       irr::scene::ISceneManager* const t_smgr,
                                       ParticleShaders* t_particleShaders,
                                       irr::core::vector3df pos,
                                       float spawnProbability,
                                       size_t maxAliveParticles_)
    : irr::scene::ISceneNode(parent, t_smgr, -1, pos)
    , device(t_device)
    , driver(t_device->getVideoDriver())
    , smgr(t_smgr)
    , maxAliveParticleCount(1 << uint32_t(log2f(maxAliveParticles_)))
    , particleShaders(t_particleShaders)
    , screenQuad(CScreenQuad(true))
    , particleMaterial()
    , box()
{
#ifdef MAKE_DEBUG_
    this->setDebugName("ParticleEmitterNode");
#endif
    this->screenQuad = CScreenQuad(true);
    bool tempTexFlagMipMaps =
        driver->getTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_CREATE_MIP_MAPS);
    bool tempTexFlag32 = driver->getTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_ALWAYS_32_BIT);

    bufferRT[0] = driver->addRenderTarget();
    bufferRT[1] = driver->addRenderTarget();

    // set TextureDimensions and make sure the sidelength are a power of 2
    uint32_t exponent = uint32_t(log2f(maxAliveParticles_));
    irr::core::dimension2du textureDimension;
    if (exponent % 2)
    {
        // odd exponent
        textureDimension.Width = 1 << uint32_t((exponent + 1) / 2);
        textureDimension.Height = 1 << uint32_t(exponent / 2);
    }
    else
    {
        // even exponent
        textureDimension.Width = 1 << (exponent / 2);
        textureDimension.Height = 1 << (exponent / 2);
    }


    // no mip-maps for the position and velocity buffer textures
    driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);

    positionBufferRTT[0] =
        driver->addRenderTargetTexture(textureDimension, "positionBuffer0", irr::video::ECOLOR_FORMAT::ECF_A32B32G32R32F);
    positionBufferRTT[1] =
        driver->addRenderTargetTexture(textureDimension, "positionBuffer1", irr::video::ECOLOR_FORMAT::ECF_A32B32G32R32F);

    velocityBufferRTT[0] =
        driver->addRenderTargetTexture(textureDimension, "velocityBuffer0", irr::video::ECOLOR_FORMAT::ECF_A32B32G32R32F);
    velocityBufferRTT[1] =
        driver->addRenderTargetTexture(textureDimension, "velocityBuffer1", irr::video::ECOLOR_FORMAT::ECF_A32B32G32R32F);

    irr::core::array<irr::video::ITexture*> bufferRTT0(2);
    bufferRTT0.push_back(positionBufferRTT[0]);
    bufferRTT0.push_back(velocityBufferRTT[0]);

    irr::core::array<irr::video::ITexture*> bufferRTT1(2);
    bufferRTT1.push_back(positionBufferRTT[1]);
    bufferRTT1.push_back(velocityBufferRTT[1]);

    bufferRT[0]->setTexture(bufferRTT0, nullptr);
    bufferRT[1]->setTexture(bufferRTT1, nullptr);

    driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, tempTexFlagMipMaps);
    driver->setTextureCreationFlag(irr::video::ETCF_ALWAYS_32_BIT, tempTexFlag32);
    quadBuffer = driver->createBgfxBuffer(false);

    quadBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::POSITION,
                                   3,
                                   irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    quadBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::NORMAL,
                                   3,
                                   irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    quadBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::COLOR0,
                                   4,
                                   irr::video::E_BGFX_ATTRIBUTE_TYPE::Uint8,
                                   true);
    quadBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::TEXCOORD0,
                                   2,
                                   irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    quadBuffer->processVertexAttributes();

    instanceBuffer = driver->createBgfxBuffer(true);

    // TODO: figure out graphical modifiers per particle, to store them inside the instancebuffer
    instanceBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::TEXCOORD7,
                                       2,
                                       irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    instanceBuffer->processVertexAttributes();

    std::vector<irr::core::vector2df> instanceData(maxAliveParticleCount);
    for (auto& i : instanceData)
    {
        i = irr::core::vector2df(1.f, 1.f);
    }

    instanceBuffer->setVertexBuffer(instanceData.data(), maxAliveParticleCount, sizeof(irr::core::vector2df), false);

    // create indices for Quads

    std::vector<uint16_t> indexList = {0, 1, 2, 0, 2, 3};

    vertices[0] = irr::video::S3DVertex(-1.0f, -1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 0.0f, 1.0f);
    vertices[1] = irr::video::S3DVertex(-1.0f, 1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 0.0f, 0.0f);
    vertices[2] = irr::video::S3DVertex(1.0f, 1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 1.0f, 0.0f);
    vertices[3] = irr::video::S3DVertex(1.0f, -1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 1.0f, 1.0f);

    quadBuffer->setIndexBuffer(indexList.data(), 6, irr::video::E_INDEX_TYPE::EIT_16BIT, false);
    quadBuffer->setVertexBuffer(vertices, 4, sizeof(irr::video::S3DVertex), false);
    // we can delete the local buffer after setting it
    indexList.clear();
    particleInformation.lifeTime = 2.f;
    particleInformation.spawnProbability = spawnProbability;
    particleInformation.damping = 3.f;

    particleInformation.initialPosition = pos;
    particleInformation.timestep = 0.033f;
    particleInformation.initialVelocity = irr::core::vector3df(0, 50.f, 0);
    particleInformation.friction = 0.5f;

    texBufferDim.bufferX = float(textureDimension.Width);
    texBufferDim.bufferY = float(textureDimension.Height);


    particleMaterial.setTexture(0, positionBufferRTT[0]);
    particleMaterial.MaterialType = static_cast<irr::video::E_MATERIAL_TYPE>(particleShaders->renderShader);
    // particleMaterial.MaterialTypeParam =
    // irr::video::pack_textureBlendFunc(irr::video::E_BLEND_FACTOR::EBF_ONE,
    // irr::video::E_BLEND_FACTOR::EBF_ONE_MINUS_SRC_ALPHA);
    particleMaterial.MaterialTypeParam =
        irr::video::pack_textureBlendFuncSeparate(irr::video::E_BLEND_FACTOR::EBF_ONE,
                                                  irr::video::E_BLEND_FACTOR::EBF_ONE_MINUS_SRC_ALPHA,
                                                  irr::video::E_BLEND_FACTOR::EBF_ONE,
                                                  irr::video::E_BLEND_FACTOR::EBF_ONE_MINUS_SRC_ALPHA);
    // turn the automatic culling off
    irr::scene::ISceneNode::setAutomaticCulling(irr::scene::EAC_OFF);
}


ParticleEmitterGPU::~ParticleEmitterGPU()
{
    this->quadBuffer->drop();
    this->instanceBuffer->drop();

    if (vectorField) this->vectorField->drop();
}

void ParticleEmitterGPU::OnRegisterSceneNode()
{
    if (IsVisible)
        SceneManager->registerNodeForRendering(this, irr::scene::ESNRP_TRANSPARENT_EFFECT);

    //ISceneNode::OnRegisterSceneNode(); // no childnodes...
}

void ParticleEmitterGPU::init()
{
    driver->beginScene(short((irr::video::ECBF_COLOR | irr::video::ECBF_DEPTH)),
                       irr::video::SColor(255, 0, 0, 0));

    particleInformation.globalTime = float(device->getTimer()->getTime());

    // set pixelvalues of one of the rtts to initial position
    driver->setRenderTargetEx(bufferRT[0],
                              irr::video::E_CLEAR_BUFFER_FLAG::ECBF_COLOR |
                                  irr::video::E_CLEAR_BUFFER_FLAG::ECBF_DEPTH);

    driver->setTransform(irr::video::ETS_VIEW, irr::core::IdentityMatrix);
    driver->setTransform(irr::video::ETS_PROJECTION, irr::core::IdentityMatrix);
    particleShaders->particleInitCB->particleInfo = &particleInformation;

    screenQuad.getMaterial().setTexture(0, bufferRT[1]->getTexture()[positionIdx]);
    screenQuad.getMaterial().setTexture(1, bufferRT[1]->getTexture()[velocityIdx]);
    screenQuad.getMaterial().MaterialType =
        static_cast<irr::video::E_MATERIAL_TYPE>(particleShaders->initShader);

    screenQuad.render(driver);

    driver->setRenderTargetEx(nullptr, 0, irr::video::SColor(0));

    swapRT(bufferRT[0], bufferRT[1]);

    swapCounter = (swapCounter + 1) % 2;

    driver->endScene();
    lastFrame = std::chrono::steady_clock::now();
}

void ParticleEmitterGPU::update()
{
    const auto now = std::chrono::steady_clock::now();
    using ms = std::chrono::duration<double, std::milli>;
    auto frameTime = std::chrono::duration_cast<ms>(now - lastFrame).count();
    lastFrame = std::chrono::steady_clock::now();

    particleInformation.initialPosition = this->getAbsolutePosition();
    texBufferDim.vectorFieldPosition = this->getAbsolutePosition(); // todo: optimize save position only one time?
    particleInformation.globalTime = float(device->getTimer()->getTime());


    particleInformation.timestep = float(frameTime) / 1000.f; // frameTime in ms/1000 -> sec

    // update
    driver->setRenderTargetEx(bufferRT[0],
                              irr::video::E_CLEAR_BUFFER_FLAG::ECBF_COLOR |
                                  irr::video::E_CLEAR_BUFFER_FLAG::ECBF_DEPTH);

    driver->setTransform(irr::video::ETS_VIEW, irr::core::IdentityMatrix);
    driver->setTransform(irr::video::ETS_PROJECTION, irr::core::IdentityMatrix);

    particleShaders->particleUpdateCB->particleInfo = &this->particleInformation;
    particleShaders->particleUpdateCB->texBufferDim = &this->texBufferDim;
    particleShaders->particleUpdateCB->spawnArea = &this->spawnArea;

    screenQuad.getMaterial().setTexture(0, vectorField); // need to have the vectorfield in first
                                                         // section
    screenQuad.getMaterial().setTexture(1, bufferRT[1]->getTexture()[positionIdx]);
    screenQuad.getMaterial().setTexture(2, bufferRT[1]->getTexture()[velocityIdx]);

    screenQuad.getMaterial().MaterialType =
        static_cast<irr::video::E_MATERIAL_TYPE>(particleShaders->updateShader);

    screenQuad.render(driver);

    driver->setRenderTargetEx(nullptr, 0, irr::video::SColor(0));

    swapRT(bufferRT[0], bufferRT[1]);

    swapCounter = (swapCounter + 1) % 2;
}

void ParticleEmitterGPU::render()
{
    // render steps:
    // after updating the particles on the GPU:
    // set quadbuffer then instancingbuffer, set Material --> renderCall();

    particleMaterial.setTexture(0, bufferRT[swapCounter]->getTexture()[positionIdx]);
    particleShaders->particleRenderCB->particleInfo = &particleInformation;
    particleShaders->particleRenderCB->texBufferDim = &texBufferDim;
    driver->setBgfxBuffer(quadBuffer);
    driver->setInstanceBuffer(instanceBuffer, 0, uint32_t(maxAliveParticleCount));
    driver->setMaterial(particleMaterial);

    driver->renderCall();
}

void ParticleEmitterGPU::setTexture(irr::video::ITexture* texture)
{
    this->setMaterialTexture(1, texture);
}

void ParticleEmitterGPU::addVectorField(VectorField* vField)
{
    this->vectorField = vField->vFieldTexture;
    texBufferDim.vectorFieldExtension = (vField->vField->maximum - vField->vField->minimum);
    texBufferDim.vectorFieldResolution = convertVector3duToVector3df(vField->vField->resolution);
}

void ParticleEmitterGPU::setPosition(const irr::core::vector3df& newpos)
{
    float xDist = spawnArea.xArea.Y - spawnArea.xArea.X;
    spawnArea.xArea = irr::core::vector2df(newpos.X - xDist / 2.f, newpos.X + xDist / 2.f);

    float yDist = spawnArea.yArea.Y - spawnArea.yArea.X;
    spawnArea.yArea = irr::core::vector2df(newpos.Y - yDist / 2.f, newpos.Y + yDist / 2.f);

    float zDist = spawnArea.zArea.Y - spawnArea.zArea.X;
    spawnArea.zArea = irr::core::vector2df(newpos.Z - zDist / 2.f, newpos.Z + zDist / 2.f);

    irr::scene::ISceneNode::setPosition(newpos);
}

void ParticleEmitterGPU::setSpawnCuboid(float spawnCuboidX, float spawnCuboidY, float spawnCuboidZ)
{
    irr::core::vector3df position = this->getAbsolutePosition();

    spawnArea.xArea = irr::core::vector2df(position.X - spawnCuboidX / 2.f, position.X + spawnCuboidX / 2.f);
    spawnArea.yArea = irr::core::vector2df(position.Y - spawnCuboidY / 2.f, position.Y + spawnCuboidY / 2.f);
    spawnArea.zArea = irr::core::vector2df(position.Z - spawnCuboidZ / 2.f, position.Z + spawnCuboidZ / 2.f);
}
