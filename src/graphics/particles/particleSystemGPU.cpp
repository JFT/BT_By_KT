/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "particleSystemGPU.h"

#include "../../core/game.h"

#include "particleShaders.h"
#include "shaderCallBacks.h"
#include "../../utils/fgaFileLoader.h"

ParticleSystemGPU::ParticleSystemGPU(irr::IrrlichtDevice* const t_device, irr::scene::ISceneManager* const t_smgr)
    : device(t_device)
    , driver(t_device->getVideoDriver())
    , smgr(t_smgr)
    , particleShaders(new ParticleShaders(driver->getGPUProgrammingServices(), driver->getDriverType()))
    , emitters(std::vector<ParticleEmitterGPU*>())
    , fgaFileLoader(new FgaFileLoader())
    , vectorFields(std::unordered_map<std::string, VectorField*>())
{
    if (driver->getDriverType() != irr::video::E_DRIVER_TYPE::EDT_BGFX_OPENGL &&
        driver->getDriverType() != irr::video::E_DRIVER_TYPE::EDT_BGFX_D3D11)
    {
        Error::errTerminate(
            "GPUParticleSystem will only work with BGFX_OPENGL and BGFX_D3D11 at the moment! "
            "Please choose the correct driver!");
    }
}

ParticleSystemGPU::~ParticleSystemGPU()
{
    this->clearEmitters();
    delete particleShaders;
    delete this->fgaFileLoader;
}

ParticleEmitterGPU*
ParticleSystemGPU::createEmitter(irr::core::vector3df position, float spawnProbability, size_t particleCount)
{
    emitters.emplace_back(new ParticleEmitterGPU(
        smgr->getRootSceneNode(), device, smgr, particleShaders, position, spawnProbability, particleCount));
    emitters.back()->init();
    return emitters.back();
}

void ParticleSystemGPU::updateEmitters()
{
    for (size_t i = 0; i < emitters.size(); ++i)
    {
        emitters[i]->update();
    }
}

void ParticleSystemGPU::renderEmitters()
{
    for (size_t i = 0; i < emitters.size(); ++i)
    {
        emitters[i]->render();
    }
}

void ParticleSystemGPU::clearEmitters()
{
    for (auto& emitter : emitters)
    {
        emitter->remove();
    }
    this->emitters.clear();
}

VectorField* ParticleSystemGPU::getVectorField(const irr::io::path& fname)
{
    if (vectorFields.find(std::string(fname.c_str())) == vectorFields.end())
    {
        // element does not exist yet
        FgaVectorField* fgaVectorField = fgaFileLoader->getFgaVectorFieldFromFile(fname);
        if (fgaVectorField != nullptr)
        {
            bool tempTexFlagMipMaps =
                driver->getTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_CREATE_MIP_MAPS);
            bool tempTexFlag32 =
                driver->getTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_ALWAYS_32_BIT);

            driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);
            driver->setTextureCreationFlag(irr::video::ETCF_ALWAYS_32_BIT, true);

            irr::video::ITexture* vFieldTexture =
                driver->addTextureArray(fname, fgaVectorField->createImageArrayFromVectorField(driver));
            if (!vFieldTexture)
            {
                Error::errContinue("Failed to create VectorField with name: ", fname.c_str());
            }
            // TODO: check if ImageArray is correctly dropped
            driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, tempTexFlagMipMaps);
            driver->setTextureCreationFlag(irr::video::ETCF_ALWAYS_32_BIT, tempTexFlag32);

            vectorFields[fname.c_str()] = new VectorField(fgaVectorField, vFieldTexture);
            return vectorFields[fname.c_str()];
        }

        Error::errContinue("Failed to create VectorField from filename ", fname.c_str());
        return nullptr;
    }
    else
    {
        return vectorFields[fname.c_str()];
    }


    return nullptr;
}
