/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "particleRenderStage.h"

#include <irrlicht/IRenderTarget.h>
#include "particleSystemGPU.h"

ParticleRenderStage::ParticleRenderStage() {}


ParticleRenderStage::~ParticleRenderStage() {}

void ParticleRenderStage::initialize(irr::IrrlichtDevice* const t_device, irr::scene::ISceneManager* const t_smgr)
{
    IRenderStage::initialize(t_device, t_smgr);

    particleSystemGPU = std::make_shared<ParticleSystemGPU>(device, smgr);
}

void ParticleRenderStage::render(irr::video::IRenderTarget* input, irr::video::IRenderTarget* outputTarget)
{
    particleSystemGPU->updateEmitters();


    auto clearColour = irr::video::SColor(255, 0, 0, 0);

    if (outputTarget)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Set Outputrendertarget");
        driver->setRenderTargetEx(outputTarget, clearFlag, clearColour);
    }

    auto cam = smgr->getActiveCamera();
    driver->setTransform(irr::video::ETS_PROJECTION, cam->getProjectionMatrix());
    driver->setTransform(irr::video::ETS_VIEW, cam->getViewMatrix());

    for (size_t i = 0; i < nodeContainer.size(); ++i)
    {
        auto node = nodeContainer[i];

        if (!node)
        {
            continue;
        }

        node->render();
    }

    if (outputTarget)
    {
        driver->setRenderTargetEx(nullptr, irr::video::E_CLEAR_BUFFER_FLAG::ECBF_NONE);
    }
}

std::shared_ptr<ParticleSystemGPU> ParticleRenderStage::getParticleSystemGPU()
{
    return this->particleSystemGPU;
}
