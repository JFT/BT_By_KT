/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <chrono>
#include <irrlicht/IBgfxManipulator.h>
#include <irrlicht/irrlicht.h>

#include "particle.h"
#include "../lighting/CScreenQuad.h"

class Game;
struct ParticleShaders;
struct FgaVectorField;

struct VectorField
{
    VectorField(FgaVectorField* field, irr::video::ITexture* tex)
        : vField(field)
        , vFieldTexture(tex)
    {
    }

    FgaVectorField* vField;
    irr::video::ITexture* vFieldTexture;
};


class ParticleEmitterGPU : public irr::scene::ISceneNode
{
   public:
    ParticleEmitterGPU(irr::scene::ISceneNode* parent,
                       irr::IrrlichtDevice* const t_device,
                       irr::scene::ISceneManager* const t_smgr,
                       ParticleShaders* t_particleShaders,
                       irr::core::vector3df pos = irr::core::vector3df(0, 0, 0),
                       float spawnProbability = 0.1f,
                       size_t maxAliveParticles_ = 1 << 14);
    virtual ~ParticleEmitterGPU();

    ParticleEmitterGPU(const ParticleEmitterGPU& particleEmitter) = default;

    ParticleEmitterGPU& operator=(const ParticleEmitterGPU& other) = delete;

    virtual void OnRegisterSceneNode();

    void init();

    void update();

    virtual void render();

    void setTexture(irr::video::ITexture* texture);

    /// pass a Vectorfield in the format of a 3D Texture
    /// internally the vectorfield is stored as a textureArray (3D Texture)
    /// the number of images in the 3D texture corresponds to the z-coordinate
    /// the dimension.x/y of the image correspond to x/y of the vector field
    void addVectorField(VectorField* vField);

    virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const { return box; }
    virtual irr::u32 getMaterialCount() const { return 1; }

    ///\brief returns the Material
    /// be careful when you alter textures of the material!
    /// don't change texture 0!! (it is used as the position buffer on the gpu)
    virtual irr::video::SMaterial& getMaterial(irr::u32) override { return particleMaterial; }

    //! Sets the position of the node relative to its parent.
    /** Note that the position is relative to the parent.
    \param newpos New relative position of the scene node. */
    virtual void setPosition(const irr::core::vector3df& newpos);

    ///\brief sets the spawn volume (Cuboid) around the emitter position
    void setSpawnCuboid(float spawnCuboidX, float spawnCuboidY, float spawnCuboidZ);


   private:
    inline void swapRT(irr::video::IRenderTarget*& one, irr::video::IRenderTarget*& two)
    {
        irr::video::IRenderTarget* temp = one;
        one = two;
        two = temp;
    }

    irr::IrrlichtDevice* const device;

    irr::video::IVideoDriver* const driver;

    irr::scene::ISceneManager* const smgr;

    const size_t maxAliveParticleCount;

    // timing information
    std::chrono::time_point<std::chrono::steady_clock> lastFrame;

    irr::video::IRenderTarget* bufferRT[2];
    irr::video::ITexture* positionBufferRTT[2];
    irr::video::ITexture* velocityBufferRTT[2];
    const int positionIdx = 0;
    const int velocityIdx = 1;

    ParticleShaders* particleShaders;


    CScreenQuad screenQuad;
    // use the instancebuffer to store additional information inside a vec
    irr::video::IBgfxBuffer* instanceBuffer = nullptr;
    irr::video::IBgfxBuffer* quadBuffer = nullptr;

    Particle particleInformation = Particle();
    TextureBufferDimension texBufferDim = TextureBufferDimension();
    SpawnArea spawnArea = SpawnArea();

    irr::video::S3DVertex vertices[4];

    irr::video::SMaterial particleMaterial;
    // bounding box
    irr::core::aabbox3d<irr::f32> box;

    size_t swapCounter = 1;

    bool initialized = false;

    // vectorField in textureformat
    irr::video::ITexture* vectorField = nullptr;

    //
    inline irr::core::vector3df convertVector3duToVector3df(const irr::core::vector3d<irr::u32>& invect)
    {
        return irr::core::vector3df(float(invect.X), float(invect.Y), float(invect.Z));
    }
};
