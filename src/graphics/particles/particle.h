/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <irrlicht/vector2d.h>
#include <irrlicht/vector3d.h>

struct Particle
{
    float globalTime = 0.f;
    float lifeTime = 0.f;
    float spawnProbability = 0.f;
    float damping = 0.f;

    irr::core::vector3df initialPosition = irr::core::vector3df();
    float timestep = 0.f;

    irr::core::vector3df initialVelocity = irr::core::vector3df();
    float friction = 0.f;
};

struct TextureBufferDimension
{
    irr::core::vector3df vectorFieldExtension = irr::core::vector3df();
    float bufferX = 0.f;
    irr::core::vector3df vectorFieldPosition = irr::core::vector3df();
    float bufferY = 0.f;
    irr::core::vector3df vectorFieldResolution = irr::core::vector3df();
    float modifier = 1.1f;
};

struct SpawnArea
{
    irr::core::vector2df xArea = irr::core::vector2df();
    irr::core::vector2df yArea = irr::core::vector2df();
    irr::core::vector2df zArea = irr::core::vector2df();
    float mod1 = 1.0f;
    float mod2 = 1.0f;
};