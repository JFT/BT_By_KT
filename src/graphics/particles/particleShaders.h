/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <irrlicht/irrlicht.h>

#include <string>

#include "../../utils/static/error.h"

#include "../binaryShaderLoader.h"

#include "shaderCallBacks.h"


struct ParticleShaders
{
    static constexpr char const PARTICLE_SHADER_PATH[] = "shader/particles/";

    explicit ParticleShaders(irr::video::IGPUProgrammingServices* t_gpu, irr::video::E_DRIVER_TYPE driverType)
        : gpu(t_gpu)
        , particleInitCB(new ParticleInitCallBack())
        , particleUpdateCB(new ParticleUpdateCallBack)
        , particleRenderCB(new ParticleRenderCallBack())
    {
        std::string typeExt = "";
        if (driverType == irr::video::E_DRIVER_TYPE::EDT_BGFX_OPENGL)
        {
            typeExt = "_glsl_bgfx.bin";
        }
        else if (driverType == irr::video::E_DRIVER_TYPE::EDT_BGFX_D3D11)
        {
            typeExt = "_hlsl_DX11_bgfx.bin";
        }
        else
        {
            Error::errTerminate("DriverType not Supported for Particlesystem! Use OpenGL or D3D11");
        }

        auto loadShader = [this, typeExt](std::string shaderName) {
            return BinaryShaderLoader::loadShader(std::string(PARTICLE_SHADER_PATH) + shaderName + typeExt);
        };

        auto init_vs = loadShader("init_particles_vs");
        auto init_fs = loadShader("init_particles_fs");

        initShader = gpu->addHighLevelShaderMaterial(init_vs.data(),
                                                     init_vs.size(),
                                                     "main",
                                                     irr::video::EVST_VS_5_0,
                                                     init_fs.data(),
                                                     init_fs.size(),
                                                     "main",
                                                     irr::video::EPST_PS_5_0,
                                                     particleInitCB,
                                                     particleInitCB->getShaderConstantNames(),
                                                     particleInitCB->getShaderConstantSizes());

        auto update_vs = loadShader("update_particles_vs");
        auto update_fs = loadShader("update_particles_fs");

        updateShader = gpu->addHighLevelShaderMaterial(update_vs.data(),
                                                       update_vs.size(),
                                                       "main",
                                                       irr::video::EVST_VS_5_0,
                                                       update_fs.data(),
                                                       update_fs.size(),
                                                       "main",
                                                       irr::video::EPST_PS_5_0,
                                                       particleUpdateCB,
                                                       particleUpdateCB->getShaderConstantNames(),
                                                       particleUpdateCB->getShaderConstantSizes());

        auto particle_vs = loadShader("particle_vs");
        auto particle_fs = loadShader("particle_fs");

        renderShader = gpu->addHighLevelShaderMaterial(particle_vs.data(),
                                                       particle_vs.size(),
                                                       "main",
                                                       irr::video::EVST_VS_5_0,
                                                       particle_fs.data(),
                                                       particle_fs.size(),
                                                       "main",
                                                       irr::video::EPST_PS_5_0,
                                                       particleRenderCB,
                                                       particleRenderCB->getShaderConstantNames(),
                                                       particleRenderCB->getShaderConstantSizes(),
                                                       irr::video::E_MATERIAL_TYPE::EMT_ONETEXTURE_BLEND); // use one_texture_blend to be able to pack blendfunctions
    }

    ~ParticleShaders()
    {
        this->particleInitCB->drop();
        this->particleUpdateCB->drop();
        this->particleRenderCB->drop();
    }

    irr::video::IGPUProgrammingServices* gpu = nullptr;


    int initShader = -1;
    int updateShader = -1;
    int renderShader = -1;
    ParticleInitCallBack* particleInitCB;
    ParticleUpdateCallBack* particleUpdateCB;
    ParticleRenderCallBack* particleRenderCB;
};
