/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <fstream>
#include <iterator>
#include <string>
#include <vector>


struct BinaryShaderLoader
{
    static std::vector<uint8_t> loadShader(const std::string& binaryFileName)
    {
        std::ifstream input(binaryFileName, std::ios::binary);
        // copies all data into buffer
        std::vector<uint8_t> buffer((std::istreambuf_iterator<char>(input)),
                                    (std::istreambuf_iterator<char>()));

        return buffer;
    }
};