/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2017 Hannes Franke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef TEXTUREARRAY_H
#define TEXTUREARRAY_H

#include <irrlicht/irrString.h>
#include <vector>

namespace irr
{
    namespace video
    {
        class ITexture;
        class IVideoDriver;
    } // namespace video
} // namespace irr

namespace Graphics
{
    /// @brief adds a texture array by loading all textures in 'imageFilenames' and combining them
    /// @param videoDriver
    /// @param imageFilenames
    /// @return the textureArray. Nullptr if loading any of the images in imageFilenames or
    /// combining them into a texture array failed. This pointer shold not be dropped. (TODO: check
    /// if it really shouldn't be dropped)
    irr::video::ITexture* addTextureArray(irr::video::IVideoDriver* const videoDriver,
                                          std::vector<irr::core::stringw> imageFilenames);
} // namespace Graphics

#endif /* ifndef TEXTUREARRAY_H */
