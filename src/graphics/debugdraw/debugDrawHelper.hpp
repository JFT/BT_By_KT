#pragma once
#include <irrlicht/vector3d.h>
#include <vector>
#include "debug_draw.hpp"

template <typename T>
constexpr std::vector<T> irrVec3ToStdVec(const irr::core::vector3d<T>& v)
{
    return {v.X, v.Y, v.Z};
}