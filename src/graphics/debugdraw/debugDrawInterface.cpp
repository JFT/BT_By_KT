#include "debugDrawInterface.hpp"

#include <cstring>
#include <iostream>
#include <irrlicht/IBgfxBuffer.h>
#include <irrlicht/IBgfxManipulator.h>
#include <irrlicht/IVideoDriver.h>
#include <numeric>
#include "debugShaders.hpp"
#include "../../utils/debug.h"


void DebugDrawInterface::beginDraw() {}

void DebugDrawInterface::endDraw() {}

void DebugDrawInterface::renderPoints()
{
    // std::cerr << "RenderPoints\n";
    videoDriver->setBgfxBuffer(pointBuffer);
    videoDriver->setMaterial(pointMaterial);
    // calling drawvertexprimitivelist with nullptr for indices and vertices will use the buffer already set in a call before.
    videoDriver->drawVertexPrimitiveList(
        nullptr, pointCount, nullptr, pointCount, irr::video::EVT_STANDARD, irr::scene::EPT_POINTS);
}

void DebugDrawInterface::renderLines()
{
    // debugOutLevel(10, "RenderLines");
    // std::cerr << "Renderlines\n";
    videoDriver->setBgfxBuffer(lineBuffer);
    videoDriver->setMaterial(lineMaterial);
    videoDriver->drawVertexPrimitiveList(
        nullptr, lineCount, nullptr, lineCount, irr::video::EVT_STANDARD, irr::scene::EPT_LINES);
}

void DebugDrawInterface::renderGlyphs()
{
    // std::cerr << "RenderGlyphs\n";
    videoDriver->setBgfxBuffer(glyphBuffer);
    videoDriver->setMaterial(glyphMaterial);
    videoDriver->drawVertexPrimitiveList(
        nullptr, glyphCount, nullptr, glyphCount, irr::video::EVT_STANDARD, irr::scene::EPT_TRIANGLES);
}

void DebugDrawInterface::renderTriangles()
{
    // std::cerr << "RenderTriangles\n";
    videoDriver->setBgfxBuffer(triangleBuffer);
    videoDriver->setMaterial(triangleMaterial);
    videoDriver->drawVertexPrimitiveList(
        nullptr, triangleCount, nullptr, triangleCount, irr::video::EVT_STANDARD, irr::scene::EPT_TRIANGLES);
}

dd::GlyphTextureHandle DebugDrawInterface::createGlyphTexture(int width, int height, const void* pixels)
{
    bool tempTexFlagMipMaps =
        videoDriver->getTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_CREATE_MIP_MAPS);

    videoDriver->setTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_CREATE_MIP_MAPS, false);
    auto image = videoDriver->createImage(irr::video::ECOLOR_FORMAT::ECF_R8,
                                          irr::core::dimension2du(width, height));

    void* data = image->getData();

    std::memcpy(data, pixels, image->getImageDataSizeInBytes());
    auto glyphTexture = videoDriver->addTexture(
        irr::io::path((std::string("glyphTexture") + std::to_string(glyphTextureCounter++)).c_str()), image);

    image->drop(); // image can be dropped
    videoDriver->setTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_CREATE_MIP_MAPS, tempTexFlagMipMaps);

    return reinterpret_cast<dd::GlyphTextureHandle>(glyphTexture);
}

void DebugDrawInterface::destroyGlyphTexture(dd::GlyphTextureHandle glyphTex)
{
    auto texture = reinterpret_cast<irr::video::ITexture*>(glyphTex);
    videoDriver->removeTexture(texture);
}

void DebugDrawInterface::drawPointList(const dd::DrawVertex* points, int count, bool depthEnabled)
{
    // TODO: it is possible that drawPointList is called multiple times if the internal debug vertex buffer is full
    // and another point,line etc. is added -> merge buffers when one already is set in the update called
    pointCount = count;
    pointBuffer->setVertexBuffer(points, count, sizeof(dd::DrawVertex), false);
    pointMaterial.Thickness = points->point.size;
}

void DebugDrawInterface::drawLineList(const dd::DrawVertex* lines, int count, bool depthEnabled)
{
    // checkAndGrowIdxsIfNeeded(count);
    lineCount = count;
    lineBuffer->setVertexBuffer(lines, count, sizeof(dd::DrawVertex), false);
}

void DebugDrawInterface::drawGlyphList(const dd::DrawVertex* glyphs, int count, dd::GlyphTextureHandle glyphTex)
{
    glyphCount = count;
    auto texture = reinterpret_cast<irr::video::ITexture*>(glyphTex);
    glyphBuffer->setVertexBuffer(glyphs, count, sizeof(dd::DrawVertex), false);
    glyphMaterial.setTexture(0, texture);
}

void DebugDrawInterface::drawTriangleList(const dd::DrawVertex* triangles, int count, bool depthEnabled)
{
    triangleCount = count;
    triangleBuffer->setVertexBuffer(triangles, count, sizeof(dd::DrawVertex), false);
}

DebugDrawInterface::DebugDrawInterface(irr::video::IVideoDriver* driver)
    : videoDriver(driver)
    , debugShaders(new DebugShaders(driver->getGPUProgrammingServices(), driver->getDriverType()))
{
    pointBuffer = videoDriver->createBgfxBuffer(true);

    pointBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::POSITION,
                                    3,
                                    irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    pointBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::COLOR0,
                                    4,
                                    irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    pointBuffer->processVertexAttributes();

    pointMaterial.MaterialType = static_cast<irr::video::E_MATERIAL_TYPE>(debugShaders->pointShader);

    lineBuffer = videoDriver->createBgfxBuffer(true);

    lineBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::POSITION,
                                   3,
                                   irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    lineBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::COLOR0,
                                   4,
                                   irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    lineBuffer->processVertexAttributes();

    lineMaterial.MaterialType = static_cast<irr::video::E_MATERIAL_TYPE>(debugShaders->lineShader);

    glyphBuffer = videoDriver->createBgfxBuffer(true);

    glyphBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::POSITION,
                                    3,
                                    irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    glyphBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::COLOR0,
                                    4,
                                    irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    glyphBuffer->processVertexAttributes();

    glyphMaterial.MaterialType = static_cast<irr::video::E_MATERIAL_TYPE>(debugShaders->glyphShader);
    glyphMaterial.BackfaceCulling = false;
    glyphMaterial.FrontfaceCulling = false;
    glyphMaterial.ZBuffer = irr::video::ECFN_DISABLED;
    glyphMaterial.ZWriteEnable = false;
    glyphMaterial.MaterialTypeParam =
        irr::video::pack_textureBlendFuncSeparate(irr::video::E_BLEND_FACTOR::EBF_SRC_ALPHA,
                                                  irr::video::E_BLEND_FACTOR::EBF_ONE_MINUS_SRC_ALPHA,
                                                  irr::video::E_BLEND_FACTOR::EBF_SRC_ALPHA,
                                                  irr::video::E_BLEND_FACTOR::EBF_ONE_MINUS_SRC_ALPHA);

    triangleBuffer = videoDriver->createBgfxBuffer(true);

    triangleBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::POSITION,
                                       3,
                                       irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);

    triangleBuffer->addVertexAttribute(irr::video::E_BGFX_VERTEX_ATTRIBUTE::COLOR0,
                                       4,
                                       irr::video::E_BGFX_ATTRIBUTE_TYPE::Float);
    triangleBuffer->processVertexAttributes();

    triangleMaterial.MaterialType = static_cast<irr::video::E_MATERIAL_TYPE>(debugShaders->triangleShader);
    triangleMaterial.MaterialTypeParam =
        irr::video::pack_textureBlendFuncSeparate(irr::video::E_BLEND_FACTOR::EBF_SRC_ALPHA,
                                                  irr::video::E_BLEND_FACTOR::EBF_ONE_MINUS_SRC_ALPHA,
                                                  irr::video::E_BLEND_FACTOR::EBF_SRC_ALPHA,
                                                  irr::video::E_BLEND_FACTOR::EBF_ONE_MINUS_SRC_ALPHA);
}

DebugDrawInterface::~DebugDrawInterface()
{
    if (pointBuffer) pointBuffer->drop();

    if (lineBuffer) lineBuffer->drop();

    if (glyphBuffer) glyphBuffer->drop();

    if (triangleBuffer) triangleBuffer->drop();

    if (debugShaders) delete debugShaders;
}
