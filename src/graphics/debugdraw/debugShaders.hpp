/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2018 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <irrlicht/irrlicht.h>

#include <string>

#include "../../utils/static/error.h"

#include "../binaryShaderLoader.h"

#include <iostream>

class DebugShaderCallBack : public irr::video::IShaderConstantSetCallBack
{
   public:
    DebugShaderCallBack() {}

    DebugShaderCallBack(const DebugShaderCallBack& debugShaderCB) = default;

    DebugShaderCallBack& operator=(const DebugShaderCallBack& other) = delete;

    virtual ~DebugShaderCallBack() {}

    irr::core::array<irr::core::stringc> getShaderConstantNames()
    {
        irr::core::array<irr::core::stringc> constantNames;
        return constantNames;
    }

    irr::core::array<irr::u32> getShaderConstantSizes()
    {
        irr::core::array<irr::u32> constantSizes;
        return constantSizes;
    }

    virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32) {}
};

struct DebugShaders
{
    static constexpr char const DEBUG_SHADER_PATH[] = "shader/debug/";

    explicit DebugShaders(irr::video::IGPUProgrammingServices* t_gpu, irr::video::E_DRIVER_TYPE driverType)
        : gpu(t_gpu)
        , debugShaderCB(new DebugShaderCallBack())
    {
        std::string typeExt = "";
        if (driverType == irr::video::E_DRIVER_TYPE::EDT_BGFX_OPENGL)
        {
            typeExt = "_glsl_bgfx.bin";
        }
        else if (driverType == irr::video::E_DRIVER_TYPE::EDT_BGFX_D3D11)
        {
            typeExt = "_hlsl_DX11_bgfx.bin";
        }
        else
        {
            Error::errTerminate("DriverType not Supported for Debug Shaders! Use OpenGL or D3D11");
        }

        auto loadShader = [this, typeExt](std::string shaderName) {
            return BinaryShaderLoader::loadShader(std::string(DEBUG_SHADER_PATH) + shaderName + typeExt);
        };

        auto point_vs = loadShader("debug_points_vs");
        auto point_fs = loadShader("debug_points_fs");

        pointShader = gpu->addHighLevelShaderMaterial(point_vs.data(),
                                                      point_vs.size(),
                                                      "main",
                                                      irr::video::EVST_VS_5_0,
                                                      point_fs.data(),
                                                      point_fs.size(),
                                                      "main",
                                                      irr::video::EPST_PS_5_0,
                                                      debugShaderCB);

        auto line_vs = loadShader("debug_lines_vs");
        auto line_fs = loadShader("debug_lines_fs");

        lineShader = gpu->addHighLevelShaderMaterial(line_vs.data(),
                                                     line_vs.size(),
                                                     "main",
                                                     irr::video::EVST_VS_5_0,
                                                     line_fs.data(),
                                                     line_fs.size(),
                                                     "main",
                                                     irr::video::EPST_PS_5_0,
                                                     debugShaderCB);

        auto glyph_vs = loadShader("debug_glyphs_vs");
        auto glyph_fs = loadShader("debug_glyphs_fs");

        glyphShader = gpu->addHighLevelShaderMaterial(glyph_vs.data(),
                                                      glyph_vs.size(),
                                                      "main",
                                                      irr::video::EVST_VS_5_0,
                                                      glyph_fs.data(),
                                                      glyph_fs.size(),
                                                      "main",
                                                      irr::video::EPST_PS_5_0,
                                                      debugShaderCB,
                                                      debugShaderCB->getShaderConstantNames(),
                                                      debugShaderCB->getShaderConstantSizes(),
                                                      irr::video::E_MATERIAL_TYPE::EMT_ONETEXTURE_BLEND); // use one_texture_blend to be able to pack blendfunctions

        auto triangle_vs = loadShader("debug_triangles_vs");
        auto triangle_fs = loadShader("debug_triangles_fs");

        triangleShader = gpu->addHighLevelShaderMaterial(triangle_vs.data(),
                                                         triangle_vs.size(),
                                                         "main",
                                                         irr::video::EVST_VS_5_0,
                                                         triangle_fs.data(),
                                                         triangle_fs.size(),
                                                         "main",
                                                         irr::video::EPST_PS_5_0,
                                                         debugShaderCB,
                                                         debugShaderCB->getShaderConstantNames(),
                                                         debugShaderCB->getShaderConstantSizes(),
                                                         irr::video::E_MATERIAL_TYPE::EMT_ONETEXTURE_BLEND); // use one_texture_blend to be able to pack blendfunctions
    }

    ~DebugShaders() { this->debugShaderCB->drop(); }

    irr::video::IGPUProgrammingServices* gpu = nullptr;


    int pointShader = -1;
    int lineShader = -1;
    int glyphShader = -1;
    int triangleShader = -1;
    DebugShaderCallBack* debugShaderCB;
};
