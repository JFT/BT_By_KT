#pragma once
#include <irrlicht/SMaterial.h>
#include <vector>
#include "debug_draw.hpp"
namespace irr
{
    namespace video
    {
        class IVideoDriver;
        class IBgfxBuffer;
        class ITexture;
    } // namespace video
} // namespace irr

class DebugShaders;
class DebugShaderCallBack;

class DebugDrawInterface : public dd::RenderInterface
{
   public:
    virtual void beginDraw();
    virtual void endDraw();

    virtual dd::GlyphTextureHandle createGlyphTexture(int width, int height, const void* pixels);
    virtual void destroyGlyphTexture(dd::GlyphTextureHandle glyphTex);

    virtual void drawPointList(const dd::DrawVertex* points, int count, bool depthEnabled);
    virtual void drawLineList(const dd::DrawVertex* lines, int count, bool depthEnabled);
    virtual void drawGlyphList(const dd::DrawVertex* glyphs, int count, dd::GlyphTextureHandle glyphTex);
    virtual void drawTriangleList(const dd::DrawVertex* triangles, int count, bool depthEnabled);

    void renderPoints();
    void renderLines();
    void renderGlyphs();
    void renderTriangles();

    // add render triangles here (for filled)

    DebugDrawInterface(irr::video::IVideoDriver* driver);
    virtual ~DebugDrawInterface();

   private:
    irr::video::IVideoDriver* videoDriver;

    DebugShaders* debugShaders = nullptr;

    irr::video::IBgfxBuffer* pointBuffer = nullptr;
    irr::video::IBgfxBuffer* lineBuffer = nullptr;
    irr::video::IBgfxBuffer* glyphBuffer = nullptr;
    irr::video::IBgfxBuffer* triangleBuffer = nullptr;

    irr::video::SMaterial pointMaterial;
    irr::video::SMaterial lineMaterial;
    irr::video::SMaterial glyphMaterial;
    irr::video::SMaterial triangleMaterial;

    int triangleCount = 0;
    int lineCount = 0;
    int glyphCount = 0;
    int pointCount = 0;

    int glyphTextureCounter = 0;
};