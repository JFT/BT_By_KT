/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ImageManipulation.h"

#include <irrlicht/IImage.h>
#include <irrlicht/ITexture.h>
#include <irrlicht/IVideoDriver.h>


irr::video::IImage* ImageManipulation::rotateClockwise(irr::video::IImage* img, irr::video::IVideoDriver* videoDriver)
{
    if (img == nullptr)
    {
        return nullptr;
    }
    else if (videoDriver == nullptr)
    {
        return nullptr;
    }

    // create a copy of the image to return
    // the new image has the dimensions rotated
    const irr::core::dimension2d<irr::u32> imageDimensions(img->getDimension().Height,
                                                           img->getDimension().Width);
    irr::video::IImage* returnImage = videoDriver->createImage(img->getColorFormat(), imageDimensions);
    for (unsigned int pixelX = 0; pixelX < imageDimensions.Width; pixelX++)
    {
        for (unsigned pixelY = 0; pixelY < imageDimensions.Height; pixelY++)
        {
            returnImage->setPixel(pixelX, pixelY, img->getPixel(pixelY, img->getDimension().Height - 1 - pixelX)); // Height - 1 because the index starts at 0 but heights start at 1
        }
    }
    // drop reference to img because it probably will be replaced with the rotated one anyway.
    img->drop();
    return returnImage;
}


irr::video::IImage* ImageManipulation::TextureToImage(irr::video::ITexture* const texture,
                                                      irr::video::IVideoDriver* videoDriver)
{
    if (texture == nullptr)
    {
        return nullptr;
    }
    else if (videoDriver == nullptr)
    {
        return nullptr;
    }

    irr::video::IImage* image = videoDriver->createImageFromData(texture->getColorFormat(),
                                                                 texture->getSize(),
                                                                 texture->lock(),
                                                                 false // copy mem
    );
    texture->unlock();
    return image;
}
