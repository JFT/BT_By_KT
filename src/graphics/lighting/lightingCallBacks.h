/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <irrlicht/IMaterialRendererServices.h>
#include <irrlicht/IShaderConstantSetCallBack.h>
#include <irrlicht/matrix4.h>
#include "lighting.h"

using namespace irr;
using namespace video;
using namespace core;


enum struct SHADOW_SAMPLING_QUALITY
{ // SamplingDistribution_#BlockerSamples_#PCFSamples
    POISSON_25_25 = 0,
    POISSON_32_64 = 1,
    POISSON_100_100 = 2,
    POISSON_64_128 = 3,
    REGULAR_49_225 = 4,
    COUNT
};

enum struct SHADOW_TECHNIQUE
{
    ESM = 0,
    PCSS = 1,
    COUNT
};

class PackDepthCallBack : public video::IShaderConstantSetCallBack
{
   public:
    PackDepthCallBack(){};
    PackDepthCallBack(const PackDepthCallBack&) = delete;
    PackDepthCallBack operator=(const PackDepthCallBack&) = delete;

    core::array<core::stringc> getShaderConstantNames()
    {
        core::array<core::stringc> constantNames;
        // constantNames.push_back("LightPos");
        //constantNames.push_back("TrapezoidTrafo"); // Trapezoid Matrix
        return constantNames;
    }

    core::array<u32> getShaderConstantSizes()
    {
        core::array<u32> constantSizes;
        // constantSizes.push_back(4);
        // constantSizes.push_back(16);
        return constantSizes;
    }

    virtual void OnSetConstants(video::IMaterialRendererServices* services, s32)
    {
        // services->setVertexShaderConstant(services->getVertexShaderConstantID("LightPos"),
        // this->farValue, 4);
        // services->setVertexShaderConstant(services->getPixelShaderConstantID("TrapezoidTrafo"),
        //                                   this->trapMatrix.pointer(),
        //                                   16);
    }

    void setFarValue(const f32 t_farValue) { this->farValue[3] = t_farValue; }
    void setTrapezoidTrafo(const matrix4& trafo) { this->trapMatrix = trafo; }

   private:
    f32 farValue[4] = {0, 0, 0, 0};
    matrix4 trapMatrix = matrix4();
};

class ShadowDeterminationCallBack : public irr::video::IShaderConstantSetCallBack
{
   public:
    /// homogenousDepth = true when NDC Depth is in [-1,1]
    ShadowDeterminationCallBack(bool homogenousDepth)
    {
        clip2Tex.setScale(core::vector3df(0.5f, 0.5f, homogenousDepth ? 0.5f : 1.f));
        clip2Tex.setTranslation(core::vector3df(0.5f, 0.5f, homogenousDepth ? 0.5f : 0.f));
    };
    ShadowDeterminationCallBack(const ShadowDeterminationCallBack&) = delete;
    ShadowDeterminationCallBack operator=(const ShadowDeterminationCallBack&) = delete;

    core::array<core::stringc> getShaderConstantNames()
    {
        core::array<core::stringc> constantNames;
        constantNames.push_back("mLightModelViewProj");
        constantNames.push_back("mLightView");
        constantNames.push_back("LightDirection");
        constantNames.push_back("LightColour");
        constantNames.push_back("AmbientColour");
        constantNames.push_back("MaterialDiffuseColour");
        constantNames.push_back("MaterialSpecularColour");
        constantNames.push_back("FogSettings");
        constantNames.push_back("ShadowParams");
        constantNames.push_back("LightRadius");
        return constantNames;
    }

    core::array<u32> getShaderConstantSizes()
    {
        core::array<u32> constantSizes;
        constantSizes.push_back(16); // mLightModelViewProj
        constantSizes.push_back(16); // mLightView
        constantSizes.push_back(4);  // vec4(LightPos.xyz, farValue)
        constantSizes.push_back(4);  // LightColour
        constantSizes.push_back(4);  // Ambient
        constantSizes.push_back(4);  // MaterialDiffuse
        constantSizes.push_back(4);  // MaterialSpecularColor
        constantSizes.push_back(4);  // FogColor (xyz) FogDensity (w)
        constantSizes.push_back(4);  // ShadowParams
        constantSizes.push_back(4);  // LightRadius
        return constantSizes;
    }

    // TODO: clean up memory usage -> saving matrices too often at the moment
    void setLight(DirectionalLight& light)
    {
        this->LightColour[0] = light.getLightColor().r;
        this->LightColour[1] = light.getLightColor().g;
        this->LightColour[2] = light.getLightColor().b;
        this->LightColour[3] = light.getLightColor().a;

        // 4th entry for the LightDirection is the far plane cutoff value
        this->LightDirection = light.getDirection();
        this->farValue = light.getFarValue();
        this->ShadowParams[2] = light.getNearValue();
        this->ViewLink = light.getViewMatrix();
        this->ProjLink = light.getProjectionMatrix();

        // this->trapMatrix = tsm.getTransformationMatrix();
        // IMPORTANT: opengl renders a vertically flipped image to a frameBuffer (=RenderTarget) if
        // using the bgfx driver
        // -> adjust the projection matrix to undo the flip
        // TODO: understand why this also works with dx11??
        this->ProjLink[1] *= -1.f;
        this->ProjLink[5] *= -1.f;
        this->ProjLink[13] *= -1.f;
        this->ProjLink[9] *= -1.f;
    }


    void setShadowTechnique(SHADOW_TECHNIQUE tech)
    {
        this->ShadowParams[0] = float(tech); // NOTE: bgfx does not support passing int/uint to shader only floats
    }

    void setShadowQuality(SHADOW_SAMPLING_QUALITY quality)
    {
        this->ShadowParams[1] = float(quality);
    }

    /// note rgb is color and alpha is power of ambient
    void setAmbient(const irr::video::SColorf& ambient)
    {
        ambientColour[0] = ambient.r;
        ambientColour[1] = ambient.g;
        ambientColour[2] = ambient.b;
        ambientColour[3] = ambient.a;
    }
    /// note rgb is color and alpha is power of diffuse material
    void setMaterialDiffuse(const irr::video::SColorf& diffuse)
    {
        materialDiffuseColour[0] = diffuse.r;
        materialDiffuseColour[1] = diffuse.g;
        materialDiffuseColour[2] = diffuse.b;
        materialDiffuseColour[3] = diffuse.a;
    }

    /// note rgb is color and alpha is power of specular material
    void setMaterialSpecular(const irr::video::SColorf& spec)
    {
        materialSpecularColour[0] = spec.r;
        materialSpecularColour[1] = spec.g;
        materialSpecularColour[2] = spec.b;
        materialSpecularColour[3] = spec.a;
    }
    /// note rgb color and alpha is fogdensity
    void setFog(const irr::video::SColorf& fog)
    {
        fogSettings[0] = fog.r;
        fogSettings[1] = fog.g;
        fogSettings[2] = fog.b;
        fogSettings[3] = fog.a;
    }

    virtual void OnSetMaterial(const SMaterial&) {}
    virtual void OnSetConstants(video::IMaterialRendererServices* services, s32)
    {
        IVideoDriver* driver = services->getVideoDriver();

        // set constants which allways need to be set
        mLightModelViewProj = ProjLink * ViewLink * driver->getTransform(video::ETS_WORLD);
        mLightModelViewProj = clip2Tex * mLightModelViewProj;
        services->setVertexShaderConstant(
            services->getVertexShaderConstantID("mLightModelViewProj"), mLightModelViewProj.pointer(), 16);

        services->setVertexShaderConstant(services->getVertexShaderConstantID("mLightView"),
                                          this->ViewLink.pointer(),
                                          16);

        vector3df lightDir = this->LightDirection;

        driver->getTransform(video::ETS_WORLD).getInverse(invWorld);
        invWorld.transformVect(lightDir);
        LightDirAndFarValue[0] = lightDir.X;
        LightDirAndFarValue[1] = lightDir.Y;
        LightDirAndFarValue[2] = lightDir.Z;
        LightDirAndFarValue[3] = this->farValue;
        services->setVertexShaderConstant(services->getVertexShaderConstantID("LightDirection"), LightDirAndFarValue, 4);

        services->setPixelShaderConstant(services->getPixelShaderConstantID("LightColour"), LightColour, 4);

        services->setPixelShaderConstant(services->getPixelShaderConstantID("AmbientColour"), ambientColour, 4);
        services->setPixelShaderConstant(
            services->getPixelShaderConstantID("MaterialDiffuseColour"), materialDiffuseColour, 4);
        services->setPixelShaderConstant(
            services->getPixelShaderConstantID("MaterialSpecularColour"), materialSpecularColour, 4);
        services->setPixelShaderConstant(services->getPixelShaderConstantID("FogSettings"), fogSettings, 4);

        services->setPixelShaderConstant(services->getPixelShaderConstantID("ShadowParams"), ShadowParams, 4);
        services->setPixelShaderConstant(services->getPixelShaderConstantID("LightRadius"), LightRadius, 4);
        this->needToSetConstants = false;
    }

    bool needToSetConstants =
        true; // if using bgfx some constants only need to be set once per light (e.g. the farValue)

    // First Entry ShadowTechnique: ESM 0 , PCSS 1
    // Second Entry SAMPLING QUALITY: 0-4
    // Third entry Light near value
    f32 ShadowParams[4] = {1.0f, 1.0f, 0.1f, 0.0f};

    // lightradius uv -  first entry u, second entry v, third entry depth-bias, the last one is not
    // used yet
    f32 LightRadius[4] = {0.25f, 0.25f, 0.0005f, 0.f}; // 0.0005f - depth bias

    core::vector3df LightDirection = core::vector3df(0.0f, 0.0f, 0.0f);
    f32 LightDirAndFarValue[4] = {0.0f, 0.0f, 0.0f, 0.0f}; // value actually put into the shader
    f32 farValue = 1.0f;
    f32 LightColour[4] = {1.0f, 1.0f, 1.0f, 1.0f};

    f32 ambientColour[4] = {1.0f, 1.0f, 1.0f, 0.0f};
    f32 materialDiffuseColour[4] = {1.0f, 1.0f, 1.0f, 0.6f};
    f32 materialSpecularColour[4] = {1.0f, 1.0f, 1.0f, 0.1f};

    f32 fogSettings[4] = {0.2f, 0.2f, 0.2f, 0.f};

    core::matrix4 invWorld = core::matrix4();

    core::matrix4 ProjLink = core::matrix4();
    core::matrix4 ViewLink = core::matrix4();
    core::matrix4 clip2Tex = core::matrix4();
    core::matrix4 u_modelViewProjection = core::matrix4();
    core::matrix4 mLightModelViewProj = core::matrix4();
    core::matrix4 trapMatrix = core::matrix4();
};

class ScreenQuadCallBack : public irr::video::IShaderConstantSetCallBack
{
   public:
    explicit ScreenQuadCallBack(bool defaultV = true)
        : defaultVertexShader(defaultV)
        , uniformDescriptors(){};

    ScreenQuadCallBack(const ScreenQuadCallBack&) = delete;
    ScreenQuadCallBack operator=(const ScreenQuadCallBack&) = delete;

    bool defaultVertexShader;

    virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32)
    {
        IVideoDriver* driver = services->getVideoDriver();

        if (uniformDescriptors.size())
        {
            irr::core::map<irr::core::stringc, SUniformDescriptor>::Iterator mapIter =
                uniformDescriptors.getIterator();

            for (; !mapIter.atEnd(); mapIter++)
            {
                if (mapIter.getNode()->getValue().fPointer == nullptr)
                {
                    continue;
                }

                services->setPixelShaderConstant(services->getPixelShaderConstantID(
                                                     mapIter.getNode()->getKey().c_str()),
                                                 mapIter.getNode()->getValue().fPointer,
                                                 mapIter.getNode()->getValue().paramCount);
            }
        }
    }

    struct SUniformDescriptor
    {
        SUniformDescriptor()
            : fPointer(nullptr)
            , paramCount(0)
        {
        }

        SUniformDescriptor(const irr::f32* fPointerIn, irr::u32 paramCountIn)
            : fPointer(fPointerIn)
            , paramCount(paramCountIn)
        {
        }

        const irr::f32* fPointer;
        irr::u32 paramCount;
    };

    irr::core::map<irr::core::stringc, SUniformDescriptor> uniformDescriptors;

    core::matrix4 u_modelViewProjection = core::matrix4();
};
