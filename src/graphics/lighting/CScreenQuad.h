/*
Copyright (C) 2007-2015 Ahmed Hilali

This software is provided 'as-is', without any express or implied warranty. In no event will the
authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial
applications, and to alter it and redistribute it freely, subject to the following restrictions:

    The origin of this software must not be misrepresented; you must not claim that you wrote the
original software. If you use this software in a product, an acknowledgement in the product
documentation would be appreciated but is not required.
    Altered source versions must be clearly marked as such, and must not be misrepresented as being
the original software.
    This notice may not be removed or altered from any source distribution.
*/

// Based on "CBaseFilter" by ItIsFree.
// Original thread: http://irrlicht.sourceforge.net/phpBB2/viewtopic.php?t=9857

// EffectHandler (released under the same license as irrlicht) based on https://github.com/monstrobishi/xeffects, modified by the BattleTanks Standalone Team


#ifndef H_XEFFECTS_SQ
#define H_XEFFECTS_SQ


#include <irrlicht/IVideoDriver.h>
#include <irrlicht/S3DVertex.h>
#include <irrlicht/SMeshBuffer.h>

// forward declarations
namespace irr
{
    namespace video
    {
        class IRenderTarget;
    }
} // namespace irr


class CScreenQuad
{
   public:
    explicit CScreenQuad(const bool bgfxRenderer = false)
        : rtt{nullptr, nullptr}
        , rt{nullptr, nullptr}
        , Material()
        , MeshBuffer()
    {
        static constexpr irr::u16 Indices[6] = {0, 1, 2, 2, 3, 0};

        Material.Wireframe = false;
        Material.Lighting = false;
        Material.ZWriteEnable = false;

        irr::video::S3DVertex vertices[4];
        // works for opengl
        vertices[0] = irr::video::S3DVertex(-1.0f, -1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 0.0f, 0.0f);
        vertices[1] = irr::video::S3DVertex(-1.0f, 1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 0.0f, 1.0f);
        vertices[2] = irr::video::S3DVertex(1.0f, 1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 1.0f, 1.0f);
        vertices[3] = irr::video::S3DVertex(1.0f, -1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 1.0f, 0.0f);
        if (bgfxRenderer)
        {
            // bgfx renders to framebuffers flipped and counters that by inverting the
            // Projection-Matrix y-axis. For normal models this behaves exactly as expected with the
            // models rendering to the RenderTarget as they would with OpenGl. But for screenQuads
            // this flipping moves the vertex coordinates around and their texcoords are no longer
            // correct.
            // -> set special texcoords when using bgfx for rendering
            vertices[0] =
                irr::video::S3DVertex(-1.0f, -1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 0.0f, 1.0f);
            vertices[1] =
                irr::video::S3DVertex(-1.0f, 1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 0.0f, 0.0f);
            vertices[2] =
                irr::video::S3DVertex(1.0f, 1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 1.0f, 0.0f);
            vertices[3] =
                irr::video::S3DVertex(1.0f, -1.0f, 0.0f, 0, 0, 1, irr::video::SColor(0x0), 1.0f, 1.0f);
        }
        MeshBuffer.append(vertices, 4, Indices, 6);

        MeshBuffer.setHardwareMappingHint(irr::scene::E_HARDWARE_MAPPING::EHM_STATIC,
                                          irr::scene::EBT_VERTEX_AND_INDEX);
    }

    CScreenQuad(const CScreenQuad& other)
        : rtt{other.rtt[0], other.rtt[1]}
        , rttDepthBuffer{other.rttDepthBuffer[0], rttDepthBuffer[1]}
        , rt{other.rt[0], other.rt[1]}
        , Material()
        , MeshBuffer()
    {
        // shallow copy because rt, rtt and rttDepthBuffer are set externally and thus have to be
        // unset externally
        this->Material = other.Material;
        this->MeshBuffer = other.MeshBuffer;
    }

    virtual ~CScreenQuad() {}
    virtual void render(irr::video::IVideoDriver* driver)
    {
        driver->setMaterial(Material);
        driver->setTransform(irr::video::ETS_WORLD, irr::core::matrix4());
        driver->drawMeshBuffer(&this->MeshBuffer);
    }

    virtual irr::video::SMaterial& getMaterial() { return Material; }
    irr::video::ITexture* rtt[2];
    irr::video::ITexture* rttDepthBuffer[2];
    irr::video::IRenderTarget* rt[2];


    CScreenQuad& operator=(const CScreenQuad& other)
    {
        if (this == &other)
        {
            return *this;
        }
        // shallow copy because rt, rtt and rttDepthBuffer are set externally and thus have to be
        // unset externally
        this->rt[0] = other.rt[0];
        this->rt[1] = other.rt[1];
        this->rtt[0] = other.rtt[0];
        this->rtt[1] = other.rtt[1];
        this->rttDepthBuffer[0] = other.rttDepthBuffer[0];
        this->rttDepthBuffer[1] = other.rttDepthBuffer[1];
        this->Material = other.Material;
        this->MeshBuffer = other.MeshBuffer;
        return *this;
    };

   private:
    irr::video::SMaterial Material;
    irr::scene::CMeshBuffer<irr::video::S3DVertex> MeshBuffer;
};

#endif
