/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "../IRenderStage.h"

#include <irrlicht/irrArray.h>
#include <vector>

#include "CScreenQuad.h"
#include "lightSettings.h"
#include "lighting.h"

namespace irr
{
    namespace scene
    {
        class ISceneNode;
    }
} // namespace irr

class PackDepthCallBack;
class ShadowDeterminationCallBack;
class ScreenQuadCallBack;

class LightingStage : public IRenderStage
{
   public:
    LightingStage();
    virtual ~LightingStage();

    virtual void initialize(irr::IrrlichtDevice* const t_device, irr::scene::ISceneManager* const t_smgr);

    virtual void render(irr::video::IRenderTarget* input = nullptr,
                        irr::video::IRenderTarget* outputTarget = nullptr);

    void setClearColour(irr::video::SColor ClearCol) { ClearColour = ClearCol; }

    irr::video::SColor getClearColour() const { return ClearColour; }

    /// Sets the global ambient color for shadowed scene nodes - rgb is color and alpha is power of
    /// ambient light
    void setAmbientColor(const irr::video::SColor& ambientColour);
    /// Gets the global ambient color.
    irr::video::SColor getAmbientColor() const { return AmbientColour; }
    /// Sets fog in lighting - rgb is fogcolor and a is fogdensity
    void setFog(const irr::video::SColorf fog);

    LightSettings getLightSettings() const { return lightSettings; }

    void setLightSettings(const LightSettings& settings);

    DirectionalLight& getLight() { return this->light; }

    virtual void setScreenResolution(const irr::core::dimension2du& res) override;

   private:
    void clearShadowMaps();

    void clearLightMap();

    void generateShadowMaps(bool regenerateAll = false);

    /// return true if all shaders are created successfull
    bool createShaders(irr::video::IGPUProgrammingServices* const gpu);

    void renderDepthPass();

    void blurShadowMap();

    void renderShadowDeterminationPass();

    void calculateDirectionalMatrices(irr::scene::ICameraSceneNode* cam);


    struct ShadowNode
    {
        ShadowNode(irr::scene::ISceneNode* t_node)
            : node(t_node)
        {
        }
        bool operator<(const ShadowNode& other) const { return node < other.node; }
        irr::scene::ISceneNode* node;
    };

    // For now only support Directional Light
    DirectionalLight light = DirectionalLight();

    LightSettings lightSettings;

    // shadow map rendertarget and texture
    irr::video::IRenderTarget* rtShadowMap = nullptr;
    irr::video::ITexture* rttShadowMap = nullptr;
    irr::video::ITexture* rttDepthShadowMap = nullptr;

    // "lightmap" from view point
    irr::video::IRenderTarget* rtLightMap = nullptr;
    irr::video::ITexture* rttLightMap = nullptr;
    // depth "
    irr::video::ITexture* rttDepthMap = nullptr;

    irr::core::dimension2du ScreenRTTSize;
    irr::video::SColor ClearColour = 0x0;
    irr::video::SColor AmbientColour = 0x0;
    irr::video::SColorf fogSetting = irr::video::SColorf(0.2f, 0.2f, 0.2f, 0.f);
    CScreenQuad ScreenQuad = CScreenQuad(true);

    // Shaders

    irr::s32 Depth = -1;
    irr::s32 DepthT = -1; // transparent

    irr::s32 ReceivingShader = -1;

    irr::s32 LightModulate = -1;
    irr::s32 ScreenQuadShader = -1;

    // vertical and horizontal blur
    irr::s32 VBlurShader = -1;
    irr::s32 HBlurShader = -1;

    static constexpr char const LIGHTING_SHADERPATH[] = "shader/lighting/";

    // Shader CallBacks
    PackDepthCallBack* depthCB = nullptr;
    ShadowDeterminationCallBack* shadowDetermCB = nullptr;
    ScreenQuadCallBack* screenQuadCB = nullptr;

    // Matrix Calculations
    irr::core::vector3df lastCamPos = irr::core::vector3df();
};
