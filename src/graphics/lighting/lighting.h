/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <cmath>
#include <vector>

#include <irrlicht/irrlicht.h>
#include "lightSettings.h"
#include "../../utils/Pow2Assert.h"


struct DirectionalLight
{
    /// Light constructor. First parameter is the direction of the light
    /// the next one is the light color. The next two are very important parameters,
    /// the far value and the near value. The higher the near value, and the lower the
    /// far value, the better the depth precision of the shadows will be, however it will
    /// cover a smaller volume. The next is the FOV, if the light was to be considered
    /// a camera, this would be similar to setting the camera's field of view. The last
    /// parameter is whether the light is directional or not, if it is, an orthogonal
    /// projection matrix will be created instead of a perspective one.
    DirectionalLight(const irr::core::vector3df& t_direction = irr::core::vector3df(0, -1.0, 0),
                     irr::video::SColorf t_lightColour = irr::video::SColor(0xffffffff),
                     irr::f32 t_nearValue = 10.f,
                     irr::f32 t_farValue = 100.f,
                     irr::f32 t_fov = irr::f32(90.0 * irr::core::DEGTORAD64))
        : direction(t_direction)
        , nearValue(t_nearValue <= 0.0f ? 0.1f : nearValue)
        , farValue(t_farValue)
        , diffuseColour(t_lightColour)
    {
    }


    void setLightDirection(const irr::core::vector3df& t_direction)
    {
        direction = t_direction;
        direction = direction.normalize();
    }

    /// Gets the light's direction (normalized)
    const irr::core::vector3df& getDirection() const { return direction; }

    /// Sets the light's view matrix.
    void setViewMatrix(const irr::core::matrix4& matrix) { viewMat = matrix; }

    void setCamerViewMatrix(const irr::core::matrix4& matrix) { cameraView = matrix; }

    void setProjectionMatrix(const irr::core::matrix4& matrix) { projMat = matrix; }

    irr::core::matrix4& getViewMatrix() { return viewMat; }

    irr::core::matrix4& getProjectionMatrix() { return projMat; }


    irr::f32 getFarValue() const { return farValue; }

    irr::f32 getNearValue() const { return nearValue; }

    const irr::video::SColorf& getLightColor() const { return diffuseColour; }
    void setLightColor(const irr::video::SColorf& lightColour) { diffuseColour = lightColour; }

    void setNearValue(const float t_near) { nearValue = t_near; }

    void setFarValue(const float t_far) { farValue = t_far; }

   private:
    irr::core::vector3df direction;
    float nearValue, farValue;
    irr::video::SColorf diffuseColour;

    irr::core::matrix4 cameraView = irr::core::matrix4();
    irr::core::matrix4 viewMat = irr::core::matrix4();
    irr::core::matrix4 projMat = irr::core::matrix4();
    irr::core::matrix4 viewProj = irr::core::matrix4();
    std::vector<irr::core::vector3df> FrustumCorners = std::vector<irr::core::vector3df>();
};
