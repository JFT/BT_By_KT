/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lightingStage.h"

#include <algorithm>
#include <irrlicht/IBgfxManipulator.h>
#include <irrlicht/IGPUProgrammingServices.h>
#include <irrlicht/IRenderTarget.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/IrrlichtDevice.h>
#include <irrlicht/SViewFrustum.h>
#include <string>

#include "../../utils/debug.h"
#include "../../utils/static/error.h"

#include "lightingCallBacks.h"
#include "../Views.h"

LightingStage::LightingStage()
    : IRenderStage()
{
    ClearColour = irr::video::SColor(255, 0, 0, 0); // set clearcolor to black
}


LightingStage::~LightingStage()
{
    if (depthCB) depthCB->drop();
    if (shadowDetermCB) shadowDetermCB->drop();
    if (screenQuadCB) screenQuadCB->drop();

    // this->clearShadowMaps();
}

void LightingStage::initialize(irr::IrrlichtDevice* const t_device, irr::scene::ISceneManager* const t_smgr)
{
    IRenderStage::initialize(t_device, t_smgr);
    shadowDetermCB = new ShadowDeterminationCallBack(driver->getDriverType() == EDT_BGFX_OPENGL);
    depthCB = new PackDepthCallBack();
    screenQuadCB = new ScreenQuadCallBack();
    generateShadowMaps(true);

    this->setScreenResolution(driver->getScreenSize());
    this->createShaders(driver->getGPUProgrammingServices());
}

void LightingStage::render(irr::video::IRenderTarget* input, irr::video::IRenderTarget* outputTarget)
{
    auto activeCamera = smgr->getActiveCamera();
    activeCamera->OnAnimate(device->getTimer()->getTime());
    activeCamera->OnRegisterSceneNode();
    activeCamera->render();

    // first pass - ShadowMap Creation Pass -> rendering Depth to ShadowMap
    driver->setRenderTargetEx(rtShadowMap,
                              irr::video::E_CLEAR_BUFFER_FLAG::ECBF_COLOR | irr::video::E_CLEAR_BUFFER_FLAG::ECBF_DEPTH,
                              SColor(0xffffffff));

    // for directionalLight we calculate the view and projectionmatrix given a camera

    calculateDirectionalMatrices(activeCamera);

    driver->setTransform(irr::video::E_TRANSFORMATION_STATE::ETS_VIEW, light.getViewMatrix());
    driver->setTransform(irr::video::E_TRANSFORMATION_STATE::ETS_PROJECTION, light.getProjectionMatrix());

    // calculate the trapezoid matrix
    // trapSM.calculateTransformationMatrix(light.getProjectionMatrix() * light.getViewMatrix(),
    //                                     *(activeCamera->getViewFrustum()));

    depthCB->setFarValue(light.getFarValue());
    // depthCB->setTrapezoidTrafo(irr::core::IdentityMatrix);
    // TODO: if more performance is needed - maybe use TrapSM
    this->renderDepthPass();

    debugOutLevel(Debug::DebugLevels::updateLoop, "depth pass for directional light finished!");

    driver->setRenderTargetEx(nullptr, 0, video::SColor(0));


    // second Pass- shadow determination -> compare depth values from shadow map and light source
    // Also uses pcss if shadow settings are high enough to generate penumbras at the borders of the
    // shadows
    debugOutLevel(Debug::DebugLevels::updateLoop, "setting render target for Shadow Determination");


    driver->setRenderTargetEx(rtLightMap,
                              irr::video::E_CLEAR_BUFFER_FLAG::ECBF_COLOR | irr::video::E_CLEAR_BUFFER_FLAG::ECBF_DEPTH,
                              SColor(0xffffffff));

    driver->setTransform(ETS_VIEW, activeCamera->getViewMatrix());
    driver->setTransform(ETS_PROJECTION, activeCamera->getProjectionMatrix());

    shadowDetermCB->setLight(light);
    this->renderShadowDeterminationPass();

    driver->setRenderTargetEx(nullptr, 0, video::SColor(0));

    // generate Output either to backbuffer or outputtarget
    debugOutLevel(Debug::DebugLevels::updateLoop, "rendering Output");

    if (outputTarget)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Set Outputrendertarget");
        driver->setRenderTargetEx(outputTarget,
                                  irr::video::E_CLEAR_BUFFER_FLAG::ECBF_COLOR |
                                      irr::video::E_CLEAR_BUFFER_FLAG::ECBF_DEPTH,
                                  ClearColour);
    }
    else
    {
        driver->setActiveView(ViewNumbers::SHADOW_COMBINATION_VIEW);
    }
    driver->setTransform(ETS_VIEW, irr::core::IdentityMatrix);
    driver->setTransform(ETS_PROJECTION, irr::core::IdentityMatrix);

    if (input)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop, "Set Inputtexture");

        ScreenQuad.getMaterial().setTexture(0, input->getTexture()[0]);
        ScreenQuad.getMaterial().setTexture(1, rttLightMap);
        ScreenQuad.getMaterial().setTexture(2, input->getDepthStencil());

        ScreenQuad.getMaterial().MaterialType = static_cast<E_MATERIAL_TYPE>(LightModulate);
        ScreenQuad.getMaterial().setFlag(irr::video::E_MATERIAL_FLAG::EMF_ZWRITE_ENABLE, true);
    }
    else
    {
        // if no input exists -> we will just copy the lightmap to the output
        ScreenQuad.getMaterial().setTexture(0, rttLightMap);
        ScreenQuad.getMaterial().MaterialType = static_cast<E_MATERIAL_TYPE>(ScreenQuadShader);
    }

    ScreenQuad.render(driver);

    if (outputTarget)
    {
        driver->setRenderTargetEx(nullptr, irr::video::E_CLEAR_BUFFER_FLAG::ECBF_NONE);
    }
    else
    {
        driver->setActiveView(ViewNumbers::DEFAULT_3D_VIEW);
    }
}

void LightingStage::setAmbientColor(const irr::video::SColor& ambientColour)
{
    AmbientColour = ambientColour;
    shadowDetermCB->setAmbient(AmbientColour);
}

void LightingStage::setFog(const irr::video::SColorf fog)
{
    fogSetting = fog;
    shadowDetermCB->setFog(fogSetting);
}

void LightingStage::setLightSettings(const LightSettings& settings)
{
    bool needFullRegenerate = false;
    if (lightSettings.shadowMapSize != settings.shadowMapSize)
    {
        if (settings.shadowMapSize > MAX_SHADOW_MAP_SIZE)
        {
            Error::errContinue("Trying to set a Shadow Map Size greater than: ", MAX_SHADOW_MAP_SIZE, " -> setting Size to MAX");

            lightSettings.shadowMapSize = MAX_SHADOW_MAP_SIZE;
        }
        else
        {
            lightSettings.shadowMapSize = settings.shadowMapSize;
        }
        needFullRegenerate = true;
    }
    generateShadowMaps(needFullRegenerate);
}

void LightingStage::setScreenResolution(const irr::core::dimension2du& res)
{
    if (res != this->resolution)
    {
        clearLightMap();
        // save texture creation flags
        bool tempTexFlagMipMaps =
            driver->getTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_CREATE_MIP_MAPS);
        bool tempTexFlag32 =
            driver->getTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_ALWAYS_32_BIT);

        this->rtLightMap = driver->addRenderTarget();
        this->resolution = res;

        this->rttLightMap =
            driver->addRenderTargetTexture(res, "LightMap", irr::video::ECOLOR_FORMAT::ECF_A8R8G8B8);
        this->rttDepthMap = driver->addRenderTargetTexture(res, "depthMap", irr::video::ECOLOR_FORMAT::ECF_D32);
        rtLightMap->setTexture(rttLightMap, rttDepthMap);

        driver->setTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_CREATE_MIP_MAPS, tempTexFlagMipMaps);
        driver->setTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_ALWAYS_32_BIT, tempTexFlag32);
    }
}

void LightingStage::clearShadowMaps()
{
    if (rttShadowMap)
    {
        driver->removeTexture(rttShadowMap);
        rttShadowMap = nullptr;
    }

    if (rtShadowMap)
    {
        rtShadowMap->drop();
        rtShadowMap = nullptr;
    }
}

void LightingStage::clearLightMap()
{
    if (rttLightMap)
    {
        driver->removeTexture(rttLightMap);
        rttDepthMap = nullptr;
    }

    if (rttDepthMap)
    {
        driver->removeTexture(rttDepthMap);
        rttDepthMap = nullptr;
    }

    if (rtLightMap)
    {
        rtLightMap->drop();
        rtLightMap = nullptr;
    }
}

void LightingStage::generateShadowMaps(bool regenerateAll)
{
    irr::core::dimension2du rttDim(lightSettings.shadowMapSize, lightSettings.shadowMapSize);
    // save texture creation flags
    bool tempTexFlagMipMaps =
        driver->getTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_CREATE_MIP_MAPS);
    bool tempTexFlag32 = driver->getTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_ALWAYS_32_BIT);

    if (regenerateAll)
    {
        // need to create new rtts so clear old ones
        clearShadowMaps();

        rtShadowMap = driver->addRenderTarget();
        irr::video::IBgfxManipulator* bgfxManipulator = static_cast<irr::video::IBgfxManipulator*>(driver);

        rttShadowMap =
            bgfxManipulator->addRenderTargetTexture(rttDim, "ShadowMap", irr::video::ECOLOR_FORMAT::ECF_D32);
        // rttDepthShadowMap =
        //    driver->addRenderTargetTexture(rttDim, "depthShadowMap",
        //    irr::video::ECOLOR_FORMAT::ECF_D32);
        // set all shadowmaps in one rendertarget
        rtShadowMap->setTexture(nullptr, rttShadowMap);
    }

    driver->setTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_CREATE_MIP_MAPS, tempTexFlagMipMaps);
    driver->setTextureCreationFlag(irr::video::E_TEXTURE_CREATION_FLAG::ETCF_ALWAYS_32_BIT, tempTexFlag32);
}

bool LightingStage::createShaders(irr::video::IGPUProgrammingServices* const gpu)
{
    auto driverType = driver->getDriverType();
    std::string typeExt = "";
    if (driverType == irr::video::E_DRIVER_TYPE::EDT_BGFX_OPENGL)
    {
        typeExt = "_glsl_bgfx.bin";
    }
    else if (driverType == irr::video::E_DRIVER_TYPE::EDT_BGFX_D3D11)
    {
        typeExt = "_hlsl_DX11_bgfx.bin";
    }
    else
    {
        Error::errTerminate("DriverType not Supported! Use BGFX with OpenGL or D3D11");
    }

    auto loadShader = [this, typeExt](std::string shaderName) {
        return BinaryShaderLoader::loadShader(std::string(this->LIGHTING_SHADERPATH) + shaderName + typeExt);
    };

    auto depthShader_vs = loadShader("shadowmap_tsm_vs");
    auto depthShader_fs = loadShader("shadowmap_tsm_fs");
    this->Depth = gpu->addHighLevelShaderMaterial(depthShader_vs.data(),
                                                  depthShader_vs.size(),
                                                  "main",
                                                  irr::video::E_VERTEX_SHADER_TYPE::EVST_VS_5_0,
                                                  depthShader_fs.data(),
                                                  depthShader_fs.size(),
                                                  "main",
                                                  irr::video::E_PIXEL_SHADER_TYPE::EPST_PS_5_0,
                                                  depthCB,
                                                  depthCB->getShaderConstantNames(),
                                                  depthCB->getShaderConstantSizes(),
                                                  irr::video::EMT_SOLID);

    if (this->Depth == -1)
    {
        return false;
    }

    this->DepthT = gpu->addHighLevelShaderMaterial(depthShader_vs.data(),
                                                   depthShader_vs.size(),
                                                   "main",
                                                   irr::video::E_VERTEX_SHADER_TYPE::EVST_VS_5_0,
                                                   depthShader_fs.data(),
                                                   depthShader_fs.size(),
                                                   "main",
                                                   irr::video::E_PIXEL_SHADER_TYPE::EPST_PS_5_0,
                                                   depthCB,
                                                   depthCB->getShaderConstantNames(),
                                                   depthCB->getShaderConstantSizes(),
                                                   irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF);

    if (this->DepthT == -1)
    {
        return false;
    }

    auto shadowDeterm_vs = loadShader("shadow_determination_tsm_vs");
    auto shadowDeterm_fs = loadShader("shadow_determination_tsm_fs");

    this->ReceivingShader = gpu->addHighLevelShaderMaterial(shadowDeterm_vs.data(),
                                                            shadowDeterm_vs.size(),
                                                            "main",
                                                            irr::video::E_VERTEX_SHADER_TYPE::EVST_VS_5_0,
                                                            shadowDeterm_fs.data(),
                                                            shadowDeterm_fs.size(),
                                                            "main",
                                                            irr::video::E_PIXEL_SHADER_TYPE::EPST_PS_5_0,
                                                            shadowDetermCB,
                                                            shadowDetermCB->getShaderConstantNames(),
                                                            shadowDetermCB->getShaderConstantSizes(),
                                                            irr::video::EMT_SOLID);

    if (this->ReceivingShader == -1)
    {
        return false;
    }

    auto screenQuad_vs = loadShader("screen_quad_vs");
    auto screenQuad_fs = loadShader("screen_quad_fs");
    auto lightMod_fs = loadShader("light_modulate_fs");

    this->LightModulate = gpu->addHighLevelShaderMaterial(screenQuad_vs.data(),
                                                          screenQuad_vs.size(),
                                                          "main",
                                                          irr::video::E_VERTEX_SHADER_TYPE::EVST_VS_5_0,
                                                          lightMod_fs.data(),
                                                          lightMod_fs.size(),
                                                          "main",
                                                          video::E_PIXEL_SHADER_TYPE::EPST_PS_5_0,
                                                          screenQuadCB);

    if (this->LightModulate == -1)
    {
        return false;
    }

    this->ScreenQuadShader = gpu->addHighLevelShaderMaterial(screenQuad_vs.data(),
                                                             screenQuad_vs.size(),
                                                             "main",
                                                             irr::video::E_VERTEX_SHADER_TYPE::EVST_VS_5_0,
                                                             screenQuad_fs.data(),
                                                             screenQuad_fs.size(),
                                                             "main",
                                                             video::E_PIXEL_SHADER_TYPE::EPST_PS_5_0,
                                                             screenQuadCB,
                                                             core::array<core::stringc>(),
                                                             core::array<u32>(),
                                                             video::EMT_TRANSPARENT_ADD_COLOR);

    if (this->ScreenQuadShader == -1)
    {
        return false;
    }


    return true;
}

void LightingStage::renderDepthPass()
{
    for (size_t i = 0; i < nodeContainer.size(); ++i)
    {
        auto sNode = nodeContainer[i];
        if (!sNode)
        {
            continue;
        }

        if (smgr->isCulled(sNode))
        {
            continue;
        }

        const irr::u32 CurrentMaterialCount = sNode->getMaterialCount();
        irr::core::array<irr::s32> BufferMaterialList(CurrentMaterialCount);
        BufferMaterialList.set_used(0);

        for (irr::u32 m = 0; m < CurrentMaterialCount; ++m)
        {
            BufferMaterialList.push_back(sNode->getMaterial(m).MaterialType);
            sNode->getMaterial(m).MaterialType = static_cast<irr::video::E_MATERIAL_TYPE>(
                BufferMaterialList[m] == irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF ? DepthT : Depth);
        }

        sNode->render();
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "render() called for node: ",
                      static_cast<void*>(&sNode),
                      "texture 0:",
                      static_cast<void*>(sNode->getMaterial(0).getTexture(0)));

        const irr::u32 BufferMaterialListSize = BufferMaterialList.size();
        for (irr::u32 m = 0; m < BufferMaterialListSize; ++m)
        {
            sNode->getMaterial(m).MaterialType =
                static_cast<irr::video::E_MATERIAL_TYPE>(BufferMaterialList[m]);
        }

    } // for (u32 i = 0; i < ShadowNodeArraySize; ++i)
}


void LightingStage::renderShadowDeterminationPass()
{
    for (size_t i = 0; i < nodeContainer.size(); ++i)
    {
        auto sNode = nodeContainer[i];

        if (!sNode)
        {
            continue;
        }

        if (smgr->isCulled(sNode))
        {
            continue;
        }

        const irr::u32 CurrentMaterialCount = sNode->getMaterialCount();
        irr::core::array<irr::s32> BufferMaterialList(CurrentMaterialCount);
        irr::core::array<irr::video::ITexture*> BufferTextureList(CurrentMaterialCount);

        irr::core::array<irr::u8> TexClampBufferU;
        irr::core::array<irr::u8> TexClampBufferV;
        irr::core::array<irr::video::E_BGFX_TEXTURE_COMPARE_FLAGS> TexCompareBuffer;
        irr::core::array<bool> TexFilterBuffer;

        for (irr::u32 m = 0; m < CurrentMaterialCount; ++m)
        {
            BufferMaterialList.push_back(sNode->getMaterial(m).MaterialType);
            BufferTextureList.push_back(sNode->getMaterial(m).getTexture(0));
            BufferTextureList.push_back(sNode->getMaterial(m).getTexture(1));
            sNode->getMaterial(m).MaterialType = static_cast<irr::video::E_MATERIAL_TYPE>(ReceivingShader);

            TexClampBufferU.push_back(sNode->getMaterial(m).TextureLayer[0].TextureWrapU);
            TexClampBufferV.push_back(sNode->getMaterial(m).TextureLayer[0].TextureWrapV);
            TexFilterBuffer.push_back(sNode->getMaterial(m).TextureLayer[0].BilinearFilter);

            TexClampBufferU.push_back(sNode->getMaterial(m).TextureLayer[1].TextureWrapU);
            TexClampBufferV.push_back(sNode->getMaterial(m).TextureLayer[1].TextureWrapV);
            TexCompareBuffer.push_back(sNode->getMaterial(m).TextureLayer[1].TextureCompareFlag);
            TexFilterBuffer.push_back(sNode->getMaterial(m).TextureLayer[1].BilinearFilter);


            sNode->getMaterial(m).TextureLayer[0].BilinearFilter = false;
            sNode->getMaterial(m).TextureLayer[0].TextureWrapU = irr::video::E_TEXTURE_CLAMP::ETC_CLAMP_TO_EDGE;
            sNode->getMaterial(m).TextureLayer[0].TextureWrapV = irr::video::E_TEXTURE_CLAMP::ETC_CLAMP_TO_EDGE;

            sNode->getMaterial(m).TextureLayer[1].TextureCompareFlag =
                irr::video::E_BGFX_TEXTURE_COMPARE_FLAGS::E_BGFX_TEXTURE_COMPARE_LEQUAL;
            sNode->getMaterial(m).TextureLayer[1].TextureWrapU = irr::video::E_TEXTURE_CLAMP::ETC_CLAMP_TO_EDGE;
            sNode->getMaterial(m).TextureLayer[1].TextureWrapV = irr::video::E_TEXTURE_CLAMP::ETC_CLAMP_TO_EDGE;
            sNode->getMaterial(m).TextureLayer[1].BilinearFilter = false;


            sNode->getMaterial(m).setTexture(0, rttShadowMap);
            sNode->getMaterial(m).setTexture(1, rttShadowMap);
        }
        shadowDetermCB->setMaterialDiffuse(irr::video::SColorf(sNode->getMaterial(0).DiffuseColor));
        shadowDetermCB->setMaterialSpecular(irr::video::SColorf(sNode->getMaterial(0).SpecularColor));
        sNode->render();

        for (irr::u32 m = 0; m < CurrentMaterialCount; ++m)
        {
            sNode->getMaterial(m).MaterialType =
                static_cast<irr::video::E_MATERIAL_TYPE>(BufferMaterialList[m]);


            /// NOTE: The order of resetting the material is important! set all Texturewraps,
            /// filters etc. and then reset the textures
            sNode->getMaterial(m).TextureLayer[0].TextureWrapU = TexClampBufferU[m];
            sNode->getMaterial(m).TextureLayer[0].TextureWrapV = TexClampBufferV[m];
            sNode->getMaterial(m).TextureLayer[0].BilinearFilter = TexFilterBuffer[m];

            sNode->getMaterial(m).TextureLayer[1].TextureWrapU = TexClampBufferU[m + 1];
            sNode->getMaterial(m).TextureLayer[1].TextureWrapV = TexClampBufferV[m + 1];
            sNode->getMaterial(m).TextureLayer[1].TextureCompareFlag = TexCompareBuffer[m];
            sNode->getMaterial(m).TextureLayer[1].BilinearFilter = TexFilterBuffer[m + 1];

            sNode->getMaterial(m).setTexture(0, BufferTextureList[m]);
            sNode->getMaterial(m).setTexture(1, BufferTextureList[m + 1]);
        }
    }
}

void LightingStage::calculateDirectionalMatrices(irr::scene::ICameraSceneNode* cam)
{
    if (cam->getPosition() == lastCamPos)
    {
        return;
    }
    lastCamPos = cam->getPosition();

    auto getIntersect = [](irr::core::plane3df plane, irr::core::line3df l) {
        irr::core::vector3df retVect;
        bool exists = plane.getIntersectionWithLine(l.start, l.getVector(), retVect);
        if (exists)
        {
            return retVect;
        }
        else
        {
            // THIS should for rts views (top down) never be reached!
            //- is reached if camera frustum edges do not intersect the plane in this case
            retVect = l.end;
            return retVect;
        }
    };

    auto buildOrthoLH =
        [](const irr::core::vector3df& minVal, const irr::core::vector3df& maxVal, float nearVal, float farVal) {
            irr::core::matrix4 M;
            M[0] = 2.f / (maxVal.X - minVal.X);
            M[1] = 0;
            M[2] = 0;
            M[3] = 0;

            M[4] = 0;
            M[5] = 2.f / (maxVal.Y - minVal.Y);
            M[6] = 0;
            M[7] = 0;

            M[8] = 0;
            M[9] = 0;
            M[10] = (1.f / (farVal - nearVal));
            M[11] = 0;

            M[12] = (minVal.X + maxVal.X) / (minVal.X - maxVal.X);
            M[13] = (maxVal.Y + minVal.Y) / (minVal.Y - maxVal.Y);
            M[14] = (nearVal / (nearVal - farVal));
            M[15] = 1;

            return M;
        };

    auto cameraViewFrustum = cam->getViewFrustum();
    irr::scene::SViewFrustum camFrust(*cameraViewFrustum);

    irr::core::plane3df mapPlane(irr::core::vector3df(0, 1, 0), 0.f);

    irr::core::line3df side0(camFrust.getNearLeftDown(), camFrust.getFarLeftDown());
    irr::core::line3df side1(camFrust.getNearRightDown(), camFrust.getFarRightDown());
    irr::core::line3df side2(camFrust.getNearRightUp(), camFrust.getFarRightUp());
    irr::core::line3df side3(camFrust.getNearLeftUp(), camFrust.getFarLeftUp());

    std::vector<irr::core::vector3df> p;
    p.push_back(getIntersect(mapPlane, side0));
    p.push_back(getIntersect(mapPlane, side1));
    p.push_back(getIntersect(mapPlane, side2));
    p.push_back(getIntersect(mapPlane, side3));


    irr::core::line2df line0(p[0].X, p[0].Z, p[2].X, p[2].Z);
    irr::core::line2df line1(p[1].X, p[1].Z, p[3].X, p[3].Z);

    auto intersect = line0.fastLinesIntersection(line1);
    irr::core::vector3df center = irr::core::vector3df(intersect.X, 0, intersect.Y);

    irr::core::matrix4 viewMatrix;
    viewMatrix.buildCameraLookAtMatrixLH(
        center - ((light.getFarValue() + 0.1f) * light.getDirection()),
        center,
        ((-light.getDirection()).dotProduct(irr::core::vector3df(1.0f, 0.0f, 1.0f)) <= FLT_EPSILON &&
         (-light.getDirection()).dotProduct(irr::core::vector3df(1.0f, 0.0f, 1.0f)) >= -FLT_EPSILON) ?
            irr::core::vector3df(0.0f, 0.0f, 1.0f) :
            irr::core::vector3df(0.0f, 1.0f, 0.0f));


    irr::scene::SViewFrustum lightFrust(*cameraViewFrustum);

    lightFrust.transform(viewMatrix);

    std::vector<irr::core::vector3df> corners(8);
    lightFrust.getBoundingBox().getEdges(corners.data());

    irr::core::vector3df maxVals, minVals;
    for (auto& c : corners)
    {
        if (c.X > maxVals.X) maxVals.X = c.X;
        if (c.X < minVals.X) minVals.X = c.X;

        if (c.Y > maxVals.Y) maxVals.Y = c.Y;
        if (c.Y < minVals.Y) minVals.Y = c.Y;

        if (c.Z > maxVals.Z) maxVals.Z = c.Z;
        if (c.Z < minVals.Z) minVals.Z = c.Z;
    }

    this->shadowDetermCB->LightRadius[0] = 10.f / lightFrust.getBoundingRadius();
    this->shadowDetermCB->LightRadius[1] = 10.f / lightFrust.getBoundingRadius();

    irr::core::matrix4 projMatrix = buildOrthoLH(minVals, maxVals, minVals.Z, maxVals.Z);

    light.setViewMatrix(viewMatrix);
    light.setProjectionMatrix(projMatrix);
}

constexpr char const LightingStage::LIGHTING_SHADERPATH[];
/*
 * Basic Shadow Calculation Approach - actual calculation is optimized a bit by using tsm:
 * http://www.comp.nus.edu.sg/~tants/tsm.html
 *
 * for each light a 'shadowmap' is generated which is just a depth-map from the perspective of
 *that light. It contains the _visible_ distance of vertices to the light.
 * Then in the normal rendering pass (from the perspective of the camera) for each vertex it's
 *_actual_ distance from the light is calculated.
 * (this is why the receive shader also needs the light-matrix)
 * The visible distance the vertex has from the light is looked up in the shadowmap (this is why
 *the receive shader has the shadowmap as a texture)
 * and compared against the actual distance. If the actual distance is LARGER than the visible
 *distance the vertex [from the perspective of the light] is occluded
 * by something else and should be in the shadow.
 *
 *   Front-View:                                            Side-View:
 *
 *       X  <- Light                                                 X                  #### is
 *an opaque which does't let any light pass through it.
 *       |                                                          /
 *       | <-----------  visible distance to v: 3 -------------->  /
 *       |                                                        /
 *############o                                             o###########
 *       :    #                                             #   :
 *       : <------------ actual distance: 3 + 8 = 11 --------> :                        The
 *vertex 'v' should be in the shadow because it's visible distance
 *       :    #                                             # :                         (from
 *the Perspective of the light) is smaller that its actual distance!
 *       :    #                                             #:
 *       :    #                                             :
 *       :    #                                            :#
 *       :    #                                           : #
 *       v <-------------------- vertex to test ----->   v  #
 *
 */
