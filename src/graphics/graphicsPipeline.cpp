/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "graphicsPipeline.h"
#include <irrlicht/ISceneManager.h>
#include <irrlicht/IrrlichtDevice.h>
#include "particles/particleSystemGPU.h"

#include "Views.h"
#include "animated/animatedRenderStage.h"
#include "debugdraw/debugDrawRenderStage.h"
#include "decals/DecalManager.h"
#include "lighting/lightingStage.h"
#include "particles/particleRenderStage.h"
#include "statics/staticsRenderStage.h"
#include "../utils/debug.h"

GraphicsPipeline::GraphicsPipeline(irr::IrrlichtDevice* const t_device, irr::scene::ISceneManager* const t_smgr)
    : device(t_device)
    , smgr(t_smgr)
{
    device->grab();
}

GraphicsPipeline::~GraphicsPipeline()
{
    device->drop();
}

void GraphicsPipeline::init()
{
    if (!initialized)
    {
        screenSize = device->getVideoDriver()->getScreenSize();
        createScreenRT(screenSize);

        renderStages[size_t(RenderStageType::STATIC)] =
            std::unique_ptr<IRenderStage>(new StaticsRenderStage());
        renderStages[size_t(RenderStageType::ANIMATED)] =
            std::unique_ptr<IRenderStage>(new AnimatedRenderStage());
        renderStages[size_t(RenderStageType::LIGHTING)] = std::unique_ptr<IRenderStage>(new LightingStage());
        renderStages[size_t(RenderStageType::PARTICLE)] =
            std::unique_ptr<IRenderStage>(new ParticleRenderStage());
        renderStages[size_t(RenderStageType::DECAL)] = std::unique_ptr<IRenderStage>(new DecalRenderStage());

        renderStages[size_t(RenderStageType::DEBUG_STAGE)] =
            std::unique_ptr<IRenderStage>(new DebugDrawRenderStage());

        renderStages[size_t(RenderStageType::STATIC)]->setClearFlagForOutput(irr::video::ECBF_COLOR |
                                                                             irr::video::ECBF_DEPTH);
        // renderStages[size_t(RenderStageType::ANIMATED)]->setClearFlagForOutput(irr::video::ECBF_COLOR
        // | 	irr::video::ECBF_DEPTH);


        preRenderStage.initialize(device, smgr);

        int i = 0;
        for (auto& stage : renderStages)
        {
            debugOutLevel(0, "Running init for stage with Number: ", i++);
            stage->initialize(device, smgr);
        }
    }

    initialized = true;
}

void GraphicsPipeline::update() {}

void GraphicsPipeline::render()
{
    if (initialized)
    {
        auto driver = device->getVideoDriver();
        preRenderStage.render();
        bool firstStage = true;
        auto runRS = [&](const RenderStageType& rsType,
                         irr::video::IRenderTarget* input,
                         irr::video::IRenderTarget* outputTarget) {
            if (renderStageEnabled[size_t(rsType)])
            {
                if (firstStage)
                {
                    // Depending on which renderstages are active the "first" might change.
                    if (firstRenderStage != rsType)
                    {
                        // Reset the clear flag in the renderstage which was previously first
                        if (firstRenderStage != RenderStageType::COUNT && firstRenderStage != rsType)
                        {
                            renderStages[size_t(firstRenderStage)]->setClearFlagForOutput(0);
                        }

                        // Set the clear flag for this stage
                        renderStages[size_t(rsType)]->setClearFlagForOutput(irr::video::ECBF_COLOR |
                                                                            irr::video::ECBF_DEPTH);
                        firstRenderStage = rsType;
                    }
                    firstStage = false;
                }
                renderStages[size_t(rsType)]->render(input, outputTarget);
            }
        };

        runRS(RenderStageType::STATIC, nullptr, screenRT);
        runRS(RenderStageType::ANIMATED, nullptr, screenRT);
        runRS(RenderStageType::LIGHTING, screenRT, nullptr);
        driver->setActiveView(ViewNumbers::PARTICLE_VIEW);
        runRS(RenderStageType::PARTICLE, nullptr, nullptr);
        /// TODO: check view set up - need DECAL_VIEW??
        runRS(RenderStageType::DECAL, nullptr, nullptr);
        driver->setActiveView(ViewNumbers::SIMPLE_3D_VIEW);
        runRS(RenderStageType::DEBUG_STAGE, nullptr, nullptr);
        driver->setActiveView(ViewNumbers::DEFAULT_3D_VIEW);
    }
}

void GraphicsPipeline::clear()
{
    preRenderStage.clearNodes();

    for (auto& stages : renderStages)
    {
        stages->clearNodes();
    }
}

void GraphicsPipeline::removeNodeFromStage(irr::scene::ISceneNode* t_node, RenderStageType type)
{
    renderStages[size_t(type)]->removeNode(t_node);

    if (type != RenderStageType::LIGHTING)
    {
        preRenderStage.removeNode(t_node);
    }
}

void GraphicsPipeline::enableRenderStage(RenderStageType type)
{
    renderStageEnabled[size_t(type)] = true;
}

void GraphicsPipeline::disableRenderStage(RenderStageType type)
{
    renderStageEnabled[size_t(type)] = false;
}

std::shared_ptr<ParticleSystemGPU> GraphicsPipeline::getParticleSystemGPU()
{
    return dynamic_cast<ParticleRenderStage*>(renderStages[size_t(RenderStageType::PARTICLE)].get())->getParticleSystemGPU();
}

void GraphicsPipeline::setScreenSize(const irr::core::dimension2du& t_screenSize)
{
    if (t_screenSize != screenSize)
    {
        screenSize = t_screenSize;

        createScreenRT(screenSize);

        for (auto& stage : renderStages)
        {
            stage->setScreenResolution(screenSize);
        }
    }
}


DirectionalLight& GraphicsPipeline::getLight()
{
    return dynamic_cast<LightingStage*>(renderStages[size_t(RenderStageType::LIGHTING)].get())->getLight();
}

LightingStage& GraphicsPipeline::getLighting()
{
    return *(dynamic_cast<LightingStage*>(renderStages[size_t(RenderStageType::LIGHTING)].get()));
}

void GraphicsPipeline::createScreenRT(const irr::core::dimension2du& screenDim)
{
    auto driver = device->getVideoDriver();

    if (screenRT)
    {
        driver->removeTexture(screenRT->getTexture()[0]);
        driver->removeTexture(screenRT->getDepthStencil());
        driver->removeRenderTarget(screenRT);
        screenRT = nullptr;
    }

    screenRT = driver->addRenderTarget();

    auto screenRTT =
        driver->addRenderTargetTexture(screenDim, "Color_MainPipe", irr::video::ECOLOR_FORMAT::ECF_A8R8G8B8);
    auto depthRTT =
        driver->addRenderTargetTexture(screenDim, "Depth_MainPipe", irr::video::ECOLOR_FORMAT::ECF_D24S8); /// TODO: is there a case where we should use a 16 bit depth buffer ?

    screenRT->setTexture(screenRTT, depthRTT);
}

void GraphicsPipeline::addNodeToStage(irr::scene::ISceneNode* t_node, RenderStageType type)
{
    if (type != RenderStageType::LIGHTING)
    {
        preRenderStage.addNode(t_node);
    }

    renderStages[size_t(type)]->addNode(t_node);
}
std::shared_ptr<DecalManager> GraphicsPipeline::getDecalManager()
{
    return dynamic_cast<DecalRenderStage*>(renderStages[size_t(RenderStageType::DECAL)].get())->getDecalManager();
}
