/*based on MeshCombiner by Lonesome Ducky:
http://irrlicht.sourceforge.net/forum/viewtopic.php?t=39598
Published under the Irrlicht license.
For more Information see licenses/irrlicht.license.txt
*/

#ifndef MESHCOMBINER_H
#define MESHCOMBINER_H

#include <stdlib.h>

#include <irrlicht/irrArray.h>
#include <irrlicht/irrString.h>
#include <irrlicht/rect.h>

// forward declarations
namespace irr
{
    namespace video
    {
        class IImage;
        class IVideoDriver;
        class ITexture;
    } // namespace video
    namespace scene
    {
        class IMesh;
        class ISceneManager;
        class ISceneNode;
        class IAnimatedMeshSceneNode;
        class IMeshSceneNode;
    } // namespace scene
} // namespace irr


// Used to pack textures
class RectPacker
{
   public:
    struct SPacked
    {
        SPacked()
            : pos()
            , id(0)
        {
        }
        irr::core::rect<irr::u32> pos;
        int id;
    };
    struct SRect
    {
        SRect()
            : pos()
            , packed(false)
            , id(0)
        {
        }
        irr::core::rect<irr::u32> pos;
        int children[2];
        bool packed;
        int id;
        bool operator<(const SRect& other) const { return pos.getArea() > other.pos.getArea(); }
    };
    RectPacker()
        : mPackSize(0)
        , mNumPacked(0)
        , mRects()
        , mPacks()
        , mRoots()
    {
    }
    void pack(const irr::core::array<irr::core::rect<irr::u32>>& rects,
              irr::core::array<irr::core::array<SPacked>>& packed,
              irr::u32 sizeOfPack);
    void clear();
    void fill(int pack);
    void split(int pack, int rect);
    bool fits(SRect rect1, SRect rect2);
    void addPackToArray(int pack, irr::core::array<SPacked>& addArray) const;

    bool isRectValid(int i) const;
    bool isPackValid(int i) const;

    unsigned int mPackSize;
    unsigned int mNumPacked;
    irr::core::array<SRect> mRects;
    irr::core::array<SRect> mPacks;
    irr::core::array<int> mRoots;
};

enum TEXTURE_PADDING_TECHNIQUE
{
    ETPT_EXPAND = 0,
    ETPT_TILE = 1
};

class MeshCombiner
{
    irr::f32 mSizingTolerance;
    TEXTURE_PADDING_TECHNIQUE mTexturePaddingTechnique;

   public:
    // MeshCombiner();
    explicit MeshCombiner(irr::f32 sizingTolerance = 0.8f, TEXTURE_PADDING_TECHNIQUE technique = ETPT_EXPAND);
    irr::core::dimension2du findOptimalPackingArea(irr::core::array<irr::core::rect<irr::u32>> rectangles);

    irr::core::array<irr::u32> getUniqueTextureIndices(irr::core::array<irr::video::ITexture*> textures) const;
    /// @brief Pack textures into a larger one and return it. outPositions returns the new positions
    /// of the textures in the order they appeared in the array
    /// @param driver
    /// @param textures the textures to be packed together
    /// @param outPositions
    /// @return the new combined texture. nullptr if 'textures' was empty
    irr::video::ITexture* packTextures(irr::video::IVideoDriver* driver,
                                       irr::core::array<irr::video::ITexture*> textures,
                                       irr::core::array<irr::core::position2di>& outPositions);
    irr::scene::IMesh* combineMeshes(irr::scene::ISceneManager* smgr,
                                     irr::core::array<irr::scene::ISceneNode*> nodes,
                                     irr::core::array<irr::scene::IMesh*> meshes,
                                     const irr::core::stringc& meshNameInCache,
                                     bool clearNodesAfterUse = true,
                                     bool useHardwareMappingHint = true);
    irr::scene::IMesh* combineMeshes(irr::scene::ISceneManager* smgr,
                                     irr::core::array<irr::scene::IAnimatedMeshSceneNode*> nodes,
                                     const irr::core::array<irr::s32> frames,
                                     const irr::core::stringc& meshNameInCache,
                                     bool clearNodesAfterUse = true,
                                     bool useHardwareMappingHint = true);
    irr::scene::IMesh* combineMeshes(irr::scene::ISceneManager* smgr,
                                     irr::core::array<irr::scene::IMeshSceneNode*> nodes,
                                     const irr::core::stringc& meshNameInCache,
                                     bool clearNodesAfterUse = true,
                                     bool useHardwareMappingHint = true);

    void setSizingTolerance(irr::f32 tolerance);
    void setTexturePaddingTechnique(TEXTURE_PADDING_TECHNIQUE technique);

    irr::f32 getSizingTolerance();
    TEXTURE_PADDING_TECHNIQUE getTexturePaddingTechnique();
};

#endif
