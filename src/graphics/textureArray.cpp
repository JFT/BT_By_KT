#include "textureArray.h"
#include <irrlicht/IVideoDriver.h>
#include <irrlicht/irrArray.h>
#include "../utils/stringwstream.h"
#include "../utils/debug.h"
#include "../utils/static/error.h"
#include "../utils/static/repr.h"

irr::video::ITexture* Graphics::addTextureArray(irr::video::IVideoDriver* const videoDriver,
                                                std::vector<irr::core::stringw> imageFilenames)
{
    debugOut("createTextureArray with size ", imageFilenames.size());
    irr::core::array<irr::video::IImage*> imageArray;
    for (size_t i = 0; i < imageFilenames.size(); ++i)
    {
        irr::video::IImage* image = videoDriver->createImageFromFile(imageFilenames[i]);
        if (image == nullptr)
        {
            Error::errContinue("couldn't load texture", repr<irr::core::stringw>(imageFilenames[i]));
            continue;
        }
        imageArray.push_back(image);
    }
    // TODO: when its finally working make sure to drop all loaded images after creating the
    // textureArray

    debugOutLevel(Debug::DebugLevels::stateInit + 1,
                  "creating TexArray:\n Size of imageArray:\t",
                  imageArray.size());

    return videoDriver->addTextureArray("textureArray", imageArray);
}
