/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once


#include <memory>
#include "../IRenderStage.h"

class DecalManager;

class DecalRenderStage : public IRenderStage
{
   public:
    DecalRenderStage();
    virtual ~DecalRenderStage();

    virtual void initialize(irr::IrrlichtDevice* const t_device, irr::scene::ISceneManager* const t_smgr);

    virtual void render(irr::video::IRenderTarget* input = nullptr,
                        irr::video::IRenderTarget* outputTarget = nullptr);

    std::shared_ptr<DecalManager> getDecalManager();


   private:
    std::shared_ptr<DecalManager> decalManager;
};
