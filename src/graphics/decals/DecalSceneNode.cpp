/*
  Copyright (C) 2011 Thijs Ferket (RdR)

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "DecalSceneNode.h"

#include <time.h>

#include <irrlicht/ICameraSceneNode.h>
#include <irrlicht/IMesh.h>
#include <irrlicht/IMeshManipulator.h>
#include <irrlicht/ISceneManager.h>
#include <irrlicht/IVideoDriver.h>

using namespace irr;
using namespace scene;

DecalSceneNode::DecalSceneNode(scene::ISceneNode* parent,
                               scene::ISceneManager* smgr,
                               scene::IMesh* mesh_,
                               video::ITexture* texture_,
                               const core::vector3df position)
    : scene::ISceneNode(
          parent, smgr, -1, position, core::vector3df(0, 0, 0), (core::vector3df(1, 1, 1) / parent->getScale()))
    , mesh(mesh_)
    , texture(texture_)
    , box(mesh_->getBoundingBox())
    , material()
    , distance(0)
    , lifetime(0)
    , creationTime(0)
    , fadeOut(true)
    , fadeOutTime(3)
    , startFadeOutTimeMs(0)
{
    time(&creationTime);

    // Material
    material.ZWriteEnable = false;
    material.Wireframe = false;
    material.Lighting = false;
    material.MaterialType = video::EMT_TRANSPARENT_ALPHA_CHANNEL;

    material.MaterialTypeParam = pack_textureBlendFunc(video::EBF_SRC_ALPHA,
                                                       video::EBF_ONE_MINUS_SRC_ALPHA,
                                                       video::EMFN_MODULATE_1X,
                                                       video::EAS_TEXTURE | video::EAS_VERTEX_COLOR);

    material.TextureLayer[0].TextureWrapU = video::ETC_CLAMP_TO_BORDER;
    material.TextureLayer[0].TextureWrapV = video::ETC_CLAMP_TO_BORDER;

    material.setTexture(0, texture);

    setAutomaticCulling(scene::EAC_FRUSTUM_BOX);
}

DecalSceneNode::~DecalSceneNode()
{
    if (mesh)
    {
        mesh->drop();
    }
}

void DecalSceneNode::OnRegisterSceneNode()
{
    if (lifetime > 0 && (difftime(time(nullptr), creationTime) > lifetime))
    {
        // If fading out enabled
        if (fadeOut)
        {
            // Delete if decal is fade out
            if (difftime(time(nullptr), creationTime) > lifetime + fadeOutTime)
            {
                SceneManager->addToDeletionQueue(this);
            }
        }
        else
        {
            SceneManager->addToDeletionQueue(this);
        }
    }
    if (IsVisible)
    {
        SceneManager->registerNodeForRendering(this);
    }
    ISceneNode::OnRegisterSceneNode();
}

void DecalSceneNode::render()
{
    video::IVideoDriver* driver = SceneManager->getVideoDriver();
    scene::ICameraSceneNode* camera = SceneManager->getActiveCamera();

    if (!camera || !driver || !IsVisible)
    {
        return;
    }

    // Check if node is out of range
    if (distance > 0)
    {
        f32 d = camera->getAbsolutePosition().getDistanceFrom(getAbsolutePosition());
        if (d > distance)
        {
            return;
        }
    }

    // Draw mesh
    driver->setTransform(video::ETS_WORLD, AbsoluteTransformation);
#if defined _DEBUG
    if (!material.Wireframe)
    {
        material.setTexture(0, texture);
    }
#endif

    driver->setMaterial(material);
    driver->drawMeshBuffer(mesh->getMeshBuffer(0));

#if defined _DEBUG
    // Debug only
    if (SHOW_DEBUG_BOX)
    {
        material.setTexture(0, nullptr);
        driver->setMaterial(material);

        driver->draw3DBox(box, video::SColor(255, 255, 0, 0));
    }
#endif
}

void DecalSceneNode::OnAnimate(u32 timeMs)
{
    if (fadeOut && lifetime > 0)
    {
        // Lifetime done, start fading out
        if (difftime(time(nullptr), creationTime) > lifetime)
        {
            if (startFadeOutTimeMs > 0)
            {
                const u32 timeDiff = timeMs - startFadeOutTimeMs;

                const s32 alpha = 255 - s32(((float(timeDiff) * 255.f) / (fadeOutTime * 1000.f)));

                SceneManager->getMeshManipulator()->setVertexColorAlpha(mesh, alpha <= 0 ? 0 : alpha);
            }
            else
            {
                material.MaterialType = video::EMT_ONETEXTURE_BLEND;
                startFadeOutTimeMs = timeMs;
            }
        }
    }
    ISceneNode::OnAnimate(timeMs);
}

const core::aabbox3d<f32>& DecalSceneNode::getBoundingBox() const
{
    return box;
}

scene::ESCENE_NODE_TYPE DecalSceneNode::getType() const
{
    return scene::ESCENE_NODE_TYPE(DECAL_SCENE_NODE_ID);
}

u32 DecalSceneNode::getMaterialCount() const
{
    return 1;
}

video::SMaterial& DecalSceneNode::getMaterial(u32)
{
    return material;
}

scene::IMesh* DecalSceneNode::getMesh()
{
    return mesh;
}

video::ITexture* DecalSceneNode::getTexture()
{
    return texture;
}

f32 DecalSceneNode::getLifetime() const
{
    return lifetime;
}

void DecalSceneNode::setLifetime(const f32 lifeT)
{
    this->lifetime = lifeT;
}

f32 DecalSceneNode::getDistance() const
{
    return distance;
}

void DecalSceneNode::setDistance(const f32 dist)
{
    this->distance = dist;
}

void DecalSceneNode::setFadeOut(const bool fadeOut_, const f32 time)
{
    this->fadeOut = fadeOut_;
    if (time > 0)
    {
        this->fadeOutTime = time;
    }
}
