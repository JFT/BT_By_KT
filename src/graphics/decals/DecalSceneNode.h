/*
Copyright (C) 2011 Thijs Ferket (RdR)

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef DECAL_SCENE_NODE_H_
#define DECAL_SCENE_NODE_H_

#include <irrlicht/ISceneNode.h>
#include <irrlicht/SMaterial.h>

// forward declarations
namespace irr
{
    namespace scene
    {
        class IMesh;
        class IMeshSceneNode;
        class ITerrainSceneNode;
        class IAnimatedMeshSceneNode;
        class IMetaTriangleSelector;
    } // namespace scene
    namespace video
    {
        class ITexture;
        class IVideoDriver;
        class IMetaTriangleSelector;
    } // namespace video
} // namespace irr


namespace irr
{
    namespace scene
    {
#ifdef MAKE_IRR_ID
        const int DECAL_SCENE_NODE_ID = MAKE_IRR_ID('d', 'e', 'c', 'l');
#else
        const int DECAL_SCENE_NODE_ID = 'decl';
#endif // MAKE_IRR_ID
#if defined _DEBUG
#define SHOW_DEBUG_BOX false
#else
#define SHOW_DEBUG_BOX false
#endif

        class DecalSceneNode : public scene::ISceneNode
        {
           private:
            scene::IMesh* mesh;
            video::ITexture* texture;
            core::aabbox3df box;
            video::SMaterial material;
            f32 distance;
            f32 lifetime;
            time_t creationTime;
            bool fadeOut;
            f32 fadeOutTime;
            u32 startFadeOutTimeMs;

           public:
            /**
             * Constructor
             * @param parent
             * @param smgr
             * @param mesh
             * @param texture
             * @param position
             */
            DecalSceneNode(scene::ISceneNode* parent,
                           scene::ISceneManager* smgr,
                           scene::IMesh* mesh_,
                           video::ITexture* texture_,
                           const core::vector3df position = core::vector3df(0, 0, 0));
            DecalSceneNode(const DecalSceneNode&) = delete;
            DecalSceneNode operator=(const DecalSceneNode&) = delete;
            /**
             * Destructor
             */
            virtual ~DecalSceneNode();

            /**
             * On register scene node
             */
            virtual void OnRegisterSceneNode();

            /**
             * Render
             */
            virtual void render();

            /**
             * On Animate
             * @param timeMs
             */
            virtual void OnAnimate(u32 timeMs);

            /**
             * Get AA bounding box
             */
            virtual const core::aabbox3d<f32>& getBoundingBox() const;

            /**
             * Get type
             * @return type
             */
            virtual scene::ESCENE_NODE_TYPE getType() const;

            /**
             * Get material count
             * @return 1
             */
            virtual u32 getMaterialCount() const;

            /**
             * Get material
             * @param i
             * @return material
             */
            virtual video::SMaterial& getMaterial(u32 i);

            /**
             * Get decal mesh
             * @return mesh
             */
            scene::IMesh* getMesh();

            /**
             * Get decal texture
             * @return texture
             */
            video::ITexture* getTexture();

            /**
             * Get life time in seconds
             * @return lifeTime
             */
            f32 getLifetime() const;

            /**
             * Set life time in seconds
             * @param lifeTime
             */
            void setLifetime(const f32 lifeT);

            /**
             * Get max viewing distance
             * @return distance
             */
            f32 getDistance() const;

            /**
             * Set max viewing distance
             * @param distance
             */
            void setDistance(const f32 dist);

            /**
             * Set fade out
             * @param fadeOut
             * @param time
             */
            void setFadeOut(const bool fadeOut_, const f32 time = 1);
        };

    } // end namespace scene

} // end namespace irr

#endif /* DECAL_SCENE_NODE_H_ */
