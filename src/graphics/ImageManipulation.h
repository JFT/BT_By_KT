/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2015 - 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IMAGE_MANIPULATION_H
#define IMAGE_MANIPULATION_H

// forward declarations
namespace irr
{
    namespace video
    {
        class IImage;
        class IVideoDriver;
        class ITexture;
    } // namespace video
} // namespace irr


namespace ImageManipulation
{
    /// @brief rotate an image clockwise by 90 degree
    /// @param img the image to rotate. This function will call img->drop() after rotating.
    /// @param videoDriver needed to reserve space for the rotated image
    /// @return img rotated 90 degrees clockwise. If you no longer need the image, you should call
    /// IImage::drop(). See IReferenceCounted::drop() for more information. If either img or
    /// videoDriver are nullptr rotateClockwise will return nullptr.
    irr::video::IImage* rotateClockwise(irr::video::IImage* img, irr::video::IVideoDriver* videoDriver);

    /// @brief convert a texture to an image
    /// @param texture texture to turn into an image.
    /// @param videoDriver needed for conversion.
    /// @return the IImage created from texture. If you no longer need the image, you should call
    /// IImage::drop(). See IReferenceCounted::drop() for more information. If either texture or
    /// videoDriver are nullptr TextureToImage will return nullptr.
    irr::video::IImage* TextureToImage(irr::video::ITexture* const texture, irr::video::IVideoDriver* videoDriver);
} // namespace ImageManipulation

#endif /* ifndef IMAGE_MANIPULATION_H */
