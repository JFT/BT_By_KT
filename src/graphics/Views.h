/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

/// Different View Numbers (a negative value x corresponds to view: 255 - x)

struct ViewNumbers
{
    static constexpr int CEGUI_VIEW = -1;
    static constexpr int DEFAULT_3D_VIEW = -11;
    static constexpr int DEFAULT_2D_VIEW = -10;
    static constexpr int SIMPLE_3D_VIEW = -3; // used for 3D Triangles, lines, boxes in the 3d scene
    static constexpr int SIMPLE_2D_VIEW = -2; // 2D drawings

    static constexpr int SHADOW_COMBINATION_VIEW = -9;
    static constexpr int PARTICLE_VIEW = -8;
    static constexpr int DECAL_VIEW = -7;
    static constexpr int POSTPROCESS_VIEW_1 = -6;
};