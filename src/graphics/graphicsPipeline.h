/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>
#include <vector>

#include "IRenderStage.h"
#include "decals/decalRenderStage.h"
#include "lighting/lighting.h"
#include "lighting/lightingStage.h"
#include "preRenderStage.h"

class ParticleSystemGPU;
class DecalManager;

// TODO: maybe rename the enum members to be not all capital letters
enum struct RenderStageType : size_t
{
    STATIC,
    ANIMATED,
    LIGHTING,
    PARTICLE,
    DECAL,
    DEBUG_STAGE, // TODO: remove debug draw in release

    COUNT
};

class GraphicsPipeline
{
   public:
    GraphicsPipeline(irr::IrrlichtDevice* const t_device, irr::scene::ISceneManager* const t_smgr);

    ~GraphicsPipeline();

    void init();

    void update();

    void render();

    void clear();

    void addNodeToStage(irr::scene::ISceneNode* t_node, RenderStageType type);

    void removeNodeFromStage(irr::scene::ISceneNode* t_node, RenderStageType type);

    void setScreenSize(const irr::core::dimension2du& t_screenSize);

    void disableRenderStage(RenderStageType type);

    void enableRenderStage(RenderStageType type);

    // Particles
    std::shared_ptr<ParticleSystemGPU> getParticleSystemGPU();


    // Lighting
    DirectionalLight& getLight();

    LightingStage& getLighting();

    // Decals
    std::shared_ptr<DecalManager> getDecalManager();

   private:
    void createScreenRT(const irr::core::dimension2du& screenDim);

    irr::IrrlichtDevice* device;
    irr::scene::ISceneManager* smgr;

    irr::video::IRenderTarget* screenRT = nullptr;

    PreRenderStage preRenderStage;

    std::vector<std::unique_ptr<IRenderStage>> renderStages =
        std::vector<std::unique_ptr<IRenderStage>>(size_t(RenderStageType::COUNT));

    std::vector<bool> renderStageEnabled = std::vector<bool>(size_t(RenderStageType::COUNT), true);

    RenderStageType firstRenderStage = RenderStageType::COUNT;

    irr::core::dimension2du screenSize = irr::core::dimension2du();

    bool initialized = false;
};
