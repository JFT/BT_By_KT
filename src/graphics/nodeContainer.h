/*
BattleTanks standalone client. A C++ implementation of BattleTanks
Copyright (C) 2017 Julius Tilly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stack>
#include <unordered_map>
#include <vector>

#include "../utils/debug.h"
template <typename T>
struct NodeContainer
{
    NodeContainer(const size_t t_initialSize)
        : nodes(std::vector<T>(t_initialSize))
        , nodesLookUp(std::unordered_map<T, size_t>())
        , freeIndices(std::stack<size_t>())
        , initialSize(t_initialSize)
    {
        for (int i = (t_initialSize - 1); i >= 0; i--)
        {
            freeIndices.push(i);
        }
    }

    void insert(T elem)
    {
        if (nodesLookUp.find(elem) == nodesLookUp.end())
        {
            size_t idx = freeIndices.top();
            nodes[idx] = elem;
            nodesLookUp[elem] = idx;

            if (highestIndex < idx)
            {
                highestIndex = idx;
            }
            freeIndices.pop();
            if (freeIndices.empty())
            {
                // vector full -> resizing
                // add new free indices to queue
                size_t animatedNodeCount = nodes.size();

                nodes.resize(animatedNodeCount + 100);

                for (size_t i = animatedNodeCount; i < animatedNodeCount + 100; ++i)
                {
                    freeIndices.push(i);
                }
            }
        }
    }

    void remove(T elem)
    {
        if (nodesLookUp.find(elem) == nodesLookUp.end())
        {
            // does not exist
            debugOutLevel(Debug::DebugLevels::firstOrderLoop,
                          "Tried to remove an animated node that was not added before!");
            return;
        }

        size_t idx = nodesLookUp[elem];

        nodes[idx] = nullptr;
        freeIndices.push(idx);

        if (highestIndex == idx)
        {
            highestIndex--;
        }
    }

    void clear()
    {
        nodes.clear();
        nodesLookUp.clear();
        highestIndex = 0;

        nodes.resize(initialSize);
    }

    T& operator[](size_t idx) { return nodes[idx]; }

    size_t size() { return highestIndex + 1; }

    std::vector<T> nodes;
    std::unordered_map<T, size_t> nodesLookUp;

    std::stack<size_t> freeIndices;

    size_t highestIndex = 0;

    size_t initialSize = 0;
    // TODO: add defragmentation routine
};