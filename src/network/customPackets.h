/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CUSTOM_PACKETS_H
#define CUSTOM_PACKETS_H

#include <raknet/MessageIdentifiers.h>
#include <raknet/RakNetTime.h>
#include <raknet/RakNetTypes.h> // GUID and SystemAddress for the serverInfos

// define custum packet IDs here
// ----------------------------------------------------------------------------
/// @brief For sending/receiving packets in processPacket() loop we can
/// define id's of our custom packets here.
//
/// @note We firstly used this method for chat messages, but now
/// we changed to a chat replica to proceed from writing handlers to writing
/// a more object oriented nework code.
// ----------------------------------------------------------------------------
enum CustomID
{
    // IDs for ServerBrowser
    ID_SERVER_ = DefaultMessageIDTypes::ID_USER_PACKET_ENUM,
    /// @brief used to signal that static replicas are created and ready to serialize/deserialize
    /// (some static replicas are created on state change but RakNet assumes static replicas exist
    /// -before- the connection is made -> this packet ensures data sending starts only when both
    /// sides have the created the replica)
    ID_STATIC_REPLICAS_CREATED,
    /// @brief if send by server: COMMAND clients (they MUST obey) to start/pause/resume the game.
    /// If send by clients: request the server to start/pause/resume the game (server must't obey
    /// and will answer with an ID_START_PAUSE_OR_RESUME if it does
    /// Data MUST be a 'PauseRequest' struct!
    ID_START_PAUSE_OR_RESUME_GAME
};


#pragma pack(push) // pragma 'pack' used to byte-align the struct in Visual Studio
struct PauseRequest
{
    // this custom packet still needs the first byte to be the packetID (see RakNet::RakPeer::Send()
    // documentation)
    char packetID = static_cast<char>(CustomID::ID_START_PAUSE_OR_RESUME_GAME);
    enum RequestType
    {
        Paused,
        Running
    };
    /// @brief should the game be running or be paused
    RequestType type = RequestType::Paused;
    /// @brief target tick the game should be paused on (0 = pause immediately). Clients must ensure
    /// they pause on that exact tick and wait for the server to signal to continue.
    uint32_t requestTick = 0;
    /// @brief server requested to wait this many ms before resuming/starting the game (remember to
    /// calculate actual wait time with ping!)
    RakNet::TimeMS waitTime = 0;
};
#pragma pack(pop)


#pragma pack(push) // pragma 'pack' used to byte-align the struct in Visual Studio
// ----------------------------------------------------------------------------
/// @brief Packet which will be sent when server advertises.
///
/// The client stores this entries in serverbrowser state to list available
/// Servers.
// ----------------------------------------------------------------------------
struct ServerInfos
{
    static constexpr size_t serverMaxNameLength = 50;
    char name[serverMaxNameLength]; // must be constant size (also across different compile
                                    // platforms!) because it is send in fixed size data packet of
                                    // server advertisement
    uint32_t filledSlots = 0;
    short unsigned int maxSlots = 10;
    RakNet::SystemAddress address = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
    RakNet::RakNetGUID guid = RakNet::UNASSIGNED_RAKNET_GUID;
    bool passwordProtected = false;
};
#pragma pack(pop)

#endif // CUSTOM_PACKETS_H
