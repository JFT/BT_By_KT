/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NETWORK_H
#define NETWORK_H

#include <vector> // used by CircularBuffer

#include "../utils/static/error.h" // defines ErrCode

#if LIBCAT_SECURITY == 1
#include <raknet/SecureHandshake.h> // Include header for secure handshake
#endif

#include "customPackets.h" // includes ServerInfos and PauseRequest structs

// forward declarations
class ReplicaManager3BT;
namespace RakNet
{
    class RakPeerInterface;
    class NetworkIDManager;
    class RakNetStatistics;
} // namespace RakNet


// ----------------------------------------------------------------------------
/// @brief Container to buffer data from the last n ticks
///
/// Stores the last n datapoints so the simulation can be recalculated if needed
/// from past datapoints. This is necessary to handle commands that
/// arrive a little late.
// ----------------------------------------------------------------------------
template <typename T>
struct CircularBuffer
{
    explicit CircularBuffer(uint32_t size_)
        : data(size_)
        , size(size_)
    {
    }

    void setData(const T& dataPoint, uint32_t tickNum)
    {
        if (latestTick < tickNum)
        {
            latestTick = tickNum;
        }
        data[tickNum % size] = dataPoint;
    }

    T& getData(const uint32_t tickNum) { return data[tickNum % size]; }
    T& operator()(const uint32_t tickNum) { return data[tickNum % size]; }
    std::vector<T> data;
    uint32_t size;
    uint32_t latestTick = 0;
};

// ----------------------------------------------------------------------------
/// @brief Base class for network
///
/// The basic initilializing of the network is outsourced from the server
/// and client network classes just to keep their code cleaner.
///
// ----------------------------------------------------------------------------
class Game;

// ----------------------------------------------------------------------------
class Network
{
   public:
    explicit Network(Game* const);
    Network(const Network&) = delete;
    Network& operator=(const Network&) = delete;
    virtual ~Network();

    virtual bool init(); // init defaults for clients and the server

    virtual void setRunning(bool running_) { this->running = running_; };
    virtual bool isRunning() const { return this->running; }
    // getter and setter
    inline RakNet::RakPeerInterface* getPeer() const { return peer; }
    virtual bool isInitialized();
    virtual bool isConnected(RakNet::SystemAddress systemAdress) const;
    virtual ReplicaManager3BT* getReplicaManager() const { return this->replicaManager; }
    void setCircularBufferSize(uint32_t newSize);
    uint32_t getCircularBufferSize() { return this->bufferSize; }
    /// \brief set ReUpdate from tick
    ///
    /// \param flag boolean to set reUpdateNecessary
    /// \param startTick ticknumber to start reupdating from
    void setReUpdateNecessary(bool flag, uint32_t startTick);

    /// \brief get the tick from which the reUpdate should start
    ///
    /// \return reUpdateStartTick uint32_t
    inline uint32_t getReUpdateStartTick() const { return this->startReUpdateTick; }
    inline bool isReUpdateNecessary() const { return this->reUpdateNecessary; }
    //! NOT WORKING - causes SEGFAULTS in Deserialize: read value from bitstream
    // void callSerializeOnReplicas();

    /// @brief parse the requested running state out of a packet send by the server or the client.
    /// (packet must have ID_START_OR_RESUME_GAME or ID_PAUSE_GAME)
    /// @param packet the packet requesting the start/resume/pause
    /// @param pauseRequest the pauseRequest will be parsed into this packet
    /// @return ErrCode indicating if parsing worked or failed (e.g. wrong ID, packet too
    /// short/long, nonsensical data). If parsing failed don't use the data in 'pauseRequest'
    ErrCode parsePauseRequest(RakNet::Packet* const packet, PauseRequest& pauseRequest) const;

    unsigned char GetPacketIdentifier(const RakNet::Packet* packet) const;

    std::string packetIdentifierToString(const unsigned char packetIdentifier_) const;

   protected:
    RakNet::RakPeerInterface* peer = nullptr;
    RakNet::SocketDescriptor* socketDescriptor = nullptr;
    RakNet::RakNetStatistics* rss = nullptr;
    RakNet::NetworkIDManager* networkIDManager = nullptr;


    ReplicaManager3BT* replicaManager = nullptr;

    Game* const game;

    bool running = true;
    bool initialized = false;

    // Size of circular-state buffer - number of steps we can look into the "past"
    uint32_t bufferSize = 4;

    // ReUpdate
    bool reUpdateNecessary = false;
    uint32_t startReUpdateTick = 0;
};

#endif /* NETWORK_H */
