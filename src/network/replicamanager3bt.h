/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REPLICAMANAGER3BT_H
#define REPLICAMANAGER3BT_H

#include <functional>
#include <map>
#include <raknet/ReplicaManager3.h>
#include "allocationIDs.h"

class Game;
class ReplicaManager3BT : public RakNet::ReplicaManager3
{
    using CallbackType = std::function<RakNet::Replica3*()>;

   public:
    explicit ReplicaManager3BT(Game* const);
    ReplicaManager3BT(const ReplicaManager3BT& orig) = delete;
    virtual ~ReplicaManager3BT();

    ReplicaManager3BT operator=(const ReplicaManager3BT&) = delete;

    virtual RakNet::Connection_RM3*
    AllocConnection(const RakNet::SystemAddress& systemAddress, RakNet::RakNetGUID rakNetGUID) const;
    virtual void DeallocConnection(RakNet::Connection_RM3* connection) const;

    void registerAllocationCallback(const Networking::AllocationID allocationID, CallbackType callback);
    RakNet::Replica3* callAllocationCallback(const Networking::AllocationID allocationID) const;

   private:
    Game* const game;

    std::map<Networking::AllocationID, CallbackType> callbacks;
};

#endif /* REPLICAMANAGER3BT_H */
