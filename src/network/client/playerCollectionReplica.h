/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAYERCOLLECTION_H_8Z1XODDJ
#define PLAYERCOLLECTION_H_8Z1XODDJ

#include <irrlicht/irrString.h> // used by getPlayerLabel()
#include <raknet/ReplicaManager3.h>
#include <string>
#include <vector>

#include "../serializeVar.h"
#include "../slotInfos.h"
#include "../staticReplicaBase.hpp"

#include "../../utils/debug.h"

class ReplicaManager3BT;

// ----------------------------------------------------------------------------
/// @brief Class which orders the players to slots.
///
/// Clients can set the slots in the lobby state. This will
/// be serialized over the network.
/// The class should be constructed with the name of the player.
///
/// PlayerCollection is defined as a static replica. So it must be constructed
/// by the server and the clients manually.
// ----------------------------------------------------------------------------
class PlayerCollectionReplica : public BattleTanks::Networking::StaticReplicaBase
{
   public:
    PlayerCollectionReplica(ReplicaManager3BT* const replicaManager, RakNet::RakWString playerName_);
    ~PlayerCollectionReplica() override;

    // ----------------------------------------------------------------------------
    // Gui Flag manipulation
    // ----------------------------------------------------------------------------

    /// @brief sets the position we will ask the server to be moved to. Doesn't nothing if we ARE
    /// set to 'ready'
    /// @param requestPosition_ the slot the player wants to move to
    inline void setRequestPosition(unsigned int requestPosition_)
    {
        if (not this->readyToStartGame.value)
        {
            this->requestPosition.value = requestPosition_ - 1;
            this->requestPosition.serialize = true;
        }
    }

    /// @brief toggle our 'ready' state and send that to the server
    inline void changeReadyToStartGameFlag()
    {
        this->readyToStartGame.value ? this->readyToStartGame.value = false :
                                       this->readyToStartGame.value = true;
        this->readyToStartGame.serialize = true;
    }

    /// @brief set the 'ready' flag and serialize it to the server
    /// @param val new state of the flag
    inline void setReadyFlag(const bool val)
    {
        this->readyToStartGame.value = val;
        this->readyToStartGame.serialize = true;
    }

    // ----------------------------------------------------------------------------
    // Lobby specific functions
    // ----------------------------------------------------------------------------

    std::wstring getPlayerName(const size_t slot) const;

    bool isReady(const size_t slot) const;

    irr::core::stringw getPlayerLabel(unsigned int slot_);

    /// @brief check if all players that we know of are ready
    bool allPlayersReady() const;

    inline uint32_t mySlotID() const { return this->currentSlot; }

    inline uint32_t getPlayerID() const { return this->slots[mySlotID()].playerID; }

    std::vector<BattleTanks::Networking::SlotInfos> getSlots() { return this->slots; };

    // ----------------------------------------------------------------------------
    // Replica functions
    // ----------------------------------------------------------------------------

    // Construction
    void SerializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                       RakNet::Connection_RM3* destinationConnection) override;

    void DeserializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                         RakNet::Connection_RM3* sourceConnection) override;


    // Serialization
    RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* serializeParameters) override;

    void Deserialize(RakNet::DeserializeParameters* deserializeParameters) override;

    RakNet::RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3* destinationConnection) override;

   private:
    const RakNet::RakString m_replicaName = "PlayerCollection"; // Name to identify/reference the Replica object

    RakNet::RakWString m_playerName;

    // Helper for debug
    void printPlayerMap();

    // Map to store the player names assigned to the slot numbers.
    std::vector<BattleTanks::Networking::SlotInfos> slots;

    // ----[ Just Serializable variables ]---------------------------

    serializeVar<unsigned int> requestPosition = serializeVar<unsigned int>(0);
    serializeVar<bool> readyToStartGame = serializeVar<bool>(false);

    // ----[ Just Deserialize Variables ]---------------------------

    unsigned int currentSlot =
        static_cast<unsigned int>(-1); // slot of the current player. Will be assigned by the server in the lobby
};

#endif /* end of include guard: PLAYERCOLLECTION_H_8Z1XODDJ */
