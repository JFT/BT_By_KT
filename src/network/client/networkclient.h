/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CHATCLIENT_H
#define CHATCLIENT_H

#include "../network.h"

class Game;

// ----------------------------------------------------------------------------
class NetworkClient : public Network
{
   public:
    // Initialising
    explicit NetworkClient(Game* const game);
    NetworkClient(const NetworkClient&) = delete;
    NetworkClient operator=(const NetworkClient&) = delete;
    virtual ~NetworkClient();

    /// @brief hold guid and address of the server we are currently connected to.
    /// RakNet::UNASSIGNED_... if not connected.
    RakNet::RakNetGUID serverGUID = RakNet::UNASSIGNED_RAKNET_GUID;
    RakNet::SystemAddress serverAddress = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
    /// @brief ports < 1024 indicating errors because they are all reserved
    uint16_t clientPort = 0;

    /// @brief start up the network
    /// @return true if succesfull, false if errors encountered while starting up (e.g. couldn't
    /// reserve a port)
    bool init();

    // void consoleListener(); ??

    /// @brief disconnects from the currently connected server and resets this->serverAddress and
    /// -guid.
    /// If not connected only resets this->serverAddress and -guid to RakNet::UNASSIGNED_X
    void disconnectFromServer();

    /// @brief request the server to start/resume or pause the game
    /// @param type should the game be running or be paused.
    /// if RequestType::Running: request to start/resume the game.
    /// if RequestType::Paused: request to pause the game.
    /// Server will respond with a ID_START_PAUSE_OR_RESUME_GAME packet (plus attached struct
    /// containing commands for the client what to do) if the request was granted and simply ignore
    /// the packet if it wasn't
    ///
    /// does nothing if unconnected.
    void requestStartPauseOrResumeGame(const PauseRequest::RequestType type);

   private:
    Game* const game;
};

#endif // CHATCLIENT_H
