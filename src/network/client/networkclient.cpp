/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "networkclient.h"

// ----------------------------------------------------------------------------
#include <random> //TODO(mario): Added for network testing. It generates random clientPorts. Di 2016/02/16
#include <stdint.h> // Same here: This is used for seed.

#include <raknet/RakPeerInterface.h>

#include "../../core/game.h"

// ----------------------------------------------------------------------------
NetworkClient::NetworkClient(Game* const game_)
    : Network(game_)
    , game(game_)
{
}

// ----------------------------------------------------------------------------
NetworkClient::~NetworkClient() {}

// ----------------------------------------------------------------------------
bool NetworkClient::init()
{
    this->initialized = false;

    // Setup ReplicaManager3
    if (not Network::init())
    {
        Error::errContinue("couldn't initialize the network!");
        return false;
    }

    // try 10 times to get a free client port
    const uint32_t seed = uint32_t(std::chrono::system_clock::now().time_since_epoch().count());
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(49152, 65535); // dynamic port range according to IANA: https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml
    // TODO: test upper end for failures (port 65535 might not work)
    bool clientPortSuccesfullyReserved = false;
    constexpr unsigned int maxPortReservingAttempts = 10;
    for (unsigned int clientPortAttempts = 0; clientPortAttempts < maxPortReservingAttempts; clientPortAttempts++)
    {
        this->clientPort = uint16_t(distribution(generator));

        debugOutLevel(15, "========== [ Starting Client-Network... ] ========");

        // Connecting the peer is very simple.  0 means we don't care about
        // a connectionValidationInteger, and false for low priority threads
        socketDescriptor = new RakNet::SocketDescriptor(this->clientPort,
                                                        nullptr); // memory will be freed by base class destructor
        socketDescriptor->socketFamily = AF_INET;                 // IPV4

        // TODO: use normal priority on windows. Linux can use higher priority
        if (int startupResult = peer->Startup(1, socketDescriptor, 1))
        {
            Error::errContinue("Error starting up socket on port",
                               this->clientPort,
                               ". RakNet::StartupResult =",
                               startupResult,
                               "trying",
                               maxPortReservingAttempts - clientPortAttempts + 1,
                               "more times");
        }
        else
        {
            clientPortSuccesfullyReserved = true;
            break;
        }
    }

    if (not clientPortSuccesfullyReserved)
    {
        this->clientPort = 0;
        Error::errContinue("couldn't reserve client port in the range of", 49152, "to", 65535, "after", maxPortReservingAttempts, "tries -> aborting!");
        return false;
    }

    // List IP addresses
    // TODO: why are there 2 addresses?
    for (unsigned int i = 0; i < peer->GetNumberOfAddresses(); i++)
    {
        debugOutLevel(15, "My IP Address", i + 1, ": ", peer->GetLocalIP(i), "|", clientPort);
    }

    debugOutLevel(15,
                  "My GUID is",
                  peer->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS).ToString());

    debugOutLevel(15, "====================================");

    // initialise security stuff TODO: look up definition in docs
    // and do connect to server

    this->initialized = true;
    return true;
}

// ----------------------------------------------------------------------------
void NetworkClient::disconnectFromServer()
{
    debugOutLevel(20, "disconnectFromServer");
    peer->CloseConnection(this->serverAddress, true);
    // reset connected server infos
    this->serverGUID = RakNet::UNASSIGNED_RAKNET_GUID;
    this->serverAddress = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
}

void NetworkClient::requestStartPauseOrResumeGame(const PauseRequest::RequestType type)
{
    if (this->serverGUID == RakNet::UNASSIGNED_RAKNET_GUID or this->serverAddress == RakNet::UNASSIGNED_SYSTEM_ADDRESS)
    {
        debugOutLevel(Debug::DebugLevels::networking, "this->serverInfos isn't populated correctly!");
        return;
    }

    PauseRequest pauseRequest;
    pauseRequest.type = type;

    this->peer->Send(reinterpret_cast<const char*>(&pauseRequest),
                     sizeof(PauseRequest),
                     PacketPriority::HIGH_PRIORITY,
                     PacketReliability::RELIABLE,
                     0,
                     this->serverGUID,
                     false);
}

/**************************************************************
 * private memberfunctions
 **************************************************************/
