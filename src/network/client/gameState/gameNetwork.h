/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GAMENETWORK_H
#define GAMENETWORK_H

#include "../../networkStateBase.h"

#include "../../customPackets.h"         // defines PauseRequest
#include "../../../utils/static/error.h" // defines ErrCode

class Game;

// ----------------------------------------------------------------------------
class GameNetwork : public NetworkStateBase
{
   public:
    explicit GameNetwork(Game* const game);
    GameNetwork(const GameNetwork&) = delete;
    GameNetwork operator=(const GameNetwork&) = delete;
    virtual ~GameNetwork();

    virtual ErrCode init();

    bool processPackets();

    inline bool hasServerCreatedStaticReplicas() const
    {
        return this->serverHasCreatedStaticReplicas;
    }

    inline PauseRequest& getServerRequestedPause() { return this->serverRequestedPause; }
    /// @brief send out a packet to the server that our static replicas have been created
    void sendStaticReplicasCreated();

   private:
    Game* const game;

    bool serverHasCreatedStaticReplicas = false;

    PauseRequest serverRequestedPause;

    RakNet::SystemAddress serverAddress = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
    RakNet::RakNetGUID serverGUID = RakNet::UNASSIGNED_RAKNET_GUID;
};

#endif // GAMENETWORK_H
