/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gameNetwork.h"

// ----------------------------------------------------------------------------
#include <raknet/PacketPriority.h>
#include <raknet/RakPeerInterface.h>

#include "../networkclient.h" // only forward-declared in game
#include "../../../core/game.h"


// ----------------------------------------------------------------------------
GameNetwork::GameNetwork(Game* const game_)
    : NetworkStateBase(game_->getNetwork()->getPeer())
    , game(game_)
    , serverRequestedPause()
{
    peer->SetTimeoutTime(60000, RakNet::UNASSIGNED_SYSTEM_ADDRESS); // SetTimeoutTime of 60 sec for
                                                                    // the game
}

// ---------------------------------------------------------------------------
GameNetwork::~GameNetwork() {}

ErrCode GameNetwork::init()
{
    unsigned short numServers = 1;
    if (not peer->GetConnectionList(&this->serverAddress, &numServers))
    {
        Error::errContinue("not connected to any server!");
        return ErrCodes::GENERIC_ERR;
    }
    if (numServers != 1)
    {
        Error::errContinue("connected to more than one server!");
    }

    // upon entering the gameState the game should pause until the server sends the command to start
    // the state checks this by using 'getServerRequestedPause()' -> set the default request value
    // to 'Paused'
    PauseRequest stateEnterRequest = PauseRequest();
    stateEnterRequest.type = PauseRequest::RequestType::Paused;
    this->serverRequestedPause = stateEnterRequest;

    return ErrCodes::NO_ERR;
}

// ---------------------------------------------------------------------------
bool GameNetwork::processPackets()
{
    bool hasProcessedPackets = false;
    for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
    {
        hasProcessedPackets = true;
        const unsigned char packetIdentifier = this->game->getNetwork()->GetPacketIdentifier(packet);
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "packet type",
                      game->getNetwork()->packetIdentifierToString(packetIdentifier),
                      "from",
                      packet->systemAddress.ToString(true));

        switch (packetIdentifier)
        {
            case ID_STATIC_REPLICAS_CREATED:
                debugOutLevel(Debug::DebugLevels::networking, "server has created his static replicas");
                this->serverHasCreatedStaticReplicas = true;
                break;
            case CustomID::ID_START_PAUSE_OR_RESUME_GAME:
                // signal by the server to start actual game ticks
                if (this->game->getNetwork()->parsePauseRequest(packet, this->serverRequestedPause) != ErrCodes::NO_ERR)
                {
                    Error::errContinue(
                        "couldn't parse a start/pause/resume request send by the server!");
                    this->serverRequestedPause = PauseRequest();
                }
                break;
        } // switch (packetIdentifier)
    }     // for ( packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet =
          // peer->Receive())

    return hasProcessedPackets;
}

void GameNetwork::sendStaticReplicasCreated()
{
    const char id = static_cast<const char>(ID_STATIC_REPLICAS_CREATED);
    this->peer->Send(&id,
                     sizeof(static_cast<const char>(ID_STATIC_REPLICAS_CREATED)),
                     PacketPriority::MEDIUM_PRIORITY,
                     PacketReliability::RELIABLE,
                     0,
                     this->serverGUID,
                     false);
}

/**************************************************************
 * private memberfunctions
 **************************************************************/
