/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lobbyChatReplica.h"
// ----------------------------------------------------------------------------

#include "../../replicamanager3bt.h"
#include "../../staticReplicaIDs.h"
#include "../../../utils/debug.h"

#include <string>

// ----------------------------------------------------------------------------
LobbyChatReplica::LobbyChatReplica(ReplicaManager3BT* const replicaManager)
    : BattleTanks::Networking::StaticReplicaBase(BattleTanks::Networking::StaticReplicaIDs::LobbyStateChatReplicaID,
                                                 replicaManager)
{
    debugOutLevel(15, "Create ChatReplica");
}

LobbyChatReplica::~LobbyChatReplica()
{
    debugOutLevel(15, "Delete ChatReplica");
}

/******************************************************************************
 *                              RakNet functions                              *
 ******************************************************************************/

// ----------------------------------------------------------------------------
void LobbyChatReplica::SerializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                                     RakNet::Connection_RM3* destinationConnection)
{
    (void) constructionBitstream;
    (void) destinationConnection;

    debugOutLevel(11, "SerializeConstructionExisting ChatReplica");
}

// ----------------------------------------------------------------------------
void LobbyChatReplica::DeserializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                                       RakNet::Connection_RM3* sourceConnection)
{
    (void) constructionBitstream;
    (void) sourceConnection;

    debugOutLevel(11, "DeserializeConstructionExisting ChatReplica");
}

// ----------------------------------------------------------------------------
RakNet::RM3SerializationResult LobbyChatReplica::Serialize(RakNet::SerializeParameters* serializeParameters)
{
    bool sendVariables = false;

    // Send message
    if (this->message.serialize == true)
    {
        serializeParameters->outputBitstream[0].Write(true);
        serializeParameters->outputBitstream[0].Write(this->message.value);
        this->message.serialize = false;
        sendVariables = true;

        debugOutLevel(11, "Serialize: Send message:", this->message.value);
    }
    else
    {
        serializeParameters->outputBitstream[0].Write(false);
    }

    // Check if something was changed
    if (sendVariables == true)
    {
        // serializeParameters->outputBitstream[0].Reset();
        return RakNet::RM3SR_SERIALIZED_ALWAYS;
    }

    return RakNet::RM3SR_DO_NOT_SERIALIZE;
}

// ----------------------------------------------------------------------------
void LobbyChatReplica::Deserialize(RakNet::DeserializeParameters* deserializeParameters)
{
    debugOutLevel(20, "ChatReplica::Deserialize");

    // Buffer to store the info of an changed slot
    bool changedMessage;

    deserializeParameters->serializationBitstream[0].Read(changedMessage);
    if (changedMessage)
    {
        // received messages can simply be displayed on the chat
        RakNet::RakWString newMessage;
        deserializeParameters->serializationBitstream[0].Read(newMessage);
        if (this->messageReceived)
        {
            this->messageReceived(std::wstring(newMessage.C_String()));
        }
        this->messageList.Push(newMessage);
        debugOutLevel(11, "Deserialize: Got message:", newMessage);
    }
    else
    {
        debugOutLevel(11, "Deserialize: no new message in deserialization");
    }
}

// ----------------------------------------------------------------------------
RakNet::RM3QuerySerializationResult LobbyChatReplica::QuerySerialization(RakNet::Connection_RM3* destinationConnection)
{
    (void) destinationConnection;

    // debugOutLevel(11, "QuerySerialization ChatReplica");
    return RakNet::RM3QSR_CALL_SERIALIZE;
}

/**************************************************************
 * private memberfunctions
 **************************************************************/
