/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOBBYCHATREPLICA_H
#define LOBBYCHATREPLICA_H

#include <raknet/DS_List.h>
#include <raknet/ReplicaManager3.h>

#include <functional>

#include "../../serializeVar.h"
#include "../../staticReplicaBase.hpp"

class ReplicaManager3BT;

// ----------------------------------------------------------------------------
class LobbyChatReplica : public BattleTanks::Networking::StaticReplicaBase
{
   public:
    explicit LobbyChatReplica(ReplicaManager3BT* const replicaManager);
    virtual ~LobbyChatReplica();

    LobbyChatReplica(const LobbyChatReplica&) = delete;
    LobbyChatReplica operator=(const LobbyChatReplica&) = delete;

    void writeMessage(RakNet::RakWString message_)
    {
        this->message.value = message_;
        this->message.serialize = true;
    };

    std::function<void(std::wstring)> messageReceived;

    DataStructures::List<RakNet::RakWString> getMessageList() { return this->messageList; }
    // ----[ Replica functions ]--------------------------------

    // Construction
    void SerializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                       RakNet::Connection_RM3* destinationConnection) override;

    void DeserializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                         RakNet::Connection_RM3* sourceConnection) override;

    // Serialization
    RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* serializeParameters) override;

    void Deserialize(RakNet::DeserializeParameters* deserializeParameters) override;

    RakNet::RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3* destinationConnection) override;

   private:
    // ----[ Non-serializable variables ]-----------------------

    const RakNet::RakString replicaName = "Chat Replica";

    DataStructures::List<RakNet::RakWString> messageList;

    // ----[ Serializable variables ]---------------------------
    serializeVar<RakNet::RakWString> message{""};
};
#endif // LOBBYCHATREPLICA_H
