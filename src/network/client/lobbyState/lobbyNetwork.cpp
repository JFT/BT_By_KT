/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lobbyNetwork.h"
// ----------------------------------------------------------------------------
#include "../networkclient.h" // only forward-declared in game
#include "../../../core/game.h"

#include <raknet/PacketLogger.h> //TODO: remove on release build (useful for debug information)
#include <raknet/RakPeerInterface.h>

// ----------------------------------------------------------------------------
LobbyNetwork::LobbyNetwork(Game* const game_)
    : NetworkStateBase(game_->getNetwork()->getPeer())
    , game(game_)
{
    peer->SetTimeoutTime(1000, RakNet::UNASSIGNED_SYSTEM_ADDRESS); // needed this tiny
                                                                   // SetTimeoutTime of 1 sec for a
                                                                   // fast kick from the server if
                                                                   // its closed;
                                                                   // UNASSIGNED_SYSTEM_ADDRESS
                                                                   // means use TimeoutTime for all
                                                                   // systems
    // RakNet::PacketLogger* plogger = new RakNet::PacketLogger;
    // peer->AttachPlugin(plogger);
}

// ---------------------------------------------------------------------------
LobbyNetwork::~LobbyNetwork() {}

ErrCode LobbyNetwork::init()
{
    return ErrCodes::NO_ERR;
}

// ---------------------------------------------------------------------------
bool LobbyNetwork::processPackets()
{
    bool hasProcessedPackets = false;
    // Check if this is a network message packet
    for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
    {
        debugOutLevel(Debug::DebugLevels::networking + 1,
                      "processing packet from GUID",
                      packet->guid.ToString());
        hasProcessedPackets = true;
        // We got a packet, get the identifier with our handy function
        const unsigned char packetIdentifier = this->game->getNetwork()->GetPacketIdentifier(packet);
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "packet type",
                      this->game->getNetwork()->packetIdentifierToString(packetIdentifier),
                      "from",
                      packet->systemAddress.ToString(true));

        switch (packetIdentifier)
        {
            case CustomID::ID_START_PAUSE_OR_RESUME_GAME: // the server wants us to switch to the
                                                          // gamestate (this is the only case it
                                                          // will send us this packet. No need to
                                                          // actually parse the data)
                // make sure this is the correct server sending us that information
                if (packet->guid == this->game->getNetwork()->serverGUID and
                    packet->systemAddress == this->game->getNetwork()->serverAddress)
                {
                    this->serverChangedToGameState = true;
                }
                break;
            case ID_CONNECTION_LOST: // Couldn't deliver a reliable packet - i.e. the other system
                                     // was abnormally terminated
            case ID_DISCONNECTION_NOTIFICATION: // Connection lost normally
                this->stillConnected = false;
                break;
        } // switch (packetIdentifier)
    }     // for ( packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet =
          // peer->Receive())

    return hasProcessedPackets;
}

/**************************************************************
 * private memberfunctions
 **************************************************************/
