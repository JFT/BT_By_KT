/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERVERBROWSERNETWORK_H
#define SERVERBROWSERNETWORK_H

#include <functional>
#include <map> // std::map and std::string used to save pings to servers based on ip
#include <string>
#include <vector>

#include <raknet/GetTime.h> // defines RakNet::TimeMS

#include "../../customPackets.h" // defines struct ServerInfos (can't use forward declaration because getServerList() returns a reference not a pointer)
#include "../../networkStateBase.h"
#include "../../rateLimiter.h"
#include "../../../utils/circularMovingBuffer.h"
#include "../../../utils/static/error.h"

class Game;

// ----------------------------------------------------------------------------
class ServerBrowserNetwork : public NetworkStateBase
{
   public:
    struct PingInformation
    {
        RakNet::SystemAddress serverAddress = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
        CircularMovingBuffer<RakNet::TimeMS, 8> lastPings;
        size_t numPings = 0;
        RakNet::TimeMS averagePing = 0;
        /// @brief timepoint the server was last pinged at. Used to calculate ping if a response
        /// comes in.
        RakNet::TimeMS lastPingedAt = 0;
        /// @brief when was the last packed received from the server. Used to determine the age and
        /// if the server should be removed from the list.
        RakNet::TimeMS lastPacketReceived = 0;
    };

    explicit ServerBrowserNetwork(Game* const game);
    ServerBrowserNetwork(const ServerBrowserNetwork&) = delete;
    ServerBrowserNetwork operator=(const ServerBrowserNetwork&) = delete;
    virtual ~ServerBrowserNetwork();

    ErrCode init();

    /// @brief process all packets to network::peer
    /// @return true if any packets where recieved which changed displayable information, false
    /// otherwise
    bool processPackets();

    /// @brief ping all known servers and get them to send us their ServerInfos (name, slots,
    /// etc...) (rate limited by pingServerRateLimiter)
    /// if a server hasn't answered for a long time remove it from the serverList
    /// even if rate limited all known servers will be pinged
    void pingAndClearKnownServers();

    void tryToConnectToServer(const RakNet::SystemAddress serverAddress, const std::string serverPassword_);
    void checkStillPartOfARunningGame();

    // Getter and setter
    std::vector<std::pair<int, struct ServerInfos>>& getServerList() { return this->serverList; }
    std::map<std::string, PingInformation*>& getServerPingMap() { return this->serverPingMap; }
    bool switch_ = false;

    std::function<void(int, ServerInfos)> serverAdded;
    std::function<void(int)> serverRemoved;
    std::function<void(int, ServerInfos)> serverChanged;
    std::function<void(int, int)> serverPingChanged;
    std::function<void(int)> invalidPassword;

   private:
    Game* const game;
    /// @brief if a server doesn't respond to pings remove it after 2.5 seconds
    static constexpr RakNet::TimeMS nonRespondingServerMaxAge = 2500;
    /// @brief ping a specific server every 500ms and do max. 15 pings per second
    RateLimiter pingServerRateLimiter = RateLimiter(500, 15);

    /// @brief std::map for easy access to the pings. Will always be in sync with serverPings list
    /// representation
    std::map<std::string, PingInformation*> serverPingMap;

    /// @brief list of pings to make sure all server are pinged ever if pinging is rate-limited (by
    /// saving the index of the last pinged server). Will always be in sync with serverPingMap
    /// std::map representation. Using this method because inserting/erasing elements into the
    /// std::map corrupts the iterator needed to assure all servers are pinged.
    std::vector<PingInformation*> serverPings;


    /// @brief counter to generate unique server IDs. Ignoring overflow because it is okay to behave wrong after >2mio servers have been shown.
    int nextServerId = 0;
    /// @brief list of all known servers. Also contains servers which names we don't know and who
    /// haven't responded to our pings
    std::vector<std::pair<int, struct ServerInfos>> serverList;

    RakNet::RakPeerInterface* broadcastReceiver = nullptr;
    RakNet::SocketDescriptor* broadcastSocketDescriptor = nullptr;

    void updatePingFromPong(const RakNet::Packet* const pongPacket);
    void updatePingFromAdvertise(const RakNet::Packet* const packet);
};

#endif // SERVERBROWSERNETWORK_H
