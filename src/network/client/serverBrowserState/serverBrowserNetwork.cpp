/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "serverBrowserNetwork.h"

#include <CEGUI/Window.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/widgets/FrameWindow.h>
#include <raknet/MessageIdentifiers.h>
#include <raknet/RakPeerInterface.h>

// ----------------------------------------------------------------------------
#include "../lobbyState/lobbyChatReplica.h"
#include "../networkclient.h" // only forward-declared in game
#include "../playerCollectionReplica.h"
#include "../../defaultPorts.h"
#include "../../../core/config.h"
#include "../../../core/game.h"
#include "../../../gui/ceGuiEnvironment.h"
#include "../../../scripting/scriptingModule.h"
#include "../../../utils/static/conversions.h"

// ----------------------------------------------------------------------------
ServerBrowserNetwork::ServerBrowserNetwork(Game* const game_)
    : NetworkStateBase(game_->getNetwork()->getPeer())
    , game(game_)
    , serverPingMap()
    , serverPings()
    , serverList()
{
    // ctor
    if (this->init() != ErrCodes::NO_ERR)
    {
        Error::errTerminate("couldn't start LAN server broadcast receiver!");
    }
}

ServerBrowserNetwork::~ServerBrowserNetwork()
{
    if (this->broadcastReceiver != nullptr)
    {
        RakNet::RakPeerInterface::DestroyInstance(this->broadcastReceiver);
    }
    if (this->broadcastSocketDescriptor != nullptr)
    {
        delete this->broadcastSocketDescriptor;
    }
    for (size_t i = 0; i < this->serverPings.size(); i++)
    {
        delete this->serverPings[i];
    }

    // if the callback is still registered notify the other end
    // all servers are removed (as the list is being deleted)
    for (const auto& server : this->serverList)
    {
        const auto id = server.first;
        debugOutLevel(Debug::DebugLevels::updateLoop, "removing server", id);
        if (this->serverRemoved)
        {
            this->serverRemoved(id);
        }
    }
}

ErrCode ServerBrowserNetwork::init()
{
    this->broadcastReceiver = RakNet::RakPeerInterface::GetInstance();
    // Connecting the peer is very simple.  0 means we don't care about
    // a connectionValidationInteger, and false for low priority threads
    this->broadcastSocketDescriptor =
        new RakNet::SocketDescriptor(BattleTanks::Networking::advertisePortLAN, nullptr);
    this->broadcastSocketDescriptor->socketFamily = AF_INET; // IPV4

    // TODO: use normal priority on windows. Linux can use higher priority
    if (int startupResult = this->broadcastReceiver->Startup(1, this->broadcastSocketDescriptor, 1)) // TODO: fix linux thread priority
    {
        Error::errContinue("Error starting up socket on port",
                           BattleTanks::Networking::advertisePortLAN,
                           ". RakNet::StartupResult=",
                           startupResult);
        return ErrCodes::GENERIC_ERR;
    }

    return ErrCodes::NO_ERR;
}

// ----------------------------------------------------------------------------
void ServerBrowserNetwork::tryToConnectToServer(const RakNet::SystemAddress serverAddress_,
                                                const std::string serverPassword_)
{
    debugOutLevel(20, "tryToConnectToServer with IP: ", serverAddress_.ToString(true), "Passwort: ", serverPassword_);
    // don't open a connection if we have to shut down
    if (not game->getNetwork()->isRunning())
    {
        return;
    }

    if (serverAddress_ == RakNet::UNASSIGNED_SYSTEM_ADDRESS)
    {
        return;
    }

    debugOutLevel(20, "Try send Connecting request...");


    debugOutLevel(15,
                  serverAddress_.ToString(true),
                  serverAddress_.GetPort(),
                  serverPassword_.c_str(),
                  static_cast<int>(strlen(serverPassword_.c_str())));

    // because the playerCollectionReplica and lobbyChatReplica are static replicas they need to
    // exist before calling connect
    // they might still exist from an older invocation with outdated information -> clear and
    // re-create
    if (game->getPlayerCollectionReplica() != nullptr)
    {
        delete game->getPlayerCollectionReplica();
    }
    game->setPlayerCollectionReplica(new PlayerCollectionReplica(
        this->game->getNetwork()->getReplicaManager(),
        RakNet::RakWString(this->game->getConfiguration().getSettings().playerName.c_str())));

    if (game->getLobbyChatReplica() != nullptr)
    {
        delete game->getLobbyChatReplica();
    }
    game->setLobbyChatReplica(new LobbyChatReplica(this->game->getNetwork()->getReplicaManager()));

    RakNet::ConnectionAttemptResult car = peer->Connect(serverAddress_.ToString(true),
                                                        serverAddress_.GetPort(),
                                                        serverPassword_.c_str(),
                                                        static_cast<int>(strlen(serverPassword_.c_str())),
                                                        nullptr,
                                                        0,
                                                        12,
                                                        500,
                                                        0);

    std::string errorMessage = "Cant try to connect with Server: ";
#pragma GCC diagnostic ignored "-Wswitch"
    switch (car)
    {
        case RakNet::ConnectionAttemptResult::CONNECTION_ATTEMPT_STARTED:
        {
            debugOutLevel(20, "Connecting...");
            return;
        }
        break;
        case RakNet::ConnectionAttemptResult::INVALID_PARAMETER:
        {
            errorMessage.append("INVALID_PARAMETER");
        }
        break;
        case RakNet::ConnectionAttemptResult::CANNOT_RESOLVE_DOMAIN_NAME:
        {
            errorMessage.append("CANNOT_RESOLVE_DOMAIN_NAME");
        }
        break;
        case RakNet::ConnectionAttemptResult::ALREADY_CONNECTED_TO_ENDPOINT:
        {
            errorMessage.append("ALREADY_CONNECTED_TO_ENDPOINT");
        }
        break;
        case RakNet::ConnectionAttemptResult::CONNECTION_ATTEMPT_ALREADY_IN_PROGRESS:
        {
            errorMessage.append("CONNECTION_ATTEMPT_ALREADY_IN_PROGRESS");
        }
        break;
        case RakNet::ConnectionAttemptResult::SECURITY_INITIALIZATION_FAILED:
        {
            errorMessage.append("SECURITY_INITIALIZATION_FAILED");
        }
        break;
        default:
        {
            errorMessage.append("Unknown");
        }
    }
#pragma GCC diagnostic pop
    Error::errContinue(errorMessage.c_str());
}

// ----------------------------------------------------------------------------
bool ServerBrowserNetwork::processPackets()
{
    for (packet = this->broadcastReceiver->Receive(); packet != nullptr;
         this->broadcastReceiver->DeallocatePacket(packet), packet = this->broadcastReceiver->Receive())
    {
        const unsigned char packetIdentifier = this->game->getNetwork()->GetPacketIdentifier(packet);
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "broadcastReceiver: packet type",
                      this->game->getNetwork()->packetIdentifierToString(packetIdentifier),
                      "from",
                      packet->systemAddress.ToString(true));
        switch (packetIdentifier)
        {
            // a server pinged us on the broadcasting port -> add it to the list of servers which
            // will be pinged (from our client-port) to make it send us an advertisement
            case DefaultMessageIDTypes::ID_UNCONNECTED_PING:
            case DefaultMessageIDTypes::ID_UNCONNECTED_PING_OPEN_CONNECTIONS:
                auto it = this->serverPingMap.find(std::string(packet->systemAddress.ToString()));
                if (it == this->serverPingMap.end())
                {
                    PingInformation* newServer = new PingInformation();
                    newServer->serverAddress = packet->systemAddress;
                    newServer->lastPacketReceived =
                        RakNet::GetTimeMS(); // since the server send us his broadcast simply set
                                             // the last response time to the time we received the
                                             // broadcast
                    this->serverPings.push_back(newServer);
                    this->serverPingMap[packet->systemAddress.ToString(true)] = newServer;
                    debugOutLevel(Debug::DebugLevels::updateLoop,
                                  "got broadcasting ping from new server",
                                  packet->systemAddress.ToString(true));
                }
                else
                {
                    // a known server
                    it->second->lastPacketReceived = RakNet::GetTimeMS();
                }
                break;
        } // switch(packetIdentifier)
    }     // for ( packet = this->broadcastReceiver->Receive(); packet;
          // this->broadcastReceiver->DeallocatePacket(packet), packet =
          // this->broadcastReceiver->Receive())

    bool hasProcessedPackets = false;
    // now handle packets for the client port
    for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
    {
        hasProcessedPackets = true;
        const unsigned char packetIdentifier = this->game->getNetwork()->GetPacketIdentifier(packet);
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "packet type",
                      this->game->getNetwork()->packetIdentifierToString(packetIdentifier),
                      "from",
                      packet->systemAddress.ToString(true),
                      "on client port",
                      this->game->getNetwork()->clientPort);

        switch (packetIdentifier)
        {
            case ID_UNCONNECTED_PONG:
                this->updatePingFromPong(packet);
                break;
            case ID_ADVERTISE_SYSTEM:
                debugOutLevel(60, "Got ADVERTISE from a server\n");
                this->updatePingFromAdvertise(packet);

                // We need to move the data pointer by the messageID size because the host appends
                // ID_Advertise_System at the beginning of the data chunk.
                if (packet->length == sizeof(ServerInfos) + sizeof(RakNet::MessageID))
                {
                    ServerInfos* newServer =
                        reinterpret_cast<ServerInfos*>(((packet->data) + sizeof(RakNet::MessageID))); // copy the data out of the packet

                    // Loop through list of all known server and add the server if it is new
                    auto known = std::find_if(serverList.begin(), serverList.end(), [this](const auto& e) {
                        return e.second.address == this->packet->systemAddress;
                    });

                    // don't use the address the server internally uses but set the address that
                    // actually send us the packet (the server might think it is at 127.0.0.1 but
                    // still broadcast to LAN)
                    newServer->address = packet->systemAddress;
                    // always make sure there is a null-byte at the end of the name
                    newServer->name[ServerInfos::serverMaxNameLength - 1] = '\0';
                    if (known == this->serverList.end())
                    {
                        const int id = this->nextServerId++;
                        this->serverList.push_back({id, *newServer});
                        if (this->serverAdded)
                        {
                            this->serverAdded(id, *newServer);
                        }
                    }
                    else
                    {
                        known->second = *newServer;
                        if (this->serverChanged)
                        {
                            this->serverChanged(known->first, known->second);
                        }
                    }

                    debugOutLevel(Debug::DebugLevels::updateLoop - 3,
                                  "Received advertisement from server",
                                  newServer->address.ToString(true),
                                  "name=",
                                  newServer->name,
                                  newServer->filledSlots,
                                  "/",
                                  newServer->maxSlots);
                }
                break;
            case ID_CONNECTION_REQUEST_ACCEPTED:
            {
                // This tells the client they have connected
                debugOutLevel(15,
                              "ID_CONNECTION_REQUEST_ACCEPTED to",
                              packet->systemAddress.ToString(true),
                              "with GUID ",
                              packet->guid.ToString());
                debugOutLevel(15,
                              "My external address is",
                              peer->GetExternalID(packet->systemAddress).ToString(true));

                this->game->getNetwork()->serverGUID = packet->guid;
                this->game->getNetwork()->serverAddress = packet->systemAddress;

                if (packet->systemAddress == game->getConfiguration().getSettings().connectedServerAddress) // TODO define
                {
                    debugOutLevel(20, "Rejoin running Game");
                }
            }
            break;
            case ID_LOBBY_GENERAL: // self-defined packet to send the identificationKey
            {
                debugOutLevel(40, "ID_LOBBY_GENERAL");

                RakNet::SystemAddress serverAddress = packet->systemAddress;
                RakNet::RakWString serverName;
                RakNet::RakWString serverPassword;
                unsigned int identificationKey;

                RakNet::BitStream bsIn(packet->data, packet->length, false);
                bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
                if (bsIn.Read(serverName) and bsIn.Read(serverPassword) and bsIn.Read(identificationKey))
                {
                    debugOutLevel(15, "ServerName: ", serverName, ", ServerPassword: ", serverPassword, ", identificationKey: ", identificationKey);

                    // Set ServerName, ServerAddress and ServerPassword in the Config. Settings will
                    // Saved together with the IdentificationKey later in the Lobby-State
                    game->getConfiguration().getModifiableSettings().connectedServerName =
                        std::string(irr::core::stringc(serverName.C_String()).c_str());
                    game->getConfiguration().getModifiableSettings().connectedServerAddress = serverAddress;
                    game->getConfiguration().getModifiableSettings().connectedServerPassword =
                        std::string(irr::core::stringc(serverPassword.C_String()).c_str());
                    game->getConfiguration().getModifiableSettings().myIdentificationKey = identificationKey;

                    if (game->getConfiguration().saveServerBackup("config/serverBackup.xml",
                                                                  game->getDevice()) == ErrCodes::NO_ERR)
                    {
                        // signal the state that it can switch to the lobbyState
                        this->switch_ = true;
                    }
                    else
                    {
                        Error::errContinue("Cant save ServerBackup so disconnect with Server");
                        game->getNetwork()->disconnectFromServer();
                    }
                }
                else
                {
                    // error receiving the server infos (wrong packet?)
                    Error::errContinue(
                        "couldn't read identification key from packet -> disconnecting");
                    game->getNetwork()->disconnectFromServer();
                }
            }
            break;
            case ID_CONNECTION_ATTEMPT_FAILED:
            {
                debugOutLevel(40, "ID_CONNECTION_ATTEMPT_FAILED");

                game->getConfiguration().getModifiableSettings().connectedServerAddress =
                    RakNet::UNASSIGNED_SYSTEM_ADDRESS; // ServerAddress is now meaningless
                game->getConfiguration().getModifiableSettings().connectedServerPassword = "";
                // Save Settings
                if (game->getConfiguration().saveConfig("config/config.xml", game->getDevice()) ==
                    ErrCodes::NO_ERR) // TODO just save the ServerAddress
                {
                    debugOutLevel(20, "save config successful");
                }
                else
                {
                    Error::errContinue("Cant save Settings");
                }
            }
            break;
            case ID_INVALID_PASSWORD:
            {
                debugOutLevel(15, "ID_INVALID_PASSWORD");
                debugOutLevel(15, "cant connect with server!! Wrong Password");
                if (this->invalidPassword)
                {
                    // do we even know this server?
                    const auto known =
                        std::find_if(serverList.begin(), serverList.end(), [this](const auto& e) {
                            return e.second.address == this->packet->systemAddress;
                        });
                    if (known != serverList.end())
                    {
                        this->invalidPassword(known->first);
                    }
                }
            }
            break;
        }
    }

    return hasProcessedPackets;
}

// ----------------------------------------------------------------------------
void ServerBrowserNetwork::pingAndClearKnownServers()
{
    static size_t toPingNext = 0;
    const RakNet::TimeMS currentTime = RakNet::GetTimeMS();

    for (size_t i = 0; i < this->serverPings.size(); i++)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                      "checking for toPingNext =",
                      toPingNext,
                      "server: ",
                      this->serverPings[toPingNext]->serverAddress.ToString(true),
                      "lastPacketReceived",
                      this->serverPings[toPingNext]->lastPacketReceived,
                      " lastPingedAt=",
                      this->serverPings[toPingNext]->lastPingedAt,
                      "pings =",
                      this->serverPings[toPingNext]->lastPings.to_string());
        if (this->serverPings[toPingNext]->lastPingedAt != 0 /* 0 means the server was never pinged */ and
            (currentTime - this->serverPings[toPingNext]->lastPacketReceived) > this->nonRespondingServerMaxAge)
        {
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "server=",
                          this->serverPings[toPingNext]->serverAddress.ToString(true),
                          "will be removed because its last ping response was at",
                          this->serverPings[toPingNext]->lastPacketReceived,
                          "(currentTime=",
                          currentTime,
                          ", lastPingedAt=",
                          this->serverPings[toPingNext]->lastPingedAt,
                          ")");
            // remove the server out of the serverList and out of the pingedServers list (the for
            // loop will do nothing if the server isn't in the serverList
            auto at = std::find_if(serverList.begin(), serverList.end(), [this](const auto& e) {
                return e.second.address == this->serverPings[toPingNext]->serverAddress;
            });
            if (at != serverList.end())
            {
                const auto id = at->first;
                debugOutLevel(Debug::DebugLevels::updateLoop, "removing server", id);
                this->serverList.erase(at);
                if (this->serverRemoved)
                {
                    this->serverRemoved(id);
                }
            }

            const std::string toRemove = this->serverPings[toPingNext]->serverAddress.ToString(true);
            delete this->serverPings[toPingNext];
            this->serverPingMap.erase(toRemove);
            this->serverPings.erase(this->serverPings.begin() + toPingNext);
            toPingNext++;
            if (toPingNext >= this->serverPings.size()) // can't use modulo operator because
                                                        // this->serverPings.size() might be = 0
            {
                toPingNext = 0;
            }
            continue; // important: re-evaluate i because there might be no more servers left in the
                      // list and the next access of this->serverPings[toPingNext] might fail
                      // otherwise
        }

        if (currentTime - this->serverPings[toPingNext]->lastPingedAt < this->pingServerRateLimiter.singleTargetInterval)
        {
            // ping only every singleTargetInterval ms
            toPingNext++;
            if (toPingNext >= this->serverPings.size()) // can't use modulo operator because
                                                        // this->serverPings.size() might be = 0
            {
                toPingNext = 0;
            }
            continue;
        }

        if (not this->pingServerRateLimiter.queryPerformAction(currentTime))
        {
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "can't ping any more servers because "
                          "packet sending rate has been reached");
            break;
        }
        this->peer->Ping(this->serverPings[toPingNext]->serverAddress.ToString(false),
                         this->serverPings[toPingNext]->serverAddress.GetPort(),
                         true); // send unconnected ping (note: RakPeer::Ping(systemAddress) is for
                                // connected systems)
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "server=",
                      this->serverPings[toPingNext]->serverAddress.ToString(true),
                      "sending ping to the server at currentTime =",
                      currentTime);
        this->serverPings[toPingNext]->lastPingedAt = currentTime;
        toPingNext++;
        if (toPingNext >= this->serverPings.size()) // can't use modulo operator because
                                                    // this->serverPings.size() might be = 0
        {
            toPingNext = 0;
        }
    }
}

// ----------------------------------------------------------------------------
void ServerBrowserNetwork::checkStillPartOfARunningGame()
{
    debugOutLevel(11, "checkStillPartOfARunningGame (does nothing)");

    game->getConfiguration().loadServerBackup("config/serverBackup.xml");

    RakNet::SystemAddress serverAddress = game->getConfiguration().getSettings().connectedServerAddress;
    unsigned int identificationKey = game->getConfiguration().getSettings().myIdentificationKey;
    std::string connectedServerPassword = game->getConfiguration().getSettings().connectedServerPassword;
    if (serverAddress != RakNet::UNASSIGNED_SYSTEM_ADDRESS and identificationKey != 0)
    {
        debugOutLevel(10,
                      "send StillPartOfARunningGame request to saved Serveraddress'",
                      serverAddress.ToString(true),
                      "'");

        //        RakNet::BitStream bsOut;
        //        bsOut.Write(static_cast<RakNet::MessageID>(ID_LOBBY_GENERAL+10)); //TODO find a better id
        //        bsOut.Write(identificationKey);
        //
        //        game->getNetwork()->getPeer()->Send(&bsOut, HIGH_PRIORITY , UNRELIABLE, 0,
        //        serverAddress,
        //        false); // only for  unconnected systems
    }
    debugOutLevel(15, serverAddress.ToString(), identificationKey);
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void ServerBrowserNetwork::updatePingFromPong(const RakNet::Packet* const pongPacket)
{
    auto it = this->serverPingMap.find(std::string(pongPacket->systemAddress.ToString(true)));
    if (it == this->serverPingMap.end())
    {
        return;
    }

    RakNet::TimeMS pingSendTime;
    RakNet::BitStream bsIn(pongPacket->data, pongPacket->length, false);
    bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
    if (not bsIn.Read(pingSendTime)) // packet might be too short
    {
        return;
    }

    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "server=",
                  it->first,
                  "updating ping at",
                  RakNet::GetTimeMS(),
                  "last pinged at=",
                  it->second->lastPingedAt,
                  "start time=",
                  pingSendTime);
    // calculate average ping
    it->second->lastPings.push_back(RakNet::GetTimeMS() -
                                    pingSendTime); // TODO: maybe patch raknet as the documentation
                                                   // claims that the timeMS saved in the pongPacket
                                                   // is the actual ping (instead of the time the
                                                   // corresponding ping was send) -> in
                                                   // RakPeer::ProcessOfflineNetworkpongPacket
                                                   // (tested to get the pongPacket correctly but
                                                   // just returning the send-time instead of
                                                   // calculating the ping upon receiveing)
    it->second->numPings++;
    if (it->second->numPings > it->second->lastPings.size())
    {
        it->second->numPings = it->second->lastPings.size();
    }
    RakNet::TimeMS sumPing = 0;
    for (size_t i = 0; i < it->second->numPings; i++)
    {
        sumPing += it->second->lastPings.get(i);
    }
    it->second->averagePing = sumPing / it->second->numPings;
    it->second->lastPacketReceived = RakNet::GetTimeMS();
    if (this->serverPingChanged)
    {
        const auto server =
            std::find_if(this->serverList.cbegin(), this->serverList.cend(), [&pongPacket](const auto& e) {
                return e.second.address == pongPacket->systemAddress;
            });
        if (server != serverList.end())
        {
            this->serverPingChanged(server->first, it->second->averagePing);
        }
    }
}

void ServerBrowserNetwork::updatePingFromAdvertise(const RakNet::Packet* const packet)
{
    auto it = this->serverPingMap.find(std::string(packet->systemAddress.ToString(true)));
    if (it == this->serverPingMap.end())
    {
        // the case that the server sends an andvertisement without us having it
        // explicitly pinged can happen if the only avaliable network connection is the loopback device
        // (Hannes: at least on my system this makes the client receive the packet from '0.0.0.0' by
        // the broadcast receiver but the server but the advertisements come in from '127.0.0.1', as expected)

        // add the server to the list of servers to be pinged because it wasn't in
        // there yet (this also makes sure all server on the serverList are on the
        // serverPings list and they get removed if they don't respond to pings for
        // too long)
        PingInformation* newServer = new PingInformation();
        newServer->serverAddress = packet->systemAddress;
        newServer->lastPacketReceived = RakNet::GetTimeMS(); // since the server send us his broadcast simply
                                                             // set the last response time to the time we
                                                             // received the broadcast
        this->serverPings.push_back(newServer);
        this->serverPingMap[packet->systemAddress.ToString(true)] = newServer;
    }
    else
    {
        it->second->lastPacketReceived = RakNet::GetTimeMS();
    }
}
