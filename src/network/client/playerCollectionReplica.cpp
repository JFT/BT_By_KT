/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "playerCollectionReplica.h"
// ----------------------------------------------------------------------------

#include "../../utils/stringwstream.h" // used to output playerNames in debug and error messages
#include "../replicamanager3bt.h"
#include "../staticReplicaIDs.h"
#include "../../states/multipleStatesConstants.h"
#include "../../utils/debug.h"
#include "../../utils/static/error.h"

#include <algorithm>

// ----------------------------------------------------------------------------

PlayerCollectionReplica::PlayerCollectionReplica(ReplicaManager3BT* const replicaManager, RakNet::RakWString playerName_)
    : StaticReplicaBase(BattleTanks::Networking::StaticReplicaIDs::PlayerCollectionReplicaID, replicaManager)
    , m_playerName(playerName_)
    , slots()
{
    debugOutLevel(15, "Create PlayerCollectionReplica");
}

// ----------------------------------------------------------------------------
PlayerCollectionReplica::~PlayerCollectionReplica()
{
    debugOutLevel(15, "Delete PlayerCollectionReplica");
}

std::wstring PlayerCollectionReplica::getPlayerName(const size_t slot) const
{
    return std::wstring(this->slots.at(slot).playerName.C_String());
}

bool PlayerCollectionReplica::isReady(const size_t slot) const
{
    return this->slots.at(slot).ready;
}

// ----------------------------------------------------------------------------
irr::core::stringw PlayerCollectionReplica::getPlayerLabel(unsigned int slot)
{
    if (slot >= slots.size())
    {
        Error::errContinue("Slot", slot, "out of range", slots.size());
        return L"ERROR";
    }

    irr::core::stringw label = irr::core::stringw(this->slots[slot].playerName.C_String());
    label += L" ";
    label += this->slots[slot].ready ? L"R" : L"";

    return label;
}

bool PlayerCollectionReplica::allPlayersReady() const
{
    return std::all_of(slots.cbegin(), slots.cend(), [](const auto& slot) {
        return !slot.isInUse || slot.ready;
    });
}

/******************************************************************************
 *                              RakNet functions                              *
 ******************************************************************************/

// ----------------------------------------------------------------------------
void PlayerCollectionReplica::SerializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                                            RakNet::Connection_RM3* /* destinationConnection */)
{
    // packet layout: (needs to keep in sync with network/server/playerCollectionReplica!)
    // 1) RakNet::RakWString: name of this player

    debugOutLevel(25, "SerializeConstructionExisting");
    constructionBitstream[0].Write(this->m_playerName);
}

// ----------------------------------------------------------------------------
void PlayerCollectionReplica::DeserializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                                              RakNet::Connection_RM3* sourceConnection)
{
    (void) sourceConnection;

    debugOutLevel(25, "DeserializeConstructionExisting ");

    // Reset any old state, this should be a new object
    this->slots.clear();
    this->readyToStartGame.value = false;
    this->requestPosition.serialize = false;


    // packet layout: (needs to keep in sync with network/server/playerCollectionReplica!)
    // 1) unsinged int: number of slots the server has
    // 2) unsigned int = N: number of used slots
    // 3) N x:
    //     3.1) unsigned int: slot id of used slot
    //     3.2) RakNet::RakWString: name of player at slot i
    //     3.3) bool: ready flag of player at slot i

    unsigned int serverSlots = 0;
    constructionBitstream[0].Read(serverSlots);

    if (serverSlots > BattleTanks::MultipleStatesConstants::AbsoluteMaxNumberOfPlayers)
    {
        Error::errContinue("server said it has",
                           serverSlots,
                           "but AbsoluteMaxNumberOfPlayers is ",
                           BattleTanks::MultipleStatesConstants::AbsoluteMaxNumberOfPlayers,
                           "ignoring the request by the server and using",
                           BattleTanks::MultipleStatesConstants::AbsoluteMaxNumberOfPlayers);
        serverSlots = BattleTanks::MultipleStatesConstants::AbsoluteMaxNumberOfPlayers;
    }

    this->slots.resize(serverSlots);

    uint32_t usedSlots;
    constructionBitstream[0].Read(usedSlots);
    if (usedSlots > serverSlots)
    {
        Error::errContinue("server said it has", usedSlots, "but it said it only has", serverSlots, "slots in total! Aborting DeserializeConstructionExisting");
        return;
    }

    for (unsigned int i = 0; i < usedSlots; i++)
    {
        uint32_t slot;
        constructionBitstream[0].Read(slot);
        if (slot >= this->slots.size())
        {
            Error::errContinue("server send information for slot",
                               slot,
                               "which is outside of the",
                               serverSlots,
                               "slots it said it has! Aborting DeserializeConstructionExisting");
            return;
        }
        RakNet::RakWString remotePlayerName = L"-";
        if (not constructionBitstream[0].Read(remotePlayerName))
        {
            Error::errContinue("error reading name of player in slot", slot);
            break;
        }
        bool isReadyToStartGame = false;
        if (not constructionBitstream[0].Read(isReadyToStartGame))
        {
            Error::errContinue("error reading isReadyToStartGame flag of slot", slot);
            break;
        }
        this->slots[slot].isInUse = true;
        this->slots[slot].playerName = remotePlayerName;
        this->slots[slot].ready = isReadyToStartGame;
    } // for (unsigned int i = 0; i < usedSlots; i++)

    this->printPlayerMap();
}

// ----------------------------------------------------------------------------
RakNet::RM3SerializationResult PlayerCollectionReplica::Serialize(RakNet::SerializeParameters* serializeParameters)
{
    // packet layout: (needs to keep in sync with network/server/playerCollectionReplica!)
    // 1) bool, readyToStartGame
    // 2) unsigned int requested slot (== current slot if no change wanted)

    // Send the need for initialize
    debugOutLevel(11, "Serialize: readyToStartGame Flag: ", this->readyToStartGame.value ? "ready" : "unready");
    serializeParameters->outputBitstream[0].Write(this->readyToStartGame.value);
    debugOutLevel(11, "Serialize: requestPosition :", this->requestPosition.value, "current position=", this->currentSlot, "if == current -> no change requested");
    serializeParameters->outputBitstream[0].Write(this->requestPosition.value);

    // everything serialized
    this->readyToStartGame.serialize = false;
    this->requestPosition.serialize = false;

    return RakNet::RM3SerializationResult::RM3SR_BROADCAST_IDENTICALLY_FORCE_SERIALIZATION; // fastest
    // method
    // (according
    // to
    // documentation)
    // because
    // it't
    // already
    // certain
    // that
    // something
    // changed
}

// ----------------------------------------------------------------------------
void PlayerCollectionReplica::Deserialize(RakNet::DeserializeParameters* deserializeParameters)
{
    if (this->slots.size() == 0) // make sure PlayerMap was init before
    {
        Error::errContinue("ERROR Deserialize before DeserializeConstructionExisting");
        return;
    } // TODO: remove on release build?

    // packet layout: (needs to keep in sync with network/server/playerCollectionReplica!)
    // 1) unsigned int: slot id of the player receiving the packet
    // 2) unsigned int = N: number of updated slots
    // N x:
    //     2.1) unsigned int: slot id which information follows
    //     2.2) bool: is the slot in use
    //     if true:
    //         2.2.1) bool: ready for this player
    //         2.2.2) bool: player ID changed?
    //         if true:
    //             2.2.2.1) uint32_t new player ID
    //         2.2.3) bool: name changed?
    //         if true:
    //             2.2.3.1) RakNet::RakWString new name

    debugOutLevel(11, "Deserialize: updateSlotInfos");

    unsigned int deserializedCurrentSlot;

    if (not deserializeParameters->serializationBitstream[0].Read(deserializedCurrentSlot))
    {
        Error::errContinue("Error desesializing number  current slot! aborting Deserialize");
        return;
    }
    debugOutLevel(11, "current slot=", deserializedCurrentSlot);
    if (deserializedCurrentSlot > BattleTanks::MultipleStatesConstants::AbsoluteMaxNumberOfPlayers)
    {
        Error::errContinue("server said our current slot is",
                           deserializedCurrentSlot,
                           "but AbsoluteMaxNumberOfPlayers is ",
                           BattleTanks::MultipleStatesConstants::AbsoluteMaxNumberOfPlayers,
                           "ignoring the request by the server and not changing our current slot "
                           "of",
                           this->currentSlot);
    }
    else
    {
        this->currentSlot = deserializedCurrentSlot;
    }

    unsigned int nrOfSerialize;

    if (not deserializeParameters->serializationBitstream[0].Read(nrOfSerialize))
    {
        Error::errContinue("Error desesializing number of changed slots! aborting Deserialize");
        return;
    }
    debugOutLevel(11, nrOfSerialize, "Slotinfo updates found");

    if (nrOfSerialize > BattleTanks::MultipleStatesConstants::AbsoluteMaxNumberOfPlayers)
    {
        Error::errContinue("server said it has updated",
                           nrOfSerialize,
                           "but AbsoluteMaxNumberOfPlayers is ",
                           BattleTanks::MultipleStatesConstants::AbsoluteMaxNumberOfPlayers,
                           "ignoring the request by the server and using",
                           BattleTanks::MultipleStatesConstants::AbsoluteMaxNumberOfPlayers);
        nrOfSerialize = BattleTanks::MultipleStatesConstants::AbsoluteMaxNumberOfPlayers;
    }

    for (unsigned int serializeNr = 0; serializeNr < nrOfSerialize; serializeNr++)
    {
        unsigned int slot;
        if (not deserializeParameters->serializationBitstream[0].Read(slot))
        {
            Error::errContinue("Error deserializing slot number!. Aborting deserialization.");
            return;
        }
        if (slot > this->slots.size())
        {
            Error::errContinue("read slot id",
                               slot,
                               "from packet which is larger than the number of slots",
                               this->slots.size(),
                               "aborting Deserialize");
            return;
        }
        debugOutLevel(20, "slot:", slot);

        bool slotInUse;
        deserializeParameters->serializationBitstream[0].Read(slotInUse);
        if (slotInUse)
        {
            debugOutLevel(20,
                          "slot",
                          slot,
                          "in use -> getting ready flag and checkint to see if a "
                          "new name needs to be read");
            this->slots[slot].isInUse = true;
            bool newReadyToStartGameFlag;
            if (not deserializeParameters->serializationBitstream[0].Read(newReadyToStartGameFlag))
            {
                Error::errContinue(
                    "Error deserializing ready to start game flag!. Aborting deserialization.");
                return;
            }
            this->slots[slot].ready = newReadyToStartGameFlag;
            debugOutLevel(11, "Update readyToStartGame: ", newReadyToStartGameFlag ? "ready" : "unready");

            bool updatePlayerID;
            if (not deserializeParameters->serializationBitstream[0].Read(updatePlayerID))
            {
                Error::errContinue("Error desesializing if a slot", slot, "name changed! aborting Deserialize");
                return;
            }
            if (updatePlayerID)
            {
                uint32_t newPlayerID;
                if (not deserializeParameters->serializationBitstream[0].Read(newPlayerID))
                {
                    Error::errContinue("couldn't deserialize name for player in slot", slot, "aborting Deserialize");
                    return;
                }
                this->slots[slot].playerID = newPlayerID;
                debugOutLevel(11, "Update playerID: ", irr::core::stringw(newPlayerID));
            }

            bool updatePlayerName;
            if (not deserializeParameters->serializationBitstream[0].Read(updatePlayerName))
            {
                Error::errContinue("Error desesializing if a slot", slot, "name changed! aborting Deserialize");
                return;
            }
            if (updatePlayerName)
            {
                RakNet::RakWString newPlayerName;
                if (not deserializeParameters->serializationBitstream[0].Read(newPlayerName))
                {
                    Error::errContinue("couldn't deserialize name for player in slot", slot, "aborting Deserialize");
                    return;
                }
                this->slots[slot].playerName = newPlayerName;
                debugOutLevel(11, "Update playerName: ", irr::core::stringw(newPlayerName.C_String()));
            }
        } // if (slotInUse)
        else
        {
            this->slots[slot] = BattleTanks::Networking::SlotInfos();
        }
    } // for (unsigned int serializeNr = 0; serializeNr < nrOfSerialize; serializeNr++)

    this->printPlayerMap();

    if (deserializeParameters->serializationBitstream->GetNumberOfUnreadBits() > 0)
    {
        Error::errContinue("left",
                           deserializeParameters->serializationBitstream->GetNumberOfUnreadBits(),
                           "bits unread in stream!");
    }
}

// ----------------------------------------------------------------------------
RakNet::RM3QuerySerializationResult
PlayerCollectionReplica::QuerySerialization(RakNet::Connection_RM3* /* destinationConnection */)
{
    debugOutLevel(Debug::DebugLevels::updateLoop, "QuerySerialization");
    if (this->readyToStartGame.serialize or this->requestPosition.serialize)
    {
        return RakNet::RM3QuerySerializationResult::RM3QSR_CALL_SERIALIZE;
    }
    return RakNet::RM3QuerySerializationResult::RM3QSR_DO_NOT_CALL_SERIALIZE;
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

void PlayerCollectionReplica::printPlayerMap() // Also just for debug!
{
    for (unsigned int position = 0; position < this->slots.size(); ++position)
    {
        debugOutLevel(20,
                      position,
                      ": ",
                      irr::core::stringw(this->slots[position].playerName.C_String()),
                      this->slots[position].ready ? "R" : "|");
    }
}
