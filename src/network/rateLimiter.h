/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RATELIMITER_H
#define RATELIMITER_H

#include <raknet/RakNetTime.h>

#include "../utils/circularMovingBuffer.h"

/// @brief class to determine if some action limited to maxActionsPerSecond shold be performed or
/// not
/// not perfectly exact (meaning there are cases where more than maxActionsPerSecond actions are
/// performed inside a short interval) but shold be close enough
class RateLimiter
{
   public:
    /// @param singleTargetInterval how long to wait between performing the action for the same
    /// target again (warning: RateLimiter doesn't track that information. The using class must
    /// ensure that the interval between actions for the same target is correct. Just stored in this
    /// class because it is likely used in the same spot)
    /// @param maxActionsPerSecond_ how many total actions can be performed per Second
    ///
    /// A simple example highlights the difference between the two parameters:
    /// Consider pinging a list of servers. A resonable thing to implement would be to ping a
    /// specific server every half second while not doing more than 10 pings per second total.
    /// -> singleTargetInterval = 500ms and maxActionsPerSecond = 10
    RateLimiter(const RakNet::TimeMS singleTargetInterval_, const unsigned int maxActionsPerSecond_);

    /// @param singleTargetInterval how long to wait between performing the action for the same
    /// target again (warning: RateLimiter doesn't track that information. The using class must
    /// ensure that the interval between actions for the same target is correct. Just stored in this
    /// class because it is likely used in the same spot)
    const RakNet::TimeMS singleTargetInterval;
    const unsigned int maxActionsPerSecond;

    /// @brief check if an action can be performed and have the rate of actions still be
    /// maxActionsPerSecond
    /// @param currentTime the current time (calling RakNet::GetTime() can be slow and if queries
    /// are performed inside a fast loop the time mostly doesn't change anyway)
    /// @return true if the action can be performed, false otherwise. If true the internal rate
    /// counter will be updated assuming the action was indeed performed.
    bool queryPerformAction(const RakNet::TimeMS currentTime);

   private:
    RakNet::TimeMS oldestActionInNewestSlice = 0;
    unsigned int sumActionsLastSecond = 0;

    /// @brief naive implementation would save each performed action and iterate over all to see how
    /// many where performed inside the last second. But that is slow
    /// very fast implementation would save only the number of actions inside the last second and
    /// assume they are equally distributed. But this is vurnerable to performing too many actions
    /// if lots of actions where performed shortly before the next action is queried
    /// -> split the last second into 8 parts and save how many actions where performed during each
    /// 8-th of the second, treat actions as equally spaced inside the 1/8th slice (maximum mistake
    /// rate thus is maxActionsPerSecond * (1 + 1/8) if lots of actions where performed just before
    /// the next queryPerformAction call)
    CircularMovingBuffer<unsigned int, 8> actionsLastSecond;
};

#endif // ifndef RATELIMITER_H
