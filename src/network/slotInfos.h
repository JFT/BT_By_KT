/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SLOTINFOS_H
#define SLOTINFOS_H

#include <raknet/RakNetTypes.h> // SystemAddress, RakNetGUID
#include <raknet/RakWString.h>

#include "serializeVar.h"

class ConnectionBT;

namespace BattleTanks
{
    namespace Networking
    {
        struct SlotInfos
        {
#if defined(MAKE_SERVER_)
            serializeVar<RakNet::RakWString> playerName =
                serializeVar<RakNet::RakWString>(RakNet::RakWString(L"-"));
            serializeVar<bool> ready = serializeVar<bool>(false);
            serializeVar<bool> isInUse = serializeVar<bool>(false);
            serializeVar<uint32_t> playerID = serializeVar<uint32_t>(static_cast<uint32_t>(-1));

            RakNet::RakNetGUID guid = RakNet::UNASSIGNED_RAKNET_GUID;
            RakNet::SystemAddress systemAddress = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
            ConnectionBT* connection;

#else
            RakNet::RakWString playerName = L"-";
            uint32_t playerID = static_cast<uint32_t>(-1);
            bool ready = false;
            bool isInUse = false;
#endif

            uint32_t identificationKey = 0; // needed for player identification in case of a
                                            // disconnect/crash of the client (SystemAddress and ID
                                            // might change)
        };
    } // namespace Networking
} // namespace BattleTanks

#endif /* ifndef SLOTINFOS_H */
