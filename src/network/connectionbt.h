/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONNECTIONBT_H
#define CONNECTIONBT_H

#include <raknet/ReplicaManager3.h>
#include "allocationIDs.h"
#include "replicamanager3bt.h"

// forward declarations
class Game;


// ----------------------------------------------------------------------------
class ConnectionBT : public RakNet::Connection_RM3
{
    friend class PlayerCollectionServer;

   public:
    ConnectionBT(const RakNet::SystemAddress& _systemAddress, RakNet::RakNetGUID _guid, Game* _game)
        : RakNet::Connection_RM3(_systemAddress, _guid)
        , game(_game)
    {
    }
    ConnectionBT(const ConnectionBT&) = delete;
    ConnectionBT operator=(const ConnectionBT&) = delete;

    virtual ~ConnectionBT();

    // See documentation - Makes all messages between ID_REPLICA_MANAGER_DOWNLOAD_STARTED and
    // ID_REPLICA_MANAGER_DOWNLOAD_COMPLETE arrive in one tick
    // disabled because combined costruction and serialization doesn't seem to work for already
    // existing replicas
    bool QueryGroupDownloadMessages(void) const { return false; }
    // saving the id of the player inside the connection for easy access
    /// @brief
    /// @return the id of the player associated with this connection. -1 if no player associated yet
    inline size_t getPlayerSlot() const { return this->playerSlot; }
    inline size_t getPlayerID() const { return this->playerID; }
    inline void setPlayerSlot(const size_t id) { this->playerSlot = id; }
    virtual RakNet::Replica3*
    AllocReplica(RakNet::BitStream* allocationIDStream, RakNet::ReplicaManager3* replicaManager3)
    {
        ReplicaManager3BT* replicaManager3BT = static_cast<ReplicaManager3BT*>(replicaManager3);

        Networking::NetworkedAllocationIDType allocationID;
        allocationIDStream->Read(allocationID);

        return replicaManager3BT->callAllocationCallback(static_cast<Networking::AllocationID>(allocationID));

        /*ObjectType typeID;
            // RakNet::RakString typeName;
            allocationId->Read(typeID);

            //TESTING
            // TODO:
            //Here we need to put together a number that tells us - which type of unit/tank/creep/building to create and which data(model, properties etc.) corresponds to
            // the object we need to create
            if (typeID == ObjectType::OT_TANK)
            {
                //return new Tank(game);
            }
            else if(typeID == ObjectType::OT_CREEP){
                //return new Creep(game);
            }
            else
            {
                return nullptr;
            }*/
    }

   private:
    inline void setPlayerID(const size_t id) { this->playerID = id; }

    Game* const game;

    size_t playerSlot = -1;
    size_t playerID = -1;
};

#endif /* CONNECTIONBT_H */
