#include "staticReplicaBase.hpp"

#include "replicamanager3bt.h"

using namespace BattleTanks::Networking;

StaticReplicaBase::StaticReplicaBase(const StaticReplicaIDs allocationId, ReplicaManager3BT* const replicaManager)
    : m_replicaManager(replicaManager)
{
    this->SetNetworkIDManager(m_replicaManager->GetNetworkIDManager());
    this->SetNetworkID(RakNet::UniqueIDType(allocationId));
    m_replicaManager->Reference(this);
}

/******************************************************************************
 *                              RakNet functions                              *
 ******************************************************************************/

// ----------------------------------------------------------------------------
RakNet::RM3ConstructionState StaticReplicaBase::QueryConstruction(RakNet::Connection_RM3* destinationConnection,
                                                                  RakNet::ReplicaManager3* replicaManager3)
{
    (void) destinationConnection;
    (void) replicaManager3;

    // Let client and server call Ser/DeserializeConstructionExististing().
    return RakNet::RM3CS_ALREADY_EXISTS_REMOTELY;
}
