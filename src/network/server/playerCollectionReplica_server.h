/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAYERCOLLECTION_SERVER_H_LGTJJECF
#define PLAYERCOLLECTION_SERVER_H_LGTJJECF

#include <vector>

#include <raknet/ReplicaManager3.h> // defines Replica3

#include "../connectionbt.h" // used to store the playerID
#include "../serializeVar.h"
#include "../slotInfos.h"
#include "../staticReplicaBase.hpp"
#include "../../core/random.h"

class NetworkServer;
class ReplicaManager3BT;

// ----------------------------------------------------------------------------
/// @brief Class which orders the players to slots.
///
/// Clients can set the slots in the lobby state. This will accordingly
/// be serialized over the network.
/// The class should be constructed with the according name of the player.
///
/// PlayerCollectionServer is defined as a static replica. So it must be constructed
/// by the server and the clients manually. Only the sending of the slot
/// will be handled.
// ----------------------------------------------------------------------------
class PlayerCollectionServer : public BattleTanks::Networking::StaticReplicaBase
{
   public:
    PlayerCollectionServer(ReplicaManager3BT* const replicaManager, NetworkServer* const networkServer_);

    /// @brief adds a new player to the lobby
    /// @param systemAddress_
    /// @param identificationKey uniquely generated key for the player. Used if the player needs to
    /// reconnect.
    /// @return the slot the new player will be placed in (-1 cast to unsigned int if no free slots)
    unsigned int addNewPlayer(const RakNet::SystemAddress systemAddress_, const unsigned int identificationKey);
    void removeDisconnectedPlayers(RakNet::SystemAddress disconnectedSystemAddress);

    /// @brief return the slotID of 'address'
    /// @param RakNet::SystemAddress
    /// @return slot of the supplied address. staitc_cast<unsigned int>(-1) if not connected or
    /// address is RakNet::UNASSIGNED_SYSTEM_ADDRESS
    unsigned int slotIDFromSystemAddress(const RakNet::SystemAddress address);

    std::vector<BattleTanks::Networking::SlotInfos>& getSlots() { return this->slots; }
    // ----------------------------------------------------------------------------
    // team management
    // ----------------------------------------------------------------------------
    enum TeamID
    {
        InTeam1,
        InTeam2,
        AnyTeam
    };

    /// @brief slot of the first player in team 2 (marks the seperation between team2 and team2)
    const size_t team2StartID;

    /// @return number of slots in use for the specific team
    unsigned int getPlayerCount(const TeamID team = TeamID::AnyTeam) const;

    unsigned int getFreeSlots(const TeamID team = TeamID::AnyTeam) const;

    inline bool isInTeam(const size_t slotID, const TeamID team) const
    {
        return team == TeamID::InTeam2 and slotID >= this->team2StartID;
    }

    // ----------------------------------------------------------------------------
    // Lobby specific functions
    // ----------------------------------------------------------------------------

    /// @brief set to allow serialization/deserialization of slot changes
    /// @param val
    void setAllowSlotChanges(const bool val);

    /// @brief check if all used slots are ready
    bool allPlayersReady() const;

    /// @brief give all players a playerID before the game starts
    void setAllPlayerIDs();

    // ----------------------------------------------------------------------------
    // Replica functions
    // ----------------------------------------------------------------------------

    void SerializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                       RakNet::Connection_RM3* destinationConnection) override;

    void DeserializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                         RakNet::Connection_RM3* sourceConnection) override;

    // Serialization
    RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* serializeParameters) override;

    void Deserialize(RakNet::DeserializeParameters* deserializeParameters) override;

    // Query Construction, Serialization, PopConnection
    RakNet::RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3* destinationConnection) override;

   private:
    NetworkServer* const networkServer;

    /// @brief set to false to stop deserializing/serializing slot changes
    bool allowSlotChanges = true;

    // Helper
    void printPlayerMap();

    // ----[ Non-serializable variables ]-----------------------

    const RakNet::RakString replicaName = "PlayerCollectionServer";

    size_t newPlayerSlot = 0; // hold the slot of a new Player (set by network on new incoming connection)

    // ----[ serializable variables ]-----------------------

    // players in the lobby (the struct BattleTanks::Networking::SlotInfos contains the
    // serializeVars
    std::vector<BattleTanks::Networking::SlotInfos> slots;
};

#endif /* end of include guard: PLAYERCOLLECTION_SERVER_H_LGTJJECF */
