/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "playerCollectionReplica_server.h"
// ----------------------------------------------------------------------------


#include "../../utils/stringwstream.h" // stringwstream needs to be included before debug.h and error.h because they use operator<< on a stringw

#include "network_server.h"

#include "../connectionbt.h" // used to store the playerID
#include "../staticReplicaIDs.h"

#include "../../states/multipleStatesConstants.h"
#include "../../utils/debug.h"

using namespace BattleTanks::Networking;

// ----------------------------------------------------------------------------
PlayerCollectionServer::PlayerCollectionServer(ReplicaManager3BT* const replicaManager,
                                               NetworkServer* const networkServer_)
    : StaticReplicaBase(BattleTanks::Networking::StaticReplicaIDs::PlayerCollectionReplicaID, replicaManager)
    , team2StartID(10 / 2)
    , networkServer(networkServer_)
    , slots(std::vector<BattleTanks::Networking::SlotInfos>(10)) //TODO: make the number of slots configurabile? //TODO: change order of initialization (but until then needs slots to be initzialized first)
{
    debugOutLevel(15, "Create PlayerCollectionReplica");
}

// ----------------------------------------------------------------------------
unsigned int PlayerCollectionServer::addNewPlayer(const RakNet::SystemAddress systemAddress_,
                                                  const unsigned int identificationKey)
{
    debugOutLevel(11, "addNewPlayer: Need to add new Player");

    if (this->getPlayerCount(TeamID::AnyTeam) == this->slots.size())
    {
        debugOutLevel(Debug::DebugLevels::networking + 2, "no free slots found!");
        return static_cast<unsigned int>(-1);
    }

    unsigned int playerCountFirstTeam = this->getPlayerCount(TeamID::InTeam1);
    unsigned int playerCountSecondTeam = this->getPlayerCount(TeamID::InTeam2);

    size_t assignedTeamBegin = 0;
    size_t assignedTeamEnd = this->team2StartID;
    if (playerCountFirstTeam > playerCountSecondTeam and this->getFreeSlots(TeamID::InTeam2) > 0) // team 1 has more players and team 2 has empty slots
    {
        assignedTeamBegin = this->team2StartID;
        assignedTeamEnd = this->slots.size();
    }
    debugOutLevel(Debug::DebugLevels::networking + 5, "searching for free slot from", assignedTeamBegin, "to", assignedTeamEnd);

    for (size_t position = assignedTeamBegin; position < assignedTeamEnd; ++position)
    {
        if (not this->slots[position].isInUse.value)
        {
            debugOutLevel(20, "connection", position, " ", systemAddress_.ToString(true));

            this->slots[position].playerName.value = L"Connecting...";
            this->slots[position].systemAddress = systemAddress_;
            this->slots[position].identificationKey = identificationKey;
            debugOutLevel(15, "Created identificationKey:", this->slots[position].identificationKey);

            // save
            this->newPlayerSlot = position;
            break;
        }
    }

    // Make the first connected player the host
    /*if (this->getPlayerCount() == 0)
    {
        this->host = newPlayer;
    }*/

    return static_cast<unsigned int>(this->newPlayerSlot);
}

// ----------------------------------------------------------------------------
void PlayerCollectionServer::removeDisconnectedPlayers(RakNet::SystemAddress disconnectedSystemAddress)
{
    debugOutLevel(20, "removeDisconnectedPlayers: ", disconnectedSystemAddress.ToString(true));
    for (unsigned int position = 0; position < this->slots.size(); ++position)
    {
        if (this->slots[position].isInUse.value)
        {
            debugOutLevel(20,
                          "found",
                          irr::core::stringw(this->slots[position].playerName.value.C_String()),
                          this->slots[position].systemAddress.ToString(true));
            if (this->slots[position].systemAddress == disconnectedSystemAddress)
            {
                const RakNet::RakWString playerName = this->slots[position].playerName.value;
                debugOutLevel(11, "Remove Player", irr::core::stringw(playerName.C_String()));
                this->slots[position] = BattleTanks::Networking::SlotInfos();

                // if noone is left connected the server will buffer up the serialize() calls and
                // the next connecting player will get old messages
                // -> only do serialization-changing stuff if there is someone to receive them
                if (this->getPlayerCount() > 0)
                {
                    this->slots[position].isInUse.serialize = true;
                }

                this->printPlayerMap();
                return;
            }
        }
    }
    this->printPlayerMap();
    Error::errContinue("Cant find/remove Player with Address '",
                       disconnectedSystemAddress.ToString(true),
                       "' -> game will Collapse later");
}

unsigned int PlayerCollectionServer::slotIDFromSystemAddress(const RakNet::SystemAddress address)
{
    if (address == RakNet::UNASSIGNED_SYSTEM_ADDRESS)
    {
        return static_cast<unsigned int>(-1);
    }
    for (size_t i = 0; i < this->slots.size(); i++)
    {
        if (this->slots[i].systemAddress == address)
        {
            return static_cast<unsigned int>(i);
        }
    }
    return static_cast<unsigned int>(-1);
}

// ----------------------------------------------------------------------------
unsigned int PlayerCollectionServer::getPlayerCount(const TeamID team) const
{
    unsigned int count = 0;
    // starting from the correct slots makes counting easier
    const unsigned int start = (team == TeamID::AnyTeam or team == TeamID::InTeam1) ?
        0 :
        static_cast<unsigned int>(this->team2StartID);
    const unsigned int end = (team == TeamID::InTeam1) ? static_cast<unsigned int>(this->team2StartID) :
                                                         static_cast<unsigned int>(this->slots.size());

    for (size_t i = start; i < end; i++)
    {
        if (this->slots[i].isInUse.value)
        {
            count++;
        }
    }
    return count;
}

unsigned int PlayerCollectionServer::getFreeSlots(const TeamID team) const
{
    unsigned int count = 0;
    // starting from the correct slots makes counting easier
    const unsigned int start = (team == TeamID::AnyTeam or team == TeamID::InTeam1) ?
        0 :
        static_cast<unsigned int>(this->team2StartID);
    const unsigned int end = (team == TeamID::InTeam1) ? static_cast<unsigned int>(this->team2StartID) :
                                                         static_cast<unsigned int>(this->slots.size());

    for (size_t i = start; i < end; i++)
    {
        if (not this->slots[i].isInUse.value)
        {
            count++;
        }
    }
    return count;
}

void PlayerCollectionServer::setAllowSlotChanges(const bool val)
{
    this->allowSlotChanges = val;
}

bool PlayerCollectionServer::allPlayersReady() const
{
    for (size_t i = 0; i < this->slots.size(); i++)
    {
        // if any used slot isn't ready don't start the game
        if (this->slots[i].isInUse.value and not this->slots[i].ready.value)
        {
            return false;
        }
    }
    return true;
}

void PlayerCollectionServer::setAllPlayerIDs()
{
    size_t playerID = 0;
    for (auto& slot : slots)
    {
        // if any used slot isn't ready don't start the game
        if (slot.isInUse.value)
        {
            slot.connection->setPlayerID(playerID);
            slot.playerID.serialize = true;
            slot.playerID.value = playerID++;
        }
    }
}
/******************************************************************************
 *                              RakNet functions                              *
 ******************************************************************************/

// ----------------------------------------------------------------------------
void PlayerCollectionServer::SerializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                                           RakNet::Connection_RM3* /* destinationConnection */)
{
    // packet layout: (needs to keep in sync with network/client/playerCollectionReplica!)
    // 1) unsinged int: number of slots the server has
    // 2) unsigned int = N: number of used slots
    // 3) N x:
    //     3.1) unsigned int: slot id of used slot
    //     3.2) RakNet::RakWString: name of player at slot i
    //     3.3) bool: isReadyToStartGame flag of player at slot i

    debugOutLevel(25, "SerializeConstructionExisting");

    constructionBitstream[0].Write(static_cast<uint32_t>(this->slots.size()));
    constructionBitstream[0].Write(static_cast<uint32_t>(this->getPlayerCount(TeamID::AnyTeam)));

    // Send Slots with playerName and ready flag
    for (size_t position = 0; position < this->slots.size(); position++)
    {
        debugOutLevel(15, this->slots[position].playerName.value.C_String());
        if (this->slots[position].isInUse.value)
        {
            constructionBitstream[0].Write(static_cast<uint32_t>(position));
            constructionBitstream[0].Write(this->slots[position].playerName.value);
            constructionBitstream[0].Write(this->slots[position].ready.value);
        }
    }
}

// ----------------------------------------------------------------------------
void PlayerCollectionServer::DeserializeConstructionExisting(RakNet::BitStream* constructionBitstream,
                                                             RakNet::Connection_RM3* sourceConnection)
{
    // TODO: do we have to check that a player calls deserializeConstructionExisting only once and
    // not multiple times?

    // packet layout: (needs to keep in sync with network/client/playerCollectionReplica!)
    // 1) RakNet::RakWString: name of the new player

    debugOutLevel(25, "DeserializeConstructionExisting");

    RakNet::RakWString newPlayerName = L" ";
    constructionBitstream[0].Read(newPlayerName);

    // this->newPlayerSlot was determined by this->addNewPlayer upon the connection request by the
    // player
    this->slots[this->newPlayerSlot].playerName.value = newPlayerName;
    this->slots[this->newPlayerSlot].isInUse.value = true;
    static_cast<ConnectionBT*>(sourceConnection)->setPlayerSlot(this->newPlayerSlot);
    this->slots[this->newPlayerSlot].connection = static_cast<ConnectionBT*>(sourceConnection);

    debugOutLevel(20, "playerName:", newPlayerName.C_String());

    // serialize the new player upon the next serialization run
    this->slots[this->newPlayerSlot].isInUse.serialize = true;
    this->slots[this->newPlayerSlot].ready.serialize = true;
    this->slots[this->newPlayerSlot].playerName.serialize = true;

    this->printPlayerMap();
}

// ----------------------------------------------------------------------------
RakNet::RM3SerializationResult PlayerCollectionServer::Serialize(RakNet::SerializeParameters* serializeParameters)
{
    // packet layout: (needs to keep in sync with network/client/playerCollectionReplica!)
    // 1) unsigned int: slot id of the player receiving the packet
    // 2) unsigned int = N: number of updated slots
    // N x:
    //     2.1) unsigned int: slot id which information follows
    //     2.2) bool: is the slot in use
    //     if true:
    //         2.2.1) bool: ready for this player
    //         2.2.2) bool: player ID changed?
    //         if true:
    //             2.2.2.1) uint32_t new player ID
    //         2.2.3) bool: name changed?
    //         if true:
    //             2.2.3.1) RakNet::RakWString new name

    size_t slotsToSerialize = 0;
    for (size_t i = 0; i < this->slots.size(); i++)
    {
        if (this->slots[i].ready.serialize or this->slots[i].playerName.serialize or
            this->slots[i].isInUse.serialize or this->slots[i].playerID.serialize)
        {
            slotsToSerialize++;
        }
    }

    if (slotsToSerialize <= 0)
    {
        return RakNet::RM3SR_DO_NOT_SERIALIZE;
    }

    debugOutLevel(Debug::DebugLevels::networking, "Serialize: updating", slotsToSerialize, "slots");
    serializeParameters->outputBitstream[0].Write(static_cast<unsigned int>(
        static_cast<ConnectionBT*>(serializeParameters->destinationConnection)->getPlayerSlot()));
    serializeParameters->outputBitstream[0].Write(static_cast<unsigned int>(slotsToSerialize));

    for (unsigned int slot = 0; slot < static_cast<unsigned int>(this->slots.size()); slot++)
    {
        if (this->slots[slot].ready.serialize or this->slots[slot].playerName.serialize or
            this->slots[slot].isInUse.serialize or this->slots[slot].playerID.serialize)
        {
            debugOutLevel(Debug::DebugLevels::networking + 2, "Serialize: updating slot", slot);
            serializeParameters->outputBitstream[0].Write(slot);
            serializeParameters->outputBitstream[0].Write(this->slots[slot].isInUse.value);
            if (this->slots[slot].isInUse.value) // if the slot isn't in use but got serialized the
                                                 // client will replace it with an empty slot
            {
                serializeParameters->outputBitstream[0].Write(this->slots[slot].ready.value);
                serializeParameters->outputBitstream[0].Write(this->slots[slot].playerID.serialize); // signal the client if the name changed
                if (this->slots[slot].playerID.serialize)
                {
                    debugOutLevel(Debug::DebugLevels::networking,
                                  "serializing updated player ID for slot",
                                  slot,
                                  this->slots[slot].playerID.value);
                    serializeParameters->outputBitstream[0].Write(this->slots[slot].playerID.value);
                }
                serializeParameters->outputBitstream[0].Write(this->slots[slot].playerName.serialize); // signal the client if the name changed
                if (this->slots[slot].playerName.serialize)
                {
                    debugOutLevel(Debug::DebugLevels::networking,
                                  "serializing updated name for slot",
                                  slot,
                                  this->slots[slot].playerName.value);
                    serializeParameters->outputBitstream[0].Write(this->slots[slot].playerName.value);
                }
            }

            debugOutLevel(11,
                          "Send for Slot",
                          slot,
                          ": ",
                          this->slots[slot].playerID.value,
                          irr::core::stringw(this->slots[slot].playerName.value.C_String()),
                          this->slots[slot].ready.value ? "ready" : "unready");
        }
        // if the slot needed serialization that was done -> reset all flags
        this->slots[slot].isInUse.serialize = false;
        this->slots[slot].ready.serialize = false;
        this->slots[slot].playerName.serialize = false;
        this->slots[slot].playerID.serialize = false;
    } // for (unsigned int slot = 0; slot < static_cast<unsigned int>(this->slots.size()); slot++)

    // everyone gets the same data
    return RakNet::RM3SR_BROADCAST_IDENTICALLY_FORCE_SERIALIZATION;
}

// ----------------------------------------------------------------------------
void PlayerCollectionServer::Deserialize(RakNet::DeserializeParameters* deserializeParameters)
{
    // packet layout: (needs to keep in sync with network/client/playerCollectionReplica!)
    // 1) bool, ready
    // 2) unsigned int requested slot (== current slot if no change wanted)

    debugOutLevel(11, "Start Deserialize");

    size_t playerSlot = static_cast<ConnectionBT*>(deserializeParameters->sourceConnection)->getPlayerSlot(); // defined here in case the player changes slots and the slot is later used

    // TODO: disallow changes to this if a gamestate-change is in progress
    bool ready;
    deserializeParameters->serializationBitstream[0].Read(ready);
    debugOutLevel(11, "Set ReadyToStartGame Flag of slot", playerSlot, " to '", ready ? "ready'" : "unready'");
    // did the value change?
    if (this->slots[playerSlot].ready.value != ready)
    {
        this->slots[playerSlot].ready.value = ready;
        this->slots[playerSlot].ready.serialize = true;
        this->printPlayerMap();
    }

    debugOutLevel(11, "Deserialize: SlotRequest");

    // Buffers to store received data
    size_t requestSlot;

    unsigned int requestSlotRead;
    deserializeParameters->serializationBitstream[0].Read(requestSlotRead);
    requestSlot = static_cast<size_t>(requestSlotRead);

    if (requestSlot >= this->slots.size()) // prevent wrong array access
    {
        debugOutLevel(Debug::DebugLevels::networking + 2,
                      "player",
                      playerSlot,
                      "requested change to slot",
                      requestSlot,
                      "which is bigger than the array size of",
                      this->slots.size());
        return;
    }

    // does the player want to move (but don't allow to/from any ready slots)
    if (playerSlot != requestSlot and this->allowSlotChanges and
        not this->slots[playerSlot].ready.value and not this->slots[requestSlot].ready.value)
    {
        if (this->slots[requestSlot].isInUse.value)
        {
            // TODO: find a method to signal the other player that someone wants to switch slots,
            // preferably in a rate-limited way.
        }
        else
        {
            debugOutLevel(11, "switching slot", playerSlot, "with slot", requestSlot);
            static_cast<ConnectionBT*>(deserializeParameters->sourceConnection)->setPlayerSlot(requestSlot);
            this->slots[requestSlot] = this->slots[playerSlot];
            // update all information for the request slot
            this->slots[requestSlot].playerName.serialize = true;
            this->slots[requestSlot].isInUse.serialize = true;
            this->slots[requestSlot].ready.serialize = true;
            // reset the slot the player was before
            this->slots[playerSlot] = BattleTanks::Networking::SlotInfos();
            // only need to serialize the isInUse variable because the client will replace that
            // slotInfo with a default-constructed one if isInUse deserializes to false
            this->slots[playerSlot].isInUse.serialize = true;

            // update the playerSlot variable in case another deserialize step uses it
            playerSlot = requestSlot;

            // Also just for debug!
            this->printPlayerMap();
        }
    }
    else
    {
        debugOutLevel(Debug::DebugLevels::networking + 1,
                      "not handling slot change request by",
                      playerSlot,
                      "to",
                      requestSlot,
                      "because 1) slots might be the same 2)",
                      this->slots[playerSlot].ready.value ? "player is ready" : "player isn't ready",
                      "3)",
                      this->slots[requestSlot].ready.value ? " request slot is ready" : "request slot isn't ready",
                      "4) slot changes are",
                      this->allowSlotChanges ? "allowed" : "forbidden");
    }

    debugOutLevel(11, "End Deserialize");
}

// ----------------------------------------------------------------------------
RakNet::RM3QuerySerializationResult
PlayerCollectionServer::QuerySerialization(RakNet::Connection_RM3* /* destinationConnection */)
{
    // the server must ALWAYS return 'RM3QSR_CALL_SERIALIZE' for all destinationConnections even if
    // there is nothing to serialize and Serialize() returns 'RakNet::RM3SR_DO_NOT_SERIALIZE'
    // otherwise only the first connected client will get the data.

    return RakNet::RM3QuerySerializationResult::RM3QSR_CALL_SERIALIZE;
}

/**************************************************************
 * private memberfunctions
 **************************************************************/

// ----------------------------------------------------------------------------
void PlayerCollectionServer::printPlayerMap() // Also just for debug!
{
    for (unsigned int position = 0; position < this->slots.size(); ++position)
    {
        debugOutLevel(20,
                      position,
                      ": ",
                      irr::core::stringw(this->slots[position].playerName.value.C_String()),
                      this->slots[position].ready.value ? "R " : " | ",
                      this->slots[position].systemAddress.ToString(true));
    }
}

// ----------------------------------------------------------------------------
