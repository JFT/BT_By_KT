/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NETWORKSERVER_H
#define NETWORKSERVER_H

#include <thread>

#if LIBCAT_SECURITY == 1
#include <raknet/SecureHandshake.h> // Include header for secure handshake
#endif

#if defined(_CONSOLE_2)
#include <raknet/Console2SampleIncludes.h>
#endif

#include "../network.h"

#ifdef _CONSOLE_2
_CONSOLE_2_SetSystemProcessParams
#endif

    class Game;

// ----------------------------------------------------------------------------
class NetworkServer : public Network
{
   public:
    explicit NetworkServer(Game* const game);
    NetworkServer(const NetworkServer&) = delete;
    NetworkServer operator=(const NetworkServer&) = delete;
    virtual ~NetworkServer();

    /// @brief hold this servers advertisement information as send out to connecting clients
    ServerInfos thisServer;

    bool init();

    void consoleListener();

    /// @brief signal all connected clients to start/resume or pause the game
    /// @param request request telling the clients what to do (e.g. how long to wait [minus ping/2]
    /// before resuming)
    ///
    /// does nothing if no connected clients are there.
    void requestStartPauseOrResumeGame(const PauseRequest request);

   private:
    Game* const game;

    // ConsoleListener thread
    std::thread consoleThread = std::thread();
};

#endif // NETWORKSERVER_H
