/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gameNetwork_server.h"

// ----------------------------------------------------------------------------
#include <raknet/RakPeerInterface.h>

#include "../network_server.h"
#include "../playerCollectionReplica_server.h" // only forward declared in game.h
#include "../../../core/game.h"

// ----------------------------------------------------------------------------
GameNetworkServer::GameNetworkServer(Game* const game_)
    : NetworkStateBase(game_->getNetwork()->getPeer())
    , game(game_)
//, chatReplica(new ChatReplica(game->getNetwork()->getReplicaManager()))
{
    peer->SetTimeoutTime(60000, RakNet::UNASSIGNED_SYSTEM_ADDRESS); // SetTimeoutTime of 60 sec for
                                                                    // the game;
                                                                    // UNASSIGNED_SYSTEM_ADDRESS
                                                                    // means use TimeoutTime for all
                                                                    // systems
    peer->SetUnreliableTimeout(0);
}

// ---------------------------------------------------------------------------
GameNetworkServer::~GameNetworkServer() {}

ErrCode GameNetworkServer::init()
{
    return ErrCodes::NO_ERR;
}

// ---------------------------------------------------------------------------
void GameNetworkServer::processPackets()
{
    // Get all packets that were send to us until now
    for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
    {
        // We got a packet, get the identifier with our handy function
        const unsigned char packetIdentifier = game->getNetwork()->GetPacketIdentifier(packet);
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "packet type",
                      game->getNetwork()->packetIdentifierToString(packetIdentifier),
                      "from",
                      packet->systemAddress.ToString(true));

        switch (packetIdentifier)
        {
            case ID_DISCONNECTION_NOTIFICATION:
            {
                // Connection lost normally
                debugOutLevel(15, "ID_DISCONNECTION_NOTIFICATION from ", packet->systemAddress.ToString(true));
            }
            break;
            case ID_NEW_INCOMING_CONNECTION:
            {
                // Somebody connected.  We have their IP now
                debugOutLevel(15,
                              "ID_NEW_INCOMING_CONNECTION from",
                              packet->systemAddress.ToString(true),
                              "with GUID",
                              packet->guid.ToString());

                // TODO: find out why there are 2 internal IDs and both are bound to the same client
                debugOutLevel(15, "Remote internal IDs:");
                for (int index = 0; index < MAXIMUM_NUMBER_OF_INTERNAL_IDS; index++)
                {
                    RakNet::SystemAddress internalId =
                        game->getNetwork()->getPeer()->GetInternalID(packet->systemAddress, index);
                    if (internalId != RakNet::UNASSIGNED_SYSTEM_ADDRESS)
                    {
                        printf("%i. %s\n", index, internalId.ToString(true));
                    }
                }
            }
            break;
            case ID_CONNECTED_PING:
            {
                debugOutLevel(15, "received ping");
            }
            case ID_CONNECTION_LOST:
            {
                // Couldn't deliver a reliable packet - i.e. the other system was abnormally
                // terminated
                debugOutLevel(15, "ID_CONNECTION_LOST from", packet->systemAddress.ToString(true));
            }
            break;
        } // switch (packetIdentifier)
    }     // for ( packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet =
          // peer->Receive())
}

/**************************************************************
 * private memberfunctions
 **************************************************************/
