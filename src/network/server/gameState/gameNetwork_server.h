/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GAMENETWORKSERVER_H
#define GAMENETWORKSERVER_H

#include "../../networkStateBase.h"

class Game;

// ----------------------------------------------------------------------------
class GameNetworkServer : public NetworkStateBase
{
   public:
    explicit GameNetworkServer(Game* const game_);
    GameNetworkServer(const GameNetworkServer&) = delete;
    GameNetworkServer operator=(const GameNetworkServer&) = delete;
    virtual ~GameNetworkServer();

    virtual ErrCode init();

    void processPackets();

    // inline ChatReplica* getChatReplica() const {return this->chatReplica;};

   private:
    Game* const game;

    // Replica for sending chat messages
    // ChatReplica* const chatReplica = nullptr;
};

#endif // GAMENETWORKSERVER_H
