/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lobbyNetwork_server.h"

// ----------------------------------------------------------------------------
#include <raknet/RakPeerInterface.h>

#include "chatReplica_server.h"
#include "../network_server.h"
#include "../playerCollectionReplica_server.h" // only forward declared in game.h
#include "../../customPackets.h"               // defines struct ServerInfos
#include "../../defaultPorts.h"
#include "../../../core/config.h"
#include "../../../core/game.h"
#include "../../../states/multipleStatesConstants.h" // defines timeout time (should be the same for client and server)

// ----------------------------------------------------------------------------
LobbyNetworkServer::LobbyNetworkServer(Game* const game_)
    : NetworkStateBase(game_->getNetwork()->getPeer())
    , game(game_)
    , chatReplica(new ChatReplicaServer(game->getNetwork()->getReplicaManager(), game->getPlayerCollectionReplica()))
{
    peer->SetTimeoutTime(BattleTanks::MultipleStatesConstants::ServerBrowserTimeoutTime,
                         RakNet::UNASSIGNED_SYSTEM_ADDRESS);
    // needed this tiny SetTimeoutTime of 0,2 sec for fast ConnectionState adjustment from
    // IS_SILENTLY_DISCONNECTING to IS_DISCONNECTED; UNASSIGNED_SYSTEM_ADDRESS means use TimeoutTime
    // for all systems
    // TODO: check if the above comment actually makes sense. Because the timeout also applies to
    // connection attempts a client with ping > 200ms could NEVER connect to ANY server.
    peer->SetUnreliableTimeout(1000);
}

// ---------------------------------------------------------------------------
LobbyNetworkServer::~LobbyNetworkServer()
{
    delete this->chatReplica;
}

void LobbyNetworkServer::advertiseToLAN()
{
    static RakNet::TimeMS lastAdvertisement = RakNet::GetTimeMS();

    if (RakNet::GetTimeMS() - lastAdvertisement < this->advertiseToLANInterval)
    {
        return;
    }
    debugOutLevel(Debug::DebugLevels::networking + 1,
                  "advertising to broadcast on port",
                  BattleTanks::Networking::advertisePortLAN);
    peer->Ping("255.255.255.255",
               BattleTanks::Networking::advertisePortLAN,
               false); // TODO: find out how to broadcast on ipv6
    // Hannes F.: on my machine if there is no network connection pinging broadcast
    // (255.255.255.255)
    // doesn't work. To allow local testing with two clients the server needs to advertise on
    // loopback.
    peer->Ping("127.0.0.1",
               BattleTanks::Networking::advertisePortLAN,
               false); // TODO: find out how to broadcast on ipv6*/

    lastAdvertisement = RakNet::GetTimeMS();
}

// ---------------------------------------------------------------------------
void LobbyNetworkServer::processPackets()
{
    // Get all packets that were send to us until now
    for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
    {
        // We got a packet, get the identifier with our handy function
        const unsigned char packetIdentifier = this->game->getNetwork()->GetPacketIdentifier(packet);
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "packet type",
                      this->game->getNetwork()->packetIdentifierToString(packetIdentifier),
                      "from",
                      packet->systemAddress.ToString(true));

        switch (packetIdentifier)
        {
            case ID_CONNECTION_LOST:            // sudden unexpected disconnect
            case ID_DISCONNECTION_NOTIFICATION: // Connection lost normally
            {
                debugOutLevel(15,
                              "ID_DISCONNECTION_NOTIFICATION or ID_CONNECTION_LOST from",
                              packet->systemAddress.ToString(true));
                // display a message that the player disconnected
                const size_t slotID =
                    this->game->getPlayerCollectionReplica()->slotIDFromSystemAddress(packet->systemAddress);
                if (slotID < this->game->getPlayerCollectionReplica()->getSlots().size())
                {
                    if (this->game->getPlayerCollectionReplica()->getPlayerCount() > 1)
                    {
                        // if the last player leaves there will be noone to receive the leaving
                        // message and it will be serialized only if a new player connects -> only
                        // send that message if the player isn't the last one;
                        const RakNet::RakWString leftPlayerName =
                            this->game->getPlayerCollectionReplica()->getSlots()[slotID].playerName.value;
                        this->chatReplica->writeOnLeaveMessage(this->game->getNetwork()->thisServer.name,
                                                               leftPlayerName);
                    }
                    game->getPlayerCollectionReplica()->removeDisconnectedPlayers(packet->systemAddress);
                }
                else
                {
                    Error::errTerminate("wrong slotID of a connected client!");
                }
            }
            break;
            case ID_NEW_INCOMING_CONNECTION:
            {
                // Somebody connected.  We have their IP now
                debugOutLevel(15,
                              "ID_NEW_INCOMING_CONNECTION from",
                              packet->systemAddress.ToString(true),
                              "with GUID",
                              packet->guid.ToString());

                // TODO: find out why there are 2 internal IDs and both are bound to the same client

                debugOutLevel(15, "Remote internal IDs:");
                for (int index = 0; index < MAXIMUM_NUMBER_OF_INTERNAL_IDS; index++)
                {
                    RakNet::SystemAddress internalId =
                        game->getNetwork()->getPeer()->GetInternalID(packet->systemAddress, index);
                    if (internalId != RakNet::UNASSIGNED_SYSTEM_ADDRESS)
                    {
                        printf("%i. %s\n", index, internalId.ToString(true));
                    }
                }

                RakNet::RakWString serverName =
                    RakNet::RakWString(game->getConfiguration().getSettings().serverName.c_str());
                RakNet::RakWString serverPassword =
                    RakNet::RakWString(game->getConfiguration().getSettings().serverPassword.c_str());
                // use a true random number generator to generate the identification keys so noone
                // can guess them
                const unsigned int identificationKey = this->trueSystemRandomNumberGenerator();
                unsigned int newSlot =
                    game->getPlayerCollectionReplica()->addNewPlayer(packet->systemAddress, identificationKey);

                if (newSlot == static_cast<unsigned int>(-1))
                {
                    // there was no free slot (maybe the slot got closed just as the connection came
                    // in?)
                    peer->CloseConnection(packet->systemAddress, true);
                }
                else
                {
                    debugOutLevel(20, "ServerName: ", serverName, ", ServerPassword: ", serverPassword, ", identificationKey: ", identificationKey);

                    RakNet::BitStream bsOut;
                    bsOut.Write(static_cast<RakNet::MessageID>(ID_LOBBY_GENERAL)); // TODO find a better id
                    bsOut.Write(serverName);
                    bsOut.Write(serverPassword);
                    bsOut.Write(identificationKey);

                    game->getNetwork()->getPeer()->Send(&bsOut, HIGH_PRIORITY, UNRELIABLE, 0, packet->systemAddress, false);
                }
            }
            break;
            case CustomID::ID_START_PAUSE_OR_RESUME_GAME: // someone (might be an unconnected
                                                          // client) requests to start the game (no
                                                          // need to parse the packet because a
                                                          // request to pause or resume the game
                                                          // doesn't really make any sense and isn't
                                                          // normally send by any client in the
                                                          // lobbyState -> just treat every packet
                                                          // with that ID as a start request. The
                                                          // requset will fail if someone isn't
                                                          // ready anyway
            {
                // check if the packet actually originated from one of the connected clients
                if (game->getPlayerCollectionReplica()->allPlayersReady() and
                    this->game->getNetwork()->isConnected(packet->systemAddress))
                {
                    // also check if the client has an actual slot on the server. It could be
                    // currently connecting with unset player information.
                    const unsigned int slotID =
                        game->getPlayerCollectionReplica()->slotIDFromSystemAddress(packet->systemAddress);
                    if (slotID != static_cast<unsigned int>(-1))
                    {
                        this->clientRequestedGameStart = true;
                    }
                }
            }
            break;
            case ID_UNCONNECTED_PING:
            case ID_UNCONNECTED_PING_OPEN_CONNECTIONS:
            {
                debugOutLevel(15, "Received Ping from", packet->systemAddress.ToString(true));

                bool knownClient = false;

                // Loop through list of all known clients and add the Client if it is new
                for (unsigned int i = 0; i < this->unconnectedClientList.size(); i++)
                {
                    if (packet->systemAddress == this->unconnectedClientList[i].adr) // update filledSlots and ping
                    {
                        knownClient = true;
                        debugOutLevel(60, "Received Ping from Client: ", i + 1);
                        this->unconnectedClientList[i].stillSearchingServer = true;
                        this->unconnectedClientList[i].lastReceivedPing = RakNet::GetTimeMS();
                        break;
                    }
                }
                if (!knownClient) // we found a new client
                {
                    debugOutLevel(10, "Received Ping from new client!");
                    UnconnectedClientEntry newClient;
                    newClient.stillSearchingServer = true; // he is pinging
                    newClient.adr = packet->systemAddress;
                    newClient.lastReceivedPing = RakNet::GetTimeMS();
                    this->unconnectedClientList.push_back(newClient);
                }
            }
            break;
        }
    }
}

void LobbyNetworkServer::replyToUnconnectedClients()
{
    const RakNet::TimeMS currentTime = RakNet::GetTimeMS();
    for (size_t i = 0; i < this->unconnectedClientList.size(); i++)
    {
        if (currentTime - this->unconnectedClientList[i].lastReceivedPing > this->unconnectedClientMaxAge) // TODO: check if this actually ever is true (because GetTimeMS might return only deltas of <= 1 second
        {
            debugOutLevel(Debug::DebugLevels::updateLoop + 1,
                          "removing unconnected client",
                          this->unconnectedClientList[i].adr.ToString(true),
                          "because the last received ping was at",
                          this->unconnectedClientList[i].lastReceivedPing);
            this->unconnectedClientList.erase(this->unconnectedClientList.begin() +
                                              i); // TODO: check if this method of erasing is actually correct
        }
        else if (currentTime - this->unconnectedClientList[i].lastAdvertisementSend <
                 this->unconnectedRateLimiter.singleTargetInterval)
        {
            // only send out ServerInfos every unconnectedRateLimiter.singleTargetInterval
            // milliseconds
            debugOutLevel(Debug::DebugLevels::updateLoop,
                          "not advertising to",
                          this->unconnectedClientList[i].adr.ToString(true),
                          "because it received the last advertisement at",
                          this->unconnectedClientList[i].lastAdvertisementSend,
                          "and it's currently",
                          currentTime);
            continue;
        }
        else
        {
            if (not this->unconnectedRateLimiter.queryPerformAction(currentTime))
            {
                // if we are already rate limited there is no need to check the other clients as the
                // loop is too fast to change the rate significantly -> stop pinging anyone
                return;
            }
            this->sendAdvertiseToSystem(this->unconnectedClientList[i].adr);
            this->unconnectedClientList[i].lastAdvertisementSend = currentTime;
        }
    }
}


/**************************************************************
 * private memberfunctions
 **************************************************************/

void LobbyNetworkServer::sendAdvertiseToSystem(const RakNet::SystemAddress address)
{
    debugOutLevel(50, "sendAdvertiseToSystem");
    game->getNetwork()->thisServer.filledSlots = game->getPlayerCollectionReplica()->getPlayerCount();

    game->getNetwork()->getPeer()->AdvertiseSystem(address.ToString(false),
                                                   address.GetPort(),
                                                   reinterpret_cast<char*>(&(game->getNetwork()->thisServer)),
                                                   int(sizeof(ServerInfos)));
}
