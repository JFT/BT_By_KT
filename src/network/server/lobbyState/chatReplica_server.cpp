/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "chatReplica_server.h"

// ----------------------------------------------------------------------------
#include "../playerCollectionReplica_server.h"
#include "../../connectionbt.h"
#include "../../staticReplicaIDs.h"
#include "../../../utils/debug.h"

// ----------------------------------------------------------------------------
ChatReplicaServer::ChatReplicaServer(ReplicaManager3BT* const replicaManager,
                                     PlayerCollectionServer* const playerCollection_)
    : StaticReplicaBase(BattleTanks::Networking::StaticReplicaIDs::LobbyStateChatReplicaID, replicaManager)
    , playerCollection(playerCollection_)
{
    debugOutLevel(15, "Create ChatReplica");
}


void ChatReplicaServer::writeOnEnterMessage(const RakNet::RakWString serverName, const RakNet::RakWString playerName_) // TODO maybe set into the lobby or game state
{
    debugOutLevel(20, "writeOnEnterMessage");
    RakNet::RakWString sendMessage = serverName;
    sendMessage += RakNet::RakWString(L": ");
    sendMessage += playerName_;
    sendMessage += RakNet::RakWString(L" has entered the Lobby");

    this->writeMessage(sendMessage);
}

// ----------------------------------------------------------------------------
void ChatReplicaServer::writeOnLeaveMessage(const RakNet::RakWString serverName, RakNet::RakWString playerName_)
{
    debugOutLevel(20, "writeOnLeaveMessage");
    RakNet::RakWString sendMessage = serverName;
    sendMessage += RakNet::RakWString(L": ");
    sendMessage += playerName_;
    sendMessage += RakNet::RakWString(L" has left the Lobby");

    this->writeMessage(sendMessage);
}


/******************************************************************************
 *                              RakNet functions                              *
 ******************************************************************************/

// ----------------------------------------------------------------------------
RakNet::RM3SerializationResult ChatReplicaServer::Serialize(RakNet::SerializeParameters* serializeParameters)
{
    bool sendVariables = false;

    // Send message
    if (this->message.serialize == true)
    {
        serializeParameters->outputBitstream[0].Write(true);
        serializeParameters->outputBitstream[0].Write(this->message.value);

        sendVariables = true;
        this->message.serialize = false;

        debugOutLevel(11, "Serialize: Send message:", this->message.value);
    }
    else
    {
        serializeParameters->outputBitstream[0].Write(false);
    }

    // Check if something was changed
    if (sendVariables == true)
    {
        // serializeParameters->outputBitstream[0].Reset();
        return RakNet::RM3SR_BROADCAST_IDENTICALLY_FORCE_SERIALIZATION;
    }

    return RakNet::RM3SR_DO_NOT_SERIALIZE;
}

// ----------------------------------------------------------------------------
void ChatReplicaServer::Deserialize(RakNet::DeserializeParameters* deserializeParameters)
{
    debugOutLevel(20, "ChatReplica::Deserialize");
    // Buffer to store the info of an changed slot
    bool changedMessage;

    deserializeParameters->serializationBitstream[0].Read(changedMessage);
    if (changedMessage)
    {
        deserializeParameters->serializationBitstream[0].Read(this->messageText);
        this->message.serialize = true;
    }

    const size_t slotID =
        static_cast<ConnectionBT*>(deserializeParameters->sourceConnection)->getPlayerSlot();
    this->message.value = this->playerCollection->getSlots()[slotID].playerName.value;
    this->message.value += L": ";
    this->message.value += this->messageText;

    this->messageList.Push(this->message.value);

    debugOutLevel(11, "Deserialize: Got message:", this->message.value);
}

// ----------------------------------------------------------------------------
RakNet::RM3QuerySerializationResult ChatReplicaServer::QuerySerialization(RakNet::Connection_RM3* destinationConnection)
{
    (void) destinationConnection;

    // debugOutLevel(11, "QuerySerialization ChatReplica");
    return RakNet::RM3QSR_CALL_SERIALIZE;
}
