/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOBBYNETWORKSERVER_H
#define LOBBYNETWORKSERVER_H

#include "../../networkStateBase.h"

#include <random> // generate player identification keys
#include <vector>

#include <raknet/GetTime.h>     // defines RakNet::TimeMS
#include <raknet/RakNetTypes.h> // defines RakNet::SystemAddress

#include "../../rateLimiter.h"
#include "../../../utils/static/error.h" // defines ErrCode

// forward declarations
class Game;
class ChatReplicaServer;

// ----------------------------------------------------------------------------
class LobbyNetworkServer : public NetworkStateBase
{
   public:
    explicit LobbyNetworkServer(Game* const game_);
    LobbyNetworkServer(const LobbyNetworkServer&) = delete;
    LobbyNetworkServer operator=(const LobbyNetworkServer&) = delete;
    virtual ~LobbyNetworkServer();

    virtual ErrCode init() { return ErrCodes::NO_ERR; }
    void processPackets();

    /// @brief send pings to IPv4 broadcast on pre-defined port. Sends only every
    /// LobbyServerNetwork::advertiseToLANInterval milliseconds.
    void advertiseToLAN();

    /// @brief send server advertisements to clients in the unconnectedClientList
    void replyToUnconnectedClients();

    /// @brief set to true inside processPackets if a client requests to start the game. Don't
    /// forget to check if everyone is ready!
    bool clientRequestedGameStart = false;

    /// @brief lists the slots of new players (used to display their names in the chat)
    std::vector<unsigned int> newPlayerSlots;

    inline ChatReplicaServer* getChatReplica() const { return this->chatReplica; };

   private:
    /// variables to make sure that clients can't overload the server by repeatedly pinging it.
    /// @brief max time in milliseconds a client's ping can be old before it is removed from the
    /// unconnectedClientList. MUST be at last be as big as replyToUnconnectedClientsInterval
    /// (converted to seconds) or no advertisement will ever be send.
    static constexpr RakNet::TimeMS unconnectedClientMaxAge = 2500;
    static constexpr RakNet::TimeMS advertiseToLANInterval = 500;

    Game* const game;

    // Replica for sending chat messages
    ChatReplicaServer* const chatReplica;

    struct UnconnectedClientEntry
    {
        bool stillSearchingServer = true;
        RakNet::SystemAddress adr = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
        /// @brief when the client pinged the server
        RakNet::TimeMS lastReceivedPing = 0;
        /// @brief when the server send out the ServerInfos (name, slots)
        RakNet::TimeMS lastAdvertisementSend = 0;
    };

    std::vector<struct UnconnectedClientEntry> unconnectedClientList =
        std::vector<struct UnconnectedClientEntry>();
    void sendAdvertiseToSystem(const RakNet::SystemAddress address);

    /// @brief send our info (name, slots) to a specific client every 500ms and limit the rate total
    /// advertisements send to unconnected clients (because a simple ping to the server sends a big
    /// packet containing the serverInfos)
    RateLimiter unconnectedRateLimiter = RateLimiter(500, 60);

    RakNet::TimeMS lastAdvertiseToSystem = RakNet::GetTimeMS();

    /// @brief on systems without a system true random number generator cration will fail. Maybe
    /// catch that (are there such systems anyway)?
    std::random_device trueSystemRandomNumberGenerator;
};

#endif // LOBBYNETWORKSERVER_H
