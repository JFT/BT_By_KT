/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CHATREPLICASERVER_H
#define CHATREPLICASERVER_H

#include <raknet/DS_List.h>
#include <raknet/ReplicaManager3.h> // defines RakNet::Replica3

#include "../../serializeVar.h"
#include "../../staticReplicaBase.hpp"

class ReplicaManager3BT;
class PlayerCollectionServer;

// ----------------------------------------------------------------------------
class ChatReplicaServer : public BattleTanks::Networking::StaticReplicaBase
{
   public:
    ChatReplicaServer(ReplicaManager3BT* const replicaManager, PlayerCollectionServer* const playerCollection);

    // Getter & Setter
    void writeMessage(RakNet::RakWString message_)
    {
        this->message.value = message_;
        this->message.serialize = true;
    };

    /// @brief write a message if a player joins
    void writeOnEnterMessage(const RakNet::RakWString serverName, const RakNet::RakWString playerName);
    void writeOnLeaveMessage(const RakNet::RakWString serverName, const RakNet::RakWString playerName);


    DataStructures::List<RakNet::RakWString> getMessageList() { return this->messageList; }

    // ----[ Replica functions ]--------------------------------

    // Serialization
    RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* serializeParameters) override;

    void Deserialize(RakNet::DeserializeParameters* deserializeParameters) override;

    RakNet::RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3* destinationConnection) override;

   private:
    PlayerCollectionServer* playerCollection; // needed to get the names of the players who send us
                                              // the message (otherwise the client could just claim
                                              // it was send by someone else)

    // ----[ Non-serializable variables ]-----------------------

    const RakNet::RakString replicaName = "ChatReplica";

    DataStructures::List<RakNet::RakWString> messageList;

    // ----[ Serializable variables ]---------------------------
    RakNet::RakWString messageText = L"-";
    serializeVar<RakNet::RakWString> message{""};
};


#endif // CHATREPLICASERVER_H
