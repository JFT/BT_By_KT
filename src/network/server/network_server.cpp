/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "network_server.h"

// ----------------------------------------------------------------------------
#include <raknet/RakPeerInterface.h>
#include <raknet/RakSleep.h>

#include "../../core/config.h"
#include "../../core/game.h"
#include "../../utils/static/conversions.h"

// ----------------------------------------------------------------------------
NetworkServer::NetworkServer(Game* const game_)
    : Network(game_)
    , game(game_)
{
}

// ----------------------------------------------------------------------------
NetworkServer::~NetworkServer()
{
    // TODO(mario): Joining threads in destructor is probably bad Mi 2016/03/02
    this->consoleThread.join();
    debugOutLevel(10, "destructing NetworkServer");
}

// ----------------------------------------------------------------------------
bool NetworkServer::init()
{
    // setup ReplicaManager3
    this->initialized = Network::init();

    const std::string serverName = this->game->getConfiguration().getSettings().serverName;
    for (size_t i = 0; i < ServerInfos::serverMaxNameLength - 1; i++)
    {
        if (i >= serverName.size())
        {
            this->thisServer.name[i] = '\0';
            break;
        }
        this->thisServer.name[i] = serverName.c_str()[i];
    }
    // always make sure the char array ends in a 0-byte
    this->thisServer.name[ServerInfos::serverMaxNameLength - 1] = '\0';

    // number of incoming connections. For Client this is 0. If this is higher than allowed number
    // by the peer then this will be reduced. If it is smaller than number of connected clients,
    // no more clients can join
    peer->SetMaximumIncomingConnections(game->getConfiguration().getSettings().numberOfSlots);
    // set a password if the config defines one
    std::string serverPassword = game->getConfiguration().getSettings().serverPassword;
    if (serverPassword.size() > 0)
    {
        peer->SetIncomingPassword(game->getConfiguration().getSettings().serverPassword.c_str(),
                                  game->getConfiguration().getSettings().serverPassword.size());
        this->thisServer.passwordProtected = true;
    }
    else
    {
        peer->SetIncomingPassword(game->getConfiguration().defaultSettings.serverPassword.c_str(),
                                  game->getConfiguration().defaultSettings.serverPassword.size());
        this->thisServer.passwordProtected = false;
    }

    //	RakNet::PacketLogger packetLogger;
    //	server->AttachPlugin(&packetLogger);

#if LIBCAT_SECURITY == 1
    cat::EasyHandshake handshake;
    char public_key[cat::EasyHandshake::PUBLIC_KEY_BYTES];
    char private_key[cat::EasyHandshake::PRIVATE_KEY_BYTES];
    handshake.GenerateServerKey(public_key, private_key);
    peer->InitializeSecurity(public_key, private_key, false);
    FILE* fp = fopen("publicKey.dat", "wb");
    fwrite(public_key, sizeof(public_key), 1, fp);
    fclose(fp);
#endif


    debugOutLevel(15, "========== [ Starting server... ] ========");
    debugOutLevel(15, "ServerName: ", this->thisServer.name);

    // 0 means we don't care about a connectionValidationInteger, and false
    // for low priority threads
    // I am creating two socketDesciptors, to create two sockets. One using IPV6 and the other IPV4
    socketDescriptor = new RakNet::SocketDescriptor(game->getConfiguration().getSettings().serverPort, nullptr);
    socketDescriptor->socketFamily = AF_INET; // IPV4

    // startup socket
    // TODO: is Threadpriority=1 ok?
    if (int startupResult = peer->Startup(this->thisServer.maxSlots, socketDescriptor, 1, 1))
    {
        Error::errContinue("Error starting up socket. RakNet::StartupResult = ", startupResult);
        this->initialized = false;
    }

    for (unsigned int i = 0; i < peer->GetNumberOfAddresses(); i++)
    {
        RakNet::SystemAddress address = peer->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
        debugOutLevel(15, "My IP Address", i + 1, ": ", address.ToString(true), ", IPV ", address.GetIPVersion());
    }

    debugOutLevel(15,
                  "My GUID is",
                  peer->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS).ToString());

    debugOutLevel(15, "========== [ Available Commands: ] =============");
    debugOutLevel(15, "-p", "Ping");
    debugOutLevel(15, "-shutdown", "Shutdown the RakPeerInterface");
    debugOutLevel(15, "-s", "Show statistics");
    debugOutLevel(15, "========== [ Type Command ] =============");

    this->thisServer.address =
        peer->GetSystemAddressFromGuid(peer->GetMyGUID()); // care needed to init peer before used

    // Start listening console input
    this->consoleThread = std::thread(&NetworkServer::consoleListener, this);

    return this->initialized;
}

// ----------------------------------------------------------------------------
void NetworkServer::consoleListener()
{
    while (this->running)
    {
        RakSleep(400);
        std::wstring userInput;
        std::getline(std::wcin, userInput);

        //isCommand(userInput);//TODO: maybe use python console to parse commands?
    }
}

void NetworkServer::requestStartPauseOrResumeGame(const PauseRequest request)
{
    this->peer->Send(reinterpret_cast<const char*>(&request),
                     sizeof(PauseRequest),
                     PacketPriority::IMMEDIATE_PRIORITY,
                     PacketReliability::RELIABLE,
                     0,                              // ordering channel
                     RakNet::UNASSIGNED_RAKNET_GUID, // exclude no connected client from the
                                                     // broadcast
                     true);                          // sending to all connected clients
}
