/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLOBALDEFINE_H_G0XVFONM
#define GLOBALDEFINE_H_G0XVFONM


// define a global boolean for network objects that need to know
// whether they are running on a server or a client

namespace GlobalBools
{
#if defined MAKE_SERVER_
    constexpr bool systemIsServer = true;
#else
    constexpr bool systemIsServer = false;
#endif
} // namespace GlobalBools

#endif /* end of include guard: GLOBALDEFINE_H_G0XVFONM */
