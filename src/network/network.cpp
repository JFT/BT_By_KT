/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "network.h"

// ----------------------------------------------------------------------------
#include <raknet/NetworkIDManager.h>
#include <raknet/RakPeerInterface.h>

#include "replicamanager3bt.h"
#include "../core/game.h"

// ----------------------------------------------------------------------------
Network::Network(Game* game_)
    : peer(RakNet::RakPeerInterface::GetInstance())
    , game(game_)
{
    // ctor
}

// ----------------------------------------------------------------------------
Network::~Network()
{
    peer->Shutdown(300);

    // We're done with the network
    RakNet::RakPeerInterface::DestroyInstance(peer);

    if (this->replicaManager != nullptr)
    {
        delete this->replicaManager;
    }

    RakNet::NetworkIDManager::DestroyInstance(this->networkIDManager);

    if (this->socketDescriptor != nullptr)
    {
        delete this->socketDescriptor;
    }

    if (rss)
    {
        delete rss;
    }
}

// ----------------------------------------------------------------------------
bool Network::init()
{
    debugOutLevel(20, "Initializing Network Base");

    this->replicaManager = new ReplicaManager3BT(this->game); // TODO check why not in cdor
    this->networkIDManager = new RakNet::NetworkIDManager();
    peer->SetOccasionalPing(true);
    peer->AttachPlugin(replicaManager);
    replicaManager->SetNetworkIDManager(networkIDManager);
    replicaManager->SetDefaultOrderingChannel(0);
    replicaManager->SetDefaultPacketPriority(PacketPriority::HIGH_PRIORITY);
    replicaManager->SetDefaultPacketReliability(PacketReliability::RELIABLE_ORDERED_WITH_ACK_RECEIPT);
    replicaManager->SetAutoSerializeInterval(0); // We want to control the serialization ourselves.
                                                 // - But doesn't work yet..Right now Serialize is
                                                 // called, when Receive is called
    replicaManager->SetAutoManageConnections(true, true);

    return true;
}

// ----------------------------------------------------------------------------
bool Network::isInitialized()
{
    return this->initialized;
}

// ----------------------------------------------------------------------------
bool Network::isConnected(RakNet::SystemAddress systemAdress_) const
{
    const RakNet::ConnectionState connectionState = this->peer->GetConnectionState(systemAdress_);
    if (connectionState == RakNet::ConnectionState::IS_CONNECTED)
    {
        return true;
    }
    else
    {
#pragma GCC diagnostic ignored "-Wswitch"
        switch (connectionState)
        {
            case RakNet::ConnectionState::IS_PENDING:
            {
                debugOutLevel(15, "SystemAddress '", systemAdress_.ToString(true), "': ", "The Connection-process hasn't started yet");
            }
            break;
            case RakNet::ConnectionState::IS_CONNECTING:
            {
                debugOutLevel(15, "SystemAddress '", systemAdress_.ToString(true), "': ", "Processing the connection attempt.");
            }
            break;
            case RakNet::ConnectionState::IS_DISCONNECTING:
            {
                debugOutLevel(15,
                              "SystemAddress '",
                              systemAdress_.ToString(true),
                              "': ",
                              "Was connected, but will disconnect as soon as the remaining "
                              "messages are delivered.");
            }
            break;
            case RakNet::ConnectionState::IS_SILENTLY_DISCONNECTING:
            {
                debugOutLevel(15, "SystemAddress '", systemAdress_.ToString(true), "': ", "A connection attempt failed and will be aborted.");
            }
            break;
            case RakNet::ConnectionState::IS_DISCONNECTED:
            {
                debugOutLevel(15, "SystemAddress '", systemAdress_.ToString(true), "': ", "No longer connected.");
            }
            break;
            case RakNet::ConnectionState::IS_NOT_CONNECTED:
            {
                debugOutLevel(15,
                              "SystemAddress '",
                              systemAdress_.ToString(true),
                              "': ",
                              "Was never connected, or else was disconnected long enough ago that "
                              "the entry has been discarded. ");
            }
            break;
        }
    }
#pragma GCC diagnostic pop
    return false;
}

void Network::setCircularBufferSize(uint32_t newSize)
{
    this->bufferSize = newSize;
}
// TODO: Move this to Server
void Network::setReUpdateNecessary(bool flag, uint32_t startTick)
{
    this->reUpdateNecessary = flag;
    // Catch too old commands and set them to the furthest tick ago
    if ((game->getTickNumber() - startTick) > bufferSize)
    {
        startReUpdateTick = game->getTickNumber() - bufferSize + 1;
    }
    else if (startReUpdateTick > startTick)
    {
        startReUpdateTick = startTick;
    }
    if (flag)
    {
        debugOutLevel(Debug::DebugLevels::networking,
                      "ReUpdate triggered - StartTick:",
                      startReUpdateTick,
                      "actual Tick:",
                      game->getTickNumber());
    }
}
//! NOT WORKING - causes SEGFAULTS in Deserialize: read value from bitstream
// void Network::callSerializeOnReplicas()
//{
//    //Quick hack to control when serialize is called
//    replicaManager->SetAutoSerializeInterval(0);
//    replicaManager->Update();
//    replicaManager->SetAutoSerializeInterval(-1);
//
//}

ErrCode Network::parsePauseRequest(RakNet::Packet* const packet, PauseRequest& pauseRequest) const
{
    unsigned char packetIdentifier = this->GetPacketIdentifier(packet);
    if (packetIdentifier != CustomID::ID_START_PAUSE_OR_RESUME_GAME or packet->length != sizeof(PauseRequest))
    {
        return ErrCodes::GENERIC_ERR;
    }
    pauseRequest = *(reinterpret_cast<PauseRequest*>(packet->data));
    return ErrCodes::NO_ERR;
}

unsigned char Network::GetPacketIdentifier(const RakNet::Packet* p) const
{
    if (p == nullptr)
    {
        return 255;
    }

    RakNet::BitStream myBitStream(p->data, p->length, false);
    RakNet::MessageID id;
    myBitStream.Read(id);

    if (id == ID_TIMESTAMP)
    {
        RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
        myBitStream.IgnoreBytes(sizeof(RakNet::Time));
        myBitStream.Read(id);
        return id;
    }
    else
    {
        return id;
    }
}

// ----------------------------------------------------------------------------
std::string Network::packetIdentifierToString(const unsigned char packetIdentifier_) const // TODO: maybe remove in release build
{
    //return; //TODO remove
    switch (packetIdentifier_)
    {
        case ID_CONNECTED_PING:
            return std::string("ID_CONNECTED_PING");
        case ID_UNCONNECTED_PING:
            return std::string("ID_UNCONNECTED_PING");
        case ID_UNCONNECTED_PING_OPEN_CONNECTIONS:
            return std::string("ID_UNCONNECTED_PING_OPEN_CONNECTIONS");
        case ID_CONNECTED_PONG:
            return std::string("ID_CONNECTED_PONG");
        case ID_DETECT_LOST_CONNECTIONS:
            return std::string("ID_DETECT_LOST_CONNECTIONS");
        case ID_OPEN_CONNECTION_REQUEST_1:
            return std::string("ID_OPEN_CONNECTION_REQUEST_1");
        case ID_OPEN_CONNECTION_REPLY_1:
            return std::string("ID_OPEN_CONNECTION_REPLY_1");
        case ID_OPEN_CONNECTION_REQUEST_2:
            return std::string("ID_OPEN_CONNECTION_REQUEST_2");
        case ID_OPEN_CONNECTION_REPLY_2:
            return std::string("ID_OPEN_CONNECTION_REPLY_2");
        case ID_CONNECTION_REQUEST:
            return std::string("ID_CONNECTION_REQUEST");
        case ID_REMOTE_SYSTEM_REQUIRES_PUBLIC_KEY:
            return std::string("ID_REMOTE_SYSTEM_REQUIRES_PUBLIC_KEY");
        case ID_OUR_SYSTEM_REQUIRES_SECURITY:
            return std::string("ID_OUR_SYSTEM_REQUIRES_SECURITY");
        case ID_PUBLIC_KEY_MISMATCH:
            return std::string("ID_PUBLIC_KEY_MISMATCH");
        case ID_OUT_OF_BAND_INTERNAL:
            return std::string("ID_OUT_OF_BAND_INTERNAL");
        case ID_SND_RECEIPT_ACKED:
            return std::string("ID_SND_RECEIPT_ACKED");
        case ID_SND_RECEIPT_LOSS:
            return std::string("ID_SND_RECEIPT_LOSS");
        case ID_CONNECTION_REQUEST_ACCEPTED:
            return std::string("ID_CONNECTION_REQUEST_ACCEPTED");
        case ID_CONNECTION_ATTEMPT_FAILED:
            return std::string("ID_CONNECTION_ATTEMPT_FAILED");
        case ID_ALREADY_CONNECTED:
            return std::string("ID_ALREADY_CONNECTED");
        case ID_NEW_INCOMING_CONNECTION:
            return std::string("ID_NEW_INCOMING_CONNECTION");
        case ID_NO_FREE_INCOMING_CONNECTIONS:
            return std::string("ID_NO_FREE_INCOMING_CONNECTIONS");
        case ID_DISCONNECTION_NOTIFICATION:
            return std::string("ID_DISCONNECTION_NOTIFICATION");
        case ID_CONNECTION_LOST:
            return std::string("ID_CONNECTION_LOST");
        case ID_CONNECTION_BANNED:
            return std::string("ID_CONNECTION_BANNED");
        case ID_INVALID_PASSWORD:
            return std::string("ID_INVALID_PASSWORD");
        case ID_IP_RECENTLY_CONNECTED:
            return std::string("ID_IP_RECENTLY_CONNECTED");
        case ID_TIMESTAMP:
            return std::string("ID_TIMESTAMP");
        case ID_UNCONNECTED_PONG:
            return std::string("ID_UNCONNECTED_PONG");
        case ID_ADVERTISE_SYSTEM:
            return std::string("ID_ADVERTISE_SYSTEM");
        case ID_DOWNLOAD_PROGRESS:
            return std::string("ID_DOWNLOAD_PROGRESS");
        case ID_REMOTE_DISCONNECTION_NOTIFICATION:
            return std::string("ID_REMOTE_DISCONNECTION_NOTIFICATION");
        case ID_REMOTE_CONNECTION_LOST:
            return std::string("ID_REMOTE_CONNECTION_LOST");
        case ID_REMOTE_NEW_INCOMING_CONNECTION:
            return std::string("ID_REMOTE_NEW_INCOMING_CONNECTION");
        case ID_FILE_LIST_TRANSFER_HEADER:
            return std::string("ID_FILE_LIST_TRANSFER_HEADER");
        case ID_FILE_LIST_TRANSFER_FILE:
            return std::string("ID_FILE_LIST_TRANSFER_FILE");
        case ID_DDT_DOWNLOAD_REQUEST:
            return std::string("ID_DDT_DOWNLOAD_REQUEST");
        case ID_TRANSPORT_STRING:
            return std::string("ID_TRANSPORT_STRING");
        case ID_REPLICA_MANAGER_CONSTRUCTION:
            return std::string("ID_REPLICA_MANAGER_CONSTRUCTION");
        case ID_REPLICA_MANAGER_SCOPE_CHANGE:
            return std::string("ID_REPLICA_MANAGER_SCOPE_CHANGE");
        case ID_REPLICA_MANAGER_SERIALIZE:
            return std::string("ID_REPLICA_MANAGER_SERIALIZE");
        case ID_REPLICA_MANAGER_DOWNLOAD_STARTED:
            return std::string("ID_REPLICA_MANAGER_DOWNLOAD_STARTED");
        case ID_REPLICA_MANAGER_DOWNLOAD_COMPLETE:
            return std::string("ID_REPLICA_MANAGER_DOWNLOAD_COMPLETE");
        case ID_RAKVOICE_OPEN_CHANNEL_REQUEST:
            return std::string("ID_RAKVOICE_OPEN_CHANNEL_REQUEST");
        case ID_RAKVOICE_OPEN_CHANNEL_REPLY:
            return std::string("ID_RAKVOICE_OPEN_CHANNEL_REPLY");
        case ID_RAKVOICE_CLOSE_CHANNEL:
            return std::string("ID_RAKVOICE_CLOSE_CHANNEL");
        case ID_RAKVOICE_DATA:
            return std::string("ID_RAKVOICE_DATA");
        case ID_AUTOPATCHER_GET_CHANGELIST_SINCE_DATE:
            return std::string("ID_AUTOPATCHER_GET_CHANGELIST_SINCE_DATE");
        case ID_AUTOPATCHER_CREATION_LIST:
            return std::string("ID_AUTOPATCHER_CREATION_LIST");
        case ID_AUTOPATCHER_DELETION_LIST:
            return std::string("ID_AUTOPATCHER_DELETION_LIST");
        case ID_AUTOPATCHER_GET_PATCH:
            return std::string("ID_AUTOPATCHER_GET_PATCH");
        case ID_AUTOPATCHER_PATCH_LIST:
            return std::string("ID_AUTOPATCHER_PATCH_LIST");
        case ID_AUTOPATCHER_REPOSITORY_FATAL_ERROR:
            return std::string("ID_AUTOPATCHER_REPOSITORY_FATAL_ERROR");
        case ID_AUTOPATCHER_FINISHED_INTERNAL:
            return std::string("ID_AUTOPATCHER_FINISHED_INTERNAL");
        case ID_AUTOPATCHER_RESTART_APPLICATION:
            return std::string("ID_AUTOPATCHER_RESTART_APPLICATION");
        case ID_NAT_PUNCHTHROUGH_REQUEST:
            return std::string("ID_NAT_PUNCHTHROUGH_REQUEST");
        // case ID_NAT_GROUP_PUNCHTHROUGH_REQUEST:return
        // std::string("ID_NAT_GROUP_PUNCHTHROUGH_REQUEST");
        // case ID_NAT_GROUP_PUNCHTHROUGH_REPLY:return
        // std::string("ID_NAT_GROUP_PUNCHTHROUGH_REPLY");
        case ID_NAT_CONNECT_AT_TIME:
            return std::string("ID_NAT_CONNECT_AT_TIME");
        case ID_NAT_GET_MOST_RECENT_PORT:
            return std::string("ID_NAT_GET_MOST_RECENT_PORT");
        case ID_NAT_CLIENT_READY:
            return std::string("ID_NAT_CLIENT_READY");
        // case ID_NAT_GROUP_PUNCHTHROUGH_FAILURE_NOTIFICATION:return
        // std::string("ID_NAT_GROUP_PUNCHTHROUGH_FAILURE_NOTIFICATION");
        case ID_NAT_TARGET_NOT_CONNECTED:
            return std::string("ID_NAT_TARGET_NOT_CONNECTED");
        case ID_NAT_TARGET_UNRESPONSIVE:
            return std::string("ID_NAT_TARGET_UNRESPONSIVE");
        case ID_NAT_CONNECTION_TO_TARGET_LOST:
            return std::string("ID_NAT_CONNECTION_TO_TARGET_LOST");
        case ID_NAT_ALREADY_IN_PROGRESS:
            return std::string("ID_NAT_ALREADY_IN_PROGRESS");
        case ID_NAT_PUNCHTHROUGH_FAILED:
            return std::string("ID_NAT_PUNCHTHROUGH_FAILED");
        case ID_NAT_PUNCHTHROUGH_SUCCEEDED:
            return std::string("ID_NAT_PUNCHTHROUGH_SUCCEEDED");
        // case ID_NAT_GROUP_PUNCH_FAILED:return std::string("ID_NAT_GROUP_PUNCH_FAILED");
        // case ID_NAT_GROUP_PUNCH_SUCCEEDED:return std::string("ID_NAT_GROUP_PUNCH_SUCCEEDED");
        case ID_READY_EVENT_SET:
            return std::string("ID_READY_EVENT_SET");
        case ID_READY_EVENT_UNSET:
            return std::string("ID_READY_EVENT_UNSET");
        case ID_READY_EVENT_ALL_SET:
            return std::string("ID_READY_EVENT_ALL_SET");
        case ID_LOBBY_GENERAL:
            return std::string("ID_LOBBY_GENERAL");
        case ID_RPC_PLUGIN:
            return std::string("ID_RPC_PLUGIN");
        case ID_FILE_LIST_REFERENCE_PUSH:
            return std::string("ID_FILE_LIST_REFERENCE_PUSH");
        case ID_READY_EVENT_FORCE_ALL_SET:
            return std::string("ID_READY_EVENT_FORCE_ALL_SET");
        case ID_ROOMS_EXECUTE_FUNC:
            return std::string("ID_ROOMS_EXECUTE_FUNC");
        case ID_LOBBY2_SEND_MESSAGE:
            return std::string("ID_LOBBY2_SEND_MESSAGE");
        case ID_FCM2_NEW_HOST:
            return std::string("ID_FCM2_NEW_HOST");
        case ID_UDP_PROXY_GENERAL:
            return std::string("ID_UDP_PROXY_GENERAL");
        case ID_SQLite3_EXEC:
            return std::string("ID_SQLite3_EXEC");
        case ID_SQLite3_UNKNOWN_DB:
            return std::string("ID_SQLite3_UNKNOWN_DB");
        case ID_SQLLITE_LOGGER:
            return std::string("ID_SQLLITE_LOGGER");
        case ID_NAT_TYPE_DETECTION_REQUEST:
            return std::string("ID_NAT_TYPE_DETECTION_REQUEST");
        case ID_NAT_TYPE_DETECTION_RESULT:
            return std::string("ID_NAT_TYPE_DETECTION_RESULT");
        case ID_ROUTER_2_INTERNAL:
            return std::string("ID_ROUTER_2_INTERNAL");
        case ID_ROUTER_2_FORWARDING_NO_PATH:
            return std::string("ID_ROUTER_2_FORWARDING_NO_PATH");
        case ID_ROUTER_2_FORWARDING_ESTABLISHED:
            return std::string("ID_ROUTER_2_FORWARDING_ESTABLISHED");
        case ID_ROUTER_2_REROUTED:
            return std::string("ID_ROUTER_2_REROUTED");
        // case ID_TEAM_BALANCER_REQUESTED_TEAM_CHANGE_PENDING:return
        // std::string("ID_TEAM_BALANCER_REQUESTED_TEAM_CHANGE_PENDING");
        // case ID_TEAM_BALANCER_TEAMS_LOCKED:return std::string("ID_TEAM_BALANCER_TEAMS_LOCKED");
        case ID_TEAM_BALANCER_TEAM_ASSIGNED:
            return std::string("ID_TEAM_BALANCER_TEAM_ASSIGNED");
        case ID_LIGHTSPEED_INTEGRATION:
            return std::string("ID_LIGHTSPEED_INTEGRATION");
        case ID_XBOX_LOBBY:
            return std::string("ID_XBOX_LOBBY");
        case ID_TWO_WAY_AUTHENTICATION_INCOMING_CHALLENGE_SUCCESS:
            return std::string("ID_TWO_WAY_AUTHENTICATION_INCOMING_CHALLENGE_SUCCESS");
        case ID_TWO_WAY_AUTHENTICATION_INCOMING_CHALLENGE_FAILURE:
            return std::string("ID_TWO_WAY_AUTHENTICATION_INCOMING_CHALLENGE_FAILURE");
        case ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_FAILURE:
            return std::string("ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_FAILURE");
        case ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_TIMEOUT:
            return std::string("ID_TWO_WAY_AUTHENTICATION_OUTGOING_CHALLENGE_TIMEOUT");
        case ID_CLOUD_POST_REQUEST:
            return std::string("ID_CLOUD_POST_REQUEST");
        case ID_SERVER_:
            return std::string("ID_SERVER_");
        case ID_STATIC_REPLICAS_CREATED:
            return std::string("ID_STATIC_REPLICAS_CREATED");
        default:
            return std::string("Unknown ID");
    }
}
