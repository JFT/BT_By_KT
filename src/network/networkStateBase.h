/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NETWORKSTATEBASE_H
#define NETWORKSTATEBASE_H

#include "../utils/static/error.h" // defines ErrCode

// forward declarations
namespace RakNet
{
    class RakPeerInterface;
    struct Packet;
} // namespace RakNet


// ----------------------------------------------------------------------------
class NetworkStateBase
{
   public:
    explicit NetworkStateBase(RakNet::RakPeerInterface* peer);
    NetworkStateBase(const NetworkStateBase&) = delete;
    NetworkStateBase& operator=(const NetworkStateBase&) = delete;
    virtual ~NetworkStateBase();

    /// @brief initializes the network (not done in ctor to get possible errors)
    /// @return ErrCodes::NO_ERR if everything is okay. Otherwise an error.
    virtual ErrCode init() = 0;

    inline bool isStillConnected() const { return this->stillConnected; }

   protected:
    //******  //ALL needed for process-Packets maybe rename class *****//
    RakNet::Packet* packet = nullptr;
    RakNet::RakPeerInterface* peer = nullptr;

    /// @brief stillConnected starts as true because the networkState should only be created if we
    /// are still connected
    bool stillConnected = true;

    //*****************************************************************//
};

#endif // NETWORKSTATEBASE_H
