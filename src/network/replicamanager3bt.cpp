/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "replicamanager3bt.h"
#include "connectionbt.h"
#include "../utils/debug.h"
#include "../utils/static/error.h"

ReplicaManager3BT::ReplicaManager3BT(Game* _game)
    : game(_game)
{
}

ReplicaManager3BT::~ReplicaManager3BT() {}

RakNet::Connection_RM3* ReplicaManager3BT::AllocConnection(const RakNet::SystemAddress& systemAddress,
                                                           RakNet::RakNetGUID rakNetGUID) const
{
    return new ConnectionBT(systemAddress, rakNetGUID, this->game);
}
void ReplicaManager3BT::DeallocConnection(RakNet::Connection_RM3* connection) const
{
    delete connection;
}


void ReplicaManager3BT::registerAllocationCallback(const Networking::AllocationID allocationID, CallbackType callback)
{
    debugOutLevel(Debug::DebugLevels::stateInit + 1, "registering allocation callback for allocationID", allocationID);
    this->callbacks[allocationID] = callback;
}

RakNet::Replica3* ReplicaManager3BT::callAllocationCallback(const Networking::AllocationID allocationID) const
{
    debugOutLevel(Debug::DebugLevels::firstOrderLoop - 1, "received allocation of allocationID", allocationID);
    auto it = this->callbacks.find(allocationID);
    if (it == this->callbacks.end())
    {
        Error::errContinue("received allocation for allocationID", allocationID, "but no callback was registered for this replica");
        return nullptr;
    }
    return it->second();
}
