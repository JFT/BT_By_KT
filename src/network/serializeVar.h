/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERIALIZEVAR_H
#define SERIALIZEVAR_H

#include <cstdint> // uint32_t

// ----------------------------------------------------------------------------
/// @brief Container to hold serializable variables
///
/// Stores variable to send within replica3.Serialize().
/// Furthermore there is a flag which holds the information whether this
/// variable should be serialized or not.
/// Assignment operator is overloaded to automatically set serialize flag to
/// true.
// ----------------------------------------------------------------------------
template <typename T>
struct serializeVar
{
    explicit serializeVar(T var, bool flag = false)
        : value(var)
        , serialize(flag)
    {
    }

    /// @brief sets the variable to 'var' and serialize to true
    /// @param var
    serializeVar& operator=(T newVar)
    {
        this->value = newVar;
        this->serialize = true;
        return *this;
    }

    /// @brief sets the variable to var and serialize to false
    /// use if the variable needs to be changed without setting serialize
    /// @param var
    void operator()(const T var)
    {
        this->value = var;
        // TODO: maybe just leave serialize unchanged (if it was set somewhere else?)
        this->serialize = false;
    }

    T value;
    bool serialize;
    uint32_t tick = 0; // we need to know when was the last time the variable was synced through the network.
};

#endif /* ifndef SERIALIZEVAR_H */
