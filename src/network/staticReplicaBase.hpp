#pragma once

#include <raknet/ReplicaManager3.h>

#include "serializeVar.h"
#include "slotInfos.h"
#include "staticReplicaIDs.h"

class ReplicaManager3BT;

namespace BattleTanks
{
    namespace Networking
    {
        /*!
         * \brief Base class for replicas which already exist on the client and the server and therefore don't need to be constructed by raknet.
         */
        class StaticReplicaBase : public RakNet::Replica3
        {
           public:
            StaticReplicaBase(const StaticReplicaIDs allocationId, ReplicaManager3BT* const replicaManager);

            // ----------------------------------------------------------------------------
            // Replica functions
            // ----------------------------------------------------------------------------

            // Allocatio, Deallocation
            void WriteAllocationID(RakNet::Connection_RM3* /* destinationConnection */,
                                   RakNet::BitStream* /* allocationIdBitstream */) const override
            {
            }

            void DeallocReplica(RakNet::Connection_RM3* /* sourceConnection */) override
            {
                // We will destroy this object ourselves, do nothing.
            }

            // Construction
            void SerializeConstruction(RakNet::BitStream* /* constructionBitstream */,
                                       RakNet::Connection_RM3* /* destinationConnection */) override
            {
                // static object, nothing to do.
            }

            bool DeserializeConstruction(RakNet::BitStream* /* constructionBitstream */,
                                         RakNet::Connection_RM3* /* sourceConnection */) override
            {
                // false -> don't create new object if someone requests it
                return false;
            }

            // Destruction
            void SerializeDestruction(RakNet::BitStream* /* destructionBitstream */,
                                      RakNet::Connection_RM3* /* destinationConnection */) override
            {
            }

            bool DeserializeDestruction(RakNet::BitStream* /* destructionBitstream */,
                                        RakNet::Connection_RM3* /* sourceConnection */) override
            {
                // dont accept somebody elses destruction
                return false;
            }

            RakNet::RM3ConstructionState QueryConstruction(RakNet::Connection_RM3* destinationConnection,
                                                           RakNet::ReplicaManager3* replicaManager3);

            bool QueryRemoteConstruction(RakNet::Connection_RM3* /* sourceConnection */) override
            {
                // if someone else created a new obj and send it to us don't accept it
                return false;
            }

            RakNet::RM3ActionOnPopConnection
            QueryActionOnPopConnection(RakNet::Connection_RM3* /* droppedConnection */) const override
            {
                return RakNet::RM3AOPC_DO_NOTHING;
            }

           protected:
            ReplicaManager3BT* m_replicaManager;
        };
    } // namespace Networking
} // namespace BattleTanks
