#pragma once

#ifndef ALLOCATIONIDS
#define ALLOCATIONIDS

#include <cstdint> // uint32_t

namespace Networking
{
    typedef uint32_t NetworkedAllocationIDType;

    enum AllocationID
    {
        NetworkEventSyncronization = 0,
        NetworkComponentSyncronization,
        NumAllocationIDs
    };
} // namespace Networking
#endif /* ifndef ALLOCATIONIDS */
