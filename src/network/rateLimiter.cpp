/*
BattleTanks Standalone. A C++ implementation of BattleTanks
Copyright (C) 2016-2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rateLimiter.h"

#include <raknet/GetTime.h>

#include "../utils/debug.h"

RateLimiter::RateLimiter(const RakNet::TimeMS singleTargetInterval_, const unsigned int maxActionsPerSecond_)
    : singleTargetInterval(singleTargetInterval_)
    , maxActionsPerSecond(maxActionsPerSecond_)
{
    // initialize the circular buffer
    for (size_t i = 0; i < this->actionsLastSecond.size(); i++)
    {
        this->actionsLastSecond.push_back(0);
    }
}

bool RateLimiter::queryPerformAction(const RakNet::TimeMS currentTime)
{
    // update the timing information
    // shift the action buffer back if there shold be a new slice before the newest one
    const size_t actionStart =
        std::min(static_cast<size_t>(static_cast<float>(currentTime - this->oldestActionInNewestSlice) /
                                     1000.f * static_cast<float>(this->actionsLastSecond.size())),
                 this->actionsLastSecond.size());
    debugOutLevel(Debug::DebugLevels::updateLoop,
                  "pushing back",
                  actionStart,
                  "empty time-slots because currently it's",
                  currentTime,
                  "and the newest slice is from",
                  this->oldestActionInNewestSlice);
    for (size_t i = 0; i < actionStart; i++)
    {
        this->sumActionsLastSecond -= this->actionsLastSecond.get(this->actionsLastSecond.size() - 1);
        this->actionsLastSecond.push_back(0);
        this->oldestActionInNewestSlice = currentTime;
    }

    if (sumActionsLastSecond >= this->maxActionsPerSecond)
    {
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "sum of actions in the last second was",
                      sumActionsLastSecond,
                      "which is greater or equal to the maximal rate of",
                      this->maxActionsPerSecond,
                      "-> returning false");
        return false;
    }
    else
    {
        this->actionsLastSecond.get(0)++;
        this->sumActionsLastSecond++;
        debugOutLevel(Debug::DebugLevels::updateLoop,
                      "sum of actions in the last second was",
                      sumActionsLastSecond,
                      "which is smaller than the maximal rate of",
                      this->maxActionsPerSecond,
                      "-> returning true and modified",
                      this->actionsLastSecond.to_string());
        return true;
    }
}
